--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Debian 12.4-1.pgdg100+1)
-- Dumped by pg_dump version 12.4

-- Started on 2020-10-01 09:31:57 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 420164)
-- Name: chimiesynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.chimiesynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.chimiesynthesisdatatype OWNER TO philippe;

--
-- TOC entry 203 (class 1259 OID 420172)
-- Name: chimiesynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.chimiesynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.chimiesynthesisvalue OWNER TO philippe;

--
-- TOC entry 204 (class 1259 OID 420180)
-- Name: chlorosynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.chlorosynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.chlorosynthesisdatatype OWNER TO philippe;

--
-- TOC entry 205 (class 1259 OID 420188)
-- Name: chlorosynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.chlorosynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.chlorosynthesisvalue OWNER TO philippe;

--
-- TOC entry 206 (class 1259 OID 420196)
-- Name: composite_node_data_set; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.composite_node_data_set (
    branch_node_id bigint NOT NULL,
    index integer NOT NULL,
    id_ancestor bigint,
    id_parent_node bigint,
    realnode bigint NOT NULL
);


ALTER TABLE public.composite_node_data_set OWNER TO philippe;

--
-- TOC entry 207 (class 1259 OID 420201)
-- Name: composite_node_ref_data; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.composite_node_ref_data (
    branch_node_id bigint NOT NULL,
    index integer NOT NULL,
    id_ancestor bigint,
    id_parent_node bigint,
    realnode bigint NOT NULL
);


ALTER TABLE public.composite_node_ref_data OWNER TO philippe;

--
-- TOC entry 208 (class 1259 OID 420206)
-- Name: composite_nodeable; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.composite_nodeable (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    ordernumber bigint
);


ALTER TABLE public.composite_nodeable OWNER TO philippe;

--
-- TOC entry 209 (class 1259 OID 420211)
-- Name: compositeactivityextraction; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.compositeactivityextraction (
    id bigint NOT NULL,
    dateend date,
    datestart date,
    idnode bigint NOT NULL,
    login character varying(255) NOT NULL
);


ALTER TABLE public.compositeactivityextraction OWNER TO philippe;

--
-- TOC entry 210 (class 1259 OID 420216)
-- Name: conditionprelevementsynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.conditionprelevementsynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.conditionprelevementsynthesisdatatype OWNER TO philippe;

--
-- TOC entry 211 (class 1259 OID 420224)
-- Name: conditionprelevementsynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.conditionprelevementsynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.conditionprelevementsynthesisvalue OWNER TO philippe;

--
-- TOC entry 213 (class 1259 OID 420234)
-- Name: controle_coherence_cdc; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.controle_coherence_cdc (
    datatypecdc character varying(31) NOT NULL,
    cdc_id bigint NOT NULL,
    valeur_max real,
    valeur_min real,
    vdt_id bigint NOT NULL,
    id bigint
);


ALTER TABLE public.controle_coherence_cdc OWNER TO philippe;

--
-- TOC entry 212 (class 1259 OID 420232)
-- Name: controle_coherence_cdc_cdc_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.controle_coherence_cdc_cdc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.controle_coherence_cdc_cdc_id_seq OWNER TO philippe;

--
-- TOC entry 4014 (class 0 OID 0)
-- Dependencies: 212
-- Name: controle_coherence_cdc_cdc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.controle_coherence_cdc_cdc_id_seq OWNED BY public.controle_coherence_cdc.cdc_id;


--
-- TOC entry 214 (class 1259 OID 420240)
-- Name: datatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.datatype (
    description text,
    name character varying(255) NOT NULL,
    dty_id bigint NOT NULL
);


ALTER TABLE public.datatype OWNER TO philippe;

--
-- TOC entry 215 (class 1259 OID 420248)
-- Name: datatype_unite_variable_vdt; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.datatype_unite_variable_vdt (
    datatypevariableunite_id bigint NOT NULL,
    dty_id bigint NOT NULL,
    uni_id bigint NOT NULL,
    var_id bigint NOT NULL
);


ALTER TABLE public.datatype_unite_variable_vdt OWNER TO philippe;

--
-- TOC entry 216 (class 1259 OID 420253)
-- Name: datatype_variable_unite_glacpe_dvug; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.datatype_variable_unite_glacpe_dvug (
    code_sandre character varying(255),
    contexte character varying(255),
    vdt_id bigint NOT NULL,
    dty_id bigint NOT NULL,
    uni_id bigint NOT NULL,
    var_id bigint NOT NULL
);


ALTER TABLE public.datatype_variable_unite_glacpe_dvug OWNER TO philippe;

--
-- TOC entry 218 (class 1259 OID 420263)
-- Name: dimension_dim; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.dimension_dim (
    dim_id bigint NOT NULL,
    date_depot date,
    date_releve date,
    heure_depot time without time zone,
    heure_releve time without time zone,
    lambdadepot real,
    lambdareleve real,
    xdepot real,
    xreleve real,
    ydepot real,
    yreleve real,
    zdepot real,
    zreleve real
);


ALTER TABLE public.dimension_dim OWNER TO philippe;

--
-- TOC entry 217 (class 1259 OID 420261)
-- Name: dimension_dim_dim_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.dimension_dim_dim_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dimension_dim_dim_id_seq OWNER TO philippe;

--
-- TOC entry 4015 (class 0 OID 0)
-- Dependencies: 217
-- Name: dimension_dim_dim_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.dimension_dim_dim_id_seq OWNED BY public.dimension_dim.dim_id;


--
-- TOC entry 219 (class 1259 OID 420269)
-- Name: extractiontype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.extractiontype (
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.extractiontype OWNER TO philippe;

--
-- TOC entry 220 (class 1259 OID 420277)
-- Name: filetype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.filetype (
    ft_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.filetype OWNER TO philippe;

--
-- TOC entry 221 (class 1259 OID 420285)
-- Name: genericdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.genericdatatype (
    generic_datatype_id bigint NOT NULL,
    ids_id bigint NOT NULL
);


ALTER TABLE public.genericdatatype OWNER TO philippe;

--
-- TOC entry 222 (class 1259 OID 420290)
-- Name: group_filecomp; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.group_filecomp (
    group_id bigint NOT NULL,
    file_comp_id bigint NOT NULL
);


ALTER TABLE public.group_filecomp OWNER TO philippe;

--
-- TOC entry 223 (class 1259 OID 420295)
-- Name: group_utilisateur; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.group_utilisateur (
    usr_id bigint NOT NULL,
    group_id bigint NOT NULL
);


ALTER TABLE public.group_utilisateur OWNER TO philippe;

--
-- TOC entry 224 (class 1259 OID 420300)
-- Name: groupe; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.groupe (
    id bigint NOT NULL,
    activityasstring text,
    group_name character varying(255),
    group_type character varying(255),
    group_which_tree character varying(255)
);


ALTER TABLE public.groupe OWNER TO philippe;

--
-- TOC entry 226 (class 1259 OID 420310)
-- Name: groupe_gro; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.groupe_gro (
    gro_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    code_sandre character varying(255),
    contexte character varying(255),
    nom_groupe character varying(255) NOT NULL,
    gro_gro_id bigint
);


ALTER TABLE public.groupe_gro OWNER TO philippe;

--
-- TOC entry 225 (class 1259 OID 420308)
-- Name: groupe_gro_gro_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.groupe_gro_gro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groupe_gro_gro_id_seq OWNER TO philippe;

--
-- TOC entry 4016 (class 0 OID 0)
-- Dependencies: 225
-- Name: groupe_gro_gro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.groupe_gro_gro_id_seq OWNED BY public.groupe_gro.gro_id;


--
-- TOC entry 227 (class 1259 OID 420319)
-- Name: hautefrequencesynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.hautefrequencesynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.hautefrequencesynthesisdatatype OWNER TO philippe;

--
-- TOC entry 228 (class 1259 OID 420327)
-- Name: hautefrequencesynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.hautefrequencesynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.hautefrequencesynthesisvalue OWNER TO philippe;

--
-- TOC entry 338 (class 1259 OID 421140)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO philippe;

--
-- TOC entry 229 (class 1259 OID 420335)
-- Name: hibernate_sequences; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.hibernate_sequences (
    sequence_name character varying(255) NOT NULL,
    next_val bigint
);


ALTER TABLE public.hibernate_sequences OWNER TO philippe;

--
-- TOC entry 230 (class 1259 OID 420340)
-- Name: insertion_dataset_ids; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.insertion_dataset_ids (
    ids_id bigint NOT NULL,
    ids_create_date timestamp without time zone NOT NULL,
    ids_date_debut_periode timestamp without time zone NOT NULL,
    ids_date_fin_periode timestamp without time zone NOT NULL,
    ids_publish_comment character varying(255),
    ids_publish_date timestamp without time zone,
    ids_create_user bigint NOT NULL,
    ids_publish_user bigint,
    ivf_id bigint,
    id bigint NOT NULL
);


ALTER TABLE public.insertion_dataset_ids OWNER TO philippe;

--
-- TOC entry 231 (class 1259 OID 420345)
-- Name: insertion_filecomp_ifcs; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.insertion_filecomp_ifcs (
    ifcs_id bigint NOT NULL,
    ifcs_code character varying(255) NOT NULL,
    contenttype character varying(255),
    ifcs_create_date timestamp without time zone,
    data oid NOT NULL,
    ifcs_date_debut_periode date,
    ifcs_date_fin_periode date,
    ifsc_description text NOT NULL,
    ifcs_name character varying(255) NOT NULL,
    forapplication boolean,
    ifcs_last_modify_date timestamp without time zone,
    mandatory boolean,
    ivfc_size integer,
    ifcs_create_user bigint NOT NULL,
    ft_id bigint NOT NULL,
    ifcs_last_modify_user bigint NOT NULL
);


ALTER TABLE public.insertion_filecomp_ifcs OWNER TO philippe;

--
-- TOC entry 232 (class 1259 OID 420353)
-- Name: insertion_version_file_ivf; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.insertion_version_file_ivf (
    ivf_id bigint NOT NULL,
    data oid,
    extention character varying(255),
    filename character varying(255),
    ivf_size integer,
    ivf_upload_date timestamp without time zone NOT NULL,
    ivf_version_number bigint,
    ids_id bigint NOT NULL,
    ivf_upload_user bigint NOT NULL
);


ALTER TABLE public.insertion_version_file_ivf OWNER TO philippe;

--
-- TOC entry 233 (class 1259 OID 420361)
-- Name: localisation; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.localisation (
    id bigint NOT NULL,
    localization character varying(255),
    colonne character varying(255),
    defaultstring text,
    entite character varying(255),
    localestring text
);


ALTER TABLE public.localisation OWNER TO philippe;

--
-- TOC entry 235 (class 1259 OID 420371)
-- Name: mesure_chimie_mchimie; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_chimie_mchimie (
    mchimie_id bigint NOT NULL,
    profondeur_min real,
    profondeur_max real,
    profondeurreelle real,
    sschimie_id bigint NOT NULL
);


ALTER TABLE public.mesure_chimie_mchimie OWNER TO philippe;

--
-- TOC entry 234 (class 1259 OID 420369)
-- Name: mesure_chimie_mchimie_mchimie_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_chimie_mchimie_mchimie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_chimie_mchimie_mchimie_id_seq OWNER TO philippe;

--
-- TOC entry 4017 (class 0 OID 0)
-- Dependencies: 234
-- Name: mesure_chimie_mchimie_mchimie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_chimie_mchimie_mchimie_id_seq OWNED BY public.mesure_chimie_mchimie.mchimie_id;


--
-- TOC entry 237 (class 1259 OID 420379)
-- Name: mesure_chloro_mchloro; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_chloro_mchloro (
    mchloro_id bigint NOT NULL,
    ligne_fichier_echange bigint,
    profondeur_min real NOT NULL,
    profondeur_max real NOT NULL,
    sschloro_id bigint NOT NULL
);


ALTER TABLE public.mesure_chloro_mchloro OWNER TO philippe;

--
-- TOC entry 236 (class 1259 OID 420377)
-- Name: mesure_chloro_mchloro_mchloro_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_chloro_mchloro_mchloro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_chloro_mchloro_mchloro_id_seq OWNER TO philippe;

--
-- TOC entry 4018 (class 0 OID 0)
-- Dependencies: 236
-- Name: mesure_chloro_mchloro_mchloro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_chloro_mchloro_mchloro_id_seq OWNED BY public.mesure_chloro_mchloro.mchloro_id;


--
-- TOC entry 239 (class 1259 OID 420387)
-- Name: mesure_condition_prelevement_mconditionprelevement; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_condition_prelevement_mconditionprelevement (
    mconditionprelevement_id bigint NOT NULL,
    commentaire character varying(255),
    date_prelevement date,
    heure time without time zone,
    ligne_fichier_echange bigint,
    loc_id bigint NOT NULL,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.mesure_condition_prelevement_mconditionprelevement OWNER TO philippe;

--
-- TOC entry 238 (class 1259 OID 420385)
-- Name: mesure_condition_prelevement_mcond_mconditionprelevement_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_condition_prelevement_mcond_mconditionprelevement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_condition_prelevement_mcond_mconditionprelevement_id_seq OWNER TO philippe;

--
-- TOC entry 4019 (class 0 OID 0)
-- Dependencies: 238
-- Name: mesure_condition_prelevement_mcond_mconditionprelevement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_condition_prelevement_mcond_mconditionprelevement_id_seq OWNED BY public.mesure_condition_prelevement_mconditionprelevement.mconditionprelevement_id;


--
-- TOC entry 241 (class 1259 OID 420395)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_contenus_stomacaux_mcontenustomacaux (
    mcontenustomacaux_id bigint NOT NULL,
    commentaire character varying(255),
    estomacvide boolean NOT NULL,
    ligne_fichier_echange bigint,
    longueurpoisson real,
    numeroecaille integer NOT NULL,
    numeropilulier integer NOT NULL,
    numero_poisson integer NOT NULL,
    poids real,
    poissoncompte boolean NOT NULL,
    utilisationcomptage boolean NOT NULL,
    etat_estomac bigint,
    malformation bigint,
    maturite bigint,
    sexe bigint,
    sscontenustomacaux_id bigint NOT NULL
);


ALTER TABLE public.mesure_contenus_stomacaux_mcontenustomacaux OWNER TO philippe;

--
-- TOC entry 240 (class 1259 OID 420393)
-- Name: mesure_contenus_stomacaux_mcontenustom_mcontenustomacaux_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_contenus_stomacaux_mcontenustom_mcontenustomacaux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_contenus_stomacaux_mcontenustom_mcontenustomacaux_id_seq OWNER TO philippe;

--
-- TOC entry 4020 (class 0 OID 0)
-- Dependencies: 240
-- Name: mesure_contenus_stomacaux_mcontenustom_mcontenustomacaux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_contenus_stomacaux_mcontenustom_mcontenustomacaux_id_seq OWNED BY public.mesure_contenus_stomacaux_mcontenustomacaux.mcontenustomacaux_id;


--
-- TOC entry 243 (class 1259 OID 420403)
-- Name: mesure_haute_frequence_mhautefrequence; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_haute_frequence_mhautefrequence (
    mhautefrequence_id bigint NOT NULL,
    heure time without time zone,
    ligne_fichier_echange bigint,
    sshautefrequence_id bigint NOT NULL
);


ALTER TABLE public.mesure_haute_frequence_mhautefrequence OWNER TO philippe;

--
-- TOC entry 242 (class 1259 OID 420401)
-- Name: mesure_haute_frequence_mhautefrequence_mhautefrequence_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_haute_frequence_mhautefrequence_mhautefrequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_haute_frequence_mhautefrequence_mhautefrequence_id_seq OWNER TO philippe;

--
-- TOC entry 4021 (class 0 OID 0)
-- Dependencies: 242
-- Name: mesure_haute_frequence_mhautefrequence_mhautefrequence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_haute_frequence_mhautefrequence_mhautefrequence_id_seq OWNED BY public.mesure_haute_frequence_mhautefrequence.mhautefrequence_id;


--
-- TOC entry 245 (class 1259 OID 420411)
-- Name: mesure_phytoplancton_mphytoplancton; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_phytoplancton_mphytoplancton (
    mphytoplancton_id bigint NOT NULL,
    ligne_fichier_echange bigint,
    ssphytoplancton_id bigint NOT NULL,
    tax_id bigint NOT NULL
);


ALTER TABLE public.mesure_phytoplancton_mphytoplancton OWNER TO philippe;

--
-- TOC entry 244 (class 1259 OID 420409)
-- Name: mesure_phytoplancton_mphytoplancton_mphytoplancton_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_phytoplancton_mphytoplancton_mphytoplancton_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_phytoplancton_mphytoplancton_mphytoplancton_id_seq OWNER TO philippe;

--
-- TOC entry 4022 (class 0 OID 0)
-- Dependencies: 244
-- Name: mesure_phytoplancton_mphytoplancton_mphytoplancton_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_phytoplancton_mphytoplancton_mphytoplancton_id_seq OWNED BY public.mesure_phytoplancton_mphytoplancton.mphytoplancton_id;


--
-- TOC entry 247 (class 1259 OID 420419)
-- Name: mesure_production_primaire_mpp; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_production_primaire_mpp (
    mpp_id bigint NOT NULL,
    ligne_fichier_echange bigint,
    profondeur real NOT NULL,
    sspp_id bigint NOT NULL
);


ALTER TABLE public.mesure_production_primaire_mpp OWNER TO philippe;

--
-- TOC entry 246 (class 1259 OID 420417)
-- Name: mesure_production_primaire_mpp_mpp_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_production_primaire_mpp_mpp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_production_primaire_mpp_mpp_id_seq OWNER TO philippe;

--
-- TOC entry 4023 (class 0 OID 0)
-- Dependencies: 246
-- Name: mesure_production_primaire_mpp_mpp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_production_primaire_mpp_mpp_id_seq OWNED BY public.mesure_production_primaire_mpp.mpp_id;


--
-- TOC entry 249 (class 1259 OID 420427)
-- Name: mesure_sonde_multi_msondemulti; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.mesure_sonde_multi_msondemulti (
    msondemulti_id bigint NOT NULL,
    heure time without time zone,
    ligne_fichier_echange bigint,
    profondeur real,
    sssondemulti_id bigint NOT NULL
);


ALTER TABLE public.mesure_sonde_multi_msondemulti OWNER TO philippe;

--
-- TOC entry 248 (class 1259 OID 420425)
-- Name: mesure_sonde_multi_msondemulti_msondemulti_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.mesure_sonde_multi_msondemulti_msondemulti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_sonde_multi_msondemulti_msondemulti_id_seq OWNER TO philippe;

--
-- TOC entry 4024 (class 0 OID 0)
-- Dependencies: 248
-- Name: mesure_sonde_multi_msondemulti_msondemulti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.mesure_sonde_multi_msondemulti_msondemulti_id_seq OWNED BY public.mesure_sonde_multi_msondemulti.msondemulti_id;


--
-- TOC entry 250 (class 1259 OID 420433)
-- Name: notification; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.notification (
    id bigint NOT NULL,
    archived boolean NOT NULL,
    attachment text,
    body text,
    date timestamp without time zone,
    level character varying(255) NOT NULL,
    message text NOT NULL,
    utilisateur_id bigint NOT NULL
);


ALTER TABLE public.notification OWNER TO philippe;

--
-- TOC entry 252 (class 1259 OID 420443)
-- Name: outils_mesure_ome; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.outils_mesure_ome (
    ome_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    code_sandre character varying(255),
    contexte character varying(255),
    description text,
    etalonnage character varying(255),
    fabriquant character varying(255),
    modele character varying(255),
    nom character varying(255) NOT NULL,
    numeroserie character varying(255),
    dim_id bigint,
    tome_id bigint NOT NULL
);


ALTER TABLE public.outils_mesure_ome OWNER TO philippe;

--
-- TOC entry 251 (class 1259 OID 420441)
-- Name: outils_mesure_ome_ome_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.outils_mesure_ome_ome_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.outils_mesure_ome_ome_id_seq OWNER TO philippe;

--
-- TOC entry 4025 (class 0 OID 0)
-- Dependencies: 251
-- Name: outils_mesure_ome_ome_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.outils_mesure_ome_ome_id_seq OWNED BY public.outils_mesure_ome.ome_id;


--
-- TOC entry 253 (class 1259 OID 420452)
-- Name: phytosynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.phytosynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.phytosynthesisdatatype OWNER TO philippe;

--
-- TOC entry 254 (class 1259 OID 420460)
-- Name: phytosynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.phytosynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.phytosynthesisvalue OWNER TO philippe;

--
-- TOC entry 255 (class 1259 OID 420468)
-- Name: plateforme_pla; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.plateforme_pla (
    altitude real,
    latitude real,
    longitude real,
    code_sandre character varying(255),
    contexte character varying(255),
    name character varying(255) NOT NULL,
    loc_id bigint NOT NULL,
    id bigint NOT NULL,
    tpla_id bigint NOT NULL
);


ALTER TABLE public.plateforme_pla OWNER TO philippe;

--
-- TOC entry 256 (class 1259 OID 420476)
-- Name: ppsynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.ppsynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.ppsynthesisdatatype OWNER TO philippe;

--
-- TOC entry 257 (class 1259 OID 420484)
-- Name: ppsynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.ppsynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.ppsynthesisvalue OWNER TO philippe;

--
-- TOC entry 258 (class 1259 OID 420492)
-- Name: projet_pro; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.projet_pro (
    code_sandre character varying(255),
    description_projet text,
    name character varying(255),
    pro_id bigint NOT NULL
);


ALTER TABLE public.projet_pro OWNER TO philippe;

--
-- TOC entry 260 (class 1259 OID 420502)
-- Name: projet_site_psi; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.projet_site_psi (
    psi_id bigint NOT NULL,
    commanditaire_projet character varying(255),
    commentaire_projet character varying(255),
    date_debut date,
    date_fin date,
    pro_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.projet_site_psi OWNER TO philippe;

--
-- TOC entry 259 (class 1259 OID 420500)
-- Name: projet_site_psi_psi_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.projet_site_psi_psi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projet_site_psi_psi_id_seq OWNER TO philippe;

--
-- TOC entry 4026 (class 0 OID 0)
-- Dependencies: 259
-- Name: projet_site_psi_psi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.projet_site_psi_psi_id_seq OWNED BY public.projet_site_psi.psi_id;


--
-- TOC entry 262 (class 1259 OID 420513)
-- Name: propriete_taxon_prota; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.propriete_taxon_prota (
    prota_id bigint NOT NULL,
    prota_code character varying(255) NOT NULL,
    definition text,
    isqualitative boolean DEFAULT false,
    nom character varying(255) NOT NULL,
    prota_ordre_affichage integer NOT NULL,
    prota_type_taxon character varying(255),
    prota_valuetype boolean NOT NULL
);


ALTER TABLE public.propriete_taxon_prota OWNER TO philippe;

--
-- TOC entry 261 (class 1259 OID 420511)
-- Name: propriete_taxon_prota_prota_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.propriete_taxon_prota_prota_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.propriete_taxon_prota_prota_id_seq OWNER TO philippe;

--
-- TOC entry 4027 (class 0 OID 0)
-- Dependencies: 261
-- Name: propriete_taxon_prota_prota_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.propriete_taxon_prota_prota_id_seq OWNED BY public.propriete_taxon_prota.prota_id;


--
-- TOC entry 264 (class 1259 OID 420525)
-- Name: protocole_prot; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.protocole_prot (
    prot_id bigint NOT NULL,
    url character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    descriptif text,
    legende text,
    nom character varying(255) NOT NULL
);


ALTER TABLE public.protocole_prot OWNER TO philippe;

--
-- TOC entry 263 (class 1259 OID 420523)
-- Name: protocole_prot_prot_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.protocole_prot_prot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.protocole_prot_prot_id_seq OWNER TO philippe;

--
-- TOC entry 4028 (class 0 OID 0)
-- Dependencies: 263
-- Name: protocole_prot_prot_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.protocole_prot_prot_id_seq OWNED BY public.protocole_prot.prot_id;


--
-- TOC entry 265 (class 1259 OID 420534)
-- Name: realnode; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.realnode (
    id bigint NOT NULL,
    path character varying(255) NOT NULL,
    id_ancestor bigint,
    id_nodeable bigint,
    id_parent_node bigint
);


ALTER TABLE public.realnode OWNER TO philippe;

--
-- TOC entry 266 (class 1259 OID 420539)
-- Name: refdata; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.refdata (
    description character varying(255),
    id bigint NOT NULL
);


ALTER TABLE public.refdata OWNER TO philippe;

--
-- TOC entry 267 (class 1259 OID 420544)
-- Name: requete; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.requete (
    id bigint NOT NULL,
    commentaire text,
    date timestamp without time zone,
    duree_extraction bigint NOT NULL,
    nom_fichier character varying(255) NOT NULL,
    extractiontype_id bigint NOT NULL,
    utilisateur_id bigint NOT NULL
);


ALTER TABLE public.requete OWNER TO philippe;

--
-- TOC entry 268 (class 1259 OID 420552)
-- Name: rightrequest; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.rightrequest (
    id bigint NOT NULL,
    answer character varying(255),
    concluded boolean,
    createdate timestamp without time zone,
    infos character varying(255),
    motivation character varying(255),
    reference character varying(255),
    request character varying(255) NOT NULL,
    status boolean,
    usr_id bigint NOT NULL
);


ALTER TABLE public.rightrequest OWNER TO philippe;

--
-- TOC entry 269 (class 1259 OID 420560)
-- Name: rightrequest_glacpe_rrg; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.rightrequest_glacpe_rrg (
    id bigint NOT NULL,
    answer character varying(255),
    concluded boolean,
    createdate timestamp without time zone,
    infos character varying(255),
    motivation character varying(255),
    reference character varying(255),
    request character varying(255) NOT NULL,
    status boolean,
    usr_id bigint NOT NULL,
    daterequest timestamp without time zone,
    dates character varying(255) NOT NULL,
    sites character varying(255) NOT NULL,
    telephone character varying(255),
    variables character varying(255) NOT NULL
);


ALTER TABLE public.rightrequest_glacpe_rrg OWNER TO philippe;

--
-- TOC entry 271 (class 1259 OID 420570)
-- Name: sequence_chimie_schimie; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_chimie_schimie (
    schimie_id bigint NOT NULL,
    date_debut_campagne date,
    date_fin_campagne date,
    date_prelevement date,
    date_reception date NOT NULL,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_chimie_schimie OWNER TO philippe;

--
-- TOC entry 270 (class 1259 OID 420568)
-- Name: sequence_chimie_schimie_schimie_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_chimie_schimie_schimie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_chimie_schimie_schimie_id_seq OWNER TO philippe;

--
-- TOC entry 4029 (class 0 OID 0)
-- Dependencies: 270
-- Name: sequence_chimie_schimie_schimie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_chimie_schimie_schimie_id_seq OWNED BY public.sequence_chimie_schimie.schimie_id;


--
-- TOC entry 273 (class 1259 OID 420578)
-- Name: sequence_chloro_schloro; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_chloro_schloro (
    schloro_id bigint NOT NULL,
    date date NOT NULL,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_chloro_schloro OWNER TO philippe;

--
-- TOC entry 272 (class 1259 OID 420576)
-- Name: sequence_chloro_schloro_schloro_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_chloro_schloro_schloro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_chloro_schloro_schloro_id_seq OWNER TO philippe;

--
-- TOC entry 4030 (class 0 OID 0)
-- Dependencies: 272
-- Name: sequence_chloro_schloro_schloro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_chloro_schloro_schloro_id_seq OWNED BY public.sequence_chloro_schloro.schloro_id;


--
-- TOC entry 275 (class 1259 OID 420586)
-- Name: sequence_contenus_stomacaux_scontenustomacaux; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_contenus_stomacaux_scontenustomacaux (
    scontenustomacaux_id bigint NOT NULL,
    date_prelevement date,
    duree real,
    nompecheur character varying(255),
    profondeur real,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_contenus_stomacaux_scontenustomacaux OWNER TO philippe;

--
-- TOC entry 274 (class 1259 OID 420584)
-- Name: sequence_contenus_stomacaux_scontenust_scontenustomacaux_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_contenus_stomacaux_scontenust_scontenustomacaux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_contenus_stomacaux_scontenust_scontenustomacaux_id_seq OWNER TO philippe;

--
-- TOC entry 4031 (class 0 OID 0)
-- Dependencies: 274
-- Name: sequence_contenus_stomacaux_scontenust_scontenustomacaux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_contenus_stomacaux_scontenust_scontenustomacaux_id_seq OWNED BY public.sequence_contenus_stomacaux_scontenustomacaux.scontenustomacaux_id;


--
-- TOC entry 277 (class 1259 OID 420594)
-- Name: sequence_haute_frequence_shautefrequence; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_haute_frequence_shautefrequence (
    shautefrequence_id bigint NOT NULL,
    date_prelevement date,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_haute_frequence_shautefrequence OWNER TO philippe;

--
-- TOC entry 276 (class 1259 OID 420592)
-- Name: sequence_haute_frequence_shautefrequence_shautefrequence_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_haute_frequence_shautefrequence_shautefrequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_haute_frequence_shautefrequence_shautefrequence_id_seq OWNER TO philippe;

--
-- TOC entry 4032 (class 0 OID 0)
-- Dependencies: 276
-- Name: sequence_haute_frequence_shautefrequence_shautefrequence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_haute_frequence_shautefrequence_shautefrequence_id_seq OWNED BY public.sequence_haute_frequence_shautefrequence.shautefrequence_id;


--
-- TOC entry 279 (class 1259 OID 420602)
-- Name: sequence_phytoplancton_sphytoplancton; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_phytoplancton_sphytoplancton (
    sphytoplancton_id bigint NOT NULL,
    date_prelevement date,
    nom_determinateur character varying(255),
    nom_sedimente real,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_phytoplancton_sphytoplancton OWNER TO philippe;

--
-- TOC entry 278 (class 1259 OID 420600)
-- Name: sequence_phytoplancton_sphytoplancton_sphytoplancton_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_phytoplancton_sphytoplancton_sphytoplancton_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_phytoplancton_sphytoplancton_sphytoplancton_id_seq OWNER TO philippe;

--
-- TOC entry 4033 (class 0 OID 0)
-- Dependencies: 278
-- Name: sequence_phytoplancton_sphytoplancton_sphytoplancton_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_phytoplancton_sphytoplancton_sphytoplancton_id_seq OWNED BY public.sequence_phytoplancton_sphytoplancton.sphytoplancton_id;


--
-- TOC entry 281 (class 1259 OID 420610)
-- Name: sequence_production_primaire_spp; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_production_primaire_spp (
    spp_id bigint NOT NULL,
    date date NOT NULL,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_production_primaire_spp OWNER TO philippe;

--
-- TOC entry 280 (class 1259 OID 420608)
-- Name: sequence_production_primaire_spp_spp_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_production_primaire_spp_spp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_production_primaire_spp_spp_id_seq OWNER TO philippe;

--
-- TOC entry 4034 (class 0 OID 0)
-- Dependencies: 280
-- Name: sequence_production_primaire_spp_spp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_production_primaire_spp_spp_id_seq OWNED BY public.sequence_production_primaire_spp.spp_id;


--
-- TOC entry 283 (class 1259 OID 420618)
-- Name: sequence_sonde_multi_ssondemulti; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sequence_sonde_multi_ssondemulti (
    ssondemulti_id bigint NOT NULL,
    date_prelevement date,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequence_sonde_multi_ssondemulti OWNER TO philippe;

--
-- TOC entry 282 (class 1259 OID 420616)
-- Name: sequence_sonde_multi_ssondemulti_ssondemulti_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sequence_sonde_multi_ssondemulti_ssondemulti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequence_sonde_multi_ssondemulti_ssondemulti_id_seq OWNER TO philippe;

--
-- TOC entry 4035 (class 0 OID 0)
-- Dependencies: 282
-- Name: sequence_sonde_multi_ssondemulti_ssondemulti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sequence_sonde_multi_ssondemulti_ssondemulti_id_seq OWNED BY public.sequence_sonde_multi_ssondemulti.ssondemulti_id;


--
-- TOC entry 284 (class 1259 OID 420624)
-- Name: site; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.site (
    description text,
    name character varying(255) NOT NULL,
    site_id bigint NOT NULL,
    parent_site_id bigint
);


ALTER TABLE public.site OWNER TO philippe;

--
-- TOC entry 285 (class 1259 OID 420632)
-- Name: site_glacpe_sit; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.site_glacpe_sit (
    code_sandre_me character varying(255),
    code_sandre_pe character varying(255),
    id bigint NOT NULL,
    tsit_id bigint NOT NULL
);


ALTER TABLE public.site_glacpe_sit OWNER TO philippe;

--
-- TOC entry 286 (class 1259 OID 420640)
-- Name: sondemultisynthesisdatatype; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sondemultisynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.sondemultisynthesisdatatype OWNER TO philippe;

--
-- TOC entry 287 (class 1259 OID 420648)
-- Name: sondemultisynthesisvalue; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sondemultisynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.sondemultisynthesisvalue OWNER TO philippe;

--
-- TOC entry 289 (class 1259 OID 420658)
-- Name: sous_sequence_chimie_sschimie; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_chimie_sschimie (
    sschimie_id bigint NOT NULL,
    ome_id bigint,
    loc_id bigint NOT NULL,
    schimie_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_chimie_sschimie OWNER TO philippe;

--
-- TOC entry 288 (class 1259 OID 420656)
-- Name: sous_sequence_chimie_sschimie_sschimie_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_chimie_sschimie_sschimie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_chimie_sschimie_sschimie_id_seq OWNER TO philippe;

--
-- TOC entry 4036 (class 0 OID 0)
-- Dependencies: 288
-- Name: sous_sequence_chimie_sschimie_sschimie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_chimie_sschimie_sschimie_id_seq OWNED BY public.sous_sequence_chimie_sschimie.sschimie_id;


--
-- TOC entry 291 (class 1259 OID 420666)
-- Name: sous_sequence_chloro_sschloro; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_chloro_sschloro (
    sschloro_id bigint NOT NULL,
    loc_id bigint NOT NULL,
    schloro_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_chloro_sschloro OWNER TO philippe;

--
-- TOC entry 290 (class 1259 OID 420664)
-- Name: sous_sequence_chloro_sschloro_sschloro_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_chloro_sschloro_sschloro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_chloro_sschloro_sschloro_id_seq OWNER TO philippe;

--
-- TOC entry 4037 (class 0 OID 0)
-- Dependencies: 290
-- Name: sous_sequence_chloro_sschloro_sschloro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_chloro_sschloro_sschloro_id_seq OWNED BY public.sous_sequence_chloro_sschloro.sschloro_id;


--
-- TOC entry 293 (class 1259 OID 420674)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_contenus_stomacaux_sscontenustomacaux (
    sscontenustomacaux_id bigint NOT NULL,
    espece character varying(255) NOT NULL,
    maille bigint,
    ome_id bigint NOT NULL,
    loc_id bigint NOT NULL,
    scontenustomacaux_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_contenus_stomacaux_sscontenustomacaux OWNER TO philippe;

--
-- TOC entry 292 (class 1259 OID 420672)
-- Name: sous_sequence_contenus_stomacaux_ssco_sscontenustomacaux_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_contenus_stomacaux_ssco_sscontenustomacaux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_contenus_stomacaux_ssco_sscontenustomacaux_id_seq OWNER TO philippe;

--
-- TOC entry 4038 (class 0 OID 0)
-- Dependencies: 292
-- Name: sous_sequence_contenus_stomacaux_ssco_sscontenustomacaux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_contenus_stomacaux_ssco_sscontenustomacaux_id_seq OWNED BY public.sous_sequence_contenus_stomacaux_sscontenustomacaux.sscontenustomacaux_id;


--
-- TOC entry 295 (class 1259 OID 420682)
-- Name: sous_sequence_haute_frequence_sshautefrequence; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_haute_frequence_sshautefrequence (
    sshautefrequence_id bigint NOT NULL,
    commentaire_hautefrequence character varying(255) NOT NULL,
    dim_id bigint NOT NULL,
    ome_id bigint NOT NULL,
    loc_id bigint NOT NULL,
    shautefrequence_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_haute_frequence_sshautefrequence OWNER TO philippe;

--
-- TOC entry 294 (class 1259 OID 420680)
-- Name: sous_sequence_haute_frequence_sshautefr_sshautefrequence_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_haute_frequence_sshautefr_sshautefrequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_haute_frequence_sshautefr_sshautefrequence_id_seq OWNER TO philippe;

--
-- TOC entry 4039 (class 0 OID 0)
-- Dependencies: 294
-- Name: sous_sequence_haute_frequence_sshautefr_sshautefrequence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_haute_frequence_sshautefr_sshautefrequence_id_seq OWNED BY public.sous_sequence_haute_frequence_sshautefrequence.sshautefrequence_id;


--
-- TOC entry 297 (class 1259 OID 420690)
-- Name: sous_sequence_phytoplancton_ssphytoplancton; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_phytoplancton_ssphytoplancton (
    ssphytoplancton_id bigint NOT NULL,
    profondeur_max real,
    profondeur_min real,
    surface_comptage real,
    mesure_id bigint,
    prelevement_id bigint,
    loc_id bigint NOT NULL,
    sphytoplancton_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_phytoplancton_ssphytoplancton OWNER TO philippe;

--
-- TOC entry 296 (class 1259 OID 420688)
-- Name: sous_sequence_phytoplancton_ssphytoplanc_ssphytoplancton_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_phytoplancton_ssphytoplanc_ssphytoplancton_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_phytoplancton_ssphytoplanc_ssphytoplancton_id_seq OWNER TO philippe;

--
-- TOC entry 4040 (class 0 OID 0)
-- Dependencies: 296
-- Name: sous_sequence_phytoplancton_ssphytoplanc_ssphytoplancton_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_phytoplancton_ssphytoplanc_ssphytoplancton_id_seq OWNED BY public.sous_sequence_phytoplancton_ssphytoplancton.ssphytoplancton_id;


--
-- TOC entry 299 (class 1259 OID 420698)
-- Name: sous_sequence_production_primaire_sspp; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_production_primaire_sspp (
    sspp_id bigint NOT NULL,
    duree_incub time without time zone,
    heure_debut_incub time without time zone,
    heure_fin_incub time without time zone,
    loc_id bigint NOT NULL,
    spp_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_production_primaire_sspp OWNER TO philippe;

--
-- TOC entry 298 (class 1259 OID 420696)
-- Name: sous_sequence_production_primaire_sspp_sspp_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_production_primaire_sspp_sspp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_production_primaire_sspp_sspp_id_seq OWNER TO philippe;

--
-- TOC entry 4041 (class 0 OID 0)
-- Dependencies: 298
-- Name: sous_sequence_production_primaire_sspp_sspp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_production_primaire_sspp_sspp_id_seq OWNED BY public.sous_sequence_production_primaire_sspp.sspp_id;


--
-- TOC entry 301 (class 1259 OID 420706)
-- Name: sous_sequence_sonde_multi_sssondemulti; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.sous_sequence_sonde_multi_sssondemulti (
    sssondemulti_id bigint NOT NULL,
    commentaire_sonde character varying(255) NOT NULL,
    ome_id bigint NOT NULL,
    loc_id bigint NOT NULL,
    ssondemulti_id bigint NOT NULL
);


ALTER TABLE public.sous_sequence_sonde_multi_sssondemulti OWNER TO philippe;

--
-- TOC entry 300 (class 1259 OID 420704)
-- Name: sous_sequence_sonde_multi_sssondemulti_sssondemulti_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.sous_sequence_sonde_multi_sssondemulti_sssondemulti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sous_sequence_sonde_multi_sssondemulti_sssondemulti_id_seq OWNER TO philippe;

--
-- TOC entry 4042 (class 0 OID 0)
-- Dependencies: 300
-- Name: sous_sequence_sonde_multi_sssondemulti_sssondemulti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.sous_sequence_sonde_multi_sssondemulti_sssondemulti_id_seq OWNED BY public.sous_sequence_sonde_multi_sssondemulti.sssondemulti_id;


--
-- TOC entry 303 (class 1259 OID 420714)
-- Name: taxon_niveau_taxn; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.taxon_niveau_taxn (
    taxn_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    code_sandre character varying(255),
    contexte character varying(255),
    nom character varying(255) NOT NULL
);


ALTER TABLE public.taxon_niveau_taxn OWNER TO philippe;

--
-- TOC entry 302 (class 1259 OID 420712)
-- Name: taxon_niveau_taxn_taxn_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.taxon_niveau_taxn_taxn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxon_niveau_taxn_taxn_id_seq OWNER TO philippe;

--
-- TOC entry 4043 (class 0 OID 0)
-- Dependencies: 302
-- Name: taxon_niveau_taxn_taxn_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.taxon_niveau_taxn_taxn_id_seq OWNED BY public.taxon_niveau_taxn.taxn_id;


--
-- TOC entry 305 (class 1259 OID 420725)
-- Name: taxon_propriete_taxon_taprota; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.taxon_propriete_taxon_taprota (
    taprota_id bigint NOT NULL,
    prota_id bigint NOT NULL,
    tax_id bigint NOT NULL,
    vprota_id bigint NOT NULL
);


ALTER TABLE public.taxon_propriete_taxon_taprota OWNER TO philippe;

--
-- TOC entry 304 (class 1259 OID 420723)
-- Name: taxon_propriete_taxon_taprota_taprota_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.taxon_propriete_taxon_taprota_taprota_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxon_propriete_taxon_taprota_taprota_id_seq OWNER TO philippe;

--
-- TOC entry 4044 (class 0 OID 0)
-- Dependencies: 304
-- Name: taxon_propriete_taxon_taprota_taprota_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.taxon_propriete_taxon_taprota_taprota_id_seq OWNED BY public.taxon_propriete_taxon_taprota.taprota_id;


--
-- TOC entry 307 (class 1259 OID 420733)
-- Name: taxon_tax; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.taxon_tax (
    tax_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    code_sandre_sup character varying(255),
    code_sandre_tax character varying(255),
    nomlatin character varying(255) NOT NULL,
    theme character varying(255),
    taxn_id bigint NOT NULL,
    taxonparent_id bigint
);


ALTER TABLE public.taxon_tax OWNER TO philippe;

--
-- TOC entry 306 (class 1259 OID 420731)
-- Name: taxon_tax_tax_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.taxon_tax_tax_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxon_tax_tax_id_seq OWNER TO philippe;

--
-- TOC entry 4045 (class 0 OID 0)
-- Dependencies: 306
-- Name: taxon_tax_tax_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.taxon_tax_tax_id_seq OWNED BY public.taxon_tax.tax_id;


--
-- TOC entry 308 (class 1259 OID 420742)
-- Name: theme; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.theme (
    description text,
    id bigint NOT NULL
);


ALTER TABLE public.theme OWNER TO philippe;

--
-- TOC entry 310 (class 1259 OID 420752)
-- Name: type_outils_mesure_tome; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.type_outils_mesure_tome (
    tome_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    code_sandre character varying(255),
    contexte character varying(255),
    nom character varying(255) NOT NULL,
    vq_id bigint
);


ALTER TABLE public.type_outils_mesure_tome OWNER TO philippe;

--
-- TOC entry 309 (class 1259 OID 420750)
-- Name: type_outils_mesure_tome_tome_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.type_outils_mesure_tome_tome_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_outils_mesure_tome_tome_id_seq OWNER TO philippe;

--
-- TOC entry 4046 (class 0 OID 0)
-- Dependencies: 309
-- Name: type_outils_mesure_tome_tome_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.type_outils_mesure_tome_tome_id_seq OWNED BY public.type_outils_mesure_tome.tome_id;


--
-- TOC entry 311 (class 1259 OID 420761)
-- Name: type_plateforme_tpla; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.type_plateforme_tpla (
    code_sandre character varying(255),
    contexte character varying(255),
    description text,
    name character varying(255),
    tpla_id bigint NOT NULL
);


ALTER TABLE public.type_plateforme_tpla OWNER TO philippe;

--
-- TOC entry 312 (class 1259 OID 420769)
-- Name: type_site_tsit; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.type_site_tsit (
    code_sandre character varying(255),
    contexte character varying(255),
    description text,
    nom character varying(255),
    tsit_id bigint NOT NULL
);


ALTER TABLE public.type_site_tsit OWNER TO philippe;

--
-- TOC entry 313 (class 1259 OID 420777)
-- Name: unite; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.unite (
    uni_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.unite OWNER TO philippe;

--
-- TOC entry 314 (class 1259 OID 420785)
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.utilisateur (
    id bigint NOT NULL,
    language character varying(255),
    active boolean NOT NULL,
    email character varying(255) NOT NULL,
    emploi character varying(255),
    isroot boolean,
    login character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    poste character varying(255),
    prenom character varying(255) NOT NULL
);


ALTER TABLE public.utilisateur OWNER TO philippe;

--
-- TOC entry 316 (class 1259 OID 420795)
-- Name: valeur_condition_prelevement_vmconditionprelevement; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_condition_prelevement_vmconditionprelevement (
    vconditionprelevement_id bigint NOT NULL,
    valeur real,
    mconditionprelevement_id bigint NOT NULL,
    id bigint NOT NULL,
    vq_id bigint
);


ALTER TABLE public.valeur_condition_prelevement_vmconditionprelevement OWNER TO philippe;

--
-- TOC entry 315 (class 1259 OID 420793)
-- Name: valeur_condition_prelevement_vmcon_vconditionprelevement_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_condition_prelevement_vmcon_vconditionprelevement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_condition_prelevement_vmcon_vconditionprelevement_id_seq OWNER TO philippe;

--
-- TOC entry 4047 (class 0 OID 0)
-- Dependencies: 315
-- Name: valeur_condition_prelevement_vmcon_vconditionprelevement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_condition_prelevement_vmcon_vconditionprelevement_id_seq OWNED BY public.valeur_condition_prelevement_vmconditionprelevement.vconditionprelevement_id;


--
-- TOC entry 318 (class 1259 OID 420803)
-- Name: valeur_mesure_chimie_vmchimie; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_chimie_vmchimie (
    vmchimie_id bigint NOT NULL,
    flag bytea,
    ligne_fichier_echange bigint,
    valeur real,
    mchimie_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_chimie_vmchimie OWNER TO philippe;

--
-- TOC entry 317 (class 1259 OID 420801)
-- Name: valeur_mesure_chimie_vmchimie_vmchimie_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_chimie_vmchimie_vmchimie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_chimie_vmchimie_vmchimie_id_seq OWNER TO philippe;

--
-- TOC entry 4048 (class 0 OID 0)
-- Dependencies: 317
-- Name: valeur_mesure_chimie_vmchimie_vmchimie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_chimie_vmchimie_vmchimie_id_seq OWNED BY public.valeur_mesure_chimie_vmchimie.vmchimie_id;


--
-- TOC entry 320 (class 1259 OID 420814)
-- Name: valeur_mesure_chloro_vmchloro; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_chloro_vmchloro (
    vmchloro_id bigint NOT NULL,
    valeur real,
    mchloro_id bigint NOT NULL,
    id bigint NOT NULL,
    var_id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_chloro_vmchloro OWNER TO philippe;

--
-- TOC entry 319 (class 1259 OID 420812)
-- Name: valeur_mesure_chloro_vmchloro_vmchloro_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_chloro_vmchloro_vmchloro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_chloro_vmchloro_vmchloro_id_seq OWNER TO philippe;

--
-- TOC entry 4049 (class 0 OID 0)
-- Dependencies: 319
-- Name: valeur_mesure_chloro_vmchloro_vmchloro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_chloro_vmchloro_vmchloro_id_seq OWNED BY public.valeur_mesure_chloro_vmchloro.vmchloro_id;


--
-- TOC entry 322 (class 1259 OID 420822)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux (
    vmcontenustomacaux_id bigint NOT NULL,
    valeur real,
    mcontenustomacaux_id bigint NOT NULL,
    proie bigint,
    id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux OWNER TO philippe;

--
-- TOC entry 321 (class 1259 OID 420820)
-- Name: valeur_mesure_contenus_stomacaux_vmco_vmcontenustomacaux_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_contenus_stomacaux_vmco_vmcontenustomacaux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_contenus_stomacaux_vmco_vmcontenustomacaux_id_seq OWNER TO philippe;

--
-- TOC entry 4050 (class 0 OID 0)
-- Dependencies: 321
-- Name: valeur_mesure_contenus_stomacaux_vmco_vmcontenustomacaux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_contenus_stomacaux_vmco_vmcontenustomacaux_id_seq OWNED BY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux.vmcontenustomacaux_id;


--
-- TOC entry 324 (class 1259 OID 420830)
-- Name: valeur_mesure_haute_frequence_vmhautefrequence; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_haute_frequence_vmhautefrequence (
    vmhautefrequence_id bigint NOT NULL,
    codequalite real,
    valeur real,
    mhautefrequence_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_haute_frequence_vmhautefrequence OWNER TO philippe;

--
-- TOC entry 323 (class 1259 OID 420828)
-- Name: valeur_mesure_haute_frequence_vmhautefr_vmhautefrequence_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_haute_frequence_vmhautefr_vmhautefrequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_haute_frequence_vmhautefr_vmhautefrequence_id_seq OWNER TO philippe;

--
-- TOC entry 4051 (class 0 OID 0)
-- Dependencies: 323
-- Name: valeur_mesure_haute_frequence_vmhautefr_vmhautefrequence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_haute_frequence_vmhautefr_vmhautefrequence_id_seq OWNED BY public.valeur_mesure_haute_frequence_vmhautefrequence.vmhautefrequence_id;


--
-- TOC entry 326 (class 1259 OID 420838)
-- Name: valeur_mesure_phytoplancton_vmphytoplancton; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_phytoplancton_vmphytoplancton (
    vmphytoplancton_id bigint NOT NULL,
    valeur real,
    mphytoplancton_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_phytoplancton_vmphytoplancton OWNER TO philippe;

--
-- TOC entry 325 (class 1259 OID 420836)
-- Name: valeur_mesure_phytoplancton_vmphytoplanc_vmphytoplancton_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_phytoplancton_vmphytoplanc_vmphytoplancton_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_phytoplancton_vmphytoplanc_vmphytoplancton_id_seq OWNER TO philippe;

--
-- TOC entry 4052 (class 0 OID 0)
-- Dependencies: 325
-- Name: valeur_mesure_phytoplancton_vmphytoplanc_vmphytoplancton_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_phytoplancton_vmphytoplanc_vmphytoplancton_id_seq OWNED BY public.valeur_mesure_phytoplancton_vmphytoplancton.vmphytoplancton_id;


--
-- TOC entry 328 (class 1259 OID 420846)
-- Name: valeur_mesure_production_primaire_vmpp; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_production_primaire_vmpp (
    vmpp_id bigint NOT NULL,
    valeur real,
    mpp_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_production_primaire_vmpp OWNER TO philippe;

--
-- TOC entry 327 (class 1259 OID 420844)
-- Name: valeur_mesure_production_primaire_vmpp_vmpp_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_production_primaire_vmpp_vmpp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_production_primaire_vmpp_vmpp_id_seq OWNER TO philippe;

--
-- TOC entry 4053 (class 0 OID 0)
-- Dependencies: 327
-- Name: valeur_mesure_production_primaire_vmpp_vmpp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_production_primaire_vmpp_vmpp_id_seq OWNED BY public.valeur_mesure_production_primaire_vmpp.vmpp_id;


--
-- TOC entry 330 (class 1259 OID 420854)
-- Name: valeur_mesure_sonde_multi_vmsondemulti; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_mesure_sonde_multi_vmsondemulti (
    vmsondemulti_id bigint NOT NULL,
    valeur real,
    msondemulti_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.valeur_mesure_sonde_multi_vmsondemulti OWNER TO philippe;

--
-- TOC entry 329 (class 1259 OID 420852)
-- Name: valeur_mesure_sonde_multi_vmsondemulti_vmsondemulti_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_mesure_sonde_multi_vmsondemulti_vmsondemulti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_mesure_sonde_multi_vmsondemulti_vmsondemulti_id_seq OWNER TO philippe;

--
-- TOC entry 4054 (class 0 OID 0)
-- Dependencies: 329
-- Name: valeur_mesure_sonde_multi_vmsondemulti_vmsondemulti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_mesure_sonde_multi_vmsondemulti_vmsondemulti_id_seq OWNED BY public.valeur_mesure_sonde_multi_vmsondemulti.vmsondemulti_id;


--
-- TOC entry 332 (class 1259 OID 420862)
-- Name: valeur_propriete_taxon_vprota; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_propriete_taxon_vprota (
    vprota_id bigint NOT NULL,
    vprota_floatvalue real,
    vprota_stringvalue text,
    vq_id bigint
);


ALTER TABLE public.valeur_propriete_taxon_vprota OWNER TO philippe;

--
-- TOC entry 331 (class 1259 OID 420860)
-- Name: valeur_propriete_taxon_vprota_vprota_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.valeur_propriete_taxon_vprota_vprota_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_propriete_taxon_vprota_vprota_id_seq OWNER TO philippe;

--
-- TOC entry 4055 (class 0 OID 0)
-- Dependencies: 331
-- Name: valeur_propriete_taxon_vprota_vprota_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.valeur_propriete_taxon_vprota_vprota_id_seq OWNED BY public.valeur_propriete_taxon_vprota.vprota_id;


--
-- TOC entry 333 (class 1259 OID 420871)
-- Name: valeur_qualitative; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.valeur_qualitative (
    vq_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    typelist character varying(255),
    valeur character varying(255) NOT NULL
);


ALTER TABLE public.valeur_qualitative OWNER TO philippe;

--
-- TOC entry 334 (class 1259 OID 420879)
-- Name: variable; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.variable (
    affichage character varying(255),
    definition text,
    isqualitative boolean DEFAULT false,
    var_id bigint NOT NULL
);


ALTER TABLE public.variable OWNER TO philippe;

--
-- TOC entry 335 (class 1259 OID 420888)
-- Name: variable_glacpe_varg; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.variable_glacpe_varg (
    code_sandre character varying(255),
    contexte character varying(255),
    name character varying(255),
    ordre_affichage_groupe integer,
    var_id bigint NOT NULL,
    gro_id bigint,
    vnor_id bigint NOT NULL
);


ALTER TABLE public.variable_glacpe_varg OWNER TO philippe;

--
-- TOC entry 337 (class 1259 OID 420898)
-- Name: variable_norme_vnor; Type: TABLE; Schema: public; Owner: philippe
--

CREATE TABLE public.variable_norme_vnor (
    vnor_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    definition text,
    nom character varying(255) NOT NULL
);


ALTER TABLE public.variable_norme_vnor OWNER TO philippe;

--
-- TOC entry 336 (class 1259 OID 420896)
-- Name: variable_norme_vnor_vnor_id_seq; Type: SEQUENCE; Schema: public; Owner: philippe
--

CREATE SEQUENCE public.variable_norme_vnor_vnor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.variable_norme_vnor_vnor_id_seq OWNER TO philippe;

--
-- TOC entry 4056 (class 0 OID 0)
-- Dependencies: 336
-- Name: variable_norme_vnor_vnor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: philippe
--

ALTER SEQUENCE public.variable_norme_vnor_vnor_id_seq OWNED BY public.variable_norme_vnor.vnor_id;


--
-- TOC entry 3280 (class 2604 OID 420237)
-- Name: controle_coherence_cdc cdc_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.controle_coherence_cdc ALTER COLUMN cdc_id SET DEFAULT nextval('public.controle_coherence_cdc_cdc_id_seq'::regclass);


--
-- TOC entry 3281 (class 2604 OID 420266)
-- Name: dimension_dim dim_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.dimension_dim ALTER COLUMN dim_id SET DEFAULT nextval('public.dimension_dim_dim_id_seq'::regclass);


--
-- TOC entry 3282 (class 2604 OID 420313)
-- Name: groupe_gro gro_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe_gro ALTER COLUMN gro_id SET DEFAULT nextval('public.groupe_gro_gro_id_seq'::regclass);


--
-- TOC entry 3283 (class 2604 OID 420374)
-- Name: mesure_chimie_mchimie mchimie_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chimie_mchimie ALTER COLUMN mchimie_id SET DEFAULT nextval('public.mesure_chimie_mchimie_mchimie_id_seq'::regclass);


--
-- TOC entry 3284 (class 2604 OID 420382)
-- Name: mesure_chloro_mchloro mchloro_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chloro_mchloro ALTER COLUMN mchloro_id SET DEFAULT nextval('public.mesure_chloro_mchloro_mchloro_id_seq'::regclass);


--
-- TOC entry 3285 (class 2604 OID 420390)
-- Name: mesure_condition_prelevement_mconditionprelevement mconditionprelevement_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_condition_prelevement_mconditionprelevement ALTER COLUMN mconditionprelevement_id SET DEFAULT nextval('public.mesure_condition_prelevement_mcond_mconditionprelevement_id_seq'::regclass);


--
-- TOC entry 3286 (class 2604 OID 420398)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux mcontenustomacaux_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux ALTER COLUMN mcontenustomacaux_id SET DEFAULT nextval('public.mesure_contenus_stomacaux_mcontenustom_mcontenustomacaux_id_seq'::regclass);


--
-- TOC entry 3287 (class 2604 OID 420406)
-- Name: mesure_haute_frequence_mhautefrequence mhautefrequence_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_haute_frequence_mhautefrequence ALTER COLUMN mhautefrequence_id SET DEFAULT nextval('public.mesure_haute_frequence_mhautefrequence_mhautefrequence_id_seq'::regclass);


--
-- TOC entry 3288 (class 2604 OID 420414)
-- Name: mesure_phytoplancton_mphytoplancton mphytoplancton_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_phytoplancton_mphytoplancton ALTER COLUMN mphytoplancton_id SET DEFAULT nextval('public.mesure_phytoplancton_mphytoplancton_mphytoplancton_id_seq'::regclass);


--
-- TOC entry 3289 (class 2604 OID 420422)
-- Name: mesure_production_primaire_mpp mpp_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_production_primaire_mpp ALTER COLUMN mpp_id SET DEFAULT nextval('public.mesure_production_primaire_mpp_mpp_id_seq'::regclass);


--
-- TOC entry 3290 (class 2604 OID 420430)
-- Name: mesure_sonde_multi_msondemulti msondemulti_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_sonde_multi_msondemulti ALTER COLUMN msondemulti_id SET DEFAULT nextval('public.mesure_sonde_multi_msondemulti_msondemulti_id_seq'::regclass);


--
-- TOC entry 3291 (class 2604 OID 420446)
-- Name: outils_mesure_ome ome_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.outils_mesure_ome ALTER COLUMN ome_id SET DEFAULT nextval('public.outils_mesure_ome_ome_id_seq'::regclass);


--
-- TOC entry 3292 (class 2604 OID 420505)
-- Name: projet_site_psi psi_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_site_psi ALTER COLUMN psi_id SET DEFAULT nextval('public.projet_site_psi_psi_id_seq'::regclass);


--
-- TOC entry 3293 (class 2604 OID 420516)
-- Name: propriete_taxon_prota prota_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.propriete_taxon_prota ALTER COLUMN prota_id SET DEFAULT nextval('public.propriete_taxon_prota_prota_id_seq'::regclass);


--
-- TOC entry 3295 (class 2604 OID 420528)
-- Name: protocole_prot prot_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.protocole_prot ALTER COLUMN prot_id SET DEFAULT nextval('public.protocole_prot_prot_id_seq'::regclass);


--
-- TOC entry 3296 (class 2604 OID 420573)
-- Name: sequence_chimie_schimie schimie_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chimie_schimie ALTER COLUMN schimie_id SET DEFAULT nextval('public.sequence_chimie_schimie_schimie_id_seq'::regclass);


--
-- TOC entry 3297 (class 2604 OID 420581)
-- Name: sequence_chloro_schloro schloro_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chloro_schloro ALTER COLUMN schloro_id SET DEFAULT nextval('public.sequence_chloro_schloro_schloro_id_seq'::regclass);


--
-- TOC entry 3298 (class 2604 OID 420589)
-- Name: sequence_contenus_stomacaux_scontenustomacaux scontenustomacaux_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_contenus_stomacaux_scontenustomacaux ALTER COLUMN scontenustomacaux_id SET DEFAULT nextval('public.sequence_contenus_stomacaux_scontenust_scontenustomacaux_id_seq'::regclass);


--
-- TOC entry 3299 (class 2604 OID 420597)
-- Name: sequence_haute_frequence_shautefrequence shautefrequence_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_haute_frequence_shautefrequence ALTER COLUMN shautefrequence_id SET DEFAULT nextval('public.sequence_haute_frequence_shautefrequence_shautefrequence_id_seq'::regclass);


--
-- TOC entry 3300 (class 2604 OID 420605)
-- Name: sequence_phytoplancton_sphytoplancton sphytoplancton_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_phytoplancton_sphytoplancton ALTER COLUMN sphytoplancton_id SET DEFAULT nextval('public.sequence_phytoplancton_sphytoplancton_sphytoplancton_id_seq'::regclass);


--
-- TOC entry 3301 (class 2604 OID 420613)
-- Name: sequence_production_primaire_spp spp_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_production_primaire_spp ALTER COLUMN spp_id SET DEFAULT nextval('public.sequence_production_primaire_spp_spp_id_seq'::regclass);


--
-- TOC entry 3302 (class 2604 OID 420621)
-- Name: sequence_sonde_multi_ssondemulti ssondemulti_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_sonde_multi_ssondemulti ALTER COLUMN ssondemulti_id SET DEFAULT nextval('public.sequence_sonde_multi_ssondemulti_ssondemulti_id_seq'::regclass);


--
-- TOC entry 3303 (class 2604 OID 420661)
-- Name: sous_sequence_chimie_sschimie sschimie_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chimie_sschimie ALTER COLUMN sschimie_id SET DEFAULT nextval('public.sous_sequence_chimie_sschimie_sschimie_id_seq'::regclass);


--
-- TOC entry 3304 (class 2604 OID 420669)
-- Name: sous_sequence_chloro_sschloro sschloro_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chloro_sschloro ALTER COLUMN sschloro_id SET DEFAULT nextval('public.sous_sequence_chloro_sschloro_sschloro_id_seq'::regclass);


--
-- TOC entry 3305 (class 2604 OID 420677)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux sscontenustomacaux_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux ALTER COLUMN sscontenustomacaux_id SET DEFAULT nextval('public.sous_sequence_contenus_stomacaux_ssco_sscontenustomacaux_id_seq'::regclass);


--
-- TOC entry 3306 (class 2604 OID 420685)
-- Name: sous_sequence_haute_frequence_sshautefrequence sshautefrequence_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence ALTER COLUMN sshautefrequence_id SET DEFAULT nextval('public.sous_sequence_haute_frequence_sshautefr_sshautefrequence_id_seq'::regclass);


--
-- TOC entry 3307 (class 2604 OID 420693)
-- Name: sous_sequence_phytoplancton_ssphytoplancton ssphytoplancton_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton ALTER COLUMN ssphytoplancton_id SET DEFAULT nextval('public.sous_sequence_phytoplancton_ssphytoplanc_ssphytoplancton_id_seq'::regclass);


--
-- TOC entry 3308 (class 2604 OID 420701)
-- Name: sous_sequence_production_primaire_sspp sspp_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_production_primaire_sspp ALTER COLUMN sspp_id SET DEFAULT nextval('public.sous_sequence_production_primaire_sspp_sspp_id_seq'::regclass);


--
-- TOC entry 3309 (class 2604 OID 420709)
-- Name: sous_sequence_sonde_multi_sssondemulti sssondemulti_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_sonde_multi_sssondemulti ALTER COLUMN sssondemulti_id SET DEFAULT nextval('public.sous_sequence_sonde_multi_sssondemulti_sssondemulti_id_seq'::regclass);


--
-- TOC entry 3310 (class 2604 OID 420717)
-- Name: taxon_niveau_taxn taxn_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_niveau_taxn ALTER COLUMN taxn_id SET DEFAULT nextval('public.taxon_niveau_taxn_taxn_id_seq'::regclass);


--
-- TOC entry 3311 (class 2604 OID 420728)
-- Name: taxon_propriete_taxon_taprota taprota_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_propriete_taxon_taprota ALTER COLUMN taprota_id SET DEFAULT nextval('public.taxon_propriete_taxon_taprota_taprota_id_seq'::regclass);


--
-- TOC entry 3312 (class 2604 OID 420736)
-- Name: taxon_tax tax_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax ALTER COLUMN tax_id SET DEFAULT nextval('public.taxon_tax_tax_id_seq'::regclass);


--
-- TOC entry 3313 (class 2604 OID 420755)
-- Name: type_outils_mesure_tome tome_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_outils_mesure_tome ALTER COLUMN tome_id SET DEFAULT nextval('public.type_outils_mesure_tome_tome_id_seq'::regclass);


--
-- TOC entry 3314 (class 2604 OID 420798)
-- Name: valeur_condition_prelevement_vmconditionprelevement vconditionprelevement_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_condition_prelevement_vmconditionprelevement ALTER COLUMN vconditionprelevement_id SET DEFAULT nextval('public.valeur_condition_prelevement_vmcon_vconditionprelevement_id_seq'::regclass);


--
-- TOC entry 3315 (class 2604 OID 420806)
-- Name: valeur_mesure_chimie_vmchimie vmchimie_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chimie_vmchimie ALTER COLUMN vmchimie_id SET DEFAULT nextval('public.valeur_mesure_chimie_vmchimie_vmchimie_id_seq'::regclass);


--
-- TOC entry 3316 (class 2604 OID 420817)
-- Name: valeur_mesure_chloro_vmchloro vmchloro_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chloro_vmchloro ALTER COLUMN vmchloro_id SET DEFAULT nextval('public.valeur_mesure_chloro_vmchloro_vmchloro_id_seq'::regclass);


--
-- TOC entry 3317 (class 2604 OID 420825)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux vmcontenustomacaux_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux ALTER COLUMN vmcontenustomacaux_id SET DEFAULT nextval('public.valeur_mesure_contenus_stomacaux_vmco_vmcontenustomacaux_id_seq'::regclass);


--
-- TOC entry 3318 (class 2604 OID 420833)
-- Name: valeur_mesure_haute_frequence_vmhautefrequence vmhautefrequence_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_haute_frequence_vmhautefrequence ALTER COLUMN vmhautefrequence_id SET DEFAULT nextval('public.valeur_mesure_haute_frequence_vmhautefr_vmhautefrequence_id_seq'::regclass);


--
-- TOC entry 3319 (class 2604 OID 420841)
-- Name: valeur_mesure_phytoplancton_vmphytoplancton vmphytoplancton_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_phytoplancton_vmphytoplancton ALTER COLUMN vmphytoplancton_id SET DEFAULT nextval('public.valeur_mesure_phytoplancton_vmphytoplanc_vmphytoplancton_id_seq'::regclass);


--
-- TOC entry 3320 (class 2604 OID 420849)
-- Name: valeur_mesure_production_primaire_vmpp vmpp_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_production_primaire_vmpp ALTER COLUMN vmpp_id SET DEFAULT nextval('public.valeur_mesure_production_primaire_vmpp_vmpp_id_seq'::regclass);


--
-- TOC entry 3321 (class 2604 OID 420857)
-- Name: valeur_mesure_sonde_multi_vmsondemulti vmsondemulti_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_sonde_multi_vmsondemulti ALTER COLUMN vmsondemulti_id SET DEFAULT nextval('public.valeur_mesure_sonde_multi_vmsondemulti_vmsondemulti_id_seq'::regclass);


--
-- TOC entry 3322 (class 2604 OID 420865)
-- Name: valeur_propriete_taxon_vprota vprota_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_propriete_taxon_vprota ALTER COLUMN vprota_id SET DEFAULT nextval('public.valeur_propriete_taxon_vprota_vprota_id_seq'::regclass);


--
-- TOC entry 3324 (class 2604 OID 420901)
-- Name: variable_norme_vnor vnor_id; Type: DEFAULT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_norme_vnor ALTER COLUMN vnor_id SET DEFAULT nextval('public.variable_norme_vnor_vnor_id_seq'::regclass);


--
-- TOC entry 3326 (class 2606 OID 420171)
-- Name: chimiesynthesisdatatype chimiesynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.chimiesynthesisdatatype
    ADD CONSTRAINT chimiesynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3328 (class 2606 OID 420179)
-- Name: chimiesynthesisvalue chimiesynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.chimiesynthesisvalue
    ADD CONSTRAINT chimiesynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3331 (class 2606 OID 420187)
-- Name: chlorosynthesisdatatype chlorosynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.chlorosynthesisdatatype
    ADD CONSTRAINT chlorosynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3333 (class 2606 OID 420195)
-- Name: chlorosynthesisvalue chlorosynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.chlorosynthesisvalue
    ADD CONSTRAINT chlorosynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3336 (class 2606 OID 420200)
-- Name: composite_node_data_set composite_node_data_set_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT composite_node_data_set_pkey PRIMARY KEY (branch_node_id);


--
-- TOC entry 3338 (class 2606 OID 420205)
-- Name: composite_node_ref_data composite_node_ref_data_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT composite_node_ref_data_pkey PRIMARY KEY (branch_node_id);


--
-- TOC entry 3340 (class 2606 OID 420210)
-- Name: composite_nodeable composite_nodeable_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_nodeable
    ADD CONSTRAINT composite_nodeable_pkey PRIMARY KEY (id);


--
-- TOC entry 3344 (class 2606 OID 420215)
-- Name: compositeactivityextraction compositeactivityextraction_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.compositeactivityextraction
    ADD CONSTRAINT compositeactivityextraction_pkey PRIMARY KEY (id);


--
-- TOC entry 3350 (class 2606 OID 420223)
-- Name: conditionprelevementsynthesisdatatype conditionprelevementsynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.conditionprelevementsynthesisdatatype
    ADD CONSTRAINT conditionprelevementsynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3352 (class 2606 OID 420231)
-- Name: conditionprelevementsynthesisvalue conditionprelevementsynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.conditionprelevementsynthesisvalue
    ADD CONSTRAINT conditionprelevementsynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3355 (class 2606 OID 420239)
-- Name: controle_coherence_cdc controle_coherence_cdc_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.controle_coherence_cdc
    ADD CONSTRAINT controle_coherence_cdc_pkey PRIMARY KEY (cdc_id);


--
-- TOC entry 3357 (class 2606 OID 420247)
-- Name: datatype datatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype
    ADD CONSTRAINT datatype_pkey PRIMARY KEY (dty_id);


--
-- TOC entry 3361 (class 2606 OID 420252)
-- Name: datatype_unite_variable_vdt datatype_unite_variable_vdt_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT datatype_unite_variable_vdt_pkey PRIMARY KEY (datatypevariableunite_id);


--
-- TOC entry 3368 (class 2606 OID 420260)
-- Name: datatype_variable_unite_glacpe_dvug datatype_variable_unite_glacpe_dvug_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_variable_unite_glacpe_dvug
    ADD CONSTRAINT datatype_variable_unite_glacpe_dvug_pkey PRIMARY KEY (vdt_id);


--
-- TOC entry 3372 (class 2606 OID 420268)
-- Name: dimension_dim dimension_dim_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.dimension_dim
    ADD CONSTRAINT dimension_dim_pkey PRIMARY KEY (dim_id);


--
-- TOC entry 3375 (class 2606 OID 420276)
-- Name: extractiontype extractiontype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT extractiontype_pkey PRIMARY KEY (id);


--
-- TOC entry 3384 (class 2606 OID 420284)
-- Name: filetype filetype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT filetype_pkey PRIMARY KEY (ft_id);


--
-- TOC entry 3392 (class 2606 OID 420289)
-- Name: genericdatatype genericdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.genericdatatype
    ADD CONSTRAINT genericdatatype_pkey PRIMARY KEY (generic_datatype_id);


--
-- TOC entry 3394 (class 2606 OID 420294)
-- Name: group_filecomp group_filecomp_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.group_filecomp
    ADD CONSTRAINT group_filecomp_pkey PRIMARY KEY (file_comp_id, group_id);


--
-- TOC entry 3396 (class 2606 OID 420299)
-- Name: group_utilisateur group_utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.group_utilisateur
    ADD CONSTRAINT group_utilisateur_pkey PRIMARY KEY (usr_id, group_id);


--
-- TOC entry 3404 (class 2606 OID 420318)
-- Name: groupe_gro groupe_gro_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe_gro
    ADD CONSTRAINT groupe_gro_pkey PRIMARY KEY (gro_id);


--
-- TOC entry 3398 (class 2606 OID 420307)
-- Name: groupe groupe_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT groupe_pkey PRIMARY KEY (id);


--
-- TOC entry 3408 (class 2606 OID 420326)
-- Name: hautefrequencesynthesisdatatype hautefrequencesynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.hautefrequencesynthesisdatatype
    ADD CONSTRAINT hautefrequencesynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3410 (class 2606 OID 420334)
-- Name: hautefrequencesynthesisvalue hautefrequencesynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.hautefrequencesynthesisvalue
    ADD CONSTRAINT hautefrequencesynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3413 (class 2606 OID 420339)
-- Name: hibernate_sequences hibernate_sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.hibernate_sequences
    ADD CONSTRAINT hibernate_sequences_pkey PRIMARY KEY (sequence_name);


--
-- TOC entry 3421 (class 2606 OID 420344)
-- Name: insertion_dataset_ids insertion_dataset_ids_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT insertion_dataset_ids_pkey PRIMARY KEY (ids_id);


--
-- TOC entry 3429 (class 2606 OID 420352)
-- Name: insertion_filecomp_ifcs insertion_filecomp_ifcs_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT insertion_filecomp_ifcs_pkey PRIMARY KEY (ifcs_id);


--
-- TOC entry 3435 (class 2606 OID 420360)
-- Name: insertion_version_file_ivf insertion_version_file_ivf_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT insertion_version_file_ivf_pkey PRIMARY KEY (ivf_id);


--
-- TOC entry 3443 (class 2606 OID 420368)
-- Name: localisation localisation_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.localisation
    ADD CONSTRAINT localisation_pkey PRIMARY KEY (id);


--
-- TOC entry 3447 (class 2606 OID 420376)
-- Name: mesure_chimie_mchimie mesure_chimie_mchimie_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chimie_mchimie
    ADD CONSTRAINT mesure_chimie_mchimie_pkey PRIMARY KEY (mchimie_id);


--
-- TOC entry 3454 (class 2606 OID 420384)
-- Name: mesure_chloro_mchloro mesure_chloro_mchloro_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chloro_mchloro
    ADD CONSTRAINT mesure_chloro_mchloro_pkey PRIMARY KEY (mchloro_id);


--
-- TOC entry 3460 (class 2606 OID 420392)
-- Name: mesure_condition_prelevement_mconditionprelevement mesure_condition_prelevement_mconditionprelevement_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_condition_prelevement_mconditionprelevement
    ADD CONSTRAINT mesure_condition_prelevement_mconditionprelevement_pkey PRIMARY KEY (mconditionprelevement_id);


--
-- TOC entry 3465 (class 2606 OID 420400)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux mesure_contenus_stomacaux_mcontenustomacaux_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT mesure_contenus_stomacaux_mcontenustomacaux_pkey PRIMARY KEY (mcontenustomacaux_id);


--
-- TOC entry 3470 (class 2606 OID 420408)
-- Name: mesure_haute_frequence_mhautefrequence mesure_haute_frequence_mhautefrequence_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_haute_frequence_mhautefrequence
    ADD CONSTRAINT mesure_haute_frequence_mhautefrequence_pkey PRIMARY KEY (mhautefrequence_id);


--
-- TOC entry 3475 (class 2606 OID 420416)
-- Name: mesure_phytoplancton_mphytoplancton mesure_phytoplancton_mphytoplancton_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_phytoplancton_mphytoplancton
    ADD CONSTRAINT mesure_phytoplancton_mphytoplancton_pkey PRIMARY KEY (mphytoplancton_id);


--
-- TOC entry 3481 (class 2606 OID 420424)
-- Name: mesure_production_primaire_mpp mesure_production_primaire_mpp_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_production_primaire_mpp
    ADD CONSTRAINT mesure_production_primaire_mpp_pkey PRIMARY KEY (mpp_id);


--
-- TOC entry 3487 (class 2606 OID 420432)
-- Name: mesure_sonde_multi_msondemulti mesure_sonde_multi_msondemulti_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_sonde_multi_msondemulti
    ADD CONSTRAINT mesure_sonde_multi_msondemulti_pkey PRIMARY KEY (msondemulti_id);


--
-- TOC entry 3498 (class 2606 OID 420440)
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- TOC entry 3500 (class 2606 OID 420451)
-- Name: outils_mesure_ome outils_mesure_ome_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.outils_mesure_ome
    ADD CONSTRAINT outils_mesure_ome_pkey PRIMARY KEY (ome_id);


--
-- TOC entry 3506 (class 2606 OID 420459)
-- Name: phytosynthesisdatatype phytosynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.phytosynthesisdatatype
    ADD CONSTRAINT phytosynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3508 (class 2606 OID 420467)
-- Name: phytosynthesisvalue phytosynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.phytosynthesisvalue
    ADD CONSTRAINT phytosynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3511 (class 2606 OID 420475)
-- Name: plateforme_pla plateforme_pla_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.plateforme_pla
    ADD CONSTRAINT plateforme_pla_pkey PRIMARY KEY (loc_id);


--
-- TOC entry 3513 (class 2606 OID 420483)
-- Name: ppsynthesisdatatype ppsynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.ppsynthesisdatatype
    ADD CONSTRAINT ppsynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3515 (class 2606 OID 420491)
-- Name: ppsynthesisvalue ppsynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.ppsynthesisvalue
    ADD CONSTRAINT ppsynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3518 (class 2606 OID 420499)
-- Name: projet_pro projet_pro_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_pro
    ADD CONSTRAINT projet_pro_pkey PRIMARY KEY (pro_id);


--
-- TOC entry 3520 (class 2606 OID 420510)
-- Name: projet_site_psi projet_site_psi_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_site_psi
    ADD CONSTRAINT projet_site_psi_pkey PRIMARY KEY (psi_id);


--
-- TOC entry 3524 (class 2606 OID 420522)
-- Name: propriete_taxon_prota propriete_taxon_prota_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.propriete_taxon_prota
    ADD CONSTRAINT propriete_taxon_prota_pkey PRIMARY KEY (prota_id);


--
-- TOC entry 3530 (class 2606 OID 420533)
-- Name: protocole_prot protocole_prot_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.protocole_prot
    ADD CONSTRAINT protocole_prot_pkey PRIMARY KEY (prot_id);


--
-- TOC entry 3536 (class 2606 OID 421021)
-- Name: realnode real_node_nk; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT real_node_nk UNIQUE (id_nodeable, id_parent_node);


--
-- TOC entry 3538 (class 2606 OID 421023)
-- Name: realnode real_node_path; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT real_node_path UNIQUE (path);


--
-- TOC entry 3544 (class 2606 OID 420538)
-- Name: realnode realnode_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT realnode_pkey PRIMARY KEY (id);


--
-- TOC entry 3546 (class 2606 OID 420543)
-- Name: refdata refdata_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.refdata
    ADD CONSTRAINT refdata_pkey PRIMARY KEY (id);


--
-- TOC entry 3549 (class 2606 OID 420551)
-- Name: requete requete_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.requete
    ADD CONSTRAINT requete_pkey PRIMARY KEY (id);


--
-- TOC entry 3557 (class 2606 OID 420567)
-- Name: rightrequest_glacpe_rrg rightrequest_glacpe_rrg_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.rightrequest_glacpe_rrg
    ADD CONSTRAINT rightrequest_glacpe_rrg_pkey PRIMARY KEY (id);


--
-- TOC entry 3552 (class 2606 OID 420559)
-- Name: rightrequest rightrequest_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.rightrequest
    ADD CONSTRAINT rightrequest_pkey PRIMARY KEY (id);


--
-- TOC entry 3563 (class 2606 OID 420575)
-- Name: sequence_chimie_schimie sequence_chimie_schimie_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chimie_schimie
    ADD CONSTRAINT sequence_chimie_schimie_pkey PRIMARY KEY (schimie_id);


--
-- TOC entry 3568 (class 2606 OID 420583)
-- Name: sequence_chloro_schloro sequence_chloro_schloro_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chloro_schloro
    ADD CONSTRAINT sequence_chloro_schloro_pkey PRIMARY KEY (schloro_id);


--
-- TOC entry 3573 (class 2606 OID 420591)
-- Name: sequence_contenus_stomacaux_scontenustomacaux sequence_contenus_stomacaux_scontenustomacaux_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_contenus_stomacaux_scontenustomacaux
    ADD CONSTRAINT sequence_contenus_stomacaux_scontenustomacaux_pkey PRIMARY KEY (scontenustomacaux_id);


--
-- TOC entry 3578 (class 2606 OID 420599)
-- Name: sequence_haute_frequence_shautefrequence sequence_haute_frequence_shautefrequence_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_haute_frequence_shautefrequence
    ADD CONSTRAINT sequence_haute_frequence_shautefrequence_pkey PRIMARY KEY (shautefrequence_id);


--
-- TOC entry 3583 (class 2606 OID 420607)
-- Name: sequence_phytoplancton_sphytoplancton sequence_phytoplancton_sphytoplancton_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_phytoplancton_sphytoplancton
    ADD CONSTRAINT sequence_phytoplancton_sphytoplancton_pkey PRIMARY KEY (sphytoplancton_id);


--
-- TOC entry 3588 (class 2606 OID 420615)
-- Name: sequence_production_primaire_spp sequence_production_primaire_spp_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_production_primaire_spp
    ADD CONSTRAINT sequence_production_primaire_spp_pkey PRIMARY KEY (spp_id);


--
-- TOC entry 3593 (class 2606 OID 420623)
-- Name: sequence_sonde_multi_ssondemulti sequence_sonde_multi_ssondemulti_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_sonde_multi_ssondemulti
    ADD CONSTRAINT sequence_sonde_multi_ssondemulti_pkey PRIMARY KEY (ssondemulti_id);


--
-- TOC entry 3599 (class 2606 OID 420639)
-- Name: site_glacpe_sit site_glacpe_sit_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.site_glacpe_sit
    ADD CONSTRAINT site_glacpe_sit_pkey PRIMARY KEY (id);


--
-- TOC entry 3597 (class 2606 OID 420631)
-- Name: site site_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT site_pkey PRIMARY KEY (site_id);


--
-- TOC entry 3601 (class 2606 OID 420647)
-- Name: sondemultisynthesisdatatype sondemultisynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sondemultisynthesisdatatype
    ADD CONSTRAINT sondemultisynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3603 (class 2606 OID 420655)
-- Name: sondemultisynthesisvalue sondemultisynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sondemultisynthesisvalue
    ADD CONSTRAINT sondemultisynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3609 (class 2606 OID 420663)
-- Name: sous_sequence_chimie_sschimie sous_sequence_chimie_sschimie_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chimie_sschimie
    ADD CONSTRAINT sous_sequence_chimie_sschimie_pkey PRIMARY KEY (sschimie_id);


--
-- TOC entry 3615 (class 2606 OID 420671)
-- Name: sous_sequence_chloro_sschloro sous_sequence_chloro_sschloro_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chloro_sschloro
    ADD CONSTRAINT sous_sequence_chloro_sschloro_pkey PRIMARY KEY (sschloro_id);


--
-- TOC entry 3621 (class 2606 OID 420679)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux sous_sequence_contenus_stomacaux_sscontenustomacaux_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux
    ADD CONSTRAINT sous_sequence_contenus_stomacaux_sscontenustomacaux_pkey PRIMARY KEY (sscontenustomacaux_id);


--
-- TOC entry 3628 (class 2606 OID 420687)
-- Name: sous_sequence_haute_frequence_sshautefrequence sous_sequence_haute_frequence_sshautefrequence_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence
    ADD CONSTRAINT sous_sequence_haute_frequence_sshautefrequence_pkey PRIMARY KEY (sshautefrequence_id);


--
-- TOC entry 3633 (class 2606 OID 420695)
-- Name: sous_sequence_phytoplancton_ssphytoplancton sous_sequence_phytoplancton_ssphytoplancton_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton
    ADD CONSTRAINT sous_sequence_phytoplancton_ssphytoplancton_pkey PRIMARY KEY (ssphytoplancton_id);


--
-- TOC entry 3639 (class 2606 OID 420703)
-- Name: sous_sequence_production_primaire_sspp sous_sequence_production_primaire_sspp_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_production_primaire_sspp
    ADD CONSTRAINT sous_sequence_production_primaire_sspp_pkey PRIMARY KEY (sspp_id);


--
-- TOC entry 3645 (class 2606 OID 420711)
-- Name: sous_sequence_sonde_multi_sssondemulti sous_sequence_sonde_multi_sssondemulti_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_sonde_multi_sssondemulti
    ADD CONSTRAINT sous_sequence_sonde_multi_sssondemulti_pkey PRIMARY KEY (sssondemulti_id);


--
-- TOC entry 3650 (class 2606 OID 420722)
-- Name: taxon_niveau_taxn taxon_niveau_taxn_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_niveau_taxn
    ADD CONSTRAINT taxon_niveau_taxn_pkey PRIMARY KEY (taxn_id);


--
-- TOC entry 3656 (class 2606 OID 420730)
-- Name: taxon_propriete_taxon_taprota taxon_propriete_taxon_taprota_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_propriete_taxon_taprota
    ADD CONSTRAINT taxon_propriete_taxon_taprota_pkey PRIMARY KEY (taprota_id);


--
-- TOC entry 3660 (class 2606 OID 420741)
-- Name: taxon_tax taxon_tax_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax
    ADD CONSTRAINT taxon_tax_pkey PRIMARY KEY (tax_id);


--
-- TOC entry 3668 (class 2606 OID 420749)
-- Name: theme theme_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.theme
    ADD CONSTRAINT theme_pkey PRIMARY KEY (id);


--
-- TOC entry 3670 (class 2606 OID 420760)
-- Name: type_outils_mesure_tome type_outils_mesure_tome_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_outils_mesure_tome
    ADD CONSTRAINT type_outils_mesure_tome_pkey PRIMARY KEY (tome_id);


--
-- TOC entry 3676 (class 2606 OID 420768)
-- Name: type_plateforme_tpla type_plateforme_tpla_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_plateforme_tpla
    ADD CONSTRAINT type_plateforme_tpla_pkey PRIMARY KEY (tpla_id);


--
-- TOC entry 3678 (class 2606 OID 420776)
-- Name: type_site_tsit type_site_tsit_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_site_tsit
    ADD CONSTRAINT type_site_tsit_pkey PRIMARY KEY (tsit_id);


--
-- TOC entry 3686 (class 2606 OID 421791)
-- Name: utilisateur uk18vwp4resqussqmlpqnymfqxk; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT uk18vwp4resqussqmlpqnymfqxk UNIQUE (login);


--
-- TOC entry 3648 (class 2606 OID 421083)
-- Name: sous_sequence_sonde_multi_sssondemulti uk1xsuvrsx2dcm2wp81jh8ndwxd; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_sonde_multi_sssondemulti
    ADD CONSTRAINT uk1xsuvrsx2dcm2wp81jh8ndwxd UNIQUE (ssondemulti_id, loc_id, ome_id);


--
-- TOC entry 3721 (class 2606 OID 421124)
-- Name: valeur_mesure_haute_frequence_vmhautefrequence uk2nym2xi69xr7cimiq4q9flmig; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_haute_frequence_vmhautefrequence
    ADD CONSTRAINT uk2nym2xi69xr7cimiq4q9flmig UNIQUE (id, mhautefrequence_id);


--
-- TOC entry 3611 (class 2606 OID 421058)
-- Name: sous_sequence_chimie_sschimie uk3qwfv32bnq5fxnya29j2l148v; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chimie_sschimie
    ADD CONSTRAINT uk3qwfv32bnq5fxnya29j2l148v UNIQUE (schimie_id, loc_id, ome_id);


--
-- TOC entry 3423 (class 2606 OID 420950)
-- Name: insertion_dataset_ids uk4enu5sb3qsya70slf6h50y524; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT uk4enu5sb3qsya70slf6h50y524 UNIQUE (id, ids_date_debut_periode, ids_date_fin_periode);


--
-- TOC entry 3580 (class 2606 OID 421043)
-- Name: sequence_haute_frequence_shautefrequence uk4hwxn5fvgx1y63rye9ab5eqiy; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_haute_frequence_shautefrequence
    ADD CONSTRAINT uk4hwxn5fvgx1y63rye9ab5eqiy UNIQUE (ivf_id, date_prelevement);


--
-- TOC entry 3715 (class 2606 OID 421121)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux uk4nftirqmbb9g520g1752te3b7; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux
    ADD CONSTRAINT uk4nftirqmbb9g520g1752te3b7 UNIQUE (proie, mcontenustomacaux_id);


--
-- TOC entry 3585 (class 2606 OID 421046)
-- Name: sequence_phytoplancton_sphytoplancton uk569v95ogdtrictcl5tueeymup; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_phytoplancton_sphytoplancton
    ADD CONSTRAINT uk569v95ogdtrictcl5tueeymup UNIQUE (ivf_id, date_prelevement);


--
-- TOC entry 3479 (class 2606 OID 420986)
-- Name: mesure_phytoplancton_mphytoplancton uk69tdy3yclqrakd484c7rnf9ym; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_phytoplancton_mphytoplancton
    ADD CONSTRAINT uk69tdy3yclqrakd484c7rnf9ym UNIQUE (ssphytoplancton_id, tax_id);


--
-- TOC entry 3570 (class 2606 OID 421037)
-- Name: sequence_chloro_schloro uk6cpm0j0ueu1kf9jdia9gj119v; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chloro_schloro
    ADD CONSTRAINT uk6cpm0j0ueu1kf9jdia9gj119v UNIQUE (ivf_id, date);


--
-- TOC entry 3406 (class 2606 OID 420940)
-- Name: groupe_gro uk6ehxmau5it6oiqji9o1igwxp9; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe_gro
    ADD CONSTRAINT uk6ehxmau5it6oiqji9o1igwxp9 UNIQUE (code, gro_gro_id);


--
-- TOC entry 3697 (class 2606 OID 421109)
-- Name: valeur_condition_prelevement_vmconditionprelevement uk6ltt3tuww5v3y8igx021i206w; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_condition_prelevement_vmconditionprelevement
    ADD CONSTRAINT uk6ltt3tuww5v3y8igx021i206w UNIQUE (id, mconditionprelevement_id);


--
-- TOC entry 3485 (class 2606 OID 420990)
-- Name: mesure_production_primaire_mpp uk7nfersnislc5cd1qqkbcd1te7; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_production_primaire_mpp
    ADD CONSTRAINT uk7nfersnislc5cd1qqkbcd1te7 UNIQUE (sspp_id, profondeur);


--
-- TOC entry 3630 (class 2606 OID 421071)
-- Name: sous_sequence_haute_frequence_sshautefrequence uk7w0f77f4f5frge7ha6r3lfrdy; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence
    ADD CONSTRAINT uk7w0f77f4f5frge7ha6r3lfrdy UNIQUE (shautefrequence_id, loc_id, ome_id, dim_id);


--
-- TOC entry 3400 (class 2606 OID 420938)
-- Name: groupe uk8da4mqyywnqafbgiti7bepn09; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT uk8da4mqyywnqafbgiti7bepn09 UNIQUE (group_name, group_which_tree);


--
-- TOC entry 3565 (class 2606 OID 421034)
-- Name: sequence_chimie_schimie uk9543vb1us0c9359upvbw1cs1o; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chimie_schimie
    ADD CONSTRAINT uk9543vb1us0c9359upvbw1cs1o UNIQUE (ivf_id, date_prelevement);


--
-- TOC entry 3735 (class 2606 OID 421133)
-- Name: valeur_mesure_sonde_multi_vmsondemulti uk95vqlhp1chs7n150qlpbwq561; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_sonde_multi_vmsondemulti
    ADD CONSTRAINT uk95vqlhp1chs7n150qlpbwq561 UNIQUE (id, msondemulti_id);


--
-- TOC entry 3445 (class 2606 OID 420964)
-- Name: localisation uk9mfgxosxjn4tu45hdhe524oph; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.localisation
    ADD CONSTRAINT uk9mfgxosxjn4tu45hdhe524oph UNIQUE (localization, entite, colonne, defaultstring);


--
-- TOC entry 3688 (class 2606 OID 421105)
-- Name: utilisateur uk_18vwp4resqussqmlpqnymfqxk; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT uk_18vwp4resqussqmlpqnymfqxk UNIQUE (login);


--
-- TOC entry 3359 (class 2606 OID 420917)
-- Name: datatype uk_1wbrux28nejdwyxvk8vtgbhky; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype
    ADD CONSTRAINT uk_1wbrux28nejdwyxvk8vtgbhky UNIQUE (name);


--
-- TOC entry 3672 (class 2606 OID 421097)
-- Name: type_outils_mesure_tome uk_392s2m67wcyqh1ub96etkbiqf; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_outils_mesure_tome
    ADD CONSTRAINT uk_392s2m67wcyqh1ub96etkbiqf UNIQUE (code);


--
-- TOC entry 3555 (class 2606 OID 421028)
-- Name: rightrequest uk_6k6t90k2feqiwir28a670fca8; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.rightrequest
    ADD CONSTRAINT uk_6k6t90k2feqiwir28a670fca8 UNIQUE (usr_id);


--
-- TOC entry 3662 (class 2606 OID 421095)
-- Name: taxon_tax uk_6mtuygyuugxgcimgk0isi7qh9; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax
    ADD CONSTRAINT uk_6mtuygyuugxgcimgk0isi7qh9 UNIQUE (nomlatin);


--
-- TOC entry 3560 (class 2606 OID 421031)
-- Name: rightrequest_glacpe_rrg uk_6v8pl7md8n8no0d9qs4xaoypq; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.rightrequest_glacpe_rrg
    ADD CONSTRAINT uk_6v8pl7md8n8no0d9qs4xaoypq UNIQUE (usr_id);


--
-- TOC entry 3526 (class 2606 OID 421011)
-- Name: propriete_taxon_prota uk_8k3nuxrj7q4cwj333kq7dpf83; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.propriete_taxon_prota
    ADD CONSTRAINT uk_8k3nuxrj7q4cwj333kq7dpf83 UNIQUE (nom);


--
-- TOC entry 3402 (class 2606 OID 420936)
-- Name: groupe uk_9n56mb0ey2u4tsas11vovq0pa; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT uk_9n56mb0ey2u4tsas11vovq0pa UNIQUE (group_name, group_type, group_which_tree);


--
-- TOC entry 3502 (class 2606 OID 421001)
-- Name: outils_mesure_ome uk_9pecahm9jibwiy1iwa13nfmn1; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.outils_mesure_ome
    ADD CONSTRAINT uk_9pecahm9jibwiy1iwa13nfmn1 UNIQUE (code);


--
-- TOC entry 3674 (class 2606 OID 421099)
-- Name: type_outils_mesure_tome uk_b9xr52fbcmf488hmrwgg7i3je; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_outils_mesure_tome
    ADD CONSTRAINT uk_b9xr52fbcmf488hmrwgg7i3je UNIQUE (nom);


--
-- TOC entry 3680 (class 2606 OID 421789)
-- Name: unite uk_c8cbymjvf78cmtcwtesp1h27e; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.unite
    ADD CONSTRAINT uk_c8cbymjvf78cmtcwtesp1h27e UNIQUE (code);


--
-- TOC entry 3532 (class 2606 OID 421013)
-- Name: protocole_prot uk_d6s6gkh4jqm4j97xg2la0inns; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.protocole_prot
    ADD CONSTRAINT uk_d6s6gkh4jqm4j97xg2la0inns UNIQUE (code);


--
-- TOC entry 3652 (class 2606 OID 421085)
-- Name: taxon_niveau_taxn uk_dxb6wpbwy40ov6sh1xs24gtg1; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_niveau_taxn
    ADD CONSTRAINT uk_dxb6wpbwy40ov6sh1xs24gtg1 UNIQUE (code);


--
-- TOC entry 3377 (class 2606 OID 421783)
-- Name: extractiontype uk_ek72kldu8j3joij9ou7e3tk88; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT uk_ek72kldu8j3joij9ou7e3tk88 UNIQUE (code);


--
-- TOC entry 3386 (class 2606 OID 420934)
-- Name: filetype uk_gonw2ifoyhnht1949bpyitog7; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT uk_gonw2ifoyhnht1949bpyitog7 UNIQUE (name);


--
-- TOC entry 3504 (class 2606 OID 421003)
-- Name: outils_mesure_ome uk_jcnvbb4d7ncm9y09njt30j0e; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.outils_mesure_ome
    ADD CONSTRAINT uk_jcnvbb4d7ncm9y09njt30j0e UNIQUE (nom);


--
-- TOC entry 3682 (class 2606 OID 421101)
-- Name: unite uk_jslkegk7riv9va6pq857n0j5l; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.unite
    ADD CONSTRAINT uk_jslkegk7riv9va6pq857n0j5l UNIQUE (code);


--
-- TOC entry 3431 (class 2606 OID 420956)
-- Name: insertion_filecomp_ifcs uk_jxiqp6e8nya0vskb87hbp29b0; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT uk_jxiqp6e8nya0vskb87hbp29b0 UNIQUE (ifcs_code);


--
-- TOC entry 3379 (class 2606 OID 420929)
-- Name: extractiontype uk_mky7ew3tmga5k29xvlwdhsofl; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT uk_mky7ew3tmga5k29xvlwdhsofl UNIQUE (name);


--
-- TOC entry 3654 (class 2606 OID 421087)
-- Name: taxon_niveau_taxn uk_muakemyvont39clubhsvyrlk3; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_niveau_taxn
    ADD CONSTRAINT uk_muakemyvont39clubhsvyrlk3 UNIQUE (nom);


--
-- TOC entry 3388 (class 2606 OID 420932)
-- Name: filetype uk_nbam0dixwdtnm4hwfu9wgvpbh; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT uk_nbam0dixwdtnm4hwfu9wgvpbh UNIQUE (code);


--
-- TOC entry 3528 (class 2606 OID 421009)
-- Name: propriete_taxon_prota uk_of0yicnpquxbylsg6d6pm8l1; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.propriete_taxon_prota
    ADD CONSTRAINT uk_of0yicnpquxbylsg6d6pm8l1 UNIQUE (prota_code);


--
-- TOC entry 3381 (class 2606 OID 420927)
-- Name: extractiontype uk_pag81d4u304wrfktrcdohi97l; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT uk_pag81d4u304wrfktrcdohi97l UNIQUE (code);


--
-- TOC entry 3664 (class 2606 OID 421093)
-- Name: taxon_tax uk_puk1gk863njloscd0kaxholx3; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax
    ADD CONSTRAINT uk_puk1gk863njloscd0kaxholx3 UNIQUE (code);


--
-- TOC entry 3750 (class 2606 OID 421137)
-- Name: variable_norme_vnor uk_qeiy6km0x9p7leqe93x8i352x; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_norme_vnor
    ADD CONSTRAINT uk_qeiy6km0x9p7leqe93x8i352x UNIQUE (code);


--
-- TOC entry 3534 (class 2606 OID 421015)
-- Name: protocole_prot uk_r5f5d57s5xvigrfv3wi4r604d; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.protocole_prot
    ADD CONSTRAINT uk_r5f5d57s5xvigrfv3wi4r604d UNIQUE (nom);


--
-- TOC entry 3690 (class 2606 OID 421103)
-- Name: utilisateur uk_rma38wvnqfaf66vvmi57c71lo; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT uk_rma38wvnqfaf66vvmi57c71lo UNIQUE (email);


--
-- TOC entry 3752 (class 2606 OID 421139)
-- Name: variable_norme_vnor uk_s85av5cf0ige39m8l9y6btp6i; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_norme_vnor
    ADD CONSTRAINT uk_s85av5cf0ige39m8l9y6btp6i UNIQUE (nom);


--
-- TOC entry 3390 (class 2606 OID 421785)
-- Name: filetype uk_t15pimato2f2f9iroqg8slv74; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT uk_t15pimato2f2f9iroqg8slv74 UNIQUE (code);


--
-- TOC entry 3366 (class 2606 OID 420922)
-- Name: datatype_unite_variable_vdt ukaj5f707xngjog30hslcut7iej; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT ukaj5f707xngjog30hslcut7iej UNIQUE (var_id, dty_id, uni_id);


--
-- TOC entry 3473 (class 2606 OID 420982)
-- Name: mesure_haute_frequence_mhautefrequence ukaqtx6gwn8lphl1myajso1n2ai; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_haute_frequence_mhautefrequence
    ADD CONSTRAINT ukaqtx6gwn8lphl1myajso1n2ai UNIQUE (sshautefrequence_id);


--
-- TOC entry 3642 (class 2606 OID 421079)
-- Name: sous_sequence_production_primaire_sspp ukb889s9v83fcspab7ni4l6158w; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_production_primaire_sspp
    ADD CONSTRAINT ukb889s9v83fcspab7ni4l6158w UNIQUE (spp_id, loc_id);


--
-- TOC entry 3726 (class 2606 OID 421127)
-- Name: valeur_mesure_phytoplancton_vmphytoplancton ukbw0yfha61unaak48ayworolqo; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_phytoplancton_vmphytoplancton
    ADD CONSTRAINT ukbw0yfha61unaak48ayworolqo UNIQUE (id, mphytoplancton_id);


--
-- TOC entry 3595 (class 2606 OID 421052)
-- Name: sequence_sonde_multi_ssondemulti ukbyh0qytj9x21qewjc97qrfl9x; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_sonde_multi_ssondemulti
    ADD CONSTRAINT ukbyh0qytj9x21qewjc97qrfl9x UNIQUE (ivf_id, date_prelevement);


--
-- TOC entry 3457 (class 2606 OID 420972)
-- Name: mesure_chloro_mchloro ukcy0ro3davp5svn082m26u21m0; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chloro_mchloro
    ADD CONSTRAINT ukcy0ro3davp5svn082m26u21m0 UNIQUE (sschloro_id, profondeur_max, profondeur_min);


--
-- TOC entry 3468 (class 2606 OID 420979)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux ukds36fjkrlo4qamspnufbyixyh; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT ukds36fjkrlo4qamspnufbyixyh UNIQUE (sscontenustomacaux_id, numero_poisson);


--
-- TOC entry 3370 (class 2606 OID 420924)
-- Name: datatype_variable_unite_glacpe_dvug ukeocwv4b215rw0pmut76cqymwm; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_variable_unite_glacpe_dvug
    ADD CONSTRAINT ukeocwv4b215rw0pmut76cqymwm UNIQUE (var_id, dty_id, uni_id);


--
-- TOC entry 3491 (class 2606 OID 420994)
-- Name: mesure_sonde_multi_msondemulti ukfkor77jmo0fr12r3leq7wswvr; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_sonde_multi_msondemulti
    ADD CONSTRAINT ukfkor77jmo0fr12r3leq7wswvr UNIQUE (sssondemulti_id, profondeur);


--
-- TOC entry 3708 (class 2606 OID 421116)
-- Name: valeur_mesure_chloro_vmchloro ukh5puhjjq6248madht3jan8pix; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chloro_vmchloro
    ADD CONSTRAINT ukh5puhjjq6248madht3jan8pix UNIQUE (id, mchloro_id);


--
-- TOC entry 3575 (class 2606 OID 421040)
-- Name: sequence_contenus_stomacaux_scontenustomacaux ukiumqh0sap4fbx07pckou3lutg; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_contenus_stomacaux_scontenustomacaux
    ADD CONSTRAINT ukiumqh0sap4fbx07pckou3lutg UNIQUE (ivf_id, date_prelevement);


--
-- TOC entry 3623 (class 2606 OID 421066)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux ukjqqjh1cid1kskbixa8culrhqu; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux
    ADD CONSTRAINT ukjqqjh1cid1kskbixa8culrhqu UNIQUE (scontenustomacaux_id, loc_id, espece);


--
-- TOC entry 3433 (class 2606 OID 421787)
-- Name: insertion_filecomp_ifcs ukjxiqp6e8nya0vskb87hbp29b0; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT ukjxiqp6e8nya0vskb87hbp29b0 UNIQUE (ifcs_code);


--
-- TOC entry 3666 (class 2606 OID 421091)
-- Name: taxon_tax ukkpcjvghutk9hwv3wv9kgadm1e; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax
    ADD CONSTRAINT ukkpcjvghutk9hwv3wv9kgadm1e UNIQUE (nomlatin, taxonparent_id);


--
-- TOC entry 3703 (class 2606 OID 421113)
-- Name: valeur_mesure_chimie_vmchimie uko1lvyusrqf47vrmv1ujy4264; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chimie_vmchimie
    ADD CONSTRAINT uko1lvyusrqf47vrmv1ujy4264 UNIQUE (id, mchimie_id);


--
-- TOC entry 3463 (class 2606 OID 420976)
-- Name: mesure_condition_prelevement_mconditionprelevement uko54hdmyqe60ia18bskstcmay8; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_condition_prelevement_mconditionprelevement
    ADD CONSTRAINT uko54hdmyqe60ia18bskstcmay8 UNIQUE (loc_id, date_prelevement, ivf_id);


--
-- TOC entry 3452 (class 2606 OID 420969)
-- Name: mesure_chimie_mchimie ukoklhakxcynd8ukwywjg7fmcfg; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chimie_mchimie
    ADD CONSTRAINT ukoklhakxcynd8ukwywjg7fmcfg UNIQUE (sschimie_id, profondeur_min);


--
-- TOC entry 3348 (class 2606 OID 420914)
-- Name: compositeactivityextraction ukorelf73ugljgad242rstffg11; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.compositeactivityextraction
    ADD CONSTRAINT ukorelf73ugljgad242rstffg11 UNIQUE (idnode, login);


--
-- TOC entry 3590 (class 2606 OID 421049)
-- Name: sequence_production_primaire_spp ukpiawgfkcwtnyo9you2ju77anw; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_production_primaire_spp
    ADD CONSTRAINT ukpiawgfkcwtnyo9you2ju77anw UNIQUE (ivf_id, date);


--
-- TOC entry 3730 (class 2606 OID 421130)
-- Name: valeur_mesure_production_primaire_vmpp ukpkosmseljctyl0gwnrwl8fjur; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_production_primaire_vmpp
    ADD CONSTRAINT ukpkosmseljctyl0gwnrwl8fjur UNIQUE (id, mpp_id);


--
-- TOC entry 3742 (class 2606 OID 421135)
-- Name: valeur_qualitative ukqnml805ln93qvtpclqe61ogdg; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_qualitative
    ADD CONSTRAINT ukqnml805ln93qvtpclqe61ogdg UNIQUE (valeur, code, typelist);


--
-- TOC entry 3692 (class 2606 OID 421793)
-- Name: utilisateur ukrma38wvnqfaf66vvmi57c71lo; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT ukrma38wvnqfaf66vvmi57c71lo UNIQUE (email);


--
-- TOC entry 3658 (class 2606 OID 421089)
-- Name: taxon_propriete_taxon_taprota ukrplsvhi67fc7sq696ka8ljryt; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_propriete_taxon_taprota
    ADD CONSTRAINT ukrplsvhi67fc7sq696ka8ljryt UNIQUE (tax_id, prota_id);


--
-- TOC entry 3636 (class 2606 OID 421075)
-- Name: sous_sequence_phytoplancton_ssphytoplancton uksdxeo0oahkm7l4wf47y8p366h; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton
    ADD CONSTRAINT uksdxeo0oahkm7l4wf47y8p366h UNIQUE (sphytoplancton_id, loc_id);


--
-- TOC entry 3522 (class 2606 OID 421007)
-- Name: projet_site_psi uksqvvr5uxm2qllsbm5kp4up4v4; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_site_psi
    ADD CONSTRAINT uksqvvr5uxm2qllsbm5kp4up4v4 UNIQUE (id, pro_id);


--
-- TOC entry 3617 (class 2606 OID 421062)
-- Name: sous_sequence_chloro_sschloro uksrkj4ri262cq5lp7r5wog8wmh; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chloro_sschloro
    ADD CONSTRAINT uksrkj4ri262cq5lp7r5wog8wmh UNIQUE (schloro_id, loc_id);


--
-- TOC entry 3438 (class 2606 OID 420959)
-- Name: insertion_version_file_ivf ukswl3lqlyfh6e48ft2j5o0a5ej; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT ukswl3lqlyfh6e48ft2j5o0a5ej UNIQUE (ivf_version_number, ids_id);


--
-- TOC entry 3342 (class 2606 OID 420910)
-- Name: composite_nodeable unique_code_dbtype_uk; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_nodeable
    ADD CONSTRAINT unique_code_dbtype_uk UNIQUE (dtype, code);


--
-- TOC entry 3684 (class 2606 OID 420784)
-- Name: unite unite_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.unite
    ADD CONSTRAINT unite_pkey PRIMARY KEY (uni_id);


--
-- TOC entry 3694 (class 2606 OID 420792)
-- Name: utilisateur utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- TOC entry 3699 (class 2606 OID 420800)
-- Name: valeur_condition_prelevement_vmconditionprelevement valeur_condition_prelevement_vmconditionprelevement_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_condition_prelevement_vmconditionprelevement
    ADD CONSTRAINT valeur_condition_prelevement_vmconditionprelevement_pkey PRIMARY KEY (vconditionprelevement_id);


--
-- TOC entry 3705 (class 2606 OID 420811)
-- Name: valeur_mesure_chimie_vmchimie valeur_mesure_chimie_vmchimie_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chimie_vmchimie
    ADD CONSTRAINT valeur_mesure_chimie_vmchimie_pkey PRIMARY KEY (vmchimie_id);


--
-- TOC entry 3710 (class 2606 OID 420819)
-- Name: valeur_mesure_chloro_vmchloro valeur_mesure_chloro_vmchloro_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chloro_vmchloro
    ADD CONSTRAINT valeur_mesure_chloro_vmchloro_pkey PRIMARY KEY (vmchloro_id);


--
-- TOC entry 3717 (class 2606 OID 420827)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux valeur_mesure_contenus_stomacaux_vmcontenustomacaux_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux
    ADD CONSTRAINT valeur_mesure_contenus_stomacaux_vmcontenustomacaux_pkey PRIMARY KEY (vmcontenustomacaux_id);


--
-- TOC entry 3723 (class 2606 OID 420835)
-- Name: valeur_mesure_haute_frequence_vmhautefrequence valeur_mesure_haute_frequence_vmhautefrequence_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_haute_frequence_vmhautefrequence
    ADD CONSTRAINT valeur_mesure_haute_frequence_vmhautefrequence_pkey PRIMARY KEY (vmhautefrequence_id);


--
-- TOC entry 3728 (class 2606 OID 420843)
-- Name: valeur_mesure_phytoplancton_vmphytoplancton valeur_mesure_phytoplancton_vmphytoplancton_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_phytoplancton_vmphytoplancton
    ADD CONSTRAINT valeur_mesure_phytoplancton_vmphytoplancton_pkey PRIMARY KEY (vmphytoplancton_id);


--
-- TOC entry 3732 (class 2606 OID 420851)
-- Name: valeur_mesure_production_primaire_vmpp valeur_mesure_production_primaire_vmpp_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_production_primaire_vmpp
    ADD CONSTRAINT valeur_mesure_production_primaire_vmpp_pkey PRIMARY KEY (vmpp_id);


--
-- TOC entry 3737 (class 2606 OID 420859)
-- Name: valeur_mesure_sonde_multi_vmsondemulti valeur_mesure_sonde_multi_vmsondemulti_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_sonde_multi_vmsondemulti
    ADD CONSTRAINT valeur_mesure_sonde_multi_vmsondemulti_pkey PRIMARY KEY (vmsondemulti_id);


--
-- TOC entry 3740 (class 2606 OID 420870)
-- Name: valeur_propriete_taxon_vprota valeur_propriete_taxon_vprota_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_propriete_taxon_vprota
    ADD CONSTRAINT valeur_propriete_taxon_vprota_pkey PRIMARY KEY (vprota_id);


--
-- TOC entry 3744 (class 2606 OID 420878)
-- Name: valeur_qualitative valeur_qualitative_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_qualitative
    ADD CONSTRAINT valeur_qualitative_pkey PRIMARY KEY (vq_id);


--
-- TOC entry 3748 (class 2606 OID 420895)
-- Name: variable_glacpe_varg variable_glacpe_varg_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_glacpe_varg
    ADD CONSTRAINT variable_glacpe_varg_pkey PRIMARY KEY (var_id);


--
-- TOC entry 3754 (class 2606 OID 420906)
-- Name: variable_norme_vnor variable_norme_vnor_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_norme_vnor
    ADD CONSTRAINT variable_norme_vnor_pkey PRIMARY KEY (vnor_id);


--
-- TOC entry 3746 (class 2606 OID 420887)
-- Name: variable variable_pkey; Type: CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable
    ADD CONSTRAINT variable_pkey PRIMARY KEY (var_id);


--
-- TOC entry 3329 (class 1259 OID 420907)
-- Name: chimiesynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX chimiesynthesisvalue_site_variable_idx ON public.chimiesynthesisvalue USING btree (site, variable);


--
-- TOC entry 3334 (class 1259 OID 420908)
-- Name: chlorosynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX chlorosynthesisvalue_site_variable_idx ON public.chlorosynthesisvalue USING btree (site, variable);


--
-- TOC entry 3353 (class 1259 OID 420915)
-- Name: conditionprelevementsynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX conditionprelevementsynthesisvalue_site_variable_idx ON public.conditionprelevementsynthesisvalue USING btree (site, variable);


--
-- TOC entry 3624 (class 1259 OID 421068)
-- Name: dim_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX dim_id ON public.sous_sequence_haute_frequence_sshautefrequence USING btree (dim_id);


--
-- TOC entry 3362 (class 1259 OID 420920)
-- Name: dvu_dty_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX dvu_dty_idx ON public.datatype_unite_variable_vdt USING btree (dty_id);


--
-- TOC entry 3363 (class 1259 OID 420918)
-- Name: dvu_uni_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX dvu_uni_idx ON public.datatype_unite_variable_vdt USING btree (uni_id);


--
-- TOC entry 3364 (class 1259 OID 420919)
-- Name: dvu_var_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX dvu_var_idx ON public.datatype_unite_variable_vdt USING btree (var_id);


--
-- TOC entry 3373 (class 1259 OID 420925)
-- Name: extractiontype_code_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX extractiontype_code_idx ON public.extractiontype USING btree (code);


--
-- TOC entry 3424 (class 1259 OID 420951)
-- Name: file_code_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX file_code_idx ON public.insertion_filecomp_ifcs USING btree (ifcs_code);


--
-- TOC entry 3425 (class 1259 OID 420954)
-- Name: file_create_user_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX file_create_user_idx ON public.insertion_filecomp_ifcs USING btree (ifcs_create_user);


--
-- TOC entry 3426 (class 1259 OID 420953)
-- Name: file_modify_user_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX file_modify_user_idx ON public.insertion_filecomp_ifcs USING btree (ifcs_last_modify_user);


--
-- TOC entry 3427 (class 1259 OID 420952)
-- Name: file_type_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX file_type_idx ON public.insertion_filecomp_ifcs USING btree (ft_id);


--
-- TOC entry 3382 (class 1259 OID 420930)
-- Name: filetype_code_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX filetype_code_idx ON public.filetype USING btree (code);


--
-- TOC entry 3411 (class 1259 OID 420941)
-- Name: hautefrequencesynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX hautefrequencesynthesisvalue_site_variable_idx ON public.hautefrequencesynthesisvalue USING btree (site, variable);


--
-- TOC entry 3712 (class 1259 OID 421119)
-- Name: id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX id ON public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux USING btree (id);


--
-- TOC entry 3345 (class 1259 OID 420912)
-- Name: idnode_loginidx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX idnode_loginidx ON public.compositeactivityextraction USING btree (idnode, login);


--
-- TOC entry 3346 (class 1259 OID 420911)
-- Name: idnodeidx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX idnodeidx ON public.compositeactivityextraction USING btree (idnode);


--
-- TOC entry 3414 (class 1259 OID 420942)
-- Name: ids_createuser_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ids_createuser_idx ON public.insertion_dataset_ids USING btree (ids_create_user);


--
-- TOC entry 3415 (class 1259 OID 420944)
-- Name: ids_datedebut_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ids_datedebut_idx ON public.insertion_dataset_ids USING btree (ids_date_debut_periode);


--
-- TOC entry 3416 (class 1259 OID 420945)
-- Name: ids_datefin_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ids_datefin_idx ON public.insertion_dataset_ids USING btree (ids_date_fin_periode);


--
-- TOC entry 3417 (class 1259 OID 420946)
-- Name: ids_dtn_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ids_dtn_idx ON public.insertion_dataset_ids USING btree (id);


--
-- TOC entry 3418 (class 1259 OID 420943)
-- Name: ids_publish_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ids_publish_idx ON public.insertion_dataset_ids USING btree (ids_publish_user);


--
-- TOC entry 3419 (class 1259 OID 420947)
-- Name: ids_version_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ids_version_idx ON public.insertion_dataset_ids USING btree (ivf_id);


--
-- TOC entry 3561 (class 1259 OID 421032)
-- Name: ivf_chimie_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_chimie_ikey ON public.sequence_chimie_schimie USING btree (ivf_id);


--
-- TOC entry 3566 (class 1259 OID 421035)
-- Name: ivf_chloro_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_chloro_ikey ON public.sequence_chloro_schloro USING btree (ivf_id);


--
-- TOC entry 3458 (class 1259 OID 420974)
-- Name: ivf_condition_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_condition_ikey ON public.mesure_condition_prelevement_mconditionprelevement USING btree (ivf_id);


--
-- TOC entry 3571 (class 1259 OID 421038)
-- Name: ivf_contenus_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_contenus_ikey ON public.sequence_contenus_stomacaux_scontenustomacaux USING btree (ivf_id);


--
-- TOC entry 3576 (class 1259 OID 421041)
-- Name: ivf_hautefrequence_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_hautefrequence_ikey ON public.sequence_haute_frequence_shautefrequence USING btree (ivf_id);


--
-- TOC entry 3436 (class 1259 OID 420957)
-- Name: ivf_ids_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_ids_idx ON public.insertion_version_file_ivf USING btree (ids_id);


--
-- TOC entry 3581 (class 1259 OID 421044)
-- Name: ivf_phyto_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_phyto_ikey ON public.sequence_phytoplancton_sphytoplancton USING btree (ivf_id);


--
-- TOC entry 3586 (class 1259 OID 421047)
-- Name: ivf_pp_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_pp_ikey ON public.sequence_production_primaire_spp USING btree (ivf_id);


--
-- TOC entry 3591 (class 1259 OID 421050)
-- Name: ivf_sonde_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ivf_sonde_ikey ON public.sequence_sonde_multi_ssondemulti USING btree (ivf_id);


--
-- TOC entry 3439 (class 1259 OID 420961)
-- Name: loc_ent_col_def_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX loc_ent_col_def_idx ON public.localisation USING btree (entite, colonne, defaultstring);


--
-- TOC entry 3440 (class 1259 OID 420962)
-- Name: loc_loc_ent_col_def_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX loc_loc_ent_col_def_idx ON public.localisation USING btree (localization, entite, colonne, defaultstring);


--
-- TOC entry 3441 (class 1259 OID 420960)
-- Name: loc_loc_ent_col_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX loc_loc_ent_col_idx ON public.localisation USING btree (localization, entite, colonne);


--
-- TOC entry 3701 (class 1259 OID 421110)
-- Name: mchimie_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX mchimie_id ON public.valeur_mesure_chimie_vmchimie USING btree (mchimie_id);


--
-- TOC entry 3695 (class 1259 OID 421106)
-- Name: mconditionprelevement_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX mconditionprelevement_id ON public.valeur_condition_prelevement_vmconditionprelevement USING btree (mconditionprelevement_id);


--
-- TOC entry 3713 (class 1259 OID 421117)
-- Name: mcontenustomacaux_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX mcontenustomacaux_id ON public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux USING btree (mcontenustomacaux_id);


--
-- TOC entry 3719 (class 1259 OID 421122)
-- Name: mhautefrequence_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX mhautefrequence_id ON public.valeur_mesure_haute_frequence_vmhautefrequence USING btree (mhautefrequence_id);


--
-- TOC entry 3724 (class 1259 OID 421125)
-- Name: mphytoplancton_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX mphytoplancton_id ON public.valeur_mesure_phytoplancton_vmphytoplancton USING btree (mphytoplancton_id);


--
-- TOC entry 3492 (class 1259 OID 420996)
-- Name: not_archived_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX not_archived_idx ON public.notification USING btree (archived);


--
-- TOC entry 3493 (class 1259 OID 420995)
-- Name: not_date_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX not_date_idx ON public.notification USING btree (date);


--
-- TOC entry 3494 (class 1259 OID 420999)
-- Name: not_user_date_archived_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX not_user_date_archived_idx ON public.notification USING btree (utilisateur_id, date, archived);


--
-- TOC entry 3495 (class 1259 OID 420998)
-- Name: not_user_date_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX not_user_date_idx ON public.notification USING btree (utilisateur_id, date);


--
-- TOC entry 3496 (class 1259 OID 420997)
-- Name: not_user_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX not_user_idx ON public.notification USING btree (utilisateur_id);


--
-- TOC entry 3466 (class 1259 OID 420977)
-- Name: numero_poisson; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX numero_poisson ON public.mesure_contenus_stomacaux_mcontenustomacaux USING btree (numero_poisson);


--
-- TOC entry 3605 (class 1259 OID 421056)
-- Name: ome_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ome_id ON public.sous_sequence_chimie_sschimie USING btree (ome_id);


--
-- TOC entry 3509 (class 1259 OID 421004)
-- Name: phytosynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX phytosynthesisvalue_site_variable_idx ON public.phytosynthesisvalue USING btree (site, variable);


--
-- TOC entry 3606 (class 1259 OID 421055)
-- Name: plateforme_chimie_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_chimie_ikey ON public.sous_sequence_chimie_sschimie USING btree (loc_id);


--
-- TOC entry 3612 (class 1259 OID 421060)
-- Name: plateforme_chloro_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_chloro_ikey ON public.sous_sequence_chloro_sschloro USING btree (loc_id);


--
-- TOC entry 3461 (class 1259 OID 420973)
-- Name: plateforme_condition_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_condition_ikey ON public.mesure_condition_prelevement_mconditionprelevement USING btree (loc_id);


--
-- TOC entry 3618 (class 1259 OID 421063)
-- Name: plateforme_conditions_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_conditions_ikey ON public.sous_sequence_contenus_stomacaux_sscontenustomacaux USING btree (loc_id);


--
-- TOC entry 3625 (class 1259 OID 421067)
-- Name: plateforme_hautefrequence_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_hautefrequence_ikey ON public.sous_sequence_haute_frequence_sshautefrequence USING btree (loc_id);


--
-- TOC entry 3631 (class 1259 OID 421072)
-- Name: plateforme_phyto_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_phyto_ikey ON public.sous_sequence_phytoplancton_ssphytoplancton USING btree (loc_id);


--
-- TOC entry 3637 (class 1259 OID 421076)
-- Name: plateforme_pp_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_pp_ikey ON public.sous_sequence_production_primaire_sspp USING btree (loc_id);


--
-- TOC entry 3643 (class 1259 OID 421080)
-- Name: plateforme_sonde_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX plateforme_sonde_ikey ON public.sous_sequence_sonde_multi_sssondemulti USING btree (loc_id);


--
-- TOC entry 3516 (class 1259 OID 421005)
-- Name: ppsynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ppsynthesisvalue_site_variable_idx ON public.ppsynthesisvalue USING btree (site, variable);


--
-- TOC entry 3482 (class 1259 OID 420987)
-- Name: profondeur; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX profondeur ON public.mesure_production_primaire_mpp USING btree (profondeur);


--
-- TOC entry 3448 (class 1259 OID 420966)
-- Name: profondeur_max; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX profondeur_max ON public.mesure_chimie_mchimie USING btree (profondeur_max);


--
-- TOC entry 3449 (class 1259 OID 420965)
-- Name: profondeur_min; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX profondeur_min ON public.mesure_chimie_mchimie USING btree (profondeur_min);


--
-- TOC entry 3488 (class 1259 OID 420992)
-- Name: profondeur_sonde_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX profondeur_sonde_ikey ON public.mesure_sonde_multi_msondemulti USING btree (profondeur);


--
-- TOC entry 3539 (class 1259 OID 421017)
-- Name: realnode_ancestor_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX realnode_ancestor_idx ON public.realnode USING btree (id_ancestor);


--
-- TOC entry 3540 (class 1259 OID 421018)
-- Name: realnode_nodeable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX realnode_nodeable_idx ON public.realnode USING btree (id_nodeable);


--
-- TOC entry 3541 (class 1259 OID 421016)
-- Name: realnode_parent_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX realnode_parent_idx ON public.realnode USING btree (id_parent_node);


--
-- TOC entry 3542 (class 1259 OID 421019)
-- Name: realnode_path_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX realnode_path_idx ON public.realnode USING btree (path);


--
-- TOC entry 3547 (class 1259 OID 421024)
-- Name: requete_extractiontype_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX requete_extractiontype_idx ON public.requete USING btree (extractiontype_id);


--
-- TOC entry 3550 (class 1259 OID 421025)
-- Name: requete_user_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX requete_user_idx ON public.requete USING btree (utilisateur_id);


--
-- TOC entry 3558 (class 1259 OID 421029)
-- Name: rightrequest_glacpe_rrgrightrequest_utilisateur_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX rightrequest_glacpe_rrgrightrequest_utilisateur_idx ON public.rightrequest_glacpe_rrg USING btree (usr_id);


--
-- TOC entry 3553 (class 1259 OID 421026)
-- Name: rightrequest_utilisateur_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX rightrequest_utilisateur_idx ON public.rightrequest USING btree (usr_id);


--
-- TOC entry 3607 (class 1259 OID 421054)
-- Name: schimie_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX schimie_id ON public.sous_sequence_chimie_sschimie USING btree (schimie_id);


--
-- TOC entry 3613 (class 1259 OID 421059)
-- Name: schloro_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX schloro_id ON public.sous_sequence_chloro_sschloro USING btree (schloro_id);


--
-- TOC entry 3619 (class 1259 OID 421064)
-- Name: scontenustomacaux_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX scontenustomacaux_id ON public.sous_sequence_contenus_stomacaux_sscontenustomacaux USING btree (scontenustomacaux_id);


--
-- TOC entry 3626 (class 1259 OID 421069)
-- Name: shautefrequence_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX shautefrequence_id ON public.sous_sequence_haute_frequence_sshautefrequence USING btree (shautefrequence_id);


--
-- TOC entry 3604 (class 1259 OID 421053)
-- Name: sondemultisynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sondemultisynthesisvalue_site_variable_idx ON public.sondemultisynthesisvalue USING btree (site, variable);


--
-- TOC entry 3634 (class 1259 OID 421073)
-- Name: sphytoplancton_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sphytoplancton_id ON public.sous_sequence_phytoplancton_ssphytoplancton USING btree (sphytoplancton_id);


--
-- TOC entry 3640 (class 1259 OID 421077)
-- Name: spp_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX spp_id ON public.sous_sequence_production_primaire_sspp USING btree (spp_id);


--
-- TOC entry 3450 (class 1259 OID 420967)
-- Name: sschimie_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sschimie_id ON public.mesure_chimie_mchimie USING btree (sschimie_id);


--
-- TOC entry 3455 (class 1259 OID 420970)
-- Name: sschloro_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sschloro_id ON public.mesure_chloro_mchloro USING btree (sschloro_id);


--
-- TOC entry 3471 (class 1259 OID 420980)
-- Name: sshautefrequence_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sshautefrequence_id ON public.mesure_haute_frequence_mhautefrequence USING btree (sshautefrequence_id);


--
-- TOC entry 3646 (class 1259 OID 421081)
-- Name: ssondemulti_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ssondemulti_id ON public.sous_sequence_sonde_multi_sssondemulti USING btree (ssondemulti_id);


--
-- TOC entry 3476 (class 1259 OID 420984)
-- Name: ssphytoplancton_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX ssphytoplancton_id ON public.mesure_phytoplancton_mphytoplancton USING btree (ssphytoplancton_id);


--
-- TOC entry 3483 (class 1259 OID 420988)
-- Name: sspp_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sspp_id ON public.mesure_production_primaire_mpp USING btree (sspp_id);


--
-- TOC entry 3489 (class 1259 OID 420991)
-- Name: sssondemulti_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX sssondemulti_id ON public.mesure_sonde_multi_msondemulti USING btree (sssondemulti_id);


--
-- TOC entry 3477 (class 1259 OID 420983)
-- Name: tax_id; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX tax_id ON public.mesure_phytoplancton_mphytoplancton USING btree (tax_id);


--
-- TOC entry 3706 (class 1259 OID 421111)
-- Name: var_chimie_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX var_chimie_ikey ON public.valeur_mesure_chimie_vmchimie USING btree (id);


--
-- TOC entry 3711 (class 1259 OID 421114)
-- Name: var_chloro_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX var_chloro_ikey ON public.valeur_mesure_chloro_vmchloro USING btree (id);


--
-- TOC entry 3700 (class 1259 OID 421107)
-- Name: var_conditions_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX var_conditions_ikey ON public.valeur_condition_prelevement_vmconditionprelevement USING btree (id);


--
-- TOC entry 3733 (class 1259 OID 421128)
-- Name: var_pp_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX var_pp_ikey ON public.valeur_mesure_production_primaire_vmpp USING btree (id);


--
-- TOC entry 3738 (class 1259 OID 421131)
-- Name: var_sonde_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX var_sonde_ikey ON public.valeur_mesure_sonde_multi_vmsondemulti USING btree (id);


--
-- TOC entry 3718 (class 1259 OID 421118)
-- Name: vq_contenus_ikey; Type: INDEX; Schema: public; Owner: philippe
--

CREATE INDEX vq_contenus_ikey ON public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux USING btree (proie);


--
-- TOC entry 3867 (class 2606 OID 421702)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux fk16maofmomf3i3rd348ldklke2; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux
    ADD CONSTRAINT fk16maofmomf3i3rd348ldklke2 FOREIGN KEY (mcontenustomacaux_id) REFERENCES public.mesure_contenus_stomacaux_mcontenustomacaux(mcontenustomacaux_id);


--
-- TOC entry 3798 (class 2606 OID 421357)
-- Name: mesure_phytoplancton_mphytoplancton fk1uhuuojg7dmiqmkt6f1b4orlo; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_phytoplancton_mphytoplancton
    ADD CONSTRAINT fk1uhuuojg7dmiqmkt6f1b4orlo FOREIGN KEY (tax_id) REFERENCES public.taxon_tax(tax_id);


--
-- TOC entry 3767 (class 2606 OID 421202)
-- Name: datatype_unite_variable_vdt fk26beo8l0x2bg769r97eo99tud; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fk26beo8l0x2bg769r97eo99tud FOREIGN KEY (datatypevariableunite_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3786 (class 2606 OID 421297)
-- Name: insertion_version_file_ivf fk2fc45e4e8yiseqa7692qrfs48; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT fk2fc45e4e8yiseqa7692qrfs48 FOREIGN KEY (ivf_upload_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3876 (class 2606 OID 421747)
-- Name: valeur_mesure_sonde_multi_vmsondemulti fk2mni20b08uqryeh3w2njx24h4; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_sonde_multi_vmsondemulti
    ADD CONSTRAINT fk2mni20b08uqryeh3w2njx24h4 FOREIGN KEY (msondemulti_id) REFERENCES public.mesure_sonde_multi_msondemulti(msondemulti_id);


--
-- TOC entry 3762 (class 2606 OID 421177)
-- Name: controle_coherence_cdc fk2r5ikmcuyobd3672si51koq9p; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.controle_coherence_cdc
    ADD CONSTRAINT fk2r5ikmcuyobd3672si51koq9p FOREIGN KEY (id) REFERENCES public.site_glacpe_sit(id);


--
-- TOC entry 3804 (class 2606 OID 421387)
-- Name: plateforme_pla fk39mhgwn83pklme42iffvh83fx; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.plateforme_pla
    ADD CONSTRAINT fk39mhgwn83pklme42iffvh83fx FOREIGN KEY (id) REFERENCES public.site_glacpe_sit(id);


--
-- TOC entry 3808 (class 2606 OID 421407)
-- Name: projet_site_psi fk3gr4xyt3vllctu74tbfcemt0p; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_site_psi
    ADD CONSTRAINT fk3gr4xyt3vllctu74tbfcemt0p FOREIGN KEY (pro_id) REFERENCES public.projet_pro(pro_id);


--
-- TOC entry 3874 (class 2606 OID 421737)
-- Name: valeur_mesure_production_primaire_vmpp fk3ktlamde53iu24qqu9j7c7yes; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_production_primaire_vmpp
    ADD CONSTRAINT fk3ktlamde53iu24qqu9j7c7yes FOREIGN KEY (mpp_id) REFERENCES public.mesure_production_primaire_mpp(mpp_id);


--
-- TOC entry 3834 (class 2606 OID 421537)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux fk3npvt70ceg09vjyt2l2ro698k; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux
    ADD CONSTRAINT fk3npvt70ceg09vjyt2l2ro698k FOREIGN KEY (maille) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3809 (class 2606 OID 421412)
-- Name: projet_site_psi fk3od6o07uxxm0hwdg39813st9r; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_site_psi
    ADD CONSTRAINT fk3od6o07uxxm0hwdg39813st9r FOREIGN KEY (id) REFERENCES public.site_glacpe_sit(id);


--
-- TOC entry 3799 (class 2606 OID 421362)
-- Name: mesure_production_primaire_mpp fk410r63cctnoks81g5805xivao; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_production_primaire_mpp
    ADD CONSTRAINT fk410r63cctnoks81g5805xivao FOREIGN KEY (sspp_id) REFERENCES public.sous_sequence_production_primaire_sspp(sspp_id);


--
-- TOC entry 3805 (class 2606 OID 421392)
-- Name: plateforme_pla fk41m3mlvkpwch9pn733lk1b0rd; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.plateforme_pla
    ADD CONSTRAINT fk41m3mlvkpwch9pn733lk1b0rd FOREIGN KEY (tpla_id) REFERENCES public.type_plateforme_tpla(tpla_id);


--
-- TOC entry 3811 (class 2606 OID 421422)
-- Name: realnode fk4lbmw99i4s6gtyfr8ir8hqypn; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT fk4lbmw99i4s6gtyfr8ir8hqypn FOREIGN KEY (id_nodeable) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3770 (class 2606 OID 421217)
-- Name: datatype_variable_unite_glacpe_dvug fk4tpm7ndcl1nc070lnor9toda9; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_variable_unite_glacpe_dvug
    ADD CONSTRAINT fk4tpm7ndcl1nc070lnor9toda9 FOREIGN KEY (var_id) REFERENCES public.variable_glacpe_varg(var_id);


--
-- TOC entry 3854 (class 2606 OID 421637)
-- Name: taxon_tax fk5506v9mb3yijwkbdbhmg35h5a; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax
    ADD CONSTRAINT fk5506v9mb3yijwkbdbhmg35h5a FOREIGN KEY (taxn_id) REFERENCES public.taxon_niveau_taxn(taxn_id);


--
-- TOC entry 3848 (class 2606 OID 421607)
-- Name: sous_sequence_sonde_multi_sssondemulti fk5e89d1vww30ft0f60wm8pjdcf; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_sonde_multi_sssondemulti
    ADD CONSTRAINT fk5e89d1vww30ft0f60wm8pjdcf FOREIGN KEY (ome_id) REFERENCES public.outils_mesure_ome(ome_id);


--
-- TOC entry 3816 (class 2606 OID 421447)
-- Name: rightrequest fk6dka4v5ub5crbb1ddnbuws6dn; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.rightrequest
    ADD CONSTRAINT fk6dka4v5ub5crbb1ddnbuws6dn FOREIGN KEY (usr_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3818 (class 2606 OID 421457)
-- Name: sequence_chimie_schimie fk6fmnit8ebej6endgia4d1e16j; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chimie_schimie
    ADD CONSTRAINT fk6fmnit8ebej6endgia4d1e16j FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3853 (class 2606 OID 421632)
-- Name: taxon_propriete_taxon_taprota fk6k5ltfcprtjn1prg1n4lqtlro; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_propriete_taxon_taprota
    ADD CONSTRAINT fk6k5ltfcprtjn1prg1n4lqtlro FOREIGN KEY (vprota_id) REFERENCES public.valeur_propriete_taxon_vprota(vprota_id);


--
-- TOC entry 3781 (class 2606 OID 421272)
-- Name: insertion_dataset_ids fk6mvhhq9whm8cwnfj3on2eua2p; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fk6mvhhq9whm8cwnfj3on2eua2p FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3850 (class 2606 OID 421617)
-- Name: sous_sequence_sonde_multi_sssondemulti fk6t75fp1hp8meprqn8clgh0dp3; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_sonde_multi_sssondemulti
    ADD CONSTRAINT fk6t75fp1hp8meprqn8clgh0dp3 FOREIGN KEY (ssondemulti_id) REFERENCES public.sequence_sonde_multi_ssondemulti(ssondemulti_id);


--
-- TOC entry 3857 (class 2606 OID 421652)
-- Name: type_outils_mesure_tome fk6wpn8tj8u43mknn7c5nau77eq; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_outils_mesure_tome
    ADD CONSTRAINT fk6wpn8tj8u43mknn7c5nau77eq FOREIGN KEY (vq_id) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3846 (class 2606 OID 421597)
-- Name: sous_sequence_production_primaire_sspp fk7g8gbckkop60m0ssrjip546pr; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_production_primaire_sspp
    ADD CONSTRAINT fk7g8gbckkop60m0ssrjip546pr FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3882 (class 2606 OID 421777)
-- Name: variable_glacpe_varg fk7tr4q1exb5dly10v1fceckpj5; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_glacpe_varg
    ADD CONSTRAINT fk7tr4q1exb5dly10v1fceckpj5 FOREIGN KEY (var_id) REFERENCES public.variable(var_id);


--
-- TOC entry 3794 (class 2606 OID 421337)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux fk87ntufua27dcvs3o29y62y26p; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT fk87ntufua27dcvs3o29y62y26p FOREIGN KEY (sexe) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3869 (class 2606 OID 421712)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux fk8a8tac7aypexwa1qo4xy6ti3e; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux
    ADD CONSTRAINT fk8a8tac7aypexwa1qo4xy6ti3e FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3778 (class 2606 OID 421257)
-- Name: insertion_dataset_ids fk8fo5skc6htgn523y2fu7c1vxs; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fk8fo5skc6htgn523y2fu7c1vxs FOREIGN KEY (ids_create_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3813 (class 2606 OID 421432)
-- Name: refdata fk92w1pw55p4enf1k3n6qobd8sa; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.refdata
    ADD CONSTRAINT fk92w1pw55p4enf1k3n6qobd8sa FOREIGN KEY (id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3838 (class 2606 OID 421557)
-- Name: sous_sequence_haute_frequence_sshautefrequence fk99a45nr5827wwehxkufumu3t7; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence
    ADD CONSTRAINT fk99a45nr5827wwehxkufumu3t7 FOREIGN KEY (dim_id) REFERENCES public.dimension_dim(dim_id);


--
-- TOC entry 3855 (class 2606 OID 421642)
-- Name: taxon_tax fk9chse5qffowhm3ix3ad42430; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_tax
    ADD CONSTRAINT fk9chse5qffowhm3ix3ad42430 FOREIGN KEY (taxonparent_id) REFERENCES public.taxon_tax(tax_id);


--
-- TOC entry 3756 (class 2606 OID 421147)
-- Name: composite_node_data_set fk9d32fv5o0c8vntnkgb9g5wpy; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT fk9d32fv5o0c8vntnkgb9g5wpy FOREIGN KEY (id_parent_node) REFERENCES public.composite_node_data_set(branch_node_id);


--
-- TOC entry 3772 (class 2606 OID 421227)
-- Name: genericdatatype fk9g6pnggaxsb9whuj2v71fh6i9; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.genericdatatype
    ADD CONSTRAINT fk9g6pnggaxsb9whuj2v71fh6i9 FOREIGN KEY (ids_id) REFERENCES public.insertion_dataset_ids(ids_id);


--
-- TOC entry 3815 (class 2606 OID 421442)
-- Name: requete fk9iuo3dtjdw7xrl9996ax8okc5; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.requete
    ADD CONSTRAINT fk9iuo3dtjdw7xrl9996ax8okc5 FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3868 (class 2606 OID 421707)
-- Name: valeur_mesure_contenus_stomacaux_vmcontenustomacaux fk9upnx4k3by1yvt50xcq9qy4k4; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_contenus_stomacaux_vmcontenustomacaux
    ADD CONSTRAINT fk9upnx4k3by1yvt50xcq9qy4k4 FOREIGN KEY (proie) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3817 (class 2606 OID 421452)
-- Name: rightrequest_glacpe_rrg fk_6v8pl7md8n8no0d9qs4xaoypq; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.rightrequest_glacpe_rrg
    ADD CONSTRAINT fk_6v8pl7md8n8no0d9qs4xaoypq FOREIGN KEY (usr_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3863 (class 2606 OID 421682)
-- Name: valeur_mesure_chimie_vmchimie fka13g6o20jd4m8rfm2tr0wf0eu; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chimie_vmchimie
    ADD CONSTRAINT fka13g6o20jd4m8rfm2tr0wf0eu FOREIGN KEY (mchimie_id) REFERENCES public.mesure_chimie_mchimie(mchimie_id);


--
-- TOC entry 3796 (class 2606 OID 421347)
-- Name: mesure_haute_frequence_mhautefrequence fka1eh4u2ixw2dbfvrkij4ydb9y; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_haute_frequence_mhautefrequence
    ADD CONSTRAINT fka1eh4u2ixw2dbfvrkij4ydb9y FOREIGN KEY (sshautefrequence_id) REFERENCES public.sous_sequence_haute_frequence_sshautefrequence(sshautefrequence_id);


--
-- TOC entry 3793 (class 2606 OID 421332)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux fkacpdmk95cco22i9fnljnxxf8w; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT fkacpdmk95cco22i9fnljnxxf8w FOREIGN KEY (maturite) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3821 (class 2606 OID 421472)
-- Name: sequence_haute_frequence_shautefrequence fkag4db96ejt9e1y262kisl1eia; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_haute_frequence_shautefrequence
    ADD CONSTRAINT fkag4db96ejt9e1y262kisl1eia FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3865 (class 2606 OID 421692)
-- Name: valeur_mesure_chloro_vmchloro fkahlql21t9wd9s5msdq4tbeeyg; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chloro_vmchloro
    ADD CONSTRAINT fkahlql21t9wd9s5msdq4tbeeyg FOREIGN KEY (mchloro_id) REFERENCES public.mesure_chloro_mchloro(mchloro_id);


--
-- TOC entry 3782 (class 2606 OID 421277)
-- Name: insertion_filecomp_ifcs fkakh3w6nqtqmxjlpa8gybl5bo0; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT fkakh3w6nqtqmxjlpa8gybl5bo0 FOREIGN KEY (ifcs_create_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3759 (class 2606 OID 421162)
-- Name: composite_node_ref_data fkb4dhbn68xjtbgh0t1bx4nr0om; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT fkb4dhbn68xjtbgh0t1bx4nr0om FOREIGN KEY (id_parent_node) REFERENCES public.composite_node_ref_data(branch_node_id);


--
-- TOC entry 3829 (class 2606 OID 421512)
-- Name: sous_sequence_chimie_sschimie fkb51nhp7rfglg4uj207nbdc060; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chimie_sschimie
    ADD CONSTRAINT fkb51nhp7rfglg4uj207nbdc060 FOREIGN KEY (ome_id) REFERENCES public.outils_mesure_ome(ome_id);


--
-- TOC entry 3830 (class 2606 OID 421517)
-- Name: sous_sequence_chimie_sschimie fkbecaxxeqybxn9emi1790ct6qx; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chimie_sschimie
    ADD CONSTRAINT fkbecaxxeqybxn9emi1790ct6qx FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3849 (class 2606 OID 421612)
-- Name: sous_sequence_sonde_multi_sssondemulti fkbome1eks9rb3w8jptm674q6uu; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_sonde_multi_sssondemulti
    ADD CONSTRAINT fkbome1eks9rb3w8jptm674q6uu FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3861 (class 2606 OID 421672)
-- Name: valeur_condition_prelevement_vmconditionprelevement fkbowp0khauyp8ansn2f3w4j7d4; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_condition_prelevement_vmconditionprelevement
    ADD CONSTRAINT fkbowp0khauyp8ansn2f3w4j7d4 FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3866 (class 2606 OID 421697)
-- Name: valeur_mesure_chloro_vmchloro fkc02i0tt9s7alvcj5yy8otteqt; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chloro_vmchloro
    ADD CONSTRAINT fkc02i0tt9s7alvcj5yy8otteqt FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3825 (class 2606 OID 421492)
-- Name: site fkc6dfy2hroo6yf1d3hoth9vkbv; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT fkc6dfy2hroo6yf1d3hoth9vkbv FOREIGN KEY (parent_site_id) REFERENCES public.site(site_id);


--
-- TOC entry 3795 (class 2606 OID 421342)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux fkc8re5ay963bmci271vejij75f; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT fkc8re5ay963bmci271vejij75f FOREIGN KEY (sscontenustomacaux_id) REFERENCES public.sous_sequence_contenus_stomacaux_sscontenustomacaux(sscontenustomacaux_id);


--
-- TOC entry 3879 (class 2606 OID 421762)
-- Name: variable fkcc6dgptrkbjplrljqqqqtwdw3; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable
    ADD CONSTRAINT fkcc6dgptrkbjplrljqqqqtwdw3 FOREIGN KEY (var_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3832 (class 2606 OID 421527)
-- Name: sous_sequence_chloro_sschloro fkcxi5ncsdchr9pamu8wnqqf3pv; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chloro_sschloro
    ADD CONSTRAINT fkcxi5ncsdchr9pamu8wnqqf3pv FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3777 (class 2606 OID 421252)
-- Name: groupe_gro fkd0luibfwwc40y6yyxqwsk9bxm; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.groupe_gro
    ADD CONSTRAINT fkd0luibfwwc40y6yyxqwsk9bxm FOREIGN KEY (gro_gro_id) REFERENCES public.groupe_gro(gro_id);


--
-- TOC entry 3881 (class 2606 OID 421772)
-- Name: variable_glacpe_varg fkd9sg98g45ogivir4tw5t3apjd; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_glacpe_varg
    ADD CONSTRAINT fkd9sg98g45ogivir4tw5t3apjd FOREIGN KEY (vnor_id) REFERENCES public.variable_norme_vnor(vnor_id);


--
-- TOC entry 3769 (class 2606 OID 421212)
-- Name: datatype_variable_unite_glacpe_dvug fkd9sj4kof0aydvchc05w0u8gym; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_variable_unite_glacpe_dvug
    ADD CONSTRAINT fkd9sj4kof0aydvchc05w0u8gym FOREIGN KEY (uni_id) REFERENCES public.unite(uni_id);


--
-- TOC entry 3792 (class 2606 OID 421327)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux fkdgpg2gm0fobgi7dic9l5lu6a1; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT fkdgpg2gm0fobgi7dic9l5lu6a1 FOREIGN KEY (malformation) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3785 (class 2606 OID 421292)
-- Name: insertion_version_file_ivf fkdjo1nvaq8ghr3h5r9owu3egop; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT fkdjo1nvaq8ghr3h5r9owu3egop FOREIGN KEY (ids_id) REFERENCES public.insertion_dataset_ids(ids_id);


--
-- TOC entry 3864 (class 2606 OID 421687)
-- Name: valeur_mesure_chimie_vmchimie fke55jo4rhwa9ki3jam80gkjtti; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_chimie_vmchimie
    ADD CONSTRAINT fke55jo4rhwa9ki3jam80gkjtti FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3835 (class 2606 OID 421542)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux fke8q6x4pks746fcy6thc4gbb0x; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux
    ADD CONSTRAINT fke8q6x4pks746fcy6thc4gbb0x FOREIGN KEY (ome_id) REFERENCES public.outils_mesure_ome(ome_id);


--
-- TOC entry 3773 (class 2606 OID 421232)
-- Name: group_filecomp fkeeelh6xxwb86dnaisp78t3lq0; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.group_filecomp
    ADD CONSTRAINT fkeeelh6xxwb86dnaisp78t3lq0 FOREIGN KEY (group_id) REFERENCES public.groupe(id);


--
-- TOC entry 3802 (class 2606 OID 421377)
-- Name: outils_mesure_ome fkesvl2ldnc38xr48ahstkn07m0; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.outils_mesure_ome
    ADD CONSTRAINT fkesvl2ldnc38xr48ahstkn07m0 FOREIGN KEY (dim_id) REFERENCES public.dimension_dim(dim_id);


--
-- TOC entry 3862 (class 2606 OID 421677)
-- Name: valeur_condition_prelevement_vmconditionprelevement fkf3j72bclhscwyybjk3wvv9wt6; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_condition_prelevement_vmconditionprelevement
    ADD CONSTRAINT fkf3j72bclhscwyybjk3wvv9wt6 FOREIGN KEY (vq_id) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3828 (class 2606 OID 421507)
-- Name: site_glacpe_sit fkffqgq0f2k2uwh4ha1ou532e76; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.site_glacpe_sit
    ADD CONSTRAINT fkffqgq0f2k2uwh4ha1ou532e76 FOREIGN KEY (id) REFERENCES public.site(site_id);


--
-- TOC entry 3761 (class 2606 OID 421172)
-- Name: controle_coherence_cdc fkghok9v9x0d10fwis19oafnd5m; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.controle_coherence_cdc
    ADD CONSTRAINT fkghok9v9x0d10fwis19oafnd5m FOREIGN KEY (vdt_id) REFERENCES public.datatype_variable_unite_glacpe_dvug(vdt_id);


--
-- TOC entry 3783 (class 2606 OID 421282)
-- Name: insertion_filecomp_ifcs fkh3o01tlmt6vpkloner7dow7hk; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT fkh3o01tlmt6vpkloner7dow7hk FOREIGN KEY (ft_id) REFERENCES public.filetype(ft_id);


--
-- TOC entry 3771 (class 2606 OID 421222)
-- Name: datatype_variable_unite_glacpe_dvug fkh5vvk5xoa8ogeadom21357stv; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_variable_unite_glacpe_dvug
    ADD CONSTRAINT fkh5vvk5xoa8ogeadom21357stv FOREIGN KEY (vdt_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3757 (class 2606 OID 421152)
-- Name: composite_node_data_set fkhoqcd3yosuwh1q34p0k0goivy; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT fkhoqcd3yosuwh1q34p0k0goivy FOREIGN KEY (realnode) REFERENCES public.realnode(id);


--
-- TOC entry 3878 (class 2606 OID 421757)
-- Name: valeur_propriete_taxon_vprota fkhtdmwfx0nb07o6tgwjhews3al; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_propriete_taxon_vprota
    ADD CONSTRAINT fkhtdmwfx0nb07o6tgwjhews3al FOREIGN KEY (vq_id) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3775 (class 2606 OID 421242)
-- Name: group_utilisateur fkhx03ovyj3mb70dx6hf5ku0o1m; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.group_utilisateur
    ADD CONSTRAINT fkhx03ovyj3mb70dx6hf5ku0o1m FOREIGN KEY (usr_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3833 (class 2606 OID 421532)
-- Name: sous_sequence_chloro_sschloro fkhx8rc6plbe30xcydn18kqtmob; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chloro_sschloro
    ADD CONSTRAINT fkhx8rc6plbe30xcydn18kqtmob FOREIGN KEY (schloro_id) REFERENCES public.sequence_chloro_schloro(schloro_id);


--
-- TOC entry 3789 (class 2606 OID 421312)
-- Name: mesure_condition_prelevement_mconditionprelevement fki824gv223nlyaol45dfkwm77c; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_condition_prelevement_mconditionprelevement
    ADD CONSTRAINT fki824gv223nlyaol45dfkwm77c FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3839 (class 2606 OID 421562)
-- Name: sous_sequence_haute_frequence_sshautefrequence fkicid355k1ud9xi7c5wxb84dbg; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence
    ADD CONSTRAINT fkicid355k1ud9xi7c5wxb84dbg FOREIGN KEY (ome_id) REFERENCES public.outils_mesure_ome(ome_id);


--
-- TOC entry 3776 (class 2606 OID 421247)
-- Name: group_utilisateur fkihbwxbljy0bv7x3y5vi1uoml0; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.group_utilisateur
    ADD CONSTRAINT fkihbwxbljy0bv7x3y5vi1uoml0 FOREIGN KEY (group_id) REFERENCES public.groupe(id);


--
-- TOC entry 3803 (class 2606 OID 421382)
-- Name: outils_mesure_ome fkikg63066jrr3bsqnc8tahiotg; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.outils_mesure_ome
    ADD CONSTRAINT fkikg63066jrr3bsqnc8tahiotg FOREIGN KEY (tome_id) REFERENCES public.type_outils_mesure_tome(tome_id);


--
-- TOC entry 3875 (class 2606 OID 421742)
-- Name: valeur_mesure_production_primaire_vmpp fkikwdktl7btkrgjvdpykfpr3kc; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_production_primaire_vmpp
    ADD CONSTRAINT fkikwdktl7btkrgjvdpykfpr3kc FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3843 (class 2606 OID 421582)
-- Name: sous_sequence_phytoplancton_ssphytoplancton fkimq9ut3yapwu4p91uenk7qkhm; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton
    ADD CONSTRAINT fkimq9ut3yapwu4p91uenk7qkhm FOREIGN KEY (prelevement_id) REFERENCES public.outils_mesure_ome(ome_id);


--
-- TOC entry 3763 (class 2606 OID 421182)
-- Name: datatype fkixi17p09knvowjmhfocbrrfbj; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype
    ADD CONSTRAINT fkixi17p09knvowjmhfocbrrfbj FOREIGN KEY (dty_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3765 (class 2606 OID 421192)
-- Name: datatype_unite_variable_vdt fkj46owt7l4apb1sp9hyarqckk6; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fkj46owt7l4apb1sp9hyarqckk6 FOREIGN KEY (uni_id) REFERENCES public.unite(uni_id);


--
-- TOC entry 3826 (class 2606 OID 421497)
-- Name: site fkjfufr8g8k1hium7hmvotou1rk; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT fkjfufr8g8k1hium7hmvotou1rk FOREIGN KEY (site_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3788 (class 2606 OID 421307)
-- Name: mesure_chloro_mchloro fkjgj3ml8k15rcggy8ux3k7tb3b; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chloro_mchloro
    ADD CONSTRAINT fkjgj3ml8k15rcggy8ux3k7tb3b FOREIGN KEY (sschloro_id) REFERENCES public.sous_sequence_chloro_sschloro(sschloro_id);


--
-- TOC entry 3845 (class 2606 OID 421592)
-- Name: sous_sequence_phytoplancton_ssphytoplancton fkjk12s5lmjgjavje8droq3pt7u; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton
    ADD CONSTRAINT fkjk12s5lmjgjavje8droq3pt7u FOREIGN KEY (sphytoplancton_id) REFERENCES public.sequence_phytoplancton_sphytoplancton(sphytoplancton_id);


--
-- TOC entry 3768 (class 2606 OID 421207)
-- Name: datatype_variable_unite_glacpe_dvug fkjkqsesyok3w895whwmx0gl01a; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_variable_unite_glacpe_dvug
    ADD CONSTRAINT fkjkqsesyok3w895whwmx0gl01a FOREIGN KEY (dty_id) REFERENCES public.datatype(dty_id);


--
-- TOC entry 3842 (class 2606 OID 421577)
-- Name: sous_sequence_phytoplancton_ssphytoplancton fkjm7ode7aomcfp52majrkqghav; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton
    ADD CONSTRAINT fkjm7ode7aomcfp52majrkqghav FOREIGN KEY (mesure_id) REFERENCES public.outils_mesure_ome(ome_id);


--
-- TOC entry 3814 (class 2606 OID 421437)
-- Name: requete fkjnkx7cw83rciufbu3i98h10mj; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.requete
    ADD CONSTRAINT fkjnkx7cw83rciufbu3i98h10mj FOREIGN KEY (extractiontype_id) REFERENCES public.extractiontype(id);


--
-- TOC entry 3766 (class 2606 OID 421197)
-- Name: datatype_unite_variable_vdt fkk1h2k3k4scs7a79fkr7qhbg8e; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fkk1h2k3k4scs7a79fkr7qhbg8e FOREIGN KEY (var_id) REFERENCES public.variable(var_id);


--
-- TOC entry 3860 (class 2606 OID 421667)
-- Name: valeur_condition_prelevement_vmconditionprelevement fkk62y9in74yt698vju9yko4a6w; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_condition_prelevement_vmconditionprelevement
    ADD CONSTRAINT fkk62y9in74yt698vju9yko4a6w FOREIGN KEY (mconditionprelevement_id) REFERENCES public.mesure_condition_prelevement_mconditionprelevement(mconditionprelevement_id);


--
-- TOC entry 3807 (class 2606 OID 421402)
-- Name: projet_pro fkkgblixlknm6f8mhev8d7a7n16; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.projet_pro
    ADD CONSTRAINT fkkgblixlknm6f8mhev8d7a7n16 FOREIGN KEY (pro_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3859 (class 2606 OID 421662)
-- Name: type_site_tsit fkkpq47xnlxxihk7kpfl5ghiebt; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_site_tsit
    ADD CONSTRAINT fkkpq47xnlxxihk7kpfl5ghiebt FOREIGN KEY (tsit_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3774 (class 2606 OID 421237)
-- Name: group_filecomp fkkt3an0qqq65wixo27nxrvub3s; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.group_filecomp
    ADD CONSTRAINT fkkt3an0qqq65wixo27nxrvub3s FOREIGN KEY (file_comp_id) REFERENCES public.insertion_filecomp_ifcs(ifcs_id);


--
-- TOC entry 3812 (class 2606 OID 421427)
-- Name: realnode fkl3j5gvobv23enxcqag4awnbwu; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT fkl3j5gvobv23enxcqag4awnbwu FOREIGN KEY (id_parent_node) REFERENCES public.realnode(id);


--
-- TOC entry 3844 (class 2606 OID 421587)
-- Name: sous_sequence_phytoplancton_ssphytoplancton fklp9dmbx9ifpjme9omy75cabm2; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_phytoplancton_ssphytoplancton
    ADD CONSTRAINT fklp9dmbx9ifpjme9omy75cabm2 FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3856 (class 2606 OID 421647)
-- Name: theme fklpjre9pry3n8m38dmskrl5x0q; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.theme
    ADD CONSTRAINT fklpjre9pry3n8m38dmskrl5x0q FOREIGN KEY (id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3819 (class 2606 OID 421462)
-- Name: sequence_chloro_schloro fklqvowa97uf1e9nvurndo07s0i; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_chloro_schloro
    ADD CONSTRAINT fklqvowa97uf1e9nvurndo07s0i FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3871 (class 2606 OID 421722)
-- Name: valeur_mesure_haute_frequence_vmhautefrequence fklxsvbojyy19tb1poqagifdmu; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_haute_frequence_vmhautefrequence
    ADD CONSTRAINT fklxsvbojyy19tb1poqagifdmu FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3764 (class 2606 OID 421187)
-- Name: datatype_unite_variable_vdt fkm5kdj129k9uodwd5os1lmk321; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fkm5kdj129k9uodwd5os1lmk321 FOREIGN KEY (dty_id) REFERENCES public.datatype(dty_id);


--
-- TOC entry 3870 (class 2606 OID 421717)
-- Name: valeur_mesure_haute_frequence_vmhautefrequence fkm8490ipuovyubh04cxx39159o; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_haute_frequence_vmhautefrequence
    ADD CONSTRAINT fkm8490ipuovyubh04cxx39159o FOREIGN KEY (mhautefrequence_id) REFERENCES public.mesure_haute_frequence_mhautefrequence(mhautefrequence_id);


--
-- TOC entry 3787 (class 2606 OID 421302)
-- Name: mesure_chimie_mchimie fkmfrx00xhkaledgglud89rqvlt; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_chimie_mchimie
    ADD CONSTRAINT fkmfrx00xhkaledgglud89rqvlt FOREIGN KEY (sschimie_id) REFERENCES public.sous_sequence_chimie_sschimie(sschimie_id);


--
-- TOC entry 3880 (class 2606 OID 421767)
-- Name: variable_glacpe_varg fkmkqscmy5p70bgomxq8dijwfgl; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.variable_glacpe_varg
    ADD CONSTRAINT fkmkqscmy5p70bgomxq8dijwfgl FOREIGN KEY (gro_id) REFERENCES public.groupe_gro(gro_id);


--
-- TOC entry 3840 (class 2606 OID 421567)
-- Name: sous_sequence_haute_frequence_sshautefrequence fkmso78rkak95uerciujmvy4yh9; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence
    ADD CONSTRAINT fkmso78rkak95uerciujmvy4yh9 FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3820 (class 2606 OID 421467)
-- Name: sequence_contenus_stomacaux_scontenustomacaux fknlswmjigw1u0ea6t5wkmurko4; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_contenus_stomacaux_scontenustomacaux
    ADD CONSTRAINT fknlswmjigw1u0ea6t5wkmurko4 FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3852 (class 2606 OID 421627)
-- Name: taxon_propriete_taxon_taprota fknpeq41e1p3ty5w5m5w0y61evm; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_propriete_taxon_taprota
    ADD CONSTRAINT fknpeq41e1p3ty5w5m5w0y61evm FOREIGN KEY (tax_id) REFERENCES public.taxon_tax(tax_id);


--
-- TOC entry 3851 (class 2606 OID 421622)
-- Name: taxon_propriete_taxon_taprota fknrkm36npdpj21scpsemhctjd7; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.taxon_propriete_taxon_taprota
    ADD CONSTRAINT fknrkm36npdpj21scpsemhctjd7 FOREIGN KEY (prota_id) REFERENCES public.propriete_taxon_prota(prota_id);


--
-- TOC entry 3837 (class 2606 OID 421552)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux fkoefiom0s6e1wvgltvfi6ddnls; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux
    ADD CONSTRAINT fkoefiom0s6e1wvgltvfi6ddnls FOREIGN KEY (scontenustomacaux_id) REFERENCES public.sequence_contenus_stomacaux_scontenustomacaux(scontenustomacaux_id);


--
-- TOC entry 3822 (class 2606 OID 421477)
-- Name: sequence_phytoplancton_sphytoplancton fkon6xlnib4622273f3mv2megt2; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_phytoplancton_sphytoplancton
    ADD CONSTRAINT fkon6xlnib4622273f3mv2megt2 FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3873 (class 2606 OID 421732)
-- Name: valeur_mesure_phytoplancton_vmphytoplancton fkow5chjt942mc2sodpt19tw57; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_phytoplancton_vmphytoplancton
    ADD CONSTRAINT fkow5chjt942mc2sodpt19tw57 FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3877 (class 2606 OID 421752)
-- Name: valeur_mesure_sonde_multi_vmsondemulti fkp0if9jhbe6b77vxe7eisj0a33; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_sonde_multi_vmsondemulti
    ADD CONSTRAINT fkp0if9jhbe6b77vxe7eisj0a33 FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3847 (class 2606 OID 421602)
-- Name: sous_sequence_production_primaire_sspp fkp2ue24c9ik9nfvg5w8sn2s0o0; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_production_primaire_sspp
    ADD CONSTRAINT fkp2ue24c9ik9nfvg5w8sn2s0o0 FOREIGN KEY (spp_id) REFERENCES public.sequence_production_primaire_spp(spp_id);


--
-- TOC entry 3800 (class 2606 OID 421367)
-- Name: mesure_sonde_multi_msondemulti fkp38q1laix14kp61ypcslypkoj; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_sonde_multi_msondemulti
    ADD CONSTRAINT fkp38q1laix14kp61ypcslypkoj FOREIGN KEY (sssondemulti_id) REFERENCES public.sous_sequence_sonde_multi_sssondemulti(sssondemulti_id);


--
-- TOC entry 3790 (class 2606 OID 421317)
-- Name: mesure_condition_prelevement_mconditionprelevement fkp6bj5yd5tdlhwyd00eley0a29; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_condition_prelevement_mconditionprelevement
    ADD CONSTRAINT fkp6bj5yd5tdlhwyd00eley0a29 FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3810 (class 2606 OID 421417)
-- Name: realnode fkp8pnep929vo4qcl1n1fkw1mtc; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT fkp8pnep929vo4qcl1n1fkw1mtc FOREIGN KEY (id_ancestor) REFERENCES public.realnode(id);


--
-- TOC entry 3823 (class 2606 OID 421482)
-- Name: sequence_production_primaire_spp fkpeapkd86j86jhvds9wa465chd; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_production_primaire_spp
    ADD CONSTRAINT fkpeapkd86j86jhvds9wa465chd FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3858 (class 2606 OID 421657)
-- Name: type_plateforme_tpla fkpi3mrnekhn5qxm0o3snko5shd; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.type_plateforme_tpla
    ADD CONSTRAINT fkpi3mrnekhn5qxm0o3snko5shd FOREIGN KEY (tpla_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3841 (class 2606 OID 421572)
-- Name: sous_sequence_haute_frequence_sshautefrequence fkpim25csmd3tgwjyq2s5hyfs0c; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_haute_frequence_sshautefrequence
    ADD CONSTRAINT fkpim25csmd3tgwjyq2s5hyfs0c FOREIGN KEY (shautefrequence_id) REFERENCES public.sequence_haute_frequence_shautefrequence(shautefrequence_id);


--
-- TOC entry 3780 (class 2606 OID 421267)
-- Name: insertion_dataset_ids fkpr9d5m8ngwhhmqr9r9pukc5vy; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fkpr9d5m8ngwhhmqr9r9pukc5vy FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3872 (class 2606 OID 421727)
-- Name: valeur_mesure_phytoplancton_vmphytoplancton fkq5xujwguoxjtx58wwncuy5afs; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.valeur_mesure_phytoplancton_vmphytoplancton
    ADD CONSTRAINT fkq5xujwguoxjtx58wwncuy5afs FOREIGN KEY (mphytoplancton_id) REFERENCES public.mesure_phytoplancton_mphytoplancton(mphytoplancton_id);


--
-- TOC entry 3791 (class 2606 OID 421322)
-- Name: mesure_contenus_stomacaux_mcontenustomacaux fkq9ehqmbii05994g6x5n1totmw; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_contenus_stomacaux_mcontenustomacaux
    ADD CONSTRAINT fkq9ehqmbii05994g6x5n1totmw FOREIGN KEY (etat_estomac) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3760 (class 2606 OID 421167)
-- Name: composite_node_ref_data fkqb0ww0h1pj3wvf6dcq3fr7hou; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT fkqb0ww0h1pj3wvf6dcq3fr7hou FOREIGN KEY (realnode) REFERENCES public.realnode(id);


--
-- TOC entry 3831 (class 2606 OID 421522)
-- Name: sous_sequence_chimie_sschimie fkqehcrjv6d4y54k8kwkynth81c; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_chimie_sschimie
    ADD CONSTRAINT fkqehcrjv6d4y54k8kwkynth81c FOREIGN KEY (schimie_id) REFERENCES public.sequence_chimie_schimie(schimie_id);


--
-- TOC entry 3779 (class 2606 OID 421262)
-- Name: insertion_dataset_ids fkqjolp3pj9lshp7bqdvr72hfo7; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fkqjolp3pj9lshp7bqdvr72hfo7 FOREIGN KEY (ids_publish_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3784 (class 2606 OID 421287)
-- Name: insertion_filecomp_ifcs fkqwxx9utu2ay0quo41kgv2t3qo; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT fkqwxx9utu2ay0quo41kgv2t3qo FOREIGN KEY (ifcs_last_modify_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3827 (class 2606 OID 421502)
-- Name: site_glacpe_sit fkqxim7ckn5p5l1m7se95m07c2c; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.site_glacpe_sit
    ADD CONSTRAINT fkqxim7ckn5p5l1m7se95m07c2c FOREIGN KEY (tsit_id) REFERENCES public.type_site_tsit(tsit_id);


--
-- TOC entry 3824 (class 2606 OID 421487)
-- Name: sequence_sonde_multi_ssondemulti fkr65ols1s5cwkvfunp1uc8aur2; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sequence_sonde_multi_ssondemulti
    ADD CONSTRAINT fkr65ols1s5cwkvfunp1uc8aur2 FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3806 (class 2606 OID 421397)
-- Name: plateforme_pla fkryc2xubw0w0f5jcxavq1wyctx; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.plateforme_pla
    ADD CONSTRAINT fkryc2xubw0w0f5jcxavq1wyctx FOREIGN KEY (loc_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3758 (class 2606 OID 421157)
-- Name: composite_node_ref_data fks5b4w85norrexocr0cnsao05w; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT fks5b4w85norrexocr0cnsao05w FOREIGN KEY (id_ancestor) REFERENCES public.composite_node_ref_data(branch_node_id);


--
-- TOC entry 3801 (class 2606 OID 421372)
-- Name: notification fksa6pr4g4dbor5dc5as615hebe; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fksa6pr4g4dbor5dc5as615hebe FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3797 (class 2606 OID 421352)
-- Name: mesure_phytoplancton_mphytoplancton fksbwsjnryvwjg5o0sa63q9mh4s; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.mesure_phytoplancton_mphytoplancton
    ADD CONSTRAINT fksbwsjnryvwjg5o0sa63q9mh4s FOREIGN KEY (ssphytoplancton_id) REFERENCES public.sous_sequence_phytoplancton_ssphytoplancton(ssphytoplancton_id);


--
-- TOC entry 3836 (class 2606 OID 421547)
-- Name: sous_sequence_contenus_stomacaux_sscontenustomacaux fktc0eh4upjbw7h4wowe000n5yu; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.sous_sequence_contenus_stomacaux_sscontenustomacaux
    ADD CONSTRAINT fktc0eh4upjbw7h4wowe000n5yu FOREIGN KEY (loc_id) REFERENCES public.plateforme_pla(loc_id);


--
-- TOC entry 3755 (class 2606 OID 421142)
-- Name: composite_node_data_set fktdgs4po4hdthw56xqq6ka6k6m; Type: FK CONSTRAINT; Schema: public; Owner: philippe
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT fktdgs4po4hdthw56xqq6ka6k6m FOREIGN KEY (id_ancestor) REFERENCES public.composite_node_data_set(branch_node_id);


-- Completed on 2020-10-01 09:31:57 UTC

--
-- PostgreSQL database dump complete
--


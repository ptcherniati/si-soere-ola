

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 377297)
-- Name: composite_node_data_set; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.composite_node_data_set (
    branch_node_id bigint NOT NULL,
    index integer NOT NULL,
    id_ancestor bigint,
    id_parent_node bigint,
    realnode bigint NOT NULL
);


ALTER TABLE public.composite_node_data_set OWNER TO kerneluser;

--
-- TOC entry 203 (class 1259 OID 377302)
-- Name: composite_node_ref_data; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.composite_node_ref_data (
    branch_node_id bigint NOT NULL,
    index integer NOT NULL,
    id_ancestor bigint,
    id_parent_node bigint,
    realnode bigint NOT NULL
);


ALTER TABLE public.composite_node_ref_data OWNER TO kerneluser;

--
-- TOC entry 204 (class 1259 OID 377307)
-- Name: composite_nodeable; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.composite_nodeable (
    dtype character varying(31) NOT NULL,
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    ordernumber bigint
);


ALTER TABLE public.composite_nodeable OWNER TO kerneluser;

--
-- TOC entry 205 (class 1259 OID 377312)
-- Name: compositeactivityextraction; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.compositeactivityextraction (
    id bigint NOT NULL,
    dateend date,
    datestart date,
    idnode bigint NOT NULL,
    login character varying(255) NOT NULL
);


ALTER TABLE public.compositeactivityextraction OWNER TO kerneluser;

--
-- TOC entry 206 (class 1259 OID 377317)
-- Name: datatype; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.datatype (
    description text,
    name character varying(255) NOT NULL,
    dty_id bigint NOT NULL
);


ALTER TABLE public.datatype OWNER TO kerneluser;

--
-- TOC entry 207 (class 1259 OID 377325)
-- Name: datatype_unite_variable_vdt; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.datatype_unite_variable_vdt (
    datatypevariableunite_id bigint NOT NULL,
    dty_id bigint NOT NULL,
    uni_id bigint NOT NULL,
    var_id bigint NOT NULL
);


ALTER TABLE public.datatype_unite_variable_vdt OWNER TO kerneluser;

--
-- TOC entry 209 (class 1259 OID 377332)
-- Name: espece_esp; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.espece_esp (
    esp_id bigint NOT NULL,
    esp_code character varying(150) NOT NULL,
    esp_definition character varying(150),
    esp_nom character varying(150) NOT NULL
);


ALTER TABLE public.espece_esp OWNER TO kerneluser;

--
-- TOC entry 208 (class 1259 OID 377330)
-- Name: espece_esp_esp_id_seq; Type: SEQUENCE; Schema: public; Owner: kerneluser
--

CREATE SEQUENCE public.espece_esp_esp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.espece_esp_esp_id_seq OWNER TO kerneluser;

--
-- TOC entry 3378 (class 0 OID 0)
-- Dependencies: 208
-- Name: espece_esp_esp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kerneluser
--

ALTER SEQUENCE public.espece_esp_esp_id_seq OWNED BY public.espece_esp.esp_id;


--
-- TOC entry 210 (class 1259 OID 377338)
-- Name: extractiontype; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.extractiontype (
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.extractiontype OWNER TO kerneluser;

--
-- TOC entry 211 (class 1259 OID 377346)
-- Name: filetype; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.filetype (
    ft_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.filetype OWNER TO kerneluser;

--
-- TOC entry 212 (class 1259 OID 377354)
-- Name: genericdatatype; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.genericdatatype (
    generic_datatype_id bigint NOT NULL,
    ids_id bigint NOT NULL
);


ALTER TABLE public.genericdatatype OWNER TO kerneluser;

--
-- TOC entry 213 (class 1259 OID 377359)
-- Name: group_filecomp; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.group_filecomp (
    group_id bigint NOT NULL,
    file_comp_id bigint NOT NULL
);


ALTER TABLE public.group_filecomp OWNER TO kerneluser;

--
-- TOC entry 214 (class 1259 OID 377364)
-- Name: group_utilisateur; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.group_utilisateur (
    usr_id bigint NOT NULL,
    group_id bigint NOT NULL
);


ALTER TABLE public.group_utilisateur OWNER TO kerneluser;

--
-- TOC entry 215 (class 1259 OID 377369)
-- Name: groupe; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.groupe (
    id bigint NOT NULL,
    activityasstring text,
    group_name character varying(255),
    group_type character varying(255),
    group_which_tree character varying(255)
);


ALTER TABLE public.groupe OWNER TO kerneluser;

--
-- TOC entry 245 (class 1259 OID 377656)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: kerneluser
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO kerneluser;

--
-- TOC entry 216 (class 1259 OID 377377)
-- Name: hibernate_sequences; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.hibernate_sequences (
    sequence_name character varying(255) NOT NULL,
    next_val bigint
);


ALTER TABLE public.hibernate_sequences OWNER TO kerneluser;

--
-- TOC entry 217 (class 1259 OID 377382)
-- Name: insertion_dataset_ids; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.insertion_dataset_ids (
    ids_id bigint NOT NULL,
    ids_create_date timestamp without time zone NOT NULL,
    ids_date_debut_periode timestamp without time zone NOT NULL,
    ids_date_fin_periode timestamp without time zone NOT NULL,
    ids_publish_comment character varying(255),
    ids_publish_date timestamp without time zone,
    ids_create_user bigint NOT NULL,
    ids_publish_user bigint,
    ivf_id bigint,
    id bigint NOT NULL
);


ALTER TABLE public.insertion_dataset_ids OWNER TO kerneluser;

--
-- TOC entry 218 (class 1259 OID 377387)
-- Name: insertion_filecomp_ifcs; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.insertion_filecomp_ifcs (
    ifcs_id bigint NOT NULL,
    ifcs_code character varying(255) NOT NULL,
    contenttype character varying(255),
    ifcs_create_date timestamp without time zone,
    data oid NOT NULL,
    ifcs_date_debut_periode date,
    ifcs_date_fin_periode date,
    ifsc_description text NOT NULL,
    ifcs_name character varying(255) NOT NULL,
    forapplication boolean,
    ifcs_last_modify_date timestamp without time zone,
    mandatory boolean,
    ivfc_size integer,
    ifcs_create_user bigint NOT NULL,
    ft_id bigint NOT NULL,
    ifcs_last_modify_user bigint NOT NULL
);


ALTER TABLE public.insertion_filecomp_ifcs OWNER TO kerneluser;

--
-- TOC entry 219 (class 1259 OID 377395)
-- Name: insertion_version_file_ivf; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.insertion_version_file_ivf (
    ivf_id bigint NOT NULL,
    data oid,
    extention character varying(255),
    filename character varying(255),
    ivf_size integer,
    ivf_upload_date timestamp without time zone NOT NULL,
    ivf_version_number bigint,
    ids_id bigint NOT NULL,
    ivf_upload_user bigint NOT NULL
);


ALTER TABLE public.insertion_version_file_ivf OWNER TO kerneluser;

--
-- TOC entry 220 (class 1259 OID 377403)
-- Name: localisation; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.localisation (
    id bigint NOT NULL,
    localization character varying(255),
    colonne character varying(255),
    defaultstring text,
    entite character varying(255),
    localestring text
);


ALTER TABLE public.localisation OWNER TO kerneluser;

--
-- TOC entry 222 (class 1259 OID 377413)
-- Name: mesure_pem; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.mesure_pem (
    mpem_id bigint NOT NULL,
    esp_id bigint NOT NULL,
    spem_id bigint NOT NULL
);


ALTER TABLE public.mesure_pem OWNER TO kerneluser;

--
-- TOC entry 221 (class 1259 OID 377411)
-- Name: mesure_pem_mpem_id_seq; Type: SEQUENCE; Schema: public; Owner: kerneluser
--

CREATE SEQUENCE public.mesure_pem_mpem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mesure_pem_mpem_id_seq OWNER TO kerneluser;

--
-- TOC entry 3379 (class 0 OID 0)
-- Dependencies: 221
-- Name: mesure_pem_mpem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kerneluser
--

ALTER SEQUENCE public.mesure_pem_mpem_id_seq OWNED BY public.mesure_pem.mpem_id;


--
-- TOC entry 223 (class 1259 OID 377419)
-- Name: notification; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.notification (
    id bigint NOT NULL,
    archived boolean NOT NULL,
    attachment text,
    body text,
    date timestamp without time zone,
    level character varying(255) NOT NULL,
    message text NOT NULL,
    utilisateur_id bigint NOT NULL
);


ALTER TABLE public.notification OWNER TO kerneluser;

--
-- TOC entry 224 (class 1259 OID 377427)
-- Name: pemsynthesisdatatype; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.pemsynthesisdatatype (
    id bigint NOT NULL,
    idnodes character varying(255) NOT NULL,
    maxdate timestamp without time zone NOT NULL,
    mindate timestamp without time zone NOT NULL,
    site character varying(255) NOT NULL
);


ALTER TABLE public.pemsynthesisdatatype OWNER TO kerneluser;

--
-- TOC entry 225 (class 1259 OID 377435)
-- Name: pemsynthesisvalue; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.pemsynthesisvalue (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    idnode bigint NOT NULL,
    ismean boolean NOT NULL,
    site character varying(255) NOT NULL,
    valuefloat real,
    valuestring character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.pemsynthesisvalue OWNER TO kerneluser;

--
-- TOC entry 226 (class 1259 OID 377443)
-- Name: projet; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.projet (
    definition text,
    id bigint NOT NULL
);


ALTER TABLE public.projet OWNER TO kerneluser;

--
-- TOC entry 227 (class 1259 OID 377451)
-- Name: realnode; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.realnode (
    id bigint NOT NULL,
    path character varying(255) NOT NULL,
    id_ancestor bigint,
    id_nodeable bigint,
    id_parent_node bigint
);


ALTER TABLE public.realnode OWNER TO kerneluser;

--
-- TOC entry 228 (class 1259 OID 377456)
-- Name: refdata; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.refdata (
    description character varying(255),
    id bigint NOT NULL
);


ALTER TABLE public.refdata OWNER TO kerneluser;

--
-- TOC entry 229 (class 1259 OID 377461)
-- Name: requete; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.requete (
    id bigint NOT NULL,
    commentaire text,
    date timestamp without time zone,
    duree_extraction bigint NOT NULL,
    nom_fichier character varying(255) NOT NULL,
    extractiontype_id bigint NOT NULL,
    utilisateur_id bigint NOT NULL
);


ALTER TABLE public.requete OWNER TO kerneluser;

--
-- TOC entry 230 (class 1259 OID 377469)
-- Name: rightrequest; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.rightrequest (
    id bigint NOT NULL,
    answer character varying(255),
    concluded boolean,
    createdate timestamp without time zone,
    infos character varying(255),
    motivation character varying(255),
    reference character varying(255),
    request character varying(255) NOT NULL,
    status boolean,
    usr_id bigint NOT NULL
);


ALTER TABLE public.rightrequest OWNER TO kerneluser;

--
-- TOC entry 232 (class 1259 OID 377479)
-- Name: sequencepem; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.sequencepem (
    spem_id bigint NOT NULL,
    date date,
    ivf_id bigint NOT NULL
);


ALTER TABLE public.sequencepem OWNER TO kerneluser;

--
-- TOC entry 231 (class 1259 OID 377477)
-- Name: sequencepem_spem_id_seq; Type: SEQUENCE; Schema: public; Owner: kerneluser
--

CREATE SEQUENCE public.sequencepem_spem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequencepem_spem_id_seq OWNER TO kerneluser;

--
-- TOC entry 3380 (class 0 OID 0)
-- Dependencies: 231
-- Name: sequencepem_spem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kerneluser
--

ALTER SEQUENCE public.sequencepem_spem_id_seq OWNED BY public.sequencepem.spem_id;


--
-- TOC entry 233 (class 1259 OID 377485)
-- Name: site; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.site (
    description text,
    name character varying(255) NOT NULL,
    site_id bigint NOT NULL,
    parent_site_id bigint
);


ALTER TABLE public.site OWNER TO kerneluser;

--
-- TOC entry 234 (class 1259 OID 377493)
-- Name: theme; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.theme (
    description text,
    id bigint NOT NULL
);


ALTER TABLE public.theme OWNER TO kerneluser;

--
-- TOC entry 236 (class 1259 OID 377503)
-- Name: types_zone_etude_tze; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.types_zone_etude_tze (
    tze_id bigint NOT NULL,
    tze_code character varying(150) NOT NULL,
    tze_definition character varying(150)
);


ALTER TABLE public.types_zone_etude_tze OWNER TO kerneluser;

--
-- TOC entry 235 (class 1259 OID 377501)
-- Name: types_zone_etude_tze_tze_id_seq; Type: SEQUENCE; Schema: public; Owner: kerneluser
--

CREATE SEQUENCE public.types_zone_etude_tze_tze_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.types_zone_etude_tze_tze_id_seq OWNER TO kerneluser;

--
-- TOC entry 3381 (class 0 OID 0)
-- Dependencies: 235
-- Name: types_zone_etude_tze_tze_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kerneluser
--

ALTER SEQUENCE public.types_zone_etude_tze_tze_id_seq OWNED BY public.types_zone_etude_tze.tze_id;


--
-- TOC entry 237 (class 1259 OID 377509)
-- Name: unite; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.unite (
    uni_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.unite OWNER TO kerneluser;

--
-- TOC entry 238 (class 1259 OID 377517)
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.utilisateur (
    id bigint NOT NULL,
    active boolean NOT NULL,
    email character varying(255) NOT NULL,
    emploi character varying(255),
    isroot boolean,
    language character varying(255),
    login character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    poste character varying(255),
    prenom character varying(255) NOT NULL
);


ALTER TABLE public.utilisateur OWNER TO kerneluser;

--
-- TOC entry 240 (class 1259 OID 377527)
-- Name: valeur_pem; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.valeur_pem (
    vpem_id bigint NOT NULL,
    vpm_value real,
    mpem_id bigint NOT NULL,
    id bigint NOT NULL,
    vq_id bigint
);


ALTER TABLE public.valeur_pem OWNER TO kerneluser;

--
-- TOC entry 239 (class 1259 OID 377525)
-- Name: valeur_pem_vpem_id_seq; Type: SEQUENCE; Schema: public; Owner: kerneluser
--

CREATE SEQUENCE public.valeur_pem_vpem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.valeur_pem_vpem_id_seq OWNER TO kerneluser;

--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 239
-- Name: valeur_pem_vpem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kerneluser
--

ALTER SEQUENCE public.valeur_pem_vpem_id_seq OWNED BY public.valeur_pem.vpem_id;


--
-- TOC entry 241 (class 1259 OID 377533)
-- Name: valeur_qualitative; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.valeur_qualitative (
    vq_id bigint NOT NULL,
    code character varying(255) NOT NULL,
    typelist character varying(255),
    valeur character varying(255) NOT NULL
);


ALTER TABLE public.valeur_qualitative OWNER TO kerneluser;

--
-- TOC entry 242 (class 1259 OID 377541)
-- Name: variable; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.variable (
    affichage character varying(255),
    definition text,
    isqualitative boolean DEFAULT false,
    var_id bigint NOT NULL
);


ALTER TABLE public.variable OWNER TO kerneluser;

--
-- TOC entry 243 (class 1259 OID 377550)
-- Name: variablemonsoere; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.variablemonsoere (
    variablemonsoere_id bigint NOT NULL
);


ALTER TABLE public.variablemonsoere OWNER TO kerneluser;

--
-- TOC entry 244 (class 1259 OID 377555)
-- Name: zones_etude_zet; Type: TABLE; Schema: public; Owner: kerneluser
--

CREATE TABLE public.zones_etude_zet (
    sitemonsoeredisc character varying(255),
    sitemonsoere_id bigint NOT NULL,
    tze_id bigint NOT NULL
);


ALTER TABLE public.zones_etude_zet OWNER TO kerneluser;

--
-- TOC entry 2954 (class 2604 OID 378661)
-- Name: espece_esp esp_id; Type: DEFAULT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.espece_esp ALTER COLUMN esp_id SET DEFAULT nextval('public.espece_esp_esp_id_seq'::regclass);


--
-- TOC entry 2955 (class 2604 OID 378662)
-- Name: mesure_pem mpem_id; Type: DEFAULT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.mesure_pem ALTER COLUMN mpem_id SET DEFAULT nextval('public.mesure_pem_mpem_id_seq'::regclass);


--
-- TOC entry 2956 (class 2604 OID 378663)
-- Name: sequencepem spem_id; Type: DEFAULT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.sequencepem ALTER COLUMN spem_id SET DEFAULT nextval('public.sequencepem_spem_id_seq'::regclass);


--
-- TOC entry 2957 (class 2604 OID 378664)
-- Name: types_zone_etude_tze tze_id; Type: DEFAULT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.types_zone_etude_tze ALTER COLUMN tze_id SET DEFAULT nextval('public.types_zone_etude_tze_tze_id_seq'::regclass);


--
-- TOC entry 2958 (class 2604 OID 378665)
-- Name: valeur_pem vpem_id; Type: DEFAULT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_pem ALTER COLUMN vpem_id SET DEFAULT nextval('public.valeur_pem_vpem_id_seq'::regclass);


--
-- TOC entry 3364 (class 2613 OID 385173)
-- Name: 385173; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385173');


ALTER LARGE OBJECT 385173 OWNER TO kerneluser;

--
-- TOC entry 3365 (class 2613 OID 385174)
-- Name: 385174; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385174');


ALTER LARGE OBJECT 385174 OWNER TO kerneluser;

--
-- TOC entry 3366 (class 2613 OID 385175)
-- Name: 385175; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385175');


ALTER LARGE OBJECT 385175 OWNER TO kerneluser;

--
-- TOC entry 3367 (class 2613 OID 385176)
-- Name: 385176; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385176');


ALTER LARGE OBJECT 385176 OWNER TO kerneluser;

--
-- TOC entry 3368 (class 2613 OID 385177)
-- Name: 385177; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385177');


ALTER LARGE OBJECT 385177 OWNER TO kerneluser;

--
-- TOC entry 3369 (class 2613 OID 385178)
-- Name: 385178; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385178');


ALTER LARGE OBJECT 385178 OWNER TO kerneluser;

--
-- TOC entry 3370 (class 2613 OID 385179)
-- Name: 385179; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385179');


ALTER LARGE OBJECT 385179 OWNER TO kerneluser;

--
-- TOC entry 3371 (class 2613 OID 385180)
-- Name: 385180; Type: BLOB; Schema: -; Owner: kerneluser
--

SELECT pg_catalog.lo_create('385180');


ALTER LARGE OBJECT 385180 OWNER TO kerneluser;

--
-- TOC entry 3320 (class 0 OID 377297)
-- Dependencies: 202
-- Data for Name: composite_node_data_set; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (222, 1, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (223, 1, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (224, 1, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (225, 1, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (226, 1, 224, 225, 191);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (227, 1, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (228, 1, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (229, 1, 227, 228, 191);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (230, 1, NULL, 229, 192);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (231, 1, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (232, 1, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (233, 1, 231, 232, 191);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (234, 1, NULL, 233, 192);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (235, 1, NULL, 234, 193);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (236, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (237, 1, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (238, 1, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (239, 1, 238, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (240, 1, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (241, 1, 240, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (242, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (243, 1, 241, 242, 196);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (244, 1, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (245, 1, 244, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (246, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (247, 1, 245, 246, 196);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (248, 1, NULL, 247, 197);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (249, 1, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (250, 1, 249, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (251, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (252, 1, 250, 251, 196);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (253, 1, NULL, 252, 197);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (254, 1, NULL, 253, 198);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (255, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (256, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (257, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (258, 2, 257, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (259, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (260, 2, 259, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (261, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (262, 1, 260, 261, 199);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (263, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (264, 2, 263, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (265, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (266, 1, 264, 265, 199);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (267, 1, NULL, 266, 200);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (268, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (269, 2, 268, NULL, 195);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (270, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (271, 1, 269, 270, 199);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (272, 1, NULL, 271, 200);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (273, 1, NULL, 272, 201);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (274, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (275, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (276, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (277, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (278, 1, 276, 277, 202);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (279, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (280, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (281, 1, 279, 280, 202);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (282, 1, NULL, 281, 203);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (283, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (284, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (285, 1, 283, 284, 202);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (286, 1, NULL, 285, 203);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (287, 1, NULL, 286, 204);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (288, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (289, 1, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (290, 1, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (291, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (292, 1, 290, 291, 206);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (293, 1, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (294, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (295, 1, 293, 294, 206);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (296, 1, NULL, 295, 207);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (297, 1, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (298, 2, NULL, NULL, 189);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (299, 1, 297, 298, 206);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (300, 1, NULL, 299, 207);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (301, 1, NULL, 300, 208);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (302, 1, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (303, 2, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (304, 2, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (305, 1, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (306, 1, 304, 305, 210);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (307, 2, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (308, 1, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (309, 1, 307, 308, 210);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (310, 1, NULL, 309, 211);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (311, 2, NULL, NULL, 190);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (312, 1, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (313, 1, 311, 312, 210);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (314, 1, NULL, 313, 211);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (315, 1, NULL, 314, 212);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (316, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (317, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (318, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (319, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (320, 1, 318, 319, 213);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (321, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (322, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (323, 1, 321, 322, 213);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (324, 1, NULL, 323, 214);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (325, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (326, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (327, 1, 325, 326, 213);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (328, 1, NULL, 327, 214);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (329, 1, NULL, 328, 215);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (330, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (331, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (332, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (333, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (334, 1, 332, 333, 216);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (335, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (336, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (337, 1, 335, 336, 216);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (338, 1, NULL, 337, 217);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (339, 2, NULL, NULL, 194);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (340, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (341, 1, 339, 340, 216);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (342, 1, NULL, 341, 217);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (343, 1, NULL, 342, 218);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (344, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (345, 2, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (346, 2, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (347, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (348, 1, 346, 347, 219);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (349, 2, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (350, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (351, 1, 349, 350, 219);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (352, 1, NULL, 351, 220);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (353, 2, NULL, NULL, 205);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (354, 2, NULL, NULL, 209);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (355, 1, 353, 354, 219);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (356, 1, NULL, 355, 220);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (357, 1, NULL, 356, 221);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (391, 1, NULL, 235, 390);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (393, 1, NULL, 235, 392);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (395, 1, NULL, 254, 394);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (397, 1, NULL, 254, 396);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (399, 1, NULL, 273, 398);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (401, 1, NULL, 273, 400);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (403, 1, NULL, 287, 402);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (405, 1, NULL, 287, 404);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (407, 1, NULL, 301, 406);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (409, 1, NULL, 301, 408);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (411, 1, NULL, 315, 410);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (413, 1, NULL, 315, 412);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (415, 1, NULL, 329, 414);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (417, 1, NULL, 329, 416);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (419, 1, NULL, 343, 418);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (421, 1, NULL, 343, 420);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (423, 1, NULL, 357, 422);
INSERT INTO public.composite_node_data_set (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (425, 1, NULL, 357, 424);


--
-- TOC entry 3321 (class 0 OID 377302)
-- Dependencies: 203
-- Data for Name: composite_node_ref_data; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (79, 1, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (80, 1, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (81, 1, 80, NULL, 78);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (84, 1, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (85, 1, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (86, 1, 85, NULL, 83);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (88, 2, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (89, 2, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (90, 1, 89, NULL, 87);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (92, 1, NULL, NULL, 91);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (94, 3, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (95, 3, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (96, 1, 95, NULL, 93);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (98, 2, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (99, 2, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (100, 1, 99, NULL, 97);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (102, 4, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (103, 4, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (104, 1, 103, NULL, 101);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (106, 3, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (107, 3, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (108, 1, 107, NULL, 105);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (110, 5, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (111, 5, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (112, 1, 111, NULL, 109);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (114, 6, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (115, 6, NULL, NULL, 82);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (116, 1, 115, NULL, 113);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (119, 1, NULL, NULL, 117);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (120, 1, NULL, NULL, 117);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (121, 1, 120, NULL, 118);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (123, 4, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (124, 4, NULL, NULL, 77);
INSERT INTO public.composite_node_ref_data (branch_node_id, index, id_ancestor, id_parent_node, realnode) VALUES (125, 1, 124, NULL, 122);


--
-- TOC entry 3322 (class 0 OID 377307)
-- Dependencies: 204
-- Data for Name: composite_nodeable; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 12, 'arborescence/types_de_sites', 1);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 20, 'arborescence/projet', 2);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 25, 'arborescence/site', 3);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 30, 'data_description/unit', 2);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 38, 'arborescence/thematic', 5);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 43, 'data_description/qualitatives_values', 3);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 48, 'arborescence/datatype', 1);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 53, 'arborescence/std', 7);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 58, 'data_description/variable', 3);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 63, 'data_description/dvu', 4);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 68, 'pem/species', 4);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 71, 'pem', 4);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 76, 'type_de_fichiers', 8);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('DataType', 130, 'piegeage_en_montee', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Projet', 144, 'projet_manche', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Projet', 149, 'projet_atlantique', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 155, 'oir', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 160, 'oir/p2', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 163, 'oir/p1', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 166, 'oir/p1/a', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 169, 'oir/p1/b', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 174, 'nivelle', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 179, 'scarff', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 180, 'nivelle/p1', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('SiteMonSoereDisc', 181, 'scarff/p1', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Theme', 187, 'donnees_biologiques', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('VariableMonSoere', 368, 'nombre_d_individus', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('VariableMonSoere', 373, 'couleur_des_individus', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('DatatypeVariableUnite', 388, 'piegeage_en_montee_nombre_d_individus_*', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('DatatypeVariableUnite', 389, 'piegeage_en_montee_couleur_des_individus_*', NULL);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 15, 'arborescence', 7);
INSERT INTO public.composite_nodeable (dtype, id, code, ordernumber) VALUES ('Refdata', 33, 'data_description', 4);


--
-- TOC entry 3323 (class 0 OID 377312)
-- Dependencies: 205
-- Data for Name: compositeactivityextraction; Type: TABLE DATA; Schema: public; Owner: kerneluser
--



--
-- TOC entry 3324 (class 0 OID 377317)
-- Dependencies: 206
-- Data for Name: datatype; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.datatype (description, name, dty_id) VALUES ('piegeage_en_montee_description', 'piegeage_en_montee', 130);


--
-- TOC entry 3325 (class 0 OID 377325)
-- Dependencies: 207
-- Data for Name: datatype_unite_variable_vdt; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.datatype_unite_variable_vdt (datatypevariableunite_id, dty_id, uni_id, var_id) VALUES (388, 130, 1, 368);
INSERT INTO public.datatype_unite_variable_vdt (datatypevariableunite_id, dty_id, uni_id, var_id) VALUES (389, 130, 1, 373);


--
-- TOC entry 3327 (class 0 OID 377332)
-- Dependencies: 209
-- Data for Name: espece_esp; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (1, 'lpf', 'LPF', 'LPF');
INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (2, 'ang', 'ANG', 'ANG');
INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (3, 'lpm', 'LPM', 'LPM');
INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (4, 'alo', 'ALO', 'ALO');
INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (5, 'trm', 'TRM', 'TRM');
INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (6, 'trf', 'TRF', 'TRF');
INSERT INTO public.espece_esp (esp_id, esp_code, esp_definition, esp_nom) VALUES (7, 'sat', 'SAT', 'SAT');


--
-- TOC entry 3328 (class 0 OID 377338)
-- Dependencies: 210
-- Data for Name: extractiontype; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.extractiontype (id, code, description, name) VALUES (131, 'pem', 'Extraction de données de piégeage en montée', 'pem');
INSERT INTO public.extractiontype (id, code, description, name) VALUES (132, 'extraction_de_fichiers_bruts', 'Extraction de données déposées sous forme de fichiers bruts', 'Extraction de fichiers bruts');


--
-- TOC entry 3329 (class 0 OID 377346)
-- Dependencies: 211
-- Data for Name: filetype; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.filetype (ft_id, code, description, name) VALUES (2, 'protocole', 'protocole d''expérimentation', 'protocole');
INSERT INTO public.filetype (ft_id, code, description, name) VALUES (3, 'donnees_brutes', 'fichier de données brutes', 'données brutes');
INSERT INTO public.filetype (ft_id, code, description, name) VALUES (4, 'licence', 'licence des données', 'licence');


--
-- TOC entry 3330 (class 0 OID 377354)
-- Dependencies: 212
-- Data for Name: genericdatatype; Type: TABLE DATA; Schema: public; Owner: kerneluser
--



--
-- TOC entry 3331 (class 0 OID 377359)
-- Dependencies: 213
-- Data for Name: group_filecomp; Type: TABLE DATA; Schema: public; Owner: kerneluser
--



--
-- TOC entry 3332 (class 0 OID 377364)
-- Dependencies: 214
-- Data for Name: group_utilisateur; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.group_utilisateur (usr_id, group_id) VALUES (2, 3);
INSERT INTO public.group_utilisateur (usr_id, group_id) VALUES (2, 4);
INSERT INTO public.group_utilisateur (usr_id, group_id) VALUES (5, 6);
INSERT INTO public.group_utilisateur (usr_id, group_id) VALUES (5, 7);


--
-- TOC entry 3333 (class 0 OID 377369)
-- Dependencies: 215
-- Data for Name: groupe; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.groupe (id, activityasstring, group_name, group_type, group_which_tree) VALUES (1, '{}', 'public', 'USER_TYPE', 'TREEDATASET');
INSERT INTO public.groupe (id, activityasstring, group_name, group_type, group_which_tree) VALUES (3, '{}', 'admin', 'USER_TYPE', 'TREEDATASET');
INSERT INTO public.groupe (id, activityasstring, group_name, group_type, group_which_tree) VALUES (4, '{}', 'admin', 'USER_TYPE', 'TREEREFDATA');
INSERT INTO public.groupe (id, activityasstring, group_name, group_type, group_which_tree) VALUES (6, '{}', 'anonymous', 'USER_TYPE', 'TREEDATASET');
INSERT INTO public.groupe (id, activityasstring, group_name, group_type, group_which_tree) VALUES (7, '{}', 'anonymous', 'USER_TYPE', 'TREEREFDATA');


--
-- TOC entry 3334 (class 0 OID 377377)
-- Dependencies: 216
-- Data for Name: hibernate_sequences; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.hibernate_sequences (sequence_name, next_val) VALUES ('default', 4);


--
-- TOC entry 3335 (class 0 OID 377382)
-- Dependencies: 217
-- Data for Name: insertion_dataset_ids; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (446, '2020-08-31 16:44:57.041', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:45:03.121', 2, 2, 448, 193);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (451, '2020-08-31 16:45:24.434', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:45:29.667', 2, 2, 453, 198);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (456, '2020-08-31 16:45:41.117', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:45:45.35', 2, 2, 458, 204);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (461, '2020-08-31 16:45:53.365', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:45:56.443', 2, 2, 463, 208);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (466, '2020-08-31 16:46:07.953', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:46:13.262', 2, 2, 468, 212);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (471, '2020-08-31 16:46:28.497', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:46:32.795', 2, 2, 473, 215);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (476, '2020-08-31 16:46:39.313', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:46:42.945', 2, 2, 478, 218);
INSERT INTO public.insertion_dataset_ids (ids_id, ids_create_date, ids_date_debut_periode, ids_date_fin_periode, ids_publish_comment, ids_publish_date, ids_create_user, ids_publish_user, ivf_id, id) VALUES (481, '2020-08-31 16:46:53.044', '1984-01-01 00:00:00', '1984-01-05 00:00:00', NULL, '2020-08-31 16:46:58.122', 2, 2, 483, 221);


--
-- TOC entry 3336 (class 0 OID 377387)
-- Dependencies: 218
-- Data for Name: insertion_filecomp_ifcs; Type: TABLE DATA; Schema: public; Owner: kerneluser
--



--
-- TOC entry 3337 (class 0 OID 377395)
-- Dependencies: 219
-- Data for Name: insertion_version_file_ivf; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (448, 385173, NULL, NULL, 2344, '2020-08-31 16:44:57.041', 1, 446, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (453, 385174, NULL, NULL, 2276, '2020-08-31 16:45:24.435', 1, 451, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (458, 385175, NULL, NULL, 2208, '2020-08-31 16:45:41.117', 1, 456, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (463, 385176, NULL, NULL, 2310, '2020-08-31 16:45:53.365', 1, 461, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (468, 385177, NULL, NULL, 2208, '2020-08-31 16:46:07.954', 1, 466, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (473, 385178, NULL, NULL, 2140, '2020-08-31 16:46:28.497', 1, 471, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (478, 385179, NULL, NULL, 2072, '2020-08-31 16:46:39.313', 1, 476, 2);
INSERT INTO public.insertion_version_file_ivf (ivf_id, data, extention, filename, ivf_size, ivf_upload_date, ivf_version_number, ids_id, ivf_upload_user) VALUES (483, 385180, NULL, NULL, 2174, '2020-08-31 16:46:53.044', 1, 481, 2);


--
-- TOC entry 3338 (class 0 OID 377403)
-- Dependencies: 220
-- Data for Name: localisation; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (8, 'en', 'name', 'arborescence/types_de_sites', 'refdata', 'Types of sites');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (9, 'fr', 'name', 'arborescence/types_de_sites', 'refdata', 'Type de sites');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (10, 'en', 'description', 'type_site_description', 'refdata', 'A type of site distinguishes the different study areas.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (11, 'fr', 'description', 'type_site_description', 'refdata', 'Un type de site permet de distingué les différentes zones d''étude');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (13, 'en', 'name', 'arborescence', 'refdata', 'Site arborescence');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (14, 'fr', 'name', 'arborescence', 'refdata', 'Arborescence');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (16, 'en', 'name', 'arborescence/projet', 'refdata', 'Projects');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (17, 'fr', 'name', 'arborescence/projet', 'refdata', 'Projets');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (18, 'en', 'description', 'projet_description', 'refdata', 'Defines the project to which the data relates');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (19, 'fr', 'description', 'projet_description', 'refdata', 'Définit le projet sur lequel portent les données');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (21, 'en', 'name', 'arborescence/site', 'refdata', 'Sites');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (22, 'fr', 'name', 'arborescence/site', 'refdata', 'Sites');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (23, 'en', 'description', 'site_description', 'refdata', 'Defines the name of the study area');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (24, 'fr', 'description', 'site_description', 'refdata', 'Définit le nom de la zone d''étude');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (26, 'en', 'name', 'data_description/unit', 'refdata', 'Units');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (27, 'fr', 'name', 'data_description/unit', 'refdata', 'Unités');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (28, 'en', 'description', 'unites_description', 'refdata', 'Defines the units. The display column is the one used for column headers.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (29, 'fr', 'description', 'unites_description', 'refdata', 'Définit les unités. La colonne affichage est celle utiliser pour les en-tête de colonne.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (31, 'en', 'name', 'data_description', 'refdata', 'Data description');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (32, 'fr', 'name', 'data_description', 'refdata', 'Description données');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (34, 'en', 'name', 'arborescence/thematic', 'refdata', 'Thematic');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (35, 'fr', 'name', 'arborescence/thematic', 'refdata', 'Thèmes');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (36, 'en', 'description', 'themes_description', 'refdata', 'A thematic group includes a set of similar data types');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (37, 'fr', 'description', 'themes_description', 'refdata', 'Une thématique regroupe un ensemble de type de données  similaires');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (39, 'en', 'name', 'data_description/qualitatives_values', 'refdata', 'Qualitatives values');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (40, 'fr', 'name', 'data_description/qualitatives_values', 'refdata', 'Valeurs qualitatives');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (41, 'en', 'description', 'vq_description', 'refdata', 'Qualitative data');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (42, 'fr', 'description', 'vq_description', 'refdata', 'Données qualitatives');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (44, 'en', 'name', 'arborescence/datatype', 'refdata', 'Datatypes');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (45, 'fr', 'name', 'arborescence/datatype', 'refdata', 'Types de données');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (46, 'en', 'description', 'datatype_description', 'refdata', 'A data type is defined by an input file format and associated processes.Each data type has its own variable definitions.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (47, 'fr', 'description', 'datatype_description', 'refdata', 'Un type de donnée est défini par un format de fichier d''entrée et de traitements associés.Chaque type de données à ses propres définitions de variables.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (49, 'en', 'name', 'arborescence/std', 'refdata', 'Data types of sites by themes');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (50, 'fr', 'name', 'arborescence/std', 'refdata', 'Types de données par thèmes de sites et projet');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (51, 'en', 'description', 'types_de_donnees_par_themes_de_sites_description', 'refdata', 'This reference data defines a tree that will be used to deposit data files on data types.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (52, 'fr', 'description', 'types_de_donnees_par_themes_de_sites_description', 'refdata', 'Cette données de référence permet de définir une arborescence qui sera utilisée pour déposer des fichiers de données sur des types de données.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (54, 'en', 'name', 'data_description/variable', 'refdata', 'Variables');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (55, 'fr', 'name', 'data_description/variable', 'refdata', 'Variables');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (56, 'en', 'description', 'variables_description', 'refdata', 'A variable defines a measurable object. It is expressed for a data type in a specific unit.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (57, 'fr', 'description', 'variables_description', 'refdata', 'Une variable définit un objet mesurable. Elle s''exprime pour un type de données dans une unité spécifique.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (59, 'en', 'name', 'data_description/dvu', 'refdata', 'Variables and units by data types');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (60, 'fr', 'name', 'data_description/dvu', 'refdata', 'Variables et unités par types de données');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (61, 'en', 'description', 'variables_et_unites_par_types_de_donnees_description', 'refdata', 'This reference datum defines which variables are associated with a data type, and in which unit it expresses itself.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (62, 'fr', 'description', 'variables_et_unites_par_types_de_donnees_description', 'refdata', 'Cette donnée de référence définit quelles sont les variables associées à un type de données, et en quelle unité elle s''exprime.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (64, 'en', 'name', 'pem/species', 'refdata', 'Species');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (65, 'fr', 'name', 'pem/species', 'refdata', 'Espèces');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (66, 'en', 'description', 'species_description', 'refdata', 'This reference data defines the studied species.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (67, 'fr', 'description', 'species_description', 'refdata', 'Cette donnée de référence définit les espèces étudiées.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (69, 'en', 'name', 'pem', 'refdata', 'Trapping in ascent');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (70, 'fr', 'name', 'pem', 'refdata', 'Piégeage en montéee');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (72, 'en', 'name', 'type_de_fichiers', 'refdata', 'Files'' type');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (73, 'fr', 'name', 'type_de_fichiers', 'refdata', 'Type de fichiers');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (74, 'en', 'description', 'type_de_fichiers_description', 'refdata', 'Used for filing additional files. Sets the list of additional file types.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (75, 'fr', 'description', 'type_de_fichiers_description', 'refdata', 'Utilisé pour le dépôt de fichiers complémentaires. Définit la liste des types de fichiers complémentaires.');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (126, 'en', 'name', 'piegeage_en_montee', 'datatype', 'Trapping in ascent');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (127, 'fr', 'name', 'piegeage_en_montee', 'datatype', 'Piégeage en montée');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (128, 'en', 'description', 'piegeage_en_montee_description', 'datatype', 'Data of trapping in ascent');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (129, 'fr', 'description', 'piegeage_en_montee_description', 'datatype', 'Données de piegeage en montée');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (133, 'fr', 'name', 'plateforme', 'types_zone_etude_tze', 'Plateforme');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (134, 'en', 'name', 'plateforme', 'types_zone_etude_tze', 'Platform');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (135, 'en', 'definition', 'Plateforme de mesure', 'types_zone_etude_tze', 'Measurement platform');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (136, 'fr', 'name', 'bassin_versant', 'types_zone_etude_tze', 'Bassin versant');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (137, 'en', 'name', 'bassin_versant', 'types_zone_etude_tze', 'Watershed');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (138, 'en', 'definition', 'Bassin versant', 'types_zone_etude_tze', 'Watershed');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (140, 'fr', 'name', 'projet_manche', 'projet', 'Projet manche');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (141, 'en', 'name', 'projet_manche', 'projet', 'Channel project');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (142, 'en', 'definition', 'Projet manche', 'projet', 'Channel project');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (143, 'en', 'name', 'Projet manche', 'projet', 'Channel project');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (145, 'fr', 'name', 'projet_atlantique', 'projet', 'Projet Atlantique');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (146, 'en', 'name', 'projet_atlantique', 'projet', 'Atlantic project');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (147, 'en', 'definition', 'Projet Atlantique', 'projet', 'Atlantic project');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (148, 'en', 'name', 'Projet Atlantique', 'projet', 'Atlantic project');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (151, 'fr', 'name', 'oir', 'sitemonsoere', 'Oir');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (152, 'en', 'name', 'oir', 'sitemonsoere', 'Oir');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (153, 'en', 'description', 'Bassin versant d''Oir', 'sitemonsoere', 'Oir catchment');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (154, 'en', 'name', 'Bassin versant d''Oir', 'sitemonsoere', 'Oir catchment');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (156, 'fr', 'name', 'p2', 'sitemonsoere', 'P2');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (157, 'en', 'name', 'p2', 'sitemonsoere', 'P2');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (158, 'en', 'description', '', 'sitemonsoere', '');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (159, 'en', 'name', '', 'sitemonsoere', '');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (161, 'fr', 'name', 'p1', 'sitemonsoere', 'P1');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (162, 'en', 'name', 'p1', 'sitemonsoere', 'P1');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (164, 'fr', 'name', 'a', 'sitemonsoere', 'A');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (165, 'en', 'name', 'a', 'sitemonsoere', 'A');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (167, 'fr', 'name', 'b', 'sitemonsoere', 'B');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (168, 'en', 'name', 'b', 'sitemonsoere', 'B');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (170, 'fr', 'name', 'nivelle', 'sitemonsoere', 'Nivelle');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (171, 'en', 'name', 'nivelle', 'sitemonsoere', 'Nivelle');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (172, 'en', 'description', 'Bassin versant de Nivelle', 'sitemonsoere', 'Watershed Nivelle');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (173, 'en', 'name', 'Bassin versant de Nivelle', 'sitemonsoere', 'Watershed Nivelle');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (175, 'fr', 'name', 'scarff', 'sitemonsoere', 'Scarff');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (176, 'en', 'name', 'scarff', 'sitemonsoere', 'Scarff');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (177, 'en', 'description', 'Bassin versant de Scarff', 'sitemonsoere', 'Watershed Scarff');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (178, 'en', 'name', 'Bassin versant de Scarff', 'sitemonsoere', 'Watershed Scarff');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (183, 'fr', 'name', 'donnees_biologiques', 'theme', 'Données biologiques');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (184, 'en', 'name', 'donnees_biologiques', 'theme', 'Biological data');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (185, 'en', 'description', 'Données biologiques', 'theme', 'Biological data');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (186, 'en', 'name', 'Données biologiques', 'theme', 'Biological data');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (359, 'fr', 'code', '*', 'unite', '*');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (360, 'en', 'code', '*', 'unite', '*');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (361, 'fr', 'name', 'sans_unite', 'unite', 'sans unite');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (362, 'en', 'name', 'sans_unite', 'unite', 'no unit');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (364, 'fr', 'name', 'nombre_d_individus', 'variablemonsoere', 'Nombre d''individus');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (365, 'en', 'name', 'nombre_d_individus', 'variablemonsoere', 'Number of individuals');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (366, 'en', 'definition', 'Nombre d''individus', 'variablemonsoere', 'Number of individuals');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (367, 'en', 'name', 'Nombre d''individus', 'variablemonsoere', 'Number of individuals');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (369, 'fr', 'name', 'couleur_des_individus', 'variablemonsoere', 'Couleur des individus');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (370, 'en', 'name', 'couleur_des_individus', 'variablemonsoere', 'color of individuals');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (371, 'en', 'definition', 'Couleur des individus', 'variablemonsoere', 'individuals'' colors');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (372, 'en', 'name', 'Couleur des individus', 'variablemonsoere', 'individuals'' colors');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (375, 'fr', 'nom', 'couleur_des_individus', 'valeur_qualitative', 'couleur des individus');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (376, 'en', 'nom', 'couleur_des_individus', 'valeur_qualitative', 'Color of individuals');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (377, 'fr', 'valeur', 'rouge', 'valeur_qualitative', 'rouge');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (378, 'en', 'valeur', 'rouge', 'valeur_qualitative', 'red');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (380, 'fr', 'valeur', 'bleu', 'valeur_qualitative', 'bleu');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (381, 'en', 'valeur', 'bleu', 'valeur_qualitative', 'blue');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (383, 'fr', 'valeur', 'vert', 'valeur_qualitative', 'vert');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (384, 'en', 'valeur', 'vert', 'valeur_qualitative', 'green');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (427, 'en', 'esp_definition', 'LPF', 'espece_esp', 'LPF');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (428, 'en', 'esp_definition', 'ANG', 'espece_esp', 'ANG');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (429, 'en', 'esp_definition', 'LPM', 'espece_esp', 'LPM');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (430, 'en', 'esp_definition', 'ALO', 'espece_esp', 'ALO');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (431, 'en', 'esp_definition', 'TRM', 'espece_esp', 'TRM');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (432, 'en', 'esp_definition', 'TRF', 'espece_esp', 'TRF');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (433, 'en', 'esp_definition', 'SAT', 'espece_esp', 'SAT');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (435, 'fr', 'nom', 'protocole', 'filetype', 'protocole');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (436, 'en', 'nom', 'protocole', 'filetype', 'protocol');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (437, 'en', 'description', 'protocole d''expérimentation', 'filetype', 'Experimental protocol');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (438, 'fr', 'nom', 'données brutes', 'filetype', 'données brutes');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (439, 'en', 'nom', 'données brutes', 'filetype', 'raw data');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (440, 'en', 'description', 'fichier de données brutes', 'filetype', 'Raw data file');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (441, 'fr', 'nom', 'licence', 'filetype', 'licence');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (442, 'en', 'nom', 'licence', 'filetype', 'licence');
INSERT INTO public.localisation (id, localization, colonne, defaultstring, entite, localestring) VALUES (443, 'en', 'description', 'licence des données', 'filetype', 'Licensing of data');


--
-- TOC entry 3340 (class 0 OID 377413)
-- Dependencies: 222
-- Data for Name: mesure_pem; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (1, 1, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (2, 2, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (3, 3, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (4, 4, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (5, 5, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (6, 6, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (7, 7, 1);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (8, 1, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (9, 2, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (10, 3, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (11, 4, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (12, 5, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (13, 6, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (14, 7, 2);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (15, 1, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (16, 2, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (17, 3, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (18, 4, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (19, 5, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (20, 6, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (21, 7, 3);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (22, 1, 4);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (23, 2, 4);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (24, 3, 4);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (25, 4, 4);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (26, 5, 4);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (27, 6, 4);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (28, 1, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (29, 2, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (30, 3, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (31, 4, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (32, 5, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (33, 6, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (34, 7, 5);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (35, 1, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (36, 2, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (37, 3, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (38, 4, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (39, 5, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (40, 6, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (41, 7, 6);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (42, 1, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (43, 2, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (44, 3, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (45, 4, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (46, 5, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (47, 6, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (48, 7, 7);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (49, 1, 8);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (50, 2, 8);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (51, 3, 8);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (52, 4, 8);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (53, 5, 8);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (54, 6, 8);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (55, 1, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (56, 2, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (57, 3, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (58, 4, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (59, 5, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (60, 6, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (61, 7, 9);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (62, 1, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (63, 2, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (64, 3, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (65, 4, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (66, 5, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (67, 6, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (68, 7, 10);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (69, 1, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (70, 2, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (71, 3, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (72, 4, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (73, 5, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (74, 6, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (75, 7, 11);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (76, 1, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (77, 2, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (78, 3, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (79, 4, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (80, 5, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (81, 6, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (82, 7, 12);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (83, 1, 13);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (84, 2, 13);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (85, 3, 13);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (86, 4, 13);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (87, 5, 13);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (88, 6, 13);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (89, 1, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (90, 2, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (91, 3, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (92, 4, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (93, 5, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (94, 6, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (95, 7, 14);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (96, 1, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (97, 2, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (98, 3, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (99, 4, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (100, 5, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (101, 6, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (102, 7, 15);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (103, 1, 16);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (104, 2, 16);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (105, 3, 16);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (106, 4, 16);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (107, 5, 16);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (108, 6, 16);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (109, 1, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (110, 2, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (111, 3, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (112, 4, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (113, 5, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (114, 6, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (115, 7, 17);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (116, 1, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (117, 2, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (118, 3, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (119, 4, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (120, 5, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (121, 6, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (122, 7, 18);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (123, 1, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (124, 2, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (125, 3, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (126, 4, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (127, 5, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (128, 6, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (129, 7, 19);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (130, 1, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (131, 2, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (132, 3, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (133, 4, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (134, 5, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (135, 6, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (136, 7, 20);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (137, 1, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (138, 2, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (139, 3, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (140, 4, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (141, 5, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (142, 6, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (143, 7, 21);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (144, 1, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (145, 2, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (146, 3, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (147, 4, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (148, 5, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (149, 6, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (150, 7, 22);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (151, 1, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (152, 2, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (153, 3, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (154, 4, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (155, 5, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (156, 6, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (157, 7, 23);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (158, 1, 24);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (159, 2, 24);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (160, 3, 24);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (161, 4, 24);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (162, 5, 24);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (163, 6, 24);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (164, 1, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (165, 2, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (166, 3, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (167, 4, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (168, 5, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (169, 6, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (170, 7, 25);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (171, 4, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (172, 2, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (173, 1, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (174, 3, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (175, 7, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (176, 6, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (177, 5, 26);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (178, 4, 27);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (179, 2, 27);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (180, 1, 27);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (181, 3, 27);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (182, 6, 27);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (183, 5, 27);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (184, 4, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (185, 2, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (186, 1, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (187, 3, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (188, 7, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (189, 6, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (190, 5, 28);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (191, 4, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (192, 2, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (193, 1, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (194, 3, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (195, 7, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (196, 6, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (197, 5, 29);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (198, 4, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (199, 2, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (200, 1, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (201, 3, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (202, 7, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (203, 6, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (204, 5, 30);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (205, 1, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (206, 2, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (207, 3, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (208, 4, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (209, 5, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (210, 6, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (211, 7, 31);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (212, 1, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (213, 2, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (214, 3, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (215, 4, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (216, 5, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (217, 6, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (218, 7, 32);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (219, 1, 33);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (220, 2, 33);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (221, 3, 33);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (222, 4, 33);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (223, 5, 33);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (224, 6, 33);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (225, 1, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (226, 2, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (227, 3, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (228, 4, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (229, 5, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (230, 6, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (231, 7, 34);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (232, 1, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (233, 2, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (234, 3, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (235, 4, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (236, 5, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (237, 6, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (238, 7, 35);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (239, 1, 36);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (240, 2, 36);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (241, 3, 36);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (242, 4, 36);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (243, 5, 36);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (244, 6, 36);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (245, 1, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (246, 2, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (247, 3, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (248, 4, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (249, 5, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (250, 6, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (251, 7, 37);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (252, 1, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (253, 2, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (254, 3, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (255, 4, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (256, 5, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (257, 6, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (258, 7, 38);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (259, 1, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (260, 2, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (261, 3, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (262, 4, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (263, 5, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (264, 6, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (265, 7, 39);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (266, 1, 40);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (267, 2, 40);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (268, 3, 40);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (269, 4, 40);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (270, 5, 40);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (271, 6, 40);
INSERT INTO public.mesure_pem (mpem_id, esp_id, spem_id) VALUES (272, 7, 40);


--
-- TOC entry 3341 (class 0 OID 377419)
-- Dependencies: 223
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (445, false, NULL, '<p>Le nom du fichier est invalide. Il doit être de la forme :</p><p>projet_atlantique_nivelle-p1_piegeage_en_montee_dd-MM-yyyy_dd-MM-yyyy.csv</p>', '2020-08-31 16:44:42.134', 'error', 'Le fichier de données "projet_manche_nivelle-p1_piegeage_en_montee_01-01-1984_05-01-1984.csv" n''est pas valide.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (447, false, NULL, '', '2020-08-31 16:44:57.034', 'info', 'Le dépôt du fichier ''projet_atlantique_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (449, false, NULL, '', '2020-08-31 16:45:03.116', 'info', 'La révision "2020-08-31T16:44:57.041" du fichier "projet_atlantique_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (450, false, NULL, '', '2020-08-31 16:45:03.729', 'info', 'La révision "2020-08-31T16:44:57.041" du fichier "projet_atlantique_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (452, false, NULL, '', '2020-08-31 16:45:24.4', 'info', 'Le dépôt du fichier ''projet_atlantique_a_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (454, false, NULL, '', '2020-08-31 16:45:29.633', 'info', 'La révision "2020-08-31T16:45:24.435" du fichier "projet_atlantique_a_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (455, false, NULL, '', '2020-08-31 16:45:30.142', 'info', 'La révision "2020-08-31T16:45:24.435" du fichier "projet_atlantique_a_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (457, false, NULL, '', '2020-08-31 16:45:41.083', 'info', 'Le dépôt du fichier ''projet_atlantique_p2_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (459, false, NULL, '', '2020-08-31 16:45:45.316', 'info', 'La révision "2020-08-31T16:45:41.117" du fichier "projet_atlantique_p2_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (460, false, NULL, '', '2020-08-31 16:45:45.816', 'info', 'La révision "2020-08-31T16:45:41.117" du fichier "projet_atlantique_p2_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (462, false, NULL, '', '2020-08-31 16:45:53.331', 'info', 'Le dépôt du fichier ''projet_atlantique_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (464, false, NULL, '', '2020-08-31 16:45:56.438', 'info', 'La révision "2020-08-31T16:45:53.365" du fichier "projet_atlantique_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (465, false, NULL, '', '2020-08-31 16:45:56.831', 'info', 'La révision "2020-08-31T16:45:53.365" du fichier "projet_atlantique_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (467, false, NULL, '', '2020-08-31 16:46:07.919', 'info', 'Le dépôt du fichier ''projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (469, false, NULL, '', '2020-08-31 16:46:13.251', 'info', 'La révision "2020-08-31T16:46:07.954" du fichier "projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (470, false, NULL, '', '2020-08-31 16:46:13.505', 'info', 'La révision "2020-08-31T16:46:07.954" du fichier "projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (472, false, NULL, '', '2020-08-31 16:46:28.461', 'info', 'Le dépôt du fichier ''projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (474, false, NULL, '', '2020-08-31 16:46:32.791', 'info', 'La révision "2020-08-31T16:46:28.497" du fichier "projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (475, false, NULL, '', '2020-08-31 16:46:33.164', 'info', 'La révision "2020-08-31T16:46:28.497" du fichier "projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (477, false, NULL, '', '2020-08-31 16:46:39.279', 'info', 'Le dépôt du fichier ''projet_manche_p2_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (479, false, NULL, '', '2020-08-31 16:46:42.941', 'info', 'La révision "2020-08-31T16:46:39.313" du fichier "projet_manche_p2_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (480, false, NULL, '', '2020-08-31 16:46:43.235', 'info', 'La révision "2020-08-31T16:46:39.313" du fichier "projet_manche_p2_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (482, false, NULL, '', '2020-08-31 16:46:53.039', 'info', 'Le dépôt du fichier ''projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv'' a été effectué avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (484, false, NULL, '', '2020-08-31 16:46:58.089', 'info', 'La révision "2020-08-31T16:46:53.044" du fichier "projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" est en cours de publication', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (485, false, NULL, '', '2020-08-31 16:46:58.331', 'info', 'La révision "2020-08-31T16:46:53.044" du fichier "projet_manche_p1_piegeage_en_montee_01-01-1984_05-01-1984#V1#.csv" a été publiée en 00:00 s.', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (486, false, NULL, '', '2020-08-31 16:47:06.555', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (527, false, NULL, '', '2020-08-31 16:47:06.699', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (528, false, NULL, '', '2020-08-31 16:47:12.71', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (569, false, NULL, '', '2020-08-31 16:47:12.778', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (570, false, NULL, '', '2020-08-31 16:47:22.817', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (611, false, NULL, '', '2020-08-31 16:47:22.854', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (612, false, NULL, '', '2020-08-31 16:56:25.587', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (613, false, NULL, '', '2020-08-31 16:56:25.738', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (614, false, NULL, '', '2020-08-31 17:02:16.225', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (615, false, NULL, '', '2020-08-31 17:02:16.448', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (616, false, NULL, '', '2020-08-31 17:05:19.933', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (665, false, NULL, '', '2020-08-31 17:05:20.101', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (666, false, NULL, '', '2020-08-31 17:06:14.628', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (715, false, NULL, '', '2020-08-31 17:06:48.91', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (716, false, NULL, '', '2020-08-31 17:08:09.352', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (765, false, NULL, '', '2020-08-31 17:08:09.416', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (766, false, NULL, '', '2020-08-31 17:08:13.528', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (815, false, NULL, '', '2020-08-31 17:08:13.584', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (816, false, NULL, '', '2020-08-31 17:08:39.028', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (865, false, NULL, '', '2020-08-31 17:55:06.9', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (914, false, NULL, '', '2020-08-31 17:55:07.035', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (915, false, NULL, '', '2020-08-31 17:55:17.258', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (964, false, NULL, '', '2020-08-31 17:56:38.631', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (965, false, NULL, '', '2020-08-31 17:56:40.208', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (1014, false, NULL, '', '2020-08-31 18:06:39.238', 'info', 'Génération des données de synthèse terminée avec succès', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (1015, false, NULL, '', '2020-08-31 18:22:23.373', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (1056, false, NULL, '', '2020-09-01 09:28:24.617', 'info', 'Génération des données de synthèse en cours...', 2);
INSERT INTO public.notification (id, archived, attachment, body, date, level, message, utilisateur_id) VALUES (1105, false, NULL, '', '2020-09-01 09:28:24.814', 'info', 'Génération des données de synthèse terminée avec succès', 2);


--
-- TOC entry 3342 (class 0 OID 377427)
-- Dependencies: 224
-- Data for Name: pemsynthesisdatatype; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1097, '235', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_atlantique,nivelle/p1');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1098, '254', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_atlantique,oir/p1/a');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1099, '287', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_atlantique,oir/p2');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1100, '301', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_atlantique,scarff/p1');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1101, '315', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_manche,nivelle/p1');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1102, '329', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_manche,oir/p1');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1103, '343', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_manche,oir/p2');
INSERT INTO public.pemsynthesisdatatype (id, idnodes, maxdate, mindate, site) VALUES (1104, '357', '1984-01-05 00:00:00', '1984-01-01 00:00:00', 'projet_manche,scarff/p1');


--
-- TOC entry 3343 (class 0 OID 377435)
-- Dependencies: 225
-- Data for Name: pemsynthesisvalue; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1057, '1984-01-01 00:00:00', 391, false, 'projet_atlantique,nivelle/p1', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1058, '1984-01-02 00:00:00', 391, false, 'projet_atlantique,nivelle/p1', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1059, '1984-01-03 00:00:00', 391, false, 'projet_atlantique,nivelle/p1', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1060, '1984-01-04 00:00:00', 391, false, 'projet_atlantique,nivelle/p1', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1061, '1984-01-05 00:00:00', 391, false, 'projet_atlantique,nivelle/p1', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1062, '1984-01-01 00:00:00', 395, false, 'projet_atlantique,oir/p1/a', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1063, '1984-01-02 00:00:00', 395, false, 'projet_atlantique,oir/p1/a', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1064, '1984-01-03 00:00:00', 395, false, 'projet_atlantique,oir/p1/a', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1065, '1984-01-04 00:00:00', 395, false, 'projet_atlantique,oir/p1/a', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1066, '1984-01-05 00:00:00', 395, false, 'projet_atlantique,oir/p1/a', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1067, '1984-01-01 00:00:00', 403, false, 'projet_atlantique,oir/p2', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1068, '1984-01-02 00:00:00', 403, false, 'projet_atlantique,oir/p2', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1069, '1984-01-03 00:00:00', 403, false, 'projet_atlantique,oir/p2', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1070, '1984-01-04 00:00:00', 403, false, 'projet_atlantique,oir/p2', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1071, '1984-01-05 00:00:00', 403, false, 'projet_atlantique,oir/p2', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1072, '1984-01-01 00:00:00', 407, false, 'projet_atlantique,scarff/p1', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1073, '1984-01-02 00:00:00', 407, false, 'projet_atlantique,scarff/p1', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1074, '1984-01-03 00:00:00', 407, false, 'projet_atlantique,scarff/p1', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1075, '1984-01-04 00:00:00', 407, false, 'projet_atlantique,scarff/p1', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1076, '1984-01-05 00:00:00', 407, false, 'projet_atlantique,scarff/p1', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1077, '1984-01-01 00:00:00', 411, false, 'projet_manche,nivelle/p1', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1078, '1984-01-02 00:00:00', 411, false, 'projet_manche,nivelle/p1', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1079, '1984-01-03 00:00:00', 411, false, 'projet_manche,nivelle/p1', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1080, '1984-01-04 00:00:00', 411, false, 'projet_manche,nivelle/p1', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1081, '1984-01-05 00:00:00', 411, false, 'projet_manche,nivelle/p1', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1082, '1984-01-01 00:00:00', 415, false, 'projet_manche,oir/p1', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1083, '1984-01-02 00:00:00', 415, false, 'projet_manche,oir/p1', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1084, '1984-01-03 00:00:00', 415, false, 'projet_manche,oir/p1', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1085, '1984-01-04 00:00:00', 415, false, 'projet_manche,oir/p1', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1086, '1984-01-05 00:00:00', 415, false, 'projet_manche,oir/p1', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1087, '1984-01-01 00:00:00', 419, false, 'projet_manche,oir/p2', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1088, '1984-01-02 00:00:00', 419, false, 'projet_manche,oir/p2', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1089, '1984-01-03 00:00:00', 419, false, 'projet_manche,oir/p2', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1090, '1984-01-04 00:00:00', 419, false, 'projet_manche,oir/p2', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1091, '1984-01-05 00:00:00', 419, false, 'projet_manche,oir/p2', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1092, '1984-01-01 00:00:00', 423, false, 'projet_manche,scarff/p1', 30.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1093, '1984-01-02 00:00:00', 423, false, 'projet_manche,scarff/p1', 25.571428, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1094, '1984-01-03 00:00:00', 423, false, 'projet_manche,scarff/p1', 28.166666, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1095, '1984-01-04 00:00:00', 423, false, 'projet_manche,scarff/p1', 28.857143, NULL, 'piegeage_en_montee_nombre_d_individus_*');
INSERT INTO public.pemsynthesisvalue (id, date, idnode, ismean, site, valuefloat, valuestring, variable) VALUES (1096, '1984-01-05 00:00:00', 423, false, 'projet_manche,scarff/p1', 31.714285, NULL, 'piegeage_en_montee_nombre_d_individus_*');


--
-- TOC entry 3344 (class 0 OID 377443)
-- Dependencies: 226
-- Data for Name: projet; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.projet (definition, id) VALUES ('Projet manche', 144);
INSERT INTO public.projet (definition, id) VALUES ('Projet Atlantique', 149);


--
-- TOC entry 3345 (class 0 OID 377451)
-- Dependencies: 227
-- Data for Name: realnode; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (77, 'data_description', NULL, 33, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (78, 'data_description/unit', 77, 30, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (82, 'arborescence', NULL, 15, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (83, 'arborescence/site', 82, 25, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (87, 'arborescence/std', 82, 53, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (91, 'type_de_fichiers', NULL, 76, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (93, 'arborescence/projet', 82, 20, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (97, 'data_description/dvu', 77, 63, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (101, 'arborescence/types_de_sites', 82, 12, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (105, 'data_description/qualitatives_values', 77, 43, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (109, 'arborescence/datatype', 82, 48, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (113, 'arborescence/thematic', 82, 38, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (117, 'pem', NULL, 71, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (118, 'pem/species', 117, 68, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (122, 'data_description/variable', 77, 58, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (189, 'projet_atlantique', NULL, 149, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (190, 'nivelle', NULL, 174, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (191, 'projet_atlantique,nivelle/p1', 190, 180, 189);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (192, 'projet_atlantique,nivelle/p1,donnees_biologiques', NULL, 187, 191);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (193, 'projet_atlantique,nivelle/p1,donnees_biologiques,piegeage_en_montee', NULL, 130, 192);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (194, 'oir', NULL, 155, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (195, 'oir/p1', 194, 163, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (196, 'projet_atlantique,oir/p1/a', 195, 166, 189);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (197, 'projet_atlantique,oir/p1/a,donnees_biologiques', NULL, 187, 196);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (198, 'projet_atlantique,oir/p1/a,donnees_biologiques,piegeage_en_montee', NULL, 130, 197);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (199, 'projet_atlantique,oir/p1/b', 195, 169, 189);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (200, 'projet_atlantique,oir/p1/b,donnees_biologiques', NULL, 187, 199);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (201, 'projet_atlantique,oir/p1/b,donnees_biologiques,piegeage_en_montee', NULL, 130, 200);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (202, 'projet_atlantique,oir/p2', 194, 160, 189);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (203, 'projet_atlantique,oir/p2,donnees_biologiques', NULL, 187, 202);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (204, 'projet_atlantique,oir/p2,donnees_biologiques,piegeage_en_montee', NULL, 130, 203);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (205, 'scarff', NULL, 179, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (206, 'projet_atlantique,scarff/p1', 205, 181, 189);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (207, 'projet_atlantique,scarff/p1,donnees_biologiques', NULL, 187, 206);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (208, 'projet_atlantique,scarff/p1,donnees_biologiques,piegeage_en_montee', NULL, 130, 207);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (209, 'projet_manche', NULL, 144, NULL);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (210, 'projet_manche,nivelle/p1', 190, 180, 209);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (211, 'projet_manche,nivelle/p1,donnees_biologiques', NULL, 187, 210);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (212, 'projet_manche,nivelle/p1,donnees_biologiques,piegeage_en_montee', NULL, 130, 211);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (213, 'projet_manche,oir/p1', 194, 163, 209);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (214, 'projet_manche,oir/p1,donnees_biologiques', NULL, 187, 213);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (215, 'projet_manche,oir/p1,donnees_biologiques,piegeage_en_montee', NULL, 130, 214);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (216, 'projet_manche,oir/p2', 194, 160, 209);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (217, 'projet_manche,oir/p2,donnees_biologiques', NULL, 187, 216);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (218, 'projet_manche,oir/p2,donnees_biologiques,piegeage_en_montee', NULL, 130, 217);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (219, 'projet_manche,scarff/p1', 205, 181, 209);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (220, 'projet_manche,scarff/p1,donnees_biologiques', NULL, 187, 219);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (221, 'projet_manche,scarff/p1,donnees_biologiques,piegeage_en_montee', NULL, 130, 220);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (390, 'projet_atlantique,nivelle/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 193);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (392, 'projet_atlantique,nivelle/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 193);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (394, 'projet_atlantique,oir/p1/a,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 198);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (396, 'projet_atlantique,oir/p1/a,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 198);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (398, 'projet_atlantique,oir/p1/b,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 201);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (400, 'projet_atlantique,oir/p1/b,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 201);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (402, 'projet_atlantique,oir/p2,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 204);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (404, 'projet_atlantique,oir/p2,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 204);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (406, 'projet_atlantique,scarff/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 208);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (408, 'projet_atlantique,scarff/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 208);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (410, 'projet_manche,nivelle/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 212);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (412, 'projet_manche,nivelle/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 212);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (414, 'projet_manche,oir/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 215);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (416, 'projet_manche,oir/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 215);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (418, 'projet_manche,oir/p2,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 218);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (420, 'projet_manche,oir/p2,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 218);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (422, 'projet_manche,scarff/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_nombre_d_individus_*', NULL, 388, 221);
INSERT INTO public.realnode (id, path, id_ancestor, id_nodeable, id_parent_node) VALUES (424, 'projet_manche,scarff/p1,donnees_biologiques,piegeage_en_montee;piegeage_en_montee_couleur_des_individus_*', NULL, 389, 221);


--
-- TOC entry 3346 (class 0 OID 377456)
-- Dependencies: 228
-- Data for Name: refdata; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.refdata (description, id) VALUES ('type_site_description', 12);
INSERT INTO public.refdata (description, id) VALUES (NULL, 15);
INSERT INTO public.refdata (description, id) VALUES ('projet_description', 20);
INSERT INTO public.refdata (description, id) VALUES ('site_description', 25);
INSERT INTO public.refdata (description, id) VALUES ('unites_description', 30);
INSERT INTO public.refdata (description, id) VALUES (NULL, 33);
INSERT INTO public.refdata (description, id) VALUES ('themes_description', 38);
INSERT INTO public.refdata (description, id) VALUES ('vq_description', 43);
INSERT INTO public.refdata (description, id) VALUES ('datatype_description', 48);
INSERT INTO public.refdata (description, id) VALUES ('types_de_donnees_par_themes_de_sites_description', 53);
INSERT INTO public.refdata (description, id) VALUES ('variables_description', 58);
INSERT INTO public.refdata (description, id) VALUES ('variables_et_unites_par_types_de_donnees_description', 63);
INSERT INTO public.refdata (description, id) VALUES ('species_description', 68);
INSERT INTO public.refdata (description, id) VALUES (NULL, 71);
INSERT INTO public.refdata (description, id) VALUES ('type_de_fichiers_description', 76);


--
-- TOC entry 3347 (class 0 OID 377461)
-- Dependencies: 229
-- Data for Name: requete; Type: TABLE DATA; Schema: public; Owner: kerneluser
--



--
-- TOC entry 3348 (class 0 OID 377469)
-- Dependencies: 230
-- Data for Name: rightrequest; Type: TABLE DATA; Schema: public; Owner: kerneluser
--



--
-- TOC entry 3350 (class 0 OID 377479)
-- Dependencies: 232
-- Data for Name: sequencepem; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (1, '1984-01-05', 448);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (2, '1984-01-01', 448);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (3, '1984-01-02', 448);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (4, '1984-01-03', 448);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (5, '1984-01-04', 448);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (6, '1984-01-01', 453);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (7, '1984-01-02', 453);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (8, '1984-01-03', 453);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (9, '1984-01-04', 453);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (10, '1984-01-05', 453);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (11, '1984-01-05', 458);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (12, '1984-01-02', 458);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (13, '1984-01-03', 458);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (14, '1984-01-01', 458);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (15, '1984-01-04', 458);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (16, '1984-01-03', 463);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (17, '1984-01-02', 463);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (18, '1984-01-01', 463);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (19, '1984-01-04', 463);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (20, '1984-01-05', 463);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (21, '1984-01-05', 468);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (22, '1984-01-01', 468);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (23, '1984-01-02', 468);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (24, '1984-01-03', 468);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (25, '1984-01-04', 468);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (26, '1984-01-01', 473);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (27, '1984-01-03', 473);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (28, '1984-01-04', 473);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (29, '1984-01-02', 473);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (30, '1984-01-05', 473);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (31, '1984-01-05', 478);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (32, '1984-01-02', 478);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (33, '1984-01-03', 478);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (34, '1984-01-01', 478);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (35, '1984-01-04', 478);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (36, '1984-01-03', 483);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (37, '1984-01-02', 483);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (38, '1984-01-01', 483);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (39, '1984-01-04', 483);
INSERT INTO public.sequencepem (spem_id, date, ivf_id) VALUES (40, '1984-01-05', 483);


--
-- TOC entry 3351 (class 0 OID 377485)
-- Dependencies: 233
-- Data for Name: site; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('Bassin versant d''Oir', 'oir', 155, NULL);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('', 'p2', 160, 155);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('', 'p1', 163, 155);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('', 'a', 166, 163);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('', 'b', 169, 163);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('Bassin versant de Nivelle', 'nivelle', 174, NULL);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('Bassin versant de Scarff', 'scarff', 179, NULL);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('', 'p1', 180, 174);
INSERT INTO public.site (description, name, site_id, parent_site_id) VALUES ('', 'p1', 181, 179);


--
-- TOC entry 3352 (class 0 OID 377493)
-- Dependencies: 234
-- Data for Name: theme; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.theme (description, id) VALUES ('Données biologiques', 187);


--
-- TOC entry 3354 (class 0 OID 377503)
-- Dependencies: 236
-- Data for Name: types_zone_etude_tze; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.types_zone_etude_tze (tze_id, tze_code, tze_definition) VALUES (1, 'plateforme', 'Plateforme de mesure');
INSERT INTO public.types_zone_etude_tze (tze_id, tze_code, tze_definition) VALUES (2, 'bassin_versant', 'Bassin versant');


--
-- TOC entry 3355 (class 0 OID 377509)
-- Dependencies: 237
-- Data for Name: unite; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.unite (uni_id, code, name) VALUES (1, '*', 'sans_unite');


--
-- TOC entry 3356 (class 0 OID 377517)
-- Dependencies: 238
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.utilisateur (id, active, email, emploi, isroot, language, login, nom, password, poste, prenom) VALUES (2, true, 'admin.admin@inra.fr', NULL, true, 'fr', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 'admin');
INSERT INTO public.utilisateur (id, active, email, emploi, isroot, language, login, nom, password, poste, prenom) VALUES (5, true, 'anonymous.anonymous@inra.fr', NULL, true, 'fr', 'anonymous', 'anonymous', 'a743894a0e4a801fc321232f297a57a5', NULL, 'anonymous');


--
-- TOC entry 3358 (class 0 OID 377527)
-- Dependencies: 240
-- Data for Name: valeur_pem; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (1, NULL, 1, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (2, 59, 1, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (3, NULL, 2, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (4, 27, 2, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (5, NULL, 3, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (6, 49, 3, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (7, NULL, 4, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (8, 17, 4, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (9, NULL, 5, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (10, 25, 5, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (11, NULL, 6, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (12, 21, 6, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (13, NULL, 7, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (14, 24, 7, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (15, NULL, 8, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (16, 54, 8, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (17, NULL, 9, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (18, 38, 9, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (19, NULL, 10, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (20, 27, 10, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (21, NULL, 11, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (22, 15, 11, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (23, NULL, 12, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (24, 25, 12, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (25, NULL, 13, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (26, 18, 13, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (27, NULL, 14, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (28, 39, 14, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (29, NULL, 15, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (30, 50, 15, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (31, NULL, 16, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (32, 25, 16, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (33, NULL, 17, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (34, 32, 17, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (35, NULL, 18, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (36, 18, 18, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (37, NULL, 19, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (38, 25, 19, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (39, NULL, 20, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (40, 14, 20, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (41, NULL, 21, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (42, 15, 21, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (43, NULL, 22, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (44, 45, 22, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (45, NULL, 23, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (46, 20, 23, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (47, NULL, 24, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (48, 41, 24, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (49, NULL, 25, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (50, 16, 25, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (51, NULL, 26, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (52, 27, 26, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (53, NULL, 27, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (54, 20, 27, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (55, NULL, 28, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (56, 51, 28, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (57, NULL, 29, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (58, 22, 29, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (59, NULL, 30, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (60, 43, 30, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (61, NULL, 31, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (62, 15, 31, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (63, NULL, 32, 392, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (64, 27, 32, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (65, NULL, 33, 392, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (66, 20, 33, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (67, NULL, 34, 392, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (68, 24, 34, 390, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (69, NULL, 35, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (70, 54, 35, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (71, NULL, 36, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (72, 38, 36, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (73, NULL, 37, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (74, 27, 37, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (75, NULL, 38, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (76, 15, 38, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (77, NULL, 39, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (78, 25, 39, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (79, NULL, 40, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (80, 18, 40, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (81, NULL, 41, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (82, 39, 41, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (83, NULL, 42, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (84, 50, 42, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (85, NULL, 43, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (86, 25, 43, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (87, NULL, 44, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (88, 32, 44, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (89, NULL, 45, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (90, 18, 45, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (91, NULL, 46, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (92, 25, 46, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (93, NULL, 47, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (94, 14, 47, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (95, NULL, 48, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (96, 15, 48, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (97, NULL, 49, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (98, 45, 49, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (99, NULL, 50, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (100, 20, 50, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (101, NULL, 51, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (102, 41, 51, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (103, NULL, 52, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (104, 16, 52, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (105, NULL, 53, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (106, 27, 53, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (107, NULL, 54, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (108, 20, 54, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (109, NULL, 55, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (110, 51, 55, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (111, NULL, 56, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (112, 22, 56, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (113, NULL, 57, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (114, 43, 57, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (115, NULL, 58, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (116, 15, 58, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (117, NULL, 59, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (118, 27, 59, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (119, NULL, 60, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (120, 20, 60, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (121, NULL, 61, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (122, 24, 61, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (123, NULL, 62, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (124, 59, 62, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (125, NULL, 63, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (126, 27, 63, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (127, NULL, 64, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (128, 49, 64, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (129, NULL, 65, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (130, 17, 65, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (131, NULL, 66, 396, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (132, 25, 66, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (133, NULL, 67, 396, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (134, 21, 67, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (135, NULL, 68, 396, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (136, 24, 68, 394, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (137, NULL, 69, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (138, 59, 69, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (139, NULL, 70, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (140, 27, 70, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (141, NULL, 71, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (142, 49, 71, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (143, NULL, 72, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (144, 17, 72, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (145, NULL, 73, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (146, 25, 73, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (147, NULL, 74, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (148, 21, 74, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (149, NULL, 75, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (150, 24, 75, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (151, NULL, 76, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (152, 50, 76, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (153, NULL, 77, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (154, 25, 77, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (155, NULL, 78, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (156, 32, 78, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (157, NULL, 79, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (158, 18, 79, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (159, NULL, 80, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (160, 25, 80, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (161, NULL, 81, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (162, 14, 81, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (163, NULL, 82, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (164, 15, 82, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (165, NULL, 83, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (166, 45, 83, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (167, NULL, 84, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (168, 20, 84, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (169, NULL, 85, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (170, 41, 85, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (171, NULL, 86, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (172, 16, 86, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (173, NULL, 87, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (174, 27, 87, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (175, NULL, 88, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (176, 20, 88, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (177, NULL, 89, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (178, 54, 89, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (179, NULL, 90, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (180, 38, 90, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (181, NULL, 91, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (182, 27, 91, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (183, NULL, 92, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (184, 15, 92, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (185, NULL, 93, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (186, 25, 93, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (187, NULL, 94, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (188, 18, 94, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (189, NULL, 95, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (190, 39, 95, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (191, NULL, 96, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (192, 51, 96, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (193, NULL, 97, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (194, 22, 97, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (195, NULL, 98, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (196, 43, 98, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (197, NULL, 99, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (198, 15, 99, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (199, NULL, 100, 404, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (200, 27, 100, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (201, NULL, 101, 404, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (202, 20, 101, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (203, NULL, 102, 404, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (204, 24, 102, 402, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (205, NULL, 103, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (206, 45, 103, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (207, NULL, 104, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (208, 20, 104, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (209, NULL, 105, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (210, 41, 105, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (211, NULL, 106, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (212, 16, 106, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (213, NULL, 107, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (214, 27, 107, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (215, NULL, 108, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (216, 20, 108, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (217, NULL, 109, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (218, 50, 109, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (219, NULL, 110, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (220, 25, 110, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (221, NULL, 111, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (222, 32, 111, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (223, NULL, 112, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (224, 18, 112, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (225, NULL, 113, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (226, 25, 113, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (227, NULL, 114, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (228, 14, 114, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (229, NULL, 115, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (230, 15, 115, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (231, NULL, 116, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (232, 54, 116, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (233, NULL, 117, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (234, 38, 117, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (235, NULL, 118, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (236, 27, 118, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (237, NULL, 119, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (238, 15, 119, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (239, NULL, 120, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (240, 25, 120, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (241, NULL, 121, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (242, 18, 121, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (243, NULL, 122, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (244, 39, 122, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (245, NULL, 123, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (246, 51, 123, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (247, NULL, 124, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (248, 22, 124, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (249, NULL, 125, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (250, 43, 125, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (251, NULL, 126, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (252, 15, 126, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (253, NULL, 127, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (254, 27, 127, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (255, NULL, 128, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (256, 20, 128, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (257, NULL, 129, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (258, 24, 129, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (259, NULL, 130, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (260, 59, 130, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (261, NULL, 131, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (262, 27, 131, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (263, NULL, 132, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (264, 49, 132, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (265, NULL, 133, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (266, 17, 133, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (267, NULL, 134, 408, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (268, 25, 134, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (269, NULL, 135, 408, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (270, 21, 135, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (271, NULL, 136, 408, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (272, 24, 136, 406, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (273, NULL, 137, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (274, 59, 137, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (275, NULL, 138, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (276, 27, 138, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (277, NULL, 139, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (278, 49, 139, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (279, NULL, 140, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (280, 17, 140, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (281, NULL, 141, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (282, 25, 141, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (283, NULL, 142, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (284, 21, 142, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (285, NULL, 143, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (286, 24, 143, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (287, NULL, 144, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (288, 54, 144, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (289, NULL, 145, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (290, 38, 145, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (291, NULL, 146, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (292, 27, 146, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (293, NULL, 147, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (294, 15, 147, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (295, NULL, 148, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (296, 25, 148, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (297, NULL, 149, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (298, 18, 149, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (299, NULL, 150, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (300, 39, 150, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (301, NULL, 151, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (302, 50, 151, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (303, NULL, 152, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (304, 25, 152, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (305, NULL, 153, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (306, 32, 153, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (307, NULL, 154, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (308, 18, 154, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (309, NULL, 155, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (310, 25, 155, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (311, NULL, 156, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (312, 14, 156, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (313, NULL, 157, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (314, 15, 157, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (315, NULL, 158, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (316, 45, 158, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (317, NULL, 159, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (318, 20, 159, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (319, NULL, 160, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (320, 41, 160, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (321, NULL, 161, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (322, 16, 161, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (323, NULL, 162, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (324, 27, 162, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (325, NULL, 163, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (326, 20, 163, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (327, NULL, 164, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (328, 51, 164, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (329, NULL, 165, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (330, 22, 165, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (331, NULL, 166, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (332, 43, 166, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (333, NULL, 167, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (334, 15, 167, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (335, NULL, 168, 412, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (336, 27, 168, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (337, NULL, 169, 412, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (338, 20, 169, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (339, NULL, 170, 412, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (340, 24, 170, 410, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (341, NULL, 171, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (342, 15, 171, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (343, NULL, 172, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (344, 38, 172, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (345, NULL, 173, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (346, 54, 173, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (347, NULL, 174, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (348, 27, 174, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (349, NULL, 175, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (350, 39, 175, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (351, NULL, 176, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (352, 18, 176, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (353, NULL, 177, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (354, 25, 177, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (355, NULL, 178, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (356, 16, 178, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (357, NULL, 179, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (358, 20, 179, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (359, NULL, 180, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (360, 45, 180, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (361, NULL, 181, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (362, 41, 181, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (363, NULL, 182, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (364, 20, 182, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (365, NULL, 183, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (366, 27, 183, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (367, NULL, 184, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (368, 15, 184, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (369, NULL, 185, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (370, 22, 185, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (371, NULL, 186, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (372, 51, 186, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (373, NULL, 187, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (374, 43, 187, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (375, NULL, 188, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (376, 24, 188, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (377, NULL, 189, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (378, 20, 189, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (379, NULL, 190, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (380, 27, 190, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (381, NULL, 191, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (382, 18, 191, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (383, NULL, 192, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (384, 25, 192, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (385, NULL, 193, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (386, 50, 193, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (387, NULL, 194, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (388, 32, 194, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (389, NULL, 195, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (390, 15, 195, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (391, NULL, 196, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (392, 14, 196, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (393, NULL, 197, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (394, 25, 197, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (395, NULL, 198, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (396, 17, 198, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (397, NULL, 199, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (398, 27, 199, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (399, NULL, 200, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (400, 59, 200, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (401, NULL, 201, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (402, 49, 201, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (403, NULL, 202, 416, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (404, 24, 202, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (405, NULL, 203, 416, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (406, 21, 203, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (407, NULL, 204, 416, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (408, 25, 204, 414, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (409, NULL, 205, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (410, 59, 205, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (411, NULL, 206, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (412, 27, 206, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (413, NULL, 207, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (414, 49, 207, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (415, NULL, 208, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (416, 17, 208, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (417, NULL, 209, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (418, 25, 209, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (419, NULL, 210, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (420, 21, 210, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (421, NULL, 211, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (422, 24, 211, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (423, NULL, 212, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (424, 50, 212, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (425, NULL, 213, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (426, 25, 213, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (427, NULL, 214, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (428, 32, 214, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (429, NULL, 215, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (430, 18, 215, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (431, NULL, 216, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (432, 25, 216, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (433, NULL, 217, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (434, 14, 217, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (435, NULL, 218, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (436, 15, 218, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (437, NULL, 219, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (438, 45, 219, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (439, NULL, 220, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (440, 20, 220, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (441, NULL, 221, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (442, 41, 221, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (443, NULL, 222, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (444, 16, 222, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (445, NULL, 223, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (446, 27, 223, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (447, NULL, 224, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (448, 20, 224, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (449, NULL, 225, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (450, 54, 225, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (451, NULL, 226, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (452, 38, 226, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (453, NULL, 227, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (454, 27, 227, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (455, NULL, 228, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (456, 15, 228, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (457, NULL, 229, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (458, 25, 229, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (459, NULL, 230, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (460, 18, 230, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (461, NULL, 231, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (462, 39, 231, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (463, NULL, 232, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (464, 51, 232, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (465, NULL, 233, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (466, 22, 233, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (467, NULL, 234, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (468, 43, 234, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (469, NULL, 235, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (470, 15, 235, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (471, NULL, 236, 420, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (472, 27, 236, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (473, NULL, 237, 420, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (474, 20, 237, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (475, NULL, 238, 420, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (476, 24, 238, 418, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (477, NULL, 239, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (478, 45, 239, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (479, NULL, 240, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (480, 20, 240, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (481, NULL, 241, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (482, 41, 241, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (483, NULL, 242, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (484, 16, 242, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (485, NULL, 243, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (486, 27, 243, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (487, NULL, 244, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (488, 20, 244, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (489, NULL, 245, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (490, 50, 245, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (491, NULL, 246, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (492, 25, 246, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (493, NULL, 247, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (494, 32, 247, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (495, NULL, 248, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (496, 18, 248, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (497, NULL, 249, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (498, 25, 249, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (499, NULL, 250, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (500, 14, 250, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (501, NULL, 251, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (502, 15, 251, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (503, NULL, 252, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (504, 54, 252, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (505, NULL, 253, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (506, 38, 253, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (507, NULL, 254, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (508, 27, 254, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (509, NULL, 255, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (510, 15, 255, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (511, NULL, 256, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (512, 25, 256, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (513, NULL, 257, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (514, 18, 257, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (515, NULL, 258, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (516, 39, 258, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (517, NULL, 259, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (518, 51, 259, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (519, NULL, 260, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (520, 22, 260, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (521, NULL, 261, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (522, 43, 261, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (523, NULL, 262, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (524, 15, 262, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (525, NULL, 263, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (526, 27, 263, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (527, NULL, 264, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (528, 20, 264, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (529, NULL, 265, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (530, 24, 265, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (531, NULL, 266, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (532, 59, 266, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (533, NULL, 267, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (534, 27, 267, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (535, NULL, 268, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (536, 49, 268, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (537, NULL, 269, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (538, 17, 269, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (539, NULL, 270, 424, 379);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (540, 25, 270, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (541, NULL, 271, 424, 382);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (542, 21, 271, 422, NULL);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (543, NULL, 272, 424, 385);
INSERT INTO public.valeur_pem (vpem_id, vpm_value, mpem_id, id, vq_id) VALUES (544, 24, 272, 422, NULL);


--
-- TOC entry 3359 (class 0 OID 377533)
-- Dependencies: 241
-- Data for Name: valeur_qualitative; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.valeur_qualitative (vq_id, code, typelist, valeur) VALUES (379, 'couleur_des_individus', 'valeur_qualitative', 'rouge');
INSERT INTO public.valeur_qualitative (vq_id, code, typelist, valeur) VALUES (382, 'couleur_des_individus', 'valeur_qualitative', 'bleu');
INSERT INTO public.valeur_qualitative (vq_id, code, typelist, valeur) VALUES (385, 'couleur_des_individus', 'valeur_qualitative', 'vert');


--
-- TOC entry 3360 (class 0 OID 377541)
-- Dependencies: 242
-- Data for Name: variable; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.variable (affichage, definition, isqualitative, var_id) VALUES (NULL, 'Nombre d''individus', false, 368);
INSERT INTO public.variable (affichage, definition, isqualitative, var_id) VALUES (NULL, 'Couleur des individus', true, 373);


--
-- TOC entry 3361 (class 0 OID 377550)
-- Dependencies: 243
-- Data for Name: variablemonsoere; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.variablemonsoere (variablemonsoere_id) VALUES (368);
INSERT INTO public.variablemonsoere (variablemonsoere_id) VALUES (373);


--
-- TOC entry 3362 (class 0 OID 377555)
-- Dependencies: 244
-- Data for Name: zones_etude_zet; Type: TABLE DATA; Schema: public; Owner: kerneluser
--

INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 155, 2);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 160, 1);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 163, 1);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 166, 1);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 169, 1);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 174, 2);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 179, 2);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 180, 1);
INSERT INTO public.zones_etude_zet (sitemonsoeredisc, sitemonsoere_id, tze_id) VALUES (NULL, 181, 1);


--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 208
-- Name: espece_esp_esp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kerneluser
--

SELECT pg_catalog.setval('public.espece_esp_esp_id_seq', 7, true);


--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 245
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: kerneluser
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1105, true);


--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 221
-- Name: mesure_pem_mpem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kerneluser
--

SELECT pg_catalog.setval('public.mesure_pem_mpem_id_seq', 272, true);


--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 231
-- Name: sequencepem_spem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kerneluser
--

SELECT pg_catalog.setval('public.sequencepem_spem_id_seq', 40, true);


--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 235
-- Name: types_zone_etude_tze_tze_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kerneluser
--

SELECT pg_catalog.setval('public.types_zone_etude_tze_tze_id_seq', 2, true);


--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 239
-- Name: valeur_pem_vpem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kerneluser
--

SELECT pg_catalog.setval('public.valeur_pem_vpem_id_seq', 544, true);


--
-- TOC entry 3372 (class 0 OID 0)
-- Data for Name: BLOBS; Type: BLOBS; Schema: -; Owner: -
--

BEGIN;

SELECT pg_catalog.lo_open('385173', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a65742061746c616e7469717565223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385174', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a65742061746c616e7469717565223b226f6972223b2270312f61223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385175', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a65742061746c616e7469717565223b224f6972223b225032223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385176', 131072);
SELECT pg_catalog.lowrite(0, '\x2250726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a65742061746c616e7469717565223b22536361726666223b225031223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385177', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a6574206d616e636865223b224e6976656c6c65223b225031223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385178', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b22414c4f223b22626c6575223b2231352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b22414e47223b2276657274223b2233382e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b224c5046223b22626c6575223b2235342e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b224c504d223b22726f756765223b2232372e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b22534154223b22626c6575223b2233392e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b22545246223b22726f756765223b2231382e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230312f30312f31393834223b2254524d223b2276657274223b2232352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b22414c4f223b2276657274223b2231382e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b22414e47223b22726f756765223b2232352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b224c5046223b2276657274223b2235302e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b224c504d223b22626c6575223b2233322e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b22534154223b2276657274223b2231352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b22545246223b22626c6575223b2231342e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230322f30312f31393834223b2254524d223b22726f756765223b2232352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230332f30312f31393834223b22414c4f223b22726f756765223b2231362e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230332f30312f31393834223b22414e47223b22626c6575223b2232302e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230332f30312f31393834223b224c5046223b22726f756765223b2234352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230332f30312f31393834223b224c504d223b2276657274223b2234312e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230332f30312f31393834223b22545246223b2276657274223b2232302e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230332f30312f31393834223b2254524d223b22626c6575223b2232372e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b22414c4f223b22626c6575223b2231352e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b22414e47223b2276657274223b2232322e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b224c5046223b22626c6575223b2235312e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b224c504d223b22726f756765223b2234332e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b22534154223b22626c6575223b2232342e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b22545246223b22726f756765223b2232302e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230342f30312f31393834223b2254524d223b2276657274223b2232372e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b22414c4f223b2276657274223b2231372e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b22414e47223b22726f756765223b2232372e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b224c5046223b2276657274223b2235392e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b224c504d223b22626c6575223b2234392e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b22534154223b2276657274223b2232342e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b22545246223b22626c6575223b2232312e30220a2250726f6a6574206d616e636865223b224f6972223b225031223b2230352f30312f31393834223b2254524d223b22726f756765223b2232352e3022');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385179', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a6574206d616e636865223b224f6972223b225032223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('385180', 131072);
SELECT pg_catalog.lowrite(0, '\x2270726f6a6574223b2273697465223b22706c617465666f726d65223b2264617465223b22657370c3a86365223b22436f756c6575722064657320696e64697669647573223b224e6f6d627265206427696e64697669647573220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b226c7066223b22626c6575223b223534220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b22616e67223b2276657274223b223338220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b226c706d223b22726f756765223b223237220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b2274726d223b2276657274223b223235220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b22747266223b22726f756765223b223138220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230312f30312f31393834223b22736174223b22626c6575223b223339220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b226c7066223b2276657274223b223530220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b22616e67223b22726f756765223b223235220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b226c706d223b22626c6575223b223332220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b22616c6f223b2276657274223b223138220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b22747266223b22626c6575223b223134220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230322f30312f31393834223b22736174223b2276657274223b223135220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230332f30312f31393834223b226c7066223b22726f756765223b223435220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230332f30312f31393834223b22616e67223b22626c6575223b223230220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230332f30312f31393834223b226c706d223b2276657274223b223431220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230332f30312f31393834223b22616c6f223b22726f756765223b223136220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230332f30312f31393834223b2274726d223b22626c6575223b223237220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230332f30312f31393834223b22747266223b2276657274223b223230220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b226c7066223b22626c6575223b223531220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b22616e67223b2276657274223b223232220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b226c706d223b22726f756765223b223433220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b22616c6f223b22626c6575223b223135220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b2274726d223b2276657274223b223237220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b22747266223b22726f756765223b223230220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230342f30312f31393834223b22736174223b22626c6575223b223234220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b226c7066223b2276657274223b223539220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b22616e67223b22726f756765223b223237220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b226c706d223b22626c6575223b223439220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b22616c6f223b2276657274223b223137220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b2274726d223b22726f756765223b223235220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b22747266223b22626c6575223b223231220a2250726f6a6574206d616e636865223b22536361726666223b225031223b2230352f30312f31393834223b22736174223b2276657274223b22323422');
SELECT pg_catalog.lo_close(0);

COMMIT;

--
-- TOC entry 2961 (class 2606 OID 377301)
-- Name: composite_node_data_set composite_node_data_set_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT composite_node_data_set_pkey PRIMARY KEY (branch_node_id);


--
-- TOC entry 2963 (class 2606 OID 377306)
-- Name: composite_node_ref_data composite_node_ref_data_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT composite_node_ref_data_pkey PRIMARY KEY (branch_node_id);


--
-- TOC entry 2965 (class 2606 OID 377311)
-- Name: composite_nodeable composite_nodeable_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_nodeable
    ADD CONSTRAINT composite_nodeable_pkey PRIMARY KEY (id);


--
-- TOC entry 2969 (class 2606 OID 377316)
-- Name: compositeactivityextraction compositeactivityextraction_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.compositeactivityextraction
    ADD CONSTRAINT compositeactivityextraction_pkey PRIMARY KEY (id);


--
-- TOC entry 2975 (class 2606 OID 377324)
-- Name: datatype datatype_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype
    ADD CONSTRAINT datatype_pkey PRIMARY KEY (dty_id);


--
-- TOC entry 2979 (class 2606 OID 377329)
-- Name: datatype_unite_variable_vdt datatype_unite_variable_vdt_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT datatype_unite_variable_vdt_pkey PRIMARY KEY (datatypevariableunite_id);


--
-- TOC entry 2986 (class 2606 OID 377337)
-- Name: espece_esp espece_esp_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.espece_esp
    ADD CONSTRAINT espece_esp_pkey PRIMARY KEY (esp_id);


--
-- TOC entry 2995 (class 2606 OID 377345)
-- Name: extractiontype extractiontype_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT extractiontype_pkey PRIMARY KEY (id);


--
-- TOC entry 3004 (class 2606 OID 377353)
-- Name: filetype filetype_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT filetype_pkey PRIMARY KEY (ft_id);


--
-- TOC entry 3012 (class 2606 OID 377358)
-- Name: genericdatatype genericdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.genericdatatype
    ADD CONSTRAINT genericdatatype_pkey PRIMARY KEY (generic_datatype_id);


--
-- TOC entry 3014 (class 2606 OID 377363)
-- Name: group_filecomp group_filecomp_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.group_filecomp
    ADD CONSTRAINT group_filecomp_pkey PRIMARY KEY (file_comp_id, group_id);


--
-- TOC entry 3016 (class 2606 OID 377368)
-- Name: group_utilisateur group_utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.group_utilisateur
    ADD CONSTRAINT group_utilisateur_pkey PRIMARY KEY (usr_id, group_id);


--
-- TOC entry 3018 (class 2606 OID 377376)
-- Name: groupe groupe_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT groupe_pkey PRIMARY KEY (id);


--
-- TOC entry 3024 (class 2606 OID 377381)
-- Name: hibernate_sequences hibernate_sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.hibernate_sequences
    ADD CONSTRAINT hibernate_sequences_pkey PRIMARY KEY (sequence_name);


--
-- TOC entry 3032 (class 2606 OID 377386)
-- Name: insertion_dataset_ids insertion_dataset_ids_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT insertion_dataset_ids_pkey PRIMARY KEY (ids_id);


--
-- TOC entry 3040 (class 2606 OID 377394)
-- Name: insertion_filecomp_ifcs insertion_filecomp_ifcs_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT insertion_filecomp_ifcs_pkey PRIMARY KEY (ifcs_id);


--
-- TOC entry 3046 (class 2606 OID 377402)
-- Name: insertion_version_file_ivf insertion_version_file_ivf_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT insertion_version_file_ivf_pkey PRIMARY KEY (ivf_id);


--
-- TOC entry 3054 (class 2606 OID 377410)
-- Name: localisation localisation_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.localisation
    ADD CONSTRAINT localisation_pkey PRIMARY KEY (id);


--
-- TOC entry 3059 (class 2606 OID 377418)
-- Name: mesure_pem mesure_pem_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.mesure_pem
    ADD CONSTRAINT mesure_pem_pkey PRIMARY KEY (mpem_id);


--
-- TOC entry 3069 (class 2606 OID 377426)
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- TOC entry 3071 (class 2606 OID 377434)
-- Name: pemsynthesisdatatype pemsynthesisdatatype_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.pemsynthesisdatatype
    ADD CONSTRAINT pemsynthesisdatatype_pkey PRIMARY KEY (id);


--
-- TOC entry 3073 (class 2606 OID 377442)
-- Name: pemsynthesisvalue pemsynthesisvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.pemsynthesisvalue
    ADD CONSTRAINT pemsynthesisvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 3076 (class 2606 OID 377450)
-- Name: projet projet_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT projet_pkey PRIMARY KEY (id);


--
-- TOC entry 3078 (class 2606 OID 377629)
-- Name: realnode real_node_nk; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT real_node_nk UNIQUE (id_nodeable, id_parent_node);


--
-- TOC entry 3080 (class 2606 OID 377631)
-- Name: realnode real_node_path; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT real_node_path UNIQUE (path);


--
-- TOC entry 3086 (class 2606 OID 377455)
-- Name: realnode realnode_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT realnode_pkey PRIMARY KEY (id);


--
-- TOC entry 3088 (class 2606 OID 377460)
-- Name: refdata refdata_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.refdata
    ADD CONSTRAINT refdata_pkey PRIMARY KEY (id);


--
-- TOC entry 3091 (class 2606 OID 377468)
-- Name: requete requete_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.requete
    ADD CONSTRAINT requete_pkey PRIMARY KEY (id);


--
-- TOC entry 3094 (class 2606 OID 377476)
-- Name: rightrequest rightrequest_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.rightrequest
    ADD CONSTRAINT rightrequest_pkey PRIMARY KEY (id);


--
-- TOC entry 3100 (class 2606 OID 377484)
-- Name: sequencepem sequencepem_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.sequencepem
    ADD CONSTRAINT sequencepem_pkey PRIMARY KEY (spem_id);


--
-- TOC entry 3104 (class 2606 OID 377492)
-- Name: site site_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT site_pkey PRIMARY KEY (site_id);


--
-- TOC entry 3106 (class 2606 OID 377500)
-- Name: theme theme_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.theme
    ADD CONSTRAINT theme_pkey PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 377508)
-- Name: types_zone_etude_tze types_zone_etude_tze_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.types_zone_etude_tze
    ADD CONSTRAINT types_zone_etude_tze_pkey PRIMARY KEY (tze_id);


--
-- TOC entry 3120 (class 2606 OID 378650)
-- Name: utilisateur uk35ysk0sh9ruwixrld3nc0weut; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT uk35ysk0sh9ruwixrld3nc0weut UNIQUE (email);


--
-- TOC entry 3034 (class 2606 OID 377599)
-- Name: insertion_dataset_ids uk4enu5sb3qsya70slf6h50y524; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT uk4enu5sb3qsya70slf6h50y524 UNIQUE (id, ids_date_debut_periode, ids_date_fin_periode);


--
-- TOC entry 3020 (class 2606 OID 377590)
-- Name: groupe uk8da4mqyywnqafbgiti7bepn09; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT uk8da4mqyywnqafbgiti7bepn09 UNIQUE (group_name, group_which_tree);


--
-- TOC entry 3130 (class 2606 OID 377652)
-- Name: valeur_pem uk9jwbpugwy2knlcu5uuycdwhcg; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_pem
    ADD CONSTRAINT uk9jwbpugwy2knlcu5uuycdwhcg UNIQUE (mpem_id, id);


--
-- TOC entry 3056 (class 2606 OID 377613)
-- Name: localisation uk9mfgxosxjn4tu45hdhe524oph; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.localisation
    ADD CONSTRAINT uk9mfgxosxjn4tu45hdhe524oph UNIQUE (localization, entite, colonne, defaultstring);


--
-- TOC entry 2977 (class 2606 OID 377567)
-- Name: datatype uk_1wbrux28nejdwyxvk8vtgbhky; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype
    ADD CONSTRAINT uk_1wbrux28nejdwyxvk8vtgbhky UNIQUE (name);


--
-- TOC entry 3122 (class 2606 OID 377645)
-- Name: utilisateur uk_35ysk0sh9ruwixrld3nc0weut; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT uk_35ysk0sh9ruwixrld3nc0weut UNIQUE (email);


--
-- TOC entry 3097 (class 2606 OID 377636)
-- Name: rightrequest uk_6k6t90k2feqiwir28a670fca8; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.rightrequest
    ADD CONSTRAINT uk_6k6t90k2feqiwir28a670fca8 UNIQUE (usr_id);


--
-- TOC entry 3022 (class 2606 OID 377588)
-- Name: groupe uk_9n56mb0ey2u4tsas11vovq0pa; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT uk_9n56mb0ey2u4tsas11vovq0pa UNIQUE (group_name, group_type, group_which_tree);


--
-- TOC entry 3110 (class 2606 OID 377641)
-- Name: types_zone_etude_tze uk_b71h9o2r3lgwcgwkgb86vfp16; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.types_zone_etude_tze
    ADD CONSTRAINT uk_b71h9o2r3lgwcgwkgb86vfp16 UNIQUE (tze_code);


--
-- TOC entry 3114 (class 2606 OID 378646)
-- Name: unite uk_c8cbymjvf78cmtcwtesp1h27e; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.unite
    ADD CONSTRAINT uk_c8cbymjvf78cmtcwtesp1h27e UNIQUE (code);


--
-- TOC entry 2997 (class 2606 OID 378638)
-- Name: extractiontype uk_ek72kldu8j3joij9ou7e3tk88; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT uk_ek72kldu8j3joij9ou7e3tk88 UNIQUE (code);


--
-- TOC entry 3006 (class 2606 OID 377586)
-- Name: filetype uk_gonw2ifoyhnht1949bpyitog7; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT uk_gonw2ifoyhnht1949bpyitog7 UNIQUE (name);


--
-- TOC entry 2988 (class 2606 OID 377576)
-- Name: espece_esp uk_jmwamgpt1la6dpjn1vyauaidb; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.espece_esp
    ADD CONSTRAINT uk_jmwamgpt1la6dpjn1vyauaidb UNIQUE (esp_nom);


--
-- TOC entry 3116 (class 2606 OID 377643)
-- Name: unite uk_jslkegk7riv9va6pq857n0j5l; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.unite
    ADD CONSTRAINT uk_jslkegk7riv9va6pq857n0j5l UNIQUE (code);


--
-- TOC entry 3042 (class 2606 OID 377605)
-- Name: insertion_filecomp_ifcs uk_jxiqp6e8nya0vskb87hbp29b0; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT uk_jxiqp6e8nya0vskb87hbp29b0 UNIQUE (ifcs_code);


--
-- TOC entry 3124 (class 2606 OID 377647)
-- Name: utilisateur uk_kmw1w139mxftir6ce47jrbxac; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT uk_kmw1w139mxftir6ce47jrbxac UNIQUE (login);


--
-- TOC entry 2999 (class 2606 OID 377581)
-- Name: extractiontype uk_mky7ew3tmga5k29xvlwdhsofl; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT uk_mky7ew3tmga5k29xvlwdhsofl UNIQUE (name);


--
-- TOC entry 3008 (class 2606 OID 377584)
-- Name: filetype uk_nbam0dixwdtnm4hwfu9wgvpbh; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT uk_nbam0dixwdtnm4hwfu9wgvpbh UNIQUE (code);


--
-- TOC entry 2990 (class 2606 OID 377574)
-- Name: espece_esp uk_o3ui7h2ug3n5ai5rpn6ek2q09; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.espece_esp
    ADD CONSTRAINT uk_o3ui7h2ug3n5ai5rpn6ek2q09 UNIQUE (esp_code);


--
-- TOC entry 3001 (class 2606 OID 377579)
-- Name: extractiontype uk_pag81d4u304wrfktrcdohi97l; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.extractiontype
    ADD CONSTRAINT uk_pag81d4u304wrfktrcdohi97l UNIQUE (code);


--
-- TOC entry 3010 (class 2606 OID 378640)
-- Name: filetype uk_t15pimato2f2f9iroqg8slv74; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.filetype
    ADD CONSTRAINT uk_t15pimato2f2f9iroqg8slv74 UNIQUE (code);


--
-- TOC entry 2984 (class 2606 OID 377572)
-- Name: datatype_unite_variable_vdt ukaj5f707xngjog30hslcut7iej; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT ukaj5f707xngjog30hslcut7iej UNIQUE (var_id, dty_id, uni_id);


--
-- TOC entry 3112 (class 2606 OID 378644)
-- Name: types_zone_etude_tze ukb71h9o2r3lgwcgwkgb86vfp16; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.types_zone_etude_tze
    ADD CONSTRAINT ukb71h9o2r3lgwcgwkgb86vfp16 UNIQUE (tze_code);


--
-- TOC entry 2992 (class 2606 OID 378636)
-- Name: espece_esp ukjmwamgpt1la6dpjn1vyauaidb; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.espece_esp
    ADD CONSTRAINT ukjmwamgpt1la6dpjn1vyauaidb UNIQUE (esp_nom);


--
-- TOC entry 3044 (class 2606 OID 378642)
-- Name: insertion_filecomp_ifcs ukjxiqp6e8nya0vskb87hbp29b0; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT ukjxiqp6e8nya0vskb87hbp29b0 UNIQUE (ifcs_code);


--
-- TOC entry 3126 (class 2606 OID 378648)
-- Name: utilisateur ukkmw1w139mxftir6ce47jrbxac; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT ukkmw1w139mxftir6ce47jrbxac UNIQUE (login);


--
-- TOC entry 2973 (class 2606 OID 377565)
-- Name: compositeactivityextraction ukorelf73ugljgad242rstffg11; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.compositeactivityextraction
    ADD CONSTRAINT ukorelf73ugljgad242rstffg11 UNIQUE (idnode, login);


--
-- TOC entry 3137 (class 2606 OID 377654)
-- Name: valeur_qualitative ukqnml805ln93qvtpclqe61ogdg; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_qualitative
    ADD CONSTRAINT ukqnml805ln93qvtpclqe61ogdg UNIQUE (valeur, code, typelist);


--
-- TOC entry 3062 (class 2606 OID 377617)
-- Name: mesure_pem ukrkxjwmbr50guansvktbqe1s2h; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.mesure_pem
    ADD CONSTRAINT ukrkxjwmbr50guansvktbqe1s2h UNIQUE (spem_id, esp_id);


--
-- TOC entry 3102 (class 2606 OID 377639)
-- Name: sequencepem ukshkhtjmrv1b9krucifih6ys0q; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.sequencepem
    ADD CONSTRAINT ukshkhtjmrv1b9krucifih6ys0q UNIQUE (ivf_id, date);


--
-- TOC entry 3049 (class 2606 OID 377608)
-- Name: insertion_version_file_ivf ukswl3lqlyfh6e48ft2j5o0a5ej; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT ukswl3lqlyfh6e48ft2j5o0a5ej UNIQUE (ivf_version_number, ids_id);


--
-- TOC entry 2967 (class 2606 OID 377561)
-- Name: composite_nodeable unique_code_dbtype_uk; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_nodeable
    ADD CONSTRAINT unique_code_dbtype_uk UNIQUE (dtype, code);


--
-- TOC entry 3118 (class 2606 OID 377516)
-- Name: unite unite_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.unite
    ADD CONSTRAINT unite_pkey PRIMARY KEY (uni_id);


--
-- TOC entry 3128 (class 2606 OID 377524)
-- Name: utilisateur utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- TOC entry 3133 (class 2606 OID 377532)
-- Name: valeur_pem valeur_pem_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_pem
    ADD CONSTRAINT valeur_pem_pkey PRIMARY KEY (vpem_id);


--
-- TOC entry 3139 (class 2606 OID 377540)
-- Name: valeur_qualitative valeur_qualitative_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_qualitative
    ADD CONSTRAINT valeur_qualitative_pkey PRIMARY KEY (vq_id);


--
-- TOC entry 3141 (class 2606 OID 377549)
-- Name: variable variable_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.variable
    ADD CONSTRAINT variable_pkey PRIMARY KEY (var_id);


--
-- TOC entry 3143 (class 2606 OID 377554)
-- Name: variablemonsoere variablemonsoere_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.variablemonsoere
    ADD CONSTRAINT variablemonsoere_pkey PRIMARY KEY (variablemonsoere_id);


--
-- TOC entry 3146 (class 2606 OID 377559)
-- Name: zones_etude_zet zones_etude_zet_pkey; Type: CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.zones_etude_zet
    ADD CONSTRAINT zones_etude_zet_pkey PRIMARY KEY (sitemonsoere_id);


--
-- TOC entry 2980 (class 1259 OID 377570)
-- Name: dvu_dty_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX dvu_dty_idx ON public.datatype_unite_variable_vdt USING btree (dty_id);


--
-- TOC entry 2981 (class 1259 OID 377568)
-- Name: dvu_uni_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX dvu_uni_idx ON public.datatype_unite_variable_vdt USING btree (uni_id);


--
-- TOC entry 2982 (class 1259 OID 377569)
-- Name: dvu_var_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX dvu_var_idx ON public.datatype_unite_variable_vdt USING btree (var_id);


--
-- TOC entry 2993 (class 1259 OID 377577)
-- Name: extractiontype_code_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX extractiontype_code_idx ON public.extractiontype USING btree (code);


--
-- TOC entry 3035 (class 1259 OID 377600)
-- Name: file_code_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX file_code_idx ON public.insertion_filecomp_ifcs USING btree (ifcs_code);


--
-- TOC entry 3036 (class 1259 OID 377603)
-- Name: file_create_user_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX file_create_user_idx ON public.insertion_filecomp_ifcs USING btree (ifcs_create_user);


--
-- TOC entry 3037 (class 1259 OID 377602)
-- Name: file_modify_user_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX file_modify_user_idx ON public.insertion_filecomp_ifcs USING btree (ifcs_last_modify_user);


--
-- TOC entry 3038 (class 1259 OID 377601)
-- Name: file_type_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX file_type_idx ON public.insertion_filecomp_ifcs USING btree (ft_id);


--
-- TOC entry 3002 (class 1259 OID 377582)
-- Name: filetype_code_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX filetype_code_idx ON public.filetype USING btree (code);


--
-- TOC entry 2970 (class 1259 OID 377563)
-- Name: idnode_loginidx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX idnode_loginidx ON public.compositeactivityextraction USING btree (idnode, login);


--
-- TOC entry 2971 (class 1259 OID 377562)
-- Name: idnodeidx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX idnodeidx ON public.compositeactivityextraction USING btree (idnode);


--
-- TOC entry 3025 (class 1259 OID 377591)
-- Name: ids_createuser_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ids_createuser_idx ON public.insertion_dataset_ids USING btree (ids_create_user);


--
-- TOC entry 3026 (class 1259 OID 377593)
-- Name: ids_datedebut_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ids_datedebut_idx ON public.insertion_dataset_ids USING btree (ids_date_debut_periode);


--
-- TOC entry 3027 (class 1259 OID 377595)
-- Name: ids_datefin_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ids_datefin_idx ON public.insertion_dataset_ids USING btree (ids_date_fin_periode);


--
-- TOC entry 3028 (class 1259 OID 377596)
-- Name: ids_dtn_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ids_dtn_idx ON public.insertion_dataset_ids USING btree (id);


--
-- TOC entry 3029 (class 1259 OID 377592)
-- Name: ids_publish_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ids_publish_idx ON public.insertion_dataset_ids USING btree (ids_publish_user);


--
-- TOC entry 3030 (class 1259 OID 377597)
-- Name: ids_version_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ids_version_idx ON public.insertion_dataset_ids USING btree (ivf_id);


--
-- TOC entry 3047 (class 1259 OID 377606)
-- Name: ivf_ids_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX ivf_ids_idx ON public.insertion_version_file_ivf USING btree (ids_id);


--
-- TOC entry 3050 (class 1259 OID 377610)
-- Name: loc_ent_col_def_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX loc_ent_col_def_idx ON public.localisation USING btree (entite, colonne, defaultstring);


--
-- TOC entry 3051 (class 1259 OID 377611)
-- Name: loc_loc_ent_col_def_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX loc_loc_ent_col_def_idx ON public.localisation USING btree (localization, entite, colonne, defaultstring);


--
-- TOC entry 3052 (class 1259 OID 377609)
-- Name: loc_loc_ent_col_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX loc_loc_ent_col_idx ON public.localisation USING btree (localization, entite, colonne);


--
-- TOC entry 3057 (class 1259 OID 377614)
-- Name: mesure_pem_espece_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX mesure_pem_espece_idx ON public.mesure_pem USING btree (esp_id);


--
-- TOC entry 3060 (class 1259 OID 377615)
-- Name: mesure_pem_sequence_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX mesure_pem_sequence_idx ON public.mesure_pem USING btree (spem_id);


--
-- TOC entry 3063 (class 1259 OID 377619)
-- Name: not_archived_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX not_archived_idx ON public.notification USING btree (archived);


--
-- TOC entry 3064 (class 1259 OID 377618)
-- Name: not_date_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX not_date_idx ON public.notification USING btree (date);


--
-- TOC entry 3065 (class 1259 OID 377622)
-- Name: not_user_date_archived_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX not_user_date_archived_idx ON public.notification USING btree (utilisateur_id, date, archived);


--
-- TOC entry 3066 (class 1259 OID 377621)
-- Name: not_user_date_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX not_user_date_idx ON public.notification USING btree (utilisateur_id, date);


--
-- TOC entry 3067 (class 1259 OID 377620)
-- Name: not_user_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX not_user_idx ON public.notification USING btree (utilisateur_id);


--
-- TOC entry 3074 (class 1259 OID 377623)
-- Name: pemsynthesisvalue_site_variable_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX pemsynthesisvalue_site_variable_idx ON public.pemsynthesisvalue USING btree (site, variable);


--
-- TOC entry 3081 (class 1259 OID 377625)
-- Name: realnode_ancestor_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX realnode_ancestor_idx ON public.realnode USING btree (id_ancestor);


--
-- TOC entry 3082 (class 1259 OID 377626)
-- Name: realnode_nodeable_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX realnode_nodeable_idx ON public.realnode USING btree (id_nodeable);


--
-- TOC entry 3083 (class 1259 OID 377624)
-- Name: realnode_parent_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX realnode_parent_idx ON public.realnode USING btree (id_parent_node);


--
-- TOC entry 3084 (class 1259 OID 377627)
-- Name: realnode_path_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX realnode_path_idx ON public.realnode USING btree (path);


--
-- TOC entry 3089 (class 1259 OID 377632)
-- Name: requete_extractiontype_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX requete_extractiontype_idx ON public.requete USING btree (extractiontype_id);


--
-- TOC entry 3092 (class 1259 OID 377633)
-- Name: requete_user_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX requete_user_idx ON public.requete USING btree (utilisateur_id);


--
-- TOC entry 3095 (class 1259 OID 377634)
-- Name: rightrequest_utilisateur_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX rightrequest_utilisateur_idx ON public.rightrequest USING btree (usr_id);


--
-- TOC entry 3098 (class 1259 OID 377637)
-- Name: sequence_pem_version_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX sequence_pem_version_idx ON public.sequencepem USING btree (ivf_id);


--
-- TOC entry 3144 (class 1259 OID 377655)
-- Name: site_monsoere_typesite_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX site_monsoere_typesite_idx ON public.zones_etude_zet USING btree (tze_id);


--
-- TOC entry 3131 (class 1259 OID 377648)
-- Name: valeur_pem_mesure_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX valeur_pem_mesure_idx ON public.valeur_pem USING btree (mpem_id);


--
-- TOC entry 3134 (class 1259 OID 377649)
-- Name: valeur_pem_realnode_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX valeur_pem_realnode_idx ON public.valeur_pem USING btree (id);


--
-- TOC entry 3135 (class 1259 OID 377650)
-- Name: valeur_pem_vq_idx; Type: INDEX; Schema: public; Owner: kerneluser
--

CREATE INDEX valeur_pem_vq_idx ON public.valeur_pem USING btree (vq_id);


--
-- TOC entry 3193 (class 2606 OID 377888)
-- Name: zones_etude_zet fk1us42ntr21h7j4poul1lsgtyw; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.zones_etude_zet
    ADD CONSTRAINT fk1us42ntr21h7j4poul1lsgtyw FOREIGN KEY (sitemonsoere_id) REFERENCES public.site(site_id);


--
-- TOC entry 3171 (class 2606 OID 377778)
-- Name: insertion_version_file_ivf fk1w5ud4ffhlip33fp0h1v0clqt; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT fk1w5ud4ffhlip33fp0h1v0clqt FOREIGN KEY (ivf_upload_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3157 (class 2606 OID 377708)
-- Name: datatype_unite_variable_vdt fk26beo8l0x2bg769r97eo99tud; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fk26beo8l0x2bg769r97eo99tud FOREIGN KEY (datatypevariableunite_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3188 (class 2606 OID 377863)
-- Name: valeur_pem fk27xodim7v1ix2wfu7tcfjxttg; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_pem
    ADD CONSTRAINT fk27xodim7v1ix2wfu7tcfjxttg FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3189 (class 2606 OID 377868)
-- Name: valeur_pem fk31wm59teq8d21p8md7sotmmpa; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_pem
    ADD CONSTRAINT fk31wm59teq8d21p8md7sotmmpa FOREIGN KEY (vq_id) REFERENCES public.valeur_qualitative(vq_id);


--
-- TOC entry 3177 (class 2606 OID 377808)
-- Name: realnode fk4lbmw99i4s6gtyfr8ir8hqypn; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT fk4lbmw99i4s6gtyfr8ir8hqypn FOREIGN KEY (id_nodeable) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3181 (class 2606 OID 377828)
-- Name: requete fk5yvj64ddb9ae0ebboumnrhhn1; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.requete
    ADD CONSTRAINT fk5yvj64ddb9ae0ebboumnrhhn1 FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3166 (class 2606 OID 377753)
-- Name: insertion_dataset_ids fk6mvhhq9whm8cwnfj3on2eua2p; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fk6mvhhq9whm8cwnfj3on2eua2p FOREIGN KEY (id) REFERENCES public.realnode(id);


--
-- TOC entry 3164 (class 2606 OID 377743)
-- Name: insertion_dataset_ids fk81x1iytx14u3ctaflltfil0m0; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fk81x1iytx14u3ctaflltfil0m0 FOREIGN KEY (ids_publish_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3179 (class 2606 OID 377818)
-- Name: refdata fk92w1pw55p4enf1k3n6qobd8sa; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.refdata
    ADD CONSTRAINT fk92w1pw55p4enf1k3n6qobd8sa FOREIGN KEY (id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3148 (class 2606 OID 377663)
-- Name: composite_node_data_set fk9d32fv5o0c8vntnkgb9g5wpy; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT fk9d32fv5o0c8vntnkgb9g5wpy FOREIGN KEY (id_parent_node) REFERENCES public.composite_node_data_set(branch_node_id);


--
-- TOC entry 3158 (class 2606 OID 377713)
-- Name: genericdatatype fk9g6pnggaxsb9whuj2v71fh6i9; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.genericdatatype
    ADD CONSTRAINT fk9g6pnggaxsb9whuj2v71fh6i9 FOREIGN KEY (ids_id) REFERENCES public.insertion_dataset_ids(ids_id);


--
-- TOC entry 3161 (class 2606 OID 377728)
-- Name: group_utilisateur fk9holr4tkrtkwc3kt9s1mbe3sd; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.group_utilisateur
    ADD CONSTRAINT fk9holr4tkrtkwc3kt9s1mbe3sd FOREIGN KEY (usr_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3175 (class 2606 OID 377798)
-- Name: projet fkas2wt3vwt313v4ku9bgbrpvff; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT fkas2wt3vwt313v4ku9bgbrpvff FOREIGN KEY (id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3151 (class 2606 OID 377678)
-- Name: composite_node_ref_data fkb4dhbn68xjtbgh0t1bx4nr0om; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT fkb4dhbn68xjtbgh0t1bx4nr0om FOREIGN KEY (id_parent_node) REFERENCES public.composite_node_ref_data(branch_node_id);


--
-- TOC entry 3184 (class 2606 OID 377843)
-- Name: site fkc6dfy2hroo6yf1d3hoth9vkbv; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT fkc6dfy2hroo6yf1d3hoth9vkbv FOREIGN KEY (parent_site_id) REFERENCES public.site(site_id);


--
-- TOC entry 3190 (class 2606 OID 377873)
-- Name: variable fkcc6dgptrkbjplrljqqqqtwdw3; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.variable
    ADD CONSTRAINT fkcc6dgptrkbjplrljqqqqtwdw3 FOREIGN KEY (var_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3192 (class 2606 OID 377883)
-- Name: zones_etude_zet fkccay2r29rqpihn9658n1jtidk; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.zones_etude_zet
    ADD CONSTRAINT fkccay2r29rqpihn9658n1jtidk FOREIGN KEY (tze_id) REFERENCES public.types_zone_etude_tze(tze_id);


--
-- TOC entry 3170 (class 2606 OID 377773)
-- Name: insertion_version_file_ivf fkdjo1nvaq8ghr3h5r9owu3egop; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_version_file_ivf
    ADD CONSTRAINT fkdjo1nvaq8ghr3h5r9owu3egop FOREIGN KEY (ids_id) REFERENCES public.insertion_dataset_ids(ids_id);


--
-- TOC entry 3159 (class 2606 OID 377718)
-- Name: group_filecomp fkeeelh6xxwb86dnaisp78t3lq0; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.group_filecomp
    ADD CONSTRAINT fkeeelh6xxwb86dnaisp78t3lq0 FOREIGN KEY (group_id) REFERENCES public.groupe(id);


--
-- TOC entry 3187 (class 2606 OID 377858)
-- Name: valeur_pem fkeyutd9lothtugv5xawnl9jp9f; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.valeur_pem
    ADD CONSTRAINT fkeyutd9lothtugv5xawnl9jp9f FOREIGN KEY (mpem_id) REFERENCES public.mesure_pem(mpem_id);


--
-- TOC entry 3172 (class 2606 OID 377783)
-- Name: mesure_pem fkgu7wje7sskdrc8yltsanftn9q; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.mesure_pem
    ADD CONSTRAINT fkgu7wje7sskdrc8yltsanftn9q FOREIGN KEY (esp_id) REFERENCES public.espece_esp(esp_id);


--
-- TOC entry 3168 (class 2606 OID 377763)
-- Name: insertion_filecomp_ifcs fkh3o01tlmt6vpkloner7dow7hk; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT fkh3o01tlmt6vpkloner7dow7hk FOREIGN KEY (ft_id) REFERENCES public.filetype(ft_id);


--
-- TOC entry 3149 (class 2606 OID 377668)
-- Name: composite_node_data_set fkhoqcd3yosuwh1q34p0k0goivy; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT fkhoqcd3yosuwh1q34p0k0goivy FOREIGN KEY (realnode) REFERENCES public.realnode(id);


--
-- TOC entry 3162 (class 2606 OID 377733)
-- Name: group_utilisateur fkihbwxbljy0bv7x3y5vi1uoml0; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.group_utilisateur
    ADD CONSTRAINT fkihbwxbljy0bv7x3y5vi1uoml0 FOREIGN KEY (group_id) REFERENCES public.groupe(id);


--
-- TOC entry 3153 (class 2606 OID 377688)
-- Name: datatype fkixi17p09knvowjmhfocbrrfbj; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype
    ADD CONSTRAINT fkixi17p09knvowjmhfocbrrfbj FOREIGN KEY (dty_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3174 (class 2606 OID 377793)
-- Name: notification fkj13vhtnf7obeuuqm7ldy5clo7; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fkj13vhtnf7obeuuqm7ldy5clo7 FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3155 (class 2606 OID 377698)
-- Name: datatype_unite_variable_vdt fkj46owt7l4apb1sp9hyarqckk6; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fkj46owt7l4apb1sp9hyarqckk6 FOREIGN KEY (uni_id) REFERENCES public.unite(uni_id);


--
-- TOC entry 3185 (class 2606 OID 377848)
-- Name: site fkjfufr8g8k1hium7hmvotou1rk; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT fkjfufr8g8k1hium7hmvotou1rk FOREIGN KEY (site_id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3191 (class 2606 OID 377878)
-- Name: variablemonsoere fkjmps8ftaacxpcwa2x1ue60ugt; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.variablemonsoere
    ADD CONSTRAINT fkjmps8ftaacxpcwa2x1ue60ugt FOREIGN KEY (variablemonsoere_id) REFERENCES public.variable(var_id);


--
-- TOC entry 3173 (class 2606 OID 377788)
-- Name: mesure_pem fkjn4d01u3mxk2i9keci0g7ut9m; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.mesure_pem
    ADD CONSTRAINT fkjn4d01u3mxk2i9keci0g7ut9m FOREIGN KEY (spem_id) REFERENCES public.sequencepem(spem_id);


--
-- TOC entry 3180 (class 2606 OID 377823)
-- Name: requete fkjnkx7cw83rciufbu3i98h10mj; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.requete
    ADD CONSTRAINT fkjnkx7cw83rciufbu3i98h10mj FOREIGN KEY (extractiontype_id) REFERENCES public.extractiontype(id);


--
-- TOC entry 3156 (class 2606 OID 377703)
-- Name: datatype_unite_variable_vdt fkk1h2k3k4scs7a79fkr7qhbg8e; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fkk1h2k3k4scs7a79fkr7qhbg8e FOREIGN KEY (var_id) REFERENCES public.variable(var_id);


--
-- TOC entry 3160 (class 2606 OID 377723)
-- Name: group_filecomp fkkt3an0qqq65wixo27nxrvub3s; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.group_filecomp
    ADD CONSTRAINT fkkt3an0qqq65wixo27nxrvub3s FOREIGN KEY (file_comp_id) REFERENCES public.insertion_filecomp_ifcs(ifcs_id);


--
-- TOC entry 3178 (class 2606 OID 377813)
-- Name: realnode fkl3j5gvobv23enxcqag4awnbwu; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT fkl3j5gvobv23enxcqag4awnbwu FOREIGN KEY (id_parent_node) REFERENCES public.realnode(id);


--
-- TOC entry 3182 (class 2606 OID 377833)
-- Name: rightrequest fkletdmcnjiqsfrfp27vx9igdu; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.rightrequest
    ADD CONSTRAINT fkletdmcnjiqsfrfp27vx9igdu FOREIGN KEY (usr_id) REFERENCES public.utilisateur(id);


--
-- TOC entry 3186 (class 2606 OID 377853)
-- Name: theme fklpjre9pry3n8m38dmskrl5x0q; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.theme
    ADD CONSTRAINT fklpjre9pry3n8m38dmskrl5x0q FOREIGN KEY (id) REFERENCES public.composite_nodeable(id);


--
-- TOC entry 3163 (class 2606 OID 377738)
-- Name: insertion_dataset_ids fklx67xw1428f91yqnq2htcls2g; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fklx67xw1428f91yqnq2htcls2g FOREIGN KEY (ids_create_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3154 (class 2606 OID 377693)
-- Name: datatype_unite_variable_vdt fkm5kdj129k9uodwd5os1lmk321; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.datatype_unite_variable_vdt
    ADD CONSTRAINT fkm5kdj129k9uodwd5os1lmk321 FOREIGN KEY (dty_id) REFERENCES public.datatype(dty_id);


--
-- TOC entry 3176 (class 2606 OID 377803)
-- Name: realnode fkp8pnep929vo4qcl1n1fkw1mtc; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.realnode
    ADD CONSTRAINT fkp8pnep929vo4qcl1n1fkw1mtc FOREIGN KEY (id_ancestor) REFERENCES public.realnode(id);


--
-- TOC entry 3169 (class 2606 OID 377768)
-- Name: insertion_filecomp_ifcs fkpebl7kmjv45u0gw30gvatbjbl; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT fkpebl7kmjv45u0gw30gvatbjbl FOREIGN KEY (ifcs_last_modify_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3165 (class 2606 OID 377748)
-- Name: insertion_dataset_ids fkpr9d5m8ngwhhmqr9r9pukc5vy; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_dataset_ids
    ADD CONSTRAINT fkpr9d5m8ngwhhmqr9r9pukc5vy FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3152 (class 2606 OID 377683)
-- Name: composite_node_ref_data fkqb0ww0h1pj3wvf6dcq3fr7hou; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT fkqb0ww0h1pj3wvf6dcq3fr7hou FOREIGN KEY (realnode) REFERENCES public.realnode(id);


--
-- TOC entry 3150 (class 2606 OID 377673)
-- Name: composite_node_ref_data fks5b4w85norrexocr0cnsao05w; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_ref_data
    ADD CONSTRAINT fks5b4w85norrexocr0cnsao05w FOREIGN KEY (id_ancestor) REFERENCES public.composite_node_ref_data(branch_node_id);


--
-- TOC entry 3183 (class 2606 OID 377838)
-- Name: sequencepem fksjilvhqox4fnscqfm25e9hnll; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.sequencepem
    ADD CONSTRAINT fksjilvhqox4fnscqfm25e9hnll FOREIGN KEY (ivf_id) REFERENCES public.insertion_version_file_ivf(ivf_id);


--
-- TOC entry 3167 (class 2606 OID 377758)
-- Name: insertion_filecomp_ifcs fkta2s1dq76hc6oajc101ao11fx; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.insertion_filecomp_ifcs
    ADD CONSTRAINT fkta2s1dq76hc6oajc101ao11fx FOREIGN KEY (ifcs_create_user) REFERENCES public.utilisateur(id);


--
-- TOC entry 3147 (class 2606 OID 377658)
-- Name: composite_node_data_set fktdgs4po4hdthw56xqq6ka6k6m; Type: FK CONSTRAINT; Schema: public; Owner: kerneluser
--

ALTER TABLE ONLY public.composite_node_data_set
    ADD CONSTRAINT fktdgs4po4hdthw56xqq6ka6k6m FOREIGN KEY (id_ancestor) REFERENCES public.composite_node_data_set(branch_node_id);


-- Completed on 2020-09-01 07:38:31 UTC

--
-- PostgreSQL database dump complete
--


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;

import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl.PhytoplanctonParameters;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ValeurProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.LoggerFactory;

/**
 * @author tcherniatinsky
 */
public class UIPhytoTaxon {

    /**
     *
     */
    public static final String ICON_BLANK = "ui-icon ui-icon-blank";

    /**
     *
     */
    public static final String ICON_CHECK = "ui-icon ui-icon-check";

    /**
     *
     */
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";

    /**
     *
     */
    public static final String CLASSE_ALGALE_SENSU_BOURRELLY = "classe_algale_sensu_bourrelly";

    /**
     *
     */
    public static final String VALEUR_DU_BIOVOLUME_SPECIFIQUE_CHOISI_POU = "valeur_du_biovolume_specifique_choisi_pour_le_taxon";

    /**
     *
     */
    public static final String LONGUEUR_DE_LA_CELLULE = "longueur_de_la_cellule";

    /**
     *
     */
    public static final String ALGAL = "algal";

    /**
     *
     */
    public static final String LEAF = "leaf";

    /**
     *
     */
    public static final String ROOT = "root";

    /**
     *
     */
    public static final String PARENT = "parent";
    protected static final String MAX = "max";
    protected static final String MIN = "min";
    private ILocalizationManager localizationManager;
    /**
     * add taxon property
     */
    private Boolean propriete = Boolean.FALSE;
    /**
     * sum on branch taxon
     */
    private Boolean sommation = Boolean.TRUE;
    /**
     * detail of determinate taxon
     */
    private Boolean detail = Boolean.FALSE;
    /**
     * select only gerre_espece taxon
     */
    private Boolean genre = false;
    private Boolean aggregated = Boolean.FALSE;
    Properties propertiesTaxonLevelNames;

    private IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager;
    /**
     * map of all 
     */
    private Map<Long, VOTaxonJSF> taxonsAvailables = new HashMap<>();
    /**
     * map of selectedtaxon By id
     */
    private SortedMap<Long, VOTaxonJSF> taxonsSelected = new TreeMap<>();

    private List<VOTaxonJSF> taxonsVOAvailables = new ArrayList<>();
    /**
     * selected taxon from addFreetaxon()    
    */
    private List<VOTaxonJSF> taxonSearch;
    /**
     * list of taxon filter by length and biovolum
     */
    private List<VOTaxonJSF> taxonsLengthAvailables = new ArrayList<>();
    /**
     * map of Bourrelly class taxon
     */
    private Map<String, List<VOTaxonJSF>> preselectionTaxonsAvailables = new HashMap<>();
    
    /**
     * tree of Bourrelly class taxon
     */
    private TreeNode preselectionRootNodes = new DefaultTreeNode(ROOT, null, null);
    /**
     * tree of taxo tree
     */
    private TreeNode rootNodes = new DefaultTreeNode(ROOT, null, null);
    
    private String minLength = "0";
    private String maxLength = "20";
    private Boolean choiceBiovolume = false;
    private Boolean filterAnd = false;
    private String minBiovolume = "0";
    private String maxBiovolume = "10000";
    private Boolean choiceLength = false;

    /**
     * add infos to parameters
     * @param metadatasMap 
     */
    public void addTaxonstoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(PhytoplanctonParameters.AGGREGATED, aggregated);
        metadatasMap.put(PhytoplanctonParameters.PROPRIETE, propriete);
        metadatasMap.put(PhytoplanctonParameters.SOMMATION, sommation);
        metadatasMap.put(
                Taxon.class.getSimpleName(),
                getListTaxonsSelected()
        );
    }

    /**
     *
     * @return @throws BusinessException
     */
    /**
     * add all finded taxon on search by length
     * @return
     * @throws BusinessException 
     */
    public String addListTaxon() throws BusinessException {
        for (VOTaxonJSF taxonJSF : taxonsLengthAvailables) {
            selectTaxonVO(taxonJSF);
        }
        return null;
    }

    /**
     *
     * @return
     */
    public boolean getTaxonStepIsValid() {
        if (!taxonsSelected.isEmpty()) {
            return true;
        }

        return false;

    }

    /**
     *
     * @param localizationManager
     * @param phytoplanctonDatatypeManager
     */
    public void initProperties(ILocalizationManager localizationManager, IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager) {
        this.localizationManager = localizationManager;
        propertiesTaxonLevelNames = localizationManager.newProperties(TaxonNiveau.TABLE_NAME, "nom");
        this.phytoplanctonDatatypeManager = phytoplanctonDatatypeManager;
    }

    /**
     *
     * @return
     */
    public String getMinLength() {
        return minLength;
    }

    /**
     *
     * @param minLength
     */
    public void setMinLength(String minLength) {
        this.minLength = minLength;
    }

    /**
     *
     * @return
     */
    public String getMaxLength() {
        return maxLength;
    }

    /**
     *
     * @param maxLength
     */
    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    /**
     *
     * @return
     */
    public Boolean getChoiceBiovolume() {
        return choiceBiovolume;
    }

    /**
     *
     * @param choiceBiovolume
     */
    public void setChoiceBiovolume(Boolean choiceBiovolume) {
        this.choiceBiovolume = choiceBiovolume;
    }

    /**
     *
     * @return
     */
    public Boolean getFilterAnd() {
        return filterAnd;
    }

    /**
     *
     * @param filterAnd
     */
    public void setFilterAnd(Boolean filterAnd) {
        this.filterAnd = filterAnd;
    }

    /**
     *
     * @return
     */
    public String getMinBiovolume() {
        return minBiovolume;
    }

    /**
     *
     * @param minBiovolume
     */
    public void setMinBiovolume(String minBiovolume) {
        this.minBiovolume = minBiovolume;
    }

    /**
     *
     * @return
     */
    public String getMaxBiovolume() {
        return maxBiovolume;
    }

    /**
     *
     * @param maxBiovolume
     */
    public void setMaxBiovolume(String maxBiovolume) {
        this.maxBiovolume = maxBiovolume;
    }

    /**
     *
     * @return
     */
    public Boolean getChoiceLength() {
        return choiceLength;
    }

    /**
     *
     * @param choiceLength
     */
    public void setChoiceLength(Boolean choiceLength) {
        this.choiceLength = choiceLength;

    }

    /**
     * add all finded taxon serch by word contains
     * @return @throws BusinessException
     */
    public String addFreeTaxon() throws BusinessException {
        for (VOTaxonJSF taxonJSF : taxonSearch) {
            if (!taxonJSF.selected) {
                selectTaxonVO(taxonJSF);
            }
        }
        return null;
    }

    /**
     * init all list
     * @param phytoplanctonDatatypeManager
     * @param uIDate
     * @param uIPlatform
     * @param uIVariable 
     */
    public void updateTaxonsAvailables(IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager, UIDate uIDate, UIPlatform uIPlatform, UIVariable uIVariable) {
        List<IntervalDate> selectedDates = uIDate.getDatesRequestParam().getCurrentDatesFormParam().getSelectedDates();
        List<PlateformeProjetVO> selectedPlateformes = uIPlatform.getSitesPlatforms().values().stream().map(p -> p.getPlatform()).collect(Collectors.toList());
        List<DatatypeVariableUniteGLACPE> selectedVariables = uIVariable.getVariableSelected();
        taxonsVOAvailables.clear();
        preselectionTaxonsAvailables.clear();
        rootNodes = new DefaultTreeNode(ROOT, null, null);
        preselectionRootNodes = new DefaultTreeNode(ROOT, null, null);
        SortedMap<Taxon, SortedMap<String, ValeurProprieteTaxon>> taxons = phytoplanctonDatatypeManager.getAvailableTaxons(selectedPlateformes, selectedDates, selectedVariables);
        List<TreeNodeTaxon> rootChildNode = new ArrayList<>();
        List<TreeNodeTaxon> rootLeavesNode = new ArrayList<>();
        Map<String, TreeNode> taxonMap = new HashMap();
        for (Map.Entry<Taxon, SortedMap<String, ValeurProprieteTaxon>> taxonEntry : taxons.entrySet()) {
            Taxon taxon = taxonEntry.getKey();
            SortedMap<String, ValeurProprieteTaxon> valeurs = taxonEntry.getValue();
            VOTaxonJSF taxonVO = taxonsAvailables.computeIfAbsent(taxon.getId(),k->new VOTaxonJSF(taxon));
//                if (taxon.getTaxonNiveau().getCode().equals("genre_espece")) {
            if (taxon.isLeaf()) {
                Float length = Optional.ofNullable(valeurs.get(LONGUEUR_DE_LA_CELLULE)).map(v -> v.getFloatValue()).orElse(null);
                taxonVO.setCellLength(length);
                Float bioVolume = Optional.ofNullable(valeurs.get(VALEUR_DU_BIOVOLUME_SPECIFIQUE_CHOISI_POU)).map(v -> v.getFloatValue()).orElse(null);
                taxonVO.setBiovolume(bioVolume);
                String algalClass = Optional.ofNullable(valeurs.get(CLASSE_ALGALE_SENSU_BOURRELLY)).map(v -> v.getQualitativeValue()).map(vq -> vq.getValeur()).orElse(null);
                taxonVO.setAlgalClass(algalClass);
                if (algalClass != null) {
                    preselectionTaxonsAvailables
                            .computeIfAbsent(algalClass, k -> new ArrayList<>())
                            .add(taxonVO);
                }
            }
            addRecursiveTaxon(taxonMap, taxon);
            taxonsVOAvailables.add(taxonVO);
            taxonsAvailables.put(taxon.getId(), taxonVO);
            if (taxon.isLeaf()) {
                taxonsLengthAvailables.add(taxonVO);
            }
        }

        preselectionTaxonsAvailables.keySet()
                .stream()
                .forEach(preselectionTaxonName -> new TreeNodeAlgal(new VOAlgalJSF(preselectionTaxonName), preselectionTaxonsAvailables.get(preselectionTaxonName)));

    }

    /**
     * select a taxon
     * @param algal
     * @return
     * @throws BusinessException 
     */
    public String selectTaxonVO(VOAlgalJSF algal) throws BusinessException {
        preselectionTaxonsAvailables.getOrDefault(algal.getNom(), new ArrayList<>())
                .stream()
                .forEach(tvo -> selectTaxonVO(tvo));
        algal.setSelected(!algal.getSelected());
        return null;
    }
    
    /**
     *
     * @param taxonsSelecteds
     * @return
     */
    public String selectTaxonVO(VOTaxonJSF taxonsSelecteds) {
        if (taxonsSelecteds.getSelected()) {
            getTaxonsSelected().remove(taxonsSelecteds.getTaxon().getId());
        } else {
            getTaxonsSelected().putIfAbsent(taxonsSelecteds.getTaxon().getId(), taxonsSelecteds);
        }
        if (detail) {
            reccursiveTaxonAdd(taxonsSelecteds.getSelected(), taxonsSelecteds);
        }
        taxonsSelecteds.setSelected(!taxonsSelecteds.getSelected());
        selectSynonyme(taxonsSelecteds.getSelected(), taxonsSelecteds);
        return null;
    }

    /**
     * 
     * select a taxon
     * @param event 
     */
    public void onNodeSelect(NodeSelectEvent event) {
        TreeNode treeNode = event.getTreeNode();
        selectTreeNode(treeNode);
    }

    /**
     * 
     * select a node
     * @param treeNode
     * @param event 
     */
    public void selectTreeNode(TreeNode treeNode) {
        final VOTaxonJSF taxon = (VOTaxonJSF) treeNode.getData();
        if (treeNode.getData() != null) {
            treeNode.setSelected(!treeNode.isSelected());
            treeNode.setExpanded(!treeNode.isExpanded());
        }
        if (treeNode.getChildren() != null && treeNode.getChildren().size() > 0) {
            treeNode.getChildren().forEach(this::selectTreeNode);
        } else {
            selectTaxonVO(taxon);
        }

    }

    /**
     * add all leaf taxons
     * @return
     */
    public String addAllTaxons() {
        removeAllTaxons();
        sommation = false;
        detail = true;
        for (TreeNode tnt : rootNodes.getChildren()) {

            selectTreeNode(tnt);
        }
        for (VOTaxonJSF taxon : taxonsVOAvailables) {
            if (taxon.getTaxon().isLeaf()) {
//            if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                getTaxonsSelected().putIfAbsent(taxon.getTaxon().getId(), taxon);
                taxon.setSelected(true);
            }
        }
        return null;
    }

    /**
     * remove all selected taxons
     * @return
     */
    public String removeAllTaxons() {
        getTaxonsSelected().clear();
        for (VOTaxonJSF taxon : taxonsVOAvailables) {
            taxon.setSelected(false);
        }
        for (TreeNode algal : preselectionRootNodes.getChildren()) {
            ((TreeNodeAlgal) algal).getAlgalClass().setSelected(false);
        }
        for (TreeNode tnt : rootNodes.getChildren()) {
            enableOrDisableAllLeaves(true, tnt);
        }
        return null;
    }

    private void enableOrDisableAllLeaves(boolean selected, TreeNode TNTaxon) {
        if (!TNTaxon.isExpanded()) {
            TNTaxon.setExpanded(true);
        }
        enableOrDisableLeaf(selected, TNTaxon);
        if (detail) {
            if (TNTaxon.getChildren() != null && !TNTaxon.getChildren().isEmpty()) {
                for (TreeNode t : TNTaxon.getChildren()) {
                    enableOrDisableLeaf(selected, t);
                    if ((t.getChildren() != null && !t.getChildren().isEmpty())) {
                        enableOrDisableAllLeaves(selected, t);
                    }
                }
            }
        }
    }

    private void enableOrDisableLeaf(Boolean conditionDisabling, TreeNode taxon) {
        Map<Long, VOTaxonJSF> taxonsSelecteds = getTaxonsSelected();
        if (conditionDisabling) {
            taxonsSelecteds.remove(((VOTaxonJSF) taxon.getData()).getTaxon().getId());
        } else {
            taxonsSelecteds.put(((VOTaxonJSF) taxon.getData()).getTaxon().getId(), ((VOTaxonJSF) taxon.getData()));
        }
        ((VOTaxonJSF) taxon.getData()).setSelected(!conditionDisabling);
        // selectSynonyme(conditionDisabling, taxon.getTaxon());
    }

    /**
     *
     * @param selected
     * @param taxonSelected
     */
    public void selectSynonyme(Boolean selected, VOTaxonJSF taxonSelected) {
        List<TaxonProprieteTaxon> taxonProprietesTaxon = phytoplanctonDatatypeManager.retrieveTaxonPropertyByTaxonId(taxonSelected.getTaxon().getId());
        if (taxonProprietesTaxon != null && !taxonProprietesTaxon.isEmpty()) {
            String synonymes = "";
            for (TaxonProprieteTaxon taprota : taxonProprietesTaxon) {
                if (taprota.getProprieteTaxon().getCode().equals("synonyme_ancien") || taprota.getProprieteTaxon().getCode().equals("synonyme_recent")) {
                    synonymes = synonymes.concat(taprota.getValeurProprieteTaxon().getStringValue()).concat(",");
                }
            }
            if (synonymes.length() != 0) {
                for (String synonyme : synonymes.split(",")) {
                    if (synonyme.length() > 0) {
                        Taxon taxon = phytoplanctonDatatypeManager.retrieveTaxonByCode(Utils.createCodeFromString(synonyme)).orElse(null);
                        if (taxon != null) {
                            VOTaxonJSF taxonJSF = taxonsAvailables.get(taxon.getId());
                            taxonJSF.setSelected(!selected);
                            if (selected) {
                                getTaxonsSelected().remove(taxon.getId());
                            } else {
                                getTaxonsSelected().putIfAbsent(taxon.getId(), taxonJSF);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param selected
     * @param taxonCurrent
     */
    public void reccursiveTaxonAdd(Boolean selected, VOTaxonJSF taxonCurrent) {
        for (Taxon taxon : taxonCurrent.getTaxon().getTaxonsEnfants()) {
            if (getTaxonsSelected().keySet().contains(taxon.getId())) {
                if (selected) {
                    getTaxonsSelected().remove(taxon.getId());
                } else {
                    getTaxonsSelected().putIfAbsent(taxon.getId(), taxonsAvailables.get(taxon.getId()));
                }
            }
            reccursiveTaxonAdd(selected, taxonsAvailables.get(taxon.getId()));
        }
    }

    /**
     *
     * @return
     */
    public Boolean getPropriete() {
        return propriete;
    }

    /**
     *
     * @return
     */
    public Boolean getSommation() {
        return sommation;
    }

    /**
     *
     * @return
     */
    public Boolean getAggregated() {
        return aggregated;
    }

    /**
     *
     * @return
     */
    public Boolean getGenre() {
        return genre;
    }

    /**
     * only taxon of genre_espece
     * @param genre
     */
    public void setGenre(Boolean genre) {
        this.genre = genre;
        try {
            filterLists();
        } catch (BusinessException e) {
            LoggerFactory.getLogger(getClass()).error(e.getMessage(), e);
        }

    }

    /**
     *
     * @throws BusinessException
     */
    public void filterLists() throws BusinessException {
        taxonsVOAvailables.clear();

        for (VOTaxonJSF taxon : taxonsAvailables.values()) {
            if (genre) {
//                if (taxon.getTaxon().getTaxonNiveau().getCode().equals("genre_espece")) {
                if (taxon.getTaxon().isLeaf()) {
                    taxonsVOAvailables.add(taxon);
                }
            } else {
                taxonsVOAvailables.add(taxon);
            }
        }
        updateTaxonList();
        updateList();
    }

    /**
     *
     * @return
     */
    public Map<Long, VOTaxonJSF> getTaxonsAvailables() {
        return taxonsAvailables;
    }

    /**
     *
     * @return
     */
    public SortedMap<Long, VOTaxonJSF> getTaxonsSelected() {
        return taxonsSelected;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getListTaxonsSelected() {
        return taxonsSelected.values()
                .stream()
                //.filter(vo->vo.getTaxon().isLeaf())
                .collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    public Boolean getDetail() {
        return detail;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonsVOAvailables() {
        return taxonsVOAvailables;
    }

    /**
     *
     * @param taxonsVOAvailables
     */
    public void setTaxonsVOAvailables(List<VOTaxonJSF> taxonsVOAvailables) {
        this.taxonsVOAvailables = taxonsVOAvailables;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonsLengthAvailables() {
        return taxonsLengthAvailables;
    }

    /**
     *
     * @param taxonsLengthAvailables
     */
    public void setTaxonsLengthAvailables(List<VOTaxonJSF> taxonsLengthAvailables) {
        this.taxonsLengthAvailables = taxonsLengthAvailables;
    }

    /**
     *
     * @return
     */
    public Map<String, List<VOTaxonJSF>> getPreselectionTaxonsAvailables() {
        return preselectionTaxonsAvailables;
    }

    /**
     *
     * @param propriete
     */
    public void setPropriete(Boolean propriete) {
        this.propriete = propriete;
    }

    /**
     *
     * @param sommation
     */
    public void setSommation(Boolean sommation) {
        this.sommation = sommation;
    }

    /**
     *
     * @param detail
     */
    public void setDetail(Boolean detail) {
        this.detail = detail;
    }

    /**
     *
     * @param preselectionTaxonsAvailables
     */
    public void setPreselectionTaxonsAvailables(Map<String, List<VOTaxonJSF>> preselectionTaxonsAvailables) {
        this.preselectionTaxonsAvailables = preselectionTaxonsAvailables;
    }

    /**
     *
     * @return
     */
    public TreeNode getRootNodes() {
        return rootNodes;
    }

    /**
     *
     * @param rootNodes
     */
    public void setRootNodes(TreeNode rootNodes) {
        this.rootNodes = rootNodes;
    }

    /**
     *
     * @return
     */
    public TreeNode getPreselectionRootNodes() {
        return preselectionRootNodes;
    }

    /**
     *
     * @param preselectionRootNodes
     */
    public void setPreselectionRootNodes(TreeNode preselectionRootNodes) {
        this.preselectionRootNodes = preselectionRootNodes;
    }

    /**
     *
     * @return
     */
    public List<VOTaxonJSF> getTaxonSearch() {
        return taxonSearch;
    }

    /**
     *
     * @param taxonSearch
     */
    public void setTaxonSearch(List<VOTaxonJSF> taxonSearch) {
        this.taxonSearch = taxonSearch;
    }

    /**
     * update list on taxonSearch
     * @return 
     */
    public String updateTaxonList() {
        taxonsVOAvailables.clear();
        taxonsVOAvailables = (taxonSearch == null ? taxonsAvailables.values() : taxonSearch).stream()
                .filter(t -> !genre || t.taxon.isLeaf())
                .collect(Collectors.toList());
        return null;
    }

    /**
     * update liste on legth search
     * @return
     */
    public String updateList() {
        taxonsLengthAvailables = (taxonSearch == null ? taxonsAvailables.values() : taxonSearch)
                .stream()
                .filter(this::isFilteredtaxon)
                .collect(Collectors.toList());
        return null;
    }

    /**
     * filter taxon on length and biovolume
     * @param taxon
     * @return 
     */
    public boolean isFilteredtaxon(VOTaxonJSF taxon) {
        boolean isFilteredtaxon = false;
        if(genre && !taxon.getTaxon().isLeaf()){
            return false;
        }
        if (choiceLength && taxon.getCellLength() != null
                && taxon.getCellLength() > Float.parseFloat(minLength) && taxon.getCellLength() < Float.parseFloat(maxLength)
                && taxon.getTaxon().isLeaf()) {
            isFilteredtaxon = true;
            if (!filterAnd) {
                return true;
            }
        }
        if (choiceBiovolume) {
            if ((taxon.getBiovolume() != null)
                    && taxon.getBiovolume() > Float.parseFloat(minBiovolume) && taxon.getBiovolume() < Float.parseFloat(maxBiovolume)
                    && taxon.getTaxon().isLeaf()) {
                if (!filterAnd) {
                    return true;
                }
            } else if (taxon.getBiovolume() != null && filterAnd) {
                return false;
            }
        }
        return isFilteredtaxon;
    }

    private TreeNode addRecursiveTaxon(Map<String, TreeNode> taxonMap, Taxon taxon) {
        Taxon taxonParent = taxon.getTaxonParent();
        if (taxonParent == null) {
            return rootNodes;
        }
        boolean leaf = taxon.isLeaf();
        TreeNode parentNode = taxonMap.get(taxonParent.getCode());
        if (parentNode == null) {
            parentNode = addRecursiveTaxon(taxonMap, taxonParent);
        }
        final DefaultTreeNode taxonNode = new DefaultTreeNode(leaf ? LEAF : PARENT, taxonsAvailables.computeIfAbsent(taxon.getId(), k->new VOTaxonJSF(taxon)), parentNode);
        taxonMap.put(taxon.getCode(), taxonNode);
        return taxonNode;
    }

    /**
     *
     */
    public class VOTaxonJSF {

        private Taxon taxon;
        private Float cellLength;
        private Float biovolume;
        private String algalClass;
        private String localizedName;
        private String localizedLevelName;
        private Boolean selected;

        /**
         *
         * @param taxon
         */
        public VOTaxonJSF(Taxon taxon) {
            super();
            this.taxon = taxon;
            this.selected = false;
            if (taxon != null) {
                this.localizedName = taxon.getNomLatin();
                this.localizedLevelName = propertiesTaxonLevelNames.getProperty(taxon.getTaxonNiveau().getNom(), taxon.getTaxonNiveau().getNom());
                if (Strings.isNullOrEmpty(this.localizedLevelName)) {
                    this.localizedLevelName = taxon.getTaxonNiveau().getNom();
                }
            }
        }

        /**
         *
         * @return
         */
        public Taxon getTaxon() {
            return taxon;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public Float getCellLength() {
            return cellLength;
        }

        /**
         *
         * @param cellLength
         */
        public void setCellLength(Float cellLength) {
            this.cellLength = cellLength;
        }

        /**
         *
         * @return
         */
        public Float getBiovolume() {
            return biovolume;
        }

        /**
         *
         * @param biovolume
         */
        public void setBiovolume(Float biovolume) {
            this.biovolume = biovolume;
        }

        /**
         *
         * @return
         */
        public String getAlgalClass() {
            return algalClass;
        }

        /**
         *
         * @param algalClass
         */
        public void setAlgalClass(String algalClass) {
            this.algalClass = algalClass;
        }

        /**
         *
         * @return
         */
        public String getLocalizedName() {
            return localizedName;
        }

        /**
         *
         * @param localizedName
         */
        public void setLocalizedName(String localizedName) {
            this.localizedName = localizedName;
        }

        /**
         *
         * @return
         */
        public String getLocalizedLevelName() {
            return localizedLevelName;
        }

        /**
         *
         * @param localizedLevelName
         */
        public void setLocalizedLevelName(String localizedLevelName) {
            this.localizedLevelName = localizedLevelName;
        }
    }

    /**
     *
     */
    public class VOAlgalJSF {

        private String nom;
        private Boolean selected = false;

        /**
         *
         * @param nom
         */
        public VOAlgalJSF(String nom) {
            super();
            this.nom = nom;
        }

        /**
         *
         * @return
         */
        public Boolean getSelected() {
            return selected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        /**
         *
         * @return
         */
        public String getNom() {
            return nom;
        }

        /**
         *
         * @param nom
         */
        public void setNom(String nom) {
            this.nom = nom;
        }
    }

    /**
     *
     */
    public class TreeNodeAlgal extends DefaultTreeNode {

        private Boolean expanded = false;

        /**
         *
         * @param algal
         * @param preselectionTaxons
         */
        public TreeNodeAlgal(VOAlgalJSF algal, List<VOTaxonJSF> preselectionTaxons) {
            super(ALGAL, algal, preselectionRootNodes);
            for (VOTaxonJSF taxon : preselectionTaxons) {
                new TreeNodeTaxon(LEAF, taxon, this);
            }
        }

        /**
         *
         * @return
         */
        public VOAlgalJSF getAlgalClass() {
            return (VOAlgalJSF) getData();
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }
    }

    /**
     *
     */
    public class TreeNodeTaxon extends DefaultTreeNode {

        private List<TreeNodeTaxon> children;
        private List<TreeNodeTaxon> leaves;
        private Boolean expanded = false;

        /**
         *
         * @param taxon
         */
        public TreeNodeTaxon(VOTaxonJSF taxon) {
            super("defaut", taxon, null);
        }

        /**
         *
         * @param taxon
         * @param parent
         */
        public TreeNodeTaxon(VOTaxonJSF taxon, TreeNode parent) {
            super(taxon, parent);
        }

        /**
         *
         * @param type
         * @param taxon
         * @param parent
         */
        public TreeNodeTaxon(String type, VOTaxonJSF taxon, TreeNode parent) {
            super(type, taxon, parent);
        }

        /**
         *
         * @return
         */
        public Boolean getExpanded() {
            return expanded;
        }

        /**
         *
         * @param expanded
         */
        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }

        /**
         *
         * @return
         */
        public VOTaxonJSF getTaxon() {
            return (VOTaxonJSF) getData();
        }

        /**
         *
         * @return
         */
        public List<TreeNode> getChildren() {
            if (children == null || children.isEmpty()) {
                children = new ArrayList<>();
                if (!getTaxon().getTaxon().getTaxonsEnfants().isEmpty() && (leaves == null || leaves.isEmpty())) {
                    leaves = getLeaves();
                    for (Taxon taxonEnfant : getTaxon().getTaxon().getTaxonsEnfants()) {
                        if (taxonEnfant.getTaxonsEnfants().isEmpty()) {
                            leaves.add(new TreeNodeTaxon(taxonsAvailables.computeIfAbsent(taxonEnfant.getId(), k->new VOTaxonJSF(taxonEnfant))));
                        } else {
                            children.add(new TreeNodeTaxon(taxonsAvailables.computeIfAbsent(taxonEnfant.getId(),k-> new VOTaxonJSF(taxonEnfant))));
                        }
                    }
                }
            }
            return children.stream().collect(Collectors.toList());
        }

        /**
         *
         * @return
         */
        @Override
        public int getChildCount() {
            return (children == null || children.isEmpty()) ? 0 : children.size();
        }

        /**
         *
         * @return
         */
        @Override
        public boolean isLeaf() {
            if (children == null || children.isEmpty()) {
                return true;
            }
            return false;
        }

        /**
         *
         * @return
         */
        public List<TreeNodeTaxon> getLeaves() {
            if (leaves == null) {
                leaves = new ArrayList<>();
            }
            return leaves;
        }

        /**
         *
         * @param leaves
         */
        public void setLeaves(List<TreeNodeTaxon> leaves) {
            this.leaves = leaves;
        }

    }
}

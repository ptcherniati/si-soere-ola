/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;

import java.util.*;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 * @author tcherniatinsky
 */
public class UIDate {

    /**
     * The helper form @link(HelperForm).
     */
    HelperForm helperForm = new HelperForm();
    /**
     * The dates request param @link(DatesRequestParamVO).
     */
    private DatesRequestParamVO datesRequestParam;
    private ILocalizationManager localizationManager;

    /**
     *
     */
    public UIDate() {
    }

    /**
     * Gets the dates request param.
     *
     * @return the dates request param
     */
    public DatesRequestParamVO getDatesRequestParam() {
        return this.datesRequestParam;
    }

    /**
     * @param datesRequestParam
     */
    public void setDatesRequestParam(DatesRequestParamVO datesRequestParam) {
        this.datesRequestParam = datesRequestParam;
    }

        /**
         *
         * @return
         */
        public Boolean getDateStepIsValid() {

            if ((datesRequestParam.getSelectedFormSelection() != null) && datesRequestParam.getCurrentDatesFormParam().getIsValid()) {
                return true;
            } else {
                return false;
            }
        }

    /**
     * Inits the dates request param.
     *
     * @param localizationManager
     */
    public void initDatesRequestParam(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        datesRequestParam = new DatesRequestParamVO(localizationManager);
    }

    /**
     * @param metadatasMap
     */
    public void addDatestoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(AbstractDatesFormParam.class.getSimpleName(), getDatesRequestParam());
        metadatasMap.put(IntervalDate.class.getSimpleName(), getDatesRequestParam().getCurrentDatesFormParam().getSelectedDates());
    }

    /**
     * The Class HelperForm.
     */
    public static class HelperForm {

        /**
         * The index @link(Integer).
         */
        Integer index;
        /**
         * The key @link(String).
         */
        String key;
        /**
         * The value @link(String).
         */
        String value;

        /**
         * Gets the index.
         *
         * @return the index
         */
        public Integer getIndex() {
            return this.index;
        }

        /**
         * Sets the index.
         *
         * @param index the new index
         */
        public final void setIndex(final Integer index) {
            this.index = index;
        }

        /**
         * Gets the key.
         *
         * @return the key
         */
        public String getKey() {
            return this.key;
        }

        /**
         * Sets the key.
         *
         * @param key the new key
         */
        public final void setKey(final String key) {
            this.key = key;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }

        /**
         * Sets the value.
         *
         * @param value the new value
         */
        public final void setValue(final String value) {
            this.value = value;
        }
    }

}

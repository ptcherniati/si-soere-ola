/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;

import java.util.*;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * @author tcherniatinsky
 */
public class UIPlatform {

    /**
     *
     */
    public static final String ICON_BLANK = "ui-icon ui-icon-blank";

    /**
     *
     */
    public static final String ICON_CHECK = "ui-icon ui-icon-check";

    /**
     *
     */
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";

    /**
     * The properties affichage platform @link(Properties).
     */
    static Properties propertiesAffichagePlatform;

    /**
     * The properties names site @link(Properties).
     */
    static Properties propertiesNamesSite;
    static Properties propertiesNamesProject;
    static Properties propertiesNamesPlatform;
    /**
     * The sites platforms @link(Map<Long,PlatformVO>).
     */
    final Map<Long, PlatformVO> sitesPlatforms = new HashMap();

    /**
     * The availables projects.
     */
    TreeNode availablesProjects = new DefaultTreeNode("Root", null);
    private TreeNode selectedNode;
    private ILocalizationManager localizationManager;

    /**
     * Gets the sites platforms.
     *
     * @return the sites platforms
     */
    public Map<Long, PlatformVO> getSitesPlatforms() {

        return this.sitesPlatforms;
    }

    /**
     *
     * @return
     */
    public TreeMap<String, TreeSet<PlatformVO>> getPlatformBySitesAndProjets() {
        return sitesPlatforms.values().stream()
                .collect(
                        Collectors.groupingBy(
                                t -> ((NodeableNode) t.getParent()).getLocalizedDisplayPath(),
                                TreeMap::new,
                                Collectors.toCollection(TreeSet::new)
                        )
                );
    }

    /**
     * Adds the all sites.
     *
     * @return the string
     */
    public final String addAllSites() {
        sitesPlatforms.clear();
        for (final TreeNode projetNode : this.availablesProjects.getChildren()) {
            for (final TreeNode platformLeafNode : projetNode.getChildren()) {
                PlatformVO platform = (PlatformVO) platformLeafNode.getData();
                platform.setSelected(true);
                sitesPlatforms.put(platform.platform.getPlateforme().getId(), platform);
                setExpanded(platformLeafNode, true);
            }
        }
        return null;
    }

    /**
     * Removes the all sites.
     *
     * @return the string
     */
    public final String removeAllSites() {
        sitesPlatforms.clear();
        this.availablesProjects.getChildren().forEach((projetNode) -> {
            for (final TreeNode platformLeafNode : projetNode.getChildren()) {
                PlatformVO platform = (PlatformVO) platformLeafNode.getData();
                platform.setSelected(false);
                setExpanded(platformLeafNode, false);
            }
            ;
        });
        return null;
    }

    /**
     * Inits the properties.
     *
     * @param localizationManager
     * @param platformManager
     */
    public void initProperties(ILocalizationManager localizationManager, IPlatformManager platformManager) {
        this.localizationManager = localizationManager;
        propertiesNamesSite = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        propertiesNamesPlatform = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        propertiesNamesProject = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        updatePlatformsAvailables(platformManager);
    }

    /**
     * Select platform.
     *
     * @param event
     */
    public final void selectNode(NodeSelectEvent event) {
        TreeNode selectedNode = event.getTreeNode();
        final String type = selectedNode.getType();
        boolean isBlank = ((NodeableNode) selectedNode).getState().equals(ICON_BLANK);
        if (NodeType.PROJECT.equals(type)) {
            ((NodeableNode) selectedNode).getChildren().forEach(
                    child -> selectPlatform((PlatformVO) child, isBlank)
            );
        } else {
            final PlatformVO platformVO = (PlatformVO) selectedNode;
            selectPlatform(platformVO, !(platformVO).isSelected());
        }
    }

    /**
     *
     * @param platform
     * @return
     */
    public String removePlatform(PlatformVO platform) {
        selectPlatform(platform, false);
        return "";
    }

    private final void selectPlatform(PlatformVO platform, boolean selected) {
        platform.setSelected(selected);
        if (selected) {
            sitesPlatforms.put(platform.getPlatform().getPlateforme().getId(), platform);
        } else {
            sitesPlatforms.remove(platform.getPlatform().getPlateforme().getId());
        }
        platform.setSelected(selected);
        setExpanded(platform, selected);
    }

    /**
     * Update agro availables.
     *
     * @param platformManager
     */
    public final void updatePlatformsAvailables(IPlatformManager platformManager) {
        this.availablesProjects = new DefaultTreeNode("Root", null);
        Collection<PlateformeProjetVO> availablesPlatforms = platformManager.getAvailablePlatforms();
        Map<Projet, TreeNode> projectsNodes = new HashMap();
        TreeNode projectTreeNode, siteTreeNode;
        for (final PlateformeProjetVO plateform : availablesPlatforms) {
            SiteGLACPE site = plateform.getSite();
            Projet projet = plateform.getProjet();
            if (!projectsNodes.containsKey(projet)) {
                final NodeableNode projectNodeableNode = new NodeableNode(projet, availablesProjects);
                projectNodeableNode.setType(NodeType.PROJECT);
                projectsNodes.put((Projet) projet, projectNodeableNode);
            }
            projectTreeNode = projectsNodes.get(projet);
            new PlatformVO(new PlatformNodeable((PlateformeProjetVO) plateform), NodeType.PLATFORM, projectTreeNode, plateform);
        }
    }

    /**
     * Gets the platform step is valid.
     *
     * @return the platform step is valid
     */
    public Boolean getPlatformStepIsValid() {
        return !this.sitesPlatforms.isEmpty();
    }

    /**
     * @param node
     * @param expanded
     */
    public void setExpanded(TreeNode node, boolean expanded) {
        node.setExpanded(expanded);
        if (node.getParent() != null) {
            setExpanded(node.getParent(), expanded);
        }
    }

    /**
     * Gets the availables projects.
     *
     * @return the availables projects
     */
    public final TreeNode getAvailablesProjects() {
        return this.availablesProjects;
    }

    /**
     * @return
     */
    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode
     */
    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    /**
     * Gets the list platform.
     *
     * @return the list platform
     */
    public List<Plateforme> getListPlatform() {
        final List<Plateforme> platforms = new LinkedList();
        for (final PlatformVO platform : this.sitesPlatforms.values()) {
            platforms.add(platform.getPlatform().getPlateforme());
        }
        return platforms;
    }

    /**
     * @param metadatasMap
     */
    public void addSitetoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(SiteGLACPE.class.getSimpleName(), getListPlatform());
    }

    /**
     * @param metadatasMap
     */
    public void addPlatformtoMap(Map<String, Object> metadatasMap) {
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<Plateforme>>> platformtoMap = sitesPlatforms.values().stream()
                .collect(Collectors.groupingBy(
                        p->p.getPlatform().getProjet(),
                        TreeMap::new,
                        Collectors.collectingAndThen(
                                Collectors.toList(),
                                t1 -> t1.stream().collect(
                                        Collectors.groupingBy(
                                                p->p.getPlatform().getSite(),
                                                TreeMap::new,
                                                Collectors.mapping(
                                                        p->p.getPlatform().getPlateforme(),
                                                        Collectors.toCollection(TreeSet::new))
                                        )
                                )
                        )
                )
                );
        metadatasMap.put(Plateforme.class.getSimpleName(), platformtoMap);
    }

    /**
     * The Class NodeType.
     */
    public static class NodeType {

        /**
         * The project.
         */
        public static final String PROJECT = "project";
        /**
         * The site.
         */
        public static final String SITE = "site";
        /**
         * The platform.
         */
        public static final String PLATFORM = "platform";

        private NodeType() {
        }
    }

    /**
     *
     */
    public class NodeableNode extends DefaultTreeNode implements Comparable<NodeableNode>, TreeNode {

        /**
         * The platform.
         */
        INodeable nodeable;

        /**
         *
         * @param in
         * @param tn
         */
        public NodeableNode(INodeable nodeable, TreeNode parent) {
            super(nodeable, parent);
            this.nodeable = nodeable;
        }

        /**
         *
         * @param in
         * @param string
         * @param o
         * @param tn
         */
        public NodeableNode(INodeable nodeable, String type, Object data, TreeNode parent) {
            super(type, data, parent);
            this.nodeable = nodeable;
        }

        /**
         * @param in
         * @param nodeable
         */
        public NodeableNode(INodeable nodeable) {
            this.nodeable = nodeable;
        }

        /**
         * @return
         */
        public String getName() {
            return localizationManager.getLocalName(nodeable);
        }

        /**
         *
         * @return
         */
        public String getLocalizedDisplayPath() {
            return getType().equals(NodeType.PROJECT)
                    ? propertiesNamesProject.getProperty(Utils.createCodeFromString(getName()), getName())
                    : propertiesNamesSite.getProperty(getName(), getName());
        }

        /**
         *
         * @param t
         * @return
         */
        @Override
        public int compareTo(NodeableNode t) {
            return getName().compareTo(t.getName());
        }

        /**
         *
         * @return
         */
        public String getState() {
            if (getType().equals(NodeType.PROJECT)) {
                if (getChildren().stream().allMatch(child -> child.isSelected())) {
                    return ICON_CHECK;
                }
                if (getChildren().stream().anyMatch(child -> child.isSelected())) {
                    return ICON_INDETERMINATE;
                }
            } else {
                return isSelected() ? ICON_CHECK : ICON_BLANK;
            }
            return ICON_BLANK;
        }

        /**
         *
         * @return
         */
        @Override
        public Object getData() {
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public String toString() {
            return "NodeableNode{" + "nodeable=" + nodeable + ", nodeableNodeSelected=" + isSelected() + '}';
        }
    }

    /**
     *
     */
    public class PlatformNodeable extends Nodeable {

        private PlateformeProjetVO platform;

        /**
         *
         * @param platform
         */
        public PlatformNodeable(PlateformeProjetVO platform) {
            super(platform.getPlateforme().getCode());
            this.platform = platform;
        }

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return platform.getPlateforme().getName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) PlatformNodeable.class;
        }

        /**
         *
         * @return
         */
        public Plateforme getPlatform() {
            return this.platform.getPlateforme();
        }
    }

    /**
     *
     */
    public class PlatformVO extends NodeableNode {

        /**
         * The platform.
         */
        PlateformeProjetVO platform;

        /**
         *
         * @param pn
         * @param o
         * @param tn
         * @param ppvo
         */
        public PlatformVO(PlatformNodeable platformNodeable, Object data, TreeNode parent, PlateformeProjetVO platform) {
            super(platformNodeable, NodeType.PLATFORM, data, parent);
            this.platform = platform;
        }

        /**
         * Gets the localized affichage.
         *
         * @return the localized affichage
         */
        public String getLocalizedAffichage() {
            final String localizedAffichage = propertiesNamesPlatform.getProperty(this.platform.getPlateforme().getName(), this.platform.getPlateforme().getName());
            final String localizedName = propertiesNamesSite.getProperty(platform.getSite().getName(), platform.getSite().getName());
            return String.format("%s (%s)", localizedName, localizedAffichage);
        }

        /**
         * Gets the localized display path.
         *
         * @return the localized display path
         */
        public String getLocalizedDisplayPath() {
            return getLocalizedAffichage();
        }

        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return this.platform.getPlateforme().getName();
        }

        /**
         * Gets the platform.
         *
         * @return the platform
         */
        public PlateformeProjetVO getPlatform() {
            return this.platform;
        }
    }
}

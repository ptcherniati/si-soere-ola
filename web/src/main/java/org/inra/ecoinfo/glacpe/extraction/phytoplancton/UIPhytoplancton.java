/*
 *
 */
package org.inra.ecoinfo.glacpe.extraction.phytoplancton;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIAssociate;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIDate;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIPhytoTaxon;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIPlatform;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIVariable;
import org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl.PhytoplanctonParameters;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UISondeMulti.
 */
@ManagedBean(name = "uiPhytoplancton")
@ViewScoped
public class UIPhytoplancton extends AbstractUIBeanForSteps implements Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;
    private static final String DEFAULT_UNCERTAINTY = "0";
    private static final String DEFAULT_TARGETVALUE = "0";

    /**
     * The affichage @link(String).
     */
    String affichage = "1";

    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{phytoplanctonDatatypeManager}")
    IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;
    
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;

    /**
     * The parameters request @link(ParametersRequest).
     */
    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The properties variables names @link(Properties).
     */
    Properties propertiesVariablesNames;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    UIPlatform uIPlatform;
    UIDate uIDate;
    UIVariable uIVariable;
    UIPhytoTaxon uiTaxon;
    UIAssociate uIAssociate;

    private ExtractionParametersVO currentParameterExtraction;
    // GETTERS SETTERS - BEANS
    /**
     * Instantiates a new uI bean mps.
     */
    public UIPhytoplancton() {
        super();
    }

    /**
     *
     * @return
     */
    public UIPlatform getuIPlatform() {
        return uIPlatform;
    }

    /**
     *
     * @return
     */
    public UIDate getuIDate() {
        return uIDate;
    }

    /**
     *
     * @return
     */
    public UIVariable getuIVariable() {
        return uIVariable;
    }

    /**
     *
     * @param uIVariable
     */
    public void setuIVariable(UIVariable uIVariable) {
        this.uIVariable = uIVariable;
    }

    /**
     *
     * @return
     */
    public UIPhytoTaxon getuITaxon() {
        return uiTaxon;
    }

    /**
     *
     * @param uiTaxon
     */
    public void setuIDepth(UIPhytoTaxon uiTaxon) {
        this.uiTaxon = uiTaxon;
    }

    /**
     *
     * @return
     */
    public UIAssociate getuIAssociate() {
        return uIAssociate;
    }

    /**
     *
     * @param uIAssociate
     */
    public void setuIAssociate(UIAssociate uIAssociate) {
        this.uIAssociate = uIAssociate;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap<>();
        // TODO
        uIPlatform.addPlatformtoMap(metadatasMap);
        uIDate.addDatestoMap(metadatasMap);
        uIVariable.addVariabletoMap(metadatasMap);
        uiTaxon.addTaxonstoMap(metadatasMap);
//        uIPlatform.addTreatmenttoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final PhytoplanctonParameters parameters = new PhytoplanctonParameters(metadatasMap);
//        if(this.parametersRequest.depthRequestParam.getAvailablesDepth().isEmpty()){
//            updateDepthAvailables();
//        }
//        if (this.parametersRequest.depthRequestParam.getChoiceDepth()) {
//            parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam.getSelectedsDepths());
//            if (parameters.getSelectedDepth().isEmpty()) {
//                parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam.getAvailablesDepth());
//            }
//        } else {
//            parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam
//                    .getAvailablesDepth().subSet(
//                            this.parametersRequest.depthRequestParam.getDepthMin(),
//                            this.parametersRequest.depthRequestParam.getDepthMax() + 1));
//        }
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, getAffichage().equals("1")?1:2);
        return null;
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public final String getAffichage() {
        return this.affichage;
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (this.getStep()) {
            case 1:
                return this.uIPlatform.getPlatformStepIsValid();
            case 2:
                return this.uIDate.getDateStepIsValid();
            case 3:
                return uIVariable.getVariableStepIsValid();
            default:
                return uiTaxon.getTaxonStepIsValid();
        }
    }

    /**
     *
     * @return
     */
    public IPhytoplanctonDatatypeManager getPhytoplanctonDatatypeManager() {
        return phytoplanctonDatatypeManager;
    }

    /**
     * Gets the parameters request.
     *
     * @return the parameters request
     */
    public final ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    final void initProperties() {
        setEnabledMotivation(false);
        uIPlatform = new UIPlatform();
        uIPlatform.initProperties(localizationManager, phytoplanctonDatatypeManager);
        uIDate = new UIDate();
        uIDate.initDatesRequestParam(localizationManager);
        uIVariable = new UIVariable();
        uIVariable.initProperties(localizationManager, phytoplanctonDatatypeManager);
        uiTaxon = new UIPhytoTaxon();
        uiTaxon.initProperties(localizationManager, phytoplanctonDatatypeManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "phytoplancton";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (this.getStep()) {
            case 3:
                uIVariable.updateVariablesAvailables(phytoplanctonDatatypeManager, uIDate, uIPlatform);
                break;
            case 4:
                uiTaxon.updateTaxonsAvailables(phytoplanctonDatatypeManager, uIDate, uIPlatform, uIVariable);
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public final void setAffichage(final String affichage) {
        this.affichage = affichage;
    }

    /**
     * Sets the extraction manager.
     *
     * @param extractionManager the new extraction manager
     */
    public final void setExtractionManager(final IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param phytoplanctonDatatypeManager
     */
    public void setPhytoplanctonDatatypeManager(IPhytoplanctonDatatypeManager phytoplanctonDatatypeManager) {
        this.phytoplanctonDatatypeManager = phytoplanctonDatatypeManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public final void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the parameters request.
     *
     * @param parametersRequest the new parameters request
     */
    public final void setParametersRequest(final ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     *
     * @return
     */
    public IFileCompManager getFileCompManager() {
        return fileCompManager;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }


    /**
     *
     * @return
     */
    public String addParameterExtraction() {
        currentParameterExtraction.getTargetValues().add(DEFAULT_TARGETVALUE);
        currentParameterExtraction.getUncertainties().add(DEFAULT_UNCERTAINTY);
        return null;
    }

    /**
     *
     * @param currentParameterExtraction
     */
    public void setCurrentParameterExtraction(ExtractionParametersVO currentParameterExtraction) {
        this.currentParameterExtraction = currentParameterExtraction;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The depth request param @link(DepthRequestParamVO).
         */
//        DepthRequestParamVO depthRequestParam;
        /**
         * The european format @link(boolean).
         */
        boolean europeanFormat = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getEuropeanFormat() {
            return this.europeanFormat;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uIPlatform.getPlatformStepIsValid() 
                    && uIDate.getDateStepIsValid()
                    && uIVariable.getVariableStepIsValid()
                    && uiTaxon.getTaxonStepIsValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Sets the european format.
         *
         * @param europeanFormat the new european format
         */
        public final void setEuropeanFormat(final boolean europeanFormat) {
            this.europeanFormat = europeanFormat;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 * @author tcherniatinsky
 */
public class UIDepth {

    /**
     *
     */
    public static final String ICON_BLANK = "ui-icon ui-icon-blank";

    /**
     *
     */
    public static final String ICON_CHECK = "ui-icon ui-icon-check";

    /**
     *
     */
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";
    private ILocalizationManager localizationManager;
    private DepthRequestParamVO depthRequestParamVO = new DepthRequestParamVO();
    private DatasRequestParamVO datasRequestParamVO = new DatasRequestParamVO();

    /**
     *
     * @param metadatasMap
     */
    public void addDepthsAndDatastoMap(Map<String, Object> metadatasMap) {
        metadatasMap.put("depths", depthRequestParamVO.getSelectedDepths());
        metadatasMap.put(DatasRequestParamVO.class.getSimpleName(), datasRequestParamVO.clone());
        metadatasMap.put(DepthRequestParamVO.class.getSimpleName(), depthRequestParamVO.clone());
    }

    /**
     *
     * @return
     */
    public boolean getDepthStepIsValid() {
        return depthRequestParamVO.isValidDepht()
                && (datasRequestParamVO.getRawData()
                || datasRequestParamVO.getMinValueAndAssociatedDepth()
                || datasRequestParamVO.getMaxValueAndAssociatedDepth()
                || datasRequestParamVO.getDataBalancedByDepth()
                || datasRequestParamVO.getTargetedValuesAndDepths());
    }

    /**
     *
     * @param localizationManager
     * @param chimieDatatypeManager
     */
    public void initProperties(ILocalizationManager localizationManager, IDepthManager chimieDatatypeManager) {
        this.localizationManager = localizationManager;
        this.depthRequestParamVO.setLocalizationManager(localizationManager);
        this.datasRequestParamVO.setLocalizationManager(localizationManager);
    }

    /**
     *
     * @param depthManager
     * @param uIDate
     * @param uIPlatform
     * @param uIVariable
     */
    public void updateDepthAvailables(IDepthManager depthManager, UIDate uIDate, UIPlatform uIPlatform, UIVariable uIVariable) {
        List<IntervalDate> selectedDates = uIDate.getDatesRequestParam().getCurrentDatesFormParam().getSelectedDates();
        List<PlateformeProjetVO> selectedPlateformes = uIPlatform.getSitesPlatforms().values().stream().map(p -> p.getPlatform()).collect(Collectors.toList());
        List<DatatypeVariableUniteGLACPE> selectedVariables = uIVariable.getVariableSelected();
        this.depthRequestParamVO.setAvailablesDepths(
                depthManager.getAvailableDepths(selectedPlateformes, selectedDates, selectedVariables)
                        .stream().filter(f -> f != null).sorted(Float::compare).collect(Collectors.toList()));
    }

    /**
     *
     * @return
     */
    public DepthRequestParamVO getDepthRequestParamVO() {
        return depthRequestParamVO;
    }

    /**
     *
     * @return
     */
    public DatasRequestParamVO getDatasRequestParamVO() {
        return datasRequestParamVO;
    }
}

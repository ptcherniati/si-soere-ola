/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;

import java.util.*;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * @author tcherniatinsky
 */
public class UIVariable {

    /**
     *
     */
    public static final String ICON_BLANK = "ui-icon ui-icon-blank";

    /**
     *
     */
    public static final String ICON_CHECK = "ui-icon ui-icon-check";

    /**
     *
     */
    public static final String ICON_INDETERMINATE = "ui-icon-minusthick";

    /**
     * The properties affichage dvu @link(Properties).
     */
    static Properties propertiesAffichageVariable;

    /**
     * The properties names groupe @link(Properties).
     */
    static Properties propertiesNamesGroupe;
    static Properties propertiesNamesVariable;
    /**
     * The groupes variables @link(Map<Long,VariableVO>).
     */
    final Map<Long, VariableVO> groupesVariables = new HashMap();

    /**
     * The availables projects.
     */
    TreeNode availablesGroupes = new DefaultTreeNode("Root", null);
    private TreeNode selectedNode;
    private ILocalizationManager localizationManager;

    /**
     * Gets the groupes variables.
     *
     * @return the groupes variables
     */
    public Map<Long, VariableVO> getGroupesVariables() {

        return this.groupesVariables;
    }

    /**
     *
     * @return
     */
    public Map<String, List<VariableVO>> getVariableByGroupes() {
        return groupesVariables.values().stream()
                .collect(
                        Collectors.groupingBy(t -> {
                            return propertiesNamesGroupe.getProperty(
                                    Optional.ofNullable(t).map(gv -> gv.getVariable()).map(v -> v.getGroupe()).map(gv -> gv.getNom()).orElse("_"),
                                    Optional.ofNullable(t).map(gv -> gv.getVariable()).map(v -> v.getGroupe()).map(gv -> gv.getNom()).orElse("_")
                            );
                        })
                );
    }

    /**
     * Adds the all groupes.
     *
     * @return the string
     */
    public final String addAllGroupes() {
        groupesVariables.clear();
        for (final TreeNode groupeNode : this.availablesGroupes.getChildren()) {
            for (TreeNode variableNode : groupeNode.getChildren()) {
                VariableVO variable = (VariableVO) variableNode.getData();
                variable.setSelected(true);
                groupesVariables.put(variable.dvu.getId(), variable);
                setExpanded(variableNode, true);
            }
        }
        return null;
    }

    /**
     * Removes the all groupes.
     *
     * @return the string
     */
    public final String removeAllGroupes() {
        groupesVariables.clear();
        this.availablesGroupes.getChildren().forEach((groupeNode) -> {
            groupeNode.getChildren().forEach(variableNode -> {
                VariableVO variable = (VariableVO) variableNode.getData();
                variable.setSelected(false);
                setExpanded(variableNode, false);
            });
        });
        return null;
    }

    /**
     * Inits the properties.
     *
     * @param localizationManager
     * @param variableManager
     */
    public void initProperties(ILocalizationManager localizationManager, IVariableManager variableManager) {
        this.localizationManager = localizationManager;
        propertiesNamesGroupe = localizationManager.newProperties(GroupeVariable.TABLE_NAME, "nom_groupe", this.localizationManager.getUserLocale().getLocale());
        propertiesNamesVariable = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME, this.localizationManager.getUserLocale().getLocale());
    }

    /**
     * Select dvu.
     *
     * @param event
     */
    public final void selectNode(NodeSelectEvent event) {
        TreeNode selectedNode = event.getTreeNode();
        final String type = selectedNode.getType();
        boolean isBlank = ((NodeableNode) selectedNode).getState().equals(ICON_BLANK);
        if (NodeType.GROUPE.equals(type)) {
            ((NodeableNode) selectedNode).getChildren().forEach(
                    child -> selectVariable((VariableVO) child, isBlank)
            );
        } else {
            final VariableVO variableVO = (VariableVO) selectedNode;
            selectVariable(variableVO, !(variableVO).isSelected());
        }
    }

    /**
     *
     * @param variable
     * @return
     */
    public String removeVariable(VariableVO variable) {
        selectVariable(variable, false);
        return "";
    }

    private final void selectVariable(VariableVO variable, boolean selected) {
        variable.setSelected(selected);
        if (selected) {
            groupesVariables.put(variable.getVariable().getId(), variable);
        } else {
            groupesVariables.remove(variable.getVariable().getId());
        }
        variable.setSelected(selected);
        setExpanded(variable, selected);
    }

    /**
     * Update agro availables.
     *
     * @param variableManager
     * @param intervalsDate
     * @param uIPlatform
     */
    public final void updateVariablesAvailables(IVariableManager variableManager, UIDate uIDate, UIPlatform uIPlatform) {
        List<IntervalDate> selectedDates = uIDate.getDatesRequestParam().getCurrentDatesFormParam().getSelectedDates();
        List<PlateformeProjetVO> selectedPlateformes = uIPlatform.getSitesPlatforms().values().stream().map(p -> p.getPlatform()).collect(Collectors.toList());
        this.availablesGroupes = new DefaultTreeNode("Root", null);
        Collection<DatatypeVariableUniteGLACPE> availablesVariables = variableManager.getAvailableVariables(selectedPlateformes, selectedDates);
        Map<GroupeVariable, TreeNode> groupesNodes = new HashMap();
        TreeNode projectTreeNode, groupeTreeNode;
        for (final DatatypeVariableUniteGLACPE dvu : availablesVariables) {
            GroupeVariable groupe = dvu.getVariable().getGroupe();
            if (!groupesNodes.containsKey(groupe)) {
                final NodeableNode groupeVariableNode = new NodeableNode(new GroupeNodeable(groupe), availablesGroupes);
                groupeVariableNode.setType(NodeType.GROUPE);
                groupesNodes.put(groupe, groupeVariableNode);
            }
            groupeTreeNode = groupesNodes.get(groupe);
            new VariableVO(new VariableNodeable(dvu), NodeType.VARIABLE, groupeTreeNode);
        }
    }

    /**
     * Gets the dvu step is valid.
     *
     * @return the dvu step is valid
     */
    public Boolean getVariableStepIsValid() {
        return !this.groupesVariables.isEmpty();
    }

    /**
     * @param node
     * @param expanded
     */
    public void setExpanded(TreeNode node, boolean expanded) {
        node.setExpanded(expanded);
        if (node.getParent() != null) {
            setExpanded(node.getParent(), expanded);
        }
    }

    /**
     * Gets the availables projects.
     *
     * @return the availables projects
     */
    public final TreeNode getAvailablesGroupes() {
        return this.availablesGroupes;
    }

    /**
     * @return
     */
    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode
     */
    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    /**
     * Gets the list dvu.
     *
     * @return the list dvu
     */
    public List<VariableGLACPE> getListVariable() {
        final List<VariableGLACPE> variables = new LinkedList();
        for (final VariableVO variable : this.groupesVariables.values()) {
            variables.add(variable.getVariable());
        }
        return variables;
    }

    /**
     * @param metadatasMap
     */
    public void addVariabletoMap(Map<String, Object> metadatasMap) {
        List<DatatypeVariableUniteGLACPE> variabletoMap = getVariableSelected();
        metadatasMap.put(DatatypeVariableUniteGLACPE.class.getSimpleName(), variabletoMap);
    }

    /**
     *
     * @return
     */
    public List<DatatypeVariableUniteGLACPE> getVariableSelected() {
        List<DatatypeVariableUniteGLACPE> variablesSelected = this.groupesVariables.values().stream()
                .filter(vvo -> vvo.isSelected())
                .map(vvo -> vvo.getDvu())
                .collect(Collectors.toList());
        return variablesSelected;
    }

    /**
     * The Class NodeType.
     */
    public static class NodeType {

        /**
         * The groupe.
         */
        public static final String GROUPE = "groupe";
        /**
         * The dvu.
         */
        public static final String VARIABLE = "variable";

        private NodeType() {
        }
    }

    /**
     *
     */
    public class NodeableNode extends DefaultTreeNode implements Comparable<NodeableNode>, TreeNode {

        /**
         * The dvu.
         */
        INodeable nodeable;

        /**
         *
         * @param nodeable
         * @param parent
         */
        public NodeableNode(INodeable nodeable, TreeNode parent) {
            super(nodeable, parent);
            this.nodeable = nodeable;
        }

        /**
         *
         * @param nodeable
         * @param type
         * @param data
         * @param parent
         */
        public NodeableNode(INodeable nodeable, String type, Object data, TreeNode parent) {
            super(type, data, parent);
            this.nodeable = nodeable;
        }

        /**
         * @param nodeable
         */
        public NodeableNode(INodeable nodeable) {
            this.nodeable = nodeable;
        }

        /**
         * @return
         */
        public String getName() {
            return getType().equals(NodeType.GROUPE)
                    ? Optional.ofNullable(nodeable).map(n -> (GroupeNodeable) n).map(gn -> gn.getGroupeVariable()).map(gv -> gv.getNom()).orElse("")
                    : localizationManager.getLocalName(nodeable);
        }

        /**
         *
         * @return
         */
        public String getLocalizedDisplayPath() {
            return getType().equals(NodeType.GROUPE)
                    ? propertiesNamesGroupe.getProperty(getName(), getName())
                    : propertiesNamesVariable.getProperty(getName(), getName());
        }

        /**
         *
         * @param t
         * @return
         */
        @Override
        public int compareTo(NodeableNode t) {
            return getName().compareTo(t.getName());
        }

        /**
         *
         * @return
         */
        public String getState() {
            if (getType().equals(NodeType.GROUPE)) {
                if (getChildren().stream().allMatch(child -> child.isSelected())) {
                    return ICON_CHECK;
                }
                if (getChildren().stream().anyMatch(child -> child.isSelected())) {
                    return ICON_INDETERMINATE;
                }
            } else {
                return isSelected() ? ICON_CHECK : ICON_BLANK;
            }
            return ICON_BLANK;
        }

        /**
         *
         * @return
         */
        @Override
        public Object getData() {
            return this;
        }

        /**
         *
         * @return
         */
        @Override
        public String toString() {
            return "NodeableNode{" + "nodeable=" + nodeable + ", nodeableNodeSelected=" + isSelected() + '}';
        }
    }

    /**
     *
     */
    public class GroupeNodeable extends Nodeable {

        private GroupeVariable groupeVariable;

        /**
         *
         * @param variable
         */
        public GroupeNodeable(GroupeVariable variable) {
            super(variable == null ? "" : variable.getCode());
            this.groupeVariable = variable;
        }

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return groupeVariable.getCode();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) VariableNodeable.class;
        }

        /**
         *
         * @return
         */
        public GroupeVariable getGroupeVariable() {
            return this.groupeVariable;
        }
    }

    /**
     *
     */
    public class VariableNodeable extends Nodeable {

        private DatatypeVariableUniteGLACPE dvu;

        /**
         *
         * @param dvu
         */
        public VariableNodeable(DatatypeVariableUniteGLACPE dvu) {
            super(dvu.getVariable().getCode());
            this.dvu = dvu;
        }

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return dvu.getVariable().getName();
        }

        /**
         *
         * @param <T>
         * @return
         */
        @Override
        public <T extends INodeable> Class<T> getNodeableType() {
            return (Class<T>) VariableNodeable.class;
        }

        /**
         *
         * @return
         */
        public DatatypeVariableUniteGLACPE getDvu() {
            return dvu;
        }
    }

    /**
     *
     */
    public class VariableVO extends NodeableNode {

        /**
         * The dvu.
         */
        DatatypeVariableUniteGLACPE dvu;

        /**
         *
         * @param variableNodeable
         * @param data
         * @param parent
         */
        public VariableVO(VariableNodeable variableNodeable, Object data, TreeNode parent) {
            super(variableNodeable, NodeType.VARIABLE, data, parent);
            this.dvu = variableNodeable.getDvu();
        }

        /**
         * Gets the localized affichage.
         *
         * @return the localized affichage
         */
        public String getLocalizedAffichage() {
            return propertiesNamesVariable.getProperty(this.dvu.getName(), this.dvu.getName());
        }

        /**
         * Gets the localized display path.
         *
         * @return the localized display path
         */
        public String getLocalizedDisplayPath() {
            return getLocalizedAffichage();
        }

        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return this.dvu.getVariable().getName();
        }

        /**
         * Gets the dvu.
         *
         * @return the dvu
         */
        public VariableGLACPE getVariable() {
            return this.dvu.getVariable();
        }

        /**
         *
         * @return
         */
        public DatatypeVariableUniteGLACPE getDvu() {
            return dvu;
        }
    }
}

/*
 *
 */
package org.inra.ecoinfo.glacpe.extraction.chimie.jsf;
import java.io.Serializable;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.chimie.ChimieParameters;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIAssociate;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIDate;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIDepth;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIPlatform;
import org.inra.ecoinfo.glacpe.extraction.jsf.UIVariable;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * The Class UIChimie.
 */
@ManagedBean(name = "uiChimie")
@ViewScoped
public class UIChimie extends AbstractUIBeanForSteps implements Serializable {

    /**
     * The Constant serialVersionUID <long>.
     */
    static final long serialVersionUID = 1L;

    /**
     * The affichage @link(String).
     */
    String affichage = "1";

    /**
     * The extraction manager.
     */
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    /**
     * The localization manager.
     */
    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    /**
     * The MPS dataset manager.
     */
    @ManagedProperty(value = "#{chimieDatatypeManager}")
    IChimieDatatypeManager chimieDatatypeManager;

    /**
     * The notifications manager.
     */
    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;
    
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;

    /**
     * The parameters request @link(ParametersRequest).
     */
    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The properties variables names @link(Properties).
     */
    Properties propertiesVariablesNames;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    UIPlatform uIPlatform;
    UIDate uIDate;
    UIVariable uIVariable;
    UIDepth uIDepth;
    UIAssociate uIAssociate;

    // GETTERS SETTERS - BEANS
    /**
     * Instantiates a new uI bean mps.
     */
    public UIChimie() {
        super();
    }

    /**
     *
     * @return
     */
    public UIPlatform getuIPlatform() {
        return uIPlatform;
    }

    /**
     *
     * @return
     */
    public UIDate getuIDate() {
        return uIDate;
    }

    /**
     *
     * @return
     */
    public UIVariable getuIVariable() {
        return uIVariable;
    }

    /**
     *
     * @param uIVariable
     */
    public void setuIVariable(UIVariable uIVariable) {
        this.uIVariable = uIVariable;
    }

    /**
     *
     * @return
     */
    public UIDepth getuIDepth() {
        return uIDepth;
    }

    /**
     *
     * @param uIDepth
     */
    public void setuIDepth(UIDepth uIDepth) {
        this.uIDepth = uIDepth;
    }

    /**
     *
     * @return
     */
    public UIAssociate getuIAssociate() {
        return uIAssociate;
    }

    /**
     *
     * @param uIAssociate
     */
    public void setuIAssociate(UIAssociate uIAssociate) {
        this.uIAssociate = uIAssociate;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap<>();
        // TODO
        uIPlatform.addPlatformtoMap(metadatasMap);
        uIDate.addDatestoMap(metadatasMap);
        uIVariable.addVariabletoMap(metadatasMap);
        uIDepth.addDepthsAndDatastoMap(metadatasMap);
//        uIPlatform.addTreatmenttoMap(metadatasMap);
        uIAssociate.addAssociateSelectedToMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final ChimieParameters parameters = new ChimieParameters(metadatasMap);
//        if(this.parametersRequest.depthRequestParam.getAvailablesDepth().isEmpty()){
//            updateDepthAvailables();
//        }
//        if (this.parametersRequest.depthRequestParam.getChoiceDepth()) {
//            parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam.getSelectedsDepths());
//            if (parameters.getSelectedDepth().isEmpty()) {
//                parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam.getAvailablesDepth());
//            }
//        } else {
//            parameters.setSelectedsDepths(this.parametersRequest.depthRequestParam
//                    .getAvailablesDepth().subSet(
//                            this.parametersRequest.depthRequestParam.getDepthMin(),
//                            this.parametersRequest.depthRequestParam.getDepthMax() + 1));
//        }
        setStaticMotivation(getMotivation());
        this.extractionManager.extract(parameters, getAffichage().equals("1")?1:2);
        return null;
    }

    /**
     * Gets the affichage.
     *
     * @return the affichage
     */
    public final String getAffichage() {
        return this.affichage;
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (this.getStep()) {
            case 1:
                return this.uIPlatform.getPlatformStepIsValid();
            case 2:
                return this.uIDate.getDateStepIsValid();
            case 3:
                return uIVariable.getVariableStepIsValid();
            default:
                return uIDepth.getDepthStepIsValid();
        }
    }

    /**
     *
     * @return
     */
    public IChimieDatatypeManager getChimieDatatypeManager() {
        return chimieDatatypeManager;
    }

    /**
     * Gets the parameters request.
     *
     * @return the parameters request
     */
    public final ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    final void initProperties() {
        setEnabledMotivation(false);
        uIPlatform = new UIPlatform();
        uIPlatform.initProperties(localizationManager, chimieDatatypeManager);
        uIDate = new UIDate();
        uIDate.initDatesRequestParam(localizationManager);
        uIVariable = new UIVariable();
        uIVariable.initProperties(localizationManager, chimieDatatypeManager);
        uIDepth = new UIDepth();
        uIDepth.initProperties(localizationManager, chimieDatatypeManager);
        uIAssociate = new UIAssociate();
        uIAssociate.initAssociate(localizationManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "chimie";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (this.getStep()) {
            case 3:
                uIVariable.updateVariablesAvailables(chimieDatatypeManager, uIDate, uIPlatform);
                break;
            case 4:
                uIDepth.updateDepthAvailables(chimieDatatypeManager, uIDate, uIPlatform, uIVariable);
                try {
                    uIAssociate.caseassociate(uIDate, uIPlatform, fileCompManager);
                } catch (BadExpectedValueException |DateTimeParseException ex) {
                    LoggerFactory.getLogger(UIChimie.class.getName()).error(ex.getMessage(), ex);
                }
                break;
            default:
                break;
        }
        return null;
    }

    /**
     * Sets the affichage.
     *
     * @param affichage the new affichage
     */
    public final void setAffichage(final String affichage) {
        this.affichage = affichage;
    }

    /**
     * Sets the extraction manager.
     *
     * @param extractionManager the new extraction manager
     */
    public final void setExtractionManager(final IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     */
    public final void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param chimieDatatypeManager
     */
    public void setChimieDatatypeManager(IChimieDatatypeManager chimieDatatypeManager) {
        this.chimieDatatypeManager = chimieDatatypeManager;
    }

    /**
     * Sets the notifications manager.
     *
     * @param notificationsManager the new notifications manager
     */
    public final void setNotificationsManager(final INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     * Sets the parameters request.
     *
     * @param parametersRequest the new parameters request
     */
    public final void setParametersRequest(final ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }

    /**
     *
     * @return
     */
    public IFileCompManager getFileCompManager() {
        return fileCompManager;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The depth request param @link(DepthRequestParamVO).
         */
//        DepthRequestParamVO depthRequestParam;
        /**
         * The european format @link(boolean).
         */
        boolean europeanFormat = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getEuropeanFormat() {
            return this.europeanFormat;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uIPlatform.getPlatformStepIsValid() 
                    && uIDate.getDateStepIsValid()
                    && uIVariable.getVariableStepIsValid()
                    && uIDepth.getDepthStepIsValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Sets the european format.
         *
         * @param europeanFormat the new european format
         */
        public final void setEuropeanFormat(final boolean europeanFormat) {
            this.europeanFormat = europeanFormat;
        }
    }
}

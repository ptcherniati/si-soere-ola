
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ptcherniati
 */
public class Test {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Map<String, Map<String, List<String>>> map = new HashMap<>();
        map
                .getOrDefault("1", new HashMap<>())
                .getOrDefault("2", new LinkedList<>())
                .add("3");
        System.out.println("--------------------------");
        System.out.println(map);
        System.out.println("--------------------------");
        map.clear();
        map
                .computeIfAbsent("1", k->new HashMap<>())
                .computeIfAbsent("2", k->new LinkedList<>())
                .add("3");
        System.out.println("--------------------------");
        System.out.println(map);
        System.out.println("--------------------------");
        map.clear();
        map
                .putIfAbsent("1", new HashMap<>())
                .putIfAbsent("2", new LinkedList<>())
                .add("3");
        System.out.println("--------------------------");
        System.out.println(map);
        System.out.println("--------------------------");
    }
}

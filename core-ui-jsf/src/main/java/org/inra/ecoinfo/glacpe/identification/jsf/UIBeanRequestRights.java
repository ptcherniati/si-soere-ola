package org.inra.ecoinfo.glacpe.identification.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE;
import org.inra.ecoinfo.identification.IUtilisateurDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.jsf.AbstractUIBeanRequestRights;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ptcherniati
 */
@ManagedBean(name = "uiRequestRightsGLACPE")
@ViewScoped
public class UIBeanRequestRights extends AbstractUIBeanRequestRights<RightRequestGLACPEVO, RightRequestGLACPE> {

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(UIBeanRequestRights.class);

    /**
     *
     */
    @ManagedProperty(value = "#{utilisateurDAO}")
    protected IUtilisateurDAO utilisateurDAO;
    
    RightRequestGLACPE rightRequest = new RightRequestGLACPE();

    /**
     *
     */
    public UIBeanRequestRights() {
    }

    /**
     * @return
     */
    @Override
    public String navigate() {
        return "requestRights";
    }

    /**
     * @return
     */
    @Override
    public String userNavigate() {
        return "requestUsersRights";
    }

    /**
     * @param dbRequest
     * @return
     */
    @Override
    protected RightRequestGLACPEVO newRequestVO(RightRequestGLACPE dbRequest) {
        return new RightRequestGLACPEVO(dbRequest);
    }

    /**
     * @return
     */
    @Override
    protected RightRequestGLACPEVO getRequestVOInstance() {
        return new RightRequestGLACPEVO().getInstance();
    }

    @Override
    public String updateRights() throws BusinessException {
        super.updateRights(); //To change body of generated methods, choose Tools | Templates.
        return "";
    }

    /**
     *
     * @param utilisateurDAO
     */
    public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
        this.utilisateurDAO = utilisateurDAO;
    }

    /**
     *
     * @param policyManager
     */
    @Override
    public void setPolicyManager(IPolicyManager policyManager) {
        super.setPolicyManager(policyManager); 
        this.request.setUtilisateur((Utilisateur) policyManager.getCurrentUser());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.identification.jsf;

import java.time.LocalDateTime;
import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.jsf.AbstractRightRequestVO;
import org.inra.ecoinfo.identification.jsf.UtilisateurVO;

/**
 * @author ptcherniati
 */
public class RightRequestGLACPEVO extends AbstractRightRequestVO<RightRequestGLACPEVO, RightRequestGLACPE> {
    private static RightRequestGLACPEVO _getInstance() {
        return new RightRequestGLACPEVO();
    }
    /**
     *
     */
    protected RightRequestGLACPE _request;
    /**
     *
     */
    protected UtilisateurVO utilisateurVO;

    /**
     * @param rglcp
     * @param request
     */
    public RightRequestGLACPEVO(RightRequestGLACPE request) {
        super();
        this._request = request;
        this.utilisateurVO = new UtilisateurVO(request.getUser());
    }

    /**
     *
     */
    public RightRequestGLACPEVO() {
        super();
    }


    /**
     * @param req
     * @return
     */
    @Override
    public RightRequestGLACPEVO getInstance(RightRequestGLACPE req) {
        RightRequestGLACPEVO instance = _getInstance();
        instance._request = req;
        return instance;
    }

    /**
     * @return
     */
    @Override
    public RightRequestGLACPEVO getInstance() {
        RightRequestGLACPEVO instance = _getInstance();
        instance.utilisateurVO = utilisateurVO;
        instance._request = new RightRequestGLACPE();
        return instance;
    }

    /**
     * @return
     */
    @Override
    public Utilisateur getUtilisateur() {
        return _request.getUser();
    }

    /**
     * @param utilisateur
     */
    @Override
    public void setUtilisateur(Utilisateur utilisateur) {
        _request.setUser(utilisateur);
    }

    /**
     * @return
     */
    @Override
    public RightRequestGLACPE getRightRequest() {
        return _request;
    }

    /**
     * @return
     */
    @Override
    public LocalDateTime getCreateDate() {
        return _request.getCreateDate();
    }

    /**
     * @return
     */
    @Override
    public boolean isValidated() {
        return _request.isValidated();
    }

    /**
     * @param validated
     */
    @Override
    public void setValidated(boolean validated) {
        _request.setValidated(validated);
    }
}

package org.inra.ecoinfo.glacpe.logging.jsf;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiLogs")
@ViewScoped
public class UIBeanLogs implements Serializable {

    private static final long serialVersionUID = 1L;
    private  List<List<String>>  fileteredValues;

    /**
     *
     */
    public Map<File, Map<String, List<List<String>>>> logFiles;

    /**
     *
     * @return
     */
    public List<List<String>> getFileteredValues() {
        return fileteredValues;
    }

    /**
     *
     * @param fileteredValues
     */
    public void setFileteredValues(List<List<String>> fileteredValues) {
        this.fileteredValues = fileteredValues;
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return "logs";
    }

    /**
     *
     * @return
     */
    public Map<File, Map<String, List<List<String>>>> getLogFiles() {
        if (logFiles != null) {
            return logFiles;
        }
        try {
            InputStream is = this.getClass().getResourceAsStream("../../../../../../my.properties");
            java.util.Properties p = new Properties();
            p.load(is);
            String home = System.getProperty("user.home");
            String deployRepository = p.getProperty("deploy.repository");
            File file = new File(String.format("%s/%s/logs/", home, deployRepository));
            if (file.isDirectory()) {
                Map<File, Map<String, List<List<String>>>> collect = Stream.of(file.listFiles())
                        .map(File::listFiles)
                        .flatMap(Stream::of)
                        .collect(
                                Collectors.groupingBy(
                                        f -> f.getParentFile(),
                                        HashMap::new,
                                        Collectors.toMap(File::getName, this::toArray)
                                )
                        );
                return collect;
            }
        } catch (IOException ex) {
            Logger.getLogger(UIBeanLogs.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new TreeMap<>();
    }

    /**
     *
     * @return
     */
    public ArrayList<Map.Entry<File, Map<String, List<List<String>>>>> getFilesEntries() {
        final Set<Map.Entry<File, Map<String, List<List<String>>>>> entrySet = getLogFiles().entrySet();
        return  new ArrayList(entrySet);
    }

    /**
     *
     * @param files
     * @return
     */
    public ArrayList<Map.Entry<String, List<List<String>>>> getFilesEntries(Map<String, List<List<String>>> files) {
        return  new ArrayList(files.entrySet());
    } 

    
    /**
     * 
     * @param file
     * @return 
     */
    public List<List<String>> toArray(File file) {
        try {
            List<List<String>> collect = Files.lines(file.toPath())
                    .map(
                            line -> Stream.of(CSVParser.parse(line, ';')[0]).collect(Collectors.toList())
                    )
                    .collect(Collectors.toList());
            return collect;
        } catch (IOException ex) {
            Logger.getLogger(UIBeanLogs.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new LinkedList<>();
    }
}

package org.inra.ecoinfo.glacpe.filecomp;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.DateTimeException;
import org.concordion.api.Unimplemented;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.CoreTestFixture;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {GLACPETransactionalTestFixtureExecutionListener.class})
@Unimplemented
public class AssociateFilesFixture extends org.inra.ecoinfo.security.CheckRightsFixture {

    /**
     *
     */
    public AssociateFilesFixture() {
        super();
        MockitoAnnotations.openMocks(this);
    }

    /**
     * @param nodePath
     * @param startDate
     * @param endDate
     * @param fileType
     * @param fileName
     * @return
     * @throws BusinessException
     * @throws DateTimeException
     * @throws BadExpectedValueException
     * @throws JsonProcessingException
     */
    public boolean associate(String nodePath, String startDate, String endDate, String fileType, String fileName) throws BusinessException, DateTimeException, BadExpectedValueException, JsonProcessingException {
        boolean addActivity = false;
        try {
            IFileCompDAO fileCompDAO = (IFileCompDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompDAO");
            FileComp fileComp = fileCompDAO.getByCode(fileName).orElseThrow(PersistenceException::new);
            final String fileUser = String.format("file_%s", fileComp.getCode());
            CoreTestFixture.loadtree("3", fileUser, "ALL", "associate");
            addActivity = addActivity(fileUser, "TREEDATASET", "associate", nodePath, startDate, endDate);
        } catch (PersistenceException e) {
            return addActivity;
        }
        return addActivity;
    }
}

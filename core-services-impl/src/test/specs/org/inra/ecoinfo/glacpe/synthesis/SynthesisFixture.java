package org.inra.ecoinfo.glacpe.synthesis;


import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.policyManager;
import org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener;
import static org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener.*;
import org.inra.ecoinfo.mga.business.composite.groups.Group;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {GLACPETransactionalTestFixtureExecutionListener.class})
public class SynthesisFixture {
    public String buildSynthesis() throws BusinessException, PersistenceException{
        synthesisManager.buildSynthesis();
        String groupName = policyManager.getCurrentUser().getGroupName();
        persistExtractActivities("testeurAuto");
        return "true";
    }

    /**
     * @param groupName
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    public int persistExtractActivities(String groupName) throws PersistenceException {
        Group group = policyManager.getGroupByGroupNameAndWhichTree(groupName, WhichTree.TREEDATASET)
                .orElseThrow(PersistenceException::new);
        policyManager.persistExtractActivity(groupName, group.getActivitiesMap()
                .get(Activities.extraction));
        return group.getActivitiesMap().size();
    }
}

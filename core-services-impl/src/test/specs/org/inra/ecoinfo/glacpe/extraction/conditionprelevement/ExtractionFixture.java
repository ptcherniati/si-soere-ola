package org.inra.ecoinfo.glacpe.extraction.conditionprelevement;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import static org.inra.ecoinfo.TransactionalTestFixtureExecutionListener.policyManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener;
import static org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl.ConditionsGeneralesParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {GLACPETransactionalTestFixtureExecutionListener.class})
public class ExtractionFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    /**
     *
     */
    public ExtractionFixture() {
        super();
        MockitoAnnotations.openMocks(this);
    }

    /**
     *
     * @param selectedDates
     * @param plateformes
     * @param datedeFin
     * @param dateDeDebut
     * @param variables
     * @param annees
     * @param allDepth
     * @param profMin
     * @param profMax
     * @param rawDatas
     * @param aggregatedDatas
     * @param minAggregated
     * @param maxAggregated
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     */
    public String extract(
            String plateformes,
            String selectedDates,
            String dateDeDebut,
            String datedeFin,
            String annees,
            String variables,
            String filecomps,
            String commentaire,
            String affichage
    ) throws BusinessException {

        final Map<String, Object> metadatasMap = new HashMap<>();
        List<String> platformPathes = Stream.of(plateformes.split(";")).collect(Collectors.toList());
        final Collection<PlateformeProjetVO> availablePlatforms = ppdao.getAvailablePlatforms(policyManager.getCurrentUser());
        availablePlatforms.addAll(chlorophylleDAO.getAvailablePlatforms(policyManager.getCurrentUser()));
        availablePlatforms.addAll(conditionGeneraleDAO.getAvailablePlatforms(policyManager.getCurrentUser()));
        final TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<Plateforme>>> collect = availablePlatforms
                .stream()
                .filter(
                        p -> platformPathes.contains(String.format("%s,%s", p.getProjet().getCode(), p.getPlateforme().getCode()))
                )
                .collect(
                        Collectors.groupingBy(
                                PlateformeProjetVO::getProjet,
                                TreeMap::new,
                                Collectors.groupingBy(
                                        PlateformeProjetVO::getSite,
                                        TreeMap::new,
                                        Collectors.mapping(
                                                PlateformeProjetVO::getPlateforme, Collectors.toCollection(TreeSet::new)
                                        )
                                )
                        )
                );
        metadatasMap.put(Plateforme.class.getSimpleName(), collect);
        final DatesRequestParamVO datesRequestFormParamVO = new DatesRequestParamVO();
        if (selectedDates.equals(DatesYearsContinuousFormParamVO.LABEL)) {
            String[] debuts = dateDeDebut.split(",");
            String[] fins = datedeFin.split(",");
            datesRequestFormParamVO.setSelectedFormSelection(DatesYearsContinuousFormParamVO.LABEL);
            final DatesYearsContinuousFormParamVO datesYearsContinuousFormParamVO = new DatesYearsContinuousFormParamVO(GLACPETransactionalTestFixtureExecutionListener.localizationManager);
            for (int i = 0; i < debuts.length; i++) {
                Map<String, String> periods = new HashMap<String, String>();
                periods.put(AbstractDatesFormParam.START_INDEX, debuts[i]);
                periods.put(AbstractDatesFormParam.END_INDEX, fins[i]);
                if (datesYearsContinuousFormParamVO.getPeriods() == null) {
                    List<Map<String, String>> periodsList = new LinkedList<Map<String, String>>();
                    datesYearsContinuousFormParamVO.setPeriods(periodsList);
                }
                datesYearsContinuousFormParamVO.getPeriods().add(periods);
            }
            datesRequestFormParamVO.setDatesYearsContinuousFormParam(datesYearsContinuousFormParamVO);
        } else if (selectedDates.equals(DatesYearsDiscretsFormParamVO.LABEL)) {
            String[] debuts = dateDeDebut.split(",");
            String[] fins = datedeFin.split(",");
            datesRequestFormParamVO.setSelectedFormSelection(DatesYearsDiscretsFormParamVO.LABEL);
            final DatesYearsDiscretsFormParamVO datesYearsDiscretsFormParamVO = new DatesYearsDiscretsFormParamVO(GLACPETransactionalTestFixtureExecutionListener.localizationManager);
            for (int i = 0; i < debuts.length; i++) {
                Map<String, String> periods = new HashMap<String, String>();
                periods.put(AbstractDatesFormParam.START_INDEX, debuts[i]);
                periods.put(AbstractDatesFormParam.END_INDEX, fins[i]);
                if (datesYearsDiscretsFormParamVO.getPeriods() == null) {
                    List<Map<String, String>> periodsList = new LinkedList<Map<String, String>>();
                    datesYearsDiscretsFormParamVO.setPeriods(periodsList);
                }
                datesYearsDiscretsFormParamVO.getPeriods().add(periods);
            }
            datesYearsDiscretsFormParamVO.setYears(annees);
            datesRequestFormParamVO.setDatesYearsDiscretsFormParam(datesYearsDiscretsFormParamVO);
        } else if (selectedDates.equals(DatesYearsRangeFormParamVO.LABEL)) {
            String[] debuts = dateDeDebut.split(",");
            String[] fins = datedeFin.split(",");
            datesRequestFormParamVO.setSelectedFormSelection(DatesYearsRangeFormParamVO.LABEL);
            final DatesYearsRangeFormParamVO datesYearsRangeFormParamVO = new DatesYearsRangeFormParamVO(GLACPETransactionalTestFixtureExecutionListener.localizationManager);
            for (int i = 0; i < debuts.length; i++) {
                Map<String, String> periods = new HashMap<String, String>();
                periods.put(AbstractDatesFormParam.START_INDEX, debuts[i]);
                periods.put(AbstractDatesFormParam.END_INDEX, fins[i]);
                if (datesYearsRangeFormParamVO.getPeriods() == null) {
                    List<Map<String, String>> periodsList = new LinkedList<Map<String, String>>();
                    datesYearsRangeFormParamVO.setPeriods(periodsList);
                }
                datesYearsRangeFormParamVO.getPeriods().add(periods);
            }
            String[] yearsRange = annees.split("-");
            List<Map<String, String>> yearsList = new LinkedList<Map<String, String>>();
            for (String range : yearsRange) {
                Map<String, String> yearsMap = new HashMap<String, String>();
                String[] year = range.split(",");
                yearsMap.put(AbstractDatesFormParam.START_INDEX, year[0]);
                yearsMap.put(AbstractDatesFormParam.END_INDEX, year[1]);
                yearsList.add(yearsMap);
            }
            datesYearsRangeFormParamVO.setYears(yearsList);
            datesRequestFormParamVO.setDatesYearsRangeFormParam(datesYearsRangeFormParamVO);
        }
        metadatasMap.put(AbstractDatesFormParam.class.getSimpleName(), datesRequestFormParamVO);
        metadatasMap.put(IntervalDate.class.getSimpleName(), datesRequestFormParamVO.getCurrentDatesFormParam().getSelectedDates());
        final List<DatatypeVariableUniteGLACPE> dvus =  Stream
                .of(variables.split(" *, *"))
                .map(v -> datatypeVariableUniteGLACPEDAO.getByDatatypeAndVariable("conditions_prelevements", v).orElse(null))
                .filter(v -> v != null)
                .collect(Collectors.toList());

        metadatasMap.put(DatatypeVariableUniteGLACPE.class.getSimpleName(), dvus);
        
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);

        final ConditionsGeneralesParameter parameters = new ConditionsGeneralesParameter(metadatasMap);
        try {
            GLACPETransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final Exception e) {
            return "false : " + e.getMessage();
        }
        return "true";
    }
}

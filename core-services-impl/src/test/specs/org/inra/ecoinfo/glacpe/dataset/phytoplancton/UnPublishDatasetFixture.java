package org.inra.ecoinfo.glacpe.dataset.phytoplancton;


import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.glacpe.GLACPETransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {GLACPETransactionalTestFixtureExecutionListener.class})
public class UnPublishDatasetFixture extends org.inra.ecoinfo.dataset.UnPublishDatasetFixture {

}

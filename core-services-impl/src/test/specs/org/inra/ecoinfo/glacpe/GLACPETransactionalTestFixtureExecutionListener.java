package org.inra.ecoinfo.glacpe;

import java.io.IOException;
import java.sql.SQLException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype.IProjetSiteThemeDatatypeDAO;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.synthesis.ISynthesisManager;
import org.springframework.test.context.TestContext;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class GLACPETransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {

    private static String testName = "";

    /**
     *
     */
    public static IMetadataManager metadataManager;

    /**
     *
     */
    public static IDatasetManager datasetManager;

    /**
     *
     */
    public static IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeGLACPEDAO;

    /**
     *
     */
    public static IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     *
     */
    public static IDatasetDAO datasetDAO;

    /**
     *
     */
    public static IVersionFileDAO versionFileDAO;

    /**
     *
     */
    public static IDatatypeDAO datatypeDAO;

    /**
     *
     */
    public static IProjetDAO projetDAO;

    /**
     *
     */
    public static ISiteGLACPEDAO siteGLACPEDAO;

    /**
     *
     */
    public static IVariableDAO variableDAO;

    /**
     *
     */
    public static IPlateformeDAO plateformeDAO;

    /**
     *
     */
    public static ITaxonDAO taxonDAO;

    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static IExtractionManager extractionManager;

    /**
     *
     */
    public static INotificationsManager notificationsManager;

    /**
     *
     */
    public static IFileCompManager fileCompManager;

    /**
     *
     */
    public static IFileCompConfiguration fileCompConfiguration;

    /**
     *
     */
    public static IChimieDAO chimieDAO;

    /**
     *
     */
    public static ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    public static IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    public static IZooplanctonDAO zooplanctonDAO;

    /**
     *
     */
    public static IPPDAO ppdao;

    /**
     *
     */
    public static IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     */
    public static IChlorophylleDAO chlorophylleDAO;

    /**
     *
     */
    public static ISynthesisManager synthesisManager;

    /**
     * After super class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws DataSetException the data set exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws Exception the exception
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        super.afterTestClass(testContext);
    }

    /**
     * Before test class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws Exception the exception @see
     * org.springframework.test.context.support.AbstractTestExecutionListener
     * #beforeTestClass(org.springframework.test.context.TestContext)
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        GLACPETransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        GLACPETransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        GLACPETransactionalTestFixtureExecutionListener.projetSiteThemeDatatypeGLACPEDAO = (IProjetSiteThemeDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetSiteThemeDatatypeGLACPEDAO");
        GLACPETransactionalTestFixtureExecutionListener.datatypeVariableUniteGLACPEDAO = (IDatatypeVariableUniteGLACPEDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datatypeVariableUniteDAO");
        GLACPETransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        GLACPETransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        GLACPETransactionalTestFixtureExecutionListener.datatypeDAO = (IDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datatypeDAO");
        GLACPETransactionalTestFixtureExecutionListener.projetDAO = (IProjetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("projetDAO");
        GLACPETransactionalTestFixtureExecutionListener.siteGLACPEDAO = (ISiteGLACPEDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteGLACPEDAO");
        GLACPETransactionalTestFixtureExecutionListener.plateformeDAO = (IPlateformeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("plateformeDAO");
        GLACPETransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        GLACPETransactionalTestFixtureExecutionListener.taxonDAO = (ITaxonDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("taxonDAO");
        GLACPETransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        GLACPETransactionalTestFixtureExecutionListener.extractionManager = (IExtractionManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("extractionManager");
        GLACPETransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        GLACPETransactionalTestFixtureExecutionListener.fileCompManager = (IFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompManager");
        GLACPETransactionalTestFixtureExecutionListener.fileCompConfiguration = (IFileCompConfiguration) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompConfiguration");
        GLACPETransactionalTestFixtureExecutionListener.chimieDAO = (IChimieDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("chimieDAO");
        GLACPETransactionalTestFixtureExecutionListener.synthesisManager = (ISynthesisManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("synthesisManager");
        GLACPETransactionalTestFixtureExecutionListener.ppdao = (IPPDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("productionPrimaireDAO");
        GLACPETransactionalTestFixtureExecutionListener.chlorophylleDAO = (IChlorophylleDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("chlorophylleDAO");
        GLACPETransactionalTestFixtureExecutionListener.conditionGeneraleDAO = (IConditionGeneraleDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("conditionGeneraleDAO");
        GLACPETransactionalTestFixtureExecutionListener.sondeMultiDAO = (ISondeMultiDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("sondeMultiDAO");
        GLACPETransactionalTestFixtureExecutionListener.phytoplanctonDAO = (IPhytoplanctonDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("phytoplanctonDAO");
        GLACPETransactionalTestFixtureExecutionListener.zooplanctonDAO = (IZooplanctonDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("zooplanctonDAO");
        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
        System.setProperty("concordion.output.dir", testName);
    }

    /**
     * Clean tables.
     *
     * @throws SQLException the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from group_utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from groupe where group_name!='public';")
                .execute();
    }
}

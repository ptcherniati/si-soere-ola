package org.inra.ecoinfo.utils.configuration;

import java.io.File;
import java.text.ParseException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ConfigurationException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractConfigurationTest {

    /**
     *
     */
    protected ILocalizationManager localizationManager;
    private Map<String, Test> tests = new TreeMap<String, Test>();
    String rootUser;

    /**
     *
     */
    protected AbstractConfigurationFactoryTests abstractConfigurationFactoryTests;
    private Map<String, Utilisateur> utilisateurs = new TreeMap<String, Utilisateur>();


    private Map<String, RoleTests> roles = new TreeMap<String, RoleTests>();
    private Map<String, FileTest> files = new TreeMap<String, FileTest>();
    private Map<Integer, RefdataTest> refsData = new TreeMap<Integer, RefdataTest>();
    private Map<Integer, DataTest> datas = new TreeMap<Integer, DataTest>();
    private Map<String, PrivilegeTest> privileges = new TreeMap<String, PrivilegeTest>();

    /**
     *
     */
    public AbstractConfigurationTest() {
        super();
    }
    /**
     *
     * @return
     */
    public String getRootUser() {
        return rootUser;
    }

    /**
     *
     * @param src
     * @throws ConfigurationException
     */
    public void parseInclude(String src) throws ConfigurationException {
        abstractConfigurationFactoryTests.parseInclude(src);
    }

    /**
     *
     * @param name
     * @param test
     * @throws ParseException
     */
    public void addTest(String name, Test test) throws ParseException {
        test.setLocalizationManager(localizationManager);
        tests.put(name, test);
        test.setName(name);
        if (test.getUploads() != null) {
            for (UploadTest uploadTest : test.getUploads()) {
                uploadTest.setInFileTest(files.get(uploadTest.getInFileRef()) != null ? files.get(uploadTest.getInFileRef()) : null);
            }
        }
        if (test.getDownloads() != null) {
            for (Entry<Long, DownloadTest> entryDownload : test.getDownloads().entrySet()) {
                entryDownload.getValue().setOutFile(files.get(entryDownload.getValue().getOutFileRef()) != null ? files.get(entryDownload.getValue().getOutFileRef()) : null);
            }
        }
        doAfterAddTest(name, test);
        if (test.getUtilisateurLogin() != null) {
            test.setUtilisateur(utilisateurs.get(test.getUtilisateurLogin()) != null ? utilisateurs.get(test.getUtilisateurLogin()) : null);
        }
        test.updatePrivileges(privileges);
    }

    /**
     *
     * @param name
     * @param test
     */
    abstract protected void doAfterAddTest(String name, Test test);

    /**
     *
     * @param utilisateurTest
     */
    public void addUtilisateur(Utilisateur utilisateurTest) {
        utilisateurs.put(utilisateurTest.getLogin(), utilisateurTest);
    }

    /**
     *
     * @param fileTest
     */
    public void addFile(FileTest fileTest) {
        files.put(fileTest.getName(), fileTest);
    }

    /**
     *
     * @param refData
     */
    public void addRefData(RefdataTest refData) {
        if (refData.getInFileCode() != null) {
            refData.setInFile(files.get(refData.getInFileCode()) != null ? files.get(refData.getInFileCode()) : null);
        }
        if (refData.getOutFileCode() != null) {
            refData.setOutFile(files.get(refData.getOutFileCode()) != null ? files.get(refData.getOutFileCode()) : null);
        }
        refsData.put(new Integer(refData.getOrder()), refData);
    }

    /**
     *
     * @param data
     */
    public void addData(DataTest data) {
        if (data.getInFileCode() != null) {
            data.setInFile(files.get(data.getInFileCode()) != null ? files.get(data.getInFileCode()) : null);
        }
        if (data.getOutFileCode() != null) {
            data.setOutFile(files.get(data.getOutFileCode()) != null ? files.get(data.getOutFileCode()) : null);
        }
        datas.put(new Integer(data.getOrder()), data);
    }

    /**
     *
     * @param privilege
     */
    public void addPrivilege(PrivilegeTest privilege) {
        privileges.put(privilege.getCode(), privilege);
    }

    /**
     *
     * @return
     */
    public Map<String, Test> getTests() {
        return tests;
    }

    /**
     *
     * @return
     */
    public Map<String, Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

    /**
     *
     * @return
     */
    public Map<String, RoleTests> getRoles() {
        return roles;
    }

    /**
     *
     * @return
     */
    public Map<String, FileTest> getFiles() {
        return files;
    }

    /**
     *
     * @return
     */
    public Map<Integer, RefdataTest> getRefsData() {
        return refsData;
    }

    /**
     *
     * @return
     */
    public Map<Integer, DataTest> getDatas() {
        return datas;
    }

    /**
     *
     * @param rootUser
     */
    public void setRootUser(String rootUser) {
        this.rootUser = rootUser;
    }

    /**
     *
     * @param code
     * @return
     */
    public AbstractData getRefDataByCode(String code) {
        if (code == null) {
            return null;
        }
        for (Entry<Integer, RefdataTest> refdataEntry : refsData.entrySet()) {
            if (code.equals(refdataEntry.getValue().getCode())) {
                return refdataEntry.getValue();
            }
        }
        return null;
    }

    /**
     *
     * @param code
     * @return
     */
    public AbstractData getDataByCode(String code) {
        if (code == null) {
            return null;
        }
        for (Entry<Integer, DataTest> dataEntry : datas.entrySet()) {
            if (code.equals(dataEntry.getValue().getCode())) {
                return dataEntry.getValue();
            }
        }
        return null;
    }

    /**
     *
     * @param code
     * @return
     */
    public File getFileByCode(String code) {
        if (code == null) {
            return null;
        }
        for (Entry<String, FileTest> fileEntry : files.entrySet()) {
            if (code.equals(fileEntry.getValue().getName())) {
                return new File(fileEntry.getValue().getPath());
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
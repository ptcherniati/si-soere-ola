package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class ExtractDatasTest {

    private ILocalizationManager localizationManager;
    private String name;
    private boolean rawDatas;
    private boolean datasBalancedByDepht;
    private boolean minValueAndAssociatedDepht;
    private boolean maxValueAndAssociatedDepht;
    private DepthRequestParamVO depthRequestParamVO = new DepthRequestParamVO();
    private List<ExtractionParametersVO> extractionParameters = new LinkedList<ExtractionParametersVO>();
    private List<String> extractionParametersRef = new LinkedList<String>();

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public boolean getRawDatas() {
        return rawDatas;
    }

    /**
     *
     * @return
     */
    public boolean getDatasBalancedByDepht() {
        return datasBalancedByDepht;
    }

    /**
     *
     * @return
     */
    public boolean getMinValueAndAssociatedDepht() {
        return minValueAndAssociatedDepht;
    }

    /**
     *
     * @return
     */
    public boolean getMaxValueAndAssociatedDepht() {
        return maxValueAndAssociatedDepht;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param rawDatas
     */
    public void setRawDatas(boolean rawDatas) {
        this.rawDatas = rawDatas;
    }

    /**
     *
     * @param datasBalancedByDepht
     */
    public void setDatasBalancedByDepht(boolean datasBalancedByDepht) {
        this.datasBalancedByDepht = datasBalancedByDepht;
    }

    /**
     *
     * @param minValueAndAssociatedDepht
     */
    public void setMinValueAndAssociatedDepht(boolean minValueAndAssociatedDepht) {
        this.minValueAndAssociatedDepht = minValueAndAssociatedDepht;
    }

    /**
     *
     * @param maxValueAndAssociatedDepht
     */
    public void setMaxValueAndAssociatedDepht(boolean maxValueAndAssociatedDepht) {
        this.maxValueAndAssociatedDepht = maxValueAndAssociatedDepht;
    }

    /**
     *
     * @param depthMin
     */
    public void setDepthMin(Float depthMin) {
        depthRequestParamVO.setDepthMin(depthMin);
    }

    /**
     *
     * @return
     */
    public DepthRequestParamVO getDepthRequestParamVO() {
        return depthRequestParamVO;
    }

    /**
     *
     * @return
     */
    public List<ExtractionParametersVO> getExtractionParameters() {
        return extractionParameters;
    }

    /**
     *
     * @param extractionParameter
     */
    public void addExtractionParameter(ExtractionParametersVO extractionParameter) {
        if (extractionParameter == null) {
            return;
        }
        this.datasBalancedByDepht = true;
        this.extractionParameters.add(extractionParameter);
    }

    /**
     *
     * @param depthMax
     */
    public void setDepthMax(Float depthMax) {
        depthRequestParamVO.setAllDepth(false);
        depthRequestParamVO.setDepthMax(depthMax);
    }

    /**
     *
     * @return
     */
    public List<String> getExtractionParametersRef() {
        return extractionParametersRef;
    }

    /**
     *
     * @param extractionParameterRef
     */
    public void addExtractionParameterRef(String extractionParameterRef) {
        this.extractionParametersRef.add(extractionParameterRef);
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        depthRequestParamVO.setLocalizationManager(localizationManager);
    }
}

package org.inra.ecoinfo.utils.exceptions;

import org.inra.ecoinfo.notifications.entity.Notification;

/**
 *
 * @author ptcherniati
 */
public class NotificationException extends Exception {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Notification notification;

    /**
     *
     */
    public NotificationException() {
        super();
    }

    /**
     *
     * @param string
     */
    public NotificationException(String message) {
        super(message);
    }

    /**
     *
     * @param thrwbl
     */
    public NotificationException(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param string
     * @param message
     * @param thrwbl
     * @param cause
     */
    public NotificationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param ntfctn
     */
    public NotificationException(Notification notification) {
        super(notification.getMessage());
        this.notification = notification;
    }

    /**
     *
     * @return
     */
    public Notification getNotification() {
        return notification;
    }
}
package org.inra.ecoinfo.utils.configuration;

import java.time.LocalDate;


/**
 *
 * @author ptcherniati
 */
public class ExtractionPrivilege {

    private LocalDate beginDate;
    private LocalDate EndDate;
    private boolean extraction;

    /**
     *
     */
    public ExtractionPrivilege() {
        super();
    }

    /**
     *
     * @return
     */
    public LocalDate getBeginDate() {
        return beginDate;
    }

    /**
     *
     * @return
     */
    public LocalDate getEndDate() {
        return EndDate;
    }

    /**
     *
     * @return
     */
    public boolean isExtraction() {
        return extraction;
    }

    /**
     *
     * @param beginDate
     */
    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(LocalDate endDate) {
        EndDate = endDate;
    }

    /**
     *
     * @param extraction
     */
    public void setExtraction(boolean extraction) {
        this.extraction = extraction;
    }
}
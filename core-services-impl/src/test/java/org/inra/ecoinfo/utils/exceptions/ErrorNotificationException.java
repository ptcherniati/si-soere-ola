package org.inra.ecoinfo.utils.exceptions;

import org.inra.ecoinfo.notifications.entity.Notification;

/**
 *
 * @author ptcherniati
 */
public class ErrorNotificationException extends NotificationException {

    private static final String ERROR = "error";

    /**
     *
     */
    public static final String type = ERROR;
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param ntfctn
     */
    public ErrorNotificationException(Notification notification) {
        super(notification);
        // TODO Auto-generated constructor stub
    }
}

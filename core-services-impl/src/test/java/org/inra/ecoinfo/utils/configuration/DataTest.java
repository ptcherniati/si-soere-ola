package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class DataTest extends AbstractData {

    private FileTest outFile;

    /**
     *
     * @return
     */
    public FileTest getOutFile() {
        return outFile;
    }

    /**
     *
     * @param outFile
     */
    public void setOutFile(FileTest outFile) {
        this.outFile = outFile;
    }
}

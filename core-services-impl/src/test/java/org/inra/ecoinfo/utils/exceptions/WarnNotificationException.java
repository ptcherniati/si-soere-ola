package org.inra.ecoinfo.utils.exceptions;

import org.inra.ecoinfo.notifications.entity.Notification;

/**
 *
 * @author ptcherniati
 */
public class WarnNotificationException extends NotificationException {

    private static final String WARN = "warn";

    /**
     *
     */
    public static final String type = WARN;
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param notification
     */
    public WarnNotificationException(Notification notification) {
        super(notification);
        // TODO Auto-generated constructor stub
    }
}

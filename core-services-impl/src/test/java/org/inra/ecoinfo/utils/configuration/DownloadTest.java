package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class DownloadTest {

    private Long order;
    private String outFileRef;
    private FileTest outFile;

    /**
     *
     * @return
     */
    public Long getOrder() {
        return order;
    }

    /**
     *
     * @return
     */
    public String getOutFileRef() {
        return outFileRef;
    }

    /**
     *
     * @return
     */
    public FileTest getOutFile() {
        return outFile;
    }

    /**
     *
     * @param order
     */
    public void setOrder(Long order) {
        this.order = order;
    }

    /**
     *
     * @param outFileRef
     */
    public void setOutFileRef(String outFileRef) {
        this.outFileRef = outFileRef;
    }

    /**
     *
     * @param outFile
     */
    public void setOutFile(FileTest outFile) {
        this.outFile = outFile;
    }

}

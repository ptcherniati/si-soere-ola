package org.inra.ecoinfo.utils.configuration;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.TreeMap;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.config.ConfigurationException;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.identification.IUsersManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractConfigurationFactoryTests {

    private static final String SCHEMA_PATH = ConfigurationFactoryTests.class.getResource("/META-INF/configurationTests.xsd").getPath();
    private static final String CONFIG_PATH = ConfigurationFactoryTests.class.getResource("/META-INF/configurationTests.xml").getPath();

    /**
     *
     */
    protected IUsersManager usersManager;

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    private AbstractConfigurationTest tests = null;

    /**
     *
     */
    public AbstractConfigurationFactoryTests() {
        super();
    }

    /**
     *
     * @return
     * @throws ConfigurationException
     * @throws PersistenceException
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    public AbstractConfigurationTest createConfig() throws ConfigurationException, PersistenceException, BusinessException {
        if (tests == null) {
            tests = new ConfigurationTest(this, localizationManager);
            parse();
        }
        return tests;
    }

    @SuppressWarnings("deprecation")
    private void parse() throws ConfigurationException {
        try {
            Digester digester = new Digester();
            digester.setSchema(SCHEMA_PATH);
            digester.setValidating(true);
            digester.setNamespaceAware(true);
            digester.setUseContextClassLoader(true);
            digester.push(tests);
            parseDigester(digester);
            digester.parse(new File(CONFIG_PATH));

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        } catch (SAXException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param digester
     */
    abstract protected void parseDigester(Digester digester);

    /**
     *
     * @param src
     * @throws ConfigurationException
     */
    @SuppressWarnings("deprecation")
    public void parseInclude(String src) throws ConfigurationException {
        try {
            String path = ConfigurationFactoryTests.class.getResource(src).getPath();
            Digester digester = new Digester();
            digester.setSchema(SCHEMA_PATH);
            digester.setValidating(true);
            digester.setNamespaceAware(true);
            digester.setUseContextClassLoader(true);
            digester.push(tests);
            parseDigester(digester);
            digester.parse(new File(path));

        } catch (IOException e) {
            throw new ConfigurationException(e.getMessage(), e);
        } catch (SAXException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param digester
     */
    protected void parseInclude(Digester digester) {
        digester.addCallMethod("tests/include", "parseInclude", 1, new Class[]{String.class});
        digester.addCallParam("tests/include", 0, "src");
    }

    /**
     *
     * @param digester
     */
    protected void parseTest(Digester digester) {
        digester.addCallMethod("tests/test", "addTest", 2, new Class[]{String.class, Test.class});
        digester.addCallParam("tests/test", 0, "name");
        digester.addObjectCreate("tests/test", Test.class);
        digester.addCallMethod("tests/test/utilisateurLogin", "setUtilisateurLogin", 0);
        digester.addCallMethod("tests/test/extractType", "setExtractType", 0);
        digester.addCallMethod("tests/test/beginDate", "setBeginDate", 0, new Class[]{java.sql.Date.class});
        digester.addCallMethod("tests/test/endDate", "setEndDate", 0, new Class[]{java.sql.Date.class});
        digester.addCallMethod("tests/test/upload", "addUpload", 1, new Class[]{UploadTest.class});
        digester.addObjectCreate("tests/test/upload", UploadTest.class);
        digester.addSetProperties("tests/test/upload");
        digester.addCallParam("tests/test/upload", 0, true);
        digester.addCallMethod("tests/test/download", "addDownload", 1, new Class[]{DownloadTest.class});
        digester.addObjectCreate("tests/test/download", DownloadTest.class);
        digester.addSetProperties("tests/test/download");
        digester.addCallParam("tests/test/download", 0, true);
        digester.addCallMethod("tests/test/privileges", "addPrivileges", 1, new Class[]{LinkedList.class});
        digester.addObjectCreate("tests/test/privileges", LinkedList.class);
        digester.addObjectCreate("tests/test/privileges/ref", StringBuffer.class);
        digester.addSetNext("tests/test/privileges/ref", "add");
        digester.addCallMethod("tests/test/privileges/ref", "append", 0, new Class[]{String.class});
        digester.addCallParam("tests/test/privileges", 0, true);
        digester.addCallMethod("tests/test/extract", "setExtract", 1, new Class[]{ExtractTest.class});
        digester.addObjectCreate("tests/test/extract", ExtractTest.class);
        digester.addSetProperties("tests/test/extract");
        digester.addObjectCreate("tests/test/extract/plateformes/plateforme", PlateformeTest.class);
        digester.addSetNext("tests/test/extract/plateformes/plateforme", "addPlateforme");
        digester.addSetProperties("tests/test/extract/plateformes/plateforme");
        digester.addCallMethod("tests/test/extract/variablesRef", "setVariablesRef", 1, new Class[]{RefTest.class});
        digester.addObjectCreate("tests/test/extract/variablesRef", RefTest.class);
        digester.addSetProperties("tests/test/extract/variablesRef");
        digester.addCallParam("tests/test/extract/variablesRef", 0, true);
        digester.addCallMethod("tests/test/extract/variables", "setVariables", 1, new Class[]{LinkedList.class});
        digester.addObjectCreate("tests/test/extract/variables", LinkedList.class);
        digester.addObjectCreate("tests/test/extract/variables/variable", RefTest.class);
        digester.addSetNext("tests/test/extract/variables/variable", "add");
        digester.addSetProperties("tests/test/extract/variables/variable", "code", "ref");
        digester.addCallParam("tests/test/extract/variables", 0, true);
        digester.addCallMethod("tests/test/extract/taxonsRef", "setTaxonsRef", 1, new Class[]{RefTest.class});
        digester.addObjectCreate("tests/test/extract/taxonsRef", RefTest.class);
        digester.addSetProperties("tests/test/extract/taxonsRef");
        digester.addCallParam("tests/test/extract/taxonsRef", 0, true);
        digester.addCallMethod("tests/test/extract/taxons", "setTaxons", 1, new Class[]{LinkedList.class});
        digester.addObjectCreate("tests/test/extract/taxons", LinkedList.class);
        digester.addObjectCreate("tests/test/extract/taxons/taxon", RefTest.class);
        digester.addSetNext("tests/test/extract/taxons/taxon", "add");
        digester.addSetProperties("tests/test/extract/taxons/taxon", "code", "ref");
        digester.addCallParam("tests/test/extract/taxons", 0, true);
        digester.addCallMethod("tests/test/extract/extractDatasRef", "setExtractDatasRef", 1, new Class[]{RefTest.class});
        digester.addObjectCreate("tests/test/extract/extractDatasRef", RefTest.class);
        digester.addSetProperties("tests/test/extract/extractDatasRef");
        digester.addCallParam("tests/test/extract/extractDatasRef", 0, true);
        digester.addCallMethod("tests/test/extract/extractDatas", "setExtractDatas", 1, new Class[]{ExtractDatasTest.class});
        digester.addObjectCreate("tests/test/extract/extractDatas", ExtractDatasTest.class);
        digester.addSetProperties("tests/test/extract/extractDatas");
        digester.addCallMethod("tests/test/extract/extractDatesRef", "setExtractDatesRef", 1, new Class[]{RefTest.class});
        digester.addObjectCreate("tests/test/extract/extractDatesRef", RefTest.class);
        digester.addSetProperties("tests/test/extract/extractDatesRef");
        digester.addCallParam("tests/test/extract/extractDatesRef", 0, true);
        digester.addCallMethod("tests/test/extract/extractDatesRef", "setExtractDates", 1, new Class[]{ExtractDatesTest.class});
        digester.addObjectCreate("tests/test/extract/extractDatesRef", ExtractDatesTest.class);
        digester.addSetProperties("tests/test/extract/extractDatesRef");
        digester.addCallMethod("tests/test/extract/affichage", "setAffichage", 0);
        digester.addCallMethod("tests/test/extract/commentaire", "setComment", 0);

        digester.addCallParam("tests/test/extract", 0, true);
        digester.addCallParam("tests/test", 1, true);
    }

    /**
     *
     * @param digester
     */
    protected void parseParameters(Digester digester) {
        parseExtractionParameters(digester);
        parseVariables(digester);
        parseTaxons(digester);
        parseExtractDatas(digester);
        parseExtractdates(digester);
    }

    /**
     *
     * @param digester
     */
    protected void parseExtractdates(Digester digester) {
        // cas d'un datesYearsRangeFormParam
        digester.addCallMethod("tests/extractDates/datesYearsRangeFormParam", "addDatesTest", 3, new Class[]{String.class, String.class, PeriodeHelper.class});
        digester.addCallParam("tests/extractDates/datesYearsRangeFormParam", 0, "name");
        digester.addObjectParam("tests/extractDates/datesYearsRangeFormParam", 1, DatesYearsRangeFormParamVO.LABEL);
        digester.addObjectCreate("tests/extractDates/datesYearsRangeFormParam", PeriodeHelper.class);
        digester.addCallParam("tests/extractDates/datesYearsRangeFormParam", 2, true);
        digester.addObjectCreate("tests/extractDates/datesYearsRangeFormParam/periode", TreeMap.class);
        digester.addSetNext("tests/extractDates/datesYearsRangeFormParam/periode", "addPeriod");
        digester.addCallMethod("tests/extractDates/datesYearsRangeFormParam/periode", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsRangeFormParam/periode", 0, "start");
        digester.addCallParam("tests/extractDates/datesYearsRangeFormParam/periode", 1, "beginPeriode");
        digester.addCallMethod("tests/extractDates/datesYearsRangeFormParam/periode", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsRangeFormParam/periode", 0, "end");
        digester.addCallParam("tests/extractDates/datesYearsRangeFormParam/periode", 1, "endPeriode");
        digester.addObjectCreate("tests/extractDates/datesYearsRangeFormParam/years", TreeMap.class);
        digester.addSetNext("tests/extractDates/datesYearsRangeFormParam/years", "addPeriodYear");
        digester.addCallMethod("tests/extractDates/datesYearsRangeFormParam/years", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsRangeFormParam/years", 0, "start");
        digester.addCallParam("tests/extractDates/datesYearsRangeFormParam/years", 1, "beginyear");
        digester.addCallMethod("tests/extractDates/datesYearsRangeFormParam/years", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsRangeFormParam/years", 0, "end");
        digester.addCallParam("tests/extractDates/datesYearsRangeFormParam/years", 1, "endYear");

        // cas d'un datesYearsRangeFormParam
        digester.addCallMethod("tests/extractDates/datesYearsContinuousFormParam", "addDatesTest", 3, new Class[]{String.class, String.class, PeriodeHelper.class});
        digester.addCallParam("tests/extractDates/datesYearsContinuousFormParam", 0, "name");
        digester.addObjectParam("tests/extractDates/datesYearsContinuousFormParam", 1, DatesYearsContinuousFormParamVO.LABEL);
        digester.addObjectCreate("tests/extractDates/datesYearsContinuousFormParam", PeriodeHelper.class);
        digester.addCallParam("tests/extractDates/datesYearsContinuousFormParam", 2, true);
        digester.addObjectCreate("tests/extractDates/datesYearsContinuousFormParam/periode", TreeMap.class);
        digester.addSetNext("tests/extractDates/datesYearsContinuousFormParam/periode", "addDatesPeriode");
        digester.addCallMethod("tests/extractDates/datesYearsContinuousFormParam/periode", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsContinuousFormParam/periode", 0, "start");
        digester.addCallParam("tests/extractDates/datesYearsContinuousFormParam/periode", 1, "beginDate");
        digester.addCallMethod("tests/extractDates/datesYearsContinuousFormParam/periode", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsContinuousFormParam/periode", 0, "end");
        digester.addCallParam("tests/extractDates/datesYearsContinuousFormParam/periode", 1, "endDate");

        // cas d'un datesYearsDiscretsFormParam
        digester.addCallMethod("tests/extractDates/datesYearsDiscretsFormParam", "addDatesTest", 3, new Class[]{String.class, String.class, PeriodeHelper.class});
        digester.addCallParam("tests/extractDates/datesYearsDiscretsFormParam", 0, "name");
        digester.addObjectParam("tests/extractDates/datesYearsDiscretsFormParam", 1, DatesYearsDiscretsFormParamVO.LABEL);
        digester.addObjectCreate("tests/extractDates/datesYearsDiscretsFormParam", PeriodeHelper.class);
        digester.addCallParam("tests/extractDates/datesYearsDiscretsFormParam", 2, true);
        digester.addObjectCreate("tests/extractDates/datesYearsDiscretsFormParam/periode", TreeMap.class);
        digester.addSetNext("tests/extractDates/datesYearsDiscretsFormParam/periode", "addPeriod");
        digester.addCallMethod("tests/extractDates/datesYearsDiscretsFormParam/periode", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsDiscretsFormParam/periode", 0, "start");
        digester.addCallParam("tests/extractDates/datesYearsDiscretsFormParam/periode", 1, "beginPeriode");
        digester.addCallMethod("tests/extractDates/datesYearsDiscretsFormParam/periode", "put", 2, new Class[]{String.class, String.class});
        digester.addObjectParam("tests/extractDates/datesYearsDiscretsFormParam/periode", 0, "end");
        digester.addCallParam("tests/extractDates/datesYearsDiscretsFormParam/periode", 1, "endPeriode");
        digester.addCallMethod("tests/extractDates/datesYearsDiscretsFormParam/years", "setYears", 0);
    }

    /**
     *
     * @param digester
     */
    protected void parseExtractDatas(Digester digester) {
        digester.addObjectCreate("tests/extractDatas", ExtractDatasTest.class);
        digester.addSetNext("tests/extractDatas", "addExtractDatasTest");
        digester.addCallMethod("tests/extractDatas/extractionParameterRef", "addExtractionParameterRef", 1, new Class[]{String.class});
        digester.addCallParam("tests/extractDatas/extractionParameterRef", 0, "ref");
        digester.addObjectCreate("tests/extractDatas/extractionParameter", ExtractionParametersVO.class);
        digester.addSetNext("tests/extractDatas/extractionParameter", "addExtractionParameter");
        digester.addSetProperties("tests/extractDatas/extractionParameter");
        digester.addCallMethod("tests/extractDatas/extractionParameter/aroundforValue", "addUncertainty", 1, new Class[]{String.class});
        digester.addCallParam("tests/extractDatas/extractionParameter/aroundforValue", 0, "uncertainty");
        digester.addCallMethod("tests/extractDatas/extractionParameter/aroundforValue", "addTargetValue", 1, new Class[]{String.class});
        digester.addCallParam("tests/extractDatas/extractionParameter/aroundforValue", 0, "targetValue");
        digester.addSetProperties("tests/extractDatas");
    }

    /**
     *
     * @param digester
     */
    protected void parseVariables(Digester digester) {
        digester.addCallMethod("tests/variables", "addVariables", 2, new Class[]{String.class, LinkedList.class});
        digester.addCallParam("tests/variables", 0, "name");
        digester.addObjectCreate("tests/variables", LinkedList.class);
        digester.addCallMethod("tests/variables/variable", "add", 1, new Class[]{VariableTest.class});
        digester.addObjectCreate("tests/variables/variable", VariableTest.class);
        digester.addSetProperties("tests/variables/variable");
        digester.addCallParam("tests/variables/variable", 0, true);
        digester.addCallParam("tests/variables", 1, true);
    }

    /**
     *
     * @param digester
     */
    protected void parseTaxons(Digester digester) {
        digester.addCallMethod("tests/taxons", "addTaxons", 2, new Class[]{String.class, LinkedList.class});
        digester.addCallParam("tests/taxons", 0, "name");
        digester.addObjectCreate("tests/taxons", LinkedList.class);
        digester.addCallMethod("tests/taxons/taxon", "add", 1, new Class[]{TaxonTest.class});
        digester.addObjectCreate("tests/taxons/taxon", TaxonTest.class);
        digester.addSetProperties("tests/taxons/taxon");
        digester.addCallParam("tests/taxons/taxon", 0, true);
        digester.addCallParam("tests/taxons", 1, true);
    }

    /**
     *
     * @param digester
     */
    protected void parseExtractionParameters(Digester digester) {
        digester.addCallMethod("tests/extractionParameters/extractionParameter", "addExtractionParameter", 2, new Class[]{String.class, ExtractionParametersVO.class});
        digester.addCallParam("tests/extractionParameters/extractionParameter", 0, "code");
        digester.addObjectCreate("tests/extractionParameters/extractionParameter", ExtractionParametersVO.class);
        digester.addCallParam("tests/extractionParameters/extractionParameter", 1, true);
        digester.addSetProperties("tests/extractionParameters/extractionParameter");
        digester.addCallMethod("tests/extractionParameters/extractionParameter/aroundforValue", "addUncertainty", 1, new Class[]{String.class});
        digester.addCallParam("tests/extractionParameters/extractionParameter/aroundforValue", 0, "uncertainty");
        digester.addCallMethod("tests/extractionParameters/extractionParameter/aroundforValue", "addTargetValue", 1, new Class[]{String.class});
        digester.addCallParam("tests/extractionParameters/extractionParameter/aroundforValue", 0, "targetValue");
    }

    /**
     *
     * @param digester
     */
    protected void parsePrivileges(Digester digester) {
        digester.addObjectCreate("tests/privileges/privilege", PrivilegeTest.class);
        digester.addSetNext("tests/privileges/privilege", "addPrivilege");
        digester.addSetProperties("tests/privileges/privilege");
        digester.addCallMethod("tests/privileges/privilege/extraction", "addExtraction", 1, new Class[]{ExtractionPrivilege.class});
        digester.addObjectCreate("tests/privileges/privilege/extraction", ExtractionPrivilege.class);
        digester.addCallMethod("tests/privileges/privilege/extraction/beginDate", "setBeginDate", 0, new Class[]{java.sql.Date.class});
        digester.addCallMethod("tests/privileges/privilege/extraction/endDate", "setEndDate", 0, new Class[]{java.sql.Date.class});
        digester.addSetProperties("tests/privileges/privilege/extraction");
        digester.addCallParam("tests/privileges/privilege/extraction", 0, true);
    }

    /**
     *
     * @param digester
     */
    protected void parseDatas(Digester digester) {
        digester.addObjectCreate("tests/datas/data", DataTest.class);
        digester.addSetNext("tests/datas/data", "addData");
        digester.addSetProperties("tests/datas/data");
    }

    /**
     *
     * @param digester
     */
    protected void parseMetadatas(Digester digester) {
        digester.addObjectCreate("tests/metadatas/metadata", RefdataTest.class);
        digester.addSetNext("tests/metadatas/metadata", "addRefData");
        digester.addSetProperties("tests/metadatas/metadata");
    }

    /**
     *
     * @param digester
     */
    protected void parseFiles(Digester digester) {
        digester.addObjectCreate("tests/files/file", FileTest.class);
        digester.addSetNext("tests/files/file", "addFile");
        digester.addCallMethod("tests/files/file/path", "setPath", 0);
        digester.addSetProperties("tests/files/file");
    }

    /**
     *
     * @param digester
     */
    protected void parseUtilisateurs(Digester digester) {
        digester.addObjectCreate("tests/utilisateurs/utilisateur", Utilisateur.class);
        digester.addSetNext("tests/utilisateurs/utilisateur", "addUtilisateur");
        digester.addCallMethod("tests/utilisateurs/utilisateur/nom", "setNom", 0);
        digester.addCallMethod("tests/utilisateurs/utilisateur/prenom", "setPrenom", 0);
        digester.addCallMethod("tests/utilisateurs/utilisateur/login", "setLogin", 0);
        digester.addCallMethod("tests/utilisateurs/utilisateur/email", "setEmail", 0);
        digester.addCallMethod("tests/utilisateurs/utilisateur/password", "setPassword", 0);
        digester.addCallMethod("tests/utilisateurs/utilisateur/isroot", "setIsRoot", 0, new Class[]{Boolean.class});

        digester.addCallMethod("tests/rootuser", "setRootUser", 1, new Class[]{String.class});
        digester.addCallParam("tests/rootuser", 0, "ref");
    }

    /**
     *
     * @param usersManager
     */
    public void setUsersManager(IUsersManager usersManager) {
        this.usersManager = usersManager;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
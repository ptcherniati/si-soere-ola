package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class ExtractTest {

    private List<PlateformeTest> plateformesTests = new LinkedList<PlateformeTest>();
    private String variablesRef;
    private List<VariableTest> variablesTest = new LinkedList<VariableTest>();
    private String taxonsRef;
    private List<TaxonTest> taxonsTest = new LinkedList<TaxonTest>();
    private ExtractDatasTest extractDatasTest;
    private String extractDatasRef;
    private AbstractDatesFormParam extractDatesTest;
    private String extractDatesRef;
    private String comment;
    private String projectCode;
    private int affichage = 1;
    private ILocalizationManager localizationManager;

    /**
     *
     * @return
     */
    public List<PlateformeTest> getPlateformesTests() {
        return plateformesTests;
    }

    /**
     *
     * @return
     */
    public List<VariableTest> getVariablesTests() {
        return variablesTest;
    }

    /**
     *
     * @return
     */
    public ExtractDatasTest getExtractDatasTest() {
        return extractDatasTest;
    }

    /**
     *
     * @return
     */
    public AbstractDatesFormParam getExtractDatesTest() {
        return extractDatesTest;
    }

    /**
     *
     * @param plateformetests
     */
    public void setPlateformesTests(List<PlateformeTest> plateformetests) {
        this.plateformesTests = plateformetests;
    }

    /**
     *
     * @param variabletests
     */
    public void setVariablesTests(List<VariableTest> variabletests) {
        this.variablesTest = variabletests;
    }

    /**
     *
     * @param extractDatasTest
     */
    public void setExtractDatasTest(ExtractDatasTest extractDatasTest) {
        extractDatasTest.setLocalizationManager(localizationManager);
        this.extractDatasTest = extractDatasTest;
    }

    /**
     *
     * @param extractDatesTest
     */
    public void setExtractDatesTest(AbstractDatesFormParam extractDatesTest) {
        this.extractDatesTest = extractDatesTest;
    }

    /**
     *
     * @param plateforme
     */
    public void addPlateforme(PlateformeTest plateforme) {
        this.plateformesTests.add(plateforme);
    }

    /**
     *
     * @param ref
     */
    public void setVariablesRef(RefTest ref) {
        this.variablesRef = ref.getRef();
    }

    /**
     *
     * @return
     */
    public String getVariablesRef() {
        return variablesRef;
    }

    /**
     *
     * @return
     */
    public String getExtractDatasRef() {
        return extractDatasRef;
    }

    /**
     *
     * @return
     */
    public String getExtractDatesRef() {
        return extractDatesRef;
    }

    /**
     *
     * @param ref
     */
    public void setExtractDatasRef(RefTest ref) {
        this.extractDatasRef = ref.getRef();
    }

    /**
     *
     * @param ref
     */
    public void setExtractDatesRef(RefTest ref) {
        this.extractDatesRef = ref.getRef();
    }

    /**
     *
     * @param variables
     */
    public void setVariables(List<RefTest> variables) {
        variablesTest.clear();
        for (RefTest refTest : variables) {
            variablesTest.add(new VariableTest(refTest.getRef()));
        }
    }

    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     */
    public String getProjectCode() {
        return projectCode;
    }

    /**
     *
     * @param project
     */
    public void setProjectCode(String project) {
        this.projectCode = project;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public List<TaxonTest> getTaxonsTest() {
        return taxonsTest;
    }

    /**
     *
     * @param taxonsTest
     */
    public void setTaxonsTest(List<TaxonTest> taxonsTest) {
        this.taxonsTest = taxonsTest;
    }

    /**
     *
     * @param taxonTest
     */
    public void setTaxons(List<RefTest> taxonTest) {
        taxonsTest.clear();
        for (RefTest ref : taxonTest) {
            taxonsTest.add(new TaxonTest(ref.getRef()));
        }
    }

    /**
     *
     * @return
     */
    public String getTaxonsRef() {
        return taxonsRef;
    }

    /**
     *
     * @param ref
     */
    public void setTaxonsRef(RefTest ref) {
        this.taxonsRef = ref.getRef();
    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(String affichage) {
        this.affichage = Integer.parseInt(affichage);
    }
}

package org.inra.ecoinfo.utils.configuration;

import java.io.File;

/**
 *
 * @author ptcherniati
 */
public class UploadTest {

    private FileTest inFileTest;
    private String inFileRef;
    private String projetCode;
    private String siteCode;
    private String themeCode;
    private String datatypeCode;

    /**
     *
     * @param inFile
     */
    public void setInFileTest(FileTest inFile) {
        this.inFileTest = inFile;
    }

    /**
     *
     * @return
     */
    public File getInFile() {
        return new File(inFileTest.getPath());
    }

    /**
     *
     * @return
     */
    public String getSiteCode() {
        return siteCode;
    }

    /**
     *
     * @return
     */
    public String getThemeCode() {
        return themeCode;
    }

    /**
     *
     * @return
     */
    public String getDatatypeCode() {
        return datatypeCode;
    }

    /**
     *
     * @param inFile
     */
    public void setInFile(FileTest inFile) {
        this.inFileTest = inFile;
    }

    /**
     *
     * @param siteCode
     */
    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    /**
     *
     * @param themeCode
     */
    public void setThemeCode(String themeCode) {
        this.themeCode = themeCode;
    }

    /**
     *
     * @param datatypeCode
     */
    public void setDatatypeCode(String datatypeCode) {
        this.datatypeCode = datatypeCode;
    }

    /**
     *
     * @return
     */
    public String getInFileRef() {
        return inFileRef;
    }

    /**
     *
     * @param inFileRef
     */
    public void setInFileRef(String inFileRef) {
        this.inFileRef = inFileRef;
    }

    /**
     *
     * @return
     */
    public String getInfileName() {
        return getInFile().getName();
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @param projetCode
     */
    public void setProjetCode(String projetCode) {
        this.projetCode = projetCode;
    }
}

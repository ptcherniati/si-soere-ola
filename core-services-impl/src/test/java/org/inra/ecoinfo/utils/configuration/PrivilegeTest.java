package org.inra.ecoinfo.utils.configuration;

import java.sql.Date;

/**
 *
 * @author ptcherniati
 */
public class PrivilegeTest {

    private String code;
    private String privilegePath;
    private boolean administration = false;
    private boolean depot = false;
    private boolean publication = false;
    private boolean suppression = false;
    private boolean synthese = false;
    private ExtractionPrivilege extractionPrivilege;
    private Date beginDate;
    private Date endDate;

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getPrivilegePath() {
        return privilegePath;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param privilegePath
     */
    public void setPrivilegePath(String privilegePath) {
        this.privilegePath = privilegePath;
    }

    /**
     *
     * @return
     */
    public boolean isAdministration() {
        return administration;
    }

    /**
     *
     * @return
     */
    public boolean isDepot() {
        return depot;
    }

    /**
     *
     * @return
     */
    public boolean isPublication() {
        return publication;
    }

    /**
     *
     * @return
     */
    public boolean isSuppression() {
        return suppression;
    }

    /**
     *
     * @return
     */
    public boolean isSynthese() {
        return synthese;
    }

    /**
     *
     * @param administration
     */
    public void setAdministration(boolean administration) {
        this.administration = administration;
    }

    /**
     *
     * @param depot
     */
    public void setDepot(boolean depot) {
        this.depot = depot;
    }

    /**
     *
     * @param publication
     */
    public void setPublication(boolean publication) {
        this.publication = publication;
    }

    /**
     *
     * @param suppression
     */
    public void setSuppression(boolean suppression) {
        this.suppression = suppression;
    }

    /**
     *
     * @param synthese
     */
    public void setSynthese(boolean synthese) {
        this.synthese = synthese;
    }

    /**
     *
     * @return
     */
    public Date getBeginDate() {
        return beginDate;
    }

    /**
     *
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     *
     * @param beginDate
     */
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     *
     * @param extraction
     */
    public void addExtraction(ExtractionPrivilege extraction) {
        this.extractionPrivilege = extraction;
    }

    /**
     *
     * @return
     */
    public ExtractionPrivilege getExtractionPrivilege() {
        return extractionPrivilege;
    }

    /**
     *
     * @param extractionPrivilege
     */
    public void setExtractionPrivilege(ExtractionPrivilege extractionPrivilege) {
        this.extractionPrivilege = extractionPrivilege;
    }

}

package org.inra.ecoinfo.utils;

import java.text.ParseException;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

/**
 *
 * @author ptcherniati
 */
public class IntervalDateTest {

    /** The Constant PROPERTY_MSG_BAD_DATES_FOR_PERIOD. */
    private static final String PROPERTY_MSG_BAD_DATES_FOR_PERIOD = "Une date de début est antérieure à une date de fin";
    /** The Constant firstDateddMMyyyy. */
    static final String firstDateddMMyyyy = "12-01-2011";
    /** The Constant secondDateddMMyyyy. */
    static final String secondDateddMMyyyy = "12-05-2012";
    /** The Constant thirdDateddMMyyyy. */
    static final String thirdDateddMMyyyy = "12-01-2015";
    /** The Constant fourthDateddMMyyyy. */
    static final String fourthDateddMMyyyy = "12-05-2015";
    /** The Constant dateMMyyyy. */
    static final String dateMMyyyy = "01-2011";

    /**
     * Gets the interval datedd m myyyy.
     * 
     * @throws BadExpectedValueException
     *             the bad expected value exception
     * @throws ParseException
     *             the parse exception
     */
    @Test()
    public void getIntervalDateddMMyyyy() throws BadExpectedValueException, ParseException {
        final IntervalDate interval = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        AssertJUnit.assertEquals("la date de début est mauvaise", IntervalDateTest.firstDateddMMyyyy, interval.getBeginDateToString());
        AssertJUnit.assertEquals("la date de fin est mauvaise", IntervalDateTest.fourthDateddMMyyyy, interval.getEndDateToString());
    }

    /**
     * Gets the interval datedd m myyyy date inversees.
     * 
     * @throws ParseException
     *             the parse exception
     */
    @Test()
    public void getIntervalDateddMMyyyyDateInversees() throws ParseException {
        try {
            IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.thirdDateddMMyyyy, IntervalDateTest.secondDateddMMyyyy);
        } catch (final BadExpectedValueException e) {
            AssertJUnit.assertTrue("pas de message pour date inversées", IntervalDateTest.PROPERTY_MSG_BAD_DATES_FOR_PERIOD.equals(e.getMessage()) || "PROPERTY_MSG_BAD_DATES_FOR_PERIOD".equals(e.getMessage()));
        }
    }

    /**
     * Gets the interval datedd m myyyy with bad pattern.
     * 
     * @throws BadExpectedValueException
     *             the bad expected value exception
     * @throws ParseException
     *             the parse exception
     */
    @Test(expectedExceptions = BadExpectedValueException.class)
    public void getIntervalDateddMMyyyyWithBadPattern() throws BadExpectedValueException, ParseException {
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.dateMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
    }

    /**
     * Checks if is into.
     * 
     * @throws BadExpectedValueException
     *             the bad expected value exception
     * @throws ParseException
     *             the parse exception
     */
    @Test()
    public void isInto() throws BadExpectedValueException, ParseException {
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.secondDateddMMyyyy);
        final IntervalDate secondIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.secondDateddMMyyyy, IntervalDateTest.thirdDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.thirdDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        final IntervalDate fourthIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.thirdDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.secondDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        AssertJUnit.assertFalse("la date n'est pas dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.firstDateddMMyyyy)));
        AssertJUnit.assertFalse("la date n'est pas dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.fourthDateddMMyyyy)));
        AssertJUnit.assertTrue("la date est dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.secondDateddMMyyyy)));
        AssertJUnit.assertTrue("la date est dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.thirdDateddMMyyyy)));
        AssertJUnit.assertTrue("la date est dans l'intervale", fourthIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.secondDateddMMyyyy)));
    }

    /**
     * Checks if is into inclusive.
     * 
     * @throws BadExpectedValueException
     *             the bad expected value exception
     * @throws ParseException
     *             the parse exception
     */
    @Test()
    public void isIntoInclusive() throws BadExpectedValueException, ParseException {
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.secondDateddMMyyyy);
        final IntervalDate secondIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.secondDateddMMyyyy, IntervalDateTest.thirdDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.thirdDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.thirdDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.secondDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        AssertJUnit.assertFalse("la date n'est pas dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.firstDateddMMyyyy), false, false));
        AssertJUnit.assertFalse("la date n'est pas dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.fourthDateddMMyyyy), false, false));
        AssertJUnit.assertTrue("la date est dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.secondDateddMMyyyy), true, false));
        AssertJUnit.assertTrue("la date est dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.thirdDateddMMyyyy), false, true));
        AssertJUnit.assertFalse("la date n'est pas dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.secondDateddMMyyyy), false, false));
        AssertJUnit.assertFalse("la date n'est pas dans l'intervale", secondIntervalDate.isInto(DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_FILE, IntervalDateTest.secondDateddMMyyyy), false, false));
    }

    /**
     * Overlaps.
     * 
     * @throws BadExpectedValueException
     *             the bad expected value exception
     * @throws ParseException
     *             the parse exception
     */
    @Test()
    public void overlaps() throws BadExpectedValueException, ParseException {
        final IntervalDate firstIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.secondDateddMMyyyy);
        final IntervalDate secondIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.secondDateddMMyyyy, IntervalDateTest.thirdDateddMMyyyy);
        final IntervalDate thirdIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.thirdDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        final IntervalDate fourthIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.thirdDateddMMyyyy);
        final IntervalDate fifthIntervalDate = IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.secondDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        IntervalDate.getIntervalDateddMMyyyy(IntervalDateTest.firstDateddMMyyyy, IntervalDateTest.fourthDateddMMyyyy);
        AssertJUnit.assertTrue("les deux intervales devraient se chevaucher", fourthIntervalDate.overlaps(fifthIntervalDate));
        AssertJUnit.assertFalse("les deux intervales devrait ne pas se chevaucher", firstIntervalDate.overlaps(secondIntervalDate));
        AssertJUnit.assertFalse("les deux intervales devrait ne pas se chevaucher", firstIntervalDate.overlaps(thirdIntervalDate));
    }
}
package org.inra.ecoinfo.utils.exceptions;

import org.inra.ecoinfo.notifications.entity.Notification;

/**
 *
 * @author ptcherniati
 */
public class InfoNotificationException extends NotificationException {

    private static final String INFO = "info";

    /**
     *
     */
    public static final String type = INFO;
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param ntfctn
     */
    public InfoNotificationException(Notification notification) {
        super(notification);
        // TODO Auto-generated constructor stub
    }
}

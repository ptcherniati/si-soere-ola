package org.inra.ecoinfo.utils.configuration;

import java.text.ParseException;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 *
 * @author ptcherniati
 */
public class ExtractDatesTest {

    private IntervalDate periode;
    private String name;

    /**
     *
     * @return
     */
    public IntervalDate getPeriode() {
        return periode;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param periode
     * @throws BadExpectedValueException
     * @throws ParseException
     */
    public void setPeriode(IntervalDateTest periode) throws BadExpectedValueException, ParseException {
        this.periode = IntervalDate.getIntervalDateyyyyMMdd(periode.getBeginDate(), periode.getEndDate());
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

}

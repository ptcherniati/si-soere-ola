package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class FileTest {

    private String path;
    private String name;

    /**
     *
     */
    public FileTest() {
        super();
    }

    /**
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        if (path == null) {
            return "";
        }
        String[] nodes = path.split("/");
        return nodes[nodes.length - 1];
    }

}

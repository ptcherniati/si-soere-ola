package org.inra.ecoinfo.utils.configuration;

/**
 *
 * @author ptcherniati
 */
public class RefTest {

    private String ref;

    /**
     *
     */
    public RefTest() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     *
     * @param string
     */
    public RefTest(String ref) {
        this.ref = ref;
    }

    /**
     *
     * @return
     */
    public String getRef() {
        return ref;
    }

    /**
     *
     * @param ref
     */
    public void setRef(String ref) {
        this.ref = ref;
    }
}

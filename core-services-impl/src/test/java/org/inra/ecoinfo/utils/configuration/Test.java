package org.inra.ecoinfo.utils.configuration;

import java.io.File;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class Test {

    private ILocalizationManager localizationManager;
    private String name;
    private Utilisateur utilisateur;
    private String utilisateurLogin;
    private List<PrivilegeTest> privilegesTest = new LinkedList<PrivilegeTest>();
    private String privilegeCode;
    private String extractType;
    private LocalDate beginDate;
    private LocalDate endDate;
    private List<UploadTest> uploads = new LinkedList<UploadTest>();
    private ExtractTest extract;
    private Map<Long, DownloadTest> downloads = new TreeMap<Long, DownloadTest>();
    private String outFileCode;
    private List<StringBuffer> privilegesString;
    private File inFile;

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     *
     * @return
     */
    public List<PrivilegeTest> getPrivilegesTest() {
        return privilegesTest;
    }

    /**
     *
     * @return
     */
    public LocalDate getBeginDate() {
        return beginDate;
    }

    /**
     *
     * @return
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param utilisateur
     */
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    /**
     *
     * @param privilegesTest
     */
    public void setPrivilegesTest(List<PrivilegeTest> privilegesTest) {
        this.privilegesTest = privilegesTest;
    }

    /**
     *
     * @param endDate
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     *
     * @return
     */
    public String getOutFileCode() {
        return outFileCode;
    }

    /**
     *
     * @param beginDate
     */
    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    /**
     *
     * @param outFileCode
     */
    public void setOutFileCode(String outFileCode) {
        this.outFileCode = outFileCode;
    }

    /**
     *
     * @return
     */
    public String getUtilisateurLogin() {
        return utilisateurLogin;
    }

    /**
     *
     * @return
     */
    public String getPrivilegeCode() {
        return privilegeCode;
    }

    /**
     *
     * @return
     */
    public String getExtractType() {
        return extractType;
    }

    /**
     *
     * @param utilisateurLogin
     */
    public void setUtilisateurLogin(String utilisateurLogin) {
        this.utilisateurLogin = utilisateurLogin;
    }

    /**
     *
     * @param privilegeCode
     */
    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    /**
     *
     * @return
     */
    public List<UploadTest> getUploads() {
        return uploads;
    }

    /**
     *
     * @return
     */
    public Map<Long, DownloadTest> getDownloads() {
        return downloads;
    }

    /**
     *
     * @param uploadTest
     */
    public void addUpload(UploadTest uploadTest) {
        uploads.add(uploadTest);
    }

    /**
     *
     * @param download
     */
    public void addDownload(DownloadTest download) {
        this.downloads.put(download.getOrder(), download);
    }

    /**
     *
     * @param extractType
     */
    public void setExtractType(String extractType) {
        this.extractType = extractType;
    }

    /**
     *
     * @return
     */
    public ExtractTest getExtract() {
        return extract;
    }

    /**
     *
     * @param extract
     */
    public void setExtract(ExtractTest extract) {
        extract.setLocalizationManager(localizationManager);
        this.extract = extract;
    }

    /**
     *
     * @param privileges
     */
    public void addPrivileges(List<StringBuffer> privileges) {
        this.privilegesString = privileges;
    }

    /**
     *
     * @param privileges
     */
    public void updatePrivileges(Map<String, PrivilegeTest> privileges) {
        for (StringBuffer ref : privilegesString) {
            this.privilegesTest.add(privileges.get(ref.toString()));
        }
    }

    /**
     *
     * @return
     */
    public File getInFile() {
        return inFile;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }
}

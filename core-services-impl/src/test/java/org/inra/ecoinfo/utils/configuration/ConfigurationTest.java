package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsContinuousFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsDiscretsFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesYearsRangeFormParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class ConfigurationTest extends AbstractConfigurationTest {

    private Map<String, List<VariableTest>> variablesList = new TreeMap<String, List<VariableTest>>();
    private Map<String, List<TaxonTest>> taxonsList = new TreeMap<String, List<TaxonTest>>();
    private Map<String, ExtractDatasTest> extractDatasTest = new TreeMap<String, ExtractDatasTest>();
    private Map<String, AbstractDatesFormParam> extractDatesTest = new TreeMap<String, AbstractDatesFormParam>();
    private Map<String, ExtractionParametersVO> extractionParameters = new TreeMap<String, ExtractionParametersVO>();

    /**
     *
     * @param acft
     * @param abstractConfigurationFactoryTests
     * @param ilm
     * @param localizationManager
     */
    public ConfigurationTest(AbstractConfigurationFactoryTests abstractConfigurationFactoryTests, ILocalizationManager localizationManager) {
        this.abstractConfigurationFactoryTests = abstractConfigurationFactoryTests;
        abstractConfigurationFactoryTests.setLocalizationManager(localizationManager);
        setLocalizationManager(localizationManager);
    }

    /**
     *
     * @return
     */
    public Map<String, List<VariableTest>> getVariablesList() {
        return variablesList;
    }

    /**
     *
     * @param variablesList
     */
    public void setVariablesList(Map<String, List<VariableTest>> variablesList) {
        this.variablesList = variablesList;
    }

    /**
     *
     * @param name
     * @param variables
     */
    public void addVariables(String name, List<VariableTest> variables) {
        variablesList.put(name, variables);
    }

    /**
     *
     * @param name
     * @param taxons
     */
    public void addTaxons(String name, List<TaxonTest> taxons) {
        taxonsList.put(name, taxons);
    }

    /**
     *
     * @param extractDatasTest
     */
    public void addExtractDatasTest(ExtractDatasTest extractDatasTest) {
        extractDatasTest.setLocalizationManager(localizationManager);
        if (extractDatasTest.getExtractionParametersRef() != null) {
            for (String extractionParametersRef : extractDatasTest.getExtractionParametersRef()) {
                ExtractionParametersVO extractionParameterVO = extractionParameters.get(extractionParametersRef);
                if (extractionParameterVO != null && !extractDatasTest.getExtractionParameters().contains(extractionParameterVO)) {
                    extractDatasTest.addExtractionParameter(extractionParameterVO);
                }
            }
        }
        this.extractDatasTest.put(extractDatasTest.getName(), extractDatasTest);
    }

    /**
     *
     * @return
     */
    public Map<String, ExtractDatasTest> getExtractDatasTest() {
        return extractDatasTest;
    }

    /**
     *
     * @param extractDatasTest
     */
    public void setExtractDatasTest(Map<String, ExtractDatasTest> extractDatasTest) {
        this.extractDatasTest = extractDatasTest;
    }

    /**
     *
     * @return
     */
    public Map<String, ExtractionParametersVO> getExtractionParameters() {
        return extractionParameters;
    }

    /**
     *
     * @param code
     * @param extractionParameterVO
     */
    public void addExtractionParameter(String code, ExtractionParametersVO extractionParameterVO) {
        extractionParameters.put(code, extractionParameterVO);
    }

    /**
     *
     * @param name
     * @param type
     * @param periods
     */
    public void addDatesTest(String name, String type, PeriodeHelper periods) {
        if (DatesYearsRangeFormParamVO.LABEL.equals(type)) {
            DatesYearsRangeFormParamVO datesYearsRangeFormParamVO = new DatesYearsRangeFormParamVO(localizationManager);
            datesYearsRangeFormParamVO.setPeriods(new LinkedList<Map<String, String>>());
            datesYearsRangeFormParamVO.setYears(new LinkedList<Map<String, String>>());
            for (Map<String, String> periode : periods.getPeriod()) {
                datesYearsRangeFormParamVO.getPeriods().add(periode);
            }
            for (Map<String, String> periodYear : periods.getPeriodYear()) {
                datesYearsRangeFormParamVO.getYears().add(periodYear);
            }
            extractDatesTest.put(name, datesYearsRangeFormParamVO);
        } else if (DatesYearsContinuousFormParamVO.LABEL.equals(type)) {
            DatesYearsContinuousFormParamVO datesYearsContinuousFormParamVO = new DatesYearsContinuousFormParamVO(localizationManager);
            datesYearsContinuousFormParamVO.setPeriods(new LinkedList<Map<String, String>>());
            for (Map<String, String> periode : periods.getDatesPeriode()) {
                datesYearsContinuousFormParamVO.getPeriods().add(periode);
            }
            extractDatesTest.put(name, datesYearsContinuousFormParamVO);
        } else if (DatesYearsDiscretsFormParamVO.LABEL.equals(type)) {
            DatesYearsDiscretsFormParamVO datesYearsDiscretsFormParamVO = new DatesYearsDiscretsFormParamVO(localizationManager);
            datesYearsDiscretsFormParamVO.setPeriods(new LinkedList<Map<String, String>>());
            for (Map<String, String> periode : periods.getPeriod()) {
                datesYearsDiscretsFormParamVO.getPeriods().add(periode);
            }
            datesYearsDiscretsFormParamVO.setYears(periods.getYears());
            extractDatesTest.put(name, datesYearsDiscretsFormParamVO);
        }
    }

    /**
     *
     * @param name
     * @param test
     */
    @Override
    protected void doAfterAddTest(String name, Test test) {
        if (test.getExtract() != null) {
            test.getExtract().setLocalizationManager(localizationManager);
            if (test.getExtract().getVariablesRef() != null) {
                List<VariableTest> variablesTest = new LinkedList<VariableTest>();
                variablesTest.addAll(variablesList.get(test.getExtract().getVariablesRef()));
                test.getExtract().setVariablesTests(variablesTest);
            }
            if (test.getExtract().getTaxonsRef() != null) {
                List<TaxonTest> taxonsTest = new LinkedList<TaxonTest>();
                taxonsTest.addAll(taxonsList.get(test.getExtract().getTaxonsRef()));
                test.getExtract().setTaxonsTest(taxonsTest);
            }
            if (test.getExtract().getExtractDatasRef() != null) {
                test.getExtract().setExtractDatasTest(extractDatasTest.get(test.getExtract().getExtractDatasRef()));
            }
            if (test.getExtract().getExtractDatesRef() != null) {
                test.getExtract().setExtractDatesTest(extractDatesTest.get(test.getExtract().getExtractDatesRef()));
            }
        }
    }

    /**
     *
     * @param taxonsList
     */
    public void setTaxonsList(Map<String, List<TaxonTest>> taxonsList) {
        this.taxonsList = taxonsList;
    }

    /**
     *
     * @return
     */
    public Map<String, List<TaxonTest>> getTaxonsList() {
        return taxonsList;
    }
}

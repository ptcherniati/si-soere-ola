package org.inra.ecoinfo.utils.configuration;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ptcherniati
 */
public class PeriodeHelper {

    private List<Map<String, String>> period = new LinkedList<Map<String, String>>();
    private List<Map<String, String>> periodYear = new LinkedList<Map<String, String>>();
    private List<Map<String, String>> datesPeriode = new LinkedList<Map<String, String>>();
    private String years;

    /**
     *
     * @param period
     */
    public void addPeriod(Map<String, String> period) {
        this.period.add(period);
    }

    /**
     *
     * @param periodYear
     */
    public void addPeriodYear(Map<String, String> periodYear) {
        this.periodYear.add(periodYear);
    }

    /**
     *
     * @param datesPeriode
     */
    public void addDatesPeriode(Map<String, String> datesPeriode) {
        this.datesPeriode.add(datesPeriode);
    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getPeriod() {
        return period;
    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getPeriodYear() {
        return periodYear;
    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getDatesPeriode() {
        return datesPeriode;
    }

    /**
     *
     * @return
     */
    public String getYears() {
        return years;
    }

    /**
     *
     * @param years
     */
    public void setYears(String years) {
        this.years = years;
    }
}

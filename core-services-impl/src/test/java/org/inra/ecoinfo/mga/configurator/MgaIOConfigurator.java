package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.typesite.TypeSite;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.*;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    /**
     *
     */
    public static final String ACBB = "ACBB";

    /**
     * Configuration Zero
     */
    final static Integer[] ENTRY_ORDER_0_1_2_3_4_5 = new Integer[]{0, 1, 2, 3, 4, 5};
    final static Integer[] ENTRY_ORDER_0_1_2_3_4 = new Integer[]{0, 1, 2, 3, 4};
    final static Integer[] ENTRY_ORDER_SYNTHESIS_0_1_2_3_4 = new Integer[]{0, 1, 2, 3, 4};
    final static Integer[] ENTRY_ORDER_REF = new Integer[]{0};
    final static Class<INodeable>[] ENTRY_INSTANCE_PTSSTD = new Class[]{
            Projet.class,
            TypeSite.class,
            SiteGLACPE.class,
            Theme.class,
            DataType.class
    };
    final static Class<INodeable>[] ENTRY_INSTANCE_PTSSTDV = new Class[]{
            Projet.class,
            TypeSite.class,
            SiteGLACPE.class,
            Theme.class,
            DataType.class,
            DatatypeVariableUniteGLACPE.class
    };
    final static Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{
            Refdata.class
    };
    final static Activities[] ACTIVITIES_SAPDSE
            = new Activities[]{
            Activities.synthese,
            Activities.administration,
            Activities.publication,
            Activities.depot,
            Activities.suppression,
            Activities.extraction
    };
    final static Activities[] ACTIVITIES_A
            = new Activities[]{
            Activities.associate
    };
    final static Activities[] ACTIVITIES_REF
            = new Activities[]{
            Activities.edition,
            Activities.suppression,
            Activities.telechargement,
            Activities.administration};

    final static int CODE_CONF_WITH_NO_PARCELLE = 7;

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new MgaDisplayerConfiguration() {
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {

        Class stickyLeafDatasetRights = DatatypeVariableUniteGLACPE.class;
        Configuration configDatasetRights
                = new Configuration(
                DATASET_CONFIGURATION_RIGHTS,
                DataType.class,
                ENTRY_ORDER_0_1_2_3_4_5,
                ENTRY_INSTANCE_PTSSTDV,
                ACTIVITIES_SAPDSE,
                ENTRY_ORDER_0_1_2_3_4_5,
                true,
                WhichTree.TREEDATASET,
                stickyLeafDatasetRights,
                false);
        Configuration configDataset
                = new Configuration(
                DATASET_CONFIGURATION,
                DataType.class,
                ENTRY_ORDER_0_1_2_3_4_5,
                ENTRY_INSTANCE_PTSSTD,
                ACTIVITIES_SAPDSE,
                ENTRY_ORDER_0_1_2_3_4_5,
                true,
                WhichTree.TREEDATASET,
                null,
                true);

        AbstractMgaIOConfiguration configRefdataRights
                = new Configuration(REFDATA_CONFIGURATION_RIGHTS,
                Refdata.class,
                ENTRY_ORDER_REF,
                ENTRY_INSTANCE_REF,
                ACTIVITIES_REF,
                ENTRY_ORDER_REF,
                true,
                WhichTree.TREEREFDATA,
                null,
                false);

        AbstractMgaIOConfiguration configRefdata
                = new Configuration(REFDATA_CONFIGURATION,
                Refdata.class,
                ENTRY_ORDER_REF,
                ENTRY_INSTANCE_REF,
                ACTIVITIES_REF,
                ENTRY_ORDER_REF,
                true,
                WhichTree.TREEREFDATA,
                null,
                true);

        AbstractMgaIOConfiguration configSynthesis
                = new Configuration(SYNTHESIS_CONFIGURATION,
                DataType.class,
                ENTRY_ORDER_0_1_2_3_4_5,
                ENTRY_INSTANCE_PTSSTD,
                ACTIVITIES_SAPDSE,
                ENTRY_ORDER_SYNTHESIS_0_1_2_3_4,
                false,
                WhichTree.TREEDATASET,
                null,
                false);
        configSynthesis.setSkeletonBuilder("synthesisManager");

        AbstractMgaIOConfiguration configAssociate
                = new Configuration(
                ASSOCIATE_CONFIGURATION,
                DataType.class,
                ENTRY_ORDER_0_1_2_3_4_5,
                ENTRY_INSTANCE_PTSSTD,
                ACTIVITIES_A,
                ENTRY_ORDER_0_1_2_3_4_5,
                true,
                WhichTree.TREEDATASET,
                null,
                false);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> configDatasetRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> configRefdataRights);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> configDataset);
        getConfigurations().computeIfAbsent(GENERIC_DATATYPE_CONFIGURATION, k -> configDataset);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configRefdata);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION, k -> configAssociate);
        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION, k -> configSynthesis);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }

    class Configuration extends AbstractMgaIOConfiguration {

        public Configuration(int codeConfig, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfig, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

}

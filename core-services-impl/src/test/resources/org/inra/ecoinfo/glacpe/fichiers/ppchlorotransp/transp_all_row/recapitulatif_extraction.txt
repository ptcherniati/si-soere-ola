Ce fichier récapitule vos paramètres d'extraction

Projet sélectionné:
   suivi_des_lacs

Plateformes selectionnées:
   lac3,defaut_lac3 (site: lac3)

Variables de Transparence selectionnées: 
   transparence_par_disque_inra
   transparence_par_secchi_20_cm

Toutes les profondeurs ont été sélectionnées

Types d'extractions souhaitées:
   Extraction des donnéees brutes de l'ORE
   concentration moyenne sur l'intervalle de profondeur choisi, au point de prélèvement
   Valeur minimale et profondeur associée
   Valeur maximale et profondeur associée

Périodes sélectionnées:
    du 01/01/2010 au 31/12/2010

Commentaire d'extraction:
   Extraction test de transparence

Ce fichier récapitule vos paramètres d'extraction

Projet sélectionné:
   suivi_des_lacs

Plateformes selectionnées:
   lac3,defaut_lac3 (site: lac3)

Variables de Production primaire selectionnées: 
   profondeur_mesuree
   production_primaire_par_heure
   production_primaire_par_tiers_median
   production_primaire_par_duree_d_incubation_reelle

Zone de profondeurs sélectionnées:
   minimum: 0,00
   maximum: 10,00

Types d'extractions souhaitées:
   Extraction des donnéees brutes de l'ORE
   concentration moyenne sur l'intervalle de profondeur choisi, au point de prélèvement
   Valeur minimale et profondeur associée
   Valeur maximale et profondeur associée

Périodes sélectionnées:
    du 01/01/2009 au 31/12/2009

Commentaire d'extraction:
   Extraction test de production primaire

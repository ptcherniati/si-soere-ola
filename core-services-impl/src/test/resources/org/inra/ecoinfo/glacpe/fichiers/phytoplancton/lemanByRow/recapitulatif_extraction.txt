Ce fichier r�capitule vos param�tres d'extraction

Projet s�lectionn�:: Suivi des lacs

Plateformes selectionn�es:
   point l3 b (site: lac3)

Variables de Physico chimie selectionn�es: 
   biovolume de l'esp�ce dans l'�chantillon
   nombre de cellules par ml
   nombre de champs compt�s
   nombre d'objets compt�s
   nombre d'objets par ml
   profondeur maximum
   profondeur minimum

Taxons s�lectionn�s:
   Eukaryota
   Chromista
   Chromobiota
   Cryptista
   Cryptophyta
   Cryptophyceae
   Cryptomonadales
   Cryptomonadaceae
   Cryptomonas sp.
   Heterokonta
   Bacillariophyta
   Bacillariophyceae
   Coscinodiscophyceae
   Fragilariophycidae
   Fragilariales
   Fragilariaceae
   Asterionella formosa
   Fragilaria crotonensis
   Fragilaria ulna var. angustissima
   Naviculales
   Naviculaceae
   Navicula sp.
   Opalozoa
   Plantae
   Prokaryota
   Bacteria
   Negibacteria
   Cyanobacteria
   Cyanobacteries
   Chroococcales
   Merismopediaceae
   Aphanocapsa holsatica colonie 10 �m
   Oscillatoriales
   Prymnesiophyceae
   Prymnesiophycidae
   Prymnesiales
   Prymnesiaceae
   Erkenia subaequiciliata
   Pseudanabaenaceae
   Pseudanabaena limnetica
   Pyrenomonadales
   Pyrenomonadaceae
   Rhodomonas minuta
   Rhodomonas minuta var. nannoplanctica
   Synechocystis parvula
   Thalassiosirales
   Stephanodiscaceae
   Cyclotella costei
   Stephanodiscus neoastraea
   Viridaeplantae
   Chlorophyta
   Chlorophyceae
   Chlorococcales
   Chlorellaceae
   Chlorella vulgaris
   Choricystis minor
   Prasinophyceae
   Chlorodendrales
   Chlorodendraceae
   Tetraselmis cordiformis
   Trebouxiophyceae
   Prasiolales
   Prasiolaceae
   Stichococcus bacillaris
   Volvocales
   Chlamydomonadaceae
   Chlamydomonas conica

Options s�lectionn�es:
Sommation des biovolumes de la branche
Biovolume algal total de l'�chantillon

P�riodes s�lectionn�es:
    du 13/01/2011 au 13/01/2011

Commentaire d'extraction:
   Extraction test de sonde 1

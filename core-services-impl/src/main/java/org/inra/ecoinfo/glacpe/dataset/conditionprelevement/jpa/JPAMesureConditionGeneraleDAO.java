package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IMesureConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale_;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale_;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme_;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet_;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

/**
 *
 * @author ptcherniati
 */
public class JPAMesureConditionGeneraleDAO extends AbstractJPADAO<MesureConditionGenerale> implements IMesureConditionGeneraleDAO {

    /**
     *
     * @param datePrelevement
     * @param plateformeCode
     * @param projetCode
     * @return
     */
    @Override
    public Optional<MesureConditionGenerale> getByDatePrelevementPlateformeCodeAndProjetCode(LocalDate datePrelevement, String plateformeCode, String projetCode) {
        CriteriaQuery<MesureConditionGenerale> query = builder.createQuery(MesureConditionGenerale.class);
        Root<MesureConditionGenerale> m = query.from(MesureConditionGenerale.class);
        Join<MesureConditionGenerale, Plateforme> platform = m.join(MesureConditionGenerale_.plateforme);
        Join<RealNode, Nodeable> projet = m
                .join(MesureConditionGenerale_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent)
                .join(RealNode_.parent)
                .join(RealNode_.parent)
                .join(RealNode_.nodeable);
        query
                .select(m)
                .where(
                        builder.and(
                                builder.equal(m.get(MesureConditionGenerale_.datePrelevement), datePrelevement),
                                builder.equal(platform.get(Plateforme_.code), plateformeCode),
                                builder.equal(projet.get(Projet_.code), projetCode)
                        )
                );
        return getOptional(query);
    }

    /**
     *
     * @param variableId
     * @param mesureId
     * @return
     */
    @Override
    public Optional<ValeurConditionGenerale> getByVariableIdAndMesureId(Long variableId, Long mesureId) {
        CriteriaQuery<ValeurConditionGenerale> query = builder.createQuery(ValeurConditionGenerale.class);
        Root<ValeurConditionGenerale> v = query.from(ValeurConditionGenerale.class);
        Join<ValeurConditionGenerale, MesureConditionGenerale> m = v.join(ValeurConditionGenerale_.mesure);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Join<DatatypeVariableUniteGLACPE, VariableGLACPE> variable = dvu.join(DatatypeVariableUniteGLACPE_.variable);
        Join<RealNode, Nodeable> dvuNodeable = v.join(ValeurConditionGenerale_.realNode).join(RealNode_.nodeable);
        query
                .select(v)
                .where(
                        builder.and(
                                builder.equal(m.get(MesureConditionGenerale_.id), mesureId),
                                builder.equal(dvuNodeable, dvu),
                                builder.equal(variable.get((VariableGLACPE_.id)), variableId)
                        )
                );
        return getOptional(query);
    }

}

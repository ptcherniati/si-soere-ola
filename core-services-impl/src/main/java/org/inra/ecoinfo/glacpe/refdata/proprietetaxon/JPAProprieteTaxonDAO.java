/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.proprietetaxon;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAProprieteTaxonDAO extends AbstractJPADAO<ProprieteTaxon> implements IProprieteTaxonDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<ProprieteTaxon> getByCode(String code) {
        CriteriaQuery<ProprieteTaxon> query = builder.createQuery(ProprieteTaxon.class);
        Root<ProprieteTaxon> proprieteTaxon = query.from(ProprieteTaxon.class);
        query
                .select(proprieteTaxon)
                .where(builder.equal(proprieteTaxon.get(ProprieteTaxon_.code), code));
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<ProprieteTaxon> getAllQualitativeValues() {
        CriteriaQuery<ProprieteTaxon> query = builder.createQuery(ProprieteTaxon.class);
        Root<ProprieteTaxon> proprieteTaxon = query.from(ProprieteTaxon.class);
        query
                .select(proprieteTaxon)
                .where(builder.equal(proprieteTaxon.get(ProprieteTaxon_.isQualitative), true));
        return getResultList(query);
    }

    /**
     *
     * @param type
     * @return
     */
    @Override
    public List<ProprieteTaxon> getAllProprietesTaxonsByType(String type) {
        CriteriaQuery<ProprieteTaxon> query = builder.createQuery(ProprieteTaxon.class);
        Root<ProprieteTaxon> proprieteTaxon = query.from(ProprieteTaxon.class);
        query
                .select(proprieteTaxon)
                .where(builder.equal(proprieteTaxon.get(ProprieteTaxon_.typeTaxon), type));
        return getResultList(query);
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.IVariableGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<ControleCoherence> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    /**
     *
     */
    public final static String PROPERTY_MSG_MISSING_VARIABLE = "PROPERTY_MSG_MISSING_VARIABLE";

    /**
     *
     */
    public final static String PROPERTY_MSG_MISSING_DATATYPE = "PROPERTY_MSG_MISSING_DATATYPE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_VARIABLE = "PROPERTY_MSG_BAD_VARIABLE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_DATATYPE = "PROPERTY_MSG_BAD_DATATYPE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_SITE = "PROPERTY_MSG_BAD_SITE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_MIN_VALUE = "PROPERTY_MSG_BAD_MIN_VALUE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_MAX_VALUE = "PROPERTY_MSG_BAD_MAX_VALUE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE = "PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE";

    /**
     *
     */
    public final static String PROPERTY_MSG_BAD_INTERVAL_VALUE = "PROPERTY_MSG_BAD_INTERVAL_VALUE";

    /**
     *
     */
    public final static String PROPERTY_MSG_ERROR_SAVE_OR_UPDATE = "PROPERTY_MSG_ERROR_SAVE_OR_UPDATE";
    private ISiteGLACPEDAO siteDAO;
    private IDatatypeDAO datatypeDAO;
    private IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    private IVariableGLACPEDAO variableDAO;
    private IControleCoherenceDAO controleCoherenceDAO;

    String[] namesVariablesPossibles = null;
    ConcurrentMap<String, String[]> namesDatatypesPossibles = new ConcurrentHashMap<String, String[]>();
    ConcurrentMap<String, String[]> namesSitesPossibles = new ConcurrentHashMap<String, String[]>();

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            VariableGLACPE variable = null;
            SiteGLACPE site = null;
            DataType dataType = null;
            DatatypeVariableUniteGLACPE dataTypeVariableUnite = null;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                if (Strings.isNullOrEmpty(variableCode)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_MISSING_VARIABLE), lineNumber, 1));
                } else {
                    try {
                        variable = (VariableGLACPE) variableDAO.getByCode(variableCode).orElse(null);
                        if (variable == null) {
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_VARIABLE), lineNumber, 1, variableCode));
                        }
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_VARIABLE), lineNumber, 1, variableCode));
                    }
                }
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                if (Strings.isNullOrEmpty(datatypeCode)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_MISSING_DATATYPE), lineNumber, 2));
                } else {
                    try {
                        dataType = datatypeDAO.getByCode(datatypeCode).orElse(null);
                        if (dataType == null) {
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE), lineNumber, 2, datatypeCode));
                        }
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE), lineNumber, 2, datatypeCode));
                    }
                }
                if (variable != null && dataType != null) {
                    try {
                        dataTypeVariableUnite = (DatatypeVariableUniteGLACPE) datatypeVariableUniteDAO.getByDatatypeAndVariable(datatypeCode, variableCode).orElse(null);
                        if (dataTypeVariableUnite == null) {
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE), variableCode, datatypeCode));
                        }
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATATYPE_VARIABLE_UNITE), variableCode, datatypeCode));
                    }
                }
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                if (!Strings.isNullOrEmpty(siteCode)) {
                    try {
                        site = (SiteGLACPE) siteDAO.getByPath(siteCode).orElse(null);
                        if (!Strings.isNullOrEmpty(siteCode) && site == null) {
                            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_SITE), lineNumber, 3, siteCode));
                        }
                    } catch (Exception e) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_SITE), lineNumber, 3, siteCode));
                    }
                }
                Float minValue = null;
                Float maxValue = null;
                try {
                    minValue = tokenizerValues.nextTokenFloat();
                } catch (Exception e) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MIN_VALUE), lineNumber, 4));
                }
                if (minValue == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MIN_VALUE), lineNumber, 4));
                }
                try {
                    maxValue = tokenizerValues.nextTokenFloat();
                } catch (Exception e) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MAX_VALUE), lineNumber, 5));
                }
                if (maxValue == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_MAX_VALUE), lineNumber, 5));
                }
                if (minValue > maxValue) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_INTERVAL_VALUE), lineNumber));
                }

                if (!errorsReport.hasErrors()) {
                    persistControleCoherence(errorsReport, dataTypeVariableUnite, site, minValue, maxValue, lineNumber);
                }
            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String dataypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                controleCoherenceDAO.remove(
                        controleCoherenceDAO.getBySiteAndDatatypeVariableUnite(
                                (SiteGLACPE) siteDAO.getByPath(siteCode)
                                        .orElseThrow(() -> new PersistenceException("can't retrieve site")),
                                datatypeVariableUniteDAO.getByDatatypeAndVariable(dataypeCode, variableCode)
                                        .orElseThrow(() -> new PersistenceException("can't retrieve datatype"))
                        )
                                .orElseThrow(() -> new PersistenceException("can't retrieve contrôleCohérence"))
                );
            }

        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistControleCoherence(ErrorsReport errorsReport, DatatypeVariableUniteGLACPE datatypeVariableUnite, SiteGLACPE site, Float minValue, Float maxValue, int lineNumber) {
        ControleCoherence dbControleCoherence = controleCoherenceDAO.getBySiteAndDatatypeVariableUnite(site, datatypeVariableUnite).orElse(null);
        try {
            createOrUpdateSite(site, datatypeVariableUnite, minValue, maxValue, dbControleCoherence);
        } catch (PersistenceException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_SAVE_OR_UPDATE), lineNumber));
        }
    }

    private void createOrUpdateSite(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite, Float minValue, Float maxValue, ControleCoherence dbControleCoherence) throws PersistenceException {
        if (dbControleCoherence == null) {
            createControleCoherence(site, datatypeVariableUnite, minValue, maxValue);
        } else {
            updateControleCoherence(minValue, maxValue, dbControleCoherence);
        }
    }

    private void createControleCoherence(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite, Float minValue, Float maxValue) throws PersistenceException {
        ControleCoherence controleCoherence = new ControleCoherence(site, datatypeVariableUnite, minValue, maxValue);
        controleCoherenceDAO.saveOrUpdate(controleCoherence);
    }

    private void updateControleCoherence(Float minValue, Float maxValue, ControleCoherence dbControleCoherence) throws PersistenceException {
        dbControleCoherence.setValeurMin(minValue);
        dbControleCoherence.setValeurMax(maxValue);
        controleCoherenceDAO.saveOrUpdate(dbControleCoherence);
    }

    /**
     *
     * @param controleCoherence
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ControleCoherence controleCoherence) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        ColumnModelGridMetadata columnModelGridMetadataVariable = new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getDatatypeVariableUnite().getVariable().getName(), namesVariablesPossibles, null, true, false,
                true);
        ColumnModelGridMetadata columnModelGridMetadataDatatype = new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getDatatypeVariableUnite().getDatatype().getName(), namesDatatypesPossibles, null, true,
                false, true);
        ColumnModelGridMetadata columnModelGridMetadataSite = new ColumnModelGridMetadata(controleCoherence == null || controleCoherence.getSite() == null ? EMPTY_STRING : controleCoherence.getSite().getName(), namesSitesPossibles, null, true, false,
                false);
        List<ColumnModelGridMetadata> refsVariable = new LinkedList<ColumnModelGridMetadata>();
        refsVariable.add(columnModelGridMetadataDatatype);
        columnModelGridMetadataVariable.setRefs(refsVariable);
        List<ColumnModelGridMetadata> refsDatatype = new LinkedList<ColumnModelGridMetadata>();
        refsDatatype.add(columnModelGridMetadataSite);
        columnModelGridMetadataDatatype.setRefs(refsDatatype);
        columnModelGridMetadataDatatype.setValue(controleCoherence == null ? EMPTY_STRING : controleCoherence.getDatatypeVariableUnite().getDatatype().getName());
        columnModelGridMetadataSite.setValue(controleCoherence == null || controleCoherence.getSite() == null ? EMPTY_STRING : controleCoherence.getSite().getName());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataVariable);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataDatatype);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModelGridMetadataSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getValeurMin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(controleCoherence == null ? EMPTY_STRING : controleCoherence.getValeurMax(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, true));

        return lineModelGridMetadata;
    }

    @Override
    protected List<ControleCoherence> getAllElements() {
        return controleCoherenceDAO.getAll(ControleCoherence.class);
    }

    @Override
    protected ModelGridMetadata<ControleCoherence> initModelGridMetadata() {
        Set<Nodeable> dvu = new TreeSet<>();
        Map<String, Map<String, List<String>>> mapOfValues = mgaServiceBuilder.getRecorder().getAllRealNodes()
                .filter(
                        rn -> rn.getNodeable().getNodeableType().equals(DatatypeVariableUniteGLACPE.class) && !dvu.contains(rn.getNodeable())
                )
                .peek(rn -> dvu.add((Nodeable) rn.getNodeable()))
                .distinct()
                .collect(
                        Collectors.groupingBy(
                                rn -> (((DatatypeVariableUniteGLACPE) rn.getNodeable()).getVariable().getName()),
                                Collectors.groupingBy(
                                        rn -> this.getMapKey(rn, DataType.class),
                                        Collectors.mapping(rn -> getMapKey(rn, SiteGLACPE.class), Collectors.toList())
                                        
                                )
                        )
                );
        List<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = Stream.of(namesDatatypesPossibles, namesSitesPossibles).collect(Collectors.toList());
        namesVariablesPossibles = readMapOfValuesPossibles(mapOfValues, listOfMapOfValuesPossibles, new LinkedList<>());
        return super.initModelGridMetadata();
    }

    String getMapKey(RealNode realNode, Class<? extends INodeable> nodeableClass) {
        return realNode.getNodeByNodeableTypeResource(nodeableClass).getNodeable().getName();
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteGLACPEDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableGLACPEDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.DefaultPPChloroTranspExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPRawsDatasExtractor extends AbstractPPRawsDatasExtractor {

    private static final String MAP_INDEX_0 = "0";
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> extractdatas = extractDatas(parameters.getParameters());
        ((PPChloroTranspParameter) parameters).getResults()
                .computeIfAbsent(CST_RESULT_EXTRACTION_CODE, k->extractdatas)
                .putAll(extractdatas);
        Map<String, Boolean> hasResultMap = (Map<String, Boolean>) ((PPChloroTranspParameter) parameters).getParameters()
                .computeIfAbsent(DefaultPPChloroTranspExtractor.CST_RESULTS, k->new HashMap<String, Boolean>());
                hasResultMap.put(CST_RESULT_EXTRACTION_CODE, extractdatas.get(CST_RESULT_EXTRACTION_CODE) != null);
        ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(MAP_INDEX_0, extractdatas.get(CST_RESULT_EXTRACTION_CODE));
    }
}

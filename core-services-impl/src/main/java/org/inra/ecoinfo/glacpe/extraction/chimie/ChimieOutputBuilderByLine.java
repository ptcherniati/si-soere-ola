package org.inra.ecoinfo.glacpe.extraction.chimie;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieOutputBuilderByLine extends ChimieOutputsBuildersResolver {

    private IOutputBuilder chimieBuildOutputByLine;

    /**
     *
     */
    public ChimieOutputBuilderByLine() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException, NoExtractionResultException {
        super.setChimieOutputBuilder(chimieBuildOutputByLine);
        return super.buildOutput(parameters);
        
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getChimieBuildOutputByLine() {
        return chimieBuildOutputByLine;
    }

    /**
     *
     * @param chimieBuildOutputByLine
     */
    public void setChimieBuildOutputByLine(IOutputBuilder chimieBuildOutputByLine) {
        this.chimieBuildOutputByLine = chimieBuildOutputByLine;
    }

}

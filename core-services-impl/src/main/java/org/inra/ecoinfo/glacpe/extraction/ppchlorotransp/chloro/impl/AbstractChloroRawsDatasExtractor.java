package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.MesureChloro;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractChloroRawsDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "chloroRawsDatas";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    //private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "The data may not exist for the selected period - Il est possible que les données soit inexistantes pour la période sélectionnée";

    /**
     *
     */
    protected IChlorophylleDAO chlorophylleDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param chlorophylleDAO
     */
    public void setChlorophylleDAO(IChlorophylleDAO chlorophylleDAO) {
        this.chlorophylleDAO = chlorophylleDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = AbstractGLACPEExtractor.getDepthsRequestParam(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = AbstractGLACPEExtractor.getPlatforms(requestMetadatas);
        List<IntervalDate> intervalsDates = AbstractGLACPEExtractor.getIntervalsDates(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = AbstractGLACPEExtractor.getDVUs(requestMetadatas);

        List<MesureChloro> mesuresChloro = mesuresChloro = (List<MesureChloro>) chlorophylleDAO.extractDatas(policyManager.getCurrentUser(), selectedPlateformes, intervalsDates, depthRequestParamVO, dvus);
        extractedDatasMap.put(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE, mesuresChloro);
        return extractedDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //sortSelectedVariables(requestMetadatasMap);
    }

    @Override
    public void setExtraction(Extraction extraction) {
        // TODO Auto-generated method stub
    }
}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ConditionGeneraleDatasOutputBuilder extends AbstractConditionGeneraleDatasOutputBuilder {

    private static final String HEADER_DATA = "PROPERTY_MSG_HEADER_RAW_CONDITION";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_DATATYPE_CONDITION_PRELEVEMENT = "conditions_prelevements";

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        StringBuilder stringBuilder = new StringBuilder();

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, CODE_DATATYPE_CONDITION_PRELEVEMENT);
        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, HEADER_DATA)));

            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
                Unite unite = dvu.getUnite();
                if (unite == null || unite.getCode().equals("nounit")) {
                    stringBuilder.append(String.format(";%s", localizedVariableName));
                } else {
                    stringBuilder.append(String.format(";%s (%s)", localizedVariableName, unite.getCode()));
                }
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<MesureConditionGenerale> mesuresConditionGenerale = resultsDatasMap.get(MAP_INDEX_0);
        Properties propertiesValeurQualitativeValue = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, "value");
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, CODE_DATATYPE_CONDITION_PRELEVEMENT);

        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_CSV);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_CSV);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>>> mesureByProject = mesuresConditionGenerale
                .stream()
                .collect(
                        Collectors.groupingBy(
                                m -> m.getProjet(),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> m.getSite(),
                                        TreeMap::new,
                                        Collectors.groupingBy(
                                                m -> m.getPlateforme(),
                                                TreeMap::new,
                                                Collectors.groupingBy(
                                                        m -> m.getDatePrelevement(),
                                                        TreeMap::new,
                                                        Collectors.toCollection(TreeSet::new)
                                                )
                                        )
                                )
                        )
                );
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> entryProjet : selectedPlateformes.entrySet()) {
            Projet projet = entryProjet.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mesureBySite = entryProjet.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> entrySite : mesureBySite.entrySet()) {
                SiteGLACPE site = entrySite.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                SortedSet<Plateforme> plateformes = entrySite.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_CSV));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_CSV));
                for (Plateforme plateforme : plateformes) {
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    for (Map.Entry<LocalDate, TreeSet<MesureConditionGenerale>> entryDate : mesureByProject.get(projet).get(site).get(plateforme).entrySet()) {
                        LocalDate date = entryDate.getKey();
                        TreeSet<MesureConditionGenerale> mesures = entryDate.getValue();
                        for (MesureConditionGenerale mesure : mesures) {
                            String line = String.format("%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, DateUtil.getUTCDateTextFromLocalDateTime(mesure.getDatePrelevement(), DateUtil.DD_MM_YYYY),
                                    mesure.getHeure() == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(mesure.getHeure(), DateUtil.HH_MM), mesure.getCommentaire());
                            Map<Long, ValeurConditionGenerale> valeursConditionGenerale = buildValeurs(mesure.getValeurs());
                            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                                rawDataPrintStream.print(";");
                                ValeurConditionGenerale valeurConditionGenerale = valeursConditionGenerale.get(dvu.getVariable().getId());
                                if (valeurConditionGenerale != null) {
                                    if (dvu.getVariable().getIsQualitative()) {
                                        if (valeurConditionGenerale.getValeurQualitative() != null) {
                                            String localizedValue = propertiesValeurQualitativeValue.getProperty(valeurConditionGenerale.getValeurQualitative().getValeur(), valeurConditionGenerale.getValeurQualitative().getValeur());
                                            rawDataPrintStream.print(String.format("%s", localizedValue));
                                        } else {
                                            rawDataPrintStream.print(String.format("%s", ""));
                                        }
                                    } else {
                                        if (valeurConditionGenerale.getValeur() != null) {
                                            rawDataPrintStream.print(String.format("%s", valeurConditionGenerale.getValeur()));
                                        } else {
                                            rawDataPrintStream.print(String.format("%s", ""));
                                        }
                                    }
                                }
                            }

                            rawDataPrintStream.println();
                        }
                    }

                }

            }

        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, ValeurConditionGenerale> buildValeurs(List<ValeurConditionGenerale> valeurs) {
        return valeurs.stream()
                .collect(Collectors.toMap(
                        valeur -> ((DatatypeVariableUniteGLACPE) valeur.getRealNode().getNodeable()).getVariable().getId(),
                        valeur -> valeur
                ));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ConditionGeneraleDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

}

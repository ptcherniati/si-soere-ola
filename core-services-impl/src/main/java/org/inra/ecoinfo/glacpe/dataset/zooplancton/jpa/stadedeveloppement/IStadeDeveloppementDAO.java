/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.dataset.zooplancton.jpa.stadedeveloppement;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IStadeDeveloppementDAO extends IDAO<StadeDeveloppement> {

    /**
     *
     * @param code
     * @return
     */
     Optional<StadeDeveloppement> getByCode(String code);
}

/**
 * OREILacs project - see LICENCE.txt for use created: 29 oct. 2009 15:02:16
 */
package org.inra.ecoinfo.glacpe.extraction.zooplancton.jsf.vo;

import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement;
import org.inra.ecoinfo.glacpe.extraction.vo.UniteVO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class StadeDeveloppementVO {

    private UniteVO unite;
    private String nom;
    private String code;
    private String description;
    /**
     *
     * @param stadeDeveloppement
     */
    public StadeDeveloppementVO(StadeDeveloppement stadeDeveloppement) {
        this.nom = stadeDeveloppement.getNom();
        this.description = stadeDeveloppement.getDescription();
        this.code = stadeDeveloppement.getCode();
        
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     *
     * @return
     */
    public UniteVO getUnite() {
        return unite;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(UniteVO unite) {
        this.unite = unite;
    }


    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

}

package org.inra.ecoinfo.glacpe.extraction.chimie;

import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieRawsDatasExtractor extends AbstractChimieRawsDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "chimieRawsDatas";

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        ((ChimieParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        
    }
}

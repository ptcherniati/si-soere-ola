/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Guillaume Enrico"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<RealNode> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.refdata.sitethemedatatype.messages";

    private static final String PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB";

    private static final String PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB";
    private static final String PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    private static final String PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB";

    private Map<String, String[]> datatypesPossibles = new ConcurrentHashMap<String, String[]>();

    /**
     *
     */
    protected ISiteDAO siteDAO;

    /**
     *
     */
    protected IThemeDAO themeDAO;

    /**
     *
     */
    protected IProjetDAO projetDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;
    private Map<String, String[]> projetsPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> sitesPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> themesPossibles = new ConcurrentHashMap<String, String[]>();

    /**
     *
     */
    protected IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeGLACPEDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     * The dataset dao @link(IDatasetDAO).
     */
    protected IDatasetDAO datasetDAO;

    /**
     *
     */
    public Recorder() {
    }

    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        Stream<String> buildOrderedPaths
                = mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                        configurator.getConfiguration(AbstractMgaIOConfigurator.DATASET_CONFIGURATION)
                                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", AbstractMgaIOConfigurator.DATASET_CONFIGURATION)))
                                .getEntryOrder(),
                        PatternConfigurator.SPLITER, false);
        buildOrderedPaths
                .map(this::completePath)
                .map(p -> mgaServiceBuilder.getRecorder().getRealNodeByNKey(p))
                .map(p -> p.orElse(null))
                .forEach(rn -> mgaServiceBuilder.getRecorder().remove(rn));
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
        treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
        treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
        policyManager.clearTreeFromSession();
    }

    @Override
    protected List<RealNode> getAllElements() {

        List<RealNode> nodes = null;
        try {
            nodes = projetSiteThemeDatatypeGLACPEDAO.loadRealDatatypeNodes();
            nodes = nodes.stream().sorted((o1, o2) -> o1.getPath().compareTo(o2.getPath())).collect(Collectors.toList());
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return nodes;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            final RealNode node) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata projetColumn = new ColumnModelGridMetadata(getNodeableCode(Projet.class, node, (n) -> n.getName()), this.projetsPossibles, null, true, false, true);
        final ColumnModelGridMetadata siteColumn = new ColumnModelGridMetadata(getNodeableCode(SiteGLACPE.class, node, (n) -> n.getName()), this.sitesPossibles, null, true, false, true);
        final ColumnModelGridMetadata themeColumn = new ColumnModelGridMetadata(getNodeableCode(Theme.class, node, (n) -> n.getName()), this.themesPossibles, null, true, false, true);
        final ColumnModelGridMetadata datatypeColumn = new ColumnModelGridMetadata(getNodeableCode(DataType.class, node, (n) -> n.getName()), this.datatypesPossibles, null, true, false, true);
        final List<ColumnModelGridMetadata> refsSite = new LinkedList();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(projetColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(siteColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(themeColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(datatypeColumn);
        return lineModelGridMetadata;
    }

    /**
     * @param <T>
     * @param clazz
     * @param node
     * @param method
     * @return
     */
    protected <T extends Nodeable> String getNodeableCode(Class<T> clazz, RealNode node, Function<RealNode, String> method) {
        return Optional.ofNullable(node)
                .map(n -> n.getNodeByNodeableTypeResource(clazz))
                .map(method)
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
    }

    @Override
    protected ModelGridMetadata<RealNode> initModelGridMetadata() {
        try {
            updatenamesProjetsPossibles();
            updatePathsSitesPossibles();
            updateNamesThemesPossibles();
            updateNamesDatatypesPossibles();
        } catch (PersistenceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();

        try {
            List listNodes = new ArrayList();
            List<String> buildOrderedPaths = buildOrderedPathes(errorsReport, file);
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            Stream<INode> listChild = buildLeaves(buildOrderedPaths)
                    .peek(node -> listNodes.add(node));
            persistNodes(listChild, parser);

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            addVariableNodes(listNodes);
            treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
            treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
            policyManager.clearTreeFromSession();
        } catch (final javax.persistence.PersistenceException | BusinessException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void addVariableNodes(List<INode> listNodes) {
        Map<DataType, List<DatatypeVariableUniteGLACPE>> datatypeNodes = datatypeVariableUniteDAO.getAllDatatypesVariablesUnitesGLACPE().stream()
                .collect(Collectors.groupingBy(DatatypeVariableUniteGLACPE::getDatatype));
        listNodes.stream()
                .forEach((node) -> {
                    DataType datatype = (DataType) node.getNodeable();
                    if (datatypeNodes.containsKey(datatype)) {
                        datatypeNodes.get(datatype).forEach((dvu) -> {
                            String path = String.format("%s%s%s", node.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = new RealNode(node.getRealNode(), null, dvu, path);
                            mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) node, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        });
                    }
                });
    }

    /**
     * @param listChild
     * @param parser
     */
    @Override
    protected void persistNodes(Stream<INode> listChild, final CSVParser parser) {
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        listChild.forEach((node) -> {
            mgaRecorder.persist(node);
            if (parser.getLastLineNumber() % 50 == 0) {
                mgaRecorder.getEntityManager().flush();
                mgaRecorder.getEntityManager().clear();
            }
        });
    }

    /**
     *      * from file, build new pathes for
     * agroecosystemeSiteThemeDatatypeParcelle</p>
     *
     * @param file
     * @return
     */
    protected List<String> buildOrderedPathes(ErrorsReport errorsReport, final File file) throws BusinessException {
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        List<String> pathes = datatypeVariableUniteDAO.getPathes();
        List<String> buildOrderedPaths
                = mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                        configurator.getConfiguration(AbstractMgaIOConfigurator.DATASET_CONFIGURATION)
                                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", AbstractMgaIOConfigurator.DATASET_CONFIGURATION)))
                                .getEntryOrder(),
                        PatternConfigurator.SPLITER, true)
                        .filter(p -> filter(errorsReport, p))
                        .map(p -> completePath(p))
                        .filter(p -> !pathes.contains(p))
                        .collect(Collectors.toList());
        return buildOrderedPaths;
    }

    private Boolean filter(ErrorsReport errorsReport, String p) {
        String[] split = p.split(PatternConfigurator.PATH_SEPARATOR);
        retrieveDBProjet(errorsReport, split[0]);
        retrieveDBSite(errorsReport, split[1]);
        retrieveDBTheme(errorsReport, split[2]);
        retrieveDBDatatype(errorsReport, split[3]);
        return !errorsReport.hasErrors();

    }

    private String completePath(String p) {
        p = Utils.createCodeFromString(p.replaceAll("^([^,]*),([^,]*),", "$1," + getTypeSite(p) + ",$2,"));
        return p;
    }

    private String getTypeSite(String p) {
        String[] split = p.split(PatternConfigurator.PATH_SEPARATOR);

        return siteDAO.getByPath(Utils.createCodeFromString(split[1]))
                .map(site -> ((SiteGLACPE) site).getTypeSite().getCode())
                .orElseGet(String::new);
    }

    /**
     *      * from a list af ordered pathes build a list of INode leaves</p>
     *
     * @param buildOrderedPaths
     * @return
     * @throws javax.persistence.PersistenceException
     */
    protected Stream<INode> buildLeaves(List<String> buildOrderedPaths) throws BusinessException {
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        Stream<INodeable> nodeables = mgaRecorder.getNodeables();
        HashMap<String, INodeable> entities = new HashMap();
        nodeables.forEach((nodeable) -> {
            entities.put(nodeable.getUniqueCode(), nodeable);
        });
        final IMgaIOConfiguration configuration = configurator.getConfiguration(AbstractMgaIOConfigurator.DATASET_CONFIGURATION)
                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", AbstractMgaIOConfigurator.DATASET_CONFIGURATION)));
        Class<INodeable> stickyLeafType = configuration
                .getStickyLeafType();
        List<INodeable> stickyNodeables = null;
        if (configuration.getStickyLeafType() != null) {
            stickyNodeables = mgaRecorder.getNodeables(stickyLeafType).collect(Collectors.toList());
        }
        return mgaBuilder.buildLeavesForPathes(
                buildOrderedPaths.stream(),
                configuration.getEntryType(),
                entities,
                WhichTree.TREEDATASET,
                stickyNodeables);
    }

    private Projet retrieveDBProjet(ErrorsReport errorsReport, final String projetCode) {
        Projet projet = (Projet) projetDAO.getByCode(Utils.createCodeFromString(projetCode)).orElse(null);
        if (projet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB), projetCode));
        }
        return projet;
    }

    private DataType retrieveDBDatatype(ErrorsReport errorsReport, final String datatypeCode) {
        DataType datatype = (DataType) datatypeDAO.getByCode(Utils.createCodeFromString(datatypeCode)).orElse(null);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_CODE_DATATYPE_NOT_FOUND_IN_DB), datatypeCode));
        }
        return datatype;
    }

    private Site retrieveDBSite(ErrorsReport errorsReport, final String pathSite) {
        Site site = siteDAO.getByPath(Utils.createCodeFromString(pathSite)).orElse(null);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), pathSite));
        }
        return site;
    }

    private Theme retrieveDBTheme(ErrorsReport errorsReport, final String themeCode) {
        Theme theme = (Theme) themeDAO.getByCode(Utils.createCodeFromString(themeCode)).orElse(null);
        if (theme == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB), themeCode));
        }
        return theme;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(final IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(final ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param projetSiteThemeDatatypeDAO
     */
    public void setProjetSiteThemeDatatypeGLACPEDAO(final IProjetSiteThemeDatatypeDAO projetSiteThemeDatatypeDAO) {
        this.projetSiteThemeDatatypeGLACPEDAO = projetSiteThemeDatatypeDAO;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(final IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    /**
     *
     * @param themeDAO
     */
    public void setThemeDAO(final IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    private void updateNamesDatatypesPossibles() throws PersistenceException {
        List<DataType> datatypes = datatypeDAO.getAllBy(DataType.class, DataType::getCode);
        String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        datatypesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesDatatypesPossibles);
    }

    private void updateNamesThemesPossibles() throws PersistenceException {
        List<Theme> themes = themeDAO.getAllBy(Theme.class, Theme::getCode);
        String[] namesThemesPossibles = new String[themes.size()];
        int index = 0;
        for (Theme theme : themes) {
            namesThemesPossibles[index++] = theme.getName();
        }
        themesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesThemesPossibles);
    }

    private void updatePathsSitesPossibles() throws PersistenceException {
        List<SiteGLACPE> sites = siteDAO.getAllBy(SiteGLACPE.class, SiteGLACPE::getPath);
        String[] pathsSitesPossibles = new String[sites.size()];
        int index = 0;
        for (Site site : sites) {
            pathsSitesPossibles[index++] = site.getName();
        }
        sitesPossibles.put(ColumnModelGridMetadata.NULL_KEY, pathsSitesPossibles);
    }

    private void updatenamesProjetsPossibles() throws PersistenceException {
        List<Projet> projets = projetDAO.getAll(Projet.class);
        String[] pathsProjetsPossibles = new String[projets.size()];
        int index = 0;
        for (Projet projet : projets) {
            pathsProjetsPossibles[index++] = projet.getName();
        }
        projetsPossibles.put(ColumnModelGridMetadata.NULL_KEY, pathsProjetsPossibles);
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getDatatypesPossibles() {
        return datatypesPossibles;
    }

    /**
     *
     * @param datatypesPossibles
     */
    public void setDatatypesPossibles(final Map<String, String[]> datatypesPossibles) {
        this.datatypesPossibles = datatypesPossibles;
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getProjetsPossibles() {
        return projetsPossibles;
    }

    /**
     *
     * @param projetsPossibles
     */
    public void setProjetsPossibles(final Map<String, String[]> projetsPossibles) {
        this.projetsPossibles = projetsPossibles;
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getSitesPossibles() {
        return sitesPossibles;
    }

    /**
     *
     * @param sitesPossibles
     */
    public void setSitesPossibles(final Map<String, String[]> sitesPossibles) {
        this.sitesPossibles = sitesPossibles;
    }

    /**
     *
     * @return
     */
    public Map<String, String[]> getThemesPossibles() {
        return themesPossibles;
    }

    /**
     *
     * @param themesPossibles
     */
    public void setThemesPossibles(final Map<String, String[]> themesPossibles) {
        this.themesPossibles = themesPossibles;
    }

    /**
     *
     * @return
     */
    public ISiteDAO getSiteDAO() {
        return siteDAO;
    }

    /**
     *
     * @return
     */
    public IThemeDAO getThemeDAO() {
        return themeDAO;
    }

    /**
     *
     * @return
     */
    public IProjetDAO getProjetDAO() {
        return projetDAO;
    }

    /**
     *
     * @return
     */
    public IDatatypeDAO getDatatypeDAO() {
        return datatypeDAO;
    }

    /**
     *
     * @return
     */
    public IProjetSiteThemeDatatypeDAO getProjetSiteThemeDatatypeGLACPEDAO() {
        return projetSiteThemeDatatypeGLACPEDAO;
    }


    /**
     *
     * @param datasetDAO
     */
    public void setDatasetDAO(IDatasetDAO datasetDAO) {
        this.datasetDAO = datasetDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteGLACPEDAO;
    }

}

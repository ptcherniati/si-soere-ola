/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.proprietetaxon;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProprieteTaxon> {


    private static final String TRUE_FR = "vrai";
    private static final String TRUE_EN = "true";
    private static final String FALSE_EN = "false";
    private static final String TRUE_FR_ABREV = "v";
    private static final String TRUE_EN_ABREV = "t";
    private static String[] TRUE_FALSE_ARRAY = new String[2];

    static {
        TRUE_FALSE_ARRAY[0] = TRUE_EN;
        TRUE_FALSE_ARRAY[1] = FALSE_EN;
    }
    /**
     *
     */
    protected IProprieteTaxonDAO proprieteTaxonDAO;
    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    private Properties propertiesNomFR;
    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            AbstractCSVMetadataRecorder.LOGGER.debug("Recording datas...");
            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            Set<String> typesValues = Stream.of("Phytoplancton", "Zooplancton").collect(Collectors.toSet());
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, ProprieteTaxon.TABLE_NAME);
                lineNumber++;

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String definition = tokenizerValues.nextToken();
                String isFloatString = tokenizerValues.nextToken();
                String isQualitativeString = tokenizerValues.nextToken();
                Boolean isQualitative = false;
                if (isQualitativeString != null && isQualitativeString.length() > 0) {
                    isQualitative = buildIsQualitativeValue(isQualitativeString);
                }
                String typeValue = tokenizerValues.nextToken();
                if (!typesValues.contains(typeValue)) {
                    errorsReport.addErrorMessage(String.format("Le type associé %s est invalide. Il doit être %s", typeValue, typesValues.stream().collect(Collectors.joining(" ou "))));
                    continue;
                }
                String ordreAffichage = tokenizerValues.nextToken();

                persistProprieteTaxon(errorsReport, nom, isFloatString, definition, isQualitative, typeValue, ordreAffichage);

            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            AbstractCSVMetadataRecorder.LOGGER.debug(String.format("%d records has been updated in DB...", lineNumber));

        } catch (IOException e) {
            AbstractCSVMetadataRecorder.LOGGER.error(e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                ProprieteTaxon proprieteTaxon = proprieteTaxonDAO.getByCode(code)
                        .orElseThrow(()->new PersistenceException("can't retrieve Proprietetaxon" ));
                proprieteTaxonDAO.remove(proprieteTaxon);
            }
        } catch (IOException e) {
            AbstractCSVMetadataRecorder.LOGGER.error(e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistProprieteTaxon(ErrorsReport errorsReport, String nom, String isFloatString, String definition, Boolean isQuaBoolean, String typeValue, String ordreAffichageString) throws PersistenceException {
        ProprieteTaxon dbProprieteTaxon = proprieteTaxonDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);
        createOrUpdateProprieteTaxon(nom, isFloatString, definition, isQuaBoolean, typeValue, ordreAffichageString, dbProprieteTaxon);
    }

    private Boolean buildIsQualitativeValue(String qualitativeValue) {
        if (qualitativeValue.equalsIgnoreCase(TRUE_EN) || qualitativeValue.equalsIgnoreCase(TRUE_EN_ABREV) || qualitativeValue.equalsIgnoreCase(TRUE_FR) || qualitativeValue.equalsIgnoreCase(TRUE_FR_ABREV)) {
            return new Boolean(true);
        } else {
            return new Boolean(false);
        }
    }

    private void createOrUpdateProprieteTaxon(String nom, String isFloatString, String definition, Boolean isQualitative, String typeValue, String ordreAffichageString, ProprieteTaxon dbProprieteTaxon) throws PersistenceException {
        if (dbProprieteTaxon == null) {
            createProprieteTaxon(nom, isFloatString, definition, isQualitative, typeValue, ordreAffichageString);
        } else {
            updateProprieteTaxon(isFloatString, definition, dbProprieteTaxon, isQualitative, typeValue, ordreAffichageString);
        }
    }

    private void updateProprieteTaxon(String valeur, String definition, ProprieteTaxon dbProprieteTaxon, Boolean isQualitative, String typeValue, String ordreAffichageString) {
        dbProprieteTaxon.setValueType(buildIsQualitativeValue(valeur));
        dbProprieteTaxon.setDefinition(definition);
        dbProprieteTaxon.setIsQualitative(isQualitative);
        dbProprieteTaxon.setTypeTaxon(typeValue);
        dbProprieteTaxon.setOrdreAffichage(Integer.parseInt(ordreAffichageString));
    }

    private void createProprieteTaxon(String nom, String valueTypeString, String definition, Boolean isQualitative, String typeValue, String ordreAffichageString) throws PersistenceException {
        ProprieteTaxon proprieteTaxon = new ProprieteTaxon(nom, Boolean.parseBoolean(valueTypeString), definition);
        proprieteTaxon.setIsQualitative(isQualitative);
        proprieteTaxon.setTypeTaxon(typeValue);
        proprieteTaxon.setOrdreAffichage(Integer.parseInt(ordreAffichageString));
        proprieteTaxonDAO.saveOrUpdate(proprieteTaxon);
    }

    /**
     *
     * @param proprieteTaxonDAO
     */
    public void setProprieteTaxonDAO(IProprieteTaxonDAO proprieteTaxonDAO) {
        this.proprieteTaxonDAO = proprieteTaxonDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param proprieteTaxon
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProprieteTaxon proprieteTaxon) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : proprieteTaxon.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : propertiesNomFR.get(proprieteTaxon.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : propertiesNomEN.get(proprieteTaxon.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : proprieteTaxon.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : propertiesDescriptionEN.get(proprieteTaxon.getDefinition()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : proprieteTaxon.getValueType().toString(), TRUE_FALSE_ARRAY, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : proprieteTaxon.getIsQualitative().toString(), TRUE_FALSE_ARRAY, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : proprieteTaxon.getTypeTaxon(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(proprieteTaxon == null ? EMPTY_STRING : proprieteTaxon.getOrdreAffichage().toString(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<ProprieteTaxon> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(ProprieteTaxon.TABLE_NAME, "nom", Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(ProprieteTaxon.TABLE_NAME, "nom", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(ProprieteTaxon.TABLE_NAME, "definition", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<ProprieteTaxon> getAllElements() {
        return proprieteTaxonDAO.getAllBy(ProprieteTaxon.class, ProprieteTaxon::getCode);
    }
}

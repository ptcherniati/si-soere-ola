package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 *
 * @author antoine Schellenberger
 */
public class ZooplanctonParameters extends DefaultParameter implements IParameter {

    /**
     *
     */
    public static final String KEY_MAP_DETAIL_DEVELOPMENT_STAGE = "0";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_PLATEFORMES = "1";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_PROJECT = "2";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_TAXONS = "3";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_DATE = "4";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_VARIABLES = "5";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_ID_PROJECT = "6";

    /**
     *
     */
    public static final String KEY_MAP_DETAIL_TAXON = "7";

    /**
     *
     */
    public static final String KEY_MAP_SELECTION_BIOVOLUME = "8";

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_ZOOPLANCTON = "zooplancton";

    /**
     *
     * @param map
     */
    public ZooplanctonParameters(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_ZOOPLANCTON;
    }

}

package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;


import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractSondeMultiRawsDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_SONDEMULTI = "org.inra.ecoinfo.glacpe.extraction.sondemulti.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";
    protected static final String SUFFIX_RAW_DATA = "raw";

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

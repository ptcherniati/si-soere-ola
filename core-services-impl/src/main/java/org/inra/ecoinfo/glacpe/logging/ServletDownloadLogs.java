package org.inra.ecoinfo.glacpe.logging;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author "Guillaume Enrico"
 */
@WebServlet(value = "/downloadLogs", name = "Servlet download logs")
public class ServletDownloadLogs extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static final String filePathName = "/../../logs/activity.csv";
    private static final String fileName = "si_production.csv";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletDownloadLogs() {
        super();
    }

    /**
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @SuppressWarnings("resource")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] files = request.getParameterMap().get("file");
            InputStream is = this.getClass().getResourceAsStream("../../../../../my.properties");
            java.util.Properties p = new Properties();
            p.load(is);
            String home = System.getProperty("user.home");
                String deployRepository = p.getProperty("deploy.repository");
                File file = new File(String.format("%s/%s/logs/%s", home, deployRepository,files[0]));
        if (file.exists()) {
            loadFile(String.format("%s/%s/logs/%s", home, deployRepository,files[0]), response);
        }
    }

    /**
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    private void loadFile(String filePath, HttpServletResponse response) throws IOException, ServletException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new ServletException("File doesn't exists on server.");
        }
        try (FileInputStream fis = new FileInputStream(file); ServletOutputStream os = response.getOutputStream()) {
            response.setContentType("application/text");
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s", file.getName()));

            byte[] bufferData = new byte[1024];
            int read = 0;
            while ((read = fis.read(bufferData)) != -1) {
                os.write(bufferData, 0, read);
            };
        }
    }
}

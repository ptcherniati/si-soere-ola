package org.inra.ecoinfo.glacpe.dataset.hautefrequence.jpa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.IHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.MesureHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.MesureHauteFrequence_;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SequenceHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SequenceHauteFrequence_;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SousSequenceHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SousSequenceHauteFrequence_;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.ValeurMesureHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.ValeurMesureHauteFrequence_;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.dimensions.Dimension;
import org.inra.ecoinfo.glacpe.refdata.dimensions.Dimension_;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure_;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.synthesis.hautefrequence.SynthesisValue;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author mylene
 */
public class JPAHauteFrequenceDAO extends AbstractJPADAO<Object> implements IHauteFrequenceDAO {

    private static final String ALIAS_PLATFORM = "platform";
    private static final String ALIAS_PROJECT = "projet";
    private static final String ALIAS_SITE = "site";
    private static final String ALIAS_VARIABLE = "variable";
    private static final String ALIAS_DEPTH = "depth";

    private static final String REQUEST_HAUTEFREQUENCE_AVAILABLES_PROJETS = "select distinct s.projetSite.projet from SequenceHauteFrequence s";
    private static final String REQUEST_HAUTEFREQUENCE_PROJETSITE_BY_PROJET_ID_AND_SITE_ID = "select distinct s.projetSite from SequenceHauteFrequence s where s.projetSite.projet.id=:projetId and s.projetSite.site.id=:siteId ";
    private static final String REQUEST_HAUTEFREQUENCE_AVAILABLES_PLATEFORMES_BY_PROJETSITE_ID = "select distinct ss.plateforme from SousSequenceHauteFrequence ss where ss.sequence.projetSite.id = :projetSiteId";
    private static final String REQUEST_HAUTEFREQUENCE_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct vm.variable from ValeurMesureHauteFrequence vm where vm.mesure.sousSequence.plateforme.id in (:plateformesIds) and vm.mesure.sousSequence.sequence.datePrelevement between :firstDate and :lastDate and vm.valeur is not null order by vm.variable.ordreAffichageGroupe";

    private static final String REQUEST_AllDepth = "select m from MesureHauteFrequence m where m.sousSequence.plateforme.id in (:selectedPlateformesIds) and m.sousSequence.sequence.projetSite.id in (:selectedProjetId) %s  order by m.sousSequence.sequence.projetSite.projet.nom,m.sousSequence.plateforme.site.nom,m.sousSequence.plateforme.nom,m.sousSequence.sequence.datePrelevement,m.profondeur";
    private static final String REQUEST_BetweenDepth = "select m from MesureHauteFrequence m where m.sousSequence.plateforme.id in (:selectedPlateformesIds) and m.sousSequence.sequence.projetSite.id in (:selectedProjetId) and m.profondeur >= :depthMin and m.profondeur <= :depthMax %s  order by m.sousSequence.sequence.projetSite.projet.nom,m.sousSequence.plateforme.site.nom,m.sousSequence.plateforme.nom,m.sousSequence.sequence.datePrelevement,m.profondeur";
    private static final String REQUEST_UniqueDepth = "select ss from SousSequenceHauteFrequence ss where ss.plateforme.id in (:selectedPlateformesIds) and ss.sequence.projetSite.id in (:selectedProjetId) %s  order by ss.sequence.projetSite.projet.nom,ss.plateforme.site.nom,ss.plateforme.nom,ss.sequence.datePrelevement";

    private static final String HQL_DATE_FIELD_1 = "vm.mesure.sousSequence.sequence.datePrelevement";
    private static final String HQL_DATE_FIELD_2 = "m.sousSequence.sequence.datePrelevement";
    private static final String HQL_DATE_FIELD_3 = "ss.sequence.datePrelevement";

    /**
     *
     * @param user
     * @param projetSiteId
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms(IUser user) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Root<SousSequenceHauteFrequence> ss = query.from(SousSequenceHauteFrequence.class);
        Join<SousSequenceHauteFrequence, SequenceHauteFrequence> s = ss.join(SousSequenceHauteFrequence_.sequence);
        Join<Dataset, RealNode> rnd2 = s.join(SequenceHauteFrequence_.versionFile).join(VersionFile_.dataset).join(Dataset_.realNode);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rns = rnt.join(RealNode_.parent);
        Path<Nodeable> site = rns.get(RealNode_.nodeable);
        Join<RealNode, RealNode> rnts = rns.join(RealNode_.parent);
        Join<RealNode, RealNode> rnp = rnts.join(RealNode_.parent);
        Path<Nodeable> projet = rnp.get(RealNode_.nodeable);
        Path<AbstractBranchNode> ndss = ndsv.get(NodeDataSet_.parent).get(NodeDataSet_.parent).get(NodeDataSet_.parent);
        Join<SousSequenceHauteFrequence, Plateforme> platform = ss.join(SousSequenceHauteFrequence_.plateforme);
        List<Predicate> and = new ArrayList<>();
        Path<LocalDateTime> date = sv.get(GenericSynthesisValue_.date);
        and.add(builder.equal(rnd, rnd2));
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .multiselect(projet.alias(ALIAS_PROJECT), rns.alias(ALIAS_SITE), platform.alias(ALIAS_PLATFORM))
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultListToStream(query)
                .map(tuple -> getRealNodePlatformFromTuple(tuple))
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private PlateformeProjetVO getRealNodePlatformFromTuple(Tuple tuple) {
        Plateforme platform = tuple.get(ALIAS_PLATFORM, Plateforme.class);
        SiteGLACPE site = tuple.get(ALIAS_SITE, SiteGLACPE.class);
        Projet projet = tuple.get(ALIAS_PROJECT, Projet.class);
        return new PlateformeProjetVO(platform, projet, site);
    }

    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            List<String> groups = getGroups(user);
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            List<Predicate> orLogin = new LinkedList<>();
            predicatesAnd.add(er.get(ExtractActivity_.login).in(groups));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

    /**
     *
     * @param user
     * @return
     */
    public List<String> getGroups(IUser user) {
        List<String> l = new LinkedList();
        if (user instanceof ICompositeGroup) {
            l = user.getAllGroups().stream()
                    .filter(g -> g.getWhichTree().equals(WhichTree.TREEDATASET))
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
        } else {
            l.add(user.getLogin());
        }
        return l;
    }

    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getAvailableVariables(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Root<SousSequenceHauteFrequence> ss = query.from(SousSequenceHauteFrequence.class);
        Join<SousSequenceHauteFrequence, SequenceHauteFrequence> s = ss.join(SousSequenceHauteFrequence_.sequence);
        Join<Dataset, RealNode> rnd2 = s.join(SequenceHauteFrequence_.versionFile).join(VersionFile_.dataset).join(Dataset_.realNode);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rns = rnt.join(RealNode_.parent);
        Join<RealNode, RealNode> rnts = rns.join(RealNode_.parent);
        Join<RealNode, RealNode> rnp = rnts.join(RealNode_.parent);
        Path<Nodeable> projet = rnp.get(RealNode_.nodeable);
        Join<SousSequenceHauteFrequence, Plateforme> platform = ss.join(SousSequenceHauteFrequence_.plateforme);
        ArrayList<Predicate> and = new ArrayList<>();
        Path<LocalDateTime> date = sv.get(GenericSynthesisValue_.date);
        and.add(builder.equal(rnv.get(RealNode_.nodeable), dvu.get(DatatypeVariableUniteGLACPE_.id)));
        and.add(builder.equal(rnd, rnd2));
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        if (!selectedPlatforms.isEmpty()) {
            ArrayList<Predicate> or = new ArrayList<>();
            selectedPlatforms.stream()
                    .forEach(pp -> or.add(builder.and(
                    builder.equal(projet, pp.getProjet()),
                    builder.equal(platform, pp.getPlateforme())
            )));
            and.add(builder.or(or.toArray(new Predicate[and.size()])));
        }
        if (!selectedDates.isEmpty()) {
            selectedDates.stream()
                    .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        }
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(dvu)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Float> getAvailableDepths(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        CriteriaQuery<Float> query = builder.createQuery(Float.class);
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Root<MesureHauteFrequence> m = query.from(MesureHauteFrequence.class);
        Join<MesureHauteFrequence, SousSequenceHauteFrequence> ss = m.join(MesureHauteFrequence_.sousSequence);
        Join<SousSequenceHauteFrequence, SequenceHauteFrequence> s = ss.join(SousSequenceHauteFrequence_.sequence);
        Join<Dataset, RealNode> rnd2 = s.join(SequenceHauteFrequence_.versionFile).join(VersionFile_.dataset).join(Dataset_.realNode);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rns = rnt.join(RealNode_.parent);
        Join<RealNode, RealNode> rnts = rns.join(RealNode_.parent);
        Join<RealNode, RealNode> rnp = rnts.join(RealNode_.parent);
        Path<Nodeable> projet = rnp.get(RealNode_.nodeable);
        Join<SousSequenceHauteFrequence, Plateforme> platform = ss.join(SousSequenceHauteFrequence_.plateforme);
        Join<OutilsMesure, Dimension> dimension = ss.join(SousSequenceHauteFrequence_.outilsMesure).join(OutilsMesure_.dimension);
        ArrayList<Predicate> and = new ArrayList<>();
        Path<LocalDateTime> date = sv.get(GenericSynthesisValue_.date);
        and.add(builder.equal(rnv.get(RealNode_.nodeable), dvu.get(DatatypeVariableUniteGLACPE_.id)));
        and.add(builder.equal(rnd, rnd2));
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        if (!selectedPlatforms.isEmpty()) {
            ArrayList<Predicate> or = new ArrayList<>();
            selectedPlatforms.stream()
                    .forEach(pp -> or.add(builder.and(
                    builder.equal(projet, pp.getProjet()),
                    builder.equal(platform, pp.getPlateforme())
            )));
            and.add(builder.or(or.toArray(new Predicate[and.size()])));
        }
        if (!selectedDates.isEmpty()) {
            selectedDates.stream()
                    .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        }
        if (!selectedVariables.isEmpty()) {
            and.add(dvu.in(selectedVariables));
        }
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(dimension.get(Dimension_.zReleve).alias(ALIAS_DEPTH))
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param platformsMap
     * @param intervalDates
     * @param depths
     * @param dvus
     * @return
     */
    @Override
    public List<ValeurMesureHauteFrequence> extractValueDatas(IUser user, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> platformsMap, List<IntervalDate> intervalDates, DepthRequestParamVO depths, List<DatatypeVariableUniteGLACPE> dvus) {
        List<Predicate> and = new LinkedList<>();
        CriteriaQuery<ValeurMesureHauteFrequence> query = builder.createQuery(ValeurMesureHauteFrequence.class);
        Root<ValeurMesureHauteFrequence> v = query.from(ValeurMesureHauteFrequence.class);
        Path<RealNode> get = v.get(ValeurMesureHauteFrequence_.realNode);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rns = rnv.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> siteNodeable = rns.join(RealNode_.nodeable);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> projetNodeable = rnp.join(RealNode_.nodeable);
        Join<ValeurMesureHauteFrequence, MesureHauteFrequence> m = v.join(ValeurMesureHauteFrequence_.mesure);
        Join<MesureHauteFrequence, SousSequenceHauteFrequence> ss = m.join(MesureHauteFrequence_.sousSequence);
        Join<SousSequenceHauteFrequence, SequenceHauteFrequence> s = ss.join(SousSequenceHauteFrequence_.sequence);
        Path<Float> depth = ss.join(SousSequenceHauteFrequence_.outilsMesure).join(OutilsMesure_.dimension).get(Dimension_.zReleve);
        Path<LocalDate> dateMesure = s.get(SequenceHauteFrequence_.datePrelevement);
        and.add(builder.equal(rnv, v.get(ValeurMesureHauteFrequence_.realNode)));
        if (!depths.getAllDepth()) {
            and.add(builder.ge(depth, depths.getDepthMin()));
            and.add(builder.le(depth, depths.getDepthMax()));
        }
        if (!dvus.isEmpty()) {
            and.add(rnv.get(RealNode_.nodeable).in(dvus));
        }
        List<Predicate> orPlateforme = new LinkedList();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> ProjetEntry : platformsMap.entrySet()) {
            Projet projet = ProjetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> platformsBySites = ProjetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : platformsBySites.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = (SortedSet<Plateforme>) siteEntry.getValue();
                for (Plateforme platform : platforms) {
                    orPlateforme.add(
                            builder.and(
                                    builder.equal(rns.get(RealNode_.nodeable), site),
                                    builder.equal(rnp.get(RealNode_.nodeable), projet),
                                    builder.equal(ss.get(SousSequenceHauteFrequence_.plateforme), platform)
                            ));
                }
            }
        }
        if (!orPlateforme.isEmpty()) {
            and.add(builder.or(orPlateforme.stream().toArray(Predicate[]::new)));
        }
        intervalDates.stream()
                .map(interval -> builder.between(dateMesure, interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        l -> and.add(builder.or(l.stream().toArray(Predicate[]::new)))
                ));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, dateMesure);
        query
                .select(v)
                .distinct(true)
                //                .orderBy(
                //                        builder.asc(projetNodeable.get(Nodeable_.code)),
                //                        builder.asc(siteNodeable.get(Nodeable_.code)),
                //                        builder.asc(ss.get(SousSequenceHauteFrequence_.plateforme)),
                //                        builder.asc(dateMesure),
                //                        builder.asc(m.get(MesureHauteFrequence_.profondeurMin)),
                //                        builder.asc(m.get(MesureHauteFrequence_.profondeurMax))
                //                )
                .where(and.stream().toArray(Predicate[]::new));
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param platformsMap
     * @param intervalDates
     * @param depths
     * @param dvus
     * @return
     */
    @Override
    public List<MesureHauteFrequence> extractDatas(IUser user, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> platformsMap, List<IntervalDate> intervalDates, DepthRequestParamVO depths, List<DatatypeVariableUniteGLACPE> dvus) {
        List<Predicate> and = new LinkedList<>();
        CriteriaQuery<MesureHauteFrequence> query = builder.createQuery(MesureHauteFrequence.class);
        Root<ValeurMesureHauteFrequence> v = query.from(ValeurMesureHauteFrequence.class);
        Path<RealNode> get = v.get(ValeurMesureHauteFrequence_.realNode);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rns = rnv.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> siteNodeable = rns.join(RealNode_.nodeable);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> projetNodeable = rnp.join(RealNode_.nodeable);
        Join<ValeurMesureHauteFrequence, MesureHauteFrequence> m = v.join(ValeurMesureHauteFrequence_.mesure);
        m.fetch(MesureHauteFrequence_.valeurs);
        Join<MesureHauteFrequence, SousSequenceHauteFrequence> ss = m.join(MesureHauteFrequence_.sousSequence);
        Join<SousSequenceHauteFrequence, SequenceHauteFrequence> s = ss.join(SousSequenceHauteFrequence_.sequence);
        Path<Float> depth = ss.join(SousSequenceHauteFrequence_.outilsMesure).join(OutilsMesure_.dimension).get(Dimension_.zReleve);
        Path<LocalDate> dateMesure = s.get(SequenceHauteFrequence_.datePrelevement);
        and.add(builder.equal(rnv, v.get(ValeurMesureHauteFrequence_.realNode)));
        if (!depths.getAllDepth()) {
            and.add(builder.ge(depth, depths.getDepthMin()));
            and.add(builder.le(depth, depths.getDepthMax()));
        }
        if (!dvus.isEmpty()) {
            and.add(rnv.get(RealNode_.nodeable).in(dvus));
        }
        List<Predicate> orPlateforme = new LinkedList();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> ProjetEntry : platformsMap.entrySet()) {
            Projet projet = ProjetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> platformsBySites = ProjetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : platformsBySites.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = (SortedSet<Plateforme>) siteEntry.getValue();
                for (Plateforme platform : platforms) {
                    orPlateforme.add(
                            builder.and(
                                    builder.equal(rns.get(RealNode_.nodeable), site),
                                    builder.equal(rnp.get(RealNode_.nodeable), projet),
                                    builder.equal(ss.get(SousSequenceHauteFrequence_.plateforme), platform)
                            ));
                }
            }
        }
        if (!orPlateforme.isEmpty()) {
            and.add(builder.or(orPlateforme.stream().toArray(Predicate[]::new)));
        }
        intervalDates.stream()
                .map(interval -> builder.between(dateMesure, interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        l -> and.add(builder.or(l.stream().toArray(Predicate[]::new)))
                ));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, dateMesure);
        query
                .select(m)
                .distinct(true)
                //                .orderBy(
                //                        builder.asc(projetNodeable.get(Nodeable_.code)),
                //                        builder.asc(siteNodeable.get(Nodeable_.code)),
                //                        builder.asc(ss.get(SousSequenceHauteFrequence_.plateforme)),
                //                        builder.asc(dateMesure),
                //                        builder.asc(m.get(MesureHauteFrequence_.profondeurMin)),
                //                        builder.asc(m.get(MesureHauteFrequence_.profondeurMax))
                //                )
                .where(and.stream().toArray(Predicate[]::new));
        return getResultList(query);
    }
}

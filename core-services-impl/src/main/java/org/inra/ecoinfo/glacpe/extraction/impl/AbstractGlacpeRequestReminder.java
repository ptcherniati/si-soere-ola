/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.impl;

import java.io.PrintStream;
import java.time.DateTimeException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.impl.ZooplanctonParameters;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.jfree.data.json.impl.JSONObject;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGlacpeRequestReminder extends AbstractOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";
    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    private static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";

    private static final String PROPERTY_MSG_SELECTED_VARIABLES = "MSG_SELECTED_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_TAXONS = "MSG_SELECTED_TAXONS";
    private static final String PROPERTY_MSG_SITE = "PROPERTY_MSG_SITE";
    private static final String PROPERTY_MSG_SELECTED_PLATEFORMS = "MSG_SELECTED_PLATEFORMS";
    private static final String PROPERTY_MSG_SELECTED_BIOVOLUME = "MSG_SELECTED_BIOVOLUME";
    private static final String MSG_GROUP = "PROPERTY_MSG_GROUP";
    private static final String MSG_SELECTED_PROJECT = "MSG_SELECTED_PROJECT";
    private static final String MSG_SELECTED_PERIODS = "MSG_SELECTED_PERIODS";
    private static final String MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    private static final String PROPERTY_MSG_SELECTED_PERIODS = "MSG_SELECTED_PERIODS";
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    private static final String KEYMAP_COMMENTS = "comments";

    protected void printDatesSummary(Map<String, Object> requestMetadatasMap, PrintStream reminderPrintStream) throws BusinessException {

        DatesRequestParamVO datesRequestParamVO = AbstractGLACPEExtractor.getDatesRequestParam(requestMetadatasMap);
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PERIODS));
        try {
            AbstractGLACPEExtractor.getIntervalsDates(requestMetadatasMap)
                    .stream()
                    .forEach(i -> {
                        MDC.put("extract.date.start", i.getBeginDateToString());
                        MDC.put("extract.date.end", i.getEndDateToString());
                    });
        } catch (DateTimeException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderPrintStream.println();
    }

    protected void printPlateformesSummary(Map<String, Object> requestMetadatasMap, PrintStream reminderPrintStream) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> platformsMap = AbstractGLACPEExtractor.getPlatforms(requestMetadatasMap);
        Properties propertiesNomProjet = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesNomPlateforme = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesNomSite = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        JSONObject jo = new JSONObject();
        SortedMap<String,SortedSet<String>> ps = new TreeMap<>();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> projetEntry : platformsMap.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = projetEntry.getValue();
            reminderPrintStream.println(String.format(PROPERTY_MSG_SELECTED_PLATEFORMS, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_PROJECT), propertiesNomProjet.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName())));
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> entry : mapBySite.entrySet()) {
                SiteGLACPE site = entry.getKey();
                SortedSet<Plateforme> platforms = entry.getValue();
                reminderPrintStream.println();
                reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PLATEFORMS));
                for (Plateforme plateforme : platforms) {
                    final String sp = String.format(PATTERN_STRING_PLATEFORMS_SUMMARY, propertiesNomPlateforme.getProperty(plateforme.getName(), plateforme.getName()), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SITE), propertiesNomSite.getProperty(site.getCode(), site.getCode()));
                    ps.computeIfAbsent(projet.getCode(), k->new TreeSet<>()).add(sp);
                    reminderPrintStream.println(sp);
                }
            }
            reminderPrintStream.println();
        }
        MDC.put("extract.plateforms", jo.toJSONString(ps));
    }

    protected void printProjectSummary(Projet project, PrintStream reminderPrintStream) {
        Properties propertiesNomProject = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_PLATEFORMS));
        reminderPrintStream.println(propertiesNomProject.getProperty(project.getName(), project.getName()));
        reminderPrintStream.println();
    }

    protected void printVariablesSummary(Map<String, Object> requestMetadatasMap, PrintStream reminderPrintStream) {
        List<DatatypeVariableUniteGLACPE> dvus = AbstractGLACPEExtractor.getDVUs(requestMetadatasMap);
        Boolean selectionBiovolume = Optional.ofNullable((Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_BIOVOLUME)).orElse(Boolean.FALSE);
        Properties propertiesNomVariable = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesNomGroupe = getLocalizationManager().newProperties(GroupeVariable.TABLE_NAME, "nom_groupe");
        Properties propertiesNomDatatype = getLocalizationManager().newProperties(DataType.NAME_ENTITY_JPA, "name");
        reminderPrintStream.println(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_VARIABLES), propertiesNomDatatype.getProperty("Physico chimie", "Physico chimie")));
        if (selectionBiovolume) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_BIOVOLUME)));
            MDC.put("extract.biovolume", selectionBiovolume.toString());
        }
        SortedMap<String, SortedSet<String>> vgs = new TreeMap<>();
        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            String property = Optional.ofNullable(dvu)
                    .map(dvu1 -> dvu1.getVariable())
                    .map(variable -> variable.getGroupe())
                    .map(groupe -> groupe.getNom())
                    .orElse("no group");
            final String vg = String.format(PATTERN_STRING_VARIABLES_SUMMARY,
                    propertiesNomVariable.getProperty(dvu.getVariable().getName(), dvu.getVariable().getName()),
                    getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_GROUP),
                    propertiesNomGroupe.getProperty(property, property));
            reminderPrintStream.println(vg);
            vgs.computeIfAbsent(property, k->new TreeSet<>()).add(dvu.getVariable().getName());
        }
        MDC.put("extract.variables", JSONObject.toJSONString(vgs));
        reminderPrintStream.println();
    }

    protected void printTaxonsSummary(Map<String, Object> requestMetadatasMap, PrintStream reminderPrintStream) {
        List<Taxon> taxons = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_SELECTED_TAXONS));
        for (Taxon taxon : taxons) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, taxon.getNomLatin()));
        }
        String joinTaxons = taxons
                .stream()
                .map(t->t.getNomLatin())
                .collect(Collectors.joining(","));
        reminderPrintStream.println();
        MDC.put("extract.taxon", joinTaxons);
    }

    protected void printComment(PrintStream reminderPrintStream, Map<String, Object> requestMetadatasMap) {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTION_COMMENTS));
        final String comment = (String) requestMetadatasMap.get(KEYMAP_COMMENTS);
        reminderPrintStream.println(String.format(PATTERN_STRING_COMMENTS_SUMMARY, comment));
        MDC.put("extract.comment", comment);
    }

}

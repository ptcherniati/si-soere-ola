/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.outilsmesures;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure.ITypeOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure.TypeOutilsMesure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<OutilsMesure> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_ERROR_CODE_TYPE_OUTILS_MESURE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TYPE_OUTILS_MESURE_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    /**
     *
     */
    protected ITypeOutilsMesureDAO typeOutilsMesureDAO;
    private String[] namesTypesOutilsMesurePossibles;

    private Properties propertiesNomFR;
    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {

            ErrorsReport errorsReport = new ErrorsReport();

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, OutilsMesure.TABLE_NAME);

                // On parcourt chaque colonne d'une ligne
                String typeOutilsMesureCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String nom = tokenizerValues.nextToken();
                String fabriquant = tokenizerValues.nextToken();
                String modele = tokenizerValues.nextToken();
                String numSerie = tokenizerValues.nextToken();
                String etalonnage = tokenizerValues.nextToken();
                String description = tokenizerValues.nextToken();
                
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                persistOutilsMesure(errorsReport, typeOutilsMesureCode, nom, fabriquant, modele, numSerie, etalonnage, description, codeSandre, contexte);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                outilsMesureDAO.remove(outilsMesureDAO.getByCode(code)
                .orElseThrow(()->new PersistenceException("can't find outilsMesure")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistOutilsMesure(ErrorsReport errorsReport, String typeOutilsMesureCode, String nom, String fabriquant, String modele, String numSerie, String etalonnage, String description, String codeSandre, String contexte) throws PersistenceException {

        TypeOutilsMesure dbTypeOutilsMesure = retrieveDBTypeOutilsMesure(errorsReport, typeOutilsMesureCode);

        if (dbTypeOutilsMesure != null) {
            OutilsMesure dbOutilsMesure = outilsMesureDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);
            createOrUpdateOutilsMesure(nom, fabriquant, modele, numSerie, etalonnage, description, dbTypeOutilsMesure, dbOutilsMesure, codeSandre, contexte);
        }

        // Enregistre un site uniquement s'il n'existe pas en BD ou bien
        // s'il est considéré comme une mise à jour

    }

    private void createOrUpdateOutilsMesure(String nom, String fabriquant, String modele, String numSerie, String etalonnage, String description, TypeOutilsMesure dbTypeOutilsMesure, OutilsMesure dbOutilsMesure, String codeSandre, String contexte) {
        if (dbOutilsMesure == null) {
            createOutilsMesure(nom, fabriquant, modele, numSerie, etalonnage, description, dbTypeOutilsMesure, codeSandre, contexte);

        } else {
            updateOutilsMesure(fabriquant, modele, numSerie, etalonnage, description, dbTypeOutilsMesure, dbOutilsMesure, codeSandre, contexte);

        }
    }

    private void updateOutilsMesure(String fabriquant, String modele, String numSerie, String etalonnage, String description, TypeOutilsMesure dbTypeOutilsMesure, OutilsMesure dbOutilsMesure, String codeSandre, String contexte) {
        dbOutilsMesure.setDescription(description);
        dbOutilsMesure.setEtalonnage(etalonnage);
        dbOutilsMesure.setFabriquant(fabriquant);
        dbOutilsMesure.setNumeroSerie(numSerie);
        dbOutilsMesure.setModele(modele);
        
        dbOutilsMesure.setCodeSandre(codeSandre);
        dbOutilsMesure.setContexte(contexte);
        
        dbOutilsMesure.setTypeOutilsMesure(dbTypeOutilsMesure);

        dbTypeOutilsMesure.getOutilsMesures().add(dbOutilsMesure);
    }

    private void createOutilsMesure(String nom, String fabriquant, String modele, String numSerie, String etalonnage, String description, TypeOutilsMesure dbTypeOutilsMesure, String codeSandre, String contexte) {
        OutilsMesure outilsMesure = new OutilsMesure(nom, description, modele, fabriquant, numSerie, etalonnage);
        
        outilsMesure.setCodeSandre(codeSandre);
        outilsMesure.setContexte(contexte);
        
        outilsMesure.setTypeOutilsMesure(dbTypeOutilsMesure);

        dbTypeOutilsMesure.getOutilsMesures().add(outilsMesure);
    }

    private TypeOutilsMesure retrieveDBTypeOutilsMesure(ErrorsReport errorsReport, String typeOutilsMesureCode) {
        TypeOutilsMesure typeOutilsMesure = typeOutilsMesureDAO.getByCode(typeOutilsMesureCode).orElse(null);
        if (typeOutilsMesure == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TYPE_OUTILS_MESURE_NOT_FOUND_IN_DB), typeOutilsMesureCode));
        }
        return typeOutilsMesure;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param typeOutilsMesureDAO
     */
    public void setTypeOutilsMesureDAO(ITypeOutilsMesureDAO typeOutilsMesureDAO) {
        this.typeOutilsMesureDAO = typeOutilsMesureDAO;
    }

    private void updateNamesTypesOutilsMesuresPossibles()  {
        List<TypeOutilsMesure> typesOutilsMesures = typeOutilsMesureDAO.getAll(TypeOutilsMesure.class);
        String[] namesTypesOutilsMesurePossibles = new String[typesOutilsMesures.size()];
        int index = 0;
        for (TypeOutilsMesure typesOutilsMesure : typesOutilsMesures) {
            namesTypesOutilsMesurePossibles[index++] = typesOutilsMesure.getNom();
        }
        this.namesTypesOutilsMesurePossibles = namesTypesOutilsMesurePossibles;
    }

    /**
     *
     * @param outilsMesure
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(OutilsMesure outilsMesure) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getTypeOutilsMesure().getNom(), namesTypesOutilsMesurePossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : propertiesNomFR.get(outilsMesure.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : propertiesNomEN.get(outilsMesure.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getFabriquant(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getModele(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getNumeroSerie(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getEtalonnage(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : propertiesDescriptionEN.get(outilsMesure.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    @Override
    protected List<OutilsMesure> getAllElements(){
        return outilsMesureDAO.getAllBy(OutilsMesure.class, OutilsMesure::getCode);
    }

    @Override
    protected ModelGridMetadata<OutilsMesure> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom", Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom", Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "description", Locale.ENGLISH);
        updateNamesTypesOutilsMesuresPossibles();
        return super.initModelGridMetadata();
    }

}

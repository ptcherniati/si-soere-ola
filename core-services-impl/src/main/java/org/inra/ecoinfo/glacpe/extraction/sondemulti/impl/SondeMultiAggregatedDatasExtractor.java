package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    private static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "sondeMultiAggregatedDatas";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        List<IntervalDate> intervalDates = getIntervalsDates(requestMetadatas);
        List<ValeurMesureSondeMulti> valeursMesuresSondeMulti
                = sondeMultiDAO.extractValueDatas(policyManager.getCurrentUser(), selectedPlateformes, intervalDates, depthRequestParamVO, dvus);

        extractedDatasMap.put(MAP_INDEX_0, valeursMesuresSondeMulti);

        if (valeursMesuresSondeMulti == null || valeursMesuresSondeMulti.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        return extractedDatasMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        ///
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        ((SondeMultiParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }
}

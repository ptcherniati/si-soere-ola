package org.inra.ecoinfo.glacpe.extraction.chimie;

import com.google.common.base.Strings;
import java.io.File;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.function.Function;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.AggregatedData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.extraction.chimie.messages";


    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED";
    private static final String MAP_INDEX_0 = "0";

    private static final String CODE_DATATYPE_CHIMIE = "physico_chimie";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChimieAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> selectedVariables = getDVUs(requestMetadatas);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA)));
        for (DatatypeVariableUniteGLACPE dvu : selectedVariables) {
            String uniteNom = "nounit";
            Unite unite = dvu.getUnite();
            if (unite != null) {
                uniteNom = unite.getCode();
            }

            String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getName());
            if (Strings.isNullOrEmpty(localizedVariableName)) {
                localizedVariableName = dvu.getVariable().getName();
            }

            if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";\"minimum(%s)%n%s\"", localizedVariableName, uniteNom));
            }
            if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";\"maximum(%s)%n%s\"", localizedVariableName, uniteNom));
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures);
        for (Iterator<Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>>> iterator = variablesAggregatedDatas.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> mapBySite = projetEntry.getValue();
            for (Iterator<Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> mapByPlateforme = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                for (Iterator<Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> plateformeEntrye = iterator2.next();
                    Plateforme plateforme = plateformeEntrye.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapByDate = plateformeEntrye.getValue();

                    for (DatatypeVariableUniteGLACPE dvu : dvus) {
                        final VariableGLACPE variable = dvu.getVariable();

                        if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                            outPutForVariable(rawDataPrintStream, mapByDate, variablesAggregatedDatas, projet, site, plateforme, dvu, propertiesOutilName, outputPrintStreamMap, localizedProjetName, localizedSiteName, localizedPlateformeName, depthMin, depthMax, dvus, minSeparator, maxSeparator, VariableAggregatedDatas::getMinAggregatedData, true);
                        }

                        if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                            outPutForVariable(rawDataPrintStream, mapByDate, variablesAggregatedDatas, projet, site, plateforme, dvu, propertiesOutilName, outputPrintStreamMap, localizedProjetName, localizedSiteName, localizedPlateformeName, depthMin, depthMax, dvus, minSeparator, maxSeparator, VariableAggregatedDatas::getMaxAggregatedData, false);
                        }
                    }
                    iterator2.remove();
                }
                iterator1.remove();
            }
            iterator.remove();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void outPutForVariable(PrintStream rawDataPrintStream, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapByDate, SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas, Projet projet, SiteGLACPE site, Plateforme plateform, DatatypeVariableUniteGLACPE dvu, Properties propertiesOutilName, Map<String, PrintStream> outputPrintStreamMap, String localizedProjetName, String localizedSiteName, String localizedPlateformeName, String depthMin, String depthMax, List<DatatypeVariableUniteGLACPE> dvus, String minSeparator, String maxSeparator, final Function<VariableAggregatedDatas, AggregatedData> method, boolean isMin) throws DateTimeException {
        for (Iterator<Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> iterator = mapByDate.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> dateEntry = iterator.next();
            LocalDate date = dateEntry.getKey();
            SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas> mapByVariable = dateEntry.getValue();
            Optional<VariableAggregatedDatas> variableAggregatedDatas = Optional.ofNullable(mapByVariable.getOrDefault(dvu, null)); 
            String localizedOutilPrelevementName = variableAggregatedDatas
                    .filter(vad -> vad.getOutilsPrelevement() != null)
                    .map(vad -> vad.getOutilsPrelevement())
                    .map(o -> propertiesOutilName.getProperty(o.getNom(), ""))
                    .orElse("");
            String localizedOutilMesureName = variableAggregatedDatas
                    .filter(vad -> vad.getOutilsMesure() != null)
                    .map(vad -> vad.getOutilsMesure())
                    .map(o -> propertiesOutilName.getProperty(o.getNom(), ""))
                    .orElse("");
            final List<ValueAggregatedData> valuesAggregated = variableAggregatedDatas
                    .map(method)
                    .map(vag -> vag.getValuesAggregatedDatasByDatesMap())
                    .map(values -> values.get(date))
                    .orElse(new LinkedList<>());
            if (!valuesAggregated.isEmpty()) {

                for (ValueAggregatedData value : valuesAggregated) {
                    rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                    rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s;%s;", DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY), localizedOutilPrelevementName, localizedOutilMesureName, depthMin, depthMax,
                            value.getDepth() == null ? "" : value.getDepth()));
                    for (Iterator<Map.Entry<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> iterator1 = mapByVariable.entrySet().iterator(); iterator1.hasNext();) {
                        if (Utils.createCodeFromString(variableAggregatedDatas.get().getVariableName()).equals(dvu.getVariable().getCode())) {
                            rawDataPrintStream.print(String.format("%s%s%s", isMin ? value.getValue() : minSeparator, isMin ? minSeparator : value.getValue(), maxSeparator));
                            break;
                        } else {
                            rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                        }
                        iterator1.remove();
                            
                    }
                    rawDataPrintStream.println();
                }
            }
            iterator.remove();
        }
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.MesureChloro;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.ValeurMesureChloro;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChloroRawsDatasOutputBuilderByRow extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_CHLORO = "org.inra.ecoinfo.glacpe.dataset.chloro.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_CHLOROPHYLLE = "chlorophylle";
    private static final String SUFFIX_FILENAME = CODE_CHLOROPHYLLE;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesUniteName = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.CHLOROPHYLLE);

        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatas.get(DepthRequestParamVO.class.getSimpleName());

        StringBuilder stringBuilder = new StringBuilder();
        if (depthRequestParamVO.getAllDepth()) {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA_ALLDEPTH)));
        } else {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA)));
        }

        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            String localizedUniteName = propertiesUniteName.getProperty(dvu.getUnite().getCode(), dvu.getUnite().getCode());
            String localizedVariableName = propertiesUniteName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
            stringBuilder.append(String.format(";%s (%s)", localizedVariableName, localizedUniteName));
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvUs = getDVUs(requestMetadatasMap, PPChloroTranspParameter.CHLOROPHYLLE);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        final List<MesureChloro> mesures = (List<MesureChloro>) resultsDatasMap.get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Comparator<MesureChloro> comparator = (m1, m2) -> {
            if (m1.getSousSequenceChloro().getSequenceChloro().getDate().compareTo(m2.getSousSequenceChloro().getSequenceChloro().getDate()) != 0) {
                return m1.getSousSequenceChloro().getSequenceChloro().getDate().compareTo(m2.getSousSequenceChloro().getSequenceChloro().getDate());
            }
            if (m1.getSousSequenceChloro().getPlateforme().compareTo(m2.getSousSequenceChloro().getPlateforme()) != 0) {
                return m1.getSousSequenceChloro().getPlateforme().compareTo(m2.getSousSequenceChloro().getPlateforme());
            }
            if (m1.getProfondeur_max() == null) {
                return m1.getId().compareTo(m2.getId());
            }
            if (m2.getProfondeur_max() != null && m1.getProfondeur_max().compareTo(m2.getProfondeur_max()) != 0) {
                return m1.getProfondeur_max().compareTo(m2.getProfondeur_max());
            }
            return m1.getId().compareTo(m2.getId());
        };
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<MesureChloro>>> mesuresByProjet = mesures
                .stream()
                .collect(
                        Collectors.groupingBy(
                                m -> LacsUtils.getProjetFromDataset(m.getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset()),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> LacsUtils.getSiteFromDataset(m.getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset()),
                                        TreeMap::new,
                                        Collectors.toCollection(
                                                () -> new TreeSet<>(comparator)
                                        )
                                )
                        )
                );
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        String line ="";
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> entry : selectedPlateformes.entrySet()) {
            Projet projet = entry.getKey();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getCode());
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = entry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> plateformeEntry : mapBySite.entrySet()) {
                SiteGLACPE site = plateformeEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getCode());
                SortedSet<Plateforme> platforms = plateformeEntry.getValue();
                final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME));
                rawDataPrintStream.println(headers);
                for (Iterator<MesureChloro> iterator = mesuresByProjet.get(projet).get(site).iterator(); iterator.hasNext();) {
                    MesureChloro mesureChloro = iterator.next();
                    String date = DateUtil.getUTCDateTextFromLocalDateTime(mesureChloro.getSousSequenceChloro().getSequenceChloro().getDate(), DateUtil.DD_MM_YYYY);
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureChloro.getSousSequenceChloro().getPlateforme().getCode(), mesureChloro.getSousSequenceChloro().getPlateforme().getName());
                    if (depthRequestParamVO.getAllDepth()) {
                        line = String.format("%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, date,
                                mesureChloro.getProfondeur_min(), mesureChloro.getProfondeur_max());
                    } else {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax(),
                               date, mesureChloro.getProfondeur_min(), mesureChloro.getProfondeur_max());
                    }
                    rawDataPrintStream.print(line);

                    Map<Long, ValeurMesureChloro> valeursMesuresChloro = buildValeurs(mesureChloro.getValeurs());

                    for (DatatypeVariableUniteGLACPE dvu : dvUs) {
                        ValeurMesureChloro valeurMesureChloro = valeursMesuresChloro.get(dvu.getVariable().getId());
                        rawDataPrintStream.print(";");
                        if (valeurMesureChloro != null && valeurMesureChloro.getValeur() != null) {
                            rawDataPrintStream.print(String.format(getLocalizationManager().getUserLocale().getLocale(), "%.3f", valeurMesureChloro.getValeur()));
                        } else {
                            rawDataPrintStream.print("");
                        }
                    }
                    rawDataPrintStream.println();
                    // commenter ?
                    //mesuresChloro.remove(); 
                }

                closeStreams(outputPrintStreamMap);
            }
        }
        return filesMap;
    }

    private Map<Long, ValeurMesureChloro> buildValeurs(List<ValeurMesureChloro> valeurs) {
        return valeurs.stream()
                .collect(Collectors.toMap(
                        valeur -> ((DatatypeVariableUniteGLACPE) valeur.getRealNode().getNodeable()).getVariable().getId(),
                        valeur -> valeur
                ));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

}

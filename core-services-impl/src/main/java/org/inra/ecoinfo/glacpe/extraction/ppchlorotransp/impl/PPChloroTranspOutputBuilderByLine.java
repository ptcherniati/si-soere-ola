package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPChloroTranspOutputBuilderByLine extends PPChloroTranspOutputsBuildersResolver {

    private IOutputBuilder ppOutputBuilderByLine;
    private IOutputBuilder chloroOutputBuilderByLine;
    private IOutputBuilder transpOutputBuilderByLine;

    /**
     *
     */
    public PPChloroTranspOutputBuilderByLine() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException, NoExtractionResultException {
        super.setPpRawsDatasOutputBuilder(ppOutputBuilderByLine);
        super.setChloroRawsDatasOutputBuilder(chloroOutputBuilderByLine);
        super.setTranspRawsDatasOutputBuilder(transpOutputBuilderByLine);
        return super.buildOutput(parameters);
    }

    /**
     *
     * @param ppOutputBuilderByLine
     */
    public void setPpOutputBuilderByLine(IOutputBuilder ppOutputBuilderByLine) {
        this.ppOutputBuilderByLine = ppOutputBuilderByLine;
    }

    /**
     *
     * @param chloroOutputBuilderByLine
     */
    public void setChloroOutputBuilderByLine(IOutputBuilder chloroOutputBuilderByLine) {
        this.chloroOutputBuilderByLine = chloroOutputBuilderByLine;
    }

    /**
     *
     * @param transpOutputBuilderByLine
     */
    public void setTranspOutputBuilderByLine(IOutputBuilder transpOutputBuilderByLine) {
        this.transpOutputBuilderByLine = transpOutputBuilderByLine;
    }

}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChloroAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {


    private static final String BUNDLE_SOURCE_PATH_CHLORO = "org.inra.ecoinfo.glacpe.dataset.chloro.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String CODE_DATATYPE_CHLOROPHYLLE = "chlorophylle";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String SUFFIX_FILENAME_CHLORO_AGGREGATED = SUFFIX_FILENAME_AGGREGATED.concat(SEPARATOR_TEXT).concat(CODE_DATATYPE_CHLOROPHYLLE);
    /**
     *
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new LinkedList<T>(c);
        Collections.sort(list);
        return list;
        
    }

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Variable.NAME_ENTITY_JPA, "nom");
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.CHLOROPHYLLE);

        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA)));
        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            VariableGLACPE variable = dvu.getVariable();
            String uniteNom;
            Unite unite = dvu.getUnite();
            String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
            if (unite == null) {
                uniteNom = "nounit";
            } else {
                uniteNom = unite.getCode();
            }

            if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";minimum(%s) (%s)", localizedVariableName, uniteNom));
            }
            if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";maximum(%s) (%s)", localizedVariableName, uniteNom));
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);
        List<DatatypeVariableUniteGLACPE> selectedVariables = getDVUs(requestMetadatasMap, ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, CODE_DATATYPE_CHLOROPHYLLE);
        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures);
        for (Iterator<Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>>> iterator = variablesAggregatedDatas.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> mapBySite = projetEntry.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            for (Iterator<Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> mapByPlateforme = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                for (Iterator<Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> platformEntry = iterator2.next();
                    Plateforme plateforme = platformEntry.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapByDate = platformEntry.getValue();
                    for (Iterator<Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> iterator3 = mapByDate.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> dateEntry = iterator3.next();
                        LocalDate date = dateEntry.getKey();
                        SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas> aggregatedByVariable = dateEntry.getValue();

                        if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                                VariableAggregatedDatas variableAggregatedDatas = aggregatedByVariable.get(dvu);
                                if (variableAggregatedDatas != null
                                        && variableAggregatedDatas.getMinAggregatedData() != null
                                        && variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap() != null
                                        && variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().containsKey(date)
                                        && variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(date) != null) {
                                    for (ValueAggregatedData value : variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(date)) {
                                        rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                        rawDataPrintStream.print(String.format("%s;%s;%s;%s;", DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY), depthMin, depthMax,
                                                value.getDepth() == null ? "" : value.getDepth()));
                                        for (DatatypeVariableUniteGLACPE dvu1 : dvus) {
                                            if (dvu1.equals(dvu)) {
                                                rawDataPrintStream.print(String.format("%s%s%s", value.getValue(), minSeparator, maxSeparator));
                                                break;
                                            } else {
                                                rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                            }
                                        }
                                        rawDataPrintStream.println();
                                    }
                                }
                            }
                        }

                        if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                                VariableAggregatedDatas variableAggregatedDatas = aggregatedByVariable.get(dvu);
                                if (variableAggregatedDatas != null
                                        && variableAggregatedDatas.getMaxAggregatedData() != null
                                        && variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap() != null
                                        && variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().containsKey(date)
                                        && variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(date) != null) {

                                    for (ValueAggregatedData value : variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(date)) {
                                        rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                        rawDataPrintStream.print(String.format("%s;%s;%s;%s;", DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY), depthMin, depthMax,
                                                value.getDepth() == null ? "" : value.getDepth()));

                                        for (DatatypeVariableUniteGLACPE dvu1 : dvus) {
                                            if (dvu1.equals(dvu)) {
                                                rawDataPrintStream.print(String.format("%s%s%s", minSeparator, value.getValue(), maxSeparator));
                                                break;
                                            } else {
                                                rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                            }
                                        }
                                        rawDataPrintStream.println();
                                    }
                                }
                            }
                        }
                        iterator3.remove();
                    }
                    iterator2.remove();
                }
                iterator1.remove();
            }
            iterator.remove();
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }
    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

}

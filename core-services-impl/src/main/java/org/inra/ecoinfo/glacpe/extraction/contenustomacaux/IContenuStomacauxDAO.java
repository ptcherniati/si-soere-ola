package org.inra.ecoinfo.glacpe.extraction.contenustomacaux;

import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.MesureContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.ValeurMesureContenuStomacaux;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public interface IContenuStomacauxDAO extends IDAO<Object> {

    /**
     *
     * @param user
     * @return
     */
    Collection<PlateformeProjetVO> getAvailablePlatforms(IUser user);

    /**
     *
     * @param currentUser
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    List<DatatypeVariableUniteGLACPE> getAvailableVariables(IUser currentUser, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates);

    /**
     *
     * @param currentUser
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    Collection<Float> getAvailableDepths(IUser currentUser, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables);

    /**
     *
     * @param currentUser
     * @param selectedPlateformeProjetVOs
     * @param datesRequestParamVO
     * @param dvus
     * @return
     */
    List<MesureContenuStomacaux> extractDatas(IUser currentUser, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformeProjetVOs, List<IntervalDate> datesRequestParamVO, List<DatatypeVariableUniteGLACPE> dvus);

    /**
     *
     * @param currentUser
     * @param selectedPlateformeProjetVOs
     * @param datesRequestParamVO
     * @param dvus
     * @return
     */
    List<ValeurMesureContenuStomacaux> extractValueDatas(IUser currentUser, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformeProjetVOs, List<IntervalDate> datesRequestParamVO, List<DatatypeVariableUniteGLACPE> dvus);

}
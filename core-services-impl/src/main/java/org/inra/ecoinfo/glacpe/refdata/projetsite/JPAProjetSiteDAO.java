/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet_;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE_;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPAProjetSiteDAO extends AbstractJPADAO<ProjetSite> implements IProjetSiteDAO {

    /**
     *
     * @param siteCode
     * @param projetCode
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Optional<ProjetSite> getBySiteCodeAndProjetCode(String siteCode, String projetCode) {
        CriteriaQuery<ProjetSite> query = builder.createQuery(ProjetSite.class);
        Root<ProjetSite> projetSite = query.from(ProjetSite.class);
        Join<ProjetSite, SiteGLACPE> site = projetSite.join(ProjetSite_.site);
        Join<ProjetSite, Projet> project = projetSite.join(ProjetSite_.projet);
        query
                .select(projetSite)
                .where(
                        builder.and(
                                builder.equal(site.get(SiteGLACPE_.code), siteCode),
                                builder.equal(project.get(Projet_.code), projetCode)
                        )
                );
        return getOptional(query);
    }

}

package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class DefaultSondeMultiparametresExtractor extends MO implements IExtractor {
    // private static final String MSG_EXTRACTION_ABORTED = Messages.getString("PROPERTY_MSG_FAILED_EXTRACT");
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";

    /**
     *
     */
    protected IExtractor sondeMultiRawsDatasExtractor;

    /**
     *
     */
    protected IExtractor sondeMultiAggregatedDatasExtractor;


    @Override
    public void setExtraction(Extraction extraction) {

    }

    private void addOutputBuilderByCondition(Boolean condition, IExtractor extractor, List<IExtractor> extractors) {
        if (condition) {
            extractors.add(extractor);
        }
    }

    /**
     *
     * @param metadatasMap
     * @return
     */
    public List<IExtractor> resolveExtractors(Map<String, Object> metadatasMap) {
        List<IExtractor> extractors = new LinkedList<IExtractor>();
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(metadatasMap);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData(), sondeMultiRawsDatasExtractor, extractors);
        addOutputBuilderByCondition(datasRequestParamVO.getDataBalancedByDepth()|| datasRequestParamVO.getTargetedValuesAndDepths()|| datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth(), sondeMultiAggregatedDatasExtractor, extractors);
        return extractors;
    }

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        for (IExtractor extractor : resolveExtractors(parameters.getParameters())) {
            try {
                extractor.extract(parameters);
            } catch (NoExtractionResultException e) {
                throw new BusinessException(String.format(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT), SondeMultiParameters.CODE_EXTRACTIONTYPE_SONDE_MULTIPARAMETRES), e);
            } catch (BusinessException e) {
                throw e;
            }
        }
    }

    /**
     *
     * @param sondeMultiRawsDatasExtractor
     */
    public void setSondeMultiRawsDatasExtractor(IExtractor sondeMultiRawsDatasExtractor) {
        this.sondeMultiRawsDatasExtractor = sondeMultiRawsDatasExtractor;
    }

    /**
     *
     * @param sondeMultiAggregatedDatasExtractor
     */
    public void setSondeMultiAggregatedDatasExtractor(IExtractor sondeMultiAggregatedDatasExtractor) {
        this.sondeMultiAggregatedDatasExtractor = sondeMultiAggregatedDatasExtractor;
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        // TODO Auto-generated method stub
        return 0;
    }

}

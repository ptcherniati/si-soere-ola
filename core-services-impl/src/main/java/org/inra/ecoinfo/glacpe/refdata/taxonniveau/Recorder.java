/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.taxonniveau;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<TaxonNiveau> {

    /**
     *
     */
    protected ITaxonNiveauDAO taxonNiveauDAO;
    private Properties propertiesNomFR;
    private Properties propertiesNomEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, TaxonNiveau.TABLE_NAME);
                lineNumber++;

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();

                // afiocca
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                TaxonNiveau dbTaxonNiveau = taxonNiveauDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);
                TaxonNiveau taxonNiveau = new TaxonNiveau(nom);

                // Enregistre un taxonNiveau uniquement s'il n'existe pas en BD
                // ou bien
                // s'il est considéré comme une mise à jour
                if (dbTaxonNiveau == null) {

                    taxonNiveau.setCodeSandre(codeSandre);
                    taxonNiveau.setContexte(contexte);
                    taxonNiveauDAO.saveOrUpdate(taxonNiveau);
                } else {

                    dbTaxonNiveau.setCodeSandre(codeSandre);
                    dbTaxonNiveau.setContexte(contexte);
                    taxonNiveauDAO.saveOrUpdate(dbTaxonNiveau);
                }
            }
            AbstractCSVMetadataRecorder.LOGGER.debug(String.format("%d records has been updatedb in DB...", lineNumber));
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                taxonNiveauDAO.remove(taxonNiveauDAO.getByCode(code)
                        .orElseThrow(() -> new PersistenceException("can't retrieve taxonniveau")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param taxonNiveauDAO
     */
    public void setTaxonNiveauDAO(ITaxonNiveauDAO taxonNiveauDAO) {
        this.taxonNiveauDAO = taxonNiveauDAO;
    }

    /**
     *
     * @param taxonNiveau
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TaxonNiveau taxonNiveau) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxonNiveau == null ? EMPTY_STRING : taxonNiveau.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxonNiveau == null ? EMPTY_STRING : propertiesNomFR.get(taxonNiveau.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxonNiveau == null ? EMPTY_STRING : propertiesNomEN.get(taxonNiveau.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        //afiocca
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxonNiveau == null ? EMPTY_STRING : taxonNiveau.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxonNiveau == null ? EMPTY_STRING : taxonNiveau.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<TaxonNiveau> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(TaxonNiveau.TABLE_NAME, "nom", Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(TaxonNiveau.TABLE_NAME, "nom", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<TaxonNiveau> getAllElements() {
        return taxonNiveauDAO.getAllBy(TaxonNiveau.class, TaxonNiveau::getCode);
    }

}

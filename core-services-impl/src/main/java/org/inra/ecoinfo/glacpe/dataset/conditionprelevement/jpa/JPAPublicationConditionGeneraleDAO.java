package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationConditionGeneraleDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_MESURE_CONDITION_BY_IDS = "delete from MesureConditionGenerale mce where mce.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_CONDITION_BY_IDS = "delete from ValeurConditionGenerale vmce where mconditionprelevement_id in (select id from MesureConditionGenerale mce where mce.versionFile = :version)";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_CONDITION_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_CONDITION_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage(), e);
            throw new PersistenceException(e);
        }
    }
}

package org.inra.ecoinfo.glacpe.dataset.conditionprelevement;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;

/**
 *
 * @author ptcherniati
 */
public interface IMesureConditionGeneraleDAO extends IDAO<MesureConditionGenerale> {

    /**
     *
     * @param datePrelevement
     * @param plateformeCode
     * @param projetCode
     * @return
     */
    Optional<MesureConditionGenerale> getByDatePrelevementPlateformeCodeAndProjetCode(LocalDate datePrelevement, String plateformeCode, String projetCode);

    /**
     *
     * @param variableId
     * @param mesureId
     * @return
     */
    Optional<ValeurConditionGenerale> getByVariableIdAndMesureId(Long variableId, Long mesureId);

    void flush();

}

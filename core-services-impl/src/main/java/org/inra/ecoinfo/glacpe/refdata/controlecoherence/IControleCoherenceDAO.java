package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;

/**
 *
 * @author ptcherniati
 */
public interface IControleCoherenceDAO extends IDAO<ControleCoherence> {

    /**
     *
     * @param siteCode
     * @param variableCode
     * @return
     */
    Optional<ControleCoherence> getBySiteCodeAndVariableCode(String siteCode, String variableCode);

    /**
     *
     * @param site
     * @param datatypeVariableUnite
     * @return
     */
    Optional<ControleCoherence> getBySiteAndDatatypeVariableUnite(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite);

    /**
     *
     * @param site
     * @param datatypeCode
     * @return
     */
    Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String site, String datatypeCode);

    /**
     *
     * @param datatypeVariableUnite
     * @return
     */
    Optional<ControleCoherence> getByNullSiteAndDatatypeVariableUnite(DatatypeVariableUniteGLACPE datatypeVariableUnite);

    /**
     *
     * @param datatypeCode
     * @return
     */
    Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String datatypeCode);
}

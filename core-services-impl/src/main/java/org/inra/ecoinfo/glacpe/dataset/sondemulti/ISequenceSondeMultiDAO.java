package org.inra.ecoinfo.glacpe.dataset.sondemulti;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceSondeMultiDAO extends IDAO<SequenceSondeMulti> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     */
    Optional<SequenceSondeMulti> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode);

    void flush();

}

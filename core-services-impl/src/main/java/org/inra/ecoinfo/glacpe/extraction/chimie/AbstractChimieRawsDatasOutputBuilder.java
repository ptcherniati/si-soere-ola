package org.inra.ecoinfo.glacpe.extraction.chimie;


import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.chimie.jpa.JPAValeurMesureChimieDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractChimieRawsDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.extraction.chimie.messages";
    protected static final String CODE_DATATYPE_PHYSICO_CHIMIE = "physico_chimie";
    protected static final String MSG_BALANCE_IONIQUE_KEY_NOT_FOUND_IN_DB = "PROPERTY_MSG_BALANCE_IONIQUE_KEY_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /** The date formatter @link(DateFormat). */
    protected static String dateFormatter = DateUtil.DD_MM_YYYY;
    protected static final String SUFFIX_RAW_DATA = "raw";

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected JPAValeurMesureChimieDAO valeurMesureChimieDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param valeurMesureChimieDAO
     */
    public void setValeurMesureChimieDAO(JPAValeurMesureChimieDAO valeurMesureChimieDAO) {
        this.valeurMesureChimieDAO = valeurMesureChimieDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IDatatypeVariableUniteGLACPEDAO extends IDAO<DatatypeVariableUniteGLACPE> {

    /**
     * Gets the by datatype.
     *
     * @param datatypeCode the datatype code
     * @return the by datatype
     * @link(String) the datatype code
     */
    List<DatatypeVariableUniteGLACPE> getByDatatype(String datatypeCode);

    /**
     * Gets the by n key.
     *
     * @param datatypeCode the datatype code
     * @param variableCode the variable code
     * @param uniteCode the unite code
     * @return the by n key
     * @link(String) the datatype code
     * @link(String) the variable code
     * @link(String) the unite code
     */
    Optional<DatatypeVariableUniteGLACPE> getByNKey(String datatypeCode, String variableCode, String uniteCode);

    /**
     * Gets the unite.
     *
     * @param datatype the datatype
     * @param variable the variable
     * @return the unite
     * @link(String) the datatype
     * @link(String) the variable
     */
    Optional<Unite> getUnite(String datatype, String variable);

    /**
     *
     * @param realNode
     * @return
     */
    RealNode mergeRealNode(RealNode realNode);

    /**
     * For a deposit node realnode, return all the children realNode variables
     * @param realNode
     * @return 
     */
    Map<Variable, RealNode> getRealNodesVariables(RealNode realNode);

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @return
     */
    Optional<DatatypeVariableUniteGLACPE> getByDatatypeAndVariable(String datatypeCode, String variableCode);

    /**
     *
     * @return
     */
    List<DatatypeVariableUniteGLACPE> getAllDatatypesVariablesUnitesGLACPE();

    /**
     *
     * @param variableCode
     * @return
     * @throws PersistenceException
     */
    List<DatatypeVariableUniteGLACPE> getByVariable(String variableCode) throws PersistenceException;

    /**
     *
     * @return
     */
    public List<String> getPathes();

    /**
     *
     * @param datatypeName
     * @return
     */
    public Map<String, DatatypeVariableUniteGLACPE> getAllVariableTypeDonneesByDataTypeMapByVariableCode(String datatypeName);

    /**
     *
     * @param code
     * @return
     */
    public Map<String, Map<String, Map<String, List<RealNode>>>> getRealNodesFromDatatypeCode(final String code);
}

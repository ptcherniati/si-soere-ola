package org.inra.ecoinfo.glacpe.dataset.phytoplancton.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationPhytoplanctonDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_PHYTO_BY_IDS = "delete from SequencePhytoplancton sce where sce.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_PHYTO_BY_IDS = "delete from SousSequencePhytoplancton ssce where sphytoplancton_id in (select id from SequencePhytoplancton sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_PHYTO_BY_IDS = "delete from MesurePhytoplancton mce where ssphytoplancton_id in (select id from SousSequencePhytoplancton ssce where sphytoplancton_id in (select id from SequencePhytoplancton sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_PHYTO_BY_IDS = "delete from ValeurMesurePhytoplancton vmce where mphytoplancton_id in (select id from MesurePhytoplancton mce where ssphytoplancton_id in (select id from SousSequencePhytoplancton ssce where sphytoplancton_id in (select id from SequencePhytoplancton sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_PHYTO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_PHYTO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_PHYTO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_PHYTO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage(), e);
            throw new PersistenceException(e);
        }
    }
}

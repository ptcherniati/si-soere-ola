package org.inra.ecoinfo.glacpe.dataset.phytoplancton;

import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public interface IPhytoplanctonDAO extends IDAO<Object> { 

    /**
     *
     * @param user
     * @return
     */
    Collection<PlateformeProjetVO> getAvailablePlatforms(IUser user);

    /**
     *
     * @param currentUser
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    List<DatatypeVariableUniteGLACPE> getAvailableVariables(IUser currentUser, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates);

    /**
     *
     * @param currentUser
     * @param selectedPlateformes
     * @param datesRequestParamVO
     * @param dvus
     * @return
     */
    List<MesurePhytoplancton> extractDatas(IUser currentUser, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes, List<IntervalDate> datesRequestParamVO, List<DatatypeVariableUniteGLACPE> dvus);

    /**
     *
     * @param currentUser
     * @param selectedPlateformes
     * @param datesRequestParamVO
     * @param dvus
     * @return
     */
    List<ValeurMesurePhytoplancton> extractValueDatas(IUser currentUser, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes, List<IntervalDate> datesRequestParamVO, List<DatatypeVariableUniteGLACPE> dvus);

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    Collection<Taxon> getAvailableTaxons(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables);

    
}
package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String nomSite;
    private String plateformeCode;
    private LocalDate datePrelevement;
    private LocalTime heure;
    private Long originalLineNumber;
    private String projetCode;
    private List<VariableValue> variablesValues;
    private String commentaire;

    /**
     *
     */
    public LineRecord() {

    }

    /**
     *
     * @param nomSite
     * @param plateformeCode
     * @param datePrelevement
     * @param heure
     * @param variablesValues
     * @param originalLineNumber
     * @param string2
     * @param projetCode
     * @param string3
     * @param commentaire
     */
    public LineRecord(String nomSite, String plateformeCode, LocalDate datePrelevement, LocalTime heure, List<VariableValue> variablesValues, Long originalLineNumber, String projetCode, String commentaire) {
        super();
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.datePrelevement = datePrelevement;
        this.heure = heure;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
        this.projetCode = projetCode;
        this.commentaire = commentaire;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.datePrelevement = line.getDatePrelevement();
        this.heure = line.getHeure();
        this.variablesValues = line.getVariablesValues();

        this.originalLineNumber = line.getOriginalLineNumber();
        this.projetCode = getProjetCode();
        this.commentaire = getCommentaire();
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @param variablesValues
     */
    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
}

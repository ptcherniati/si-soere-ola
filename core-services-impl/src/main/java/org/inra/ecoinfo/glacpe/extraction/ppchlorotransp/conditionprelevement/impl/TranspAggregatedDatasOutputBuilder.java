package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class TranspAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.dataset.messages";
    private static final String BUNDLE_SOURCE_PATH_TRANSP = "org.inra.ecoinfo.glacpe.extraction.conditionprelevement.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String CODE_DATATYPE_TRANSP = "transparence";
    private static final String CODE_DATATYPE_CONDITION_GENERALE = "condition_prelevement";

    /**
     *
     */
    protected static final String END_PREFIX = "end";

    /**
     *
     */
    protected static final String START_PREFIX = "start";

    private static final String HEADER_AGGREGATED_DATA = "PROPERTY_MSG_HEADER_RAW_TRANSP";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    /**
     *
     */
    protected static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    private static final String SUFFIX_FILENAME_TRANSP_AGGREGATED = SUFFIX_FILENAME_AGGREGATED.concat(SEPARATOR_TEXT).concat(CODE_DATATYPE_TRANSP);

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, TranspAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        Properties propertiesNomVariable = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, PPChloroTranspParameter.TRANSPARENCE);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_TRANSP, HEADER_AGGREGATED_DATA)));
        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            String localizedVariableName = propertiesNomVariable.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
            if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";minimum(%s) (%s)", localizedVariableName, dvu.getUnite().getName()));
            }
            if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                stringBuilder.append(String.format(";maximum(%s) (%s)", localizedVariableName, dvu.getUnite().getName()));
            }
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, PPChloroTranspParameter.TRANSPARENCE);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>>>>>> valeursMesures = buildAggregatedData(resultsDatasMap.get(TranspAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);

        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        List<IntervalDate> intervalDates = getIntervalsDates(requestMetadatasMap);
        String line;
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> projetEntry : selectedPlateformes.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = projetEntry.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getCode());
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : mapBySite.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getCode());
                SortedSet<Plateforme> platforms = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                for (Plateforme platform : platforms) {
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(platform.getCode(), platform.getName());
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>>> valeursBydDates = valeursMesures
                            .getOrDefault(projet, new TreeMap<>())
                            .getOrDefault(site, new TreeMap<>())
                            .getOrDefault(platform, new TreeMap<>());
                    for (Iterator<Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>>>> iterator = valeursBydDates.entrySet().iterator(); iterator.hasNext();) {
                        Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>>> entryByDate = iterator.next();
                        LocalDate localDate = entryByDate.getKey();
                        String date = DateUtil.getUTCDateTextFromLocalDateTime(localDate, DateUtil.DD_MM_YYYY);
                        SortedMap<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>> mapByVariable = entryByDate.getValue();
                        for (Iterator<Map.Entry<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>>> iterator2 = mapByVariable.entrySet().iterator(); iterator2.hasNext();) {
                            Map.Entry<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>> mapByDvus = iterator2.next();
                            DatatypeVariableUniteGLACPE dvu = mapByDvus.getKey();
                            SortedMap<String, List<ValeurConditionGenerale>> valeurs = mapByDvus.getValue();
                            List<ValeurConditionGenerale> valeursRefMin = null;
                            List<ValeurConditionGenerale> valeursRefMax = null;
                            valeursRefMin = valeurs.get("max");
                            valeursRefMax = valeurs.get("min");
                            printLine(minSeparator, maxSeparator, "min", valeursRefMin, localizedProjetName, localizedSiteName, localizedPlateformeName, date, rawDataPrintStream, dvus, datasRequestParamVO, dvu);
                            printLine(minSeparator, maxSeparator, "max", valeursRefMax, localizedProjetName, localizedSiteName, localizedPlateformeName, date, rawDataPrintStream, dvus, datasRequestParamVO, dvu);
                            iterator2.remove();
                        }
                        iterator.remove();
                    }
                }

            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param minSeparator
     * @param maxSeparator
     * @param minOrMax
     * @param valeurs
     * @param localizedProjetName
     * @param localizedSiteName
     * @param localizedPlateformeName
     * @param date
     * @param rawDataPrintStream
     * @param dvus
     * @param datasRequestParamVO
     * @param dvu0
     * @throws DateTimeException
     */
    public void printLine(String minSeparator, String maxSeparator, String minOrMax, List<ValeurConditionGenerale> valeurs, String localizedProjetName, String localizedSiteName, String localizedPlateformeName, String date, PrintStream rawDataPrintStream, Iterable<DatatypeVariableUniteGLACPE> dvus, DatasRequestParamVO datasRequestParamVO, DatatypeVariableUniteGLACPE dvu0) throws DateTimeException {
        String line;
        if (CollectionUtils.isEmpty(valeurs)) {
            return;
        }
        for (ValeurConditionGenerale valRef : valeurs) {
            if (valRef.getMesure().getHeure() == null) {
                line = String.format("%s;%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName, date);
            } else {
                line = String.format("%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, date,
                        DateUtil.getUTCDateTextFromLocalDateTime(valRef.getMesure().getHeure(), DateUtil.HH_MM_SS));
            }
            rawDataPrintStream.print(line);
            for (DatatypeVariableUniteGLACPE selectedDvu : dvus) {
                if (selectedDvu.equals(dvu0)) {
                    if (datasRequestParamVO.getMinValueAndAssociatedDepth() && "min".equals(minOrMax)) {
                        if (valeurs != null) {
                            for (ValeurConditionGenerale valByDate : valeurs) {
                                if (valByDate.getMesure().getDatePrelevement().equals(date)) {
                                    rawDataPrintStream.print(String.format(";%s", valByDate.getValeur()));
                                }
                            }
                        } else {
                            rawDataPrintStream.print(minSeparator);
                        }
                    }
                    if (datasRequestParamVO.getMaxValueAndAssociatedDepth() && "max".equals(minOrMax)) {
                        if (valeurs != null) {
                            for (ValeurConditionGenerale valByDate : valeurs) {
                                if (valByDate.getMesure().getDatePrelevement().equals(date)) {
                                    rawDataPrintStream.print(String.format(";%s", valByDate.getValeur()));
                                }
                            }
                        } else {
                            rawDataPrintStream.print(maxSeparator);
                        }
                    }
                } else {
                    rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                }
            }
            rawDataPrintStream.println();

        }
    }

    /**
     *
     * @param listValeursMesures
     * @return
     */
    public SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedMap<String, List<ValeurConditionGenerale>>>>>>> buildAggregatedData(List<ValeurConditionGenerale> listValeursMesures) {
        Map<Date, Map<Long, Map<String, List<ValeurConditionGenerale>>>> sortedMapAggregated = new HashMap<>();
        return listValeursMesures.stream()
                .collect(
                        Collectors.groupingBy(
                                v -> v.getMesure().getProjet(),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        v -> v.getMesure().getSite(),
                                        TreeMap::new,
                                        Collectors.groupingBy(
                                                v -> v.getMesure().getPlateforme(),
                                                TreeMap::new,
                                                Collectors.groupingBy(
                                                        v -> v.getMesure().getDatePrelevement(),
                                                        TreeMap::new,
                                                        Collectors.groupingBy(
                                                                v -> (DatatypeVariableUniteGLACPE) v.getRealNode().getNodeable(),
                                                                TreeMap::new,
                                                                Collectors.collectingAndThen(
                                                                        Collectors.toList(),
                                                                        this::buildMinMaxMap
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                );
    }

    protected SortedMap<String, List<ValeurConditionGenerale>> buildMinMaxMap(List<ValeurConditionGenerale> valeurs) {
        SortedMap<String, List<ValeurConditionGenerale>> treeMap = new TreeMap<String, List<ValeurConditionGenerale>>();
        treeMap.put("min", getMinValues(valeurs));
        treeMap.put("max", getMaxValues(valeurs));
        return treeMap;
    }

    private List<ValeurConditionGenerale> getMinValues(List<ValeurConditionGenerale> valeurs) {

        List<ValeurConditionGenerale> minListValues = new ArrayList<>();
        if (valeurs == null || valeurs.isEmpty()) {
            return minListValues;
        }
        ValeurConditionGenerale minValues = getMinValue(valeurs);

        valeurs.stream()
                .filter(val -> val.getValeur() != null)
                .filter((val) -> (val.getValeur().compareTo(minValues.getValeur()) == 0))
                .forEachOrdered((val) -> {
                    minListValues.add(val);
                });

        return minListValues;
    }

    private List<ValeurConditionGenerale> getMaxValues(List<ValeurConditionGenerale> valeurs) {

        List<ValeurConditionGenerale> maxListValues = new ArrayList<>();
        if (valeurs == null || valeurs.isEmpty()) {
            return maxListValues;
        }
        ValeurConditionGenerale maxValues = getMaxValue(valeurs);

        valeurs.stream()
                .filter(val -> val.getValeur() != null)
                .filter((val) -> (val.getValeur().compareTo(maxValues.getValeur()) == 0))
                .forEachOrdered((val) -> {
                    maxListValues.add(val);
                });

        return maxListValues;
    }

    private ValeurConditionGenerale getMinValue(List<ValeurConditionGenerale> valeurs) {
        ValeurConditionGenerale minValue = null;
        for (ValeurConditionGenerale valeur : valeurs) {
            if (valeur.getValeur() != null) {
                if (minValue == null) {
                    minValue = valeur;
                }
                if (minValue.getValeur().compareTo(valeur.getValeur()) > 0) {
                    minValue = valeur;
                }
            }
        }
        return minValue;
    }

    private ValeurConditionGenerale getMaxValue(List<ValeurConditionGenerale> valeurs) {
        ValeurConditionGenerale maxValue = null;
        for (ValeurConditionGenerale valeur : valeurs) {
            if (valeur.getValeur() != null) {
                if (maxValue == null) {
                    maxValue = valeur;
                }
                if (maxValue.getValeur().compareTo(valeur.getValeur()) < 0) {
                    maxValue = valeur;
                }
            }
        }
        return maxValue;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;


import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.datatype.DataType;

/**
 *
 * @author ptcherniati
 */
public class JPAProjetSiteThemeDatatypeDAO extends AbstractJPADAO<RealNode> implements IProjetSiteThemeDatatypeDAO {


    /**
     * @return
     */
    @Override
    public List<RealNode> loadRealDatatypeNodes() {

        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        Join<RealNode, Nodeable> rnNodeable = rn.join(RealNode_.nodeable);
        query
                .select(rn)
                .where(builder.equal(rnNodeable.type(), builder.literal(DataType.class)))
                .orderBy(builder.asc(rn.get(RealNode_.path)));
        return getResultList(query);
    }
    
}

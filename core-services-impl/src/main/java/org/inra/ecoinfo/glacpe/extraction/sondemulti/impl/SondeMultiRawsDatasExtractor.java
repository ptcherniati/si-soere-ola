package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiRawsDatasExtractor extends AbstractSondeMultiRawsDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "sondeMultiRawsDatas";

    

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        ((SondeMultiParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }
}

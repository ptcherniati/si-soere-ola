/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<GroupeVariable> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_MISSING_REFERENCE_DATA_DBGROUPEVARIABLE = "PROPERTY_MSG_MISSING_REFERENCE_DATA_DBGROUPEVARIABLE";
    private static final String MSG_RECCURSIVE_LOOP = "PROPERTY_MSG_RECCURSIVE_LOOP";

    private Properties propertiesNomFR;
    private Properties propertiesNomEN;

    /**
     *
     */
    protected IGroupeVariableDAO groupeVariableDAO;
    String[] namesGroupesVariablesPossibles;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        CSVParser parserBis;
        try {
            parserBis = new CSVParser(new InputStreamReader(new FileInputStream(file), encoding), PROPERTY_CST_CVS_SEPARATOR);
            subProcessRecord(parserBis, false);

            // La deuxième passe permet de mettre à jour les parents
            parserBis = new CSVParser(new InputStreamReader(new FileInputStream(file), encoding), PROPERTY_CST_CVS_SEPARATOR);
            subProcessRecord(parserBis, true);
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        String[] values = null;
        try {
            TokenizerValues tokenizerValues = new TokenizerValues(values);
            String code = Utils.createCodeFromString(tokenizerValues.nextToken());
            groupeVariableDAO.remove(groupeVariableDAO.getByCode(code)
                    .orElseThrow(() -> new PersistenceException("can't retrieve groupeVariable")));
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param parser
     * @param testReferencedGroupes
     * @throws BusinessException
     */
    public void subProcessRecord(CSVParser parser, Boolean testReferencedGroupes) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, GroupeVariable.TABLE_NAME);
                lineNumber++;

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String parentString = tokenizerValues.nextToken();

                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                String parentCode = null;
                if (parentString != null) {
                    parentCode = Utils.createCodeFromString(parentString);
                }
                persistGroupeVariable(errorsReport, nom, parentCode, testReferencedGroupes, codeSandre, contexte);

            }
            if (errorsReport.hasErrors() && testReferencedGroupes) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            AbstractCSVMetadataRecorder.LOGGER.debug(String.format("%d records has been updated in DB...", lineNumber));
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private GroupeVariable retrieveDbGroupeVariable(ErrorsReport errorsReport, String groupeVariableCode) throws PersistenceException {
        GroupeVariable groupeVariable = null;

        if (groupeVariableCode != null && groupeVariableCode.length() > 0) {
            groupeVariable = groupeVariableDAO.getByCode(groupeVariableCode).orElse(null);
            if (groupeVariable == null && groupeVariableCode.length() > 0) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_MISSING_REFERENCE_DATA_DBGROUPEVARIABLE), groupeVariableCode));
            }
        }
        return groupeVariable;
    }

    private void persistGroupeVariable(ErrorsReport errorsReport, String nom, String parentCode, Boolean testReferencedGroupe, String codeSandre, String contexte) throws PersistenceException {
        try {
            GroupeVariable dbGroupeParent = retrieveDbGroupeVariable(errorsReport, parentCode);
            GroupeVariable dbGroupe = groupeVariableDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);

            createOrUpdateGroupeVariable(nom, dbGroupe, dbGroupeParent, errorsReport, codeSandre, contexte);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PersistenceException(e);
        }

    }

    private void createOrUpdateGroupeVariable(String nom, GroupeVariable dbGroupe, GroupeVariable dbGroupeParent, ErrorsReport errorsReport, String codeSandre, String contexte) throws PersistenceException {
        // Enregistre un taxon uniquement s'il n'existe pas en BD ou
        // bien s'il est considéré comme une mise à jour
        if (dbGroupe == null) {
            createGroupeVariable(nom, dbGroupeParent, errorsReport, codeSandre, contexte);
        } else {
            updateGroupeVariable(dbGroupe, dbGroupeParent, errorsReport, codeSandre, contexte);
        }
    }

    //modifications a vérifier 
    private void updateGroupeVariable(GroupeVariable dbGroupeVariable, GroupeVariable dbGroupeParent, ErrorsReport errorsReport, String codeSandre, String contexte) {
        if (dbGroupeParent != null) {
            checkIsLoop(dbGroupeVariable, dbGroupeParent, errorsReport);

            dbGroupeParent.getEnfants().add(dbGroupeVariable);
            dbGroupeVariable.setParent(dbGroupeParent);
        } else {
            if (dbGroupeVariable.getParent() != null) {
                dbGroupeVariable.getParent().getEnfants().remove(dbGroupeVariable);
            }
            dbGroupeVariable.setParent(null);
        }
        dbGroupeVariable.setCodeSandre(codeSandre);
        dbGroupeVariable.setContexte(contexte);

    }

    //modifications a vérifier 
    private void createGroupeVariable(String nom, GroupeVariable dbGroupeParent, ErrorsReport errorsReport, String codeSandre, String contexte) throws PersistenceException {
        GroupeVariable groupeVariable = new GroupeVariable(nom);
        updateGroupeVariable(groupeVariable, dbGroupeParent, errorsReport, codeSandre, contexte);

        groupeVariable.setCodeSandre(codeSandre);
        groupeVariable.setContexte(contexte);
        groupeVariableDAO.saveOrUpdate(groupeVariable);
    }

    private void checkIsLoop(GroupeVariable dbGroupeVariable, GroupeVariable dbGroupeParent, ErrorsReport errorsReport) {
        if (dbGroupeVariable.getNom().equals(dbGroupeParent.getNom())) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_RECCURSIVE_LOOP)));
        } else if (dbGroupeParent.getParent() != null) {
            checkIsLoop(dbGroupeVariable, dbGroupeParent.getParent(), errorsReport);
        }
    }

    /**
     *
     * @param groupeVariableDAO
     */
    public void setGroupeVariableDAO(IGroupeVariableDAO groupeVariableDAO) {
        this.groupeVariableDAO = groupeVariableDAO;
    }

    private void updateNamesGroupesVariablesPossibles() throws PersistenceException {
        List<GroupeVariable> groupesVariables = groupeVariableDAO.getAll(GroupeVariable.class);
        String[] namesGroupesVariablesPossibles = new String[groupesVariables.size() + 1];
        int index = 0;
        namesGroupesVariablesPossibles[index++] = "";
        for (GroupeVariable groupeVariable : groupesVariables) {
            namesGroupesVariablesPossibles[index++] = groupeVariable.getNom();
        }
        this.namesGroupesVariablesPossibles = namesGroupesVariablesPossibles;
    }

    @Override
    protected ModelGridMetadata<GroupeVariable> initModelGridMetadata() {
        try {
            propertiesNomFR = localizationManager.newProperties(GroupeVariable.TABLE_NAME, "nom_groupe", Locale.FRENCH);
            propertiesNomEN = localizationManager.newProperties(GroupeVariable.TABLE_NAME, "nom_groupe", Locale.ENGLISH);
            updateNamesGroupesVariablesPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param groupeVariable
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(GroupeVariable groupeVariable) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(groupeVariable == null ? EMPTY_STRING : groupeVariable.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(groupeVariable == null ? EMPTY_STRING : propertiesNomFR.get(groupeVariable.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(groupeVariable == null ? EMPTY_STRING : propertiesNomEN.get(groupeVariable.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(groupeVariable == null ? EMPTY_STRING : groupeVariable.getParent() == null ? "" : groupeVariable.getParent().getNom(), namesGroupesVariablesPossibles, null, true, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(groupeVariable == null ? EMPTY_STRING : groupeVariable.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(groupeVariable == null ? EMPTY_STRING : groupeVariable.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected List<GroupeVariable> getAllElements() {
        return groupeVariableDAO.getAllBy(GroupeVariable.class, GroupeVariable::getCode);
    }
}

package org.inra.ecoinfo.glacpe.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.IProcessRecord;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_FLOAT_VALUE_OUT_OF_RANGE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractProcessRecord implements IProcessRecord {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The logger @link(Logger). */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    /** The localization manager @link(ILocalizationManager). */
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IVersionFileDAO versionFileDAO;

    /**
     *
     */
    protected DatasetDescriptor datasetDescriptor;
    protected IMgaRecorder mgaRecorder;

    /**
     * Instantiates a new abstract process record.
     */
    protected AbstractProcessRecord() {
        super();
    }

    /**
     * Builds the variables header and skip header.
     * 
     * @param parser
     *            the parser
     * @param datasetDescriptor
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @link(DatasetDescriptor) the dataset descriptor
     * @return the list
     * @throws PersistenceException
     *             the persistence exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected List<RealNode> buildVariableHeaderAndSkipHeader(final String datatypeName, final int variableHeaderIndex, final ErrorsReport badsFormatsReport, final CSVParser parser) throws PersistenceException, IOException,
            BusinessException
    {
        final List<String> variablesHeaders = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parser);
        Map<String, RealNode> dvuNodes = mgaRecorder.getAllRealNodes()
                .filter(rn->rn.getNodeable().getNodeableType().equals(DatatypeVariableUnite.class))
                .collect(Collectors.toMap(rn->((DatatypeVariableUnite)rn.getNodeable()).getVariable().getCode(),rn->rn));
                
        final List<RealNode> dbVariables = new LinkedList<>();
        for (String variableHeader : variablesHeaders) {
            if (!dvuNodes.containsKey(variableHeader)) {
                badsFormatsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableHeader)));
            }
            dbVariables.add(dvuNodes.get(variableHeader));
        }
        if (badsFormatsReport.hasErrors()) {
            throw new PersistenceException(badsFormatsReport.buildHTMLMessages());
        }
        return dbVariables;
    }

    /**
     * Skip header and retrieve variable name.
     * 
     * @param variableColumnIndex
     * @link(int) the variable column index
     * @param parser
     * @link(CSVParser) the parser
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected List<String> skipHeaderAndRetrieveVariableName(final int variableColumnIndex, final CSVParser parser) throws IOException {
        final String[] values = parser.getLine();
        final List<String> variablesNames = new LinkedList<String>();

        for (int vci = variableColumnIndex; vci < values.length; vci++) {
            variablesNames.add(Utils.createCodeFromString(values[vci]));
        }
        return variablesNames;
    }

    /**
     *
     * @param value
     * @param valeurMin
     * @param valeurMax
     * @param lineNumber
     * @param columnNumber
     * @throws BadExpectedValueException
     */
    protected void testValueCoherence(Float value, Float valeurMin, Float valeurMax, long lineNumber, int columnNumber) throws BadExpectedValueException {
        if (value > valeurMax || value < valeurMin) {
            throw new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_OUT_OF_RANGE), lineNumber, columnNumber, value, valeurMin, valeurMax));
        }
    }

    public abstract void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException;

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     *
     * @return
     */
    public DatasetDescriptor getDatasetDescriptor() {
        return datasetDescriptor;
    }

    /**
     *
     * @param datasetDescriptor
     */
    public void setDatasetDescriptor(DatasetDescriptor datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

    /**
     *
     * @param mgaRecorder
     */
    public void setMgaRecorder(IMgaRecorder mgaRecorder) {
        this.mgaRecorder = mgaRecorder;
    }
}

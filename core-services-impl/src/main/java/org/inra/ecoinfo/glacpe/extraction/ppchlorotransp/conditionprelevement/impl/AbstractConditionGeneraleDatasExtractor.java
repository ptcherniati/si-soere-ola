package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractConditionGeneraleDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";
    private static final String CODE_DATATYPE_CONDITION_PRELEVEMENT = "conditions_prelevements";

    /**
     *
     */
    //protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "The data may not exist for the selected period - Il est possible que les données soit inexistantes pour la période sélectionnée";
    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvUs = getDVUs(requestMetadatas, CODE_DATATYPE_CONDITION_PRELEVEMENT);
        List<IntervalDate> intervalsDates = getIntervalsDates(requestMetadatas);
        List<MesureConditionGenerale> mesuresConditionGenerale = null;
        mesuresConditionGenerale = (List<MesureConditionGenerale>) conditionGeneraleDAO.extractDatas(policyManager.getCurrentUser(), selectedPlateformes, intervalsDates, dvUs);
        if (mesuresConditionGenerale == null || mesuresConditionGenerale.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(MAP_INDEX_0, mesuresConditionGenerale);
        return extractedDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //sortSelectedVariables(requestMetadatasMap);
    }

}

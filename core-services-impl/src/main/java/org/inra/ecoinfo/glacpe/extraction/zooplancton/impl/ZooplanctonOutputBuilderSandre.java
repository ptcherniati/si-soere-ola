/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonRawsDatas;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author afiocca
 */
public class ZooplanctonOutputBuilderSandre extends OutputBuilderZooBiovolume {

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.zooplancton.messages";
    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER_SANDRE";
    private static final String FILENAME_INFO_SUP = "Informations_supplementaires";
    private static final String CODE_DATATYPE_ZOO = "zooplancton";

    protected IPlateformeDAO plateformeDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        try {

            Iterator<ResultExtractionZooplanctonRawsDatas> resultsExtractionZooplancton = resultsDatasMap.get(MAP_INDEX_0).iterator();
            DataType datatype = datatypeDAO.getByCode(CODE_DATATYPE_ZOO).orElseThrow(() -> new BusinessException("can't find datatype"));

            Map<String, String> variablesMapSandreElement = new HashMap<>();
            Map<String, String> variablesMapSandreContext = new HashMap<>();
            Map<String, String> taxonsMapSandre = new HashMap<>();

            Map<String, String> taxonsSupMapSandre = new HashMap<>();

            Map<String, String> taxonsLvlMapSandreElement = new HashMap<>();
            Map<String, String> taxonsLvlMapSandreContext = new HashMap<>();

            Map<String, String> sitesMapSandreElement = new HashMap<>();
            Map<String, String> sitesMapSandreContext = new HashMap<>();

            Map<String, String> platformsMapSite = new HashMap<>();
            Map<String, String> platformsMapSandreElement = new HashMap<>();
            Map<String, String> platformsMapSandreContext = new HashMap<>();
            Map<String, Float> platformsMapLongitude = new HashMap<>();
            Map<String, Float> platformsMapLatitude = new HashMap<>();
            Map<String, Float> platformsMapAltitude = new HashMap<>();

            Map<String, String> stadeDevMapSandreElement = new HashMap<>();
            Map<String, String> stadeDevSandreContext = new HashMap<>();

            Map<String, File> reminderMap = new HashMap<String, File>();
            File reminderFile = buildOutputFile(FILENAME_INFO_SUP, EXTENSION_CSV);
            PrintStream printStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER));

            while (resultsExtractionZooplancton.hasNext()) {
                final ResultExtractionZooplanctonRawsDatas resultExtractionZooplancton = resultsExtractionZooplancton.next();

                final String nameTaxon = resultExtractionZooplancton.getNomLatinTaxon();
                final String codeSandreTaxon = resultExtractionZooplancton.getCodeSandreTaxon();

                final String nameSupTaxon = resultExtractionZooplancton.getNameSupTaxon();
                final String codeSandreSupTaxon = resultExtractionZooplancton.getCodeSandreSupTaxon();

                final String nameLevelTaxon = resultExtractionZooplancton.getNameLevelTaxon();
                final String codeSandreLevelTaxon = resultExtractionZooplancton.getCodeSandreLevelTaxon();
                final String codeSandreLevelContextTaxon = resultExtractionZooplancton.getCodeSandreLevelContextTaxon();

                final String nameVariable = resultExtractionZooplancton.getNomVariable();
                final String codeSandreElementVariable = resultExtractionZooplancton.getCodeSandreElementVariable();
                final String codeSandreContextVariable = resultExtractionZooplancton.getCodeSandreContextVariable();

                final String nameSite = resultExtractionZooplancton.getSite();
                final String codeSandreElementSite = resultExtractionZooplancton.getCodeSandreElementSite();
                final String codeSandreContextSite = resultExtractionZooplancton.getCodeSandreContextSite();

                final String namePlatform = resultExtractionZooplancton.getPlateforme();
                final String codeSandreElementPlatform = resultExtractionZooplancton.getCodeSandreElementPlatform();
                final String codeSandreContextPlatform = resultExtractionZooplancton.getCodeSandreContextPlatform();
                final Float longitudePlatform = resultExtractionZooplancton.getLongitudePlatform();
                final Float latitudePlatform = resultExtractionZooplancton.getLatitudePlatform();
                final Float altitudePlatform = resultExtractionZooplancton.getAltitudePlatform();

                final String nameStadeDev = resultExtractionZooplancton.getStadeDeveloppement();
                final String codeSandreElementStadeDev = resultExtractionZooplancton.getCodeSandreElementStadeDev();
                final String codeSandreContextStadeDev = resultExtractionZooplancton.getCodeSandreContextStadeDev();

                taxonsMapSandre.put(nameTaxon, codeSandreTaxon);

                taxonsSupMapSandre.put(nameSupTaxon, codeSandreSupTaxon);

                taxonsLvlMapSandreElement.put(nameLevelTaxon, codeSandreLevelTaxon);
                taxonsLvlMapSandreContext.put(nameLevelTaxon, codeSandreLevelContextTaxon);

                variablesMapSandreElement.put(nameVariable, codeSandreElementVariable);
                variablesMapSandreContext.put(nameVariable, codeSandreContextVariable);

                sitesMapSandreElement.put(nameSite, codeSandreElementSite);
                sitesMapSandreContext.put(nameSite, codeSandreContextSite);

                platformsMapSite.put(namePlatform, nameSite);
                platformsMapSandreElement.put(namePlatform, codeSandreElementPlatform);
                platformsMapSandreContext.put(namePlatform, codeSandreContextPlatform);
                platformsMapLongitude.put(namePlatform, longitudePlatform);
                platformsMapLatitude.put(namePlatform, latitudePlatform);
                platformsMapAltitude.put(namePlatform, altitudePlatform);

                stadeDevMapSandreElement.put(nameStadeDev, codeSandreElementStadeDev);
                stadeDevSandreContext.put(nameStadeDev, codeSandreContextStadeDev);

            }
            resultsExtractionZooplancton.remove();

            mapSitesSandreCSV(sitesMapSandreElement, sitesMapSandreContext, printStream);

            mapPlatformsCoordCSV(platformsMapSite, platformsMapSandreElement, platformsMapSandreContext, platformsMapLongitude, platformsMapLatitude, platformsMapAltitude, printStream);

            mapVariablesSandreCSV(variablesMapSandreElement, variablesMapSandreContext, printStream);

            mapUnitSandreCSV(variablesMapSandreElement, datatype, printStream);

            mapTaxonSandreCSV(taxonsMapSandre, printStream);

            mapTaxonParentSandreCSV(taxonsSupMapSandre, printStream);

            mapLvlTaxonSandreCSV(taxonsLvlMapSandreElement, taxonsLvlMapSandreContext, printStream);

            mapStadeDevCSV(stadeDevMapSandreElement, stadeDevSandreContext, printStream);

            reminderMap.put(FILENAME_INFO_SUP, reminderFile);

            printStream.flush();
            printStream.close();

            return reminderMap;
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param variablesMapSandreElement
     * @param variablesMapSandreContext
     * @param printStream
     */
    private void mapVariablesSandreCSV(Map<String, String> variablesMapSandreElement, Map<String, String> variablesMapSandreContext, PrintStream printStream) {

        for (String name : variablesMapSandreElement.keySet()) {

            if (variablesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"variable\";\"%s\";\"%s\";\"%s\";", name, variablesMapSandreElement.get(name), variablesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"variable\";\"%s\";", name));
            }
        }
    }

    /**
     *
     * @param sitesMapSandreElem
     * @param sitesMapSandreCont
     * @param printStream
     */
    private void mapSitesSandreCSV(Map<String, String> sitesMapSandreElement, Map<String, String> sitesMapSandreContext, PrintStream printStream) {
        for (String name : sitesMapSandreElement.keySet()) {
            if (sitesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"site\";\"%s\";\"%s\";\"%s\";", name, sitesMapSandreElement.get(name), sitesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"site\";\"%s\";", name));
            }
        }
    }

    /**
     *
     * @param sitesMapSandreElem
     * @param platformsMapLongitude
     * @param platformsMapLatitude
     * @param platformsMapAltitude
     * @param printStream
     */
    private void mapPlatformsCoordCSV(Map<String, String> platformsMapSite, Map<String, String> platformsMapSandreElement, Map<String, String> platformsMapSandreContext, Map<String, Float> platformsMapLongitude, Map<String, Float> platformsMapLatitude, Map<String, Float> platformsMapAltitude, PrintStream printStream) {

        for (String name : platformsMapLongitude.keySet()) {

            if (platformsMapLongitude.get(name) != null) {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";\"%s\";\"%s\";\"%s\";\"%s\";\"%s\";", name, platformsMapSite.get(name), platformsMapSandreElement.get(name), platformsMapSandreContext.get(name), platformsMapLongitude.get(name), platformsMapLatitude.get(name), platformsMapAltitude.get(name)));
            } else {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";", name, platformsMapSite.get(name)));
            }
        }
    }

    /**
     *
     * @param variablesMap
     * @param datatype
     * @param printStream
     * @throws PersistenceException
     */
    private void mapUnitSandreCSV(Map<String, String> variablesMap, DataType datatype, PrintStream printStream) throws PersistenceException {

        for (String name : variablesMap.keySet()) {

            DatatypeVariableUniteGLACPE datatypeVariableUnite = datatypeVariableUniteDAO.getByDatatypeAndVariable(datatype.getCode(), Utils.createCodeFromString(name)).orElseThrow(() -> new PersistenceException("can't find variable"));

            if (datatypeVariableUnite.getCodeSandre() != null) {
                printStream.println(String.format("\"Unité(variable)\";\"%s(%s)\";\"%s\";\"%s\"", datatypeVariableUnite.getUnite().getName(), name, datatypeVariableUnite.getCodeSandre(), datatypeVariableUnite.getContexte()));
            } else {
                printStream.println(String.format("\"Unité(variable)\";\"%s(%s)\"", datatypeVariableUnite.getUnite().getName(), name));
            }
        }
    }

    /**
     *
     * @param taxonsMapSandre
     * @param printStream
     */
    private void mapTaxonSandreCSV(Map<String, String> taxonsMapSandre, PrintStream printStream) {

        for (String name : taxonsMapSandre.keySet()) {

            if (taxonsMapSandre.get(name) != null) {
                printStream.println(String.format("\"taxon\";\"%s\";\"%s\";", name, taxonsMapSandre.get(name)));
            } else {
                printStream.println(String.format("\"taxon\";\"%s\";", name));
            }
        }

    }

    /**
     *
     * @param taxonsSupMapSandres
     * @param printStream
     */
    private void mapTaxonParentSandreCSV(Map<String, String> taxonsSupMapSandre, PrintStream printStream) {

        for (String name : taxonsSupMapSandre.keySet()) {

            if (taxonsSupMapSandre.get(name) != null) {
                printStream.println(String.format("\"taxon parent\";\"%s\";\"%s\";", name, taxonsSupMapSandre.get(name)));
            } else {
                printStream.println(String.format("\"taxon parent\";\"%s\";", name));
            }
        }

    }

    /**
     *
     * @param taxonsLvlMapSandreElement
     * @param taxonsLvlMapSandreContext
     * @param printStream
     */
    private void mapLvlTaxonSandreCSV(Map<String, String> taxonsLvlMapSandreElement, Map<String, String> taxonsLvlMapSandreContext, PrintStream printStream) {

        for (String name : taxonsLvlMapSandreElement.keySet()) {

            if (taxonsLvlMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"niveau de taxon\";\"%s\";\"%s\";\"%s\";", name, taxonsLvlMapSandreElement.get(name), taxonsLvlMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"niveau de taxon\";\"%s\";", name));
            }
        }

    }

    private void mapStadeDevCSV(Map<String, String> stadeDevMapSandreElement, Map<String, String> stadeDevSandreContext, PrintStream printStream) {

        for (String name : stadeDevMapSandreElement.keySet()) {

            if (stadeDevMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"stade de développement\";\"%s\";\"%s\";\"%s\";", name, stadeDevMapSandreElement.get(name), stadeDevSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"stade de développement\";\"%s\";", name));
            }
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ZooplanctonRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

}

package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonOutputBuilderByRow extends PhytoplanctonOutputsBuildersResolver {

    private IOutputBuilder phytoplanctonBuildOutputByRow;

    /**
     *
     */
    public PhytoplanctonOutputBuilderByRow() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException, NoExtractionResultException {
        super.setPhytoplanctonOutputBuilder(phytoplanctonBuildOutputByRow);
        return super.buildOutput(parameters);
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getPhytoplanctonBuildOutputByRow() {
        return phytoplanctonBuildOutputByRow;
    }

    /**
     *
     * @param phytoplanctonBuildOutputByRow
     */
    public void setPhytoplanctonBuildOutputByRow(IOutputBuilder phytoplanctonBuildOutputByRow) {
        this.phytoplanctonBuildOutputByRow = phytoplanctonBuildOutputByRow;
    }
}

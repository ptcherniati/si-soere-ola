package org.inra.ecoinfo.glacpe.dataset.phytoplancton.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.ISequencePhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SequencePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SequencePhytoplancton_;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet_;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPASequencePhytoplanctonDAO extends AbstractJPADAO<SequencePhytoplancton> implements ISequencePhytoplanctonDAO {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public Optional<SequencePhytoplancton> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode) throws PersistenceException {
        CriteriaQuery<SequencePhytoplancton> query = builder.createQuery(SequencePhytoplancton.class);
        Root<SequencePhytoplancton> s = query.from(SequencePhytoplancton.class);
        Join<RealNode, RealNode> siteRn = s
                .join(SequencePhytoplancton_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent);
        Join<RealNode, Nodeable> site = siteRn
                .join(RealNode_.nodeable);
        Join<RealNode, Nodeable> projet = siteRn
                .join(RealNode_.parent)
                .join(RealNode_.parent)
                .join(RealNode_.nodeable);
        query
                .select(s)
                .where(
                        builder.equal(s.get(SequencePhytoplancton_.datePrelevement), datePrelevement),
                        builder.equal(projet.get(Projet_.code), projetCode),
                        builder.equal(site.get(SiteGLACPE_.code), siteCode)
                );
        return getOptional(query);
    }

}

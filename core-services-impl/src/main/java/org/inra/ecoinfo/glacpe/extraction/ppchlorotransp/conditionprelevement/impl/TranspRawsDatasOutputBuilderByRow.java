package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */


public class TranspRawsDatasOutputBuilderByRow extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT = "org.inra.ecoinfo.glacpe.extraction.conditionprelevement.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_TRANSP";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_TRANSPARENCE = "transparence";
    private static final String CODE_CONDITION_GENERALE = "conditions_prelevements";
    private static final String SUFFIX_FILENAME = CODE_TRANSPARENCE;
    private IVariableDAO variableDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.TRANSPARENCE);

        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT, HEADER_RAW_DATA)));

            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
                String uniteNom = dvu.getUnite().getName();

                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, PPChloroTranspParameter.TRANSPARENCE);

        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>>> mapByProject = ((List<MesureConditionGenerale>) resultsDatasMap.get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE))
                .stream()
                .collect(
                        Collectors.groupingBy(
                                m -> m.getProjet(),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> m.getSite(),
                                        TreeMap::new,
                                        Collectors.groupingBy(
                                                m -> m.getPlateforme(),
                                                TreeMap::new,
                                                Collectors.groupingBy(
                                                        m -> m.getDatePrelevement(),
                                                        TreeMap::new,
                                                        Collectors.toCollection(TreeSet::new)
                                                )
                                        )
                                )
                        )
                );

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        String line;
        for (Iterator<Map.Entry<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>>>> iterator = mapByProject.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>> mapBySite = projetEntry.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            for (Iterator<Map.Entry<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                TreeMap<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>> mapByPlateforme = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME));
                for (Iterator<Map.Entry<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, TreeMap<LocalDate, TreeSet<MesureConditionGenerale>>> platformEntry = iterator2.next();
                    Plateforme plateforme = platformEntry.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    TreeMap<LocalDate, TreeSet<MesureConditionGenerale>> mapByDate = platformEntry.getValue();
                    for (Iterator<Map.Entry<LocalDate, TreeSet<MesureConditionGenerale>>> iterator3 = mapByDate.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, TreeSet<MesureConditionGenerale>> dateEntry = iterator3.next();
                        LocalDate date = dateEntry.getKey();
                        TreeSet<MesureConditionGenerale> sortedMesures = dateEntry.getValue();
                        for (MesureConditionGenerale mesureTransparence : sortedMesures) {

                            // nom projet;nom site;nom plateforme;date prélèvement,heure prélèvement
                            line = String.format("%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, date, mesureTransparence.getHeure() != null
                                    ? DateUtil.getUTCDateTextFromLocalDateTime(mesureTransparence.getHeure(), DateUtil.HH_MM_SS)
                                    : "");

                            rawDataPrintStream.print(line);

                            List<ValeurConditionGenerale> valeursTransparence = mesureTransparence.getValeurs();
                            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                                rawDataPrintStream.print(";");

                                for (ValeurConditionGenerale valeurTransparence : valeursTransparence) {
                                    if (valeurTransparence.getRealNode().getNodeable().equals(dvu)) {
                                        if (valeurTransparence.getValeur() != null) {
                                            rawDataPrintStream.print(String.format("%s", valeurTransparence.getValeur()));
                                        } else {
                                            rawDataPrintStream.print("");
                                        }
                                        break;
                                    }
                                }
                            }
                            rawDataPrintStream.println();
                            // commenter ?
                            //mesuresTransparence.remove();

                        }
                    }
                }
            }
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }
}

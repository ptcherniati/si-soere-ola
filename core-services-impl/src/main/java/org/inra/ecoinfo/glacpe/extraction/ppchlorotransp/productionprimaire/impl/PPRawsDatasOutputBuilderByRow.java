package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import static org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl.AbstractPPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPRawsDatasOutputBuilderByRow extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_PP = "org.inra.ecoinfo.glacpe.dataset.productionprimaire.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_DATATYPE_PRODUCTION_PRIMAIRE = "production_primaire";
    private static final String SUFFIX_FILENAME = CODE_DATATYPE_PRODUCTION_PRIMAIRE;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesUniteName = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, Unite.ATTRIBUTE_JPA_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA)));
            }

            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                String localizedUniteName = propertiesUniteName.getProperty(dvu.getUnite().getCode(), dvu.getUnite().getCode());
                String localizedVariableName = propertiesUniteName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());

                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, dvu.getUnite().getCode()));
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        Comparator<MesurePP> comparator = (m1, m2) -> {
            if (m1.getSousSequencePP().getSequencePP().getDate().compareTo(m2.getSousSequencePP().getSequencePP().getDate()) != 0) {
                return m1.getSousSequencePP().getSequencePP().getDate().compareTo(m2.getSousSequencePP().getSequencePP().getDate());
            }
            if (m1.getSousSequencePP().getPlateforme().compareTo(m2.getSousSequencePP().getPlateforme()) != 0) {
                return m1.getSousSequencePP().getPlateforme().compareTo(m2.getSousSequencePP().getPlateforme());
            }
            if (m1.getProfondeur() == null) {
                return m1.getId().compareTo(m2.getId());
            }
            return m1.getId().compareTo(m2.getId());
        };
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<MesurePP>>> mesuresByProjet = ((List<MesurePP>) resultsDatasMap.get(CST_RESULT_EXTRACTION_CODE))
                .stream()
                .collect(
                        Collectors.groupingBy(
                                m -> LacsUtils.getProjetFromDataset(m.getSousSequencePP().getSequencePP().getVersionFile().getDataset()),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> LacsUtils.getSiteFromDataset(m.getSousSequencePP().getSequencePP().getVersionFile().getDataset()),
                                        TreeMap::new,
                                        Collectors.toCollection(
                                                () -> new TreeSet<>(comparator)
                                        )
                                )
                        )
                );
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> entry : selectedPlateformes.entrySet()) {
            Projet projet = entry.getKey();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getCode());
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = entry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> plateformeEntry : mapBySite.entrySet()) {
                SiteGLACPE site = plateformeEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getCode());
                SortedSet<Plateforme> platforms = plateformeEntry.getValue();
                final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME));
                rawDataPrintStream.println(headers);
                for (Iterator<MesurePP> iterator = mesuresByProjet.get(projet).get(site).iterator(); iterator.hasNext();) {
                    MesurePP mesurePP = iterator.next();
                    String line;
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(mesurePP.getSousSequencePP().getPlateforme().getCode(), mesurePP.getSousSequencePP().getPlateforme().getName());
                    final String date = DateUtil.getUTCDateTextFromLocalDateTime(mesurePP.getSousSequencePP().getSequencePP().getDate(), DateUtil.DD_MM_YYYY);
                    final String dureeIncubation = DateUtil.getUTCDateTextFromLocalDateTime(mesurePP.getSousSequencePP().getDureeIncubation(), DateUtil.HH_MM_SS);
                    final String heureDebutIncubation = DateUtil.getUTCDateTextFromLocalDateTime(mesurePP.getSousSequencePP().getHeureDebutIncubation(), DateUtil.HH_MM_SS);
                    final String heureFinIncubation = DateUtil.getUTCDateTextFromLocalDateTime(mesurePP.getSousSequencePP().getHeureFinIncubation(), DateUtil.HH_MM_SS);
                    if (depthRequestParamVO.getAllDepth()) {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s",
                                localizedProjetName,
                                localizedSiteName,
                                localizedPlateformeName,
                                date,
                                mesurePP.getProfondeur(),
                                mesurePP.getSousSequencePP().getHeureDebutIncubation() != null ? heureDebutIncubation : "",
                                mesurePP.getSousSequencePP().getHeureFinIncubation() != null ? heureFinIncubation : "",
                                mesurePP.getSousSequencePP().getDureeIncubation() != null ? dureeIncubation : "");
                    } else {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                                localizedProjetName,
                                localizedSiteName,
                                localizedPlateformeName,
                                depthRequestParamVO.getDepthMin(),
                                depthRequestParamVO.getDepthMax(),
                                date,
                                mesurePP.getProfondeur(),
                                mesurePP.getSousSequencePP().getHeureDebutIncubation() != null ? heureDebutIncubation : "",
                                mesurePP.getSousSequencePP().getHeureFinIncubation() != null ? heureFinIncubation : "",
                                mesurePP.getSousSequencePP().getDureeIncubation() != null ? dureeIncubation : "");
                    }
                    rawDataPrintStream.print(line);

                    Map<Long, Float> valeursMesuresPP = buildValeurs(mesurePP.getValeurs());
                    for (DatatypeVariableUniteGLACPE dvu : dvus) {
                        rawDataPrintStream.print(";");
                        Float valeur = valeursMesuresPP.get(dvu.getId());
                        if (valeur != null) {
                            rawDataPrintStream.print(String.format("%s", valeur));
                        } else {
                            rawDataPrintStream.print("");
                        }
                    }
                    rawDataPrintStream.println();
                    // commenter ?
                    //mesuresPP.remove();
                }
            }
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesurePP> valeurs) {
        return valeurs.stream()
                .collect(
                        Collectors.toMap(
                                v -> v.getRealNode().getNodeable().getId(),
                                v -> v.getValeur()
                        )
                );
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

}

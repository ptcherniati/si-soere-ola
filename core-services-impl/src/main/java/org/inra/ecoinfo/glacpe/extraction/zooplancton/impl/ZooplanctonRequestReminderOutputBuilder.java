package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGlacpeRequestReminder;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public class ZooplanctonRequestReminderOutputBuilder extends AbstractGlacpeRequestReminder {

    private static final String DATATYPE_ZOOPLANKTON = "Zooplancton";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "ZooplanctonRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.zooplancton.messages";

    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER";
    private static final String PROPERTY_MSG_DETAILS_DEVELOPMENT_STAGE = "MSG_DETAILS_DEVELOPMENT_STAGE";
    private static final String PROPERTY_MSG_DETAILS_TAXA = "MSG_DETAILS_TAXA";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;

        try {

            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printPlateformesSummary(requestMetadatasMap, reminderPrintStream);
            printVariablesSummary(requestMetadatasMap, reminderPrintStream);
            printTaxonsSummary(requestMetadatasMap, reminderPrintStream);
            printDatesSummary(requestMetadatasMap, reminderPrintStream);
            printCheckBoxesDetails((Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_DEVELOPMENT_STAGE), AbstractGLACPEExtractor.getDVUs(requestMetadatasMap).isEmpty(), reminderPrintStream);
            printComment(reminderPrintStream, requestMetadatasMap);

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }


    private void printCheckBoxesDetails(Boolean cbxDetailDevelopmentStage, Boolean cbxDetailTaxon, PrintStream reminderPrintStream) {
        if (cbxDetailDevelopmentStage) {
            MDC.put("extract.detail.developpement", cbxDetailDevelopmentStage.toString());
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DETAILS_DEVELOPMENT_STAGE));
        }
        if (cbxDetailTaxon) {
            MDC.put("extract.detail.taxon", cbxDetailTaxon.toString());
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_DETAILS_TAXA));
        }

        reminderPrintStream.println();
    }


    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;
    }
}

package org.inra.ecoinfo.glacpe.dataset.impl;

import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.IDeleteRecord;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class DeleteRecord implements IDeleteRecord {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * The version file dao @link(IVersionFileDAO). {@link IVersionFileDAO} The version file dao.
     */
    private IVersionFileDAO versionFileDAO;

    /**
     * The publication dao @link(ILocalPublicationDAO). {@link ILocalPublicationDAO} publication dao.
     */
    private ILocalPublicationDAO publicationDAO;

    /** The logger @link(Logger). {@link Logger} The logger. */
    protected final Logger logger = LoggerFactory.getLogger(RecorderGLACPE.class.getName());

    /**
     * Instantiates a new delete record.
     */
    public DeleteRecord() {}

    /**
     * Delete record.
     * 
     * @param versionFile
     * @link(VersionFile) the version file
     * @throws BusinessException
     *             the business exception
     * @see org.inra.ecoinfo.glacpe.dataset.IDeleteRecord#deleteRecord(org.inra.ecoinfo.dataset.versioning.entity.VersionFile)
     * @see org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO#removeVersion((org .inra.ecoinfo.dataset.versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecord(VersionFile versionFile) throws BusinessException {
        try {
            versionFile = versionFileDAO.merge(versionFile);
            publicationDAO.removeVersion(versionFile);
        } catch (final PersistenceException e) {
            logger.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Sets the publication dao.
     * 
     * @param publicationDAO
     *            {@link ILocalPublicationDAO} the new publication dao
     */
    public void setPublicationDAO(final ILocalPublicationDAO publicationDAO) {
        this.publicationDAO = publicationDAO;
    }

    /**
     * Sets the version file dao.
     * 
     * @param versionFileDAO
     *            {@link IVersionFileDAO} the new version file dao
     */
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }
}

/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPATaxonDAO extends AbstractJPADAO<Taxon> implements ITaxonDAO {

    private static final String PHYTOPLANCTON = "Phytoplancton";
    private static final String ZOOPLANCTON = "Zooplancton";

    /**
     * from Taxon t where t.code = :code
     *
     * @param code
     * @return
     */
    @Override
    public Optional<Taxon> getByCode(String code) {
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        query
                .select(taxon)
                .where(builder.equal(taxon.get(Taxon_.code), code));
        return getOptional(query);
    }

    /**
     * from Taxon t where t.nomLatin = :nom
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Taxon> getByNomLatin(String nom) {
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        query
                .select(taxon)
                .where(builder.equal(taxon.get(Taxon_.nomLatin), nom));
        return getOptional(query);
    }

    /**
     * from Taxon t where t.taxonParent is null and t.theme =
     * &apos;Phytoplancton&apos; order by t.nomLatin
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    public List<Taxon> getPhytoRootTaxa() {
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        query
                .select(taxon)
                .where(builder.and(
                        builder.and(builder.isNull(taxon.get(Taxon_.taxonParent)),
                                builder.equal(taxon.get(Taxon_.theme), PHYTOPLANCTON)
                        )
                ))
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }

    /**
     * select t.id from Taxon t where t.taxonParent.id = :taxonParentId
     *
     * @param parentTaxonId
     * @return
     */
    @Override
    public List<Long> getAllSubtaxaIdsAndParentTaxaId(Long parentTaxonId) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        Join<Taxon, Taxon> parent = taxon.join(Taxon_.taxonParent);
        query
                .select(taxon.get(Taxon_.id))
                .where(
                        builder.equal(parent.get(Taxon_.id), parentTaxonId)
                )
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }

    /**
     * select t.id from Taxon t where t.taxonParent.id in (:taxaIds)
     *
     * @param taxaIds
     * @return
     */
    private List<Long> getAllSubTaxa(List<Long> taxaIds) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        Join<Taxon, Taxon> parent = taxon.join(Taxon_.taxonParent);
        query
                .select(taxon.get(Taxon_.id))
                .where(
                        parent.get(Taxon_.id).in(taxaIds)
                )
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }

    /**
     * select t.nomLatin from Taxon t where t.theme = :theme order by t.nomLatin
     *
     * @param theme
     * @return
     */
    @Override
    public List<String> getTaxonNameByTheme(String theme) {
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        query
                .select(taxon.get(Taxon_.nomLatin))
                .where(
                        builder.equal(taxon.get(Taxon_.theme), theme)
                )
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }

    /**
     * = from TaxonProprieteTaxon tpt where tpt.taxon.id = :taxonId
     *
     * @param taxonId
     * @return
     */
    @Override
    public List<TaxonProprieteTaxon> getTaxonProprieteTaxonByTaxonId(long taxonId) {
        CriteriaQuery<TaxonProprieteTaxon> query = builder.createQuery(TaxonProprieteTaxon.class);
        Root<TaxonProprieteTaxon> taxonProprieteTaxon = query.from(TaxonProprieteTaxon.class);
        Join<TaxonProprieteTaxon, Taxon> taxon = taxonProprieteTaxon.join(TaxonProprieteTaxon_.taxon);
        query
                .select(taxonProprieteTaxon)
                .where(
                        builder.equal(taxon.get(Taxon_.id), taxonId)
                );
        return getResultList(query);
    }

    /**
     * from Taxon t where t.theme = :theme order by t.nomLatin
     *
     * @param theme
     * @return
     */
    @Override
    public List<Taxon> getTaxonByTheme(String theme) {
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        query
                .select(taxon)
                .where(
                        builder.equal(taxon.get(Taxon_.theme), theme)
                )
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }

    /**
     * from Taxon t where t.theme = &apos;Phytoplancton&apos; order by
     * t.nomLatin
     */
    @Override
    public List<Taxon> getPhytoTaxonInfos() {
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        query
                .select(taxon)
                .where(
                        builder.equal(taxon.get(Taxon_.theme), PHYTOPLANCTON)
                )
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }

    /**
     * from Taxon t where t.taxonParent is null and t.theme =
     * &apos;Zooplancton&apos; order by t.nomLatin
     *
     */
    @Override
    public List<Taxon> getZooRootTaxa() {
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class
        );
        Root<Taxon> taxon = query.from(Taxon.class
        );
        query
                .select(taxon)
                .where(
                        builder.equal(taxon.get(Taxon_.theme), ZOOPLANCTON)
                )
                .orderBy(builder.asc(taxon.get(Taxon_.nomLatin)));
        return getResultList(query);
    }
}

package org.inra.ecoinfo.glacpe.dataset.hautefrequence;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SequenceHauteFrequence;

/**
 *
 * @author mylene
 */
public interface ISequenceHauteFrequenceDAO extends IDAO<SequenceHauteFrequence> {
    
    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     */
    Optional<SequenceHauteFrequence> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode);

    void flush();
    
}

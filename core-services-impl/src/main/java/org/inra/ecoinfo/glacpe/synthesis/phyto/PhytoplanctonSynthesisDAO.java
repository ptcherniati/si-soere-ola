/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.phyto;

import java.util.stream.Stream;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     *
     */
    public static String SYNTHESIS_REQUEST = "insert into phytosynthesisvalue(id, date, idnode, ismean, site, valuefloat, valuestring, variable, siteid, projetid, plateformeid, variableid, profondeurs, taxons)\n"
            + "(SELECT \n"
            + "            nextval('hibernate_sequence') id,            "
            + "            s.date_prelevement date,\n"
            + "            dnv.branch_node_id idNode,\n"
            + "            false isMean,\n"
            + "            rns.path site,\n"
            + "            avg(v.valeur) valueFloat,\n"
            + "            null valueString,\n"
            + "            nddvu.code variable,\n"
            + "            rns.id_nodeable siteid,\n"
            + "            rnp.id_nodeable projetid,\n"
            + "            ss.loc_id plateformeid,\n"
            + "            dvu.var_id variableid,\n"
            + "            array_agg(distinct ss.profondeur_max order by ss.profondeur_max)::::text profondeurs,\n"
            + "            array_agg(distinct tax.code order by tax.code)::::text profondeurs\n"
            + "            from valeur_mesure_phytoplancton_vmphytoplancton v\n"
            + "            join composite_node_data_set dnv on dnv.realnode= v.id\n"
            + "            join mesure_phytoplancton_mphytoplancton m ON m.mphytoplancton_id = v.mphytoplancton_id\n"
            + "            join sous_sequence_phytoplancton_ssphytoplancton ss ON ss.ssphytoplancton_id = m.ssphytoplancton_id\n"
            + "            join sequence_phytoplancton_sphytoplancton s ON s.sphytoplancton_id = ss.sphytoplancton_id\n"
            + "            JOIN taxon_tax tax ON tax.tax_id = m.tax_id\n"
            + "            join realnode rnv ON rnv.id = v.id\n"
            + "            join composite_nodeable nddvu on nddvu.id = rnv.id_nodeable\n"
            + "            join datatype_variable_unite_glacpe_dvug dvu ON rnv.id_nodeable = vdt_id\n"
            + "            join realnode rnd ON rnd.id = rnv.id_parent_node\n"
            + "            JOIN realnode rnt ON rnt.id = rnd.id_parent_node\n"
            + "            JOIN realnode rns ON rns.id = rnt.id_parent_node\n"
            + "            JOIN realnode rnts ON rnts.id = rns.id_parent_node\n"
            + "            JOIN realnode rnp ON rnp.id = rnts.id_parent_node\n"
            + "            group by s.date_prelevement, rns.path, dnv.branch_node_id, nddvu.code, ss.loc_id, rns.id_nodeable, rnp.id_nodeable, rnp.id_nodeable,  dvu.var_id"
            + ")";

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        entityManager.createNativeQuery(SYNTHESIS_REQUEST).executeUpdate();
        return Stream.empty();
    }

}

package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IMesureConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_SOURCE_PATH_CONDITIONPRELEVEMENT = "org.inra.ecoinfo.glacpe.dataset.conditionprelevement.messages";

    private static final String PROPERTY_MSG_CONSTRAINT_EXCEPTION = "PROPERTY_MSG_CONSTRAINT_EXCEPTION";

    /**
     *
     */
    protected IMesureConditionGeneraleDAO mesureConditionGeneraleDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureConditionGeneraleDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        String datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datatypeCode, datasetDescriptor)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
        try {
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();

            String[] values = null;
            long lineCount = 1;
            // Saute la 1ere ligne
            parser.getLine();
            SortedMap<LocalDateTime, SortedMap<String, List<LineRecord>>> sequencesMapLines = new TreeMap<>();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());

                if (!projetCode.equals(projetCodeFromVersion)) {
                    InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), cleanerValues.currentTokenIndex(), projetCodeFromVersion, lineCount, projetCode));
                    throw insertionDatabaseException;
                }
                String nomSite = cleanerValues.nextToken();

                if (!Utils.createCodeFromString(nomSite).equals(siteCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, lineCount, 2, nomSite)));
                }

                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                LocalDate datePrelevement = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);
                LocalTime heure = readTime(errorsReport, lineCount, 5, cleanerValues, datasetDescriptor);
                String commentaire = cleanerValues.nextToken();

                // On crée une liste de Variables + Sa valeur - ici
                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        RealNode realNode = Optional
                                .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                                .map(c -> Utils.createCodeFromString(c.getName()))
                                .map(n -> realNodes.get(n))
                                .map(v -> v.get(0))
                                .orElse(null);
                        if (realNode != null) {

                            VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                            if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                            }
                            variablesValues.add(new VariableValue(realNode, values[actualVariableArray].trim()));
                        } else {
                            errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)));

                        }
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, heure, variablesValues, lineCount, projetCode, commentaire);
                sequencesMapLines
                        .computeIfAbsent(datePrelevement.atTime(heure), k -> new TreeMap<>())
                        .computeIfAbsent(plateformeCode, k -> new LinkedList<>())
                        .add(line);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
            Allocator allocator = Allocator.getInstance();

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<LocalDateTime, SortedMap<String, List<LineRecord>>>> iterator = sequencesMapLines.entrySet().iterator();
            for (Entry<LocalDateTime, SortedMap<String, List<LineRecord>>> entry : sequencesMapLines.entrySet()) {
                LocalDateTime datePrelevement = entry.getKey();
                SortedMap<String, List<LineRecord>> platformMap = entry.getValue();
                for (Entry<String, List<LineRecord>> platformEntry : platformMap.entrySet()) {
                    String platformCode = platformEntry.getKey();
                    List<LineRecord> lineRecords = platformEntry.getValue();
                    buildSequence(datePrelevement, projetCodeFromVersion, siteCodeFromVersion, platformCode, lineRecords, versionFile, errorsReport);
                    logger.debug(String.format("%d - %s", count++, datePrelevement));

                    // Très important à cause de problèmes de performances
                    mesureConditionGeneraleDAO.flush();
                    versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                }

            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private LocalTime readTime(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String timeString;
        LocalTime time = null;
        timeString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            time = Strings.isNullOrEmpty(timeString) ? null
                    : DateUtil.readLocalTimeFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), timeString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_TIME), timeString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return time;

    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, String datatypeCode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    private void buildSequence(LocalDateTime datePrelevementSequence, String projetCodeSequence, String siteCode, String plateformeCodeSequence, List<LineRecord> mesureLines, VersionFile versionFile,
            ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        LineRecord firstLine = mesureLines.get(0);

        MesureConditionGenerale mesureConditionGenerale = new MesureConditionGenerale();
        Plateforme plateforme = plateformeDAO.getByNKey(plateformeCodeSequence, Utils.createCodeFromString(firstLine.getNomSite())).orElse(null);
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCodeSequence, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }
        if (!((SiteGLACPE) plateforme.getSite()).getCode().equals(siteCode)) {
            String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), plateformeCodeSequence, firstLine.getOriginalLineNumber(), siteCode);
            errorsReport.addException(new BusinessException(message));
        }

        mesureConditionGenerale.setPlateforme(plateforme);
        mesureConditionGenerale.setDatePrelevement(datePrelevementSequence.toLocalDate());

        mesureConditionGenerale.setVersionFile(versionFile);
        mesureConditionGenerale.setHeure(datePrelevementSequence.toLocalTime());
        mesureConditionGenerale.setCommentaire(firstLine.getCommentaire());
        mesureConditionGenerale.setLigneFichierEchange(firstLine.getOriginalLineNumber());

        if (!errorsReport.hasErrors()) {
            mesureConditionGeneraleDAO.saveOrUpdate(mesureConditionGenerale);
        }

        for (LineRecord line : mesureLines) {
            for (VariableValue variableValue : line.getVariablesValues()) {
                ValeurConditionGenerale valeurConditionGenerale = new ValeurConditionGenerale();
                VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());
                valeurConditionGenerale.setRealNode(mgaRecorder.getRealNodeById(variableValue.getRealNode().getId()).orElse(null));

                if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                    valeurConditionGenerale.setValeur(null);
                } else {
                    if (variable.getIsQualitative()) {
                        Boolean notInList = true; 
                        Optional<IValeurQualitative> valeurQualitativeOpt = valeurQualitativeDAO.getByCode(variable.getCode())
                                .stream()
                                .filter(valeurQualitative->Utils.createCodeFromString(valeurQualitative.getValeur()).equals(Utils.createCodeFromString(variableValue.getValue())))
                                .findFirst();
                        if (valeurQualitativeOpt.isPresent()) {
                            valeurConditionGenerale.setValeurQualitative((ValeurQualitative) valeurQualitativeOpt.get());
                        }else{
                            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), variableValue.getValue(), variable.getName())));
                        }
                    } else {
                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurConditionGenerale.setValeur(value);
                    }
                }
                valeurConditionGenerale.setMesure(mesureConditionGenerale);
                mesureConditionGenerale.getValeurs().add(valeurConditionGenerale);
            }
        }
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param valeurQualitativeDAO
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param mesureConditionGeneraleDAO
     */
    public void setMesureConditionGeneraleDAO(IMesureConditionGeneraleDAO mesureConditionGeneraleDAO) {
        this.mesureConditionGeneraleDAO = mesureConditionGeneraleDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }

}

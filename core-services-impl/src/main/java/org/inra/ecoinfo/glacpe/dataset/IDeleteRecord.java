package org.inra.ecoinfo.glacpe.dataset;

import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IDeleteRecord extends Serializable {

    /**
     * Delete record.
     * 
     * @param versionFile
     * @link(VersionFile) the version file
     * @throws BusinessException
     *             delete the version of versionFile {@link VersionFile} the version file
     */
    void deleteRecord(VersionFile versionFile) throws BusinessException;
}

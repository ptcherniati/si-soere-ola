package org.inra.ecoinfo.glacpe.dataset.productionprimaire;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP;

/**
 *
 * @author ptcherniati
 */
public interface ISequencePPDAO extends IDAO<SequencePP> {

    /**
     *
     * @param date
     * @param projetCode
     * @param siteCode
     * @return
     */
    Optional<SequencePP> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate date, String projetCode, String siteCode);

}

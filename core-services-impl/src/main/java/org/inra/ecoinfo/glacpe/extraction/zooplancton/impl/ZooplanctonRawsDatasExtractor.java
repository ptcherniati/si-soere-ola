package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDAO;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonRawsDatas;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author Antoine Schellenberger
 */
public class ZooplanctonRawsDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "zooplanctonRawDatas";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "MSG_NO_EXTRACTION_RESULT_RAW_DATAS";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.zooplancton.messages";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatas.get(Taxon.class.getSimpleName());
        List<Long> idsTaxonsLeaves = buildIdsTaxons(selectedTaxons);
        List<IntervalDate> intervalDates = getIntervalsDates(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        List<ResultExtractionZooplanctonRawsDatas> results = null;

        results = (List<ResultExtractionZooplanctonRawsDatas>) zooplanctonDAO.extractDatas(policyManager.getCurrentUser(), selectedPlateformes, intervalDates, dvus, idsTaxonsLeaves);
        if (results == null || results.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(MAP_INDEX_0, results);
        return extractedDatasMap;
    }

    private List<Long> buildIdsTaxons(List<Taxon> selectedTaxons) {
        List<Long> taxonsIds = new LinkedList<Long>();
        for (Taxon selectedTaxon : selectedTaxons) {
            if (selectedTaxon.getTaxonsEnfants() != null && !selectedTaxon.getTaxonsEnfants().isEmpty()) {
                taxonsIds.addAll(buildIdsTaxons(selectedTaxon.getTaxonsEnfants()));
            } else {
                taxonsIds.add(selectedTaxon.getId());

            }
        }
        return taxonsIds;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        ((ZooplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

}

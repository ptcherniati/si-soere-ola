package org.inra.ecoinfo.glacpe.dataset.zooplancton.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_ERROR;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_INVALID_DATE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_INVALID_DEPTH;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_INVALID_DEPTH_INTERVAL;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.PROPERTY_MSG_TWICE_DATA;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessageWithBundle;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IMetadataZooplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.DataZooplancton;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.MetadataZooplancton;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.jpa.stadedeveloppement.IStadeDeveloppementDAO;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    private static final String STRING_EMPTY = "";

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String PATH_BUNDLE_SOURCE = "org.inra.ecoinfo.zooplancton.services.messages";
    private static final String PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS = "MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS = "MSG_MISSING_TAXON_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS = "MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS = "MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS";

    /**
     * The Constant PROPERTY_MSG_PROJET_EXPECTED.
     */
    public static final String PROPERTY_MSG_PROJET_EXPECTED = "PROPERTY_MSG_PROJET_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_SITE_EXPECTED.
     */
    public static final String PROPERTY_MSG_SITE_EXPECTED = "PROPERTY_MSG_SITE_EXPECTED";
    private static final String VARIABLE_PROFONDEUR_MINIMUM = "profondeur_minimum";
    private static final String VARIABLE_PROFONDEUR_MAXIMUM = "profondeur_maximum";

    /**
     *
     */
    protected IMetadataZooplanctonDAO metadataZooplanctonDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     */
    protected IProjetDAO projetDAO;
    private IStadeDeveloppementDAO stadeDeveloppementDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;
    protected String datatypeCode;

    /**
     *
     * @param stadeDeveloppementDAO
     */
    public void setStadeDeveloppementDAO(Object stadeDeveloppementDAO) {
        this.stadeDeveloppementDAO = (IStadeDeveloppementDAO) stadeDeveloppementDAO;
    }

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(this.getClass().getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        Projet projetFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset());
        SiteGLACPE siteFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset());
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datasetDescriptor)
                .computeIfAbsent(projetFromVersion.getCode(), k -> new HashMap<>())
                .computeIfAbsent(siteFromVersion.getCode(), k -> new HashMap<String, List<RealNode>>());

        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();
            // Saute la 1ere ligne
            parser.getLine();
            SortedMap<LocalDate, SortedMap<String, SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>>> sequencesMapLines = new TreeMap<>();

            // On parcourt chaque ligne du fichier
            Set<String> doublonSet = new HashSet();

            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                if (!Utils.createCodeFromString(projetCode).equals(projetFromVersion.getCode())) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), projetFromVersion.getCode(), lineCount, 1, projetCode)));
                }
                String nomSite = cleanerValues.nextToken();
                if (!Utils.createCodeFromString(nomSite).equals(siteFromVersion.getCode())) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteFromVersion.getCode(), lineCount, 2, nomSite)));
                }
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                LocalDate datePrelevement = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String outilPrelevementCode = Utils.createCodeFromString(cleanerValues.nextToken());

                //Traitement de la profondeur Min
                Float profondeurMin = readDepth(cleanerValues, "profondeur_minimum", errorsReport, controlesCoherenceMap, lineCount, 7);

                //Traitement de la profondeur Max
                Float profondeurMax = readDepth(cleanerValues, "profondeur_minimum", errorsReport, controlesCoherenceMap, lineCount, 8);

                //Comparaison profondeurs Min et Max
                if (profondeurMin != null && profondeurMax != null && profondeurMax < profondeurMin) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH_INTERVAL), profondeurMin, profondeurMax, lineCount, 5, 6)));
                }
                String determinateur = cleanerValues.nextToken();
                Float volumeSedimente = readFloat(errorsReport, lineCount, cleanerValues, datasetDescriptor).orElse(null);
                String nomTaxon = Utils.createCodeFromString(cleanerValues.nextToken());
                String stadeDeveloppement = Utils.createCodeFromString(cleanerValues.nextToken());

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    RealNode realNode = Optional
                            .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                            .map(c -> Utils.createCodeFromString(c.getName()))
                            .map(n -> realNodes.get(n))
                            .map(v -> v.get(0))
                            .orElse(null);
                    if (realNode != null) {
                        VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                        if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                            testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                        }
                        variablesValues.add(new VariableValue(realNode, values[actualVariableArray].replaceAll(" ", "")));
                    } else {
                        errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetFromVersion.getCode(), siteFromVersion.getCode())));

                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, profondeurMin, profondeurMax, outilMesureCode, outilPrelevementCode, determinateur, volumeSedimente, stadeDeveloppement, nomTaxon, variablesValues, lineCount,
                        projetCode);

                if (!doublonSet.contains(line.toString())) {
                    doublonSet.add(line.toString());
                    sequencesMapLines
                            .computeIfAbsent(datePrelevement, k -> new TreeMap<>())
                            .computeIfAbsent(plateformeCode, k -> new TreeMap<>())
                            .computeIfAbsent(outilPrelevementCode, k -> new TreeMap<>())
                            .computeIfAbsent(nomTaxon, k -> new TreeMap<>())
                            .computeIfAbsent(stadeDeveloppement, k -> new LinkedList<>())
                            .add(line);
                } else {
                    errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_TWICE_DATA), line.getOriginalLineNumber())));
                }

            }
            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }
            int count = 0;
            doublonSet = null;
            List<String> listErrors = new LinkedList<>();
            for (Iterator<Map.Entry<LocalDate, SortedMap<String, SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>>>> iterator = sequencesMapLines.entrySet().iterator(); iterator.hasNext();) {
                Map.Entry<LocalDate, SortedMap<String, SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>>> dateEntry = iterator.next();
                LocalDate datePrelevement = dateEntry.getKey();
                SortedMap<String, SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>> mapByPlateforme = dateEntry.getValue();
                for (Iterator<Map.Entry<String, SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>>> iterator1 = mapByPlateforme.entrySet().iterator(); iterator1.hasNext();) {
                    Map.Entry<String, SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>> plateformeEntry = iterator1.next();
                    String plateformeCode = plateformeEntry.getKey();
                    SortedMap<String, SortedMap<String, SortedMap<String, List<LineRecord>>>> mapByOutil = plateformeEntry.getValue();
                    for (Iterator<Map.Entry<String, SortedMap<String, SortedMap<String, List<LineRecord>>>>> iterator2 = mapByOutil.entrySet().iterator(); iterator2.hasNext();) {
                        Map.Entry<String, SortedMap<String, SortedMap<String, List<LineRecord>>>> outilEntry = iterator2.next();
                        String outilPrelevementCode = outilEntry.getKey();
                        SortedMap<String, SortedMap<String, List<LineRecord>>> mapByTaxon = outilEntry.getValue();
                        try {
                            buildMetadata(datePrelevement, projetFromVersion, plateformeCode, siteFromVersion, outilPrelevementCode, mapByTaxon, versionFile, errorsReport, listErrors);
                            logger.debug(String.format("%d - %s", count++, datePrelevement));

                            // Très important à cause de problèmes de performances
                            metadataZooplanctonDAO.flush();
                            versionFile = (VersionFile) versionFileDAO.merge(versionFile);

                        } catch (InsertionDatabaseException e) {
                            errorsReport.addException(e);
                        }
                        iterator2.remove();
                    }
                    iterator1.remove();
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private Float readDepth(CleanerValues cleanerValues, String variableCode, ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, int columnsCount) {
        String profondeurString = cleanerValues.nextToken();
        Float value = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
        if (value != null) {
            try {
                value = Float.parseFloat(profondeurString);

                if (!controlesCoherenceMap.isEmpty() && controlesCoherenceMap.get(variableCode) != null) {
                    testValueCoherence(
                            value,
                            controlesCoherenceMap.get(variableCode).getValeurMin(),
                            controlesCoherenceMap.get(variableCode).getValeurMax(),
                            lineCount,
                            columnsCount);
                }
            } catch (BadExpectedValueException e) {
                errorsReport.addException(e);

            } catch (Exception e) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, columnsCount)));
            }
        }
        return value;
    }

    private Optional<Float> readFloat(ErrorsReport errorsReport, long lineCount, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String stringDecimal;
        Float decimal = null;
        stringDecimal = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            decimal = Strings.isNullOrEmpty(stringDecimal) ? null
                    : Float.parseFloat(stringDecimal);
        } catch (NumberFormatException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), stringDecimal, lineCount, cleanerValues.currentTokenIndex(), DateUtil.DD_MM_YYYY)));
        }
        return Optional.ofNullable(decimal);

    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    private void buildMetadata(LocalDate datePrelevement, Projet projet, String codePlateforme, SiteGLACPE site, String codeOutilsPrelevement, SortedMap<String, SortedMap<String, List<LineRecord>>> mapByTaxon, VersionFile versionFile, ErrorsReport errorsReport, List<String> listErrors)
            throws PersistenceException, InsertionDatabaseException {

        MetadataZooplancton metadataZooplancton = metadataZooplanctonDAO.getByNKey(datePrelevement, projet.getCode(), codePlateforme, codeOutilsPrelevement).orElse(null);

        LineRecord firstLine = mapByTaxon.values()
                .stream()
                .map(m -> m.values()
                .stream()
                .map(l -> l
                .stream()
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null);
        if (metadataZooplancton == null) {

            OutilsMesure outilsMesure = lookupOutilMesure(errorsReport, firstLine);
            OutilsMesure outilsPrelevement = lookupOutilPrelevement(errorsReport, firstLine);
            Plateforme plateforme = lookupPlateforme(codePlateforme, site, errorsReport);

            metadataZooplancton = new MetadataZooplancton();
            metadataZooplancton.setProjet(projetDAO.merge(projet));
            metadataZooplancton.setDate(datePrelevement);
            metadataZooplancton.setNomDeterminateur(firstLine.getDeterminateur());
            metadataZooplancton.setVolumeSedimente(firstLine.getVolumeSedimente());
            metadataZooplancton.setOutilsPrelevement(outilsPrelevement);
            metadataZooplancton.setOutilsMesure(outilsMesure);
            metadataZooplancton.setVersionFile(versionFileDAO.merge(versionFile));
            metadataZooplancton.setPlateforme(plateforme);
            metadataZooplancton.setProfondeurMax(firstLine.getProfondeurMax());
            metadataZooplancton.setProfondeurMin(firstLine.getProfondeurMin());

        }
        for (Iterator<Map.Entry<String, SortedMap<String, List<LineRecord>>>> iterator = mapByTaxon.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, SortedMap<String, List<LineRecord>>> taxonEntry = iterator.next();
            String taxonCode = taxonEntry.getKey();

            Taxon taxon = lookupTaxon(errorsReport, taxonCode);
            SortedMap<String, List<LineRecord>> mapByStade = taxonEntry.getValue();
            for (Iterator<Map.Entry<String, List<LineRecord>>> iterator1 = mapByStade.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<String, List<LineRecord>> stadeEntry = iterator1.next();
                String stadeCode = stadeEntry.getKey();
                StadeDeveloppement stadeDeveloppement = lookupStadeDeveloppement(errorsReport, stadeCode);
                List<LineRecord> linesRecord = stadeEntry.getValue();
                for (Iterator<LineRecord> iterator2 = linesRecord.iterator(); iterator2.hasNext();) {
                    LineRecord lineRecord = iterator2.next();

                    for (VariableValue variableValue : lineRecord.getVariablesValues()) {

                        DataZooplancton dataZooplancton = new DataZooplancton();
                        VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());
                        dataZooplancton.setRealNode(datatypeVariableUniteGLACPEDAO.mergeRealNode(variableValue.getRealNode()));
                        dataZooplancton.setTaxon(taxon);
                        dataZooplancton.setStadeDeveloppement(stadeDeveloppement);

                        if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                            dataZooplancton.setValue(null);
                        } else {
                            Float value = Float.parseFloat(variableValue.getValue());
                            dataZooplancton.setValue(value);
                        }

                        dataZooplancton.setMetadataZooplancton(metadataZooplancton);
                        metadataZooplancton.getDatas().add(dataZooplancton);
                    }
                }
            }

            iterator.remove();
            metadataZooplanctonDAO.saveOrUpdate(metadataZooplancton);
        }
    }

    private OutilsMesure lookupOutilPrelevement(ErrorsReport errorsReport, LineRecord firstLine) throws PersistenceException {
        String codeOutilsPrelevement = firstLine.getOutilsPrelevementCode();
        OutilsMesure outilsPrelevement = null;
        if (codeOutilsPrelevement != null && !codeOutilsPrelevement.equals(STRING_EMPTY)) {
            outilsPrelevement = outilsMesureDAO.getByCode(firstLine.getOutilsPrelevementCode()).orElse(null);
            if (outilsPrelevement == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), codeOutilsPrelevement)));
                throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), codeOutilsPrelevement));
            }
        }
        return outilsPrelevement;
    }

    private OutilsMesure lookupOutilMesure(ErrorsReport errorsReport, LineRecord firstLine) throws PersistenceException {
        OutilsMesure outilsMesure = null;
        String codeOutilMesure = firstLine.getOutilsMesureCode();
        if (codeOutilMesure != null && !codeOutilMesure.equals(STRING_EMPTY)) {
            outilsMesure = outilsMesureDAO.getByCode(codeOutilMesure).orElse(null);
            if (outilsMesure == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), codeOutilMesure)));
                throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), codeOutilMesure));
            }
        }
        return outilsMesure;
    }

    private Plateforme lookupPlateforme(String codePlateforme, SiteGLACPE site, ErrorsReport errorsReport) throws PersistenceException {
        Plateforme plateforme = plateformeDAO.getByNKey(codePlateforme, site.getCode()).orElse(null);
        if (plateforme == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS), codePlateforme)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS), codePlateforme));
        }
        return plateforme;
    }

    private StadeDeveloppement lookupStadeDeveloppement(ErrorsReport errorsReport, String codeStadeDeveloppement) throws PersistenceException {
        StadeDeveloppement stadeDeveloppement = stadeDeveloppementDAO.getByCode(codeStadeDeveloppement).orElse(null);
        if (stadeDeveloppement == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS), codeStadeDeveloppement)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_STADES_DEVELOPPEMENT_IN_REFERENCES_DATAS), codeStadeDeveloppement));
        }
        return stadeDeveloppement;
    }

    private Taxon lookupTaxon(ErrorsReport errorsReport, String codeTaxon) throws PersistenceException {
        Taxon taxon = taxonDAO.getByCode(codeTaxon).orElse(null);
        if (taxon == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS), codeTaxon)));
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), PATH_BUNDLE_SOURCE, PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS), codeTaxon));
        }
        return taxon;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param metadataZooplanctonDAO
     */
    public void setMetadataZooplanctonDAO(IMetadataZooplanctonDAO metadataZooplanctonDAO) {
        this.metadataZooplanctonDAO = metadataZooplanctonDAO;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    /**
     *
     * @param stadeDeveloppementDAO
     */
    public void setStadeDeveloppementDAO(IStadeDeveloppementDAO stadeDeveloppementDAO) {
        this.stadeDeveloppementDAO = stadeDeveloppementDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }

}

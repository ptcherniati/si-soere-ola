package org.inra.ecoinfo.glacpe.extraction.impl;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEAggregatedDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     * @param displaySeparator
     * @return
     */
    protected String buildCSVSeparator(Boolean displaySeparator) {
        String separator = "";
        if (displaySeparator) {
            separator = ";";
        }
        return separator;
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> buildVariablesAggregatedDatas(List<IGLACPEAggregateData> valeursMesures) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByVariablesAndDates(valeursMesures);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = new TreeMap<Projet,SortedMap<SiteGLACPE,SortedMap<Plateforme,SortedMap<LocalDate,SortedMap<DatatypeVariableUniteGLACPE,VariableAggregatedDatas>>>>>();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>>> projetEntry : valeursMesuresReorderedByDatesVariablesAndDates.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>> mapBySite = projetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>> siteEntry : mapBySite.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>> mapByPlatform = siteEntry.getValue();
                for (Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>> platformEntry : mapByPlatform.entrySet()) {
                    Plateforme platform = platformEntry.getKey();
                    SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>> mapBydate = platformEntry.getValue();
                    for (Map.Entry<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>> dateEntry : mapBydate.entrySet()) {
                        LocalDate dateKey = dateEntry.getKey();
                        SortedMap<RealNode, SortedSet<IGLACPEAggregateData>> mapByRealNode = dateEntry.getValue();
                        for (Map.Entry<RealNode, SortedSet<IGLACPEAggregateData>> realNodeEntry : mapByRealNode.entrySet()) {
                            RealNode realNode = realNodeEntry.getKey();
                            SortedSet<IGLACPEAggregateData> valeursMesureDates = realNodeEntry.getValue();
                            VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getName(), platform.getName(), ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable().getName(), ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable().getOrdreAffichageGroupe());
                            valeursMesureDates = sortValeursMesuresDatesByDepth(valeursMesureDates, (vm1, vm2) -> vm1.getDepth().compareTo(vm2.getDepth()));
                            fillVariableAggregatedDatas(variableAggregatedDatas, dateKey, valeursMesureDates);

                            variablesAggregatedDatas
                                    .computeIfAbsent(projet,k-> new TreeMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate,SortedMap<DatatypeVariableUniteGLACPE,VariableAggregatedDatas>>>>())
                                    .computeIfAbsent(site,k-> new TreeMap<Plateforme,SortedMap<LocalDate,SortedMap<DatatypeVariableUniteGLACPE,VariableAggregatedDatas>>>())
                                    .computeIfAbsent(platform,k-> new TreeMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>())
                                    .computeIfAbsent(dateKey,k-> new TreeMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>())
                                    .put((DatatypeVariableUniteGLACPE) realNode.getNodeable(), variableAggregatedDatas);
                        }
                    }
                }
            }
        }

        return variablesAggregatedDatas;

    }

    /**
     *
     * @param variablesAggregatedDatas
     */
    protected void sortVariablesAggregatedDatas(List<VariableAggregatedDatas> variablesAggregatedDatas) {
        Collections.sort(variablesAggregatedDatas, new Comparator<VariableAggregatedDatas>() {

            @Override
            public int compare(VariableAggregatedDatas o1, VariableAggregatedDatas o2) {
                String str1 = o1.getSiteName().concat(o1.getPlateformeName());
                String str2 = o2.getSiteName().concat(o2.getPlateformeName());
                return str1.compareTo(str2);
            }

        });
    }

    /**
     *
     * @param valeursMesureDates
     */
    protected SortedSet<IGLACPEAggregateData> sortValeursMesuresDatesByDepth(Set<IGLACPEAggregateData> valeursMesureDates, Comparator<IGLACPEAggregateData> comparator) {
        return valeursMesureDates
                .stream()
                .sorted(comparator)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     */
    protected void fillVariableAggregatedDatas(VariableAggregatedDatas variableAggregatedDatas, LocalDate dateKey, SortedSet<IGLACPEAggregateData> valeursMesureDates) {

        Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
        while (iterator.hasNext()) {
            IGLACPEAggregateData valeurMesure = iterator.next();
            if (valeurMesure.getValue() != null) {
                if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> variables = new LinkedList<ValueAggregatedData>();
                    variables.add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, variables);
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() > valeurMesure.getValue()) {
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (Math.abs(variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                }

                if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> variables = new LinkedList<ValueAggregatedData>();
                    variables.add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, variables);
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() < valeurMesure.getValue()) {
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                } else if (Math.abs(variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue()));
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                }
                iterator.remove();
            }
        }
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>>> buildValeursMesuresReorderedByVariablesAndDates(List<IGLACPEAggregateData> valeursMesures) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedBySitesProjetPlateformesVariablesDates = new TreeMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>>>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {
            valeursMesuresReorderedBySitesProjetPlateformesVariablesDates
                    .computeIfAbsent(valeurMesure.getProjet(),k-> new TreeMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>>())
                    .computeIfAbsent(valeurMesure.getSite(),k-> new TreeMap<Plateforme, SortedMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>>())
                    .computeIfAbsent(valeurMesure.getPlateforme(),k-> new TreeMap<LocalDate, SortedMap<RealNode, SortedSet<IGLACPEAggregateData>>>())
                    .computeIfAbsent(valeurMesure.getDate(),k-> new TreeMap<RealNode, SortedSet<IGLACPEAggregateData>>(
                            (r1,r2)->((DatatypeVariableUniteGLACPE)r1.getNodeable()).getVariable().compareTo(((DatatypeVariableUniteGLACPE)r2.getNodeable()).getVariable()))
                    )
                    .computeIfAbsent(valeurMesure.getRealNode(),k-> new TreeSet<IGLACPEAggregateData>())
                    .add(valeurMesure);
        }
        return valeursMesuresReorderedBySitesProjetPlateformesVariablesDates;
    }
}

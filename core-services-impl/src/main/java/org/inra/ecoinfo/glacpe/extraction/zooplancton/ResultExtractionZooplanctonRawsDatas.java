package org.inra.ecoinfo.glacpe.extraction.zooplancton;

import java.time.LocalDate;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
public class ResultExtractionZooplanctonRawsDatas {

    private String projet;
    private String site;
    private String plateforme;
    private LocalDate date;
    private String outilPrelevement;
    private String outilMesure;
    private Float profondeurMin;
    private Float profondeurMax;
    private String nomDeterminateur;
    private Float volumeSedimente;
    private String stadeDeveloppement;
    private Float value;
    private String nomLatinTaxon;
    private String nomVariable;
    private String theme;
    private String datatype;

    private String codeSandreElementVariable;
    private String codeSandreContextVariable;
    private String codeSandreElementSite;
    private String codeSandreContextSite;
    private String codeSandreElementPlatform;
    private String codeSandreContextPlatform;
    private Float longitudePlatform;
    private Float latitudePlatform;
    private Float altitudePlatform;
    private RealNode realNode;

    private String nameSupTaxon;
    private String codeSandreTaxon;
    private String codeSandreSupTaxon;
    private String nameLevelTaxon;
    private String codeSandreLevelTaxon;
    private String codeSandreLevelContextTaxon;

    private String codeSandreElementStadeDev;
    private String codeSandreContextStadeDev;

    /**
     *
     * @param projet
     * @param site
     * @param plateforme
     * @param date
     * @param outilPrelevement
     * @param outilMesure
     * @param profondeurMin
     * @param profondeurMax
     * @param nomDeterminateur
     * @param volumeSedimente
     * @param nomLatinTaxon
     * @param stadeDeveloppement
     * @param nomVariable
     * @param value
     * @param realNode
     * @param theme
     * @param codeSandreElementVariable
     * @param datatype
     * @param codeSandreContextVariable
     * @param codeSandreElementSite
     * @param codeSandreContextSite
     * @param codeSandreElementPlatform
     * @param codeSandreContextPlatform
     * @param longitudePlatform
     * @param latitudePlatform
     * @param altitudePlatform
     * @param nameSupTaxon
     * @param codeSandreTaxon
     * @param codeSandreSupTaxon
     * @param nameLevelTaxon
     * @param codeSandreLevelTaxon
     * @param codeSandreLevelContextTaxon
     * @param codeSandreElementStadeDev
     * @param codeSandreContextStadeDev
     */
    public ResultExtractionZooplanctonRawsDatas(
            String projet,
            String site,
            String plateforme,
            LocalDate date,
            String outilPrelevement,
            String outilMesure,
            Float profondeurMin,
            Float profondeurMax,
            String nomDeterminateur,
            Float volumeSedimente,
            String nomLatinTaxon,
            String stadeDeveloppement,
            String nomVariable,
            Float value,
            Long realNode,
            String theme,
            String datatype,
            String codeSandreElementVariable,
            String codeSandreContextVariable,
            String codeSandreElementSite,
            String codeSandreContextSite,
            String codeSandreElementPlatform,
            String codeSandreContextPlatform,
            Float longitudePlatform,
            Float latitudePlatform,
            Float altitudePlatform,
            String nameSupTaxon,
            String codeSandreTaxon,
            String codeSandreSupTaxon,
            String nameLevelTaxon,
            String codeSandreLevelTaxon,
            String codeSandreLevelContextTaxon,
            String codeSandreElementStadeDev,
            String codeSandreContextStadeDev
    ) {
        super();

        this.projet = projet;
        this.nomLatinTaxon = nomLatinTaxon;
        this.site = site;
        this.nomVariable = nomVariable;
        this.plateforme = plateforme;
        this.date = date;
        this.outilPrelevement = outilPrelevement;
        this.outilMesure = outilMesure;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.nomDeterminateur = nomDeterminateur;
        this.volumeSedimente = volumeSedimente;
        this.stadeDeveloppement = stadeDeveloppement;

        this.value = value;
        this.theme = theme;
        this.datatype = datatype;

        this.codeSandreElementVariable = codeSandreElementVariable;
        this.codeSandreContextVariable = codeSandreContextVariable;
        this.codeSandreElementSite = codeSandreElementSite;
        this.codeSandreContextSite = codeSandreContextSite;
        this.codeSandreElementPlatform = codeSandreElementPlatform;
        this.codeSandreContextPlatform = codeSandreContextPlatform;
        this.altitudePlatform = altitudePlatform;
        this.latitudePlatform = latitudePlatform;
        this.longitudePlatform = longitudePlatform;

        this.nameSupTaxon = nameSupTaxon;
        this.codeSandreTaxon = codeSandreTaxon;
        this.codeSandreSupTaxon = codeSandreSupTaxon;
        this.nameLevelTaxon = nameLevelTaxon;
        this.codeSandreLevelTaxon = codeSandreLevelTaxon;
        this.codeSandreLevelContextTaxon = codeSandreLevelContextTaxon;
        this.codeSandreElementStadeDev = codeSandreElementStadeDev;
        this.codeSandreContextStadeDev = codeSandreContextStadeDev;

    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return date;
    }

    /**
     *
     * @return
     */
    public String getProjet() {
        return projet;
    }

    /**
     *
     * @return
     */
    public String getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @return
     */
    public String getStadeDeveloppement() {
        return stadeDeveloppement;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(String projet) {
        this.projet = projet;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(String plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     *
     * @param stadeDeveloppement
     */
    public void setStadeDeveloppement(String stadeDeveloppement) {
        this.stadeDeveloppement = stadeDeveloppement;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreElementStadeDev() {
        return codeSandreElementStadeDev;
    }

    /**
     *
     * @param codeSandreElementStadeDev
     */
    public void setCodeSandreElementStadeDev(String codeSandreElementStadeDev) {
        this.codeSandreElementStadeDev = codeSandreElementStadeDev;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreContextStadeDev() {
        return codeSandreContextStadeDev;
    }

    /**
     *
     * @param codeSandreContextStadeDev
     */
    public void setCodeSandreContextStadeDev(String codeSandreContextStadeDev) {
        this.codeSandreContextStadeDev = codeSandreContextStadeDev;
    }

    /**
     *
     * @return
     */
    public String getSite() {
        return site;
    }

    /**
     *
     * @return
     */
    public String getOutilPrelevement() {
        return outilPrelevement;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @return
     */
    public String getNomDeterminateur() {
        return nomDeterminateur;
    }

    /**
     *
     * @return
     */
    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    /**
     *
     * @param site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     *
     * @param outilPrelevement
     */
    public void setOutilPrelevement(String outilPrelevement) {
        this.outilPrelevement = outilPrelevement;
    }

    /**
     *
     * @param profondeurMin
     */
    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    /**
     *
     * @param profondeurMax
     */
    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    /**
     *
     * @param nomDeterminateur
     */
    public void setNomDeterminateur(String nomDeterminateur) {
        this.nomDeterminateur = nomDeterminateur;
    }

    /**
     *
     * @param volumeSedimente
     */
    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    /**
     *
     * @return
     */
    public String getNomLatinTaxon() {
        return nomLatinTaxon;
    }

    /**
     *
     * @param nomLatinTaxon
     */
    public void setNomLatinTaxon(String nomLatinTaxon) {
        this.nomLatinTaxon = nomLatinTaxon;
    }

    /**
     *
     * @return
     */
    public String getNomVariable() {
        return nomVariable;
    }

    /**
     *
     * @param nomVariable
     */
    public void setNomVariable(String nomVariable) {
        this.nomVariable = nomVariable;
    }

    /**
     *
     * @return
     */
    public Float getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public String getTheme() {
        return theme;
    }

    /**
     *
     * @return
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     *
     * @param theme
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     *
     * @param datatype
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    /**
     *
     * @return
     */
    public String getOutilMesure() {
        return outilMesure;
    }

    /**
     *
     * @param outilMesure
     */
    public void setOutilMesure(String outilMesure) {
        this.outilMesure = outilMesure;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreElementVariable() {
        return codeSandreElementVariable;
    }

    /**
     *
     * @param codeSandreElementVariable
     */
    public void setCodeSandreElementVariable(String codeSandreElementVariable) {
        this.codeSandreElementVariable = codeSandreElementVariable;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreContextVariable() {
        return codeSandreContextVariable;
    }

    /**
     *
     * @param codeSandreContextVariable
     */
    public void setCodeSandreContextVariable(String codeSandreContextVariable) {
        this.codeSandreContextVariable = codeSandreContextVariable;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreElementSite() {
        return codeSandreElementSite;
    }

    /**
     *
     * @param codeSandreElementSite
     */
    public void setCodeSandreElementSite(String codeSandreElementSite) {
        this.codeSandreElementSite = codeSandreElementSite;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreContextSite() {
        return codeSandreContextSite;
    }

    /**
     *
     * @param codeSandreContextSite
     */
    public void setCodeSandreContextSite(String codeSandreContextSite) {
        this.codeSandreContextSite = codeSandreContextSite;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreElementPlatform() {
        return codeSandreElementPlatform;
    }

    /**
     *
     * @param codeSandreElementPlatform
     */
    public void setCodeSandreElementPlatform(String codeSandreElementPlatform) {
        this.codeSandreElementPlatform = codeSandreElementPlatform;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreContextPlatform() {
        return codeSandreContextPlatform;
    }

    /**
     *
     * @param codeSandreContextPlatform
     */
    public void setCodeSandreContextPlatform(String codeSandreContextPlatform) {
        this.codeSandreContextPlatform = codeSandreContextPlatform;
    }

    /**
     *
     * @return
     */
    public Float getLongitudePlatform() {
        return longitudePlatform;
    }

    /**
     *
     * @param longitudePlatform
     */
    public void setLongitudePlatform(Float longitudePlatform) {
        this.longitudePlatform = longitudePlatform;
    }

    /**
     *
     * @return
     */
    public Float getLatitudePlatform() {
        return latitudePlatform;
    }

    /**
     *
     * @param latitudePlatform
     */
    public void setLatitudePlatform(Float latitudePlatform) {
        this.latitudePlatform = latitudePlatform;
    }

    /**
     *
     * @return
     */
    public Float getAltitudePlatform() {
        return altitudePlatform;
    }

    /**
     *
     * @param altitudePlatform
     */
    public void setAltitudePlatform(Float altitudePlatform) {
        this.altitudePlatform = altitudePlatform;
    }

    /**
     *
     * @return
     */
    public String getNameSupTaxon() {
        return nameSupTaxon;
    }

    /**
     *
     * @param nameSupTaxon
     */
    public void setNameSupTaxon(String nameSupTaxon) {
        this.nameSupTaxon = nameSupTaxon;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreTaxon() {
        return codeSandreTaxon;
    }

    /**
     *
     * @param codeSandreTaxon
     */
    public void setCodeSandreTaxon(String codeSandreTaxon) {
        this.codeSandreTaxon = codeSandreTaxon;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreSupTaxon() {
        return codeSandreSupTaxon;
    }

    /**
     *
     * @param codeSandreSupTaxon
     */
    public void setCodeSandreSupTaxon(String codeSandreSupTaxon) {
        this.codeSandreSupTaxon = codeSandreSupTaxon;
    }

    /**
     *
     * @return
     */
    public String getNameLevelTaxon() {
        return nameLevelTaxon;
    }

    /**
     *
     * @param nameLevelTaxon
     */
    public void setNameLevelTaxon(String nameLevelTaxon) {
        this.nameLevelTaxon = nameLevelTaxon;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreLevelTaxon() {
        return codeSandreLevelTaxon;
    }

    /**
     *
     * @param codeSandreLevelTaxon
     */
    public void setCodeSandreLevelTaxon(String codeSandreLevelTaxon) {
        this.codeSandreLevelTaxon = codeSandreLevelTaxon;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreLevelContextTaxon() {
        return codeSandreLevelContextTaxon;
    }

    /**
     *
     * @param codeSandreLevelContextTaxon
     */
    public void setCodeSandreLevelContextTaxon(String codeSandreLevelContextTaxon) {
        this.codeSandreLevelContextTaxon = codeSandreLevelContextTaxon;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }
}

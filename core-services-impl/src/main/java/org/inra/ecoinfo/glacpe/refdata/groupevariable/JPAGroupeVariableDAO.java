/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPAGroupeVariableDAO extends AbstractJPADAO<GroupeVariable> implements IGroupeVariableDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<GroupeVariable> getByCode(String code) {
        CriteriaQuery<GroupeVariable> query = builder.createQuery(GroupeVariable.class);
        Root<GroupeVariable> GroupeVariable = query.from(GroupeVariable.class);
        query
                .select(GroupeVariable)
                .where(builder.equal(GroupeVariable.get(GroupeVariable_.code), code));
        return getOptional(query);
    }

}

package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.ISequencePhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SequencePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SousSequencePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.dataset.phytoplancton.messages";
    private static final String MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS";
    private static final String MSG_MISSING_TAXON_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_TAXON_IN_REFERENCES_DATAS";
    private static final String VARIABLE_PROFONDEUR_MINIMUM = "profondeur_minimum";
    private static final String VARIABLE_PROFONDEUR_MAXIMUM = "profondeur_maximum";

    /**
     *
     */
    protected ISequencePhytoplanctonDAO sequencePhytoplanctonDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;
    protected String datatypeCode;

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequencePhytoplanctonDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());

        datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datasetDescriptor)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        try {

            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();
            // Saute la 1ere ligne
            parser.getLine();
            SortedMap<LocalDate, SortedMap<String, SortedMap<String, List<LineRecord>>>> sequencesMapLines = new TreeMap<>();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                if (!Utils.createCodeFromString(projetCode).equals(projetCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), projetCodeFromVersion, lineCount, 1, projetCode)));
                }
                String nomSite = cleanerValues.nextToken();
                if (!Utils.createCodeFromString(nomSite).equals(siteCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, lineCount, 2, nomSite)));
                }
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                LocalDate datePrelevement = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);

                //Traitement de la profondeur Min
                Float profondeurMin = readDepth(cleanerValues, "profondeur_minimum", errorsReport, controlesCoherenceMap, lineCount, 5);

                //Traitement de la profondeur Max
                Float profondeurMax = readDepth(cleanerValues, "profondeur_minimum", errorsReport, controlesCoherenceMap, lineCount, 6);

                //Comparaison profondeurs Min et Max
                if (profondeurMin != null && profondeurMax != null && profondeurMax < profondeurMin) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH_INTERVAL), profondeurMin, profondeurMax, lineCount, 5, 6)));
                }

                String outilPrelevementCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String determinateur = cleanerValues.nextToken();
                Float volumeSedimente = readFloat(errorsReport, lineCount, cleanerValues, datasetDescriptor).orElse(null);
                Float surfaceComptage = readFloat(errorsReport, lineCount, cleanerValues, datasetDescriptor).orElse(null);
                String nomTaxon = Utils.createCodeFromString(cleanerValues.nextToken());

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        RealNode realNode = Optional
                                .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                                .map(c -> Utils.createCodeFromString(c.getName()))
                                .map(n -> realNodes.get(n))
                                .map(v -> v.get(0))
                                .orElse(null);
                        if (realNode != null) {
                            VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                            if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                            }
                            variablesValues.add(new VariableValue(realNode, values[actualVariableArray].replaceAll(" ", "")));
                        } else {
                            errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)));

                        }
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, profondeurMin, profondeurMax, outilMesureCode, outilPrelevementCode, determinateur, volumeSedimente, surfaceComptage, nomTaxon, variablesValues, lineCount,
                        projetCode);
                sequencesMapLines
                        .computeIfAbsent(datePrelevement, k -> new TreeMap<>())
                        .computeIfAbsent(plateformeCode, k -> new TreeMap<>())
                        .computeIfAbsent(nomTaxon, k -> new LinkedList<>())
                        .add(line);

            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
            Allocator allocator = Allocator.getInstance();
            List<String> listErrors = new LinkedList<>();
            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<LocalDate, SortedMap<String, SortedMap<String, List<LineRecord>>>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {

                final Entry<LocalDate, SortedMap<String, SortedMap<String, List<LineRecord>>>> entry = iterator.next();
                LocalDate datePrelevement = entry.getKey();
                SortedMap<String, SortedMap<String, List<LineRecord>>> sequenceLines = entry.getValue();
                if (!sequenceLines.isEmpty()) {
                    try {
                        buildSequence(realNodes, datePrelevement, projetCodeFromVersion, siteCodeFromVersion, sequenceLines, versionFile, errorsReport, listErrors);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequencePhytoplanctonDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);

                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private Float readDepth(CleanerValues cleanerValues, String variableCode, ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, int columnsCount) {
        String profondeurString = cleanerValues.nextToken();
        Float value = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
        if (value != null) {
            try {
                value = Float.parseFloat(profondeurString);

                if (!controlesCoherenceMap.isEmpty() && controlesCoherenceMap.get(variableCode) != null) {
                    testValueCoherence(
                            value,
                            controlesCoherenceMap.get(variableCode).getValeurMin(),
                            controlesCoherenceMap.get(variableCode).getValeurMax(),
                            lineCount,
                            columnsCount);
                }
            } catch (BadExpectedValueException e) {
                errorsReport.addException(e);

            } catch (Exception e) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, columnsCount)));
            }
        }
        return value;
    }

    private String readValue(ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, String variableCode, String value, CleanerValues cleanerValues) {
        try {
            value = cleanerValues.nextToken();
            if (controlesCoherenceMap.get(variableCode) != null && value != null && !value.trim().isEmpty()) {
                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variableCode).getValeurMin(), controlesCoherenceMap.get(variableCode).getValeurMax(), lineCount, 13);
            }
        } catch (NumberFormatException e) {
            errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 13, value)));
        } catch (BadExpectedValueException e) {
            errorsReport.addException(e);
        }
        return value;
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private Optional<Float> readFloat(ErrorsReport errorsReport, long lineCount, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String stringDecimal;
        Float decimal = null;
        stringDecimal = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            decimal = Strings.isNullOrEmpty(stringDecimal) ? null
                    : Float.parseFloat(stringDecimal);
        } catch (NumberFormatException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), stringDecimal, lineCount, cleanerValues.currentTokenIndex(), DateUtil.DD_MM_YYYY)));
        }
        return Optional.ofNullable(decimal);

    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    private void buildSequence(Map<String, List<RealNode>> realNodes, LocalDate datePrelevement, String projetCode, String siteCode, SortedMap<String, SortedMap<String, List<LineRecord>>> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport, List<String> listErrors) throws PersistenceException,
            InsertionDatabaseException {

        LineRecord firstLine = sequenceLines.values()
                .stream()
                .map(m -> m.values()
                .stream()
                .map(l -> l
                .stream()
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null);
        ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode).orElse(null);

        if (projetSite == null) {
            errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode)));
            throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode));
        }

        SequencePhytoplancton sequencePhytoplancton = new SequencePhytoplancton();
        sequencePhytoplancton.setDatePrelevement(datePrelevement);
        sequencePhytoplancton.setNomDeterminateur(firstLine.getDeterminateur());
        sequencePhytoplancton.setVolumeSedimente(firstLine.getVolumeSedimente());

        sequencePhytoplancton.setVersionFile(versionFile);
        for (Iterator<Entry<String, SortedMap<String, List<LineRecord>>>> iterator = sequenceLines.entrySet().iterator(); iterator.hasNext();) {
            Entry<String, SortedMap<String, List<LineRecord>>> entry = iterator.next();
            String plateformName = entry.getKey();
            SortedMap<String, List<LineRecord>> sousSequenceLines = entry.getValue();
            firstLine = sousSequenceLines.values()
                    .stream()
                    .map(l -> l
                    .stream()
                    .filter(li -> li != null)
                    .findAny()
                    .orElse(null)
                    )
                    .filter(li -> li != null)
                    .findAny()
                    .orElse(null);
            try {
                    buildSousSequence(firstLine, realNodes, siteCode, plateformName, firstLine.getProfondeurMin(), firstLine.getProfondeurMax(), firstLine.getSurfaceComptage(), firstLine.getOutilsMesureCode(), firstLine.getOutilsPrelevementCode(), sousSequenceLines, sequencePhytoplancton, errorsReport, listErrors);

            } catch (InsertionDatabaseException |PersistenceException e) {
                errorsReport.addException(e);
            } finally {
                iterator.remove();
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequencePhytoplanctonDAO.saveOrUpdate(sequencePhytoplancton);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getUTCDateTextFromLocalDateTime(datePrelevement, DateUtil.DD_MM_YYYY));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(LineRecord firstLine, Map<String, List<RealNode>> realNodes, String siteName, String plateformeCode, Float profondeurMin, Float profondeurMax, Float surfaceComptage, String outilMesureCode, String outilPrelevementCode, SortedMap<String, List<LineRecord>> sousSequenceLines,
            SequencePhytoplancton sequencePhytoplancton, ErrorsReport errorsReport, List<String> listErrors) throws PersistenceException, InsertionDatabaseException {

        Plateforme plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(siteName)).orElse(null);
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        OutilsMesure outilsMesure = null;
        if (outilMesureCode != "") {
            outilsMesure = outilsMesureDAO.getByCode(outilMesureCode).orElse(null);
            if (outilsMesure == null || !outilsMesure.getTypeOutilsMesure().getType().getValeur().equals("mesure")) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilMesureCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilMesureCode));
            }
        }

        OutilsMesure outilsPrelevement = null;
        if (!Strings.isNullOrEmpty(outilPrelevementCode)) {
            outilsPrelevement = outilsMesureDAO.getByCode(outilPrelevementCode).orElse(null);
            if (outilsPrelevement == null || !outilsPrelevement.getTypeOutilsMesure().getType().getValeur().equals("prelevement")) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), outilPrelevementCode)));
                throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_OUTILS_PRELEVEMENT_IN_REFERENCES_DATAS), outilPrelevementCode));
            }
        }

        SousSequencePhytoplancton sousSequencePhytoplancton = new SousSequencePhytoplancton();

        sousSequencePhytoplancton.setPlateforme(plateforme);
        sousSequencePhytoplancton.setOutilsPrelevement(outilsPrelevement);
        sousSequencePhytoplancton.setOutilsMesure(outilsMesure);
        sousSequencePhytoplancton.setProfondeurMin(profondeurMin);
        sousSequencePhytoplancton.setProfondeurMax(profondeurMax);
        sousSequencePhytoplancton.setSurfaceComptage(surfaceComptage);
        sousSequencePhytoplancton.setSequence(sequencePhytoplancton);
        sequencePhytoplancton.getSousSequences().add(sousSequencePhytoplancton);

        Map<String, List<LineRecord>> taxonsLinesMap = new HashMap<String, List<LineRecord>>();
        for (Iterator<Entry<String, List<LineRecord>>> iterator = sousSequenceLines.entrySet().iterator(); iterator.hasNext();) {
            Entry<String, List<LineRecord>> entry = iterator.next();
            String taxonName = entry.getKey();
            List<LineRecord> taxonLines = entry.getValue();
            try {
                buildMesure(realNodes, siteName, taxonName, taxonLines, sousSequencePhytoplancton, errorsReport, listErrors);
            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(Map<String, List<RealNode>> realNodes, String siteName, String taxonCode, List<LineRecord> taxonLines, SousSequencePhytoplancton sousSequencePhytoplancton, ErrorsReport errorsReport, List<String> listErrors) throws PersistenceException, InsertionDatabaseException {
        MesurePhytoplancton mesurePhytoplancton = null;
        Taxon taxon = taxonDAO.getByCode(taxonCode).orElse(null);

        try {

            if (taxon == null) {
                if (!listErrors.contains(taxonCode)) {
                    listErrors.add(taxonCode);
                }
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_PHYTOPLANCTON, MSG_MISSING_TAXON_IN_REFERENCES_DATAS), taxonCode)));
            } else {
                mesurePhytoplancton = new MesurePhytoplancton();
                mesurePhytoplancton.setSousSequence(sousSequencePhytoplancton);
                mesurePhytoplancton.setTaxon(taxon);
                sousSequencePhytoplancton.getMesures().add(mesurePhytoplancton);
                mesurePhytoplancton.setLigneFichierEchange(taxonLines.get(0).getOriginalLineNumber());

                for (LineRecord taxonLine : taxonLines) {
                    for (VariableValue variableValue : taxonLine.getVariablesValues()) {
                        RealNode realNode = variableValue.getRealNode();

                        ValeurMesurePhytoplancton valeurPhytoplancton = new ValeurMesurePhytoplancton();

                        valeurPhytoplancton.setRealNode(datatypeVariableUniteGLACPEDAO.mergeRealNode(realNode));

                        if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                            valeurPhytoplancton.setValeur(null);
                        } else {
                            Float value = Float.parseFloat(variableValue.getValue());
                            valeurPhytoplancton.setValeur(value);
                        }

                        valeurPhytoplancton.setMesure(mesurePhytoplancton);
                        mesurePhytoplancton.getValeurs().add(valeurPhytoplancton);
                    }
                }
            }
        } catch (Exception e) {
            errorsReport.addException(e);
        }
    }

    /**
     *
     * @param sequencePhytoplanctonDAO
     */
    public void setSequencePhytoplanctonDAO(ISequencePhytoplanctonDAO sequencePhytoplanctonDAO) {
        this.sequencePhytoplanctonDAO = sequencePhytoplanctonDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }
}

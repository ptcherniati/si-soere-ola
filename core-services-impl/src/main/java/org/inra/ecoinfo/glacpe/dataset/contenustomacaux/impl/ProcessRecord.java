package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.ISequenceContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.MesureContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SousSequenceContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.ValeurMesureContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX = "org.inra.ecoinfo.glacpe.dataset.contenustomacaux.messages";
    private static final String MSG_NO_LIST = "PROPERTY_MSG_NO_LIST";//$NON-NLS-1$

    /**
     *
     */
    protected ISequenceContenuStomacauxDAO sequenceContenuStomacauxDAO;

    /**
     *
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequenceContenuStomacauxDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, String datatypeCode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        String datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datatypeCode, datasetDescriptor)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();

        try {

            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();

            String[] values = null;
            long lineCount = 1;
            // Saute la 1ere ligne
            parser.getLine();
            SortedMap<LocalDate, SortedMap<String, SortedMap<Integer, List<LineRecord>>>> sequencesMapLines = new TreeMap<>();

            Map<String, List<ValeurQualitative>> referenceMap = buildReferenceMap();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());

                if (!projetCode.equals(projetCodeFromVersion)) {
                    InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), cleanerValues.currentTokenIndex(), projetCodeFromVersion, lineCount, projetCode));
                    throw insertionDatabaseException;
                }
                String nomSite = cleanerValues.nextToken();

                if (!nomSite.equals(siteCodeFromVersion)) {
                    InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, cleanerValues.currentTokenIndex(), lineCount, nomSite));
                    throw insertionDatabaseException;
                }
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String datePrelevementString = cleanerValues.nextToken();
                LocalDate date = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);
                String nomPecheur = cleanerValues.nextToken();
                String nomEspece = cleanerValues.nextToken();
                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String maille = cleanerValues.nextToken();
                String profondeurString = cleanerValues.nextToken();
                Float profondeur = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : 0.0f;
                // il est possible d'ajouter un contrôle de cohérence pour les profondeurs comme ce qui a été fait pour les autres types de données
                String dureeString = cleanerValues.nextToken();
                Float duree = dureeString.length() > 0 ? Float.parseFloat(dureeString) : 0.0f;
                String utilisationComptage = cleanerValues.nextToken();
                String numeroPoissonString = cleanerValues.nextToken();
                Integer numeroPoisson = numeroPoissonString.length() > 0 ? Integer.parseInt(numeroPoissonString) : 0;
                String taillePoissonString = cleanerValues.nextToken();
                Float taillePoisson = taillePoissonString.length() > 0 ? Float.parseFloat(taillePoissonString) : 0.0f;
                String malformation = cleanerValues.nextToken();
                String sexe = cleanerValues.nextToken();
                String maturite = cleanerValues.nextToken();
                String estomacVide = cleanerValues.nextToken();
                String poidsString = cleanerValues.nextToken();
                Float poids = poidsString.length() > 0 ? Float.parseFloat(poidsString) : 0.0f;
                String poissonCompte = cleanerValues.nextToken();
                String numeroPilulierString = cleanerValues.nextToken();
                Integer numeroPilulier = numeroPilulierString.length() > 0 ? Integer.parseInt(numeroPilulierString) : 0;
                String numeroEcailleString = cleanerValues.nextToken();
                Integer numeroEcaille = numeroPilulierString.length() > 0 ? Integer.parseInt(numeroEcailleString) : 0;
                String etatEstomac = cleanerValues.nextToken();
                String commentaires = cleanerValues.nextToken();

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        RealNode realNode = Optional
                                .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                                .map(c -> Utils.createCodeFromString(c.getName()))
                                .map(n -> realNodes.get(n))
                                .map(v -> v.get(0))
                                .orElse(null);
                        if (realNode != null) {

                            VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                            if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                            }
                            variablesValues.add(new VariableValue(realNode, values[actualVariableArray].replaceAll(" ", "")));
                        } else {
                            errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)));

                        }
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, date, outilMesureCode, nomPecheur, nomEspece, maille, duree, utilisationComptage, poissonCompte, numeroPoisson, taillePoisson, malformation, sexe, maturite, estomacVide, poids, etatEstomac, numeroPilulier, numeroEcaille, commentaires, profondeur, variablesValues, lineCount, projetCode);
                sequencesMapLines
                        .computeIfAbsent(date, k -> new TreeMap<>())
                        .computeIfAbsent(String.format("%s@%s", plateformeCode, outilMesureCode), k -> new TreeMap<>())
                        .computeIfAbsent(numeroPoisson, k -> new LinkedList<>())
                        .add(line);
                int count = 0;
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
            for (Iterator<Map.Entry<LocalDate, SortedMap<String, SortedMap<Integer, List<LineRecord>>>>> iterator = sequencesMapLines.entrySet().iterator(); iterator.hasNext();) {
                Map.Entry<LocalDate, SortedMap<String, SortedMap<Integer, List<LineRecord>>>> dateEntry = iterator.next();
                LocalDate datePrelevement = dateEntry.getKey();
                SortedMap<String, SortedMap<Integer, List<LineRecord>>> mapByPlatform = dateEntry.getValue();
                try {
                    LineRecord firstLine = mapByPlatform.values()
                            .stream()
                            .map(
                                    l -> l
                                            .values()
                                            .stream()
                                            .filter(li -> li != null)
                                            .findAny()
                                            .orElse(null)
                            )
                            .filter(li -> li != null)
                            .findAny()
                            .orElse(null)
                            .stream()
                            .filter(li -> li != null)
                            .findAny()
                            .orElse(new LineRecord());
                    String nomPecheur = firstLine.getNomPecheur();
                    Float profondeur = firstLine.getProfondeur();
                    Float duree = firstLine.getDuree();
                    buildSequence(datePrelevement, firstLine.getProjetCode(), firstLine.getNomSite(), nomPecheur, profondeur, duree, mapByPlatform, versionFile, referenceMap, errorsReport);

                    // Très important à cause de problèmes de performances
                    sequenceContenuStomacauxDAO.flush();
                    versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                } catch (InsertionDatabaseException e) {
                    errorsReport.addException(e);
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }
    }

    private Map<String, List<ValeurQualitative>> buildReferenceMap() throws PersistenceException {
        Map<String, List<ValeurQualitative>> referenceMap = new HashMap<String, List<ValeurQualitative>>();

        List<ValeurQualitative> vqMaille = valeurQualitativeDAO.getByCode(SousSequenceContenuStomacaux.MAILLE_FILET);
        if (vqMaille == null || vqMaille.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "maille"));
        } else {
            referenceMap.put(SousSequenceContenuStomacaux.MAILLE_FILET, vqMaille);
        }
        List<ValeurQualitative> vqSexe = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.SEXE_POISSON);
        if (vqSexe == null || vqSexe.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "sexe"));
        } else {
            referenceMap.put(MesureContenuStomacaux.SEXE_POISSON, vqSexe);
        }
        List<ValeurQualitative> vqMaturite = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.SEXUALITE_POISSON);
        if (vqMaturite == null || vqMaturite.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "maturite"));
        } else {
            referenceMap.put(MesureContenuStomacaux.SEXUALITE_POISSON, vqMaturite);
        }
        List<ValeurQualitative> vqMalformation = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.MALFORMATION_POISSON);
        if (vqMalformation == null || vqMalformation.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "malformation"));
        } else {
            referenceMap.put(MesureContenuStomacaux.MALFORMATION_POISSON, vqMalformation);
        }
        List<ValeurQualitative> vqEtatEstomac = valeurQualitativeDAO.getByCode(MesureContenuStomacaux.ETAT_ESTOMAC_POISSON);
        if (vqEtatEstomac == null || vqEtatEstomac.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "etat_estomac"));
        } else {
            referenceMap.put(MesureContenuStomacaux.ETAT_ESTOMAC_POISSON, vqEtatEstomac);
        }
        List<ValeurQualitative> vqNomItem = valeurQualitativeDAO.getByCode(ValeurMesureContenuStomacaux.PROIE);
        if (vqNomItem == null || vqNomItem.isEmpty()) {
            throw new PersistenceException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CONTENUSTOMACAUX, MSG_NO_LIST), "proie"));
        } else {
            referenceMap.put(ValeurMesureContenuStomacaux.PROIE, vqNomItem);
        }
        return referenceMap;
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private void buildSequence(LocalDate datePrelevement, String projetCode, String siteCode, String nomPecheur, Float profondeur, Float duree, SortedMap<String, SortedMap<Integer, List<LineRecord>>> poissonsLinesMap, VersionFile versionFile, Map<String, List<ValeurQualitative>> referenceMap,
            ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        SequenceContenuStomacaux sequence = sequenceContenuStomacauxDAO.getByDatePrelevementAndProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode).orElse(null);

        if (sequence == null) {

            ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode).orElse(null);

            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode));
            }

            sequence = new SequenceContenuStomacaux();

            sequence.setDatePrelevement(datePrelevement);
            sequence.setNomPecheur(nomPecheur);
            sequence.setDuree(duree);
            sequence.setProfondeur(profondeur);

            sequence.setVersionFile(versionFile);
        }

        Map<String, List<LineRecord>> sousSequencesMap = new HashMap<String, List<LineRecord>>();
        for (Iterator<Map.Entry<String, SortedMap<Integer, List<LineRecord>>>> iterator = poissonsLinesMap.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, SortedMap<Integer, List<LineRecord>>> entryByPlatform = iterator.next();
            String platformeOutilCode = entryByPlatform.getKey();
            String[] plateformeOutil = platformeOutilCode.split("@");
            String plateformeCode = plateformeOutil[0];
            String codeOutil = plateformeOutil[1];
            SortedMap<Integer, List<LineRecord>> mapByFish = entryByPlatform.getValue();
            buildSousSequence(plateformeCode, codeOutil, errorsReport, sequence, referenceMap, mapByFish);
            iterator.remove();
        }
        try {
            sequenceContenuStomacauxDAO.saveOrUpdate(sequence);
        } catch (ConstraintViolationException e) {
            String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getUTCDateTextFromLocalDateTime(datePrelevement, DateUtil.DD_MM_YYYY), siteCode);
            errorsReport.addException(new BusinessException(message));
        }
    }

    protected void buildSousSequence(String plateformeCode, String codeOutil, ErrorsReport errorsReport, SequenceContenuStomacaux sequence, Map<String, List<ValeurQualitative>> referenceMap, SortedMap<Integer, List<LineRecord>> mapByFish) throws NumberFormatException, PersistenceException {
        try {
            Plateforme plateforme = null;

            LineRecord firstLine = mapByFish.values()
                    .stream()
                    .filter(li -> li != null)
                    .findAny()
                    .orElse(null)
                    .stream()
                    .filter(li -> li != null)
                    .findAny()
                    .orElse(new LineRecord());

            plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(firstLine.getNomSite())).orElse(null);
            if (plateforme == null) {
                InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), firstLine.getPlateformeCode(), firstLine.getOriginalLineNumber()));
                throw insertionDatabaseException;
            }

            OutilsMesure outilsMesure = outilsMesureDAO.getByCode(codeOutil).orElse(null);
            if (outilsMesure == null) {
                errorsReport.addException((new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), codeOutil))));
                throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), codeOutil));
            }

            SousSequenceContenuStomacaux sousSequence = new SousSequenceContenuStomacaux();
            String nomEspece = firstLine.getNomEspece();
            // penser aux vérification de valeurQualitatives sur le nom de l'espèce et la maille
            sousSequence.setPlateforme(plateforme);
            sousSequence.setOutilsMesure(outilsMesure);
            sousSequence.setSequence(sequence);
            sousSequence.setEspece(nomEspece);

            if (sousSequence.getEspece() == null) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), nomEspece, "espèce")));
            }
            String maille = firstLine.getMaille();
            for (ValeurQualitative vqm : referenceMap.get(SousSequenceContenuStomacaux.MAILLE_FILET)) {
                if (maille.equals(vqm.getValeur())) {
                    sousSequence.setMaille(vqm);
                    break;
                }
            }
            if (sousSequence.getMaille() == null) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), maille, "maille")));
            }
            sequence.getSousSequence().add(sousSequence);
            for (Iterator<Map.Entry<Integer, List<LineRecord>>> iterator1 = mapByFish.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<Integer, List<LineRecord>> entryByFish = iterator1.next();
                Integer numeroPoisson = entryByFish.getKey();
                List<LineRecord> poissonsLines = entryByFish.getValue();
                try {
                    buildMesure(
                            numeroPoisson,
                            firstLine.getSexe(),
                            firstLine.getMaturite(),
                            firstLine.getMalformation(),
                            firstLine.getPoids(),
                            firstLine.getEstomacVide(),
                            firstLine.getEtatEstomac(),
                            firstLine.getPoissonCompte(),
                            firstLine.getUtilisationComptage(),
                            firstLine.getTaillePoisson(),
                            firstLine.getNumeroPilulier(),
                            firstLine.getNumeroEcaille(),
                            firstLine.getCommentaires(),
                            poissonsLines,
                            sousSequence, referenceMap, errorsReport);

                } catch (InsertionDatabaseException e) {
                    errorsReport.addException(e);
                }
            }

        } catch (InsertionDatabaseException e) {
            errorsReport.addException(e);
        }
    }

    private void buildMesure(Integer numeroPoisson, String sexe, String maturite, String malformation, Float poids, String estomacVide, String etatEstomac, String poissonCompte, String utilisationComptage, Float taillePoisson,
            Integer numeroPilulier, Integer numeroEcaille, String commentaires, List<LineRecord> poissonsLines, SousSequenceContenuStomacaux sousSequence, Map<String, List<ValeurQualitative>> referenceMap, ErrorsReport errorsReport)
            throws PersistenceException, InsertionDatabaseException {
        LineRecord firstLine = poissonsLines.get(0);
        MesureContenuStomacaux mesure = new MesureContenuStomacaux();
        mesure.setSousSequence(sousSequence);
        sousSequence.getMesures().add(mesure);
        mesure.setLigneFichierEchange(firstLine.getOriginalLineNumber());
        mesure.setNumeroPoisson(numeroPoisson);

        // faire des vérifications sur les valeurs qualitatives
        for (ValeurQualitative vqs : referenceMap.get(MesureContenuStomacaux.SEXE_POISSON)) {
            if (sexe.equals(vqs.getValeur())) {
                mesure.setSexePoisson(vqs);
                break;
            }
        }
        if (mesure.getSexePoisson() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), sexe, MesureContenuStomacaux.SEXE_POISSON)));
        }

        for (ValeurQualitative vqm : referenceMap.get(MesureContenuStomacaux.SEXUALITE_POISSON)) {
            if (maturite.equals(vqm.getValeur())) {
                mesure.setMaturitePoisson(vqm);
                break;
            }
        }
        if (mesure.getMaturitePoisson() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), maturite, MesureContenuStomacaux.SEXUALITE_POISSON)));
        }

        for (ValeurQualitative vqm : referenceMap.get(MesureContenuStomacaux.MALFORMATION_POISSON)) {
            if (malformation.equals(vqm.getValeur())) {
                mesure.setMalformation(vqm);
                break;
            }
        }
        if (mesure.getMalformation() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), malformation, MesureContenuStomacaux.MALFORMATION_POISSON)));
        }

        for (ValeurQualitative vqe : referenceMap.get(MesureContenuStomacaux.ETAT_ESTOMAC_POISSON)) {
            if (etatEstomac.equals(vqe.getValeur())) {
                mesure.setEtatEstomac(vqe);
                break;
            }
        }
        if (mesure.getEstomacVide() == null) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), etatEstomac, MesureContenuStomacaux.ETAT_ESTOMAC_POISSON)));
        }
        mesure.setPoids(poids);
        mesure.setLongueurPoisson(taillePoisson);
        mesure.setNumeroPilulier(numeroPilulier);
        mesure.setNumeroEcaille(numeroEcaille);
        mesure.setCommentaire(commentaires);

        if (estomacVide.equals("Oui") || estomacVide.equals("oui") || estomacVide.equals("OUI")) {
            mesure.setEstomacVide(true);
        } else {
            mesure.setEstomacVide(false);
        }
        if (poissonCompte.equals("Oui") || poissonCompte.equals("oui") || poissonCompte.equals("OUI")) {
            mesure.setPoissonCompte(true);
        } else {
            mesure.setPoissonCompte(false);
        }
        if (utilisationComptage.equals("Oui") || utilisationComptage.equals("oui") || utilisationComptage.equals("OUI")) {
            mesure.setUtilisationComptage(true);
        } else {
            mesure.setUtilisationComptage(false);
        }

        for (LineRecord poissonLine : poissonsLines) {
            for (VariableValue variableValue : poissonLine.getVariablesValues()) {
                ValeurMesureContenuStomacaux valeurMesure = new ValeurMesureContenuStomacaux();

                // vérification de la valeur qualitative de type "variable"
                for (ValeurQualitative vqn : referenceMap.get(ValeurMesureContenuStomacaux.PROIE)) {
                    if (((DatatypeVariableUniteGLACPE) variableValue.getRealNode().getNodeable()).getVariable().getCode().equals(vqn.getCode())) {
                        valeurMesure.setProie(vqn);
                        break;
                    }
                }
                if (valeurMesure.getProie() == null) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_NOT_IN_LIST), etatEstomac, ValeurMesureContenuStomacaux.PROIE)));
                }
                if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                    valeurMesure.setValeur(null);
                } else {
                    Float value = Float.parseFloat(variableValue.getValue());
                    valeurMesure.setValeur(value);
                }
                valeurMesure.setMesure(mesure);
                mesure.getValeurs().add(valeurMesure);
            }
        }
    }

    /**
     *
     * @param sequenceContenuStomacauxDAO
     */
    public void setSequenceContenuStomacauxDAO(ISequenceContenuStomacauxDAO sequenceContenuStomacauxDAO) {
        this.sequenceContenuStomacauxDAO = sequenceContenuStomacauxDAO;
    }

    /**
     *
     * @param valeurQualitativeDAO
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }
}

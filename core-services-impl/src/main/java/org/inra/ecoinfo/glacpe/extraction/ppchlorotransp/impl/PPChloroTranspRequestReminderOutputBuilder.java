package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGlacpeRequestReminder;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public class PPChloroTranspRequestReminderOutputBuilder extends AbstractGlacpeRequestReminder {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "ppRequestReminder";
    
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";
    
    private static final String MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";
    
    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";
    
    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    private static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";
    private static final String PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY = "%s\n   %s";
    private static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";
    
    private static final String MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";
    private static final String MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";
    private static final String MSG_SITE = "PROPERTY_MSG_SITE";
    private static final String MSG_SELECTED_PROJECT = "PROPERTY_MSG_SELECTED_PROJECT";
    private static final String MSG_SELECTED_PLATEFORMS = "PROPERTY_MSG_SELECTED_PLATEFORMS";
    
    private static final String KEYMAP_COMMENTS = "comments";
    
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_HEADER);
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        
        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;
        
        try {
            
            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();
            
            printPlateformesSummary(requestMetadatasMap, reminderPrintStream);
            List<DatatypeVariableUniteGLACPE> selectedVariablesList = getDVUs(requestMetadatasMap);
            List<VariableGLACPE> selectedVariables = getVariables(selectedVariablesList, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
            if (selectedVariables != null && !selectedVariables.isEmpty()) {
                printVariablesSummary(selectedVariables, reminderPrintStream, "Production primaire");
            }
            selectedVariables = getVariables(selectedVariablesList, PPChloroTranspParameter.CHLOROPHYLLE);
            if (selectedVariables != null && !selectedVariables.isEmpty()) {
                printVariablesSummary(selectedVariables, reminderPrintStream, "Chlorophylle");
            }
            selectedVariables = selectedVariablesList.stream()
                    .filter(dvu -> dvu.getCode().startsWith("conditions_prelevements_transparence"))
                    .map(dvu -> dvu.getVariable())
                    .collect(Collectors.toList());
            if (selectedVariables != null && !selectedVariables.isEmpty()) {
                printVariablesSummary(selectedVariables, reminderPrintStream, "Transparence");
            }
            getDepthsRequestParam(requestMetadatasMap).buildSummary(reminderPrintStream);
            getDatasRequestParam(requestMetadatasMap).buildSummary(reminderPrintStream);
            printDatesSummary(requestMetadatasMap, reminderPrintStream);
            printComment(reminderPrintStream, requestMetadatasMap);
        } catch (FileNotFoundException e) {
            throw new BusinessException(e);
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);
        
        reminderPrintStream.flush();
        reminderPrintStream.close();
        
        return reminderMap;
    }
    
    protected List<VariableGLACPE> getVariables(List<DatatypeVariableUniteGLACPE> dvus, String datatypeName) {
        return dvus.stream()
                .filter(dvu -> Utils.createCodeFromString(datatypeName).equals(dvu.getDatatype().getCode()))
                .map(dvu -> dvu.getVariable())
                .collect(Collectors.toList());
    }
    
    private void printVariablesSummary(List<VariableGLACPE> list, PrintStream reminderPrintStream, String datatype) {
        Properties propertiesNomVariable = getLocalizationManager().newProperties(Variable.NAME_ENTITY_JPA, "nom");
        Properties propertiesNomDatatype = getLocalizationManager().newProperties(DataType.NAME_ENTITY_JPA, "name");
        String datatypeName = propertiesNomDatatype.getProperty(datatype, datatype);
        if (datatypeName == null) {
            datatypeName = "Transparency";
        }
        reminderPrintStream.println(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_VARIABLES), datatypeName));
        for (VariableGLACPE variable : list) {
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY, propertiesNomVariable.getProperty(variable.getCode(), variable.getCode())));
        }
        MDC.put("extraction.variables", list.stream().map(v->v.getCode()).collect(Collectors.joining(",")));
        reminderPrintStream.println();
    }
    
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {

        //ficher recap
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        
        return null;
    }
}

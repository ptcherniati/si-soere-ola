package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class DefaultZooplanctonDatatypeManager extends MO implements IZooplanctonDatatypeManager {

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Override
    public List<Taxon> retrieveRootTaxon() throws BusinessException {
        List<Taxon> taxonsRoots = taxonDAO.getZooRootTaxa();
        return taxonsRoots;
    }

    /**
     *
     * @param taxonId
     * @return
     */
    @Override
    public List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId) {
        return taxonDAO.getTaxonProprieteTaxonByTaxonId(taxonId);
    }

    /**
     *
     * @param taxonCode
     * @return
     */
    @Override
    public Optional<Taxon> retrieveTaxonByCode(String taxonCode) {
        return taxonDAO.getByCode(taxonCode);
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return zooplanctonDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return zooplanctonDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Map<Taxon, Boolean> getAvailableTaxons(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        Collection<Taxon> taxons = zooplanctonDAO.getAvailableTaxons(policyManager.getCurrentUser(), selectedPlatforms, selectedDates, selectedVariables);
        
        Map<Taxon, Boolean> taxonsInfos = null;
        if (taxons != null && !taxons.isEmpty()) {
            taxonsInfos = new HashMap<>();
            
            for (Taxon taxon : (List<Taxon>) taxons) {
                addTaxonToList(taxon, taxonsInfos);
                if(taxon.getTaxonParent()!=null && !taxonsInfos.containsKey(taxon.getTaxonParent())){
                    addTaxonToList(taxon.getTaxonParent(), taxonsInfos);
                }
            }

        }
        return taxonsInfos;
    }

    protected void addTaxonToList(Taxon taxon, Map<Taxon, Boolean> taxonsInfos) {
        Boolean isPreselected = false;
        for (TaxonProprieteTaxon tpt : taxonDAO.getTaxonProprieteTaxonByTaxonId(taxon.getId())) {
            if (tpt.getProprieteTaxon().getCode().equals(ProprieteTaxon.VALUES_ATTR_CODE.PRESELECTED) && tpt.getValeurProprieteTaxon().getStringValue() != null && !tpt.getValeurProprieteTaxon().getStringValue().trim().isEmpty()) {
                isPreselected = true;
                break;
            }
        }
        
        taxonsInfos.put(taxon, isPreselected);
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }
}

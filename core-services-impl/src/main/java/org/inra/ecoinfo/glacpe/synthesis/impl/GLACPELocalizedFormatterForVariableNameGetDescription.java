/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

/**
 * @author tcherniatinsky
 */
class GLACPELocalizedFormatterForVariableNameGetDescription implements ILocalizedFormatter<DatatypeVariableUniteGLACPE> {

    ILocalizationManager localizationManager;

    public GLACPELocalizedFormatterForVariableNameGetDescription() {
    }

    @Override
    public String format(DatatypeVariableUniteGLACPE nodeable, Locale locale, Object... arguments) {
        final Properties propertiesVariablesDefinitions = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), "definition", locale);
        return Optional.ofNullable(nodeable).map(nodeabledvu -> StringUtils.isEmpty(nodeabledvu.getVariable().getDefinition()) ? nodeabledvu.getVariable().getName() : nodeabledvu.getVariable().getDefinition())
                .map(definition -> propertiesVariablesDefinitions.getProperty(definition, definition))
                .map(definition -> String.format("<i>%s</i>", definition))
                .orElse("Error while retrieving variable definition)");
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}

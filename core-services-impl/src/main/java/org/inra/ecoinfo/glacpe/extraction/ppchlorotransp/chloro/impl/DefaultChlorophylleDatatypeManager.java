package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl;

import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultChlorophylleDatatypeManager extends MO implements IChlorophylleDatatypeManager {

    /**
     *
     */
    protected IChlorophylleDAO chlorophylleDAO;

    /**
     *
     * @param chlorophylleDAO
     */
    public void setChlorophylleDAO(IChlorophylleDAO chlorophylleDAO) {
        this.chlorophylleDAO = chlorophylleDAO;
    }

    /**
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return chlorophylleDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return chlorophylleDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Collection<Float> getAvailableDepths(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        return chlorophylleDAO.getAvailableDepths(policyManager.getCurrentUser(), selectedPlatforms, selectedDates, selectedVariables);
    }
}

package org.inra.ecoinfo.glacpe.refdata.site;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.refdata.site.ISiteDAO;

/**
 *
 * @author ptcherniati
 */
public interface ISiteGLACPEDAO extends ISiteDAO {

    /**
     *
     * @param id
     * @return
     */
    List<SiteGLACPE> getByTyepsiteId(Long id);

    /**
     *
     * @return
     */
    List<SiteGLACPE> getAllSitesGLACPE();
    
    /**
     *
     * @param code
     * @return
     */
    Optional<SiteGLACPE> getByCode(String code);
}

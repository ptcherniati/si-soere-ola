/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.taxon;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.IProprieteTaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.ITaxonNiveauDAO;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Guillaume Enrico"
 *
 */
public abstract class AbstractTaxonRecorder extends AbstractCSVMetadataRecorder<Taxon> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_ERROR_CODE_TAXON_LEVEL_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TAXON_LEVEL_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_CODE_TAXON_PARENT_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TAXON_PARENT_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected static final String MSG_ERROR_CODE_VALEUR_PROPRIETE_TAXON_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_VALEUR_PROPRIETE_TAXON_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected static final String MSG_ERROR_CODE_BAD_VALEUR_PROPRIETE_TAXON = "PROPERTY_MSG_ERROR_CODE_BAD_VALEUR_PROPRIETE_TAXON";

    /**
     *
     */
    protected static final String MSG_ERROR_CODE_PROPRIETE_TAXON_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PROPRIETE_TAXON_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected static final String MSG_ERROR_CODE_TAXON_THEME_NOT_RIGHT = "PROPERTY_MSG_ERROR_CODE_TAXON_THEME_NOT_RIGHT";

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     */
    protected ITaxonNiveauDAO taxonNiveauDAO;

    /**
     *
     */
    protected IProprieteTaxonDAO proprieteTaxonDAO;

    /**
     *
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;

    /**
     *
     */
    protected String[] namesTaxonNiveauxPossibles;

    /**
     *
     */
    protected String[] namesTaxonPossibles;

    /**
     *
     */
    protected String[] namesProprieteTaxonsPossibles;

    /**
     *
     */
    protected Map<String, String[]> mapListProprietesPossibles = new HashMap<String, String[]>();
    protected Properties valeursQualitativeValues;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        CSVParser parserBis;
        try {
            parserBis = new CSVParser(new InputStreamReader(new FileInputStream(file), encoding), PROPERTY_CST_CVS_SEPARATOR);

            ErrorsReport errorsReport = new ErrorsReport();
            try {

                // skipHeader(parser);
                // On parcourt chaque ligne du fichier
                String[] values = null;
                int lineNumber = 0;
                int variableHeaderIndex = 7;

                List<String> variableNames = skipHeaderAndRetrieveVariableName(variableHeaderIndex, parserBis);
                Map<String, ProprieteTaxon> proprieteMap = new HashMap<String, ProprieteTaxon>();

                for (String codePropriete : variableNames) {
                    ProprieteTaxon proTa = proprieteTaxonDAO.getByCode(codePropriete).orElse(null);
                    if (proTa != null) {
                        proprieteMap.put(codePropriete, proTa);
                    } else {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_PROPRIETE_TAXON_NOT_FOUND_IN_DB), codePropriete));
                    }
                }

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

                // List<String> taxonRoot = Lists.newLinkedList();
                Map<String, List<Map<String, String>>> taxon = new HashMap<String, List<Map<String, String>>>();

                while ((values = parserBis.getLine()) != null) {
                    TokenizerValues tokenizerValues = new TokenizerValues(values);
                    lineNumber++;

                    Map<String, String> variableMapLines = new HashMap<String, String>();

                    String nomLatin = tokenizerValues.nextToken();
                    String theme = tokenizerValues.nextToken();
                    String taxonNiveauCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                    String taxonParentNom = tokenizerValues.nextToken();

                    //afiocca
                    String codeSandreTax = tokenizerValues.nextToken();

                    String codeSandreSup = tokenizerValues.nextToken();

                    for (int actualVariableArray = variableHeaderIndex - 1; actualVariableArray < values.length; actualVariableArray++) {
                        variableMapLines.put(Utils.createCodeFromString(variableNames.get(actualVariableArray - variableHeaderIndex + 1)), tokenizerValues.nextToken());
                    }

                    if (!theme.equals(getTheme())) {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TAXON_THEME_NOT_RIGHT), nomLatin));
                    } else {
                        if (taxonParentNom == null || taxonParentNom.equals("")) {
                            persistTaxon(errorsReport, taxonNiveauCode, taxonParentNom, getTheme(), nomLatin, variableMapLines, proprieteMap, codeSandreTax, codeSandreSup);
                            if (taxon.get(Utils.createCodeFromString(nomLatin)) != null) {
                                reccursivePersistence(nomLatin, taxon, proprieteMap, errorsReport, codeSandreTax, codeSandreSup);
                            }
                        } else {
                            Taxon dbTaxonParent = taxonDAO.getByCode(Utils.createCodeFromString(taxonParentNom)).orElse(null);

                            if (dbTaxonParent != null) {
                                persistTaxon(errorsReport, taxonNiveauCode, taxonParentNom, getTheme(), nomLatin, variableMapLines, proprieteMap, codeSandreTax, codeSandreSup);
                                if (taxon.get(Utils.createCodeFromString(nomLatin)) != null) {
                                    reccursivePersistence(nomLatin, taxon, proprieteMap, errorsReport, codeSandreTax, codeSandreSup);
                                }
                            } else {
                                variableMapLines.put("nomLatin", nomLatin);
                                variableMapLines.put("taxonNiveauCode", taxonNiveauCode);
                                variableMapLines.put("taxonParentNom", taxonParentNom);

                                if (taxon.isEmpty() || taxon.get(Utils.createCodeFromString(taxonParentNom)) == null) {
                                    List<Map<String, String>> listTaxon = Lists.newArrayList();
                                    listTaxon.add(variableMapLines);
                                    taxon.put(Utils.createCodeFromString(taxonParentNom), listTaxon);
                                } else {
                                    taxon.get(Utils.createCodeFromString(taxonParentNom)).add(variableMapLines);
                                }
                            }
                        }
                    }
                }

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                AbstractCSVMetadataRecorder.LOGGER.debug(String.format("%d records has been updated in DB...", lineNumber));
            } catch (IOException | PersistenceException e) {
                throw new BusinessException(e.getMessage(), e);
            }
            initModelGridMetadata();
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
//                tokenizerValues.nextToken();
                String nomLatin = Utils.createCodeFromString(tokenizerValues.nextToken());
                Taxon taxon = taxonDAO.getByCode(Utils.createCodeFromString(nomLatin))
                        .orElseThrow(()->new PersistenceException("can't retrieve taxon"));
                Taxon taxonParent = taxon.getTaxonParent();
                taxonParent.getTaxonsEnfants().remove(taxon);
                // taxonDAO.saveOrUpdate(taxonParent);
                taxonDAO.remove(taxon);
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void reccursivePersistence(String parent, Map<String, List<Map<String, String>>> taxon, Map<String, ProprieteTaxon> proprieteMap, ErrorsReport errorsReport, String codeSandreTax, String codeSandreSup) throws BusinessException {
        try {
            if (taxon.get(Utils.createCodeFromString(parent)) != null) {
                for (Map<String, String> taxonEnfants : taxon.get(Utils.createCodeFromString(parent))) {
                    Map<String, String> variableLinesMap = new HashMap<String, String>();
                    for (String key : taxonEnfants.keySet()) {
                        if (!key.equals("taxonNiveauCode") && !key.equals("taxonParentNom") && !key.equals("nomLatin")) {
                            variableLinesMap.put(key, taxonEnfants.get(key));
                        }
                    }
                    persistTaxon(errorsReport, taxonEnfants.get("taxonNiveauCode"), taxonEnfants.get("taxonParentNom"), getTheme(), taxonEnfants.get("nomLatin"), variableLinesMap, proprieteMap, codeSandreTax, codeSandreSup);
                    if (taxon.get(Utils.createCodeFromString(taxonEnfants.get("nomLatin"))) != null) {
                        reccursivePersistence(taxonEnfants.get("nomLatin"), taxon, proprieteMap, errorsReport, codeSandreTax, codeSandreSup);
                    }
                }
            }
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param errorsReport
     * @param taxonCode
     * @param errorMessageTemplate
     * @return
     * @throws PersistenceException
     */
    protected Taxon retrieveDBTaxon(ErrorsReport errorsReport, String taxonCode, String errorMessageTemplate) throws PersistenceException {
        Taxon dbTaxon = taxonDAO.getByCode(taxonCode).orElse(null);
        if (dbTaxon == null && taxonCode.trim().length() > 0) {
            errorsReport.addErrorMessage(String.format(errorMessageTemplate, taxonCode));
        }
        return dbTaxon;
    }

    /**
     *
     * @param errorsReport
     * @param taxonNiveauCode
     * @return
     * @throws PersistenceException
     */
    protected TaxonNiveau retrieveDBTaxonNiveau(ErrorsReport errorsReport, String taxonNiveauCode) throws PersistenceException {
        TaxonNiveau dbTaxonNiveau = taxonNiveauDAO.getByCode(taxonNiveauCode).orElse(null);
        if (dbTaxonNiveau == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TAXON_LEVEL_NOT_FOUND_IN_DB), taxonNiveauCode));
        }
        return dbTaxonNiveau;
    }

    /**
     *
     * @param errorsReport
     * @param taxonNiveauCode
     * @param taxonParentNom
     * @param theme
     * @param nomLatin
     * @param variableMapLines
     * @param proprieteMap
     * @param codeSandreTax
     * @param codeSandre
     * @throws PersistenceException
     */
    protected void persistTaxon(ErrorsReport errorsReport, String taxonNiveauCode, String taxonParentNom, String theme, String nomLatin, Map<String, String> variableMapLines, Map<String, ProprieteTaxon> proprieteMap, String codeSandreTax, String codeSandreSup) throws PersistenceException {
        try {
            Taxon dbTaxonParent = null;
            if (taxonParentNom != null) {
                dbTaxonParent = retrieveDBTaxon(errorsReport, Utils.createCodeFromString(taxonParentNom), localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TAXON_PARENT_NOT_FOUND_IN_DB));
            }
            TaxonNiveau dbTaxonNiveau = retrieveDBTaxonNiveau(errorsReport, taxonNiveauCode);

            if (dbTaxonNiveau != null) {
                Taxon dbTaxon = taxonDAO.getByCode(Utils.createCodeFromString(nomLatin)).orElse(null);

                createOrUpdateTaxon(errorsReport, theme, nomLatin, dbTaxonParent, dbTaxonNiveau, dbTaxon, variableMapLines, proprieteMap, codeSandreTax, codeSandreSup);
            }
        } catch (Exception e) {
            throw new PersistenceException(e);
        }

    }

    private void createOrUpdateTaxon(ErrorsReport errorsReport, String theme, String nomLatin, Taxon dbTaxonParent, TaxonNiveau dbTaxonNiveau, Taxon dbTaxon, Map<String, String> variableMapLines, Map<String, ProprieteTaxon> proprieteMap, String codeSandreTax, String codeSandreSup)
            throws PersistenceException {
        // Enregistre un taxon uniquement s'il n'existe pas en BD ou
        // bien s'il est considéré comme une mise à jour
        if (dbTaxon == null) {
            createTaxon(errorsReport, theme, nomLatin, dbTaxonParent, dbTaxonNiveau, variableMapLines, proprieteMap, codeSandreTax, codeSandreSup);
        } else {
            updateTaxon(errorsReport, theme, dbTaxonParent, dbTaxonNiveau, dbTaxon, variableMapLines, proprieteMap, codeSandreTax, codeSandreSup);
        }
    }

    /**
     *
     * @param errorsReport
     * @param theme
     * @param dbTaxonParent
     * @param dbTaxonNiveau
     * @param dbTaxon
     * @param variableMapLines
     * @param proprieteMap
     * @param codeSandreTax
     * @param codeSandreSup
     * @param codeSandre
     * @throws PersistenceException
     */
    protected void updateTaxon(ErrorsReport errorsReport, String theme, Taxon dbTaxonParent, TaxonNiveau dbTaxonNiveau, Taxon dbTaxon, Map<String, String> variableMapLines, Map<String, ProprieteTaxon> proprieteMap, String codeSandreTax, String codeSandreSup) throws PersistenceException {
        dbTaxon.setTheme(theme);

        dbTaxon.setTaxonNiveau(dbTaxonNiveau);

        dbTaxon.setCodeSandreTax(codeSandreTax);

        dbTaxon.setCodeSandreSup(codeSandreSup);

        dbTaxonNiveau.getTaxons().add(dbTaxon);

        dbTaxon.setTaxonParent(dbTaxonParent);
        if (dbTaxonParent != null) {
            dbTaxonParent.getTaxonsEnfants().add(dbTaxon);
        }

        for (String codePropriete : variableMapLines.keySet()) {
            if (variableMapLines.get(codePropriete) != null) {
                if (dbTaxon.getTaxonProprieteTaxon() != null) {
                    Boolean updated = false;
                    for (TaxonProprieteTaxon taxonProprieteTaxon : dbTaxon.getTaxonProprieteTaxon()) {
                        if (taxonProprieteTaxon.getProprieteTaxon().getCode().equals(codePropriete)) {
                            ValeurProprieteTaxon valeurProprieteTaxon = taxonProprieteTaxon.getValeurProprieteTaxon();
                            if (!taxonProprieteTaxon.getProprieteTaxon().getValueType()) {
                                if (taxonProprieteTaxon.getProprieteTaxon().getIsQualitative()) {
                                    ValeurQualitative valeurQualitative = (ValeurQualitative) valeurQualitativeDAO.getByCodeAndValue(taxonProprieteTaxon.getProprieteTaxon().getCode(), variableMapLines.get(codePropriete)).orElse(null);
                                    if (valeurQualitative == null) {
                                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_VALEUR_PROPRIETE_TAXON_NOT_FOUND_IN_DB), variableMapLines.get(codePropriete), taxonProprieteTaxon
                                                .getProprieteTaxon().getNom()));
                                        updated = true;
                                        break;
                                    } else {
                                        valeurProprieteTaxon.setQualitativeValue(valeurQualitative);
                                    }
                                } else if (!valeurProprieteTaxon.getStringValue().contentEquals(variableMapLines.get(codePropriete))) {
                                    valeurProprieteTaxon.setStringValue(variableMapLines.get(codePropriete));
                                }
                            } else {
                                try {
                                    Float value = Float.parseFloat(variableMapLines.get(codePropriete));
                                    if (valeurProprieteTaxon.getFloatValue() != value) {
                                        valeurProprieteTaxon.setFloatValue(value);
                                    }
                                } catch (NumberFormatException e) {
                                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_BAD_VALEUR_PROPRIETE_TAXON), variableMapLines.get(codePropriete), codePropriete));
                                }
                            }
                            updated = true;
                            break;
                        }
                    }
                    if (!updated) {
                        createNewProprieteTaxon(errorsReport, proprieteMap.get(codePropriete), dbTaxon, variableMapLines.get(codePropriete));
                    }
                } else {
                    createNewProprieteTaxon(errorsReport, proprieteMap.get(codePropriete), dbTaxon, variableMapLines.get(codePropriete));
                }
            }
        }
    }

    /**
     *
     * @param errorsReport
     * @param proTa
     * @param taxon
     * @param value
     * @throws PersistenceException
     */
    protected void createNewProprieteTaxon(ErrorsReport errorsReport, ProprieteTaxon proTa, Taxon taxon, String value) throws PersistenceException {
        TaxonProprieteTaxon taxonProprieteTaxon = new TaxonProprieteTaxon(taxon, proTa);

        ValeurProprieteTaxon valeurProprieteTaxon = new ValeurProprieteTaxon();
        taxonProprieteTaxon.setValeurProprieteTaxon(valeurProprieteTaxon);

        if (!proTa.getValueType()) {
            if (proTa.getIsQualitative()) {
                ValeurQualitative valeurQualitative = (ValeurQualitative) valeurQualitativeDAO.getByCodeAndValue(proTa.getCode(), Utils.createCodeFromString(value)).orElse(null);
                if (valeurQualitative == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_VALEUR_PROPRIETE_TAXON_NOT_FOUND_IN_DB), value, taxonProprieteTaxon.getProprieteTaxon().getNom()));
                } else {
                    valeurProprieteTaxon.setQualitativeValue(valeurQualitative);
                }
            } else {
                valeurProprieteTaxon.setStringValue(value);
            }
        } else {
            try {
                Float valeur = Float.parseFloat(value);
                valeurProprieteTaxon.setFloatValue(valeur);
            } catch (NumberFormatException e) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_BAD_VALEUR_PROPRIETE_TAXON), value, proTa.getCode()));
            }
        }

        if (taxon.getTaxonProprieteTaxon() != null) {
            taxon.getTaxonProprieteTaxon().add(taxonProprieteTaxon);
        } else {
            List<TaxonProprieteTaxon> listTaxonProprieteTaxon = Lists.newArrayList();
            listTaxonProprieteTaxon.add(taxonProprieteTaxon);
            taxon.setTaxonProprieteTaxon(listTaxonProprieteTaxon);
        }
        taxonProprieteTaxon.setValeurProprieteTaxon(valeurProprieteTaxon);
    }

    /**
     *
     * @param errorsReport
     * @param theme
     * @param nomLatin
     * @param dbTaxonParent
     * @param dbTaxonNiveau
     * @param variableMapLines
     * @param proprieteMap
     * @param codeSandreTax
     * @param codeSandreSup
     * @throws PersistenceException
     */
    protected void createTaxon(ErrorsReport errorsReport, String theme, String nomLatin, Taxon dbTaxonParent, TaxonNiveau dbTaxonNiveau, Map<String, String> variableMapLines, Map<String, ProprieteTaxon> proprieteMap, String codeSandreTax, String codeSandreSup) throws PersistenceException {
        Taxon taxon = new Taxon(nomLatin, theme);

        taxon.setTaxonNiveau(dbTaxonNiveau);
        dbTaxonNiveau.getTaxons().add(taxon);
        taxon.setCodeSandreTax(codeSandreTax);

        taxon.setCodeSandreSup(codeSandreSup);

        taxon.setTaxonParent(dbTaxonParent);

        if (dbTaxonParent != null) {
            dbTaxonParent.getTaxonsEnfants().add(taxon);
        }

        for (String codePropriete : variableMapLines.keySet()) {
            if (variableMapLines.get(codePropriete) != null) {
                createNewProprieteTaxon(errorsReport, proprieteMap.get(codePropriete), taxon, variableMapLines.get(codePropriete));
            }
        }
        taxonDAO.saveOrUpdate(taxon);
    }

    /**
     *
     * @param variableColumnIndex
     * @param parser
     * @return
     * @throws IOException
     */
    protected List<String> skipHeaderAndRetrieveVariableName(int variableColumnIndex, CSVParser parser) throws IOException {
        String[] values = parser.getLine();
        List<String> variablesNames = new LinkedList<String>();

        for (; variableColumnIndex <= values.length; variableColumnIndex++) {
            variablesNames.add(Utils.createCodeFromString(values[variableColumnIndex - 1]));
        }
        return variablesNames;
    }

    /**
     *
     * @throws PersistenceException
     */
    protected void updateNamesTaxonNiveauxPossibles() throws PersistenceException {
        List<TaxonNiveau> taxonNiveaux = taxonNiveauDAO.getAll(TaxonNiveau.class);
        String[] namesTaxonNiveauxPossibles = new String[taxonNiveaux.size()];
        int index = 0;
        for (TaxonNiveau taxonNiveau : taxonNiveaux) {
            namesTaxonNiveauxPossibles[index++] = taxonNiveau.getNom();
        }
        this.namesTaxonNiveauxPossibles = namesTaxonNiveauxPossibles;
    }

    /**
     *
     * @throws PersistenceException
     */
    protected void updateNamesTaxonPossibles() throws PersistenceException {
        List<String> taxons = getAllElementsByTheme();
        String[] namesTaxonPossibles = new String[taxons.size() + 1];
        namesTaxonPossibles[0] = "";
        int index = 1;
        for (String taxon : taxons) {
            namesTaxonPossibles[index++] = taxon;
        }
        this.namesTaxonPossibles = namesTaxonPossibles;
    }

    /**
     *
     * @throws PersistenceException
     */
    protected void updateNamesProprieteTaxonPossibles() throws PersistenceException {
        List<ProprieteTaxon> proprieteTaxons = proprieteTaxonDAO.getAll(ProprieteTaxon.class);
        String[] namesProprieteTaxonPossibles = new String[proprieteTaxons.size()];
        int index = 0;
        for (ProprieteTaxon taxon : proprieteTaxons) {
            namesProprieteTaxonPossibles[index++] = taxon.getNom();
            if (taxon.getIsQualitative()) {
                List<ValeurQualitative> valuesProprietePossibles = valeurQualitativeDAO.getByCode(taxon.getCode());
                String[] valeursProprieteTaxonPossibles = new String[valuesProprietePossibles.size()];
                int index2 = 0;
                for (ValeurQualitative valueProprietePossible : valuesProprietePossibles) {
                    valeursProprieteTaxonPossibles[index2++] = valueProprietePossible.getValeur();
                }
                mapListProprietesPossibles.put(taxon.getNom(), valeursProprieteTaxonPossibles);
            }
        }
        this.namesProprieteTaxonsPossibles = namesProprieteTaxonPossibles;
    }

    /**
     *
     * @param taxon
     * @return
     * @throws PersistenceException
     */
    protected Map<String, TaxonProprieteTaxon> updateValeurProprieteTaxon(Taxon taxon) {
        if (taxon == null) {
            return null;
        } else {
            Map<String, TaxonProprieteTaxon> orderedTaxonMap = new HashMap<String, TaxonProprieteTaxon>();
            if (taxon.getTaxonProprieteTaxon() != null && !taxon.getTaxonProprieteTaxon().isEmpty()) {
                for (TaxonProprieteTaxon taprota : taxon.getTaxonProprieteTaxon()) {
                    orderedTaxonMap.put(taprota.getProprieteTaxon().getNom(), taprota);
                }
            }
            return orderedTaxonMap;
        }
    }

    /**
     *
     * @return
     */
    protected abstract String getTheme();

    /**
     *
     * @return @throws PersistenceException
     */
    protected abstract List<String> getAllElementsByTheme() throws PersistenceException;

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param taxonNiveauDAO
     */
    public void setTaxonNiveauDAO(ITaxonNiveauDAO taxonNiveauDAO) {
        this.taxonNiveauDAO = taxonNiveauDAO;
    }

    /**
     *
     * @param proprieteTaxonDAO
     */
    public void setProprieteTaxonDAO(IProprieteTaxonDAO proprieteTaxonDAO) {
        this.proprieteTaxonDAO = proprieteTaxonDAO;
    }

    /**
     *
     * @param taxon
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Taxon taxon) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Map<String, TaxonProprieteTaxon> valeurProprietetaxons = updateValeurProprieteTaxon(taxon);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxon == null ? EMPTY_STRING : taxon.getNomLatin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxon == null ? EMPTY_STRING : taxon.getTheme(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxon == null ? EMPTY_STRING : taxon.getTaxonNiveau().getNom(), namesTaxonNiveauxPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(taxon == null ? EMPTY_STRING : taxon.getTaxonParent() == null ? EMPTY_STRING : taxon.getTaxonParent().getNomLatin(), namesTaxonPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxon == null && Strings.isNullOrEmpty(taxon.getCodeSandreTax())? EMPTY_STRING : taxon.getCodeSandreTax(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(taxon == null && Strings.isNullOrEmpty(taxon.getCodeSandreSup()) ? EMPTY_STRING : taxon.getCodeSandreSup(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        for (String proprieteTaxonName : namesProprieteTaxonsPossibles) {
            Object value = EMPTY_STRING;
            if(taxon!=null){
                if(valeurProprietetaxons.get(proprieteTaxonName) != null ){
                    if(valeurProprietetaxons.get(proprieteTaxonName).getProprieteTaxon().getIsQualitative()){
                        value=valeursQualitativeValues.getOrDefault(
                                valeurProprietetaxons.get(proprieteTaxonName).getValeurProprieteTaxon().getQualitativeValue().getValeur(),
                                valeurProprietetaxons.get(proprieteTaxonName).getValeurProprieteTaxon().getQualitativeValue().getValeur()
                        );
                    }else{
                        if(valeurProprietetaxons.get(proprieteTaxonName).getProprieteTaxon().getValueType()){
                            value=valeurProprietetaxons.get(proprieteTaxonName).getValeurProprieteTaxon().getFloatValue();
                        } else {
                            value=valeurProprietetaxons.get(proprieteTaxonName).getValeurProprieteTaxon().getStringValue();
                        }
                    }
                }
            }
            lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                    new ColumnModelGridMetadata(value, mapListProprietesPossibles.get(proprieteTaxonName), null, false, false, false));
        }

        return lineModelGridMetadata;
    }

    /**
     *
     * @param valeurQualitativeDAO
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

    @Override
    protected ModelGridMetadata<Taxon> initModelGridMetadata() {
        try {
            updateNamesTaxonNiveauxPossibles();
            updateNamesTaxonPossibles();
            updateNamesProprieteTaxonPossibles();
        } catch (PersistenceException e) {
        }
        ModelGridMetadata<Taxon> modelGridMetadata = super.initModelGridMetadata();
        for (String proprieteTaxonName : namesProprieteTaxonsPossibles) {
            modelGridMetadata.getHeadersColumns().add(proprieteTaxonName);
        }
        valeursQualitativeValues = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, ValeurQualitative.ATTRIBUTE_JPA_VALUE);
        return modelGridMetadata;
    }
}

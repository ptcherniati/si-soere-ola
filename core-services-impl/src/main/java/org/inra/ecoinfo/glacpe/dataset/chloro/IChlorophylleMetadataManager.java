package org.inra.ecoinfo.glacpe.dataset.chloro;

import java.text.DateFormat;
import java.util.List;

/**
 *
 * @author ptcherniati
 */
public interface IChlorophylleMetadataManager {

    /**
     *
     * @param variable
     * @return
     */
    List<String> retrieveAllowedSitesNamesByVariable(String variable);

    /**
     *
     * @param siteSelected
     * @param variableSelected
     * @param startString
     * @param endString
     * @param dateFormatter
     * @param datatType
     * @return
     */
    String commentDateInRange(String siteSelected, String variableSelected, String startString, String endString, DateFormat dateFormatter, String datatType);

}

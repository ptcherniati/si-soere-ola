package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.AggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.extraction.phytoplancton.messages";

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";

    /**
     *
     */
    public static String BIOVOLUME_SEDIMENTE = "biovolume_de_l_espece_dans_l_echantillon";

    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s",
                getLocalizationManager().
                        getMessage(
                                BUNDLE_SOURCE_PATH_PHYTOPLANCTON,
                                HEADER_RAW_DATA_ALLDEPTH)));
        return stringBuilder.toString();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    protected Map<String, File> buildBody(String headers,
            Map<String, List> resultsDatasMap,
            Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>>> variablesAggregatedDatas = buildVariablesAggregated(valeursMesures);

        for (Iterator<Map.Entry<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>>>> iterator = variablesAggregatedDatas.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>> mapBySite = projetEntry.getValue();
            for (Iterator<Map.Entry<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>> mapByPlateforme = siteEntry.getValue();
                final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                rawDataPrintStream.println(headers);
                for (Iterator<Map.Entry<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>> plateformeEntrye = iterator2.next();
                    Plateforme plateforme = plateformeEntrye.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>> mapByDate = plateformeEntrye.getValue();
                    for (Iterator<Map.Entry<LocalDate, SortedSet<VariableAggregatedDatas>>> iterator3 = mapByDate.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, SortedSet<VariableAggregatedDatas>> dateEntry = iterator3.next();
                        LocalDate date = dateEntry.getKey();
                        String dateKey = DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY);
                        SortedSet<VariableAggregatedDatas> value = dateEntry.getValue();
                        for (Iterator<VariableAggregatedDatas> iterator4 = value.iterator(); iterator4.hasNext();) {
                            VariableAggregatedDatas variableAggregatedData = iterator4.next();

                            AggregatedData aggregatedData = variableAggregatedData.
                                    getAverageAggregatedData();
                            String localizedOutilPrelevementName = variableAggregatedData.
                                    getOutilsPrelevement() == null ? "" : propertiesOutilName.
                                                    getProperty(variableAggregatedData.
                                                            getOutilsPrelevement().
                                                            getNom(),
                                                            variableAggregatedData.
                                                                    getOutilsPrelevement().
                                                                    getNom());

                            String localizedOutilMesureName = variableAggregatedData.
                                    getOutilsMesure() == null ? "" : propertiesOutilName.
                                                    getProperty(variableAggregatedData.
                                                            getOutilsMesure().
                                                            getNom(),
                                                            variableAggregatedData.
                                                                    getOutilsMesure().
                                                                    getNom());
                            rawDataPrintStream.print(String.format("%s;%s;%s;",
                                    localizedProjetName,
                                    localizedSiteName,
                                    localizedPlateformeName));
                            rawDataPrintStream.print(String.format("%s;%s;%s;",
                                    date,
                                    localizedOutilPrelevementName,
                                    localizedOutilMesureName));
                            rawDataPrintStream.print(String.format("%s",
                                    aggregatedData.
                                            getValuesByDatesMap().
                                            get(dateKey)));

                            rawDataPrintStream.println();
                        }
                    }
                }
            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    protected SortedSet<VariableAggregatedDatas> getAggregated(TreeSet<IGLACPEAggregateData> aggregateddatasList) {
        SortedSet<VariableAggregatedDatas> variablesAggregatedDatas = new TreeSet<>();
        return aggregateddatasList.stream().findFirst()
                .map((aggregateData) -> {
                    VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(
                            aggregateData.getSite().getName(),
                            aggregateData.getPlateforme().getName(),
                            ((DatatypeVariableUniteGLACPE) aggregateData.getRealNode().getNodeable()).getVariable().getName());
                    fillVariableAggregatedDatas(variableAggregatedDatas,
                            aggregateData.getDate(),
                            aggregateddatasList);
                    variablesAggregatedDatas.add(variableAggregatedDatas);
                    return variablesAggregatedDatas; //To change body of generated lambdas, choose Tools | Templates.
                })
                .orElse(variablesAggregatedDatas);
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, SortedSet<VariableAggregatedDatas>>>>> buildVariablesAggregated(List<IGLACPEAggregateData> valeursMesures) {
        return valeursMesures
                .stream()
                .filter(valeur -> BIOVOLUME_SEDIMENTE.equals(((DatatypeVariableUniteGLACPE) valeur.getRealNode().getNodeable()).getVariable().getCode()))
                .collect(
                        Collectors.groupingBy(
                                v -> v.getProjet(),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        v -> v.getSite(),
                                        TreeMap::new,
                                        Collectors.groupingBy(
                                                v -> v.getPlateforme(),
                                                TreeMap::new,
                                                Collectors.groupingBy(
                                                        v -> v.getDate(),
                                                        TreeMap::new,
                                                        Collectors.collectingAndThen(
                                                                Collectors.toCollection(TreeSet::new),
                                                                map -> getAggregated(map)
                                                        )
                                                )
                                        )
                                )
                        )
                );
    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     */
    @Override
    protected void fillVariableAggregatedDatas(
            VariableAggregatedDatas variableAggregatedDatas,
            LocalDate dateKey,
            SortedSet<IGLACPEAggregateData> valeursMesureDates) {
        int index = 0;
        Float m = 0f;
        Optional<IGLACPEAggregateData> valeurOpt = valeursMesureDates
                .stream()
                .limit(1)
                .findFirst();
        valeurOpt.ifPresent(valeur -> variableAggregatedDatas.setOutilsMesure(valeur.getOutilsMesure()));
        valeurOpt.ifPresent(valeur -> variableAggregatedDatas.setOutilsPrelevement(valeur.getOutilsPrelevement()));

        Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
        final int size = valeursMesureDates.size();

        while (iterator.hasNext()) {
            IGLACPEAggregateData valeurMesure = iterator.next();
            if (valeurMesure.getValue() != null) {
                if (index <= size - 1) {
                    index++;
                    m += valeurMesure.getValue();
                }
            }
            iterator.remove();
        }

        variableAggregatedDatas.getAverageAggregatedData().
                getValuesByDatesMap().
                put(dateKey,
                        m);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().
                add(super.buildOutput(parameters,
                        PhytoplanctonAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

}

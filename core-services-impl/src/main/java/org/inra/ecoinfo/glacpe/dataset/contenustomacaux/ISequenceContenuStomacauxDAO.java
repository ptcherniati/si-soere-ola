package org.inra.ecoinfo.glacpe.dataset.contenustomacaux;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceContenuStomacauxDAO extends IDAO<SequenceContenuStomacaux> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     */
    Optional<SequenceContenuStomacaux> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode);
}

package org.inra.ecoinfo.glacpe.dataset.hautefrequence.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.IHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.ISequenceHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.MesureHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SequenceHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SousSequenceHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.ValeurMesureHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.dimensions.Dimension;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author mylene
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     * The Constant GLACPE_DATASET_BUNDLE_NAME.
     */
    public static final String GLACPE_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.glacpe.dataset.messages";

    private static final long serialVersionUID = 1L;
    private static final String VARIABLE_PROFONDEUR_MESUREE = "profondeur_mesuree";
    private static volatile ILocalizationManager localizationManager;

    /**
     *
     * @param zone
     * @return
     * @throws BusinessException
     */
    public static ZoneId getZone(String zone) throws BusinessException {
        return ZoneId.getAvailableZoneIds()
                .stream()
                .filter(s -> s.endsWith(zone.toUpperCase()))
                .map(Z -> ZoneId.of(Z))
                .findFirst()
                .orElseThrow(() -> new BusinessException("can't find zone"));
        
    }

    /**
     *
     */
    protected ISequenceHauteFrequenceDAO sequenceHauteFrequenceDAO;

    /**
     *
     */
    protected IHauteFrequenceDAO hauteFrequenceDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;

    protected String datatypeCode;

    /**
     *
     * @param sequenceHauteFrequenceDAO
     */
    public void setSequenceHauteFrequenceDAO(ISequenceHauteFrequenceDAO sequenceHauteFrequenceDAO) {
        this.sequenceHauteFrequenceDAO = sequenceHauteFrequenceDAO;
    }

    /**
     *
     * @param shauteFrequenceDAO
     */
    public void sethauteFrequenceDAO(IHauteFrequenceDAO shauteFrequenceDAO) {
        this.hauteFrequenceDAO = hauteFrequenceDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IHauteFrequenceDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(ErrorsReport errorsReport, final CSVParser parser, final String projetCodeFromVersion, final String siteCodeFromVersion, final RealNode realNode, final DatasetDescriptor datasetDescriptorGLACPE) throws IOException {
        parser.getLine();
        for (int i = datasetDescriptorGLACPE.getUndefinedColumn(); i < datasetDescriptorGLACPE.getColumns().size(); i++) {
            Optional.of(datasetDescriptorGLACPE.getColumns().get(i))
                    .filter(c->!c.isVariable())
                    .ifPresent(
                            c->errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), datasetDescriptorGLACPE.getColumns().indexOf(c)+1, c.getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)))
                    );
        }

        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

// A adapter pour les données Haute Fréquence :
//      - processRecord
//      - buildSequence
//      - buildSousSequence
//      - buildMesure
    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        try {
            ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
            Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
            Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());
            datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
            String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
            String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
            Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(errorsReport, parser,projetCodeFromVersion, siteCodeFromVersion, versionFile.getDataset().getRealNode(), datasetDescriptor)
                    .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                    .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
            if(errorsReport.hasErrors()){
                throw new BusinessException(errorsReport.getMessages());
            }
            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(
                    siteCodeFromVersion, datatypeCode);
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();
            SortedMap<LocalDate, SortedMap<String, SortedMap<LocalTime, List<LineRecord>>>> sequencesMapLines = new TreeMap<>();
            String projetCode;
            String nomSite;
            String plateformeCode;
            LocalDate datePrelevement;
            LocalTime heurePrelevement;
            ZoneId fuseauHoraire;
            String outilMesureCode;
            String commentaires;
            Float profondeur;
            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                if (!Utils.createCodeFromString(projetCode).equals(projetCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), projetCodeFromVersion, lineCount, 1, projetCode)));
                }
                nomSite = cleanerValues.nextToken();
                if (!Utils.createCodeFromString(nomSite).equals(siteCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, lineCount, 2, nomSite)));
                }
                plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                Plateforme plateforme = plateformeDAO.getByNKey(plateformeCode, nomSite).orElse(null);
                if (plateforme == null) {
                    InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, lineCount));
                    errorsReport.addException(insertionDatabaseException);
                }
                String datePrelevementString = "";
                String heurePrelevementString = "";
                String zoneIdString = "";
                ZonedDateTime zdt = readZone(errorsReport, lineCount, 6, cleanerValues, datasetDescriptor);
                datePrelevement = zdt.toLocalDate();
                heurePrelevement = zdt.toLocalTime();
                fuseauHoraire = zdt.getZone();
                outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                commentaires = cleanerValues.nextToken();
                profondeur = readDepth(errorsReport, controlesCoherenceMap, lineCount, 9, cleanerValues);

                //Traitement de la profondeur
                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        RealNode realNode = Optional
                                .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                                .map(c -> Utils.createCodeFromString(c.getName()))
                                .map(n -> realNodes.get(n))
                                .map(v -> v.get(0))
                                .orElse(null);
                        if (realNode != null) {

                            VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                            if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                            }
                            variablesValues.add(new VariableValue(realNode, values[actualVariableArray].replaceAll(" ", "")));
                        } else {
                            errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)));

                        }
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, outilMesureCode, commentaires, profondeur, heurePrelevement, fuseauHoraire, variablesValues, lineCount, projetCode);
                sequencesMapLines
                        .computeIfAbsent(datePrelevement, k -> new TreeMap<>())
                        .computeIfAbsent(String.format("%s,%s,%s", plateformeCode, outilMesureCode, profondeur), k -> new TreeMap<>())
                        .computeIfAbsent(heurePrelevement, k -> new LinkedList<>())
                        .add(line);

            }
            if(errorsReport.hasErrors()){
                throw new BusinessException(errorsReport.getMessages());
            }
            Allocator allocator = Allocator.getInstance();

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            for (Iterator<Map.Entry<LocalDate, SortedMap<String, SortedMap<LocalTime, List<LineRecord>>>>> iterator = sequencesMapLines.entrySet().iterator(); iterator.hasNext();) {
                Map.Entry<LocalDate, SortedMap<String, SortedMap<LocalTime, List<LineRecord>>>> entry = iterator.next();
                SortedMap<String, SortedMap<LocalTime, List<LineRecord>>> sequenceLines = entry.getValue();
                datePrelevement = entry.getKey();
                if (!sequenceLines.isEmpty()) {
                    try {
                        datePrelevement = datePrelevement;
                        buildSequence(datePrelevement, projetCodeFromVersion, siteCodeFromVersion, sequenceLines, versionFile, errorsReport);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequenceHauteFrequenceDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                    } catch (InsertionDatabaseException | PersistenceException ex) {
                        errorsReport.addException(ex);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
        } catch (IOException | InterruptedException  ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

    }

    private Float readDepth(ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, int columnNumber, CleanerValues cleanerValues) {
        String profondeurString = cleanerValues.nextToken();
        Float profondeur = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
        if (profondeur != null) {
            try {
                profondeur = Float.parseFloat(profondeurString);

                if (!controlesCoherenceMap.isEmpty() && controlesCoherenceMap.get(VARIABLE_PROFONDEUR_MESUREE) != null) {
                    testValueCoherence(
                            profondeur,
                            controlesCoherenceMap.get(VARIABLE_PROFONDEUR_MESUREE).getValeurMin(),
                            controlesCoherenceMap.get(VARIABLE_PROFONDEUR_MESUREE).getValeurMax(),
                            lineCount,
                            8);
                }
            } catch (BadExpectedValueException e) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, columnNumber)));
            }
        }
        return profondeur;
    }

    private String readValue(ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, String variableCode, String value, CleanerValues cleanerValues) {
        try {
            value = cleanerValues.nextToken();
            if (controlesCoherenceMap.get(variableCode) != null && value != null && !value.trim().isEmpty()) {
                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variableCode).getValeurMin(), controlesCoherenceMap.get(variableCode).getValeurMax(), lineCount, 13);
            }
        } catch (NumberFormatException e) {
            errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 13, value)));
        } catch (BadExpectedValueException e) {
            errorsReport.addException(e);
        }
        return value;
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private LocalTime readTime(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String timeString;
        LocalTime time = null;
        timeString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            time = Strings.isNullOrEmpty(timeString) ? null
                    : DateUtil.readLocalTimeFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), timeString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_TIME), timeString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return time;

    }

    private ZonedDateTime readZone(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        LocalDate datePrelevement = readDate(errorsReport, lineCount, columnNumber - 2, cleanerValues, datasetDescriptor);
        LocalTime heurePrelevement = readTime(errorsReport, lineCount, columnNumber - 1, cleanerValues, datasetDescriptor);

        String zone = cleanerValues.nextToken();
        ZoneId zoneId = null;
        try {
            zoneId = getZone(zone);
        } catch (BusinessException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_ZONE), zone, lineCount, columnNumber)));
        }
        ZonedDateTime zonedDateTime = null;
        if (datePrelevement != null && heurePrelevement != null && zoneId != null) {
            zonedDateTime = datePrelevement.atTime(heurePrelevement).atZone(DateUtil.UTC_ZONEID).withZoneSameInstant(zoneId);
        }
        return zonedDateTime;
    }


    private void fillLinesMap(Map<String, List<LineRecord>> linesMap, LineRecord line, String keyLine) {
        List<LineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<LineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }

    private void buildSequence(LocalDate datePrelevement, String projetCode, String siteCode, SortedMap<String, SortedMap<LocalTime, List<LineRecord>>> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        LineRecord firstLine = sequenceLines.values().stream()
                .map(m -> m.values()
                .stream()
                .map(l -> l
                .stream()
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null);

        SequenceHauteFrequence sequenceHauteFrequence = sequenceHauteFrequenceDAO.getByDatePrelevementAndProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode).orElse(null);

        if (sequenceHauteFrequence == null) {

            ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode).orElse(null);
            if (projetSite == null) {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode)));
                throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS), projetCode, siteCode));
            }

            if (!siteCode.equals(Utils.createCodeFromString(firstLine.getNomSite()))) {
                InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), firstLine.getPlateformeCode(), firstLine.getOriginalLineNumber(),
                        siteCode));
                throw insertionDatabaseException;
            }

            sequenceHauteFrequence = new SequenceHauteFrequence();
            sequenceHauteFrequence.setDatePrelevement(datePrelevement);

            sequenceHauteFrequence.setVersionFile(versionFile);
        }

        for (Map.Entry<String, SortedMap<LocalTime, List<LineRecord>>> sousSequenceEntry : sequenceLines.entrySet()) {
            String key = sousSequenceEntry.getKey();
            String[] split = key.split(",");
            String plateformeCode = split.length > 2 ? split[0] : "";
            String outilCode = split.length > 2 ? split[1] : "";
            String profondeur = split.length > 2 ? split[2] : "";
            SortedMap<LocalTime, List<LineRecord>> projetLines = sousSequenceEntry.getValue();
            try {
                buildSousSequence(siteCode, plateformeCode, outilCode, profondeur, "commentaire", projetLines, sequenceHauteFrequence, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequenceHauteFrequenceDAO.saveOrUpdate(sequenceHauteFrequence);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getUTCDateTextFromLocalDateTime(datePrelevement, DateUtil.DD_MM_YYYY));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String siteCode, String plateformeCode, String outilsMesureCode, String profondeur, String commentaires, SortedMap<LocalTime, List<LineRecord>> sousSequenceLines, SequenceHauteFrequence sequenceHauteFrequence, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException {

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, siteCode).orElseThrow(()->new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, 0)));
        OutilsMesure outilsMesure = outilsMesureDAO.getByCode(outilsMesureCode).orElse(null);
        if (outilsMesure == null) {
            errorsReport.addException((new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode))));
            throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode));
        }

        SousSequenceHauteFrequence sousSequenceHauteFrequence = new SousSequenceHauteFrequence();
        sousSequenceHauteFrequence.setPlateforme(plateforme);
        sousSequenceHauteFrequence.setSequence(sequenceHauteFrequence);
        sousSequenceHauteFrequence.setCommentaires(commentaires);
        sousSequenceHauteFrequence.setOutilsMesure(outilsMesure);
        Dimension dimension = new Dimension();
        dimension.setZReleve(Strings.isNullOrEmpty(profondeur) ? null : Float.parseFloat(profondeur));
        sousSequenceHauteFrequence.setDimension(dimension);
        sequenceHauteFrequence.getSousSequences().add(sousSequenceHauteFrequence);

        for (Map.Entry<LocalTime, List<LineRecord>> profondeurEntry : sousSequenceLines.entrySet()) {
            LocalTime heurePrelevement = profondeurEntry.getKey();
            List<LineRecord> profondeurLines = profondeurEntry.getValue();
            try {
                buildMesure(heurePrelevement, profondeurLines, sousSequenceHauteFrequence, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(LocalTime heure, List<LineRecord> profondeurLines, SousSequenceHauteFrequence sousSequenceHauteFrequence, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        MesureHauteFrequence mesureHauteFrequence = null;

        mesureHauteFrequence = new MesureHauteFrequence();
        mesureHauteFrequence.setSousSequence(sousSequenceHauteFrequence);
        sousSequenceHauteFrequence.getMesures().add(mesureHauteFrequence);
        mesureHauteFrequence.setHeure(heure);
        mesureHauteFrequence.setLigneFichierEchange(profondeurLines.get(0).getOriginalLineNumber());

        if (profondeurLines.size() > 1) {
            errorsReport.addException(new BusinessException((String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DOUBLON_LINE), mesureHauteFrequence.getLigneFichierEchange()))));
        } else {
            for (LineRecord profondeurLine : profondeurLines) {
                for (VariableValue variableValue : profondeurLine.getVariablesValues()) {
                    ValeurMesureHauteFrequence valeurMesureHauteFrequence = new ValeurMesureHauteFrequence();
                    valeurMesureHauteFrequence.setRealNode(mgaRecorder.getRealNodeById(variableValue.getRealNode().getId()).orElse(null));
                    if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                        valeurMesureHauteFrequence.setValeur(null);
                    } else {

                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurMesureHauteFrequence.setValeur(value);
                    }

                    valeurMesureHauteFrequence.setMesure(mesureHauteFrequence);
                    mesureHauteFrequence.getValeurs().add(valeurMesureHauteFrequence);

                }
            }
        }
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }

}

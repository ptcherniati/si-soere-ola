/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.site;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.typesite.ITypeSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.typesite.TypeSite;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<SiteGLACPE> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_ERROR_CODE_TYPESITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TYPESITE_NOT_FOUND_IN_DB";
    private static final String MSG_USE_POSIX_CHARACTER = "PROPERTY_MSG_USE_POSIX_CHARACTER";

    /**
     *
     */
    protected ISiteGLACPEDAO siteDAO;

    /**
     *
     */
    protected ITypeSiteDAO typeSiteDAO;
    private Properties propertiesNomFR;
    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    private String[] namesTypesSitesPossibles;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(SiteGLACPE.class));

                // On parcourt chaque colonne d'une ligne
                String typeSiteCode = Utils.createCodeFromString(tokenizerValues.nextToken(true));
                String siteNom = tokenizerValues.nextToken(true);
                if (!Utils.isPOSIXFree(siteNom)) {
                    errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_NAME, MSG_USE_POSIX_CHARACTER));
                }
                String description = tokenizerValues.nextToken();

                //afiocca 
                String codeSandreMe = tokenizerValues.nextToken();
                String codeSandrePe = tokenizerValues.nextToken();

                persistSite(errorsReport, typeSiteCode, siteNom, description, codeSandreMe, codeSandrePe);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                siteDAO.remove(siteDAO.getByCode(code)
                        .orElseThrow(() -> new PersistenceException("can't retrieve site")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistSite(ErrorsReport errorsReport, String typeSiteCode, String siteNom, String description, String codeSandreMe, String codeSandrePe) throws PersistenceException {

        TypeSite dbTypeSite = retrieveDBTypeSite(errorsReport, typeSiteCode);
        if (dbTypeSite != null) {
            SiteGLACPE dbSite = (SiteGLACPE) siteDAO.getByCode(Utils.createCodeFromString(siteNom)).orElse(null);

            createOrUpdateSite(siteNom, description, dbTypeSite, dbSite, codeSandreMe, codeSandrePe);
        }
    }

    private void createOrUpdateSite(String siteNom, String description, TypeSite dbTypeSite, SiteGLACPE dbSite, String codeSandreMe, String codeSandrePe) throws PersistenceException {
        if (dbSite == null) {
            createSite(siteNom, description, dbTypeSite, codeSandreMe, codeSandrePe);
        } else {
            updateSite(description, dbTypeSite, dbSite, codeSandreMe, codeSandrePe);
        }
    }

    private void updateSite(String description, TypeSite dbTypeSite, SiteGLACPE dbSite, String codeSandreMe, String codeSandrePe) throws PersistenceException {
        dbSite.setDescription(description);
        dbSite.setTypeSite(dbTypeSite);
        dbSite.setCodeSandreMe(codeSandreMe);
        dbSite.setCodeSandrePe(codeSandrePe);
        siteDAO.saveOrUpdate(dbSite);
    }

    private void createSite(String siteNom, String description, TypeSite dbTypeSite, String codeSandreMe, String codeSandrePe) throws PersistenceException {
        SiteGLACPE site = new SiteGLACPE(siteNom, description);
        site.setTypeSite(dbTypeSite);
        dbTypeSite.getSites().add(site);
        site.setCodeSandreMe(codeSandreMe);
        site.setCodeSandrePe(codeSandrePe);
        siteDAO.saveOrUpdate(site);

    }

    private TypeSite retrieveDBTypeSite(ErrorsReport errorsReport, String typeSiteCode) {
        TypeSite typeSite = typeSiteDAO.getByCode(typeSiteCode).orElse(null);
        if (typeSite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TYPESITE_NOT_FOUND_IN_DB), typeSiteCode));
        }
        return typeSite;
    }

    private void updateNamesTypesSitesPossibles() throws PersistenceException {
        List<TypeSite> typesSites = typeSiteDAO.getAll(TypeSite.class);
        String[] namesTypesSitesPossibles = new String[typesSites.size()];
        int index = 0;
        for (TypeSite typeSite : typesSites) {
            namesTypesSitesPossibles[index++] = typeSite.getName();
        }
        this.namesTypesSitesPossibles = namesTypesSitesPossibles;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteGLACPEDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param typeSiteDAO
     */
    public void setTypeSiteDAO(ITypeSiteDAO typeSiteDAO) {
        this.typeSiteDAO = typeSiteDAO;
    }

    /**
     *
     * @param site
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(SiteGLACPE site) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : site.getTypeSite().getName(), namesTypesSitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : site.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : propertiesNomFR.get(site.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : propertiesNomEN.get(site.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : site.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true)); // ajout de true pour le mandatory 
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : propertiesDescriptionEN.get(site.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : site.getCodeSandreMe(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? EMPTY_STRING : site.getCodeSandrePe(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected List<SiteGLACPE> getAllElements() {
        return siteDAO.getAllBy(SiteGLACPE.class, SiteGLACPE::getCode);
    }

    @Override
    protected ModelGridMetadata<SiteGLACPE> initModelGridMetadata() {
        try {
            propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRANCE);
            propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
            propertiesDescriptionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), "description", Locale.ENGLISH);
            updateNamesTypesSitesPossibles();
        } catch (PersistenceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return super.initModelGridMetadata();
    }

}

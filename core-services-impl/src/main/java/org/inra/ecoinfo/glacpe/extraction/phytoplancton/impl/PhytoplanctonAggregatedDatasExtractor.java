package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.getDVUs;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.getDepthsRequestParam;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.getIntervalsDates;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.getPlatforms;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    private static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "phytoplanctonAggregatedDatas";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "MSG_NO_EXTRACTION_RESULT_BIOVOLUMES";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.genericfilesrepository.impl.messages";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //sort?
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        prepareRequestMetadatas(parameters.getParameters());
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        ((PhytoplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<IntervalDate> datesRequestParamVO = getIntervalsDates(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        List<ValeurMesurePhytoplancton> valeurMesurePhytoplancton = null;

        valeurMesurePhytoplancton = phytoplanctonDAO.extractValueDatas(policyManager.getCurrentUser(), selectedPlateformes, datesRequestParamVO, dvus);
        extractedDatasMap.put(MAP_INDEX_0, valeurMesurePhytoplancton);

        if (valeurMesurePhytoplancton == null || valeurMesurePhytoplancton.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(MAP_INDEX_0, valeurMesurePhytoplancton);
        return extractedDatasMap;
    }

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

}

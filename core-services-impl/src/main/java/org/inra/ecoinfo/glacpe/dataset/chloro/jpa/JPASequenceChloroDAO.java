/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 11:18:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.chloro.ISequenceChloroDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.SequenceChloro;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.SequenceChloro_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;


/**
 * @author "Cédric Anache"
 * 
 **/

public class JPASequenceChloroDAO extends AbstractJPADAO<SequenceChloro> implements ISequenceChloroDAO {

    // Récupére toutes les séquences chlorophylle d'une date et plateforme précise.

    /**
     *
     * @param date
     * @param projetCode
     * @param siteCode
     * @return
     */
        @Override
    public Optional<SequenceChloro> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate date, String projetCode, String siteCode) {
            CriteriaQuery<SequenceChloro> query = builder.createQuery(SequenceChloro.class);
            Root<SequenceChloro> s = query.from(SequenceChloro.class);
        Join<RealNode, RealNode> realNodeSite = s
                .join(SequenceChloro_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        Join<RealNode, Nodeable> projet = realNodeSite.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.nodeable);
        query
                .select(s)
                .where(
                        builder.equal(s.get(SequenceChloro_.date), date),
                        builder.equal(projet.get(Nodeable_.code), projetCode),
                        builder.equal(site.get(Nodeable_.code), siteCode)
                );
        return getOptional(query);
    }
}

package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGlacpeRequestReminder;
import static org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl.PhytoplanctonParameters.*;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonRequestReminderOutputBuilder extends AbstractGlacpeRequestReminder {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "phytoplanctonRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";
    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";

    private static final String MSG_SELECTED_OPTIONS = "PROPERTY_MSG_SELECTED_OPTIONS";

    private static final String MSG_PROPERTY_SOMMATION = "PROPERTY_MSG_PROPERTY_SOMMATION";
    private static final String MSG_PROPERTY_PROPERTY = "PROPERTY_MSG_PROPERTY_PROPERTY";
    private static final String MSG_PROPERTY_AGGREGATED = "PROPERTY_MSG_PROPERTY_AGGREGATED";
    private static final String MSG_GROUP = "PROPERTY_MSG_GROUP";

    private static final String KEYMAP_COMMENTS = "comments";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;
        try {

            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();
            
            printPlateformesSummary(requestMetadatasMap, reminderPrintStream);
            printVariablesSummary(requestMetadatasMap, reminderPrintStream);
            printTaxonsSummary(requestMetadatasMap, reminderPrintStream);
            printPropertiesSummary((Boolean) requestMetadatasMap.getOrDefault(SOMMATION, false), (Boolean) requestMetadatasMap.getOrDefault(PROPRIETE, false), (Boolean) requestMetadatasMap.getOrDefault(AGGREGATED, false), reminderPrintStream);
            printDatesSummary(requestMetadatasMap, reminderPrintStream);
            printComment(reminderPrintStream, requestMetadatasMap);

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }


    private void printPropertiesSummary(Boolean sommation, Boolean propriete, Boolean aggregated, PrintStream reminderPrintStream) {
        reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_SELECTED_OPTIONS));
        if (sommation) {
            MDC.put("extraction.sommation", sommation.toString());
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_PROPERTY_SOMMATION));
        }
        if (propriete) {
            MDC.put("extraction.propriete", propriete.toString());
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_PROPERTY_PROPERTY));
        }
        if (aggregated) {
            MDC.put("extraction.aggregated", aggregated.toString());
            reminderPrintStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_PROPERTY_AGGREGATED));
        }
        reminderPrintStream.println();
    }

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;
    }
}

package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDAO;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonRawsDatas;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author Antoine Schellenberger
 */
public class ZooplanctonRawsDatasOutputBuilderByLine extends AbstractOutputBuilder {

    private static final String PROPERTY_MSG_HEADER_RAW = "MSG_HEADER_RAW_DATA_LINE";
    private static final String PROPERTY_MSG_ALL_STAGES_DEVELOPMENT = "MSG_ALL_STAGES_DEVELOPMENT";
    protected static final String SUFFIX_RAW_DATA = "raw";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_ZOOPLANKTON = "org.inra.ecoinfo.glacpe.extraction.zooplancton.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    protected IProjetDAO projetDAO;
    protected ISiteGLACPEDAO siteGLACPEDAO;

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    /**
     *
     * @param siteGLACPEDAO
     */
    public void setSiteGLACPEDAO(ISiteGLACPEDAO siteGLACPEDAO) {
        this.siteGLACPEDAO = siteGLACPEDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(
            IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().
                add(buildOutput(parameters,
                        ZooplanctonRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        StringBuilder stringBuilder = new StringBuilder();

        try {
            stringBuilder.append(getLocalizationManager().
                    getMessage(BUNDLE_SOURCE_PATH_ZOOPLANKTON,
                            PROPERTY_MSG_HEADER_RAW));
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    private Map<String, List<Taxon>> buildMapParenteTaxonsSelected(
            List<Taxon> selectedTaxons) {
        Map<String, List<Taxon>> mapParenteTaxonsSelected = Maps.newHashMap();

        // On récupère tous les taxons selectionnés racines cad n'ayant pas de parents parmis les autres taxons selectionnés dans la requête
        List<Taxon> rootTaxons = Lists.newLinkedList();
        for (Taxon taxonL1 : selectedTaxons) {
            boolean isRoot = true;
            for (Taxon taxonL2 : selectedTaxons) {
                if (taxonL2.isParentOf(taxonL1)) {
                    isRoot = false;
                    break;
                }
            }
            if (isRoot) {
                rootTaxons.add(taxonL1);
                mapParenteTaxonsSelected.put(taxonL1.getNomLatin(),
                        new LinkedList<Taxon>());
            }
        }

        // On purge la liste des taxons selectionné en enlevant les taxons racines
        List<Taxon> remainingListTaxons = Lists.newLinkedList(selectedTaxons);

        // comment ? 
        remainingListTaxons.removeAll(rootTaxons);

        // On créer la MAP
        for (Taxon taxonRootSelected : rootTaxons) {
            for (Taxon taxonNodeSelected : remainingListTaxons) {
                if (taxonRootSelected.isParentOf(taxonNodeSelected)) {
                    List<Taxon> childrenTaxonRootSelected = mapParenteTaxonsSelected.
                            get(taxonRootSelected.getNomLatin());
                    childrenTaxonRootSelected.add(taxonNodeSelected);
                }
            }
        }

        return mapParenteTaxonsSelected;

    }

    private List<Taxon> holdListFromKeyTaxon(Taxon taxon,
            Map<String, List<Taxon>> mapParenteTaxons) {
        String nomLatin = taxon.getNomLatin();
        if (mapParenteTaxons.containsKey(nomLatin)) {
            return mapParenteTaxons.get(nomLatin);
        } else {
            List<Taxon> listTaxons = new LinkedList<>();
            listTaxons.add(taxon);
            mapParenteTaxons.put(nomLatin,
                    listTaxons);
            return listTaxons;
        }
    }

    /**
     * This map hold the leaf taxon nomLatin as the key and the lists of the
     * taxons selected during extraction that are parents of each leaf.
     *
     * @param taxonsChildren
     * @param taxonsSelectedInRequest
     * @param mapParenteTaxons
     */
    private void buildAndSaveMapParenteTaxonsLeaves(List<Taxon> taxonsChildren, List<Taxon> taxonsSelectedInRequest, Map<String, List<Taxon>> mapParenteTaxons) {
        for (Taxon taxonChild : taxonsChildren) {
            if (taxonChild.getTaxonsEnfants() != null && !taxonChild.
                    getTaxonsEnfants().
                    isEmpty()) {
                buildAndSaveMapParenteTaxonsLeaves(taxonChild.getTaxonsEnfants(),
                        taxonsSelectedInRequest,
                        mapParenteTaxons);
            } else {
                List<Taxon> taxonsParents = holdListFromKeyTaxon(taxonChild,
                        mapParenteTaxons);
                for (Taxon taxonSelected : taxonsSelectedInRequest) {
                    if (taxonSelected.isParentOf(taxonChild) && !taxonsParents.
                            contains(taxonSelected)) {
                        taxonsParents.add(taxonSelected);
                    }
                }
            }
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, File> buildOutput(final IParameter parameters,
            final String cstResultExtractionCode) throws BusinessException, NoExtractionResultException {

        final Map<String, List> results = parameters.getResults().
                get(cstResultExtractionCode);
        if (results == null || (results != null && results.get(
                AbstractOutputBuilder.MAP_INDEX_0).
                isEmpty())) {
            throw new NoExtractionResultException(localizationManager.
                    getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                            NoExtractionResultException.ERROR));
        }
        final String resultsHeader = buildHeader(parameters.getParameters());
        return buildBody(resultsHeader,
                results,
                parameters.getParameters());
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers,
            Map<String, List> resultsDatasMap,
            Map<String, Object> requestMetadatasMap) throws BusinessException {
        // List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES);

        List<ResultExtractionZooplanctonRawsDatas> resultsExtraction = resultsDatasMap.get(MAP_INDEX_0);

        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        Boolean isDetailTaxon = (Boolean) CollectionUtils.isNotEmpty(selectedTaxons);
        Boolean isDetailStageDevelopment = (Boolean) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_DETAIL_DEVELOPMENT_STAGE);
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");
        String msgAllStages = getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_ZOOPLANKTON, PROPERTY_MSG_ALL_STAGES_DEVELOPMENT);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_CSV);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_CSV);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        Map<String, List<Taxon>> mapParenteTaxonsLeaves = Maps.newHashMap();

        buildAndSaveMapParenteTaxonsLeaves(selectedTaxons,
                selectedTaxons,
                mapParenteTaxonsLeaves);

        Map<String, List<Taxon>> mapParenteTaxonsSelected = buildMapParenteTaxonsSelected(
                selectedTaxons);

        Map<KeyMapExtractionL1Zooplancton, Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>>> resultsStructuredMap = buildResultsStructuredExtraction(
                resultsExtraction,
                mapParenteTaxonsLeaves);

        // On construit la liste et on la trie à partir des clés
        List<KeyMapExtractionL1Zooplancton> listKeyMapL1 = new LinkedList<>(
                resultsStructuredMap.keySet());
        Collections.sort(listKeyMapL1);

        for (KeyMapExtractionL1Zooplancton keyMapL1 : listKeyMapL1) {

            String localizedNameProjet = lookupLocalizedProperty(
                    propertiesProjetName,
                    keyMapL1.getProjet());

            String localizedNameSite = lookupLocalizedProperty(
                    propertiesSiteName,
                    keyMapL1.getSite());

            String localizedNamePlateforme = lookupLocalizedProperty(
                    propertiesPlateformeName,
                    keyMapL1.getPlateforme());

            String localizedNameOutilPrelevement = lookupLocalizedProperty(
                    propertiesOutilName,
                    keyMapL1.getOutilPrelevement());

            String localizedNameOutilMesure = lookupLocalizedProperty(
                    propertiesOutilName,
                    keyMapL1.getOutilMesure());

            LocalDate date = keyMapL1.getDate();
            Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxons = resultsStructuredMap.
                    get(keyMapL1);

            List<KeyMapExtractionL2Zooplancton> listKeyMapL2 = new LinkedList<>(
                    mapTaxons.keySet());
            Collections.sort(listKeyMapL2);
            PrintStream rawDataPrintStream = outputPrintStreamMap.get(String.format("%1$s/%2$s/%1$s_%2$s_%3$s",
                    Utils.createCodeFromString(keyMapL1.getProjet()),
                    Utils.createCodeFromString(keyMapL1.getSite()),
                    SUFFIX_FILENAME_CSV
            ));
            mainProcessOuput(isDetailTaxon,
                    isDetailStageDevelopment,
                    msgAllStages,
                    mapParenteTaxonsSelected,
                    keyMapL1,
                    localizedNameProjet,
                    localizedNameSite,
                    localizedNamePlateforme,
                    localizedNameOutilPrelevement,
                    localizedNameOutilMesure,
                    date,
                    mapTaxons,
                    listKeyMapL2,
                    rawDataPrintStream);

        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void mainProcessOuput(Boolean isDetailTaxon,
            Boolean isDetailStageDevelopment,
            String msgAllStages,
            Map<String, List<Taxon>> mapParenteTaxonsSelected,
            KeyMapExtractionL1Zooplancton keyMapL1,
            String localizedNameProjet,
            String localizedNameSite,
            String localizedNamePlateforme,
            String localizedNameOutilPrelevement,
            String localizedNameOutilMesure,
            LocalDate date,
            Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxons,
            List<KeyMapExtractionL2Zooplancton> listKeyMapL2,
            PrintStream rawDataPrintStream) {

        for (KeyMapExtractionL2Zooplancton keyMapL2 : listKeyMapL2) {

            if (mapParenteTaxonsSelected.
                    containsKey(keyMapL2.getNomLatinTaxon())) {

                String line = String.format(
                        "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                        localizedNameProjet,
                        localizedNameSite,
                        localizedNamePlateforme,
                        DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                        localizedNameOutilPrelevement,
                        localizedNameOutilMesure,
                        keyMapL1.getProfondeurMin(),
                        keyMapL1.getProfondeurMax(),
                        keyMapL1.getNomDeterminateur(),
                        keyMapL1.getVolumeSedimente(),
                        keyMapL2.getNomLatinTaxon(),
                        msgAllStages,
                        keyMapL2.getNomVariable(),
                        computeSumVariable(mapTaxons.get(keyMapL2)));
                rawDataPrintStream.println(line);

                processOutputChildTaxonSelected(msgAllStages,
                        mapParenteTaxonsSelected,
                        keyMapL1,
                        localizedNameProjet,
                        localizedNameSite,
                        localizedNamePlateforme,
                        localizedNameOutilPrelevement,
                        localizedNameOutilMesure,
                        date,
                        mapTaxons,
                        rawDataPrintStream,
                        keyMapL2);

                processOutputDetailsTaxons(isDetailTaxon,
                        isDetailStageDevelopment,
                        keyMapL1,
                        localizedNameProjet,
                        localizedNameSite,
                        localizedNamePlateforme,
                        localizedNameOutilPrelevement,
                        localizedNameOutilMesure,
                        date,
                        mapTaxons,
                        rawDataPrintStream,
                        keyMapL2,
                        line);
            }
        }
    }

    private void processOutputDetailsTaxons(Boolean isDetailTaxon,
            Boolean isDetailStageDevelopment,
            KeyMapExtractionL1Zooplancton keyMapL1,
            String localizedNameProjet,
            String localizedNameSite,
            String localizedNamePlateforme,
            String localizedNameOutilPrelevement,
            String localizedNameOutilMesure,
            LocalDate date,
            Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxons,
            PrintStream rawDataPrintStream,
            KeyMapExtractionL2Zooplancton keyMapL2,
            String line) {
        if (isDetailTaxon) {
            Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>> mapResults = mapTaxons.
                    get(keyMapL2);
            List<KeyMapExtractionL3Zooplancton> listKeyMapL3 = new LinkedList<>(
                    mapResults.keySet());

            if (isDetailStageDevelopment) {
                Map<String, List<ResultExtractionZooplanctonRawsDatas>> subMap = new HashMap<>();

                for (KeyMapExtractionL3Zooplancton keyMapL3 : listKeyMapL3) {
                    String nomLatin = keyMapL3.getNomLatinTaxon();
                    if (!subMap.containsKey(nomLatin)) {
                        subMap.put(nomLatin,
                                new LinkedList<ResultExtractionZooplanctonRawsDatas>());
                    }
                    subMap.get(nomLatin).
                            add(mapResults.get(keyMapL3).
                                    get(0));

                }
                for (String taxon : subMap.keySet()) {
                    processOutputStageDevelopment(keyMapL1,
                            localizedNameProjet,
                            localizedNameSite,
                            localizedNamePlateforme,
                            localizedNameOutilPrelevement,
                            localizedNameOutilMesure,
                            date,
                            keyMapL2,
                            line,
                            subMap.get(taxon),
                            rawDataPrintStream);

                }
            } else {

                Map<String, List<ResultExtractionZooplanctonRawsDatas>> subMapResult = new HashMap<>();

                for (KeyMapExtractionL3Zooplancton key : mapResults.keySet()) {

                    if (!subMapResult.containsKey(key.getNomLatinTaxon())) {
                        subMapResult.put(key.getNomLatinTaxon(),
                                new LinkedList<ResultExtractionZooplanctonRawsDatas>());
                    }
                    subMapResult.get(key.getNomLatinTaxon()).
                            addAll(mapResults.get(key));
                }

                for (String taxonName : subMapResult.keySet()) {

                    line = String.format(
                            "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                            localizedNameProjet,
                            localizedNameSite,
                            localizedNamePlateforme,
                            DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                            localizedNameOutilPrelevement,
                            localizedNameOutilMesure,
                            keyMapL1.getProfondeurMin(),
                            keyMapL1.getProfondeurMax(),
                            keyMapL1.getNomDeterminateur(),
                            keyMapL1.getVolumeSedimente(),
                            taxonName,
                            "tous stades confondus",
                            keyMapL2.getNomVariable(),
                            computeSumVariableForStageDevelopment(subMapResult.
                                    get(taxonName)));

                    rawDataPrintStream.println(line);
                }

            }
        }
    }

    private String processOutputStageDevelopment(
            KeyMapExtractionL1Zooplancton keyMapL1,
            String localizedNameProjet,
            String localizedNameSite,
            String localizedNamePlateforme,
            String localizedNameOutilPrelevement,
            String localizedNameOutilMesure,
            LocalDate date,
            KeyMapExtractionL2Zooplancton keyMapL2,
            String line,
            List<ResultExtractionZooplanctonRawsDatas> results,
            PrintStream rawDataPrintStream) {

        for (ResultExtractionZooplanctonRawsDatas result : results) {

            line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                    localizedNameProjet,
                    localizedNameSite,
                    localizedNamePlateforme,
                    DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                    localizedNameOutilPrelevement,
                    localizedNameOutilMesure,
                    result.getProfondeurMin(),
                    result.getProfondeurMax(),
                    result.getNomDeterminateur(),
                    result.getVolumeSedimente(),
                    result.getNomLatinTaxon(),
                    result.getStadeDeveloppement(),
                    result.getNomVariable(),
                    result.getValue());

            rawDataPrintStream.println(line);

        }
        return line;

    }

    private void processOutputChildTaxonSelected(String msgAllStages,
            Map<String, List<Taxon>> mapParenteTaxonsSelected,
            KeyMapExtractionL1Zooplancton keyMapL1,
            String localizedNameProjet,
            String localizedNameSite,
            String localizedNamePlateforme,
            String localizedNameOutilPrelevement,
            String localizedNameOutilMesure,
            LocalDate date,
            Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxons,
            PrintStream rawDataPrintStream,
            KeyMapExtractionL2Zooplancton keyMapL2) {
        String line;
        for (Taxon childTaxonSelected : mapParenteTaxonsSelected.get(keyMapL2.
                getNomLatinTaxon())) {
            KeyMapExtractionL2Zooplancton childLevel2 = new KeyMapExtractionL2Zooplancton(
                    childTaxonSelected.getNomLatin(),
                    keyMapL2.getNomVariable());
            if (mapTaxons.containsKey(childLevel2)) {
                line = String.
                        format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                                localizedNameProjet,
                                localizedNameSite,
                                localizedNamePlateforme,
                                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                                localizedNameOutilPrelevement,
                                localizedNameOutilMesure,
                                keyMapL1.getProfondeurMin(),
                                keyMapL1.getProfondeurMax(),
                                keyMapL1.getNomDeterminateur(),
                                keyMapL1.getVolumeSedimente(),
                                childTaxonSelected.getNomLatin(),
                                msgAllStages,
                                keyMapL2.getNomVariable(),
                                computeSumVariable(mapTaxons.get(childLevel2)));
                rawDataPrintStream.println(line);
            }
        }

    }

    private Float computeSumVariableForStageDevelopment(
            List<ResultExtractionZooplanctonRawsDatas> resultsExtractions) {
        Float sum = 0f;

        for (ResultExtractionZooplanctonRawsDatas result : resultsExtractions) {
            sum += result.getValue();
        }
        return sum;
    }

    private Float computeSumVariable(
            Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>> map) {
        Float sum = 0f;
        for (KeyMapExtractionL3Zooplancton keyMapL3 : map.keySet()) {
            sum += computeSumVariableForStageDevelopment(map.get(keyMapL3));
        }
        return sum;
    }

    private String lookupLocalizedProperty(Properties properties,
            String defaultValue) {
        return properties.getProperty(
                defaultValue,
                properties.getProperty(
                        Utils.createCodeFromString(defaultValue),
                        defaultValue
                )
        );
    }

    private Map<KeyMapExtractionL1Zooplancton, Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>>> buildResultsStructuredExtraction(
            List<ResultExtractionZooplanctonRawsDatas> resultsExtraction,
            Map<String, List<Taxon>> mapParenteTaxonsLeaves) {

        Map<KeyMapExtractionL1Zooplancton, Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>>> mapResultsStructuredMap
                = new HashMap<>();

        for (ResultExtractionZooplanctonRawsDatas resultExtraction : resultsExtraction) {

            KeyMapExtractionL1Zooplancton keyMapL1
                    = new KeyMapExtractionL1Zooplancton(
                            resultExtraction.getPlateforme(),
                            resultExtraction.getDate(),
                            resultExtraction.getProjet(),
                            resultExtraction.getSite(),
                            resultExtraction.getOutilPrelevement(),
                            resultExtraction.getOutilMesure(),
                            resultExtraction.getVolumeSedimente(),
                            resultExtraction.getProfondeurMin(),
                            resultExtraction.getProfondeurMax(),
                            resultExtraction.getNomDeterminateur());

            Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxons
                    = holdMapTaxon(
                            mapResultsStructuredMap,
                            keyMapL1);

            for (Taxon taxonParent : mapParenteTaxonsLeaves.get(
                    resultExtraction.getNomLatinTaxon())) {

                Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>> mapExtractionL2;

                KeyMapExtractionL2Zooplancton keyMapL2 = new KeyMapExtractionL2Zooplancton(
                        taxonParent.getNomLatin(),
                        resultExtraction.getNomVariable());

                if (mapTaxons.containsKey(keyMapL2)) {
                    mapExtractionL2 = mapTaxons.get(keyMapL2);
                } else {
                    mapExtractionL2 = new HashMap<>();
                    mapTaxons.put(keyMapL2,
                            mapExtractionL2);
                }

                KeyMapExtractionL3Zooplancton keyMapL3 = new KeyMapExtractionL3Zooplancton(
                        resultExtraction.getStadeDeveloppement(),
                        resultExtraction.getNomLatinTaxon());
                List<ResultExtractionZooplanctonRawsDatas> resultLeaf;
                if (mapExtractionL2.containsKey(keyMapL3)) {
                    resultLeaf = mapExtractionL2.get(keyMapL3);
                } else {
                    resultLeaf = Lists.newLinkedList();
                    mapExtractionL2.put(keyMapL3,
                            resultLeaf);
                }
                resultLeaf.add(resultExtraction);
            }
        }
        return mapResultsStructuredMap;
    }

    private Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> holdMapTaxon(
            Map<KeyMapExtractionL1Zooplancton, Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>>> mapResultsStructuredMap,
            KeyMapExtractionL1Zooplancton keyMapL1) {

        Map<KeyMapExtractionL2Zooplancton, Map<KeyMapExtractionL3Zooplancton, List<ResultExtractionZooplanctonRawsDatas>>> mapTaxon;

        if (mapResultsStructuredMap.containsKey(keyMapL1)) {
            mapTaxon = mapResultsStructuredMap.get(keyMapL1);
        } else {
            mapTaxon = new HashMap<>();
            mapResultsStructuredMap.put(keyMapL1,
                    mapTaxon);
        }
        return mapTaxon;
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

}

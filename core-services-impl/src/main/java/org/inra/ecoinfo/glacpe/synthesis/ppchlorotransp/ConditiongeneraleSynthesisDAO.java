/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.ppchlorotransp;

import java.util.stream.Stream;
import org.inra.ecoinfo.glacpe.synthesis.conditionprelevement.SynthesisDatatype;
import org.inra.ecoinfo.glacpe.synthesis.conditionprelevement.SynthesisValue;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class ConditiongeneraleSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     *
     */
    public static String SYNTHESIS_REQUEST = "insert into conditionprelevementsynthesisvalue(id, date, idnode, ismean, site, valuefloat, valuestring, variable, siteid, projetid, plateformeid, variableid, profondeurs)\n"
            + "(select \n"
            + "nextval('hibernate_sequence') id,\n"
            + "m.date_prelevement date,\n"
            + "dnv.branch_node_id idNode,\n"
            + "false isMean,\n"
            + "rns.path site,\n"
            + "avg(v.valeur) valueFloat,\n"
            + "null valueString,\n"
            + "nddvu.code variable,\n"
            + "rns.id_nodeable siteid,\n"
            + "rnp.id_nodeable projetid,\n"
            + "m.loc_id plateformeid,\n"
            + "dvu.var_id variableid,\n"
            + "'{}' profondeurs\n"
            + "from valeur_condition_prelevement_vmconditionprelevement v\n"
            + "join composite_node_data_set dnv on dnv.realnode= v.id\n"
            + "JOIN mesure_condition_prelevement_mconditionprelevement m ON m.mconditionprelevement_id = v.mconditionprelevement_id\n"
            + "\n"
            + "JOIN realnode rnv ON rnv.id = v.id\n"
            + "join composite_nodeable nddvu on nddvu.id = rnv.id_nodeable\n"
            + "join datatype_variable_unite_glacpe_dvug dvu ON rnv.id_nodeable = vdt_id\n"
            + "join realnode rnd ON rnd.id = rnv.id_parent_node\n"
            + "JOIN realnode rnt ON rnt.id = rnd.id_parent_node\n"
            + "JOIN realnode rns ON rns.id = rnt.id_parent_node\n"
            + "JOIN realnode rnts ON rnts.id = rns.id_parent_node\n"
            + "JOIN realnode rnp ON rnp.id = rnts.id_parent_node\n"
            + "group by m.date_prelevement, rns.path, dnv.branch_node_id, nddvu.code, m.loc_id, rns.id_nodeable, rnp.id_nodeable, rnp.id_nodeable,  dvu.var_id)";

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        entityManager.createNativeQuery(SYNTHESIS_REQUEST).executeUpdate();
        return Stream.empty();
    }

}

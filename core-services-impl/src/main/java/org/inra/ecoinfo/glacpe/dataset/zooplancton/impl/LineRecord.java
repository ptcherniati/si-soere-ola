package org.inra.ecoinfo.glacpe.dataset.zooplancton.impl;

import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;


class LineRecord {

    private String nomSite;
    private String plateformeCode;
    private LocalDate datePrelevement;
    private Float profondeurMin;
    private Float profondeurMax;
    private String outilsMesureCode;
    private String outilsPrelevementCode;
    private String determinateur;
    private String stadeDeveloppement;
    private Float volumeSedimente;
    private String taxonCode;
    private Long originalLineNumber;
    private String projetCode;
    private List<VariableValue> variablesValues;

    public LineRecord() {

    }

    public LineRecord(String nomSite, String plateformeCode, LocalDate datePrelevement, Float profondeurMin, Float profondeurMax, String outilsMesureCode, String outilsPrelevementCode, String determinateur, Float volumeSedimente,
        String stadeDeveloppement, String taxonCode, List<VariableValue> variablesValues, Long originalLineNumber, String projetCode)
    {
        super();
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.datePrelevement = datePrelevement;
        this.outilsMesureCode = outilsMesureCode;
        this.outilsPrelevementCode = outilsPrelevementCode;
        this.determinateur = determinateur;
        this.volumeSedimente = volumeSedimente;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.stadeDeveloppement = stadeDeveloppement;
        this.taxonCode = taxonCode;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
        this.projetCode = projetCode;
    }

    public void copy(LineRecord line) {
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.datePrelevement = line.getDatePrelevement();
        this.profondeurMin = line.getProfondeurMin();
        this.profondeurMax = line.getProfondeurMax();
        this.outilsMesureCode = line.getOutilsMesureCode();
        this.outilsPrelevementCode = line.getOutilsPrelevementCode();
        this.determinateur = line.getDeterminateur();
        this.volumeSedimente = line.getVolumeSedimente();
        this.stadeDeveloppement = line.getStadeDeveloppement();
        this.variablesValues = line.getVariablesValues();
        this.taxonCode = line.getTaxonCode();

        this.originalLineNumber = line.getOriginalLineNumber();
        this.projetCode = getProjetCode();
    }

    public String getStadeDeveloppement() {
        return stadeDeveloppement;
    }

    public String getNomSite() {
        return nomSite;
    }

    public String getPlateformeCode() {
        return plateformeCode;
    }

    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    public String getProjetCode() {
        return projetCode;
    }

    public String getOutilsMesureCode() {
        return outilsMesureCode;
    }

    public Float getProfondeurMin() {
        return profondeurMin;
    }

    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    public Float getProfondeurMax() {
        return profondeurMax;
    }

    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    public String getDeterminateur() {
        return determinateur;
    }

    public void setDeterminateur(String determinateur) {
        this.determinateur = determinateur;
    }

    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    public String getTaxonCode() {
        return taxonCode;
    }

    public void setTaxonCode(String taxonCode) {
        this.taxonCode = taxonCode;
    }

    public String getOutilsPrelevementCode() {
        return outilsPrelevementCode;
    }

    public void setOutilsPrelevementCode(String outilsPrelevementCode) {
        this.outilsPrelevementCode = outilsPrelevementCode;
    }

    @Override
    public String toString() {
        return "LineRecord{" + "nomSite=" + nomSite + ", plateformeCode=" + plateformeCode + ", datePrelevement=" + datePrelevement + ", outilsPrelevementCode=" + outilsPrelevementCode + ", stadeDeveloppement=" + stadeDeveloppement + ", taxonCode=" + taxonCode + ", projetCode=" + projetCode + '}';
    }


    
    
}

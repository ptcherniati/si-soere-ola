package org.inra.ecoinfo.glacpe.dataset.sondemulti.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISequenceSondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;


/**
 * @author "Guillaume Enrico"
 * 
 */

public class JPASequenceSondeMultiDAO extends AbstractJPADAO<SequenceSondeMulti> implements ISequenceSondeMultiDAO {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     */
    @Override
    public Optional<SequenceSondeMulti> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode)  {
        CriteriaQuery<SequenceSondeMulti> query = builder.createQuery(SequenceSondeMulti.class);
        Root<SequenceSondeMulti> s = query.from(SequenceSondeMulti.class);
        Join<Dataset, RealNode> rnd = s.join(SequenceSondeMulti_.versionFile).join(VersionFile_.dataset).join(Dataset_.realNode);
        Join<RealNode, RealNode> rns = rnd.join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> site = rns.join(RealNode_.nodeable);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> projet = rnp.join(RealNode_.nodeable);
        
        query
                .select(s)
                .where(
                        builder.and(
                                builder.equal(s.get(SequenceSondeMulti_.datePrelevement), datePrelevement),
                                builder.equal(site.get(Nodeable_.code), siteCode),
                                builder.equal(projet.get(Nodeable_.code), projetCode)
                        )
                );
        return getOptional(query);
    }

}

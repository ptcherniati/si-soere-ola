package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.DefaultPPChloroTranspExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    private static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "ppAggregatedDatas";

    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";

    /**
     *
     */
    protected IPPDAO productionPrimaireDAO;

    /**
     *
     * @param productionPrimaireDAO
     */
    public void setProductionPrimaireDAO(IPPDAO productionPrimaireDAO) {
        this.productionPrimaireDAO = productionPrimaireDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        List<IntervalDate> intervalDates = getIntervalsDates(requestMetadatas);

        List<ValeurMesurePP> valeursMesuresPP = null;
        valeursMesuresPP = productionPrimaireDAO.extractValueDatas(policyManager.getCurrentUser(), selectedPlateformes, intervalDates, depthRequestParamVO, dvus);
        extractedDatasMap.put(PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE, valeursMesuresPP);
        if (valeursMesuresPP == null || valeursMesuresPP.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT));
        }
        return extractedDatasMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> extractdatas = extractDatas(parameters.getParameters());
        ((PPChloroTranspParameter) parameters).getResults()
                .computeIfAbsent(CST_RESULT_EXTRACTION_CODE, k->extractdatas)
                .putAll(extractdatas);
        Map<String, Boolean> hasResultMap = (Map<String, Boolean>) ((PPChloroTranspParameter) parameters).getParameters()
                .computeIfAbsent(DefaultPPChloroTranspExtractor.CST_RESULTS, k->new HashMap<String, Boolean>());
                hasResultMap.put(CST_RESULT_EXTRACTION_CODE, extractdatas.get(CST_RESULT_EXTRACTION_CODE) != null);
        ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(MAP_INDEX_0, extractdatas.get(CST_RESULT_EXTRACTION_CODE));
    }
}

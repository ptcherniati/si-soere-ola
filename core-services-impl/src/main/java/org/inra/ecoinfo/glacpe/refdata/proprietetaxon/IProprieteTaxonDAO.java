/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.proprietetaxon;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public interface IProprieteTaxonDAO extends IDAO<ProprieteTaxon> {

    /**
     *
     * @param code
     * @return
     */
    Optional<ProprieteTaxon> getByCode(String code);

    /**
     *
     * @return
     */
    List<ProprieteTaxon> getAllQualitativeValues();

    /**
     *
     * @param type
     * @return
     */
    List<ProprieteTaxon> getAllProprietesTaxonsByType(String type);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.hautefrequence;

import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.IHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.IHauteFrequenceDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author mylene
 */
public class DefaultHauteFrequenceDatatypeManager extends MO implements IHauteFrequenceDatatypeManager{
    IHauteFrequenceDAO hauteFrequenceDAO;

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return hauteFrequenceDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return hauteFrequenceDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }
}

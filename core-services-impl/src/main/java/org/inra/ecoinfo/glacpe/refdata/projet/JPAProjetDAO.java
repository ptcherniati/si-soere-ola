/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.projet;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAProjetDAO extends AbstractJPADAO<Projet> implements IProjetDAO {

    /**
     *
     * @param code
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<Projet> getByCode(String code) {
        CriteriaQuery<Projet> query = builder.createQuery(Projet.class);
        Root<Projet> projet = query.from(Projet.class);
        query
                .select(projet)
                .where(builder.equal(projet.get(Projet_.code), code));
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Projet> getAll() {
       return getAllBy(Projet.class, Projet::getName);
    }

}

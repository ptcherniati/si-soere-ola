package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import java.util.List;
import org.inra.ecoinfo.glacpe.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;

/**
 *
 * @author ptcherniati
 */
public interface IGroupeVariableBuilder {

    /**
     *
     */
    static String BEAN_ID = "groupeVariableBuilder";

    /**
     *
     * @param variables
     * @return
     */
    List<GroupeVariableVO> build(List<VariableGLACPE> variables);
}

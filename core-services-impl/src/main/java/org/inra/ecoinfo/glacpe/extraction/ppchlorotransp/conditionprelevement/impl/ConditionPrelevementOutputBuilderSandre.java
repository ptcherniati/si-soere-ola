/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author afiocca
 */
public class ConditionPrelevementOutputBuilderSandre extends AbstractConditionGeneraleDatasOutputBuilder{
    
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.conditionprelevement.messages";
    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER_SANDRE";
    private static final String FILENAME_INFO_SUP = "Informations_supplementaires";
    private static final String CODE_EXTRACTIONTYPE_CONDITION_PRELEVEMENTS = "conditions_prelevements";
    
    protected IPlateformeDAO plateformeDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        try {

            Iterator<MesureConditionGenerale> mesuresConditionGenerale = resultsDatasMap.get(MAP_INDEX_0).iterator();
            DataType datatype = datatypeDAO.getByCode(CODE_EXTRACTIONTYPE_CONDITION_PRELEVEMENTS).orElseThrow(()->new PersistenceException("Can't find datatype"));

            Map<DatatypeVariableUniteGLACPE, String> variablesMapSandreElement = new HashMap<>(); 
            Map<String, String> variablesMapSandreContext = new HashMap<>(); 
           
            Map<String, String> sitesMapSandreElement = new HashMap<>(); 
            Map<String, String> sitesMapSandreContext = new HashMap<>(); 
            
            Map<String, String> platformsMapSite = new HashMap<>();
            Map<String, String> platformsMapSandreElement = new HashMap<>(); 
            Map<String, String> platformsMapSandreContext = new HashMap<>(); 
            Map<String, Float> platformsMapLongitude = new HashMap<>(); 
            Map<String, Float> platformsMapLatitude = new HashMap<>(); 
            Map<String, Float> platformsMapAltitude = new HashMap<>(); 
            

            Map<String, File> reminderMap = new HashMap<String, File>();
            File reminderFile = buildOutputFile(FILENAME_INFO_SUP, EXTENSION_CSV);
            PrintStream printStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER));

            while (mesuresConditionGenerale.hasNext()) {
                final MesureConditionGenerale mesureConditionGenerale = mesuresConditionGenerale.next();
                
                Iterator<ValeurConditionGenerale> valeursMesureConditionGenerale = mesureConditionGenerale.getValeurs().iterator();
                while (valeursMesureConditionGenerale.hasNext()) {
                    ValeurConditionGenerale valeurMesureConditionGenerale = valeursMesureConditionGenerale.next();
                    final DatatypeVariableUniteGLACPE dvu = (DatatypeVariableUniteGLACPE) valeurMesureConditionGenerale.getRealNode().getNodeable();
                    VariableGLACPE variable = dvu.getVariable();
                    final String nameVariable = variable.getName();
                    final String codeSandreElementVariable = variable.getCodeSandre();
                    final String codeSandreContextVariable = variable.getContexte();
                    
                    final String nameSite = valeurMesureConditionGenerale.getMesure().getSite().getName();
                    final String codeSandreElementSite = ((SiteGLACPE) valeurMesureConditionGenerale.getMesure().getSite()).getCodeSandrePe();
                    final String codeSandreContextSite = ((SiteGLACPE) valeurMesureConditionGenerale.getMesure().getSite()).getCodeSandreMe();
    
                    final String namePlatform = valeurMesureConditionGenerale.getMesure().getPlateforme().getName();
                    final String codeSandreElementPlatforms = valeurMesureConditionGenerale.getMesure().getPlateforme().getCodeSandre();
                    final String codeSandreContextPlatforms = valeurMesureConditionGenerale.getMesure().getPlateforme().getContexte();
                    final Float longitudePlatform = valeurMesureConditionGenerale.getMesure().getPlateforme().getLongitude();
                    final Float latitudePlatform = valeurMesureConditionGenerale.getMesure().getPlateforme().getLatitude();
                    final Float altitudePlatform = valeurMesureConditionGenerale.getMesure().getPlateforme().getAltitude();
                 

                    variablesMapSandreElement.put(dvu, codeSandreElementVariable);
                    variablesMapSandreContext.put(nameVariable, codeSandreContextVariable);
                   
                    sitesMapSandreElement.put(nameSite, codeSandreElementSite);
                    sitesMapSandreContext.put(nameSite, codeSandreContextSite);
                    
                    platformsMapSite.put(namePlatform,nameSite);
                    platformsMapSandreElement.put(namePlatform, codeSandreElementPlatforms);
                    platformsMapSandreContext.put(namePlatform, codeSandreContextPlatforms);
                    platformsMapLongitude.put(namePlatform, longitudePlatform);
                    platformsMapLatitude.put(namePlatform, latitudePlatform);
                    platformsMapAltitude.put(namePlatform, altitudePlatform);
                    
                }
                mesuresConditionGenerale.remove();
            }       
            mapSitesSandreCSV(sitesMapSandreElement, sitesMapSandreContext,printStream);
            
            mapPlatformsCoordCSV(platformsMapSite,platformsMapSandreElement,platformsMapSandreContext, platformsMapLongitude, platformsMapLatitude, platformsMapAltitude, printStream);

            mapVariablesSandreCSV(variablesMapSandreElement,variablesMapSandreContext, printStream);

            mapUnitSandreCSV(variablesMapSandreElement, datatype, printStream);

            reminderMap.put(FILENAME_INFO_SUP, reminderFile);

            printStream.flush();
            printStream.close();

            return reminderMap;
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    
    
    /**
     * 
     * @param variablesMapSandreElement
     * @param variablesMapSandreContext
     * @param printStream 
     */
    private void mapVariablesSandreCSV(Map<DatatypeVariableUniteGLACPE, String> variablesMapSandreElement, Map<String, String> variablesMapSandreContext, PrintStream printStream) {
        
        for (DatatypeVariableUniteGLACPE dvu : variablesMapSandreElement.keySet()) {
            final String name = dvu.getVariable().getName();
            
            if (variablesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"variable\";\"%s\";\"%s\";\"%s\";", name, variablesMapSandreElement.get(name),variablesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"variable\";\"%s\";", name));
            }
        }
    }

    /**
     * 
     * @param sitesMapSandreElem
     * @param sitesMapSandreCont
     * @param printStream 
     */
    private void mapSitesSandreCSV(Map<String, String> sitesMapSandreElement, Map<String, String> sitesMapSandreContext, PrintStream printStream) {
        for (String name : sitesMapSandreElement.keySet()) {
            if (sitesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"site\";\"%s\";\"%s\";\"%s\";", name, sitesMapSandreElement.get(name),sitesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"site\";\"%s\";", name));
            }
        }
    }
    /**
     * 
     * @param sitesMapSandreElem
     * @param platformsMapLongitude
     * @param platformsMapLatitude
     * @param platformsMapAltitude
     * @param printStream 
     */
    private void mapPlatformsCoordCSV(Map<String, String> platformsMapSite, Map<String, String> platformsMapSandreElement, Map<String, String> platformsMapSandreContext, Map<String, Float> platformsMapLongitude, Map<String, Float> platformsMapLatitude, Map<String, Float> platformsMapAltitude, PrintStream printStream) {
        
        for (String name : platformsMapLongitude.keySet()) {

            if (platformsMapLongitude.get(name) != null) {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";\"%s\";\"%s\";\"%s\";\"%s\";\"%s\";", name, platformsMapSite.get(name),platformsMapSandreElement.get(name),platformsMapSandreContext.get(name), platformsMapLongitude.get(name), platformsMapLatitude.get(name), platformsMapAltitude.get(name)));
            } else {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";", name, platformsMapSite.get(name)));
            }
        }
    }

    /**
     *
     * @param variablesMap 
     * @param datatype
     * @param printStream
     * @throws PersistenceException
     */
    private void mapUnitSandreCSV(Map<DatatypeVariableUniteGLACPE, String> variablesMap, DataType datatype, PrintStream printStream) throws PersistenceException {

        for (DatatypeVariableUniteGLACPE datatypeVariableUnite : variablesMap.keySet()) {
            final String uniteName = datatypeVariableUnite.getUnite().getName();
            final String variableName = datatypeVariableUnite.getVariable().getName();
            if (datatypeVariableUnite.getCodeSandre() != null) {
                printStream.println(String.format("\"Unité(variable)\";\"%s(%s)\";\"%s\";\"%s\"", uniteName, variableName, datatypeVariableUnite.getCodeSandre(),datatypeVariableUnite.getContexte()));
            } else {
                printStream.println(String.format("\"Unité(variable)\";\"%s(%s)\"", uniteName, variableName));
            }
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ConditionGeneraleDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }
    
    
}

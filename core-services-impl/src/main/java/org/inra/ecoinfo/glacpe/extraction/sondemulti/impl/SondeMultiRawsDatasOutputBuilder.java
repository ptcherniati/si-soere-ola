package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiRawsDatasOutputBuilder extends AbstractSondeMultiRawsDatasOutputBuilder {

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String CODE_DATATYPE_SONDE_MULTI = "sonde_multiparametres";

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> dvUs = getDVUs(requestMetadatas);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA)));
            }

            for (DatatypeVariableUniteGLACPE dvu : dvUs) {
                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
                String uniteNom = dvu.getUnite().getName();
                stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);
        Comparator<MesureSondeMulti> comparator = (m1, m2) -> {
            if (m1.getSousSequence().getSequence().getDatePrelevement().compareTo(m2.getSousSequence().getSequence().getDatePrelevement()) != 0) {
                return m1.getSousSequence().getSequence().getDatePrelevement().compareTo(m2.getSousSequence().getSequence().getDatePrelevement());
            }
            if (m1.getSousSequence().getPlateforme().compareTo(m2.getSousSequence().getPlateforme()) != 0) {
                return m1.getSousSequence().getPlateforme().compareTo(m2.getSousSequence().getPlateforme());
            }
            if (m1.getProfondeur() == null) {
                return m1.getId().compareTo(m2.getId());
            }
            if (m2.getProfondeur() != null && m1.getProfondeur().compareTo(m2.getProfondeur()) != 0) {
                return m1.getProfondeur().compareTo(m2.getProfondeur());
            }
            if (m1.getSousSequence().getOutilsMesure() == null) {
                return m1.getId().compareTo(m2.getId());
            }
            return m1.getSousSequence().getOutilsMesure().compareTo(m2.getSousSequence().getOutilsMesure());
        };
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<MesureSondeMulti>>> mesures = ((List<MesureSondeMulti>) resultsDatasMap.get(MAP_INDEX_0))
                .stream()
                .collect(
                        Collectors.groupingBy(
                                m -> LacsUtils.getProjetFromDataset(m.getSousSequence().getSequence().getVersionFile().getDataset()),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> LacsUtils.getSiteFromDataset(m.getSousSequence().getSequence().getVersionFile().getDataset()),
                                        TreeMap::new,
                                        Collectors.toCollection(
                                                () -> new TreeSet<>(comparator))
                                )
                        )
                );
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_RAW_DATA);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_CSV);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> projetEntry : selectedPlateformes.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = projetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : mapBySite.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = siteEntry.getValue();
                String typeSite = site.getTypeSite().getCode();
                outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_RAW_DATA)).println(buildHeader(requestMetadatasMap));
                Iterator<MesureSondeMulti> iterator = mesures
                        .getOrDefault(projet, new TreeMap<>())
                        .getOrDefault(site, new TreeSet<>())
                        .iterator();
                while (iterator.hasNext()) {
                    final MesureSondeMulti mesureSondeMulti = iterator.next();
                    PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_RAW_DATA));
                    String line = "";
                    String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getCode());
                    String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getCode());
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureSondeMulti.getSousSequence().getPlateforme().getCode(), mesureSondeMulti.getSousSequence().getPlateforme().getName());
                    String localizedOutilName = propertiesOutilName.getProperty(mesureSondeMulti.getSousSequence().getOutilsMesure().getNom());
                    String date = DateUtil.getUTCDateTextFromLocalDateTime(mesureSondeMulti.getSousSequence().getSequence().getDatePrelevement(), DateUtil.DD_MM_YYYY);
                    String heure = mesureSondeMulti.getHeure() != null ? DateUtil.getUTCDateTextFromLocalDateTime(mesureSondeMulti.getHeure(), DateUtil.HH_MM_SS):"";

                    if (depthRequestParamVO.getAllDepth()) {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s",
                                localizedProjetName,
                                localizedSiteName,
                                localizedPlateformeName,
                                localizedOutilName,
                                mesureSondeMulti.getSousSequence().getCommentaires() != null ? mesureSondeMulti.getSousSequence().getCommentaires() : "",
                                date, 
                                heure, 
                                mesureSondeMulti.getProfondeur()
                        );
                    } else {
                        line = String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", 
                                localizedProjetName, 
                                localizedSiteName, 
                                localizedPlateformeName, 
                                localizedOutilName, 
                                mesureSondeMulti.getSousSequence().getCommentaires() != null ? mesureSondeMulti.getSousSequence().getCommentaires() : "", 
                                depthRequestParamVO.getDepthMin(), 
                                depthRequestParamVO.getDepthMax(),
                                date,
                                heure, 
                                mesureSondeMulti.getProfondeur()
                        );
                    }
                    rawDataPrintStream.print(line);

                    Map<Long, Float> valeursMesuresSondeMulti = buildValeurs(mesureSondeMulti.getValeurs());
                    for (DatatypeVariableUniteGLACPE dvu : dvus) {
                        rawDataPrintStream.print(";");
                        Float valeur = valeursMesuresSondeMulti.get(dvu.getId());
                        if (valeur != null) {
                            rawDataPrintStream.print(String.format("%s", valeur));
                        } else {
                            rawDataPrintStream.print("");
                        }
                    }
                    rawDataPrintStream.println();
                    iterator.remove(); // ???? 
                }
            }
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesureSondeMulti> valeurs) {
        return valeurs.stream()
                .filter(v->v.getValeur()!=null)
                .collect(
                        Collectors.toMap(
                                v->v.getRealNode().getNodeable().getId(),
                                v->v.getValeur()
                        )
                );
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, SondeMultiRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class DefaultConditionGeneraleDatatypeManager extends MO implements IConditionGeneraleDatatypeManager {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_CONDITION_PRELEVEMENTS = "conditions_prelevements";


    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return conditionGeneraleDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatformsTransparence() {
        return conditionGeneraleDAO.getAvailablePlatformsTransparence(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return conditionGeneraleDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariablesTransparence(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return conditionGeneraleDAO.getAvailableVariablesTransparence(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Float> getAvailableDepths(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        return conditionGeneraleDAO.getAvailableDepths(policyManager.getCurrentUser(), selectedPlatforms, selectedDates, selectedVariables);
    }

}

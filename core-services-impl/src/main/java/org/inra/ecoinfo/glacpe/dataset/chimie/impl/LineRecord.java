package org.inra.ecoinfo.glacpe.dataset.chimie.impl;


import java.time.LocalDate;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String nomSite;
    private String plateformeCode;
    private LocalDate datePrelevement;
    private String outilPrelevementCode;
    private Float profondeurMin;
    private Float profondeurMax;
    private Float profondeurReelle;
    private LocalDate dateDebutCampagne;
    private LocalDate dateFinCampagne;
    private LocalDate dateReception;
    private String variableCode;
    private String value;
    private Long originalLineNumber;
    private String projetCode;

    /**
     *
     */
    public LineRecord() {

    }

    /**
     *
     * @param nomSite
     * @param plateformeCode
     * @param datePrelevement
     * @param dateDebutCampagne
     * @param dateFinCampagne
     * @param dateReception
     * @param outilPrelevementCode
     * @param profondeurMin
     * @param profondeurMax
     * @param profondeurReelle
     * @param variableCode
     * @param value
     * @param originalLineNumber
     * @param projetCode
     * @param variable
     */
    public LineRecord(String nomSite, String plateformeCode, LocalDate datePrelevement, LocalDate dateDebutCampagne, LocalDate dateFinCampagne, LocalDate dateReception, String outilPrelevementCode, Float profondeurMin, Float profondeurMax, Float profondeurReelle,
        String variableCode, String value, Long originalLineNumber, String projetCode, String variable)
    {
        super();
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.datePrelevement = datePrelevement;
        this.dateDebutCampagne = dateDebutCampagne;
        this.dateFinCampagne = dateFinCampagne;
        this.dateReception = dateReception;
        this.outilPrelevementCode = outilPrelevementCode;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.profondeurReelle = profondeurReelle;
        this.variableCode = variableCode;
        this.value = value;
        this.originalLineNumber = originalLineNumber;
        this.projetCode = projetCode;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.datePrelevement = line.getDatePrelevement();
        this.dateDebutCampagne = line.getDateDebutCampagne();
        this.dateFinCampagne = line.getDateFinCampagne();
        this.dateReception = line.getDateReception();
        this.outilPrelevementCode = line.getOutilPrelevementCode();
        this.profondeurMin = line.getProfondeurMin();
        this.profondeurMax = line.getProfondeurMax();
        this.profondeurReelle = line.getProfondeurReelle();
        this.variableCode = line.getVariableCode();
        this.value = line.getValue();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.projetCode = getProjetCode();
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @return
     */
    public String getOutilPrelevementCode() {
        return outilPrelevementCode;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateDebutCampagne() {
        return dateDebutCampagne;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateFinCampagne() {
        return dateFinCampagne;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateReception() {
        return dateReception;
    }

    /**
     *
     * @return
     */
    public String getVariableCode() {
        return variableCode;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurReelle() {
        return profondeurReelle;
    }

    /**
     *
     * @param profondeurReelle
     */
    public void setProfondeurReelle(Float profondeurReelle) {
        this.profondeurReelle = profondeurReelle;
    }
}

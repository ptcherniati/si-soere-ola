package org.inra.ecoinfo.glacpe.dataset.phytoplancton;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SequencePhytoplancton;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequencePhytoplanctonDAO extends IDAO<SequencePhytoplancton> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    Optional<SequencePhytoplancton> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode) throws PersistenceException;

    void flush();

}

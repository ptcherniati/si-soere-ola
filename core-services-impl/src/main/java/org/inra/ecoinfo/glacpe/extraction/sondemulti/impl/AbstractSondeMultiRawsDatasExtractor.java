package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractSondeMultiRawsDatasExtractor extends AbstractExtractor {

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<IntervalDate> intervalsDates = getIntervalsDates(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        List<MesureSondeMulti> mesuresSondeMulti = sondeMultiDAO.extractDatas(policyManager.getCurrentUser(), selectedPlateformes, intervalsDates, depthRequestParamVO, dvus);

        extractedDatasMap.put(MAP_INDEX_0, mesuresSondeMulti);

        return extractedDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //;
    }

    private List<MesureSondeMulti> buildMesuresSondeMulti(List<SousSequenceSondeMulti> sousSequenceSondeMulti, Float depth) {
        List<MesureSondeMulti> mesuresSondeMulti = new LinkedList<>();
        sousSequenceSondeMulti.stream().map((ssq) -> {
            MesureSondeMulti currentMesure = null;
            for (Iterator<MesureSondeMulti> it = ssq.getMesures().iterator(); it.hasNext();) {
                MesureSondeMulti mesureSondeMulti = it.next();
                if (currentMesure == null) {
                    currentMesure = mesureSondeMulti;
                } else if (Math.abs(depth - mesureSondeMulti.getProfondeur()) < Math.abs(depth - currentMesure.getProfondeur())) {
                    currentMesure = mesureSondeMulti;
                }
            }
            return currentMesure;
        }).forEachOrdered((currentMesure) -> {
            mesuresSondeMulti.add(currentMesure);
        });
        return mesuresSondeMulti;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        // TODO Auto-generated method stub

    }
}

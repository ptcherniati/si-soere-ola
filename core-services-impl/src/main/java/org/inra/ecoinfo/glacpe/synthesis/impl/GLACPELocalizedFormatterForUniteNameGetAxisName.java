/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

/**
 * @author tcherniatinsky
 */
class GLACPELocalizedFormatterForUniteNameGetAxisName implements ILocalizedFormatter<DatatypeVariableUniteGLACPE> {

    ILocalizationManager localizationManager;

    public GLACPELocalizedFormatterForUniteNameGetAxisName() {
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    @Override
    public String format(DatatypeVariableUniteGLACPE dvu, Locale locale, Object... arguments) {
        return Optional.ofNullable(dvu).map(nodeabledvu -> getVariableAxixName(nodeabledvu, locale))
                .orElse("Error while retrieving variable definition)");
    }

    private String getVariableAxixName(DatatypeVariableUniteGLACPE dvu, Locale locale) {
        Variable variable = dvu.getVariable();
        Unite unite = dvu.getUnite();
        final Properties propertiesVariablesName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME, locale);
        final Properties propertiesUnitDisplay = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, "nom", locale);
        String unitName = propertiesUnitDisplay.getProperty(unite.getName(), unite.getName());
        String variableName = propertiesVariablesName.getProperty(variable.getName(), variable.getName());
        String variableAffichage = variable.getAffichage();
        return String.format("<b>%s</b><br /> (%s - <i>%s</i>)", variableName, variableAffichage, unitName);
    }

}

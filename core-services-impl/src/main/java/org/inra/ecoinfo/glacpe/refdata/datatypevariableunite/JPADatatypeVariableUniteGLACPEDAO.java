package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.DataType_;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.unite.Unite_;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("unchecked")
public class JPADatatypeVariableUniteGLACPEDAO extends AbstractJPADAO<DatatypeVariableUniteGLACPE> implements IDatatypeVariableUniteGLACPEDAO {

    private static final String QUERY_HQL_GET_BY_DATATYPE_AND_VARIABLE = "from DatatypeVariableUniteGLACPE d where d.datatype.code = :datatypeCode and d.variable.code = :variableCode";
    private static final String QUERY_HQL_GET_BY_VARIABLE = "from DatatypeVariableUniteGLACPE d where d.variable.code = :variableCode";
    private static final String QUERY_ALL = "from DatatypeVariableUniteGLACPE";

    /**
     *
     * @return
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getAllDatatypesVariablesUnitesGLACPE() {
        return getAllBy(DatatypeVariableUniteGLACPE.class, DatatypeVariableUniteGLACPE::getCode);
    }

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @return
     */
    @Override
    public Optional<DatatypeVariableUniteGLACPE> getByDatatypeAndVariable(String datatypeCode, String variableCode) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        Root<DatatypeVariableUniteGLACPE> datatypeVariableUniteGLACPE = query.from(DatatypeVariableUniteGLACPE.class);
        Join<DatatypeVariableUniteGLACPE, DataType> datatype = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.datatype);
        Join<DatatypeVariableUniteGLACPE, VariableGLACPE> variable = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.variable);
        query
                .select(datatypeVariableUniteGLACPE)
                .where(builder.and(
                        builder.equal(variable.get(VariableGLACPE_.code), variableCode),
                        builder.equal(datatype.get(DataType_.code), datatypeCode)
                ));
        return getOptional(query);
    }

    /**
     *
     * @param variableCode
     * @return
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getByVariable(String variableCode) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        Root<DatatypeVariableUniteGLACPE> datatypeVariableUniteGLACPE = query.from(DatatypeVariableUniteGLACPE.class);
        Join<DatatypeVariableUniteGLACPE, VariableGLACPE> variable = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.variable);
        query
                .select(datatypeVariableUniteGLACPE)
                .where(builder.equal(datatypeVariableUniteGLACPE.get(DatatypeVariableUniteGLACPE_.code), variableCode));
        return getResultList(query);
    }

    @Override
    public List<DatatypeVariableUniteGLACPE> getByDatatype(String datatypeCode) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        Root<DatatypeVariableUniteGLACPE> datatypeVariableUniteGLACPE = query.from(DatatypeVariableUniteGLACPE.class);
        Join<DatatypeVariableUniteGLACPE, DataType> datatype = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.datatype);
        query
                .select(datatypeVariableUniteGLACPE)
                .where(
                        builder.equal(datatype.get(DataType_.code), datatypeCode)
                );
        return getResultList(query);
    }

    @Override
    public Optional<DatatypeVariableUniteGLACPE> getByNKey(String datatypeCode, String variableCode, String uniteCode) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        Root<DatatypeVariableUniteGLACPE> datatypeVariableUniteGLACPE = query.from(DatatypeVariableUniteGLACPE.class);
        Join<DatatypeVariableUniteGLACPE, DataType> datatype = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.datatype);
        Join<DatatypeVariableUniteGLACPE, VariableGLACPE> variable = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.variable);
        Join<DatatypeVariableUniteGLACPE, Unite> unite = datatypeVariableUniteGLACPE.join(DatatypeVariableUniteGLACPE_.unite);
        query
                .select(datatypeVariableUniteGLACPE)
                .where(builder.and(
                        builder.equal(variable.get(VariableGLACPE_.code), variableCode),
                        builder.equal(datatype.get(DataType_.code), datatypeCode),
                        builder.equal(unite.get(Unite_.code), uniteCode)
                ));
        return getOptional(query);
    }

    @Override
    public Optional<Unite> getUnite(String datatype, String variable) {
        return getByDatatypeAndVariable(datatype, variable).map(dvu -> dvu.getUnite());
    }

    /**
     *
     * @param realNode
     * @return
     */
    @Override
    public RealNode mergeRealNode(RealNode realNode) {
        return entityManager.merge(realNode);
    }

    @Override
    public Map<Variable, RealNode> getRealNodesVariables(RealNode realNode) {
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        final Join<RealNode, Nodeable> nodeable = rn.join(RealNode_.nodeable);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        dvu.join(DatatypeVariableUniteGLACPE_.variable, JoinType.LEFT);
        query.where(
                builder.equal(rn.join(RealNode_.parent), realNode),
                builder.equal(nodeable.get(Nodeable_.id), dvu.get(DatatypeVariableUniteGLACPE_.id))
        );
        query.select(rn);
        final List<RealNode> resultList = getResultList(query);
        return Maps.uniqueIndex(resultList, c -> ((DatatypeVariableUniteGLACPE) c.getNodeable()).getVariable());
    }

    /**
     * @return
     */
    @Override
    public List<String> getPathes() {

        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<RealNode> rn = query.from(RealNode.class);
        query.select(rn.get(RealNode_.path));
        return getResultList(query);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Map<String, DatatypeVariableUniteGLACPE> getAllVariableTypeDonneesByDataTypeMapByVariableCode(
            final String code) {
        return getMapForDatatypeCodeByFunctionKey(code, v -> v.getVariable().getCode());
    }

    private Map<String, DatatypeVariableUniteGLACPE> getMapForDatatypeCodeByFunctionKey(final String code, Function<DatatypeVariableUniteGLACPE, String> mapKey) {

        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        query.where(
                builder.equal(dvu.join(DatatypeVariableUniteGLACPE_.datatype).get(Nodeable_.code), code)
        );
        query.select(dvu);
        return getResultAsStream(query)
                .collect(Collectors.toMap(mapKey, v -> v));
    }

    /**
     * A map of realnode for this datatype @param code The realNodes are
     * grouping by project code, then by site code then by variable code
     *
     * @param code
     * @return
     */
    public Map<String, Map<String, Map<String, List<RealNode>>>> getRealNodesFromDatatypeCode(final String code) {
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> realnode = query.from(RealNode.class);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Join<RealNode, Nodeable> dvuNodeable = realnode.join(RealNode_.nodeable);
        Join<RealNode, RealNode> datatypeNode = realnode.join(RealNode_.parent);
        Join<RealNode, Nodeable> datatype = datatypeNode.join(RealNode_.nodeable);
        query
                .select(realnode)
                .where(
                        builder.equal(datatype.get(Nodeable_.code), code),
                        builder.equal(dvu, dvuNodeable)
                );
        return getResultAsStream(query)
                .collect(Collectors.groupingBy(
                        rn -> ((RealNode) rn).getNodeByNodeableTypeResource(Projet.class).getNodeable().getCode(),
                        Collectors.groupingBy(
                                rn -> ((RealNode) rn).getNodeByNodeableTypeResource(SiteGLACPE.class).getNodeable().getCode(),
                                Collectors.groupingBy(
                                        rn -> ((DatatypeVariableUniteGLACPE) rn.getNodeable()).getVariable().getCode()
                                )
                        )
                ));

    }
}

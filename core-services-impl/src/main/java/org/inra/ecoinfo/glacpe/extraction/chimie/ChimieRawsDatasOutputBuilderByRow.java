package org.inra.ecoinfo.glacpe.extraction.chimie;

import com.google.common.base.Strings;
import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chimie.ConstantsChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import static org.inra.ecoinfo.glacpe.extraction.chimie.AbstractChimieRawsDatasOutputBuilder.MAP_INDEX_0;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class ChimieRawsDatasOutputBuilderByRow extends AbstractChimieRawsDatasOutputBuilder {

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_ALLDEPTH_LINE";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_LINE";
    private static final String HEADER_RAW_DATA_RIVER = "PROPERTY_MSG_HEADER_RAW_RIVER_LINE";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";
    private static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    private static final String CODE_DATATYPE_CHIMIE = "physico_chimie";
    private static final String CODE_TYPE_SITE_RIVIERE = "riviere";
    private static final String CODE_TYPE_OUTIL_MESURE = "mesure";

    private static String KEY_VARIABLE_BALANCE_IONIQUE = ConstantsChimie.KEY_VARIABLE_BALANCE_IONIQUE;
    private Map<String, Float> variablesMap = new HashMap();
    private Map<String, String> variablesHeaderMap = new HashMap();

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return "";
    }

    protected String buildHeader(Map<String, Object> requestMetadatas, String typeSite) throws BusinessException {
        initVariablesMap();
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        ErrorsReport errorsReport = new ErrorsReport(localizationManager.getMessage(BUNDLE_SOURCE_PATH_CHIMIE, PROPERTY_MSG_ERROR));
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();

        if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA)));
            }
        } else {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA_RIVER)));
        }
        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            String uniteNom;
            try {
                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getName());
                if (Strings.isNullOrEmpty(localizedVariableName)) {
                    localizedVariableName = dvu.getVariable().getName();
                }

                Unite unite = dvu.getUnite();
                if (unite == null) {
                    uniteNom = "nounit";
                } else {
                    uniteNom = unite.getCode();
                }

                if (!variablesMap.containsKey(dvu.getVariable().getCode())) {
                    stringBuilder.append(String.format(";%s (%s)", localizedVariableName, uniteNom));
                } else {
                    stringBuilder.append(String.format(";%s (%s);%s", localizedVariableName, uniteNom, variablesHeaderMap.get(dvu.getVariable().getCode())));
                }
                if (errorsReport.hasErrors()) {
                    throw new PersistenceException(errorsReport.buildHTMLMessages());
                }
            } catch (PersistenceException e) {
                throw new BusinessException(e);
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        try {

            List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap);
            DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

            
            Comparator<MesureChimie> comparator = (m1,m2)->{
                if(m1.getSousSequence().getSequence().getDatePrelevement().compareTo(m2.getSousSequence().getSequence().getDatePrelevement())!=0){
                    return m1.getSousSequence().getSequence().getDatePrelevement().compareTo(m2.getSousSequence().getSequence().getDatePrelevement());
                }
                if(m1.getSousSequence().getPlateforme().compareTo(m2.getSousSequence().getPlateforme())!=0){
                    return m1.getSousSequence().getPlateforme().compareTo(m2.getSousSequence().getPlateforme());
                }
                if(m1.getProfondeurReelle()==null){
                    return m1.getId().compareTo(m2.getId());
                }
                if(m2.getProfondeurReelle()!=null && m1.getProfondeurReelle().compareTo(m2.getProfondeurReelle())!=0){
                    return  m1.getProfondeurReelle().compareTo(m2.getProfondeurReelle());
                }
                if(m1.getSousSequence().getOutilsMesure()==null){
                    return m1.getId().compareTo(m2.getId());
                }
                return  m1.getSousSequence().getOutilsMesure().compareTo(m2.getSousSequence().getOutilsMesure());
            };
            TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<MesureChimie>>> mesures = ((List<MesureChimie>) resultsDatasMap.get(MAP_INDEX_0))
                    .stream()
                    .collect(
                            Collectors.groupingBy(
                                    m -> LacsUtils.getProjetFromDataset(m.getSousSequence().getSequence().getVersionFile().getDataset()),
                                    TreeMap::new,
                                    Collectors.groupingBy(
                                            m -> LacsUtils.getSiteFromDataset(m.getSousSequence().getSequence().getVersionFile().getDataset()),
                                            TreeMap::new,
                                            Collectors.toCollection(
                                                    ()-> new TreeSet<>(comparator))
                                            )
                                    )
                            );
            Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
            Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
            Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

            DatatypeVariableUniteGLACPE balanceIoniqueVariable = datatypeVariableUniteDAO.getByDatatypeAndVariable(CODE_DATATYPE_PHYSICO_CHIMIE, KEY_VARIABLE_BALANCE_IONIQUE).orElseThrow(() -> new BusinessException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, MSG_BALANCE_IONIQUE_KEY_NOT_FOUND_IN_DB)));

            SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
            Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_RAW_DATA);
            Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_CSV);
            Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
            for (String projetSitenames : outputPrintStreamMap.keySet()) {
                outputPrintStreamMap.get(projetSitenames).println(headers);
            }
            for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> projetEntry : selectedPlateformes.entrySet()) {
                Projet projet = projetEntry.getKey();
                SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = projetEntry.getValue();
                for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : mapBySite.entrySet()) {
                    SiteGLACPE site = siteEntry.getKey();
                    SortedSet<Plateforme> platforms = siteEntry.getValue();
                    final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_RAW_DATA));
                    final String typeSite = site.getTypeSite().getCode();
                    rawDataPrintStream.println(buildHeader(requestMetadatasMap, typeSite));
                    Iterator<MesureChimie> mesuresChimies = mesures
                            .getOrDefault(projet, new TreeMap<>())
                            .getOrDefault(site, new TreeSet<>())
                            .iterator();
                    while (mesuresChimies.hasNext()) {
                        final MesureChimie mesureChimie = mesuresChimies.next();
                        String line;

                        String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(mesureChimie.getSousSequence().getSequence().getProjet().getName()), mesureChimie.getSousSequence().getSequence().getProjet().getName());
                        String localizedSiteName = propertiesSiteName.getProperty(mesureChimie.getSousSequence().getSequence().getSite().getName(), mesureChimie.getSousSequence().getSequence().getSite().getName());
                        String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureChimie.getSousSequence().getPlateforme().getCode(), mesureChimie.getSousSequence().getPlateforme().getName());

                        if (!typeSite.equals(CODE_TYPE_SITE_RIVIERE)) {
                            if (depthRequestParamVO.getAllDepth()) {
                                line = String.format(
                                        "%s;%s;%s;%s;%s;%s;%s;%s;%s",
                                        localizedProjetName,
                                        localizedSiteName,
                                        localizedPlateformeName,
                                        DateUtil.getUTCDateTextFromLocalDateTime(mesureChimie.getSousSequence().getSequence().getDatePrelevement(), dateFormatter),
                                        mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(),
                                        mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "",
                                        mesureChimie.getProfondeurMin(),
                                        mesureChimie.getProfondeurMax(),
                                        mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle()
                                );
                            } else {
                                line = String.format(
                                        "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                                        localizedProjetName,
                                        localizedSiteName,
                                        localizedPlateformeName,
                                        depthRequestParamVO.getDepthMin(),
                                        depthRequestParamVO.getDepthMax(),
                                        DateUtil.getUTCDateTextFromLocalDateTime(mesureChimie.getSousSequence().getSequence().getDatePrelevement(), dateFormatter),
                                        mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(),
                                        mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "",
                                        mesureChimie.getProfondeurMin(),
                                        mesureChimie.getProfondeurMax(),
                                        mesureChimie.getProfondeurReelle() == null ? "" : mesureChimie.getProfondeurReelle()
                                );
                            }
                        } else {
                            line = String.format(
                                    "%s;%s;%s;%s;%s;%s;%s;%s",
                                    localizedProjetName,
                                    localizedSiteName,
                                    localizedPlateformeName,
                                    retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateDebutCampagne()),
                                    retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateFinCampagne()),
                                    retrieveValidDateString(mesureChimie.getSousSequence().getSequence().getDateReception()),
                                    mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? "" : mesureChimie.getSousSequence().getOutilsMesure().getNom(),
                                    mesureChimie.getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals(CODE_TYPE_OUTIL_MESURE) ? mesureChimie.getSousSequence().getOutilsMesure().getNom() : "");
                        }

                        // rawDataPrintStream.print(line);
                        Map<Long, Float> valeursMesuresChimie = buildValeurs(mesureChimie.getValeurs());
                        Boolean isLineEmpty = true;
                        for (DatatypeVariableUniteGLACPE dvu : dvus) {
                            // rawDataPrintStream.print(";");
                            line = line.concat(";");
                            if (!dvu.getVariable().getCode().equals(KEY_VARIABLE_BALANCE_IONIQUE)) {
                                Float valeur = valeursMesuresChimie.get(dvu.getVariable().getId());
                                if (valeur != null) {
                                    if (!variablesMap.containsKey(dvu.getVariable().getCode())) {
                                        line = line.concat(String.format("%s", valeur));
                                    } else {
                                        line = line.concat(String.format("%s;%.5f", valeur, valeur * variablesMap.get(dvu.getVariable().getCode())));
                                    }
                                    isLineEmpty = false;
                                } else {
                                    if (variablesMap.containsKey(dvu.getVariable().getCode())) {
                                        line = line.concat(";");
                                    }
                                    // rawDataPrintStream.print("");
                                }

                            } else {
                                line = line.concat(String.format("%s", processBalanceIonique(mesureChimie)));
                            }
                        }

                        if (!isLineEmpty) {
                            rawDataPrintStream.print(line);
                            rawDataPrintStream.println();
                        }
                        mesuresChimies.remove();
                    }

                }

            }

            closeStreams(outputPrintStreamMap);
            return filesMap;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private void initVariablesMap() {
        if (variablesMap.isEmpty()) {

            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NH4, 1.2878f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NO3, 4.4268f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_NO2, 3.28443f);
            variablesMap.put(ConstantsChimie.KEY_VARIABLE_PO4, 3.065f);
        }

        if (variablesHeaderMap.isEmpty()) {
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NO3, "Nitrates [mg(NO3)/l]");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NH4, "Ammonium [mg(NH4)/l]");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_NO2, "Nitrites [mg(NO2)/l]");
            variablesHeaderMap.put(ConstantsChimie.KEY_VARIABLE_PO4, "Orthophosphates [mg(PO4)/l]");
        }
    }

    private Map<Long, Float> buildValeurs(List<ValeurMesureChimie> valeurs) {
        Map<Long, Float> mapValue = new HashMap<Long, Float>();
        for (ValeurMesureChimie valeur : valeurs) {
            mapValue.put(((DatatypeVariableUniteGLACPE) valeur.getRealNode().getNodeable()).getVariable().getId(), valeur.getValeur());
        }
        return mapValue;
    }

    private String processBalanceIonique(MesureChimie mesureChimie) throws PersistenceException {

        Float balanceIonique = null;
        String balanceIoniqueString = "";

        ValeurMesureChimie vmcCA = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_CA, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcK = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_K, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcCL = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_CL, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcMG = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_MG, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcNA = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NA, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcNH4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NH4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcNO3 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NO3, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcNO2 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_NO2, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcPO4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_PO4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcSO4 = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_SO4, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);
        ValeurMesureChimie vmcTAC = valeurMesureChimieDAO.getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(ConstantsChimie.KEY_VARIABLE_TAC, mesureChimie.getProfondeurMin(), mesureChimie.getProfondeurMax(), mesureChimie
                .getSousSequence().getSequence().getProjet().getCode(), mesureChimie.getSousSequence().getSequence().getDatePrelevement(), mesureChimie.getSousSequence().getPlateforme().getCode()).orElse(null);

        if (vmcCA != null && vmcK != null && vmcCL != null && vmcMG != null && vmcNA != null && vmcNH4 != null && vmcNO3 != null && vmcNO2 != null && vmcPO4 != null && vmcSO4 != null && vmcTAC != null) {
            Float sumCation = ((2 * vmcCA.getValue() / 40.1f) + (2 * vmcMG.getValue() / 24.3f) + (vmcK.getValue() / 39.1f) + (vmcNA.getValue() / 23f) + (vmcNH4.getValue() / 14f));
            Float sumAnion = ((vmcCL.getValue() / 35.5f) + (2 * vmcSO4.getValue() / 96.1f) + (vmcNO2.getValue() / 14.0f) + (vmcNO3.getValeur() / 14) + vmcTAC.getValue() + (3 * vmcPO4.getValue() / 31.0f));

            balanceIonique = 100f * (sumCation - sumAnion) / (sumCation + sumAnion);
        }

        if (balanceIonique != null) {
            balanceIoniqueString = String.format("%.3f%%", balanceIonique);
        }

        return balanceIoniqueString;
    }

    private String retrieveValidDateString(LocalDate dateCampagne) {
        if (dateCampagne == null) {
            return "";
        } else {
            return DateUtil.getUTCDateTextFromLocalDateTime(dateCampagne, dateFormatter);
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChimieRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;

    }

}

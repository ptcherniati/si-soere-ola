package org.inra.ecoinfo.glacpe.dataset.zooplancton.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationZooplanctonDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_METADATA_ZOO_BY_IDS = "delete from MetadataZooplancton mtdz where mtdz.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_DATA_ZOO_BY_IDS = "delete from DataZooplancton dz where dz.metadataZooplancton.id in (select id from MetadataZooplancton mtdz where mtdz.versionFile = :version)";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {

            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_DATA_ZOO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_METADATA_ZOO_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage());
            throw new PersistenceException(e);
        }
    }
}

package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonOutputBuilderByLine extends PhytoplanctonOutputsBuildersResolver {

    private IOutputBuilder phytoplanctonBuildOutputByLine;

    /**
     *
     */
    public PhytoplanctonOutputBuilderByLine() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException, NoExtractionResultException {
        super.setPhytoplanctonOutputBuilder(phytoplanctonBuildOutputByLine);
        return super.buildOutput(parameters);
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getPhytoplanctonBuildOutputByLine() {
        return phytoplanctonBuildOutputByLine;
    }

    /**
     *
     * @param phytoplanctonBuildOutputByLine
     */
    public void setPhytoplanctonBuildOutputByLine(IOutputBuilder phytoplanctonBuildOutputByLine) {
        this.phytoplanctonBuildOutputByLine = phytoplanctonBuildOutputByLine;
    }
}

/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeOutilsMesure> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";
    private static final String PROPERTY_MSG_ERROR_CODE_TYPE_OUTILS_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TYPE_OUTILS_NOT_FOUND_IN_DB";
    private static final String MSG_MISSING_REFERENCE_DATA_TYPE = "PROPERTY_MSG_MISSING_REFERENCE_DATA_TYPE";
    private static final String TYPE = "type_outil";

    private Properties propertiesNomFR;
    private Properties propertiesNomEN;
    /**
     *
     */
    protected ITypeOutilsMesureDAO typeOutilsMesureDAO;

    /**
     *
     */
    protected IValeurQualitativeDAO valeurQualitativeDAO;


    /**
     *
     */
    protected String[] typeOutilPossibles;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, TypeOutilsMesure.TABLE_NAME);
                lineNumber++;

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String typeString = tokenizerValues.nextToken();
                
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                ValeurQualitative dbType = (ValeurQualitative) valeurQualitativeDAO.getByCodeAndValue(TYPE, Utils.createCodeFromString(typeString)).orElse(null);
                if(dbType==null){    
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_ERROR_CODE_TYPE_OUTILS_NOT_FOUND_IN_DB), typeString));
                }
                
                TypeOutilsMesure typeOutilsMesure = new TypeOutilsMesure(nom, dbType);
                TypeOutilsMesure dbTypeOutilsMesure = typeOutilsMesureDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);

                // Enregistre uniquement s'il n'existe pas en BD ou bien
                // s'il est considéré comme une mise à jour
               if (dbTypeOutilsMesure == null) {
                    if (dbType != null) {
                        typeOutilsMesure.setCodeSandre(codeSandre);
                        typeOutilsMesure.setContexte(contexte);
                        typeOutilsMesureDAO.saveOrUpdate(new TypeOutilsMesure(nom, dbType));
                    } else {
                        errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_MISSING_REFERENCE_DATA_TYPE)));
                    }
                } else {
                    dbTypeOutilsMesure.setCodeSandre(codeSandre);
                    dbTypeOutilsMesure.setContexte(contexte);
                    dbTypeOutilsMesure.setType(dbType);
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                typeOutilsMesureDAO.remove(typeOutilsMesureDAO.getByCode(code)
                .orElseThrow(()->new javax.persistence.PersistenceException("can't find typeOutilsMesure")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param typeOutilsMesureDAO
     */
    public void setTypeOutilsMesureDAO(ITypeOutilsMesureDAO typeOutilsMesureDAO) {
        this.typeOutilsMesureDAO = typeOutilsMesureDAO;
    }

    /**
     *
     * @param outilsMesure
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeOutilsMesure outilsMesure) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : propertiesNomFR.get(outilsMesure.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : propertiesNomEN.get(outilsMesure.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getType().getValeur(), typeOutilPossibles, null, false, false, false));
        
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(outilsMesure == null ? EMPTY_STRING : outilsMesure.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /**
     *
     * @throws PersistenceException
     */
    protected void updateTypeOutilPossibles() throws PersistenceException {
        List<ValeurQualitative> valeursQualitatives = valeurQualitativeDAO.getByCode(TYPE);
        String[] namesTypeOutilsPossibles = new String[valeursQualitatives.size()];
        int index = 0;
        for (ValeurQualitative valeur : valeursQualitatives) {
            namesTypeOutilsPossibles[index++] = valeur.getValeur();
        }
        this.typeOutilPossibles = namesTypeOutilsPossibles;
    }

    @Override
    protected List<TypeOutilsMesure> getAllElements() {
        return typeOutilsMesureDAO.getAllBy(TypeOutilsMesure.class, TypeOutilsMesure::getCode);
    }

    @Override
    protected ModelGridMetadata<TypeOutilsMesure> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(TypeOutilsMesure.TABLE_NAME, "nom", Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(TypeOutilsMesure.TABLE_NAME, "nom", Locale.ENGLISH);
        try {
            updateTypeOutilPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param valeurQualitativeDAO
     */
    public void setValeurQualitativeDAO(IValeurQualitativeDAO valeurQualitativeDAO) {
        this.valeurQualitativeDAO = valeurQualitativeDAO;
    }

}

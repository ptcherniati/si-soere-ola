package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

class KeyMapExtractionL2Zooplancton implements Comparable<KeyMapExtractionL2Zooplancton> {

    private String nomLatinTaxon;
    private String nomVariable;

    public KeyMapExtractionL2Zooplancton(String nomLatinTaxon, String nomVariable) {
        super();
        this.nomLatinTaxon = nomLatinTaxon;
        this.nomVariable = nomVariable;
    }

    public String getNomLatinTaxon() {
        return nomLatinTaxon;
    }

    public void setNomLatinTaxon(String nomLatinTaxon) {
        this.nomLatinTaxon = nomLatinTaxon;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(this.nomLatinTaxon).append(nomVariable).toString();

    }

    @Override
    public int compareTo(KeyMapExtractionL2Zooplancton keyMapToCompare) {
        return toString().compareTo(keyMapToCompare.toString());
    }

    public String getNomVariable() {
        return nomVariable;
    }

    public void setNomVariable(String nomVariable) {
        this.nomVariable = nomVariable;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nomLatinTaxon == null) ? 0 : nomLatinTaxon.hashCode());
        result = prime * result + ((nomVariable == null) ? 0 : nomVariable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        KeyMapExtractionL2Zooplancton other = (KeyMapExtractionL2Zooplancton) obj;
        if (nomLatinTaxon == null) {
            if (other.nomLatinTaxon != null) {
                return false;
            }
        } else if (!nomLatinTaxon.equals(other.nomLatinTaxon)) {
            return false;
        }
        if (nomVariable == null) {
            if (other.nomVariable != null) {
                return false;
            }
        } else if (!nomVariable.equals(other.nomVariable)) {
            return false;
        }
        return true;
    }

}

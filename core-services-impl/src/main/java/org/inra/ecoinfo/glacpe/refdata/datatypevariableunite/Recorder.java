/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.refdata.variable.IVariableGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<DatatypeVariableUniteGLACPE> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_CAN_T_SAVE_DATATYPE_VARIABLE_UNITE = "MSG_CAN_T_SAVE_DATATYPE_VARIABLE_UNITE";
    private static final String MSG_VARIABLE_MISSING_IN_DATABASE = "PROPERTY_MSG_VARIABLE_MISSING_IN_DATABASE";
    private static final String MSG_DATATYPE_MISSING_IN_DATABASE = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";
    private static final String MSG_UNITE_MISSING_IN_DATABASE = "PROPERTY_MSG_UNITE_MISSING_IN_DATABASE";

    /**
     *
     */
    public Logger LOGGER = LoggerFactory.getLogger(getClass());

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    private Map<String, String[]> variablesPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> unitesPossibles = new ConcurrentHashMap<String, String[]>();
    private Map<String, String[]> datatypesPossibles = new ConcurrentHashMap<String, String[]>();

    private IVariableGLACPEDAO variableDAO;

    private IUniteDAO uniteDAO;

    private IDatatypeDAO datatypeDAO;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        Map<DataType, List<DatatypeVariableUniteGLACPE>> nodeablesDVU = new HashMap();
        try {
            this.skipHeader(parser);
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String uniteCode = Utils.createCodeFromString(tokenizerValues.nextToken());

                // afiocca
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();
                final DatatypeVariableUniteGLACPE nodeable;
                try {
                    nodeable = persistDatatypeVariable(errorsReport, datatypeCode, variableCode, uniteCode, codeSandre, contexte).orElse(null);
                } catch (PersistenceException ex) {
                    String message = String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_CAN_T_SAVE_DATATYPE_VARIABLE_UNITE), datatypeCode, variableCode, uniteCode);
                    LOGGER.debug(message, ex);
                    throw new BusinessException(message, ex);
                }
                if (nodeable != null) {
                    nodeablesDVU.computeIfAbsent(nodeable.getDatatype(), k -> new LinkedList())
                            .add(nodeable);
                }
                if (parser.getLastLineNumber() % 50 == 0) {
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                }
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

            try {

                Map<DataType, List<INode>> datatypeNodesMap = mgaServiceBuilder.loadNodesByTypeResource(WhichTree.TREEDATASET, DataType.class)
                        .filter(n -> nodeablesDVU.containsKey(n.getNodeable()))
                        .collect(Collectors.groupingBy(
                                p -> ((DataType) p.getNodeable()))
                        );
                Map<String, RealNode> realNodes = mgaServiceBuilder.getRecorder().getAllRealNodes()
                        .collect(Collectors.toMap(r -> r.getPath(), k -> k));
                int[] i = new int[]{0};
                for (Iterator<Map.Entry<DataType, List<DatatypeVariableUniteGLACPE>>> iteratorOnDVU = nodeablesDVU.entrySet().iterator(); iteratorOnDVU.hasNext();) {
                    Map.Entry<DataType, List<DatatypeVariableUniteGLACPE>> dvusEntry = iteratorOnDVU.next();
                    DataType datatype = dvusEntry.getKey();
                    List<DatatypeVariableUniteGLACPE> dvus = dvusEntry.getValue();
                    List<INode> datatypeNodeList = datatypeNodesMap.get(datatype);
                    if (datatypeNodeList == null) {
                        continue;
                    }
                    for (Iterator<INode> iteratorOnDatatypeNodes = datatypeNodeList.iterator(); iteratorOnDatatypeNodes.hasNext();) {
                        INode datatypeNode = iteratorOnDatatypeNodes.next();
                        RealNode parentRn = datatypeNode.getRealNode();
                        for (DatatypeVariableUniteGLACPE dvu : dvus) {
                            String path = String.format("%s%s%s", parentRn.getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = realNodes.get(path);
                            if (rn == null) {
                                rn = new RealNode(parentRn, null, dvu, path);
                                realNodes.put(path, rn);
                                mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            }
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) datatypeNode, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        }
                        iteratorOnDatatypeNodes.remove();
                    }
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                    iteratorOnDVU.remove();
                }
                policyManager.clearTreeFromSession();

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            } catch (final BusinessException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = tokenizerValues.nextToken();
                final String uniteCode = tokenizerValues.nextToken();
                final DatatypeVariableUniteGLACPE dbDVU = this.datatypeVariableUniteDAO
                        .getByNKey(datatypeCode, variableCode, uniteCode)
                        .orElseThrow(PersistenceException::new);
                datatypeVariableUniteDAO.remove(dbDVU);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    private Optional<DatatypeVariableUniteGLACPE> persistDatatypeVariable(ErrorsReport errorsReport, String datatypeCode, String variableCode, String uniteCode, String codeSandre, String contexte) throws PersistenceException {
        DataType dbDatatype = retrieveDBDatatype(errorsReport, datatypeCode);
        VariableGLACPE dbVariable = retrieveDBVariable(errorsReport, variableCode);
        Unite dbUnite = retrieveDBUnite(errorsReport, uniteCode);

        if (dbDatatype != null && dbVariable != null && dbUnite != null) {
            Optional<DatatypeVariableUniteGLACPE> dbDatatypeVariableUnite = datatypeVariableUniteDAO.getByNKey(datatypeCode, variableCode, uniteCode);
            return createOrUpdateDatatypeVariable((DatatypeVariableUniteGLACPE) dbDatatypeVariableUnite.orElse(null), dbDatatype, dbUnite, dbVariable, codeSandre, contexte);

        }
        return Optional.empty();
    }

    /**
     *
     * @param dbDatatypeVariableUnite
     * @param dbDatatype
     * @param dbUnite
     * @param dbVariable
     * @param codeSandre
     * @param contexte
     * @throws PersistenceException
     */
    private Optional<DatatypeVariableUniteGLACPE> createOrUpdateDatatypeVariable(final DatatypeVariableUniteGLACPE dbDatatypeVariableUnite, DataType dbDatatype, Unite dbUnite, VariableGLACPE dbVariable, String codeSandre, String contexte) throws PersistenceException {
        if (dbDatatypeVariableUnite == null) {

            DatatypeVariableUniteGLACPE datatypeVariableUnite = new DatatypeVariableUniteGLACPE(dbDatatype, dbUnite, dbVariable);
            datatypeVariableUnite.setCodeSandre(codeSandre);
            datatypeVariableUnite.setContexte(contexte);
            datatypeVariableUniteDAO.saveOrUpdate(datatypeVariableUnite);
        return Optional.ofNullable(datatypeVariableUnite);
        } else {
            dbDatatypeVariableUnite.setContexte(contexte);
            dbDatatypeVariableUnite.setCodeSandre(codeSandre);
            datatypeVariableUniteDAO.saveOrUpdate(dbDatatypeVariableUnite);
        return Optional.ofNullable(dbDatatypeVariableUnite);
        }
    }

    private VariableGLACPE retrieveDBVariable(ErrorsReport errorsReport, String variableCode) throws PersistenceException {
        VariableGLACPE variable = (VariableGLACPE) variableDAO.getByCode(variableCode).orElse(null);
        if (variable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_VARIABLE_MISSING_IN_DATABASE), variableCode));
        }
        return variable;
    }

    private Unite retrieveDBUnite(ErrorsReport errorsReport, String uniteCode) throws PersistenceException {
        Unite unite = uniteDAO.getByCode(uniteCode).orElse(null);
        if (unite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_UNITE_MISSING_IN_DATABASE), uniteCode));
        }
        return unite;
    }

    private DataType retrieveDBDatatype(ErrorsReport errorsReport, String datatypeCode) throws PersistenceException {
        DataType datatype = datatypeDAO.getByCode(datatypeCode).orElse(null);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_DATATYPE_MISSING_IN_DATABASE), datatypeCode));
        }
        return datatype;
    }

    private void updateNamesVariablesPossibles() throws PersistenceException {
        List<Variable> variables = variableDAO.getAll(Variable.class);
        String[] namesVariablesPossibles = new String[variables.size()];
        int index = 0;
        for (Variable variable : variables) {
            namesVariablesPossibles[index++] = variable.getName();
        }
        variablesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesVariablesPossibles);

    }

    private void updateNamesDatatypesPossibles() throws PersistenceException {
        List<DataType> datatypes = datatypeDAO.getAll(DataType.class);
        String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        datatypesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesDatatypesPossibles);
    }

    private void updateNamesUnitesPossibles() throws PersistenceException {
        List<Unite> unites = uniteDAO.getAll(Unite.class);
        String[] namesUnitesPossibles = new String[unites.size()];
        int index = 0;
        for (Unite unite : unites) {
            namesUnitesPossibles[index++] = unite.getCode();
        }
        unitesPossibles.put(ColumnModelGridMetadata.NULL_KEY, namesUnitesPossibles);
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = (IVariableGLACPEDAO) variableDAO;
    }

    /**
     *
     * @param uniteDAO
     */
    public void setUniteDAO(IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    /**
     *
     * @param datatypeVariableUnite
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DatatypeVariableUniteGLACPE datatypeVariableUnite) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getDatatype().getName(), datatypesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getVariable().getName(), variablesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getUnite().getCode(), unitesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? EMPTY_STRING : datatypeVariableUnite.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected List<DatatypeVariableUniteGLACPE> getAllElements() {
        return datatypeVariableUniteDAO.getAllDatatypesVariablesUnitesGLACPE();
    }

    @Override
    protected ModelGridMetadata<DatatypeVariableUniteGLACPE> initModelGridMetadata() {
        try {
            updateNamesVariablesPossibles();
            updateNamesUnitesPossibles();
            updateNamesDatatypesPossibles();
        } catch (PersistenceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }

}

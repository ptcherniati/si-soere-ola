package org.inra.ecoinfo.glacpe.dataset.chimie.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.IValeurMesureChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie_;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie_;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SousSequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SousSequenceChimie_;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie_;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme_;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

/**
 *
 * @author ptcherniati
 */
public class JPAValeurMesureChimieDAO extends AbstractJPADAO<ValeurMesureChimie> implements IValeurMesureChimieDAO {
    /**
     *
     * @param variableCode
     * @param profondeurMin
     * @param profondeurMax
     * @param projetCode
     * @param datePrelevement
     * @param plateformeCode
     * @return
     */
    @Override
    public Optional<ValeurMesureChimie> getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(String variableCode, Float profondeurMin, Float profondeurMax, String projetCode, LocalDate datePrelevement, String plateformeCode){
        CriteriaQuery<ValeurMesureChimie> query = builder.createQuery(ValeurMesureChimie.class);
        Root<ValeurMesureChimie> v = query.from(ValeurMesureChimie.class);
        final Join<ValeurMesureChimie, RealNode> rnv = v.join(ValeurMesureChimie_.realNode);
        Join<DatatypeVariableUniteGLACPE, VariableGLACPE> variable = builder.treat(rnv.join(RealNode_.nodeable), DatatypeVariableUniteGLACPE.class).join(DatatypeVariableUniteGLACPE_.variable);
        Join<ValeurMesureChimie, MesureChimie> m = v.join(ValeurMesureChimie_.mesure);
        Join<MesureChimie, SousSequenceChimie> ss = m.join(MesureChimie_.sousSequence);
        Join<SousSequenceChimie, SequenceChimie> s = ss.join(SousSequenceChimie_.sequence);
        Join<SousSequenceChimie, Plateforme> platform = ss.join(SousSequenceChimie_.plateforme);
        Join<RealNode, Nodeable> projet = rnv.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.nodeable);
        query
                .select(v)
                .where(
                        builder.and(
                                builder.equal(variable.get(VariableGLACPE_.code), variableCode),
                                builder.equal(m.get(MesureChimie_.profondeurMin), profondeurMin),
                                builder.equal(m.get(MesureChimie_.profondeurMax), profondeurMax),
                                builder.equal(s.get(SequenceChimie_.datePrelevement), datePrelevement),
                                builder.equal(platform.get(Plateforme_.code), plateformeCode),
                                builder.equal(projet.get(Nodeable_.code), projetCode)
                        )
                );

            return getOptional(query);
    }

}

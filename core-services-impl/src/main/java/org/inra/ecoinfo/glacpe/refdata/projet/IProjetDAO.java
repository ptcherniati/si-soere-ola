/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.projet;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IProjetDAO extends IDAO<Projet> {

    /**
     *
     * @param code
     * @return
     */
    public Optional<Projet> getByCode(String code);

    /**
     *
     * @return
     */
    public List<Projet> getAll();
}

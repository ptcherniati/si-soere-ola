package org.inra.ecoinfo.glacpe.refdata.variable;

import java.util.List;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;

/**
 *
 * @author ptcherniati
 */
public interface IVariableGLACPEDAO extends IVariableDAO {

    /**
     *
     * @return
     */
    List<VariableGLACPE> getAllQualitativeValues();
}

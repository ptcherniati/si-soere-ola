package org.inra.ecoinfo.glacpe.extraction.chimie;


import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class DefaultChimieDatatypeManager extends MO implements IChimieDatatypeManager {

    /**
     *
     */
    protected IChimieDAO chimieDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     * @param chimieDAO
     */
    public void setChimieDAO(IChimieDAO chimieDAO) {
        this.chimieDAO = chimieDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return chimieDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return chimieDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Float> getAvailableDepths(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        return chimieDAO.getAvailableDepths(policyManager.getCurrentUser(), selectedPlatforms, selectedDates, selectedVariables);
    }

}

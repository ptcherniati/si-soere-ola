package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String nomSite;
    private String plateformeCode;
    private LocalDate datePrelevement;
    private String outilsMesureCode;
    private String commentaires;
    private Float profondeur;
    private LocalTime heure;
    private Long originalLineNumber;
    private String projetCode;
    private List<VariableValue> variablesValues;

    /**
     *
     */
    public LineRecord() {

    }

    /**
     *
     * @param nomSite
     * @param plateformeCode
     * @param datePrelevement
     * @param outilsMesureCode
     * @param commentaires
     * @param profondeur
     * @param heure
     * @param variablesValues
     * @param originalLineNumber
     * @param projetCode
     */
    public LineRecord(String nomSite, String plateformeCode, LocalDate datePrelevement, String outilsMesureCode, String commentaires, Float profondeur, LocalTime heure, List<VariableValue> variablesValues, Long originalLineNumber, String projetCode) {
        super();
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.datePrelevement = datePrelevement;
        this.outilsMesureCode = outilsMesureCode;
        this.commentaires = commentaires;
        this.profondeur = profondeur;
        this.heure = heure;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
        this.projetCode = projetCode;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.datePrelevement = line.getDatePrelevement();
        this.outilsMesureCode = line.getOutilsMesureCode();
        this.commentaires = line.getCommentaires();
        this.profondeur = line.getProfondeur();
        this.heure = line.getHeure();
        this.variablesValues = line.getVariablesValues();

        this.originalLineNumber = line.getOriginalLineNumber();
        this.projetCode = getProjetCode();
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @return
     */
    public String getOutilsMesureCode() {
        return outilsMesureCode;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @param variablesValues
     */
    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }
}

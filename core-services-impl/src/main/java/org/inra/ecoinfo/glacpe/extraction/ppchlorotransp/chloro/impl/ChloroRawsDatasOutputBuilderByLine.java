package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.MesureChloro;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.ValeurMesureChloro;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.AbstractPPChloroTranspRawsDatasOutputBuilder;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChloroRawsDatasOutputBuilderByLine extends AbstractPPChloroTranspRawsDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_CHLORO = "org.inra.ecoinfo.glacpe.dataset.chloro.messages";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_RAW_DATA_ROW";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ALLDEPTH_ROW";

    private static final String CODE_CHLOROPHYLLE = "chlorophylle";
    private static final String SUFFIX_FILENAME = CODE_CHLOROPHYLLE;

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();
        try {
            if (depthRequestParamVO.getAllDepth()) {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA_ALLDEPTH)));
            } else {
                stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHLORO, HEADER_RAW_DATA)));
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvUs = getDVUs(requestMetadatasMap, PPChloroTranspParameter.CHLOROPHYLLE);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);
        final List<MesureChloro> mesures = (List<MesureChloro>) resultsDatasMap.get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Comparator<MesureChloro> comparator = (m1, m2) -> {
            if (m1.getSousSequenceChloro().getSequenceChloro().getDate().compareTo(m2.getSousSequenceChloro().getSequenceChloro().getDate()) != 0) {
                return m1.getSousSequenceChloro().getSequenceChloro().getDate().compareTo(m2.getSousSequenceChloro().getSequenceChloro().getDate());
            }
            if (m1.getSousSequenceChloro().getPlateforme().compareTo(m2.getSousSequenceChloro().getPlateforme()) != 0) {
                return m1.getSousSequenceChloro().getPlateforme().compareTo(m2.getSousSequenceChloro().getPlateforme());
            }
            if (m1.getProfondeur_max() == null) {
                return m1.getId().compareTo(m2.getId());
            }
            if (m2.getProfondeur_max() != null && m1.getProfondeur_max().compareTo(m2.getProfondeur_max()) != 0) {
                return m1.getProfondeur_max().compareTo(m2.getProfondeur_max());
            }
            return m1.getId().compareTo(m2.getId());
        };
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeSet<MesureChloro>>> mesuresByProjet = mesures
                .stream()
                .collect(
                        Collectors.groupingBy(
                                m -> LacsUtils.getProjetFromDataset(m.getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset()),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> LacsUtils.getSiteFromDataset(m.getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset()),
                                        TreeMap::new,
                                        Collectors.toCollection(
                                                () -> new TreeSet<>(comparator)
                                        )
                                )
                        )
                );
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_CSV);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> entry : selectedPlateformes.entrySet()) {
            Projet projet = entry.getKey();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getCode());
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> mapBySite = entry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> plateformeEntry : mapBySite.entrySet()) {
                SiteGLACPE site = plateformeEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getCode());
                SortedSet<Plateforme> platforms = plateformeEntry.getValue();
                final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME));
                rawDataPrintStream.println(headers);

                try {
                    for (Iterator<MesureChloro> iterator = mesuresByProjet.get(projet).get(site).iterator(); iterator.hasNext();) {
                        MesureChloro mesureChloro = iterator.next();

                        String localizedPlateformeName = propertiesPlateformeName.getProperty(mesureChloro.getSousSequenceChloro().getPlateforme().getCode(), mesureChloro.getSousSequenceChloro().getPlateforme().getName());

                        Map<Long, ValeurMesureChloro> valeursMesuresChloro = buildValeurs(mesureChloro.getValeurs());
                        for (DatatypeVariableUniteGLACPE dvu : dvUs) {
                            ValeurMesureChloro valeurMesureChloro = valeursMesuresChloro.get(dvu.getVariable().getId());
                            if (valeurMesureChloro != null && valeurMesureChloro.getValue() != null) {
                                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
                                final String uniteName = ((DatatypeVariableUniteGLACPE) valeurMesureChloro.getRealNode().getNodeable()).getUnite().getName();
                                if (depthRequestParamVO.getAllDepth()) {
                                    rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName,
                                            DateUtil.getUTCDateTextFromLocalDateTime(mesureChloro.getSousSequenceChloro().getSequenceChloro().getDate(), DateUtil.DD_MM_YYYY), mesureChloro.getProfondeur_min(), mesureChloro.getProfondeur_max(), localizedVariableName,
                                            String.format(getLocalizationManager().getUserLocale().getLocale(), "%.3f", valeurMesureChloro.getValeur()), uniteName));
                                } else {
                                    rawDataPrintStream.println(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", localizedProjetName, localizedSiteName, localizedPlateformeName, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax(),
                                            DateUtil.getUTCDateTextFromLocalDateTime(mesureChloro.getSousSequenceChloro().getSequenceChloro().getDate(), DateUtil.DD_MM_YYYY), mesureChloro.getProfondeur_min(), mesureChloro.getProfondeur_max(), localizedVariableName,
                                            String.format(getLocalizationManager().getUserLocale().getLocale(), "%.3f", valeurMesureChloro.getValeur()), uniteName));
                                }
                            }
                        }
                        // commenter ?
                        iterator.remove();

                    }
                } catch (Exception e) {
                } finally {
                    closeStreams(outputPrintStreamMap);
                }
            }
        }
        return filesMap;
    }

    private Map<Long, ValeurMesureChloro> buildValeurs(List<ValeurMesureChloro> valeurs) {
        return valeurs.stream()
                .collect(Collectors.toMap(
                        valeur -> ((DatatypeVariableUniteGLACPE) valeur.getRealNode().getNodeable()).getVariable().getId(),
                        valeur -> valeur
                ));
    }


    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        if (((DefaultParameter) parameters).getResults().get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE) == null
                || ((DefaultParameter) parameters).getResults().get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }
    class ErrorsReport {
        
        private String errorsMessages = new String();
        
        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }

}

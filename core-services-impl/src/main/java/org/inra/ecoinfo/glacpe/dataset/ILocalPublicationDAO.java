package org.inra.ecoinfo.glacpe.dataset;

import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * The Interface ILocalPublicationDAO.
 *  * interface of DAO use to delete file
 * 
 * @author Enrico Guillaume
 */
public interface ILocalPublicationDAO extends IVersionFileDAO {

    /**
     * Removes the version.
     * 
     * @param version
     * @link(VersionFile) the version
     * @throws PersistenceException
     *             delete the file {@link VersionFile} the version
     */
    void removeVersion(VersionFile version) throws PersistenceException;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

/**
 * @author tcherniatinsky
 */
class GLACPELocalizedFormatterForVariableNameGetDisplay implements ILocalizedFormatter<DatatypeVariableUniteGLACPE> {

    ILocalizationManager localizationManager;

    public GLACPELocalizedFormatterForVariableNameGetDisplay() {
    }

    @Override
    public String format(DatatypeVariableUniteGLACPE nodeable, Locale locale, Object... arguments) {
        final Properties propertiesVariablesNames = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME, locale);
        return Optional.ofNullable(nodeable).map(nodeabledvu -> nodeabledvu.getVariable().getName())
                .map(name -> propertiesVariablesNames.getProperty(name, name))
                .map(definition -> String.format("<i>%s</i>", definition))
                .orElse("Error while retrieving variable display)");
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}

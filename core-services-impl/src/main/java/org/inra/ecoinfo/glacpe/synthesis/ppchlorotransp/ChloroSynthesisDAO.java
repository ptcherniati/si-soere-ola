/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.ppchlorotransp;

import java.util.stream.Stream;
import org.inra.ecoinfo.glacpe.synthesis.contenustomacaux.SynthesisDatatype;
import org.inra.ecoinfo.glacpe.synthesis.contenustomacaux.SynthesisValue;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class ChloroSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     *
     */
    public static String SYNTHESIS_REQUEST = "insert into chlorosynthesisvalue(id, date, idnode, ismean, site, valuefloat, valuestring, variable, siteid, projetid, plateformeid, variableid, profondeurs)\n"
            + "(select \n"
            + "nextval('hibernate_sequence') id,\n"
            + "s.date date,\n"
            + "dnv.branch_node_id idNode,\n"
            + "false isMean,\n"
            + "rns.path site,\n"
            + "avg(v.valeur) valueFloat,\n"
            + "null valueString,\n"
            + "nddvu.code variable,\n"
            + "rns.id_nodeable siteid,\n"
            + "rnp.id_nodeable projetid,\n"
            + "ss.loc_id plateformeid,\n"
            + "dvu.var_id variableid,\n"
            + "array_agg(distinct m.profondeur_max order by m.profondeur_max)::::text profondeurs\n"
            + "from valeur_mesure_chloro_vmchloro v\n"
            + "join composite_node_data_set dnv on dnv.realnode= v.id\n"
            + "JOIN mesure_chloro_mchloro m ON m.mchloro_id = v.mchloro_id\n"
            + "join sous_sequence_chloro_sschloro ss ON ss.sschloro_id = m.sschloro_id\n"
            + "join sequence_chloro_schloro s ON s.schloro_id = ss.schloro_id\n"
            + "\n"
            + "JOIN realnode rnv ON rnv.id = v.id\n"
            + "join composite_nodeable nddvu on nddvu.id = rnv.id_nodeable\n"
            + "join datatype_variable_unite_glacpe_dvug dvu ON rnv.id_nodeable = vdt_id\n"
            + "join realnode rnd ON rnd.id = rnv.id_parent_node\n"
            + "JOIN realnode rnt ON rnt.id = rnd.id_parent_node\n"
            + "JOIN realnode rns ON rns.id = rnt.id_parent_node\n"
            + "JOIN realnode rnts ON rnts.id = rns.id_parent_node\n"
            + "JOIN realnode rnp ON rnp.id = rnts.id_parent_node\n"
            + "group by s.date, rns.path, dnv.branch_node_id, nddvu.code, ss.loc_id, rns.id_nodeable, rnp.id_nodeable, rnp.id_nodeable,  dvu.var_id)";

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        entityManager.createNativeQuery(SYNTHESIS_REQUEST).executeUpdate();
        return Stream.empty();
    }
}

/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.typeplateforme;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface ITypePlateformeDAO extends IDAO<TypePlateforme> {

    /**
     *
     * @param code
     * @return
     */
    public Optional<TypePlateforme> getByCode(String code);
}

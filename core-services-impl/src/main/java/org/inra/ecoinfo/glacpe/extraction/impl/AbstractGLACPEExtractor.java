/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEExtractor extends AbstractExtractor {

    /**
     * The Constant CST_RESULTS.
     */
    public static final String CST_RESULTS = "extractionResults";
    private static final String DVUS = "dvus";
    private static final String PROJET_SITE_NAMES = "projetSiteNames";
    private static final String PLATFORMS = "platforms";

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static List<Long> getDepths(Map<String, Object> requestMetadatas) {
        return (List<Long>) requestMetadatas.getOrDefault("depths", new LinkedList());
    }

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static DepthRequestParamVO getDepthsRequestParam(Map<String, Object> requestMetadatas) {
        return (DepthRequestParamVO) requestMetadatas.getOrDefault(DepthRequestParamVO.class.getSimpleName(), null);
    }

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static DatesRequestParamVO getDatesRequestParam(Map<String, Object> requestMetadatas) {
        return (DatesRequestParamVO) requestMetadatas.getOrDefault(AbstractDatesFormParam.class.getSimpleName(), null);
    }

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static List<IntervalDate> getIntervalsDates(Map<String, Object> requestMetadatas) {
        return (List<IntervalDate>) requestMetadatas.getOrDefault(IntervalDate.class.getSimpleName(), new LinkedList());
    }

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static DatasRequestParamVO getDatasRequestParam(Map<String, Object> requestMetadatas) {
        return (DatasRequestParamVO) requestMetadatas.getOrDefault(DatasRequestParamVO.class.getSimpleName(), null);
    }

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> getPlatforms(Map<String, Object> requestMetadatas) {
        return (SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>>) requestMetadatas.getOrDefault(Plateforme.class.getSimpleName(), new TreeMap<>());
    }

    /**
     *
     * @param requestMetadatas
     * @param type
     * @return
     */
    public static SortedSet<String> getProjetSiteNames(Map<String, Object> requestMetadatas, String type) {
        SortedSet<String> sortedProjetSiteNames = (SortedSet<String>) requestMetadatas.get(PROJET_SITE_NAMES);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        sortedProjetSiteNames = selectedPlateformes.keySet().stream()
                .map(p -> {
                    return selectedPlateformes.get(p).keySet().stream()
                            .map(s -> getProjetSiteName(p, s, type));
                })
                .flatMap(Function.identity())
                .collect(Collectors.toCollection(TreeSet::new));
        requestMetadatas.put(PROJET_SITE_NAMES, sortedProjetSiteNames);
        return sortedProjetSiteNames;
    }

    /**
     *
     * @param requestMetadatas
     * @return
     */
    public static List<DatatypeVariableUniteGLACPE> getDVUs(Map<String, Object> requestMetadatas) {
        List<DatatypeVariableUniteGLACPE> dvus = (List<DatatypeVariableUniteGLACPE>) requestMetadatas.getOrDefault(DatatypeVariableUniteGLACPE.class.getSimpleName(), new LinkedList());
        if (dvus == null) {
            dvus = (List<DatatypeVariableUniteGLACPE>) requestMetadatas.getOrDefault(DatatypeVariableUniteGLACPE.class.getSimpleName(), new LinkedList());
            Collections.sort(dvus);
            requestMetadatas.put(DVUS, dvus);
        }
        return dvus;
    }

    /**
     *
     * @param requestMetadatas
     * @param datatypeName
     * @return
     */
    public static List<DatatypeVariableUniteGLACPE> getDVUs(Map<String, Object> requestMetadatas, String datatypeName) {
        List<DatatypeVariableUniteGLACPE> dvus = (List<DatatypeVariableUniteGLACPE>) requestMetadatas.getOrDefault(DatatypeVariableUniteGLACPE.class.getSimpleName(), new LinkedList());
        if (dvus == null) {
            dvus = ((List<DatatypeVariableUniteGLACPE>) requestMetadatas.getOrDefault(DatatypeVariableUniteGLACPE.class.getSimpleName(), new LinkedList()))
                    .stream()
                    .filter(
                            dvu -> Utils.createCodeFromString(datatypeName).equals(dvu.getDatatype().getCode())
                    )
                    .collect(Collectors.toList());
            Collections.sort(dvus);
            requestMetadatas.put(DVUS, dvus);
        }
        return dvus;
    }

    /**
     *
     * @param projet
     * @param site
     * @param type
     * @return
     */
    public static String getProjetSiteName(Projet projet, SiteGLACPE site, String type) {
        return String.format("%1$s/%2$s/%1$s_%2$s_%3$s", projet.getCode(), site.getCode(), type);
    }

    /**
     *
     * @param dvUs
     * @param datatypeName
     * @return
     */
    public static boolean hasVariable(List<DatatypeVariableUniteGLACPE> dvUs, String datatypeName) {
        return dvUs.stream()
                .anyMatch(dvu -> Utils.createCodeFromString(datatypeName).equals(dvu.getDatatype().getCode()));
    }

    /**
     *
     * @param dvUs
     * @param datatypeName
     * @return
     */
    public static List<DatatypeVariableUniteGLACPE> getVariables(List<DatatypeVariableUniteGLACPE> dvUs, String datatypeName) {
        return dvUs.stream()
                .filter(dvu -> datatypeName.equals(dvu.getDatatype().getCode()))
                .collect(Collectors.toList());
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.dataset.zooplancton.jpa.stadedeveloppement;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement_;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAStadeDeveloppementDAO extends AbstractJPADAO<StadeDeveloppement> implements IStadeDeveloppementDAO {

    /**
     *
     * @param code
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Optional<StadeDeveloppement> getByCode(String code) {
        CriteriaQuery<StadeDeveloppement> query = builder.createQuery(StadeDeveloppement.class);
        Root<StadeDeveloppement> stadeDeveloppement = query.from(StadeDeveloppement.class);
        query
                .select(stadeDeveloppement)
                .where(
                        builder.equal(stadeDeveloppement.get(StadeDeveloppement_.code), code)
                );
        return getOptional(query);
    }

}

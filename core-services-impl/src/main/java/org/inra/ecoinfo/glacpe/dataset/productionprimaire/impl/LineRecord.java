package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String nomSite;
    private String projetCode;
    private String plateformeCode;
    private LocalDate date;
    private Float profondeur;
    private LocalTime debutIncubation;
    private LocalTime finIncubation;
    private LocalTime dureeIncubation;
    private List<VariableValue> variablesValues = new LinkedList<VariableValue>();
    private Long originalLineNumber;

    /**
     *
     */
    public LineRecord() {

    }

    /**
     *
     * @param projetCode
     * @param nomSite
     * @param plateformeCode
     * @param date
     * @param profondeur
     * @param debutIncubation
     * @param finIncubation
     * @param dureeIncubation
     * @param list
     * @param variablesValues
     * @param l
     * @param lineNumber
     */
    public LineRecord(String projetCode, String nomSite, String plateformeCode, LocalDate date, Float profondeur, LocalTime debutIncubation, LocalTime finIncubation, LocalTime dureeIncubation, List<VariableValue> variablesValues, Long lineNumber) {
        this.projetCode = projetCode;
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.date = date;
        this.profondeur = profondeur;
        this.debutIncubation = debutIncubation;
        this.finIncubation = finIncubation;
        this.dureeIncubation = dureeIncubation;
        this.originalLineNumber = lineNumber;
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.projetCode = line.getProjetCode();
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.date = line.getDate();
        this.profondeur = line.getProfondeur();
        this.debutIncubation = line.getDebutIncubation();
        this.finIncubation = line.getFinIncubation();
        this.dureeIncubation = line.getDureeIncubation();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.variablesValues = line.getVariablesValues();

    }

    /**
     *
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @return
     */
    public LocalTime getDebutIncubation() {
        return debutIncubation;
    }

    /**
     *
     * @return
     */
    public LocalTime getFinIncubation() {
        return finIncubation;
    }

    /**
     *
     * @return
     */
    public LocalTime getDureeIncubation() {
        return dureeIncubation;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @param nomSite
     */
    public void setNomSite(String nomSite) {
        this.nomSite = nomSite;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @param projetCode
     */
    public void setProjetCode(String projetCode) {
        this.projetCode = projetCode;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @param plateformeCode
     */
    public void setPlateformeCode(String plateformeCode) {
        this.plateformeCode = plateformeCode;
    }
}

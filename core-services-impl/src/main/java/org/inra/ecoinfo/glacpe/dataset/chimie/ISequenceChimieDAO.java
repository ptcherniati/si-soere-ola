package org.inra.ecoinfo.glacpe.dataset.chimie;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceChimieDAO extends IDAO<SequenceChimie> {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param projetSiteId
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    Optional<SequenceChimie> getByDatePrelevementProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode) throws PersistenceException;

    void flush();

}

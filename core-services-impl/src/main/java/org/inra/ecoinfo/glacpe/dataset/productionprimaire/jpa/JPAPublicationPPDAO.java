package org.inra.ecoinfo.glacpe.dataset.productionprimaire.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationPPDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_PP_BY_IDS = "delete from SequencePP sce where sce.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_PP_BY_IDS = "delete from SousSequencePP ssce where sPP_id in (select id from SequencePP sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_PP_BY_IDS = "delete from MesurePP mce where ssPP_id in (select id from SousSequencePP ssce where sPP_id in (select id from SequencePP sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_PP_BY_IDS = "delete from ValeurMesurePP vmce where mPP_id in (select id from MesurePP mce where ssPP_id in (select id from SousSequencePP ssce where sPP_id in (select id from SequencePP sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_PP_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_PP_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_PP_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_PP_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage(), e);
            throw new PersistenceException(e);
        }
    }
}

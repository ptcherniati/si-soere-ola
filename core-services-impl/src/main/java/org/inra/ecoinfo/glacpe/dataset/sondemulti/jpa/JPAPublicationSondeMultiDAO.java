package org.inra.ecoinfo.glacpe.dataset.sondemulti.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationSondeMultiDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_SONDEMULTI_BY_IDS = "delete from SequenceSondeMulti sce where sce.versionFile= :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_SONDEMULTI_BY_IDS = "delete from SousSequenceSondeMulti ssce where ssondemulti_id in (select id from SequenceSondeMulti sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_SONDEMULTI_BY_IDS = "delete from MesureSondeMulti mce where sssondemulti_id in (select id from SousSequenceSondeMulti ssce where ssondemulti_id in (select id from SequenceSondeMulti sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_SONDEMULTI_BY_IDS = "delete from ValeurMesureSondeMulti vmce where msondemulti_id in (select id from MesureSondeMulti mce where sssondemulti_id in (select id from SousSequenceSondeMulti ssce where ssondemulti_id in (select id from SequenceSondeMulti sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_SONDEMULTI_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_SONDEMULTI_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_SONDEMULTI_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_SONDEMULTI_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new PersistenceException(e);
        }

    }

}

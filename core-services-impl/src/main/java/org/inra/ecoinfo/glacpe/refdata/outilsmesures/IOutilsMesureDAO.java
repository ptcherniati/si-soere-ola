/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.outilsmesures;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public interface IOutilsMesureDAO extends IDAO<OutilsMesure> {

    /**
     *
     * @param code
     * @return
     */
    public Optional<OutilsMesure> getByCode(String code);
}

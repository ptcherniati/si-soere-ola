/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IProjetSiteDAO extends IDAO<ProjetSite> {

    /**
     *
     * @param siteCode
     * @param projetCode
     * @return
     */
    public Optional<ProjetSite> getBySiteCodeAndProjetCode(String siteCode, String projetCode);
}

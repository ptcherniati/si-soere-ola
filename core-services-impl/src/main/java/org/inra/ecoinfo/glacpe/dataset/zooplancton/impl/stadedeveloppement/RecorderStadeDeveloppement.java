/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.dataset.zooplancton.impl.stadedeveloppement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.jpa.stadedeveloppement.IStadeDeveloppementDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class RecorderStadeDeveloppement extends AbstractCSVMetadataRecorder<StadeDeveloppement> {

    /**
     *
     */
    protected IStadeDeveloppementDAO stadeDeveloppementDAO;
    private Properties propertiesNomEN, propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, StadeDeveloppement.NAME_ENTITY_JPA);

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String description = tokenizerValues.nextToken();
                
                
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                persistStadeDeveloppement(errorsReport, nom, description, codeSandre, contexte);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(final CSVParser parser, final File file, final String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                stadeDeveloppementDAO.remove(stadeDeveloppementDAO.getByCode(code).orElseThrow(()->new PersistenceException("can't retrieve stade")));
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } 
    }

    private void persistStadeDeveloppement(final ErrorsReport errorsReport, final String nom, final String description, final String codeSandre, final String contexte) throws PersistenceException {

        StadeDeveloppement dbStadeDeveloppement = stadeDeveloppementDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);

        if (dbStadeDeveloppement == null) {
            StadeDeveloppement stadeDeveloppement = new StadeDeveloppement(nom, description);
            stadeDeveloppement.setCodeSandre(codeSandre);
            stadeDeveloppement.setContexte(contexte);
            stadeDeveloppementDAO.saveOrUpdate(stadeDeveloppement);

        } else {
            dbStadeDeveloppement.setCodeSandre(codeSandre);
            dbStadeDeveloppement.setContexte(contexte);
            dbStadeDeveloppement.setDescription(description);
        }

    }

    /**
     *
     * @param stadeDeveloppementDAO
     */
    public void setStadeDeveloppementDAO(final IStadeDeveloppementDAO stadeDeveloppementDAO) {
        this.stadeDeveloppementDAO = stadeDeveloppementDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StadeDeveloppement stadeDeveloppement) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(stadeDeveloppement == null ? EMPTY_STRING : stadeDeveloppement.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(stadeDeveloppement == null ? EMPTY_STRING : propertiesNomEN.get(stadeDeveloppement.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(stadeDeveloppement == null ? EMPTY_STRING : stadeDeveloppement.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(stadeDeveloppement == null ? EMPTY_STRING : propertiesDescriptionEN.get(stadeDeveloppement.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(stadeDeveloppement == null ? EMPTY_STRING : stadeDeveloppement.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(stadeDeveloppement == null ? EMPTY_STRING : stadeDeveloppement.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<StadeDeveloppement> initModelGridMetadata() {
        propertiesNomEN = localizationManager.newProperties(StadeDeveloppement.NAME_ENTITY_JPA, StadeDeveloppement.FIELD_ENTITY_NAME, Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(StadeDeveloppement.NAME_ENTITY_JPA, StadeDeveloppement.FIELD_ENTITY_DESCRIPTION, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<StadeDeveloppement> getAllElements() throws BusinessException {
        return stadeDeveloppementDAO.getAll(StadeDeveloppement.class);
    }

}

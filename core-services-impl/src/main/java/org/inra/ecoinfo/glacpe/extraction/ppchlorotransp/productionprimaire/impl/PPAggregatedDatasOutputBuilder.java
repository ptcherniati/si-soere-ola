package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class PPAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {


    private static final String BUNDLE_SOURCE_PATH_PP = "org.inra.ecoinfo.glacpe.dataset.productionprimaire.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String CODE_DATATYPE_PRODUCTION_PRIMAIRE = "production_primaire";

    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";
    private static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    private static final String SUFFIX_FILENAME_PP_AGGREGATED = SUFFIX_FILENAME_AGGREGATED.concat(SEPARATOR_TEXT).concat(CODE_DATATYPE_PRODUCTION_PRIMAIRE);
    /**
     *
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new LinkedList<T>(c);
        Collections.sort(list);
        return list;
        
    }

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatas);
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA)));
        try {
            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
                String uniteNom = dvu.getUnite().getCode();

                if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                    stringBuilder.append(String.format(";minimum(%s) (%s)", localizedVariableName, uniteNom));
                }
                if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                    stringBuilder.append(String.format(";maximum(%s) (%s)", localizedVariableName, uniteNom));
                }
            }
            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        String minSeparator = buildCSVSeparator(datasRequestParamVO.getMinValueAndAssociatedDepth());
        String maxSeparator = buildCSVSeparator(datasRequestParamVO.getMaxValueAndAssociatedDepth());

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);

        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_PP_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_PP_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures);

        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> projetEntry : variablesAggregatedDatas.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> mapBySite = projetEntry.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            for (Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> siteEntry : mapBySite.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> mapByPlatform = siteEntry.getValue();
                for (Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> platformEntry : mapByPlatform.entrySet()) {
                    Plateforme plateforme = platformEntry.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapBydate = platformEntry.getValue();
                    final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_PP_AGGREGATED));
                    projetSiteOutputStream.println(headers);
                    PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_PP_AGGREGATED));
                    for (Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> dateEntry : mapBydate.entrySet()) {
                        LocalDate dateKey = dateEntry.getKey();
                        String date = DateUtil.getUTCDateTextFromLocalDateTime(dateKey, DateUtil.DD_MM_YYYY);
                        SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas> mapByRealNode = dateEntry.getValue();
                        for (DatatypeVariableUniteGLACPE dvu : dvus) {
                            VariableAggregatedDatas variableAggregatedDatas = mapByRealNode.get(dvu);
                            if (datasRequestParamVO.getMinValueAndAssociatedDepth()) {
                                if (variableAggregatedDatas != null
                                        && variableAggregatedDatas.getMinAggregatedData() != null
                                        && variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap() != null
                                        && variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().containsKey(dateKey)
                                        && variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) != null) {
                                    for (ValueAggregatedData value : variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                        rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                        rawDataPrintStream.print(String.format("%s;%s;%s;%s;", date, depthMin, depthMax,
                                                value.getDepth() == null ? "" : value.getDepth()));

                                        for (DatatypeVariableUniteGLACPE dvu2 : dvus) {
                                            if (dvu.equals(dvu2)) {
                                                rawDataPrintStream.print(String.format("%s%s%s", value.getValue(), minSeparator, maxSeparator));
                                                break;
                                            } else {
                                                rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                            }
                                        }
                                        rawDataPrintStream.println();
                                    }
                                }
                            }

                            if (datasRequestParamVO.getMaxValueAndAssociatedDepth()) {
                                if (variableAggregatedDatas != null
                                        && variableAggregatedDatas.getMaxAggregatedData() != null
                                        && variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap() != null
                                        && variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().containsKey(dateKey)
                                        && variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) != null) {
                                    for (ValueAggregatedData value : variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                        rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                                        rawDataPrintStream.print(String.format("%s;%s;%s;%s;", date, depthMin, depthMax,
                                                value.getDepth() == null ? "" : value.getDepth()));

                                        for (DatatypeVariableUniteGLACPE dvu2 : dvus) {
                                            if (dvu.equals(dvu2)) {
                                                rawDataPrintStream.print(String.format("%s%s%s", minSeparator, value.getValue(), maxSeparator));
                                                break;
                                            } else {
                                                rawDataPrintStream.print(String.format("%s%s", minSeparator, maxSeparator));
                                            }
                                        }
                                        rawDataPrintStream.println();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }
    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    class ErrorsReport {

        private String errorsMessages = new String();

        public void addErrorMessage(String errorMessage) {
            errorsMessages = errorsMessages.concat("-").concat(errorMessage).concat("\n");
        }

        public String getErrorsMessages() {
            return errorsMessages;
        }

        public boolean hasErrors() {
            return (errorsMessages.length() > 0);
        }
    }


}

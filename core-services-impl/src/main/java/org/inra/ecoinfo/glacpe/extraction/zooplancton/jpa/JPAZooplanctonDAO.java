package org.inra.ecoinfo.glacpe.extraction.zooplancton.jpa;

import com.google.common.base.Strings;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.*;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.DataZooplancton;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.DataZooplancton_;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.MetadataZooplancton;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.MetadataZooplancton_;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.StadeDeveloppement_;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonBiovolume;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonRawsDatas;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure_;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme_;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet_;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon_;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau_;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE_;
import org.inra.ecoinfo.glacpe.synthesis.zooplancton.SynthesisValue;
import org.inra.ecoinfo.glacpe.synthesis.zooplancton.SynthesisValue_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.DataType_;
import org.inra.ecoinfo.refdata.site.Site_;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.refdata.theme.Theme_;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAZooplanctonDAO extends AbstractJPADAO<Object> implements IZooplanctonDAO {

    private static final String ALIAS_PLATFORM = "platform";
    private static final String ALIAS_PROJECT = "projet";
    private static final String ALIAS_SITE = "site";
    private static final String ALIAS_VARIABLE = "variable";
    private static final String ALIAS_DEPTH = "depth";

    private static final String REQUEST_ZOO_AVAILABLES_PROJETS = "select distinct m.projet from MetadataZooplancton m";
    private static final String REQUEST_ZOO_AVAILABLES_PLATEFORMES_BY_PROJET_ID = "select distinct mz.plateforme from MetadataZooplancton mz where mz.projet.id = :projetId";
    private static final String REQUEST_ZOO_AVAILABLES_VARIABLES_BY_PLATEFORMES_ID = "select distinct dz.variable from DataZooplancton dz where dz.metadataZooplancton.plateforme.id in (:plateformesIds) and dz.metadataZooplancton.date between :firstDate and :lastDate and dz.value is not null";

    private static final String REQUEST_EXTRACTION_RAWS_DATA = "select new org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonRawsDatas("
            + "dz.metadataZooplancton.projet.nom,"
            + "dz.metadataZooplancton.plateforme.site.nom,"
            + "dz.metadataZooplancton.plateforme.nom, "
            + "dz.metadataZooplancton.date,"
            + "dz.metadataZooplancton.outilsPrelevement.nom,"
            + " dz.metadataZooplancton.outilsMesure.nom, "
            + "dz.metadataZooplancton.profondeurMin, "
            + "dz.metadataZooplancton.profondeurMax, "
            + "dz.metadataZooplancton.nomDeterminateur,"
            + "dz.metadataZooplancton.volumeSedimente,"
            + "dz.taxon.nomLatin,"
            + "dz.stadeDeveloppement.nom, "
            + "dz.variable.nom, "
            + "dz.value,"
            + "dz.metadataZooplancton.versionFile.dataset.leafNode,"
            + "dz.variable.codeSandre,"
            + "dz.variable.contexte,"
            + "dz.metadataZooplancton.plateforme.site.codeSandrePe,"
            + "dz.metadataZooplancton.plateforme.site.codeSandreMe,"
            + "dz.metadataZooplancton.plateforme.codeSandre,"
            + "dz.metadataZooplancton.plateforme.contexte,"
            + "dz.metadataZooplancton.plateforme.latitude,"
            + "dz.metadataZooplancton.plateforme.longitude,"
            + "dz.metadataZooplancton.plateforme.altitude,"
            + "dz.taxon.taxonParent.nomLatin,"
            + "dz.taxon.codeSandreTax,"
            + "dz.taxon.taxonParent.codeSandreTax,"
            + "dz.taxon.taxonNiveau.nom,"
            + "dz.taxon.taxonNiveau.codeSandre,"
            + "dz.taxon.taxonNiveau.contexte,"
            + "dz.stadeDeveloppement.codeSandre,"
            + "dz.stadeDeveloppement.contexte"
            + ") "
            + "from DataZooplancton dz "
            + "where dz.metadataZooplancton.plateforme.id in (:selectedPlateformesIds) "
            + "and dz.metadataZooplancton.projet.id in (:selectedProjetId) "
            + "and dz.variable.id in (:idsVariablesSelected) "
            + "and dz.taxon.id in (:selectedTaxonsIds) %s ";

    private static final String REQUEST_EXTRACTION_BIOVOLUME = "select new org.inra.ecoinfo.zooplancton.services.ResultExtractionZooplanctonBiovolume(mtdz.projet.nom,mtdz.plateforme.site.nom,mtdz.plateforme.nom, mtdz.date, mtdz.outilsPrelevement.nom, mtdz.outilsMesure.nom, mtdz.profondeurMin, mtdz.profondeurMax, mtdz.nomDeterminateur,mtdz.volumeSedimente) from MetadataZooplancton mtdz where mtdz.plateforme.id in (:selectedPlateformesIds) and mtdz.projet.id in (:selectedProjetId) %s order by mtdz.projet.nom,mtdz.plateforme.site.nom,mtdz.plateforme.nom, mtdz.date";

    private static final String HQL_DATE_FIELD_2 = "mtdz.date";
    private static final String HQL_DATE_FIELD_1 = "dz.metadataZooplancton.date";

    /**
     *
     * @param user
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms(IUser user) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        ArrayList<Predicate> and = new ArrayList<>();
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<SynthesisValue, Projet> projet = sv.join(SynthesisValue_.projetId);
        Join<SynthesisValue, Plateforme> plateforme = sv.join(SynthesisValue_.plateformeId);
        Join<SynthesisValue, SiteGLACPE> site = sv.join(SynthesisValue_.siteId);
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, sv.get(SynthesisValue_.date));
        query
                .multiselect(projet.alias(ALIAS_PROJECT), site.alias(ALIAS_SITE), plateforme.alias(ALIAS_PLATFORM))
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultListToStream(query)
                .map(tuple -> getRealNodePlatformFromTuple(tuple))
                .collect(Collectors.toCollection(TreeSet::new));
    }

    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            List<String> groups = getGroups(user);
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            List<Predicate> orLogin = new LinkedList<>();
            predicatesAnd.add(er.get(ExtractActivity_.login).in(groups));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

    /**
     *
     * @param user
     * @return
     */
    public List<String> getGroups(IUser user) {
        List<String> l = new LinkedList<>();
        if (user instanceof ICompositeGroup) {
            l = user.getAllGroups().stream()
                    .filter(g -> g.getWhichTree().equals(WhichTree.TREEDATASET))
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
        } else {
            l.add(user.getLogin());
        }
        return l;
    }

    private PlateformeProjetVO getRealNodePlatformFromTuple(Tuple tuple) {
        Plateforme platform = tuple.get(ALIAS_PLATFORM, Plateforme.class);
        SiteGLACPE site = tuple.get(ALIAS_SITE, SiteGLACPE.class);
        Projet projet = tuple.get(ALIAS_PROJECT, Projet.class);
        return new PlateformeProjetVO(platform, projet, site);
    }

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getAvailableVariables(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        ArrayList<Predicate> and = new ArrayList<>();
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Path<RealNode> rnv = ndsv.get(NodeDataSet_.realNode);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Join<SynthesisValue, Projet> projet = sv.join(SynthesisValue_.projetId);
        Join<SynthesisValue, Plateforme> plateforme = sv.join(SynthesisValue_.plateformeId);
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        and.add(builder.equal(rnv.get(RealNode_.nodeable), dvu.get(DatatypeVariableUniteGLACPE_.id)));
        if (!selectedPlatforms.isEmpty()) {
            if (selectedPlatforms.size() == 1) {
                and.add(builder.equal(projet, selectedPlatforms.get(0).getProjet()));
                and.add(builder.equal(plateforme, selectedPlatforms.get(0).getPlateforme()));
            } else {
                ArrayList<Predicate> or = new ArrayList<>();
                selectedPlatforms.stream()
                        .forEach(pp -> or.add(builder.and(
                        builder.equal(projet, pp.getProjet()),
                        builder.equal(plateforme, pp.getPlateforme())
                )));
                and.add(builder.or(or.toArray(new Predicate[and.size()])));
            }
        }
        Path<LocalDateTime> date = sv.get(SynthesisValue_.date);
        if (!selectedDates.isEmpty()) {
            selectedDates.stream()
                    .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        }
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(dvu)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param intervalDates
     * @param datesRequestParamVO
     * @param dvus
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ResultExtractionZooplanctonBiovolume> extractDatasBiovolume(IUser user, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes, List<IntervalDate> intervalDates, List<DatatypeVariableUniteGLACPE> dvus) {
        CriteriaQuery<ResultExtractionZooplanctonBiovolume> query = builder.createQuery(ResultExtractionZooplanctonBiovolume.class);
        Root<DataZooplancton> dataZooplancton = query.from(DataZooplancton.class);
        Join<DataZooplancton, MetadataZooplancton> zooplancton = dataZooplancton.join(DataZooplancton_.metadataZooplancton);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        List<Predicate> and = new LinkedList<>();
        and.add(builder.equal(rnv, dataZooplancton.get(DataZooplancton_.realNode)));
        Join<RealNode, RealNode> rns = rnv.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<MetadataZooplancton, Projet> dbProjet = zooplancton.join(MetadataZooplancton_.projet);
        Join<MetadataZooplancton, Plateforme> plateforme = zooplancton.join(MetadataZooplancton_.plateforme);
        Join<Plateforme, SiteGLACPE> dbSite = plateforme.join(Plateforme_.site);
        Join<MetadataZooplancton, OutilsMesure> outilsPrelevement = zooplancton.join(MetadataZooplancton_.outilsPrelevement);
        Join<MetadataZooplancton, OutilsMesure> outilsMesure = zooplancton.join(MetadataZooplancton_.outilsMesure);
        dataZooplancton.join(DataZooplancton_.taxon);
        List<Predicate> orPlateforme = new LinkedList();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> ProjetEntry : selectedPlateformes.entrySet()) {
            Projet projet = ProjetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> platformsBySites = ProjetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : platformsBySites.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = (SortedSet<Plateforme>) siteEntry.getValue();
                for (Plateforme platform : platforms) {
                    orPlateforme.add(
                            builder.and(
                                    builder.equal(rns.get(RealNode_.nodeable), site),
                                    builder.equal(rnp.get(RealNode_.nodeable), projet),
                                    builder.equal(plateforme, platform)
                            ));
                }
            }
        }
        if (!dvus.isEmpty()) {
            and.add(rnv.get(RealNode_.nodeable).in(dvus));
        }
        if (!orPlateforme.isEmpty()) {
            and.add(builder.or(orPlateforme.stream().toArray(Predicate[]::new)));
        }
        final Path<LocalDate> date = zooplancton.get(MetadataZooplancton_.date);
        intervalDates.stream()
                .map(interval -> builder.between(date, interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        l -> and.add(builder.or(l.stream().toArray(Predicate[]::new)))
                ));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        final Path<String> projetName = dbProjet.get(Projet_.name);
        final Path<String> siteName = dbSite.get(Site_.name);
        final Path<String> plateformename = plateforme.get(Plateforme_.name);
        query
                .select(builder.construct(
                        ResultExtractionZooplanctonBiovolume.class,
                        projetName,
                        siteName,
                        plateformename,
                        date,
                        outilsPrelevement.get(OutilsMesure_.code),
                        outilsMesure.get(OutilsMesure_.code),
                        zooplancton.get(MetadataZooplancton_.profondeurMin),
                        zooplancton.get(MetadataZooplancton_.profondeurMax),
                        zooplancton.get(MetadataZooplancton_.nomDeterminateur),
                        zooplancton.get(MetadataZooplancton_.volumeSedimente)
                )
                )
                .orderBy(
                        builder.asc(projetName),
                        builder.asc(siteName),
                        builder.asc(plateformename),
                        builder.asc(date)
                )
                .where(
                        and.stream().toArray(Predicate[]::new)
                );
        return getResultList(query);
    }

    /**
     *
     * @param selectedPlateformesIds
     * @param projetId
     * @param intervalDates
     * @param datesRequestParamVO
     * @param selectedTaxonsIds
     * @param idsTaxonsLeaves
     * @param idsVariablesSelected
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ResultExtractionZooplanctonRawsDatas> extractDatas(IUser user, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes, List<IntervalDate> intervalDates, List<DatatypeVariableUniteGLACPE> dvus, List<Long> idsTaxonsLeaves) {
        CriteriaQuery<ResultExtractionZooplanctonRawsDatas> query = builder.createQuery(ResultExtractionZooplanctonRawsDatas.class);
        Root<DataZooplancton> dataZooplancton = query.from(DataZooplancton.class);
        Join<DataZooplancton, MetadataZooplancton> zooplancton = dataZooplancton.join(DataZooplancton_.metadataZooplancton);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        List<Predicate> and = new LinkedList<>();
        and.add(builder.equal(rnv, dataZooplancton.get(DataZooplancton_.realNode)));
        final Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, Nodeable> datatypeNdb = rnd.join(RealNode_.nodeable);
        Join<RealNode, DataType> datatype = builder.treat(datatypeNdb, DataType.class);
        final Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, Nodeable> themeNdb = rnt.join(RealNode_.nodeable);
        Join<RealNode, Theme> theme = builder.treat(themeNdb, Theme.class);
        Join<RealNode, RealNode> rns = rnt.join(RealNode_.parent);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<MetadataZooplancton, Projet> dbProjet = zooplancton.join(MetadataZooplancton_.projet);
        Join<MetadataZooplancton, Plateforme> plateforme = zooplancton.join(MetadataZooplancton_.plateforme);
        Join<Plateforme, SiteGLACPE> dbSite = plateforme.join(Plateforme_.site);
        Join<MetadataZooplancton, OutilsMesure> outilsPrelevement = zooplancton.join(MetadataZooplancton_.outilsPrelevement);
        Join<MetadataZooplancton, OutilsMesure> outilsMesure = zooplancton.join(MetadataZooplancton_.outilsMesure);
        Join<DataZooplancton, Taxon> taxon = dataZooplancton.join(DataZooplancton_.taxon);
        and.add(taxon.get(Taxon_.id).in(idsTaxonsLeaves));
        Join<Taxon, Taxon> taxonParent = taxon.join(Taxon_.taxonParent);
        Path<TaxonNiveau> taxonNiveau = taxon.get(Taxon_.taxonNiveau);
        Join<DataZooplancton, StadeDeveloppement> stadeDeveloppement = dataZooplancton.join(DataZooplancton_.stadeDeveloppement);
        List<Predicate> orPlateforme = new LinkedList();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> ProjetEntry : selectedPlateformes.entrySet()) {
            Projet projet = ProjetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> platformsBySites = ProjetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : platformsBySites.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = (SortedSet<Plateforme>) siteEntry.getValue();
                for (Plateforme platform : platforms) {
                    orPlateforme.add(
                            builder.and(
                                    builder.equal(rns.get(RealNode_.nodeable), site),
                                    builder.equal(rnp.get(RealNode_.nodeable), projet),
                                    builder.equal(plateforme, platform)
                            ));
                }
            }
        }
        Join<RealNode, Nodeable> dvuNdb = rnv.join(RealNode_.nodeable);
        and.add(builder.equal(dvuNdb.get(Nodeable_.id), dvu.get(DatatypeVariableUniteGLACPE_.id)));
        Join<DatatypeVariableUniteGLACPE, VariableGLACPE> variable = dvu.join(DatatypeVariableUniteGLACPE_.variable);
        if (!dvus.isEmpty()) {
            and.add(dvu.in(dvus));
        }
        if (!orPlateforme.isEmpty()) {
            and.add(builder.or(orPlateforme.stream().toArray(Predicate[]::new)));
        }
        final Path<LocalDate> date = zooplancton.get(MetadataZooplancton_.date);
        intervalDates.stream()
                .map(interval -> builder.between(date, interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        l -> and.add(builder.or(l.stream().toArray(Predicate[]::new)))
                ));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        final Path<String> projetName = dbProjet.get(Projet_.name);
        final Path<String> siteName = dbSite.get(Site_.name);
        final Path<String> plateformename = plateforme.get(Plateforme_.name);
        query
                .select(builder.construct(
                        ResultExtractionZooplanctonRawsDatas.class,
                        projetName,
                        siteName,
                        plateformename,
                        date,
                        outilsPrelevement.get(OutilsMesure_.code),
                        outilsMesure.get(OutilsMesure_.code),
                        zooplancton.get(MetadataZooplancton_.profondeurMin),
                        zooplancton.get(MetadataZooplancton_.profondeurMax),
                        zooplancton.get(MetadataZooplancton_.nomDeterminateur),
                        zooplancton.get(MetadataZooplancton_.volumeSedimente),
                        taxon.get(Taxon_.nomLatin),
                        stadeDeveloppement.get(StadeDeveloppement_.nom),
                        variable.get(VariableGLACPE_.name),
                        dataZooplancton.get(DataZooplancton_.value),
                        rnv.get(RealNode_.id),
                        theme.get(Theme_.code),
                        datatype.get(DataType_.name),
                        variable.get(VariableGLACPE_.codeSandre),
                        variable.get(VariableGLACPE_.contexte),
                        dbSite.get(SiteGLACPE_.codeSandrePe),
                        dbSite.get(SiteGLACPE_.codeSandreMe),
                        plateforme.get(Plateforme_.codeSandre),
                        plateforme.get(Plateforme_.contexte),
                        plateforme.get(Plateforme_.latitude),
                        plateforme.get(Plateforme_.longitude),
                        plateforme.get(Plateforme_.altitude),
                        taxonParent.get(Taxon_.nomLatin),
                        taxon.get(Taxon_.codeSandreTax),
                        taxonParent.get(Taxon_.codeSandreTax),
                        taxonNiveau.get(TaxonNiveau_.nom),
                        taxonNiveau.get(TaxonNiveau_.codeSandre),
                        taxonNiveau.get(TaxonNiveau_.contexte),
                        stadeDeveloppement.get(StadeDeveloppement_.codeSandre),
                        stadeDeveloppement.get(StadeDeveloppement_.contexte)
                )
                )
                .orderBy(
                        builder.asc(projetName),
                        builder.asc(siteName),
                        builder.asc(plateformename),
                        builder.asc(date)
                )
                .where(
                        and.stream().toArray(Predicate[]::new)
                );
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Taxon> getAvailableTaxons(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        String requestDepths = "select distinct unnest(taxons::::text[]) code  from zooplanctonsynthesisvalue\n"
                + "join site_glacpe_sit site on site.id = siteid\n"
                + "join projet_pro projet on projet.pro_id = projetid\n"
                + "join plateforme_pla plateforme on plateforme.loc_id = plateformeid\n"
                + "join variable_glacpe_varg variable on variable.var_id= variableid\n"
                + "%s\n"
                + "where %s\n"
                + "order by code";
        /*(projetid,plateformeid)in (%s))\n"
        + "and date BETWEEN %s and %s\n"
        + "and variableid in (%s)*/
        String plateformeWhere = CollectionUtils.isEmpty(selectedPlatforms) ? "" : selectedPlatforms.stream()
                .map(sp -> String.format("(%s,%s)", sp.getProjet().getId(), sp.getPlateforme().getId()))
                .collect(Collectors.joining(",", " (projetid,plateformeid)in (", ") "));
        String variableWhere = CollectionUtils.isEmpty(selectedDates) ? "" : selectedDates.stream()
                .map(interval -> String.format(" date BETWEEN '%s' and '%s' ", DateUtil.getUTCDateTextFromLocalDateTime(interval.getBeginDate(), DateUtil.YYYY_MM_DD_FILE), DateUtil.getUTCDateTextFromLocalDateTime(interval.getEndDate(), DateUtil.YYYY_MM_DD_FILE)))
                .collect(Collectors.joining("or", "( ", ") "));
        String depthsWhere = CollectionUtils.isEmpty(selectedVariables) ? "" : selectedVariables.stream()
                .map(dvu -> dvu.getVariable().getId().toString())
                .collect(Collectors.joining(",", " variableid in ( ", ") "));
        String restrictiveSql = "", joinRights = "";
        if (!user.getIsRoot()) {
            joinRights = "join compositeactivityextraction using(idnode)";
            restrictiveSql = " (compositeactivityextraction.datestart is null or compositeactivityextraction.datestart<=date)\n"
                    + "and (compositeactivityextraction.dateend is null or compositeactivityextraction.dateend>=date)";
        }
        String sqlWhere = Stream.of(new String[]{plateformeWhere, variableWhere, depthsWhere, restrictiveSql})
                .filter(sql -> !Strings.isNullOrEmpty(sql))
                .collect(Collectors.joining(" and "));
        List<String> codeTaxon = entityManager.createNativeQuery(String.format(requestDepths, joinRights, sqlWhere)).getResultList();
        CriteriaQuery<Taxon> query = builder.createQuery(Taxon.class);
        Root<Taxon> taxon = query.from(Taxon.class);
        Fetch<Taxon, TaxonProprieteTaxon> proprietes = taxon.fetch(Taxon_.taxonProprieteTaxon, JoinType.LEFT);
        query
                .select(taxon)
                .where(taxon.get(Taxon_.code).in(codeTaxon));
        return getResultList(query);
    }

}

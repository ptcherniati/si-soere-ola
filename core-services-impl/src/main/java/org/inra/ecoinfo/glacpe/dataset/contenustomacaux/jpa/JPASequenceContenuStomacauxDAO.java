package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.ISequenceContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity.SequenceContenuStomacaux_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPASequenceContenuStomacauxDAO extends AbstractJPADAO<SequenceContenuStomacaux> implements ISequenceContenuStomacauxDAO {

    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     */
    @Override
    public Optional<SequenceContenuStomacaux> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode)  {
            CriteriaQuery<SequenceContenuStomacaux> query = builder.createQuery(SequenceContenuStomacaux.class);
            Root<SequenceContenuStomacaux> s = query.from(SequenceContenuStomacaux.class);
        Join<RealNode, RealNode> realNodeSite = s
                .join(SequenceContenuStomacaux_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        Join<RealNode, Nodeable> projet = realNodeSite.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.nodeable);
        query
                .select(s)
                .where(
                        builder.equal(s.get(SequenceContenuStomacaux_.datePrelevement), datePrelevement),
                        builder.equal(projet.get(Nodeable_.code), projetCode),
                        builder.equal(site.get(Nodeable_.code), siteCode)
                );
        return getOptional(query);
    }

}

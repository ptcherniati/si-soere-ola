package org.inra.ecoinfo.glacpe.extraction.chimie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieOutputsBuildersResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {
    
    private static final String EXTRACTION = "extraction";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     *
     */
    protected static final String SEPARATOR_TEXT = "_";

    /**
     *
     */
    protected static final String EXTENSION_ZIP = ".zip";

    /**
     *
     */
    protected IOutputBuilder chimieAggregatedDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder chimieMeanDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder requestReminderOutputBuilder;
    
    protected IOutputBuilder physicoChimieOutputBuilderSandre;

    /**
     *
     */
    protected IOutputBuilder chimieOutputBuilder;
    
    private IFileCompConfiguration fileCompConfiguration;
    
    /**
     *
     */
    public ChimieOutputsBuildersResolver() {
    }
    
    private void addOutputBuilderByCondition(Boolean condition, IOutputBuilder outputBuilder, List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }
    
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        List<IOutputBuilder> outputsBuilders = new LinkedList<IOutputBuilder>();
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(metadatasMap);
        
        addOutputBuilderByCondition(datasRequestParamVO.getDataBalancedByDepth(), chimieMeanDatasOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData(), chimieOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition((datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()), chimieAggregatedDatasOutputBuilder, outputsBuilders);
        outputsBuilders.add(requestReminderOutputBuilder);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData(), physicoChimieOutputBuilderSandre, outputsBuilders);
        return outputsBuilders;
    }

    /**
     *
     * @param chimieAggregatedDatasOutputBuilder
     */
    public void setChimieAggregatedDatasOutputBuilder(IOutputBuilder chimieAggregatedDatasOutputBuilder) {
        this.chimieAggregatedDatasOutputBuilder = chimieAggregatedDatasOutputBuilder;
    }
    
    /**
     *
     * @param physicoChimieOutputBuilderSandre
     */
    public void setPhysicoChimieOutputBuilderSandre(IOutputBuilder physicoChimieOutputBuilderSandre) {
        this.physicoChimieOutputBuilderSandre = physicoChimieOutputBuilderSandre;
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     */
    protected void buidOutputFile(final IParameter parameters) throws BusinessException {
        final Set<String> datatypeNames = new HashSet<>();
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     * Builds the output.
     *
     * @param parameters
     * @return
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.extraction.IOutputBuilder#buildOutput(org.inra.ecoinfo
     * .extraction.IParameter)
     * @link(IParameter)
     */
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        NoExtractionResultException noDataToExtract = null;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            List<IOutputBuilder> outputBuilders = resolveOutputsBuilders(parameters.getParameters());
            for (IOutputBuilder outputBuilder : outputBuilders) {
                try {
                    outputBuilder.buildOutput(parameters);
                } catch (NoExtractionResultException e) {
                    noDataToExtract = e;
                }
            }
            parameters.getParameters().put(AbstractGLACPEExtractor.CST_RESULTS, parameters.getResults());
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
        } catch (FileNotFoundException e1) {
            throw new BusinessException("FileNotFoundException", e1);
        } catch (IOException e) {
            throw new BusinessException("IOException", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getChimieOutputBuilder() {
        return chimieOutputBuilder;
    }

    /**
     *
     * @param chimieOutputBuilder
     */
    public void setChimieOutputBuilder(IOutputBuilder chimieOutputBuilder) {
        this.chimieOutputBuilder = chimieOutputBuilder;
    }

    /**
     *
     * @param chimieMeanDatasOutputBuilder
     */
    public void setChimieMeanDatasOutputBuilder(IOutputBuilder chimieMeanDatasOutputBuilder) {
        this.chimieMeanDatasOutputBuilder = chimieMeanDatasOutputBuilder;
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }
    
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }
    
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }
}

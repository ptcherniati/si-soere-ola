package org.inra.ecoinfo.glacpe.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;

/**
 *
 * @author ptcherniati
 */
public interface ITestFormat extends Serializable {

    /** The Constant PROPERTY_MSG_ERROR_BAD_FORMAT. */
    static final String PROPERTY_MSG_ERROR_BAD_FORMAT = "PROPERTY_MSG_ERROR_BAD_FORMAT";

    /** The Constant PROPERTY_MSG_CHECKING_FORMAT_FILE. */
    static final String PROPERTY_MSG_CHECKING_FORMAT_FILE = "PROPERTY_MSG_CHECKING_FORMAT_FILE";

    /** The Constant PROPERTY_MSG_BAD_HEADER_SIZE. */
    static final String PROPERTY_MSG_BAD_HEADER_SIZE = "PROPERTY_MSG_BAD_HEADER_SIZE";

    /** The Constant DATASET_DESCRIPTOR_XML. */
    static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";

    /**
     * Sets the datatype name.
     * 
     * @param datatypeName
     *            {@link String} the new datatypeName
     */
    void setDatatypeName(String datatypeName);

    /**
     * Sets the test headers.
     * 
     * @param testHeaders
     *            setter of object that test the headers of the file
     */
    void setTestHeaders(ITestHeaders testHeaders);

    /**
     * Sets the test values.
     * 
     * @param testValues
     *            the new test values
     */
    void setTestValues(ITestValues testValues);

    /**
     * Test format.
     * 
     * @param parser
     *            the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(ISessionPropertiesGLACPE) the session properties
     * @param encoding
     *            the encoding
     * @param datasetDescriptor
     * @link(DatasetDescriptorGLACPE) the dataset descriptor
     * @throws BadFormatException
     *             the bad format exception {@link VersionFile} the version file {@link ISessionPropertiesGLACPE} the session properties {@link DatasetDescriptorGLACPE} test the format of a version file
     */
    void testFormat(CSVParser parser, VersionFile versionFile, String encoding, DatasetDescriptor datasetDescriptor) throws BadFormatException;
}

package org.inra.ecoinfo.glacpe.extraction.zooplancton;

import java.time.LocalDate;

/**
 *
 * @author ptcherniati
 */
public class ResultExtractionZooplanctonBiovolume implements Comparable<ResultExtractionZooplanctonBiovolume> {

    private String projet;
    private String site;
    private String plateforme;
    private LocalDate date;
    private String outilPrelevement;
    private String outilMesure;
    private Float profondeurMin;
    private Float profondeurMax;
    private String nomDeterminateur;
    private Float volumeSedimente;

    /**
     *
     * @param projet
     * @param site
     * @param plateforme
     * @param date
     * @param outilPrelevement
     * @param outilMesure
     * @param profondeurMin
     * @param profondeurMax
     * @param string5
     * @param nomDeterminateur
     * @param f2
     * @param volumeSedimente
     */
    public ResultExtractionZooplanctonBiovolume(String projet, String site, String plateforme, LocalDate date, String outilPrelevement, String outilMesure, Float profondeurMin, Float profondeurMax, String nomDeterminateur, Float volumeSedimente) {
        super();
        this.projet = projet;
        this.site = site;
        this.plateforme = plateforme;
        this.date = date;
        this.outilPrelevement = outilPrelevement;
        this.outilMesure = outilMesure;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.nomDeterminateur = nomDeterminateur;
        this.volumeSedimente = volumeSedimente;

    }

    /**
     *
     * @return
     */
    public String getProjet() {
        return projet;
    }

    /**
     *
     * @return
     */
    public String getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(String projet) {
        this.projet = projet;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(String plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getSite() {
        return site;
    }

    /**
     *
     * @return
     */
    public String getOutilPrelevement() {
        return outilPrelevement;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @return
     */
    public String getNomDeterminateur() {
        return nomDeterminateur;
    }

    /**
     *
     * @return
     */
    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    /**
     *
     * @param site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     *
     * @param outilPrelevement
     */
    public void setOutilPrelevement(String outilPrelevement) {
        this.outilPrelevement = outilPrelevement;
    }

    /**
     *
     * @param profondeurMin
     */
    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    /**
     *
     * @param profondeurMax
     */
    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    /**
     *
     * @param nomDeterminateur
     */
    public void setNomDeterminateur(String nomDeterminateur) {
        this.nomDeterminateur = nomDeterminateur;
    }

    /**
     *
     * @param volumeSedimente
     */
    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    /**
     *
     * @return
     */
    public String getOutilMesure() {
        return outilMesure;
    }

    /**
     *
     * @param outilMesure
     */
    public void setOutilMesure(String outilMesure) {
        this.outilMesure = outilMesure;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(ResultExtractionZooplanctonBiovolume t) {
        if (t == null) {
            return -1;
        }
        int compareToProjet = getProjet().compareTo(t.getProjet());
        if (compareToProjet != 0) {
            return compareToProjet;
        }
        int compareToSite = getSite().compareTo(t.getSite());
        if (compareToSite != 0) {
            return compareToSite;
        }
        int compareToPlateforme = getPlateforme().compareTo(t.getPlateforme());
        if (compareToPlateforme != 0) {
            return compareToPlateforme;
        }
        int compareToDate = getDate().compareTo(t.getDate());
        if (compareToDate != 0) {
            return compareToDate;
        }
        int compareToProfondeurMax = getProfondeurMax().compareTo(t.getProfondeurMax());
        if (compareToProfondeurMax != 0) {
            return compareToProfondeurMax;
        }
        return getOutilMesure().compareTo(t.getOutilMesure());
    }

}

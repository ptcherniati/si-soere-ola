package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDAO;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonBiovolume;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NotYetImplementedException;

/**
 *
 * @author ptcherniati
 */
public class ExtractorZooBiovolume extends AbstractExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "zooplanctonBiovolume";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "MSG_NO_EXTRACTION_RESULT_BIOVOLUMES";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.zooplancton.services.messages";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<IntervalDate> intervalsDates = getIntervalsDates(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        List<ResultExtractionZooplanctonBiovolume> results = null;

        results = (List<ResultExtractionZooplanctonBiovolume>) zooplanctonDAO.extractDatasBiovolume(policyManager.getCurrentUser(), selectedPlateformes, intervalsDates, dvus);
        if (results == null || results.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(MAP_INDEX_0, results);
        return extractedDatasMap;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        // Map<String, List> filteredResultsDatasMap = filterExtractedDatas(resultsDatasMap);
        ((ZooplanctonParameters) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new NotYetImplementedException();

    }

}

package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.impl;

import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String nomSite;
    private String plateformeCode;
    private LocalDate datePrelevement;
    private String outilsMesureCode;
    private String nomPecheur;
    private String nomEspece;
    private String maille;
    private Float duree;
    private String utilisationComptage;
    private String poissonCompte;
    private Integer numeroPoisson;
    private Float taillePoisson;
    private String malformation;
    private String sexe;
    private String maturite;
    private String estomacVide;
    private Float poids;
    private String etatEstomac;
    private Integer numeroPilulier;
    private Integer numeroEcaille;
    private String commentaires;
    private Float profondeur;
    private Long originalLineNumber;
    private String projetCode;
    private List<VariableValue> variablesValues;

    /**
     *
     */
    public LineRecord() {

    }

    /**
     *
     * @param nomSite
     * @param plateformeCode
     * @param datePrelevement
     * @param outilsMesureCode
     * @param nomPecheur
     * @param nomEspece
     * @param maille
     * @param duree
     * @param utilisationComptage
     * @param poissonCompte
     * @param numeroPoisson
     * @param taillePoisson
     * @param malformation
     * @param sexe
     * @param maturite
     * @param estomacVide
     * @param poids
     * @param etatEstomac
     * @param numeroPilulier
     * @param numeroEcaille
     * @param commentaires
     * @param profondeur
     * @param variablesValues
     * @param l
     * @param originalLineNumber
     * @param string14
     * @param projetCode
     */
    public LineRecord(String nomSite, String plateformeCode, LocalDate datePrelevement, String outilsMesureCode, String nomPecheur, String nomEspece, String maille, Float duree, String utilisationComptage, String poissonCompte, Integer numeroPoisson,
        Float taillePoisson, String malformation, String sexe, String maturite, String estomacVide, Float poids, String etatEstomac, Integer numeroPilulier, Integer numeroEcaille, String commentaires, Float profondeur,
        List<VariableValue> variablesValues, Long originalLineNumber, String projetCode)
    {
        super();
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.datePrelevement = datePrelevement;
        this.outilsMesureCode = outilsMesureCode;
        this.nomPecheur = nomPecheur;
        this.nomEspece = nomEspece;
        this.maille = maille;
        this.duree = duree;
        this.utilisationComptage = utilisationComptage;
        this.numeroPoisson = numeroPoisson;
        this.taillePoisson = taillePoisson;
        this.malformation = malformation;
        this.sexe = sexe;
        this.maturite = maturite;
        this.estomacVide = estomacVide;
        this.poids = poids;
        this.etatEstomac = etatEstomac;
        this.numeroPilulier = numeroPilulier;
        this.numeroEcaille = numeroEcaille;
        this.commentaires = commentaires;
        this.profondeur = profondeur;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
        this.projetCode = projetCode;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.datePrelevement = line.getDatePrelevement();
        this.outilsMesureCode = line.getOutilsMesureCode();
        this.nomPecheur = line.getNomPecheur();
        this.nomEspece = line.getNomEspece();
        this.maille = line.getMaille();
        this.duree = line.getDuree();
        this.utilisationComptage = line.getUtilisationComptage();
        this.numeroPoisson = line.getNumeroPoisson();
        this.taillePoisson = line.getTaillePoisson();
        this.malformation = line.getMalformation();
        this.sexe = line.getSexe();
        this.maturite = line.getMaturite();
        this.estomacVide = line.getEstomacVide();
        this.poids = line.getPoids();
        this.etatEstomac = line.getEtatEstomac();
        this.numeroPilulier = line.getNumeroPilulier();
        this.numeroEcaille = line.getNumeroEcaille();
        this.commentaires = line.getCommentaires();
        this.profondeur = line.getProfondeur();
        this.variablesValues = line.getVariablesValues();

        this.originalLineNumber = line.getOriginalLineNumber();
        this.projetCode = getProjetCode();
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @return
     */
    public String getOutilsMesureCode() {
        return outilsMesureCode;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @param variablesValues
     */
    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     *
     * @return
     */
    public String getNomPecheur() {
        return nomPecheur;
    }

    /**
     *
     * @return
     */
    public String getNomEspece() {
        return nomEspece;
    }

    /**
     *
     * @return
     */
    public String getMaille() {
        return maille;
    }

    /**
     *
     * @return
     */
    public Float getDuree() {
        return duree;
    }

    /**
     *
     * @return
     */
    public String getUtilisationComptage() {
        return utilisationComptage;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroPoisson() {
        return numeroPoisson;
    }

    /**
     *
     * @return
     */
    public Float getTaillePoisson() {
        return taillePoisson;
    }

    /**
     *
     * @return
     */
    public String getMalformation() {
        return malformation;
    }

    /**
     *
     * @return
     */
    public String getSexe() {
        return sexe;
    }

    /**
     *
     * @return
     */
    public String getMaturite() {
        return maturite;
    }

    /**
     *
     * @return
     */
    public String getEstomacVide() {
        return estomacVide;
    }

    /**
     *
     * @return
     */
    public Float getPoids() {
        return poids;
    }

    /**
     *
     * @return
     */
    public String getEtatEstomac() {
        return etatEstomac;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroPilulier() {
        return numeroPilulier;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroEcaille() {
        return numeroEcaille;
    }

    /**
     *
     * @return
     */
    public String getPoissonCompte() {
        return poissonCompte;
    }

    /**
     *
     * @param poissonCompte
     */
    public void setPoissonCompte(String poissonCompte) {
        this.poissonCompte = poissonCompte;
    }
}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGlacpeRequestReminder;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ConditionGeneraleRequestReminderOutputBuilder extends AbstractGlacpeRequestReminder {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "conditionPrelevementRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";

    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";

    private static final String PATTERN_STRING_PLATEFORMS_SUMMARY = "   %s (%s: %s)";
    private static final String PATTERN_STRING_HEADER_PLATEFORMS_SUMMARY = "%s\n   %s";

    private static final String MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";
    private static final String MSG_SITE = "PROPERTY_MSG_SITE";
    private static final String MSG_SELECTED_PROJECT = "PROPERTY_MSG_SELECTED_PROJECT";
    private static final String MSG_SELECTED_PLATEFORMS = "PROPERTY_MSG_SELECTED_PLATEFORMS";

    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_HEADER);
    }

    /**
     *
     * @param headers
     * @param resultsDatasMap
     * @param parameters
     * @return
     * @throws BusinessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;

        try {

            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printPlateformesSummary(requestMetadatasMap, reminderPrintStream);
            DatasRequestParamVO datasRequestParamVO = new DatasRequestParamVO();
            datasRequestParamVO.setRawData(Boolean.TRUE);
            datasRequestParamVO.setLocalizationManager(localizationManager);
            datasRequestParamVO.buildSummary(reminderPrintStream);
            printDatesSummary(requestMetadatasMap, reminderPrintStream);
            printComment(reminderPrintStream, requestMetadatasMap);

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;
    }
}

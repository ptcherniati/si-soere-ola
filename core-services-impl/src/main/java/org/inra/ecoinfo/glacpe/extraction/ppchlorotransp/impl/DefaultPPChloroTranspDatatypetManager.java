/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.ppchloroatransp.IPpChloroTranspDatatypeManager;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class DefaultPPChloroTranspDatatypetManager extends MO implements IPpChloroTranspDatatypeManager {

    IPPDatatypeManager ppDatatypeManager;
    IChlorophylleDatatypeManager chlorophylleDatatypeManager;
    IConditionGeneraleDatatypeManager conditionGeneraleDatatypeManager;

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        TreeSet<PlateformeProjetVO> availablePlatforms = new TreeSet<>();
        availablePlatforms.addAll(ppDatatypeManager.getAvailablePlatforms());
        availablePlatforms.addAll(chlorophylleDatatypeManager.getAvailablePlatforms());
        availablePlatforms.addAll(conditionGeneraleDatatypeManager.getAvailablePlatformsTransparence());
        return availablePlatforms;
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        TreeSet<DatatypeVariableUniteGLACPE> availableVariables = new TreeSet<>();
        availableVariables.addAll(ppDatatypeManager.getAvailableVariables(selectedPlatforms, selectedDates));
        availableVariables.addAll(chlorophylleDatatypeManager.getAvailableVariables(selectedPlatforms, selectedDates));
        availableVariables.addAll(conditionGeneraleDatatypeManager.getAvailableVariablesTransparence(selectedPlatforms, selectedDates));
        return availableVariables;
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Float> getAvailableDepths(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        TreeSet<Float> availableDepths = new TreeSet<>();
        availableDepths.addAll(ppDatatypeManager.getAvailableDepths(selectedPlatforms, selectedDates, selectedVariables));
        availableDepths.addAll(chlorophylleDatatypeManager.getAvailableDepths(selectedPlatforms, selectedDates, selectedVariables));
        return availableDepths;

    }

    /**
     *
     * @param ppDatatypeManager
     */
    public void setPpDatatypeManager(IPPDatatypeManager ppDatatypeManager) {
        this.ppDatatypeManager = ppDatatypeManager;
    }

    /**
     *
     * @param chlorophylleDatatypeManager
     */
    public void setChlorophylleDatatypeManager(IChlorophylleDatatypeManager chlorophylleDatatypeManager) {
        this.chlorophylleDatatypeManager = chlorophylleDatatypeManager;
    }

    /**
     *
     * @param conditionGeneraleDatatypeManager
     */
    public void setConditionGeneraleDatatypeManager(IConditionGeneraleDatatypeManager conditionGeneraleDatatypeManager) {
        this.conditionGeneraleDatatypeManager = conditionGeneraleDatatypeManager;
    }

}

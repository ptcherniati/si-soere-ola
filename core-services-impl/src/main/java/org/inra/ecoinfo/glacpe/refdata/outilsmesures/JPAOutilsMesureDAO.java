/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.outilsmesures;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPAOutilsMesureDAO extends AbstractJPADAO<OutilsMesure> implements IOutilsMesureDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<OutilsMesure> getByCode(String code) {
        CriteriaQuery<OutilsMesure> query = builder.createQuery(OutilsMesure.class);
        Root<OutilsMesure> outilsMesure = query.from(OutilsMesure.class);
        query
                .select(outilsMesure)
                .where(builder.equal(outilsMesure.get(OutilsMesure_.code), code));
        return getOptional(query);
    }

}

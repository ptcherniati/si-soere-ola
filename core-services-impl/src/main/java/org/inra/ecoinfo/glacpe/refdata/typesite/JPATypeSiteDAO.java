/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.typesite;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATypeSiteDAO extends AbstractJPADAO<TypeSite> implements ITypeSiteDAO {

    /**
     *
     * @param code
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Optional<TypeSite> getByCode(String code) {
        CriteriaQuery<TypeSite> query = builder.createQuery(TypeSite.class);
        Root<TypeSite> typeSite = query.from(TypeSite.class);
        query
                .select(typeSite)
                .where(builder.equal(typeSite.get(TypeSite_.code), code));
        return getOptional(query);
    }

}

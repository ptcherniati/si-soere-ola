package org.inra.ecoinfo.glacpe.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.ITestFormat;
import org.inra.ecoinfo.glacpe.dataset.ITestHeaders;
import org.inra.ecoinfo.glacpe.dataset.ITestValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.getGLACPEMessage;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class GenericTestFormat implements ITestFormat {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The test headers @link(ITestHeaders). */
    private ITestHeaders testHeaders;

    /** The test values @link(ITestValues). */
    private ITestValues testValues;

    /** The datatype name @link(String). */
    private String datatypeName;

    /**
     * Instantiates a new abstract generic test format.
     */
    public GenericTestFormat() {
        super();
    }

    /**
     * Gets the datatype name.
     * 
     * @return {@link String} the datatype name
     */
    protected String getDatatypeName() {
        return datatypeName;
    }

    @Override
    public void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    /**
     * Sets the test headers.
     * 
     * @param testHeaders
     *            the new test headers
     * @see org.inra.ecoinfo.glacpe.dataset.impl.ITestFormat#setTestHeaders(org.inra.ecoinfo.glacpe.dataset.ITestHeaders)
     */
    @Override
    public void setTestHeaders(final ITestHeaders testHeaders) {
        this.testHeaders = testHeaders;
    }

    /**
     * Sets the test values.
     * 
     * @param testValues
     *            the new test values
     * @see org.inra.ecoinfo.glacpe.dataset.impl.ITestFormat#setTestValues(org.inra.ecoinfo.glacpe.dataset.ITestValues)
     */
    @Override
    public void setTestValues(final ITestValues testValues) {
        this.testValues = testValues;
    }

    /**
     * Test format.
     * 
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(ISessionPropertiesMonSoere) the session properties
     * @param encoding
     * @link(String) the encoding
     * @param datasetDescriptor
     * @link(DatasetDescriptor) the dataset descriptor
     * @throws BadFormatException
     *             the bad format exception
     * @see org.inra.ecoinfo.glacpe.dataset.impl.ITestFormat#testFormat(com.Ostermiller.util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile, org.inra.ecoinfo.glacpe.dataset.impl.ISessionPropertiesACBB, java.lang.String,
     *      org.inra.ecoinfo.dataset.impl.DatasetDescriptor)
     */
    @Override
    public void testFormat(CSVParser parser, VersionFile versionFile, String encoding, DatasetDescriptor datasetDescriptor) throws BadFormatException {
        final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
        final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(),PROPERTY_MSG_ERROR_BAD_FORMAT));
        logger.debug(RecorderGLACPE.getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(),PROPERTY_MSG_CHECKING_FORMAT_FILE));
 
        long lineNumber = -1;
        try {
            lineNumber = testHeaders.testHeaders(parser, versionFile, encoding, badsFormatsReport, datasetDescriptor);
            testValues.testValues(lineNumber, parser, versionFile, encoding, badsFormatsReport, datasetDescriptor, getDatatypeName());
        } catch (final BusinessException e) {
            badsFormatsReport.addException(e);
            throw new BadFormatException(badsFormatsReport);
        }

        if (badsFormatsReport.hasErrors()) {
            logger.debug(badsFormatsReport.getMessages());
            throw new BadFormatException(badsFormatsReport);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.identification.jpa;

import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE;
import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE_;
import org.inra.ecoinfo.identification.IRightRequestDAO;
import org.inra.ecoinfo.identification.entity.Utilisateur;

/**
 * @author ptcherniati
 */
public class JPARightRequestDAO extends AbstractJPADAO<RightRequestGLACPE> implements IRightRequestDAO<RightRequestGLACPE> {
    /**
     * @param utilisateur
     * @return
     */
    @Override
    public List<RightRequestGLACPE> getByUser(Utilisateur utilisateur) {

        CriteriaQuery<RightRequestGLACPE> query = builder.createQuery(RightRequestGLACPE.class);
        Root<RightRequestGLACPE> rightRequest = query.from(RightRequestGLACPE.class);
        query.where(builder.equal(rightRequest.join(RightRequestGLACPE_.user), utilisateur));
        query.select(rightRequest);
        return getResultList(query);
    }

    /**
     * @return
     */
    @Override
    public List<RightRequestGLACPE> getAll() {
        return getAll(RightRequestGLACPE.class);
    }

}

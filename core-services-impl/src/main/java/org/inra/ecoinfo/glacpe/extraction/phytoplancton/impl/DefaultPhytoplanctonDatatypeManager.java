package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ValeurProprieteTaxon;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultPhytoplanctonDatatypeManager extends MO implements IPhytoplanctonDatatypeManager {

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public SortedMap<Taxon, SortedMap<String, ValeurProprieteTaxon>> getAvailableTaxons(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        Collection<Taxon> taxons = phytoplanctonDAO.getAvailableTaxons(policyManager.getCurrentUser(), selectedPlatforms, selectedDates, selectedVariables);
        SortedMap<Taxon, SortedMap<String, ValeurProprieteTaxon>> taxonsInfos = new TreeMap<>();
        if (taxons != null && !taxons.isEmpty()) {

            for (Taxon taxon : (List<Taxon>) taxons) {
                addTaxonToList(taxon, taxonsInfos);
                if(taxon.getTaxonParent()!=null && !taxonsInfos.containsKey(taxon.getTaxonParent())){
                    addTaxonToList(taxon.getTaxonParent(), taxonsInfos);
                }
            }

        }
        return taxonsInfos;
    }

    protected void addTaxonToList(Taxon taxon, SortedMap<Taxon, SortedMap<String, ValeurProprieteTaxon>> taxonsInfos) {
        SortedMap<String, ValeurProprieteTaxon> valeurs = new TreeMap<String, ValeurProprieteTaxon>();
        if (taxon.isLeaf()) {
            for (TaxonProprieteTaxon tpt : taxonDAO.getTaxonProprieteTaxonByTaxonId(taxon.getId())) {
                if (tpt.getProprieteTaxon().getCode().equals("longueur_de_la_cellule")
                        || tpt.getProprieteTaxon().getCode().equals("valeur_du_biovolume_specifique_choisi_pour_le_taxon")
                        || tpt.getProprieteTaxon().getCode().equals("classe_algale_sensu_bourrelly")) {
                    valeurs.put(tpt.getProprieteTaxon().getCode(), tpt.getValeurProprieteTaxon());
                }
            }
        }
        taxonsInfos.put(taxon, valeurs);
    }

    /**
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<Taxon> retrievePhytoplanctonRootTaxon() {
        List<Taxon> taxonsRoots = taxonDAO.getPhytoRootTaxa();
        return taxonsRoots;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }

    /**
     *
     * @param taxonId
     * @return
     */
    @Override
    public List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId) {
        return taxonDAO.getTaxonProprieteTaxonByTaxonId(taxonId);
    }

    /**
     *
     * @param taxonCode
     * @return
     */
    @Override
    public Optional<Taxon> retrieveTaxonByCode(String taxonCode) {
        return taxonDAO.getByCode(taxonCode);
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return phytoplanctonDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return phytoplanctonDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }
}

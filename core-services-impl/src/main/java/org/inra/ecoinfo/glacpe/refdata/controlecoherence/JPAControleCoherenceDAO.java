package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE_;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.DataType_;

/**
 *
 * @author ptcherniati
 */
public class JPAControleCoherenceDAO extends AbstractJPADAO<ControleCoherence> implements IControleCoherenceDAO {

    private static final String QUERY_BY_SITE_AND_DATATYPE_VARIABLE_UNITE = "from ControleCoherence where site=:site and datatypeVariableUnite=:datatypeVariableUnite";
    private static final String QUERY_BY_NULL_SITE_AND_DATATYPE_VARIABLE_UNITE = "from ControleCoherence where site is null and datatypeVariableUnite=:datatypeVariableUnite";
    private static final String QUERY_HQL_GET_BY_SITE_CODE_AND_DATATYPE_CODE = "from ControleCoherence cc where cc.site.code = :siteCode and cc.datatypeVariableUnite.datatype.code = :datatypeCode";
    private static final String QUERY_HQL_GET_BY_NULL_SITE_CODE_AND_DATATYPE_CODE = "from ControleCoherence cc where cc.site.code is null and cc.datatypeVariableUnite.datatype.code = :datatypeCode";

    /**
     *
     * @param siteCode
     * @param variableCode
     * @return
     */
    @Override
    public Optional<ControleCoherence> getBySiteCodeAndVariableCode(String siteCode, String variableCode) {
        CriteriaQuery<ControleCoherence> query = builder.createQuery(ControleCoherence.class);
        Root<ControleCoherence> controleCoherence = query.from(ControleCoherence.class);
        Join<ControleCoherence, SiteGLACPE> site = controleCoherence.join(ControleCoherence_.site);
        Join<ControleCoherence, VariableGLACPE> variable = controleCoherence
                .join(ControleCoherence_.datatypeVariableUnite)
                .join(DatatypeVariableUniteGLACPE_.VARIABLE);
        query
                .select(controleCoherence)
                .where(
                        builder.and(
                                builder.equal(site.get(SiteGLACPE_.code), siteCode),
                                builder.equal(variable.get(VariableGLACPE_.code), variableCode)
                        )
                );
        return getOptional(query);
    }

    /**
     *
     * @param site
     * @param datatypeVariableUnite
     * @return
     */
    @Override
    public Optional<ControleCoherence> getBySiteAndDatatypeVariableUnite(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite) {
        CriteriaQuery<ControleCoherence> query = builder.createQuery(ControleCoherence.class);
        Root<ControleCoherence> controleCoherence = query.from(ControleCoherence.class);
        Join<ControleCoherence, SiteGLACPE> siteDB = controleCoherence.join(ControleCoherence_.site);
        Join<ControleCoherence, DatatypeVariableUniteGLACPE> dvuDB = controleCoherence
                .join(ControleCoherence_.datatypeVariableUnite);
        query
                .select(controleCoherence)
                .where(
                        builder.and(
                                builder.equal(siteDB, site),
                                builder.equal(dvuDB, datatypeVariableUnite)
                        )
                );
        return getOptional(query);
    }

    /**
     *
     * @param datatypeVariableUnite
     * @return
     */
    @Override
    public Optional<ControleCoherence> getByNullSiteAndDatatypeVariableUnite(DatatypeVariableUniteGLACPE datatypeVariableUnite) {
        try {
            CriteriaQuery<ControleCoherence> query = builder.createQuery(ControleCoherence.class);
            Root<ControleCoherence> controleCoherence = query.from(ControleCoherence.class);
            Join<ControleCoherence, SiteGLACPE> siteDB = controleCoherence.join(ControleCoherence_.site);
            Join<ControleCoherence, DatatypeVariableUniteGLACPE> dvuDB = controleCoherence
                    .join(ControleCoherence_.datatypeVariableUnite);
            query
                    .select(controleCoherence)
                    .where(
                            builder.and(
                                    builder.isNull(siteDB),
                                    builder.equal(dvuDB, datatypeVariableUnite)
                            )
                    );
            return getOptional(query);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param siteCode
     * @param datatypeCode
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String siteCode, String datatypeCode) {
        CriteriaQuery<ControleCoherence> query = builder.createQuery(ControleCoherence.class);
        Root<ControleCoherence> controleCoherence = query.from(ControleCoherence.class);
        Join<ControleCoherence, SiteGLACPE> site = controleCoherence.join(ControleCoherence_.site);
        Join<DatatypeVariableUniteGLACPE, DataType> datatype = controleCoherence
                .join(ControleCoherence_.datatypeVariableUnite)
                .join(DatatypeVariableUniteGLACPE_.datatype);
        query
                .select(controleCoherence)
                .where(
                        builder.and(
                                builder.equal(site.get(SiteGLACPE_.code), siteCode),
                                builder.equal(datatype.get(DataType_.code), datatypeCode)
                        )
                );
        return getResultAsStream(query)
                .collect(Collectors.toMap(cc->cc.getDatatypeVariableUnite().getVariable().getCode(), cc->cc));
    }

    /**
     *
     * @param datatypeCode
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, ControleCoherence> getControleCoherenceBySitecodeAndDatatypecode(String datatypeCode) {
       //from ControleCoherence cc where cc.site.code is null and cc.datatypeVariableUnite.datatype.code = :datatypeCode
        
        CriteriaQuery<ControleCoherence> query = builder.createQuery(ControleCoherence.class);
        Root<ControleCoherence> controleCoherence = query.from(ControleCoherence.class);
        Join<ControleCoherence, SiteGLACPE> site = controleCoherence.join(ControleCoherence_.site);
        Join<DatatypeVariableUniteGLACPE, DataType> datatype = controleCoherence
                .join(ControleCoherence_.datatypeVariableUnite)
                .join(DatatypeVariableUniteGLACPE_.datatype);
        query
                .select(controleCoherence)
                .where(
                        builder.and(
                                builder.isNull(site),
                                builder.equal(datatype.get(DataType_.code), datatypeCode)
                        )
                );
        return getResultAsStream(query)
                .collect(Collectors.toMap(cc->cc.getDatatypeVariableUnite().getVariable().getCode(), cc->cc));
    }

}

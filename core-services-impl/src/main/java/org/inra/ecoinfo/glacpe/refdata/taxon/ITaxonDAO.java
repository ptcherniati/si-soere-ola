/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public interface ITaxonDAO extends IDAO<Taxon> {

    /**
     *
     * @param code
     * @return
     */
    Optional<Taxon> getByCode(String code);

    /**
     *
     * @param parentTaxonId
     * @return Les Ids des taxons composants l'arborescence complète du taxon
     * d'id parentTaxonId
     */
    List<Long> getAllSubtaxaIdsAndParentTaxaId(Long parentTaxonId);

    /**
     *
     * @return
     */
    List<Taxon> getPhytoRootTaxa();

    /**
     *
     * @return
     */
    List<Taxon> getZooRootTaxa();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Taxon> getByNomLatin(String nom);

    /**
     *
     * @param taxonId
     * @return
     */
    List<TaxonProprieteTaxon> getTaxonProprieteTaxonByTaxonId(long taxonId);

    /**
     *
     * @param theme
     * @return
     */
    List<String> getTaxonNameByTheme(String theme);

    /**
     *
     * @param theme
     * @return
     */
    List<Taxon> getTaxonByTheme(String theme);

    /**
     *
     * @return
     */
    List<Taxon> getPhytoTaxonInfos();

}

/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.taxonniveau;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPATaxonNiveauDAO extends AbstractJPADAO<TaxonNiveau> implements ITaxonNiveauDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<TaxonNiveau> getByCode(String code) {
        CriteriaQuery<TaxonNiveau> query = builder.createQuery(TaxonNiveau.class);
        Root<TaxonNiveau> taxonNiveau = query.from(TaxonNiveau.class);
        query
                .select(taxonNiveau)
                .where(builder.equal(taxonNiveau.get(TaxonNiveau_.code), code));
        return getOptional(query);
    }

}

package org.inra.ecoinfo.glacpe.dataset.phytoplancton.impl;

import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;

/**
 *
 * @author ptcherniati
 */
public class LineRecord {

    private String nomSite;
    private String plateformeCode;
    private LocalDate datePrelevement;
    private Float profondeurMin;
    private Float profondeurMax;
    private String outilsMesureCode;
    private String outilsPrelevementCode;
    private String determinateur;
    private Float surfaceComptage;
    private Float volumeSedimente;
    private String taxonCode;
    private Long originalLineNumber;
    private String projetCode;
    private List<VariableValue> variablesValues;

    /**
     *
     */
    public LineRecord() {

    }

    /**
     *
     * @param nomSite
     * @param plateformeCode
     * @param datePrelevement
     * @param profondeurMin
     * @param profondeurMax
     * @param outilsMesureCode
     * @param outilsPrelevementCode
     * @param determinateur
     * @param volumeSedimente
     * @param surfaceComptage
     * @param taxonCode
     * @param variablesValues
     * @param l
     * @param originalLineNumber
     * @param string6
     * @param projetCode
     */
    public LineRecord(String nomSite, String plateformeCode, LocalDate datePrelevement, Float profondeurMin, Float profondeurMax, String outilsMesureCode, String outilsPrelevementCode, String determinateur, Float volumeSedimente, Float surfaceComptage,
        String taxonCode, List<VariableValue> variablesValues, Long originalLineNumber, String projetCode)
    {
        super();
        this.nomSite = nomSite;
        this.plateformeCode = plateformeCode;
        this.datePrelevement = datePrelevement;
        this.outilsMesureCode = outilsMesureCode;
        this.outilsPrelevementCode = outilsPrelevementCode;
        this.determinateur = determinateur;
        this.volumeSedimente = volumeSedimente;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.surfaceComptage = surfaceComptage;
        this.taxonCode = taxonCode;
        this.variablesValues = variablesValues;
        this.originalLineNumber = originalLineNumber;
        this.projetCode = projetCode;
    }

    /**
     *
     * @param line
     */
    public void copy(LineRecord line) {
        this.nomSite = line.getNomSite();
        this.plateformeCode = line.getPlateformeCode();
        this.datePrelevement = line.getDatePrelevement();
        this.profondeurMin = line.getProfondeurMin();
        this.profondeurMax = line.getProfondeurMax();
        this.outilsMesureCode = line.getOutilsMesureCode();
        this.outilsPrelevementCode = line.getOutilsPrelevementCode();
        this.determinateur = line.getDeterminateur();
        this.volumeSedimente = line.getVolumeSedimente();
        this.surfaceComptage = line.getSurfaceComptage();
        this.variablesValues = line.getVariablesValues();
        this.taxonCode = line.getTaxonCode();

        this.originalLineNumber = line.getOriginalLineNumber();
        this.projetCode = getProjetCode();
    }

    /**
     *
     * @return
     */
    public String getNomSite() {
        return nomSite;
    }

    /**
     *
     * @return
     */
    public String getPlateformeCode() {
        return plateformeCode;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @param variablesValues
     */
    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @return
     */
    public String getProjetCode() {
        return projetCode;
    }

    /**
     *
     * @return
     */
    public String getOutilsMesureCode() {
        return outilsMesureCode;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @param profondeurMin
     */
    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @param profondeurMax
     */
    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    /**
     *
     * @return
     */
    public String getDeterminateur() {
        return determinateur;
    }

    /**
     *
     * @param determinateur
     */
    public void setDeterminateur(String determinateur) {
        this.determinateur = determinateur;
    }

    /**
     *
     * @return
     */
    public Float getSurfaceComptage() {
        return surfaceComptage;
    }

    /**
     *
     * @param surfaceComptage
     */
    public void setSurfaceComptage(Float surfaceComptage) {
        this.surfaceComptage = surfaceComptage;
    }

    /**
     *
     * @return
     */
    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    /**
     *
     * @param volumeSedimente
     */
    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    /**
     *
     * @return
     */
    public String getTaxonCode() {
        return taxonCode;
    }

    /**
     *
     * @param taxonCode
     */
    public void setTaxonCode(String taxonCode) {
        this.taxonCode = taxonCode;
    }

    /**
     *
     * @return
     */
    public String getOutilsPrelevementCode() {
        return outilsPrelevementCode;
    }

    /**
     *
     * @param outilsPrelevementCode
     */
    public void setOutilsPrelevementCode(String outilsPrelevementCode) {
        this.outilsPrelevementCode = outilsPrelevementCode;
    }
}

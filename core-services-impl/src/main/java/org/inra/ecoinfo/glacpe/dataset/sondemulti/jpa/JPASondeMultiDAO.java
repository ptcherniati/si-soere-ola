package org.inra.ecoinfo.glacpe.dataset.sondemulti.jpa;

import com.google.common.base.Strings;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti_;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti_;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti_;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti_;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE_;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.synthesis.sondemulti.SynthesisValue;
import org.inra.ecoinfo.glacpe.synthesis.sondemulti.SynthesisValue_;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.mga.business.composite.groups.ICompositeGroup;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class JPASondeMultiDAO extends AbstractJPADAO<Object> implements ISondeMultiDAO {

    private static final String ALIAS_PLATFORM = "platform";
    private static final String ALIAS_PROJECT = "projet";
    private static final String ALIAS_SITE = "site";
    private static final String ALIAS_VARIABLE = "variable";
    private static final String ALIAS_DEPTH = "depth";

    /**
     *
     * @param user
     * @param platformsMap
     * @param intervalDates
     * @param depths
     * @param dvus
     * @return
     */
    @Override
    public List<ValeurMesureSondeMulti> extractValueDatas(IUser user, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> platformsMap, List<IntervalDate> intervalDates, DepthRequestParamVO depths, List<DatatypeVariableUniteGLACPE> dvus) {
        List<Predicate> and = new LinkedList<>();
        CriteriaQuery<ValeurMesureSondeMulti> query = builder.createQuery(ValeurMesureSondeMulti.class);
        Root<ValeurMesureSondeMulti> v = query.from(ValeurMesureSondeMulti.class);
        Path<RealNode> get = v.get(ValeurMesureSondeMulti_.realNode);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rns = rnv.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> siteNodeable = rns.join(RealNode_.nodeable);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> projetNodeable = rnp.join(RealNode_.nodeable);
        Join<ValeurMesureSondeMulti, MesureSondeMulti> m = v.join(ValeurMesureSondeMulti_.mesure);
        Join<MesureSondeMulti, SousSequenceSondeMulti> ss = m.join(MesureSondeMulti_.sousSequence);
        Join<SousSequenceSondeMulti, SequenceSondeMulti> s = ss.join(SousSequenceSondeMulti_.sequence);
        Path<LocalDate> dateMesure = s.get(SequenceSondeMulti_.datePrelevement);
        and.add(builder.equal(rnv, v.get(ValeurMesureSondeMulti_.realNode)));
        if (!depths.getAllDepth()) {
            and.add(builder.ge(m.get(MesureSondeMulti_.profondeur), depths.getDepthMin()));
            and.add(builder.le(m.get(MesureSondeMulti_.profondeur), depths.getDepthMax()));
        }
        if (!dvus.isEmpty()) {
            and.add(rnv.get(RealNode_.nodeable).in(dvus));
        }
        List<Predicate> orPlateforme = new LinkedList();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> ProjetEntry : platformsMap.entrySet()) {
            Projet projet = ProjetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> platformsBySites = ProjetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : platformsBySites.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = (SortedSet<Plateforme>) siteEntry.getValue();
                for (Plateforme platform : platforms) {
                    orPlateforme.add(
                            builder.and(
                                    builder.equal(rns.get(RealNode_.nodeable), site),
                                    builder.equal(rnp.get(RealNode_.nodeable), projet),
                                    builder.equal(ss.get(SousSequenceSondeMulti_.plateforme), platform)
                            ));
                }
            }
        }
        if (!orPlateforme.isEmpty()) {
            and.add(builder.or(orPlateforme.stream().toArray(Predicate[]::new)));
        }
        intervalDates.stream()
                .map(interval -> builder.between(dateMesure, interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        l -> and.add(builder.or(l.stream().toArray(Predicate[]::new)))
                ));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, dateMesure);
        query
                .select(v)
                .distinct(true)
                //                .orderBy(
                //                        builder.asc(projetNodeable.get(Nodeable_.code)),
                //                        builder.asc(siteNodeable.get(Nodeable_.code)),
                //                        builder.asc(ss.get(SousSequenceSondeMulti_.plateforme)),
                //                        builder.asc(dateMesure),
                //                        builder.asc(m.get(MesureSondeMulti_.profondeurMin)),
                //                        builder.asc(m.get(MesureSondeMulti_.profondeurMax))
                //                )
                .where(and.stream().toArray(Predicate[]::new));
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param platformsMap
     * @param intervalDates
     * @param depths
     * @param dvus
     * @return
     */
    @Override
    public List<MesureSondeMulti> extractDatas(IUser user, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> platformsMap, List<IntervalDate> intervalDates, DepthRequestParamVO depths, List<DatatypeVariableUniteGLACPE> dvus) {
        List<Predicate> and = new LinkedList<>();
        CriteriaQuery<MesureSondeMulti> query = builder.createQuery(MesureSondeMulti.class);
        Root<ValeurMesureSondeMulti> v = query.from(ValeurMesureSondeMulti.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rns = rnv.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> siteNodeable = rns.join(RealNode_.nodeable);
        Join<RealNode, RealNode> rnp = rns.join(RealNode_.parent).join(RealNode_.parent);
        Join<RealNode, Nodeable> projetNodeable = rnp.join(RealNode_.nodeable);
        Join<ValeurMesureSondeMulti, MesureSondeMulti> m = v.join(ValeurMesureSondeMulti_.mesure);
        m.fetch(MesureSondeMulti_.valeurs);
        Join<MesureSondeMulti, SousSequenceSondeMulti> ss = m.join(MesureSondeMulti_.sousSequence);
        Join<SousSequenceSondeMulti, SequenceSondeMulti> s = ss.join(SousSequenceSondeMulti_.sequence);
        Path<LocalDate> dateMesure = s.get(SequenceSondeMulti_.datePrelevement);
        and.add(builder.equal(rnv, v.get(ValeurMesureSondeMulti_.realNode)));
        if (!depths.getAllDepth()) {
            and.add(builder.ge(m.get(MesureSondeMulti_.profondeur), depths.getDepthMin()));
            and.add(builder.le(m.get(MesureSondeMulti_.profondeur), depths.getDepthMax()));
        }
        if (!dvus.isEmpty()) {
            and.add(rnv.get(RealNode_.nodeable).in(dvus));
            /*            dvus.stream()
                    .map(dvu->dvu.getId().toString())
                    .sorted()
                    .collect(Collectors.joining(",","(",")"))*/
        }
        List<Predicate> orPlateforme = new LinkedList();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> ProjetEntry : platformsMap.entrySet()) {
            Projet projet = ProjetEntry.getKey();
            SortedMap<SiteGLACPE, SortedSet<Plateforme>> platformsBySites = ProjetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedSet<Plateforme>> siteEntry : platformsBySites.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedSet<Plateforme> platforms = (SortedSet<Plateforme>) siteEntry.getValue();
                for (Plateforme platform : platforms) {
                    orPlateforme.add(
                            builder.and(
                                    builder.equal(rns.get(RealNode_.nodeable), site),
                                    builder.equal(rnp.get(RealNode_.nodeable), projet),
                                    builder.equal(ss.get(SousSequenceSondeMulti_.plateforme), platform)
                            ));
                }
            }
        }
        if (!orPlateforme.isEmpty()) {
            and.add(builder.or(orPlateforme.stream().toArray(Predicate[]::new)));
        }
        intervalDates.stream()
                .map(interval -> builder.between(dateMesure, interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()))
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        l -> and.add(builder.or(l.stream().toArray(Predicate[]::new)))
                ));
        //addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, dateMesure);
        query
                .select(m)
                .distinct(true)
                //                .orderBy(
                //                        builder.asc(projetNodeable.get(Nodeable_.code)),
                //                        builder.asc(siteNodeable.get(Nodeable_.code)),
                //                        builder.asc(ss.get(SousSequenceSondeMulti_.plateforme)),
                //                        builder.asc(dateMesure),
                //                        builder.asc(m.get(MesureSondeMulti_.profondeurMin)),
                //                        builder.asc(m.get(MesureSondeMulti_.profondeurMax))
                //                )
                .where(and.stream().toArray(Predicate[]::new));
        return getResultList(query);
    }

    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            List<String> groups = getGroups(user);
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            List<Predicate> orLogin = new LinkedList<>();
            predicatesAnd.add(er.get(ExtractActivity_.login).in(groups));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

    /**
     *
     * @param user
     * @return
     */
    public List<String> getGroups(IUser user) {
        List<String> l = new LinkedList();
        if (user instanceof ICompositeGroup) {
            l = user.getAllGroups().stream()
                    .filter(g -> g.getWhichTree().equals(WhichTree.TREEDATASET))
                    .map(g -> g.getGroupName())
                    .collect(Collectors.toList());
        } else {
            l.add(user.getLogin());
        }
        return l;
    }

    /**
     *
     * @param user
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms(IUser user) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        ArrayList<Predicate> and = new ArrayList<>();
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<SynthesisValue, Projet> projet = sv.join(SynthesisValue_.projetId);
        Join<SynthesisValue, Plateforme> plateforme = sv.join(SynthesisValue_.plateformeId);
        Join<SynthesisValue, SiteGLACPE> site = sv.join(SynthesisValue_.siteId);
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, sv.get(SynthesisValue_.date));
        query
                .multiselect(projet.alias(ALIAS_PROJECT), site.alias(ALIAS_SITE), plateforme.alias(ALIAS_PLATFORM))
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultListToStream(query)
                .map(tuple -> getRealNodePlatformFromTuple(tuple))
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private PlateformeProjetVO getRealNodePlatformFromTuple(Tuple tuple) {
        Plateforme platform = tuple.get(ALIAS_PLATFORM, Plateforme.class);
        SiteGLACPE site = tuple.get(ALIAS_SITE, SiteGLACPE.class);
        Projet projet = tuple.get(ALIAS_PROJECT, Projet.class);
        return new PlateformeProjetVO(platform, projet, site);
    }

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public List<DatatypeVariableUniteGLACPE> getAvailableVariables(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        CriteriaQuery<DatatypeVariableUniteGLACPE> query = builder.createQuery(DatatypeVariableUniteGLACPE.class);
        ArrayList<Predicate> and = new ArrayList<>();
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Path<RealNode> rnv = ndsv.get(NodeDataSet_.realNode);
        Root<DatatypeVariableUniteGLACPE> dvu = query.from(DatatypeVariableUniteGLACPE.class);
        Join<SynthesisValue, Projet> projet = sv.join(SynthesisValue_.projetId);
        Join<SynthesisValue, Plateforme> plateforme = sv.join(SynthesisValue_.plateformeId);
        and.add(builder.equal(ndsv.get(NodeDataSet_.id), sv.get(GenericSynthesisValue_.idNode)));
        and.add(builder.equal(rnv.get(RealNode_.nodeable), dvu.get(DatatypeVariableUniteGLACPE_.id)));
        if (!selectedPlatforms.isEmpty()) {
            if (selectedPlatforms.size() == 1) {
                and.add(builder.equal(projet, selectedPlatforms.get(0).getProjet()));
                and.add(builder.equal(plateforme, selectedPlatforms.get(0).getPlateforme()));
            } else {
                ArrayList<Predicate> or = new ArrayList<>();
                selectedPlatforms.stream()
                        .forEach(pp -> or.add(builder.and(
                        builder.equal(projet, pp.getProjet()),
                        builder.equal(plateforme, pp.getPlateforme())
                )));
                and.add(builder.or(or.toArray(new Predicate[and.size()])));
            }
        }
        Path<LocalDateTime> date = sv.get(SynthesisValue_.date);
        if (!selectedDates.isEmpty()) {
            selectedDates.stream()
                    .forEach(intervalDate -> and.add(builder.between(date, intervalDate.getBeginDate(), intervalDate.getEndDate())));
        }
        addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, date);
        query
                .select(dvu)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    /**
     *
     * @param user
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Float> getAvailableDepths(IUser user, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        String requestDepths = "select distinct unnest(profondeurs::::REAL[]) profondeur  from sondemultisynthesisvalue\n"
                + "join site_glacpe_sit site on site.id = siteid\n"
                + "join projet_pro projet on projet.pro_id = projetid\n"
                + "join plateforme_pla plateforme on plateforme.loc_id = plateformeid\n"
                + "join variable_glacpe_varg variable on variable.var_id= variableid\n"
                + "%s\n"
                + "where %s\n"
                + "order by profondeur";
        /*(projetid,plateformeid)in (%s))\n"
        + "and date BETWEEN %s and %s\n"
        + "and variableid in (%s)*/
        String plateformeWhere = CollectionUtils.isEmpty(selectedPlatforms) ? "" : selectedPlatforms.stream()
                .map(sp -> String.format("(%s,%s)", sp.getProjet().getId(), sp.getPlateforme().getId()))
                .collect(Collectors.joining(",", " (projetid,plateformeid)in (", ") "));
        String variableWhere = CollectionUtils.isEmpty(selectedDates) ? "" : selectedDates.stream()
                .map(interval -> String.format(" date BETWEEN '%s' and '%s' ", DateUtil.getUTCDateTextFromLocalDateTime(interval.getBeginDate(), DateUtil.YYYY_MM_DD_FILE), DateUtil.getUTCDateTextFromLocalDateTime(interval.getEndDate(), DateUtil.YYYY_MM_DD_FILE)))
                .collect(Collectors.joining("or", "( ", ") "));
        String depthsWhere = CollectionUtils.isEmpty(selectedVariables) ? "" : selectedVariables.stream()
                .map(dvu -> dvu.getVariable().getId().toString())
                .collect(Collectors.joining(",", " variableid in ( ", ") "));
        String restrictiveSql = "", joinRights = "";
        if (!user.getIsRoot()) {
            joinRights = "join compositeactivityextraction using(idnode)";
            restrictiveSql = " (compositeactivityextraction.datestart is null or compositeactivityextraction.datestart<=date)\n"
                    + "and (compositeactivityextraction.dateend is null or compositeactivityextraction.dateend>=date)";
        }
        String sqlWhere = Stream.of(new String[]{plateformeWhere, variableWhere, depthsWhere, restrictiveSql})
                .filter(sql->!Strings.isNullOrEmpty(sql))
                .collect(Collectors.joining(" and "));
        List<Float> profondeurs = entityManager.createNativeQuery(String.format(requestDepths, joinRights, sqlWhere)).getResultList();
        return profondeurs;
    }
}

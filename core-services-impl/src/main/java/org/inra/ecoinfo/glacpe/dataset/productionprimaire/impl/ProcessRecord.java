package org.inra.ecoinfo.glacpe.dataset.productionprimaire.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.ISequencePPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.MesurePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SousSequencePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.ValeurMesurePP;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    private static final long serialVersionUID = 1L;
    private static final String VARIABLE_PROFONDEUR_MESUREE = "profondeur_mesuree";

    /**
     *
     */
    protected ISequencePPDAO sequencePPDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected ISiteDAO siteDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequencePPDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        String datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datatypeCode, datasetDescriptor)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();
        try {
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();

            String[] values = null;
            long lineCount = 1;
            // Saute la 1ere ligne
            parser.getLine();
            SortedMap<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> sequencesMapLines = new TreeMap<>();

            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());

                if (!projetCode.equals(projetCodeFromVersion)) {
                    InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), cleanerValues.currentTokenIndex(), projetCodeFromVersion, lineCount, projetCode));
                    throw insertionDatabaseException;
                }
                String nomSite = cleanerValues.nextToken();

                if (!nomSite.equals(siteCodeFromVersion)) {
                    InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, cleanerValues.currentTokenIndex(), lineCount, nomSite));
                    throw insertionDatabaseException;
                }
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                LocalDate date = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);

                //Traitement de la profondeur
                Float profondeur = readDepth(cleanerValues, datatypeCode, errorsReport, controlesCoherenceMap, lineCount, 5);
                LocalTime debutIncubation = readTime(errorsReport, lineCount, 6, cleanerValues, datasetDescriptor);
                LocalTime finIncubationString = readTime(errorsReport, lineCount, 7, cleanerValues, datasetDescriptor);
                LocalTime dureeIncubation = readTime(errorsReport, lineCount, 8, cleanerValues, datasetDescriptor);

                // On crée une liste de Variables + Sa valeur - ici
                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        RealNode realNode = Optional
                                .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                                .map(c -> Utils.createCodeFromString(c.getName()))
                                .map(n -> realNodes.get(n))
                                .map(v -> v.get(0))
                                .orElse(null);
                        if (realNode != null) {

                            VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                            if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                            }
                            variablesValues.add(new VariableValue(realNode, values[actualVariableArray].replaceAll(" ", "")));
                        } else {
                            errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)));

                        }
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(projetCode, nomSite, plateformeCode, date, profondeur, debutIncubation, dureeIncubation, dureeIncubation, variablesValues, lineCount);
                sequencesMapLines
                        .computeIfAbsent(date, k -> new TreeMap<>())
                        .computeIfAbsent(plateformeCode, k -> new TreeMap<>())
                        .computeIfAbsent(profondeur, k -> new LinkedList<>())
                        .add(line);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
            Allocator allocator = Allocator.getInstance();

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {

                final Entry<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> entry = iterator.next();
                SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines = entry.getValue();
                LocalDate datePrelevement = entry.getKey();
                if (!sequenceLines.isEmpty()) {
                    try {
                        buildSequence(datePrelevement, projetCodeFromVersion, siteCodeFromVersion, sequenceLines, versionFile, errorsReport);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequencePPDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }

    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, String datatypeCode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private LocalTime readTime(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String timeString;
        LocalTime time = null;
        timeString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            time = Strings.isNullOrEmpty(timeString) ? null
                    : DateUtil.readLocalTimeFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), timeString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_TIME), timeString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return time;

    }

    private Float readDepth(CleanerValues cleanerValues, String variableCode, ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, int columnsCount) {
        String profondeurString = cleanerValues.nextToken();
        Float value = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
        if (value != null) {
            try {
                value = Float.parseFloat(profondeurString);

                if (!controlesCoherenceMap.isEmpty() && controlesCoherenceMap.get(variableCode) != null) {
                    testValueCoherence(
                            value,
                            controlesCoherenceMap.get(variableCode).getValeurMin(),
                            controlesCoherenceMap.get(variableCode).getValeurMax(),
                            lineCount,
                            columnsCount);
                }
            } catch (BadExpectedValueException e) {
                errorsReport.addException(e);

            } catch (Exception e) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, columnsCount)));
            }
        }
        return value;
    }

    private void buildSequence(LocalDate datePrelevement, String projetCode, String siteCode, SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        SequencePP sequencePP = new SequencePP();
        sequencePP.setDate(datePrelevement);

        sequencePP.setVersionFile(versionFile);
        for (Entry<String, SortedMap<Float, List<LineRecord>>> entry : sequenceLines.entrySet()) {
            String platformCode = entry.getKey();
            SortedMap<Float, List<LineRecord>> sousSequenceLines = entry.getValue();
            try {
                buildSousSequence(platformCode, sousSequenceLines, sequencePP, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequencePPDAO.saveOrUpdate(sequencePP);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getUTCDateTextFromLocalDateTime(datePrelevement, DateUtil.DD_MM_YYYY));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String plateformeCode, SortedMap<Float, List<LineRecord>> sousSequenceLines, SequencePP sequencePP, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException {
        LineRecord firstLine = sousSequenceLines.values()
                .stream()
                .map(
                        l -> l
                                .stream()
                                .filter(li -> li != null)
                                .findAny()
                                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(new LineRecord());

        Plateforme plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(firstLine.getNomSite())).orElse(null);
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }
        LocalTime debutIncubation = firstLine.getDebutIncubation();
        LocalTime finIncubation = firstLine.getFinIncubation();
        LocalTime dureeIncubation = firstLine.getDureeIncubation();

        SousSequencePP sousSequencePP = new SousSequencePP();
        sousSequencePP.setPlateforme(plateforme);
        sousSequencePP.setHeureDebutIncubation(debutIncubation);
        sousSequencePP.setHeureFinIncubation(finIncubation);
        sousSequencePP.setDureeIncubation(dureeIncubation);
        sousSequencePP.setSequencePP(sequencePP);
        sequencePP.getSousSequences().add(sousSequencePP);
        for (Entry<Float, List<LineRecord>> entry : sousSequenceLines.entrySet()) {
            Float profondeur = entry.getKey();
            List<LineRecord> lineRecords = entry.getValue();
            try {
                buildMesure(lineRecords, sousSequencePP, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(List<LineRecord> profondeurLines, SousSequencePP sousSequencePP, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        MesurePP mesurePP = new MesurePP();
        LineRecord firstLine = profondeurLines.get(0);

        mesurePP.setSousSequencePP(sousSequencePP);
        sousSequencePP.getMesures().add(mesurePP);
        mesurePP.setProfondeur(firstLine.getProfondeur());
        mesurePP.setLigneFichierEchange(profondeurLines.get(0).getOriginalLineNumber());

        if (profondeurLines.size() > 1) {
            errorsReport.addException(new BusinessException((String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DOUBLON_LINE), mesurePP.getLigneFichierEchange()))));
        } else {
            for (LineRecord profondeurLine : profondeurLines) {
                for (VariableValue variableValue : profondeurLine.getVariablesValues()) {
                    ValeurMesurePP valeurMesureChloro = new ValeurMesurePP();
                    VariableGLACPE variable = (VariableGLACPE) variableDAO.merge(variableValue.getVariableGLACPE());
                    valeurMesureChloro.setRealNode(mgaRecorder.getRealNodeById(variableValue.getRealNode().getId()).orElse(null));

                    if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                        valeurMesureChloro.setValeur(null);
                    } else {

                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurMesureChloro.setValeur(value);
                    }

                    valeurMesureChloro.setMesure(mesurePP);
                    mesurePP.getValeurs().add(valeurMesureChloro);

                }
            }
        }
    }

    /**
     *
     * @param sequencePPDAO
     */
    public void setSequencePPDAO(ISequencePPDAO sequencePPDAO) {
        this.sequencePPDAO = sequencePPDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }
}

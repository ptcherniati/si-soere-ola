/**
 * OREILacs project - see LICENCE.txt for use created: 13 août 2009 14:33:43
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.chimie.ISequenceChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPASequenceChimieDAO extends AbstractJPADAO<SequenceChimie> implements ISequenceChimieDAO {

    /**
     *
     * @param datePrelevement
     * @param projetcode
     * @param projetSiteId
     * @param siteCode
     * @return
     * @throws PersistenceException
     */
    @Override
    public Optional<SequenceChimie> getByDatePrelevementProjetCodeAndSiteCode(LocalDate datePrelevement, String projetcode, String siteCode){
        CriteriaQuery<SequenceChimie> query = builder.createQuery(SequenceChimie.class);
        Root<SequenceChimie> sequence = query.from(SequenceChimie.class);
        Join<RealNode, RealNode> realNodeSite = sequence
                .join(SequenceChimie_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        Join<RealNode, Nodeable> projet = realNodeSite.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.nodeable);
        query
                .select(sequence)
                .where(
                        builder.equal(sequence.get(SequenceChimie_.datePrelevement), datePrelevement),
                        builder.equal(projet.get(Nodeable_.code), projetcode),
                        builder.equal(site.get(Nodeable_.code), siteCode)
                );
        return getOptional(query);
    }

}

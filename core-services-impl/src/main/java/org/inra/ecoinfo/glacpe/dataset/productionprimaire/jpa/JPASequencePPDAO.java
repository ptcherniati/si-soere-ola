/**
 * OREILacs project - see LICENCE.txt for use created: 13 août 2009 14:33:43
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.ISequencePPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity.SequencePP_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPASequencePPDAO extends AbstractJPADAO<SequencePP> implements ISequencePPDAO {

    /**
     *
     * @param date
     * @param projetCode
     * @param siteCode
     * @return
     */
    @Override
    public Optional<SequencePP> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate date, String projetCode, String siteCode) {
        CriteriaQuery<SequencePP> query = builder.createQuery(SequencePP.class);
        Root<SequencePP> s = query.from(SequencePP.class);
        Join<RealNode, RealNode> realNodeSite = s
                .join(SequencePP_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        Join<RealNode, Nodeable> projet = realNodeSite.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.nodeable);
        query
                .select(s)
                .where(
                        builder.equal(s.get(SequencePP_.date), date),
                        builder.equal(projet.get(Nodeable_.code), projetCode),
                        builder.equal(site.get(Nodeable_.code), siteCode)
                );
        return getOptional(query);
    }
}

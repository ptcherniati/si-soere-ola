package org.inra.ecoinfo.glacpe.dataset.zooplancton.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IMetadataZooplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.MetadataZooplancton;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.MetadataZooplancton_;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure_;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme_;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet_;

/**
 *
 * @author ptcherniati
 */
public class JPAMetadataZooplanctonDAO extends AbstractJPADAO<MetadataZooplancton> implements IMetadataZooplanctonDAO {

    /**
     *
     * @param datePrelevement
     * @param keyProjet
     * @param keyPlateforme
     * @param keyOutilsPrelevement
     * @return
     */
    @Override
    public Optional<MetadataZooplancton> getByNKey(LocalDate datePrelevement, String keyProjet, String keyPlateforme, String keyOutilsPrelevement) {
        CriteriaQuery<MetadataZooplancton> query = builder.createQuery(MetadataZooplancton.class);
        Root<MetadataZooplancton> zoo = query.from(MetadataZooplancton.class);
        Join<MetadataZooplancton, Projet> projet = zoo.join(MetadataZooplancton_.projet);
        Join<MetadataZooplancton, Plateforme> plateforme = zoo.join(MetadataZooplancton_.plateforme);
        Join<MetadataZooplancton, OutilsMesure> outil = zoo.join(MetadataZooplancton_.outilsPrelevement);
        query
                .select(zoo)
                .where(
                        builder.equal(zoo.get(MetadataZooplancton_.date), datePrelevement),
                        builder.equal(projet.get(Projet_.code), keyProjet),
                        builder.equal(plateforme.get(Plateforme_.code), keyPlateforme),
                        builder.equal(outil.get(OutilsMesure_.code), keyOutilsPrelevement)
                );
        return getOptional(query);
    }

}

package org.inra.ecoinfo.glacpe.dataset.zooplancton;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.entity.MetadataZooplancton;

/**
 *
 * @author ptcherniati
 */
public interface IMetadataZooplanctonDAO extends IDAO<MetadataZooplancton> {

    /**
     *
     * @param datePrelevement
     * @param keyProjet
     * @param keyPlateforme
     * @param keyOutilsPrelevement
     * @return
     */
    Optional<MetadataZooplancton> getByNKey(LocalDate datePrelevement, String keyProjet, String keyPlateforme, String keyOutilsPrelevement);

    @Override
    void flush();
}

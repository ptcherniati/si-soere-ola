package org.inra.ecoinfo.glacpe.extraction.chimie;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieOutputBuilderByRow extends ChimieOutputsBuildersResolver {

    private IOutputBuilder chimieBuildOutputByRow;

    /**
     *
     */
    public ChimieOutputBuilderByRow() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) throws BusinessException, NoExtractionResultException {
        super.setChimieOutputBuilder(chimieBuildOutputByRow);
        return super.buildOutput(parameters);
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getChimieBuildOutputByRow() {
        return chimieBuildOutputByRow;
    }

    /**
     *
     * @param chimieBuildOutputByRow
     */
    public void setChimieBuildOutputByRow(IOutputBuilder chimieBuildOutputByRow) {
        this.chimieBuildOutputByRow = chimieBuildOutputByRow;
    }

}

package org.inra.ecoinfo.glacpe.extraction.chimie;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieMeanDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.extraction.chimie.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "mean";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_MEAN";
    private static final String MSG_MEAN = "PROPERTY_MSG_MEAN";
    private static final String MAP_INDEX_0 = "0";

    private static final String CODE_DATATYPE_CHIMIE = "physico_chimie";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, ChimieAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, HEADER_RAW_DATA)));
        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            String uniteNom = "nounit";
            Unite unite = dvu.getUnite();
            if (unite != null) {
                uniteNom = unite.getCode();
            }

            String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getName(), dvu.getVariable().getName());
            stringBuilder.append(String.format(";\"%s(%s)%n%s\"", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_CHIMIE, MSG_MEAN), localizedVariableName, uniteNom));

        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = (DepthRequestParamVO) requestMetadatasMap.get(DepthRequestParamVO.class.getSimpleName());

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = buildVariablesMeanDatas(valeursMesures);
        for (Iterator<Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>>> iterator = variablesAggregatedDatas.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> mapBySite = projetEntry.getValue();
            for (Iterator<Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> mapByPlatform = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                projetSiteOutputStream.println(headers);
                for (Iterator<Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> iterator2 = mapByPlatform.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> platformEntry = iterator2.next();
                    Plateforme plateforme = platformEntry.getKey();
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapByDate = platformEntry.getValue();
                    String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
                    String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    for (Iterator<Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> iterator3 = mapByDate.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> dateEntry = iterator3.next();
                        LocalDate date = dateEntry.getKey();
                        SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas> mapByVariable = dateEntry.getValue();
                        Optional<VariableAggregatedDatas> variableAggregatedDatas = Optional.empty();
                        String localizedOutilPrelevementName = "";
                        String localizedOutilMesureName = "";
                        Optional<DatatypeVariableUniteGLACPE> dvuOpt = Optional.empty();
                        PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));

                        rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                        localizedOutilPrelevementName = mapByVariable.values().stream()
                                .map(v -> v.getOutilsPrelevement())
                                .filter(v -> v != null)
                                .map(o -> propertiesOutilName.getProperty(o.getNom(), o.getNom()))
                                .findAny()
                                .orElse("");
                        localizedOutilMesureName = mapByVariable.values().stream()
                                .map(v -> v.getOutilsMesure())
                                .filter(v -> v != null)
                                .map(o -> propertiesOutilName.getProperty(o.getNom(), o.getNom()))
                                .findAny()
                                .orElse("");

                        rawDataPrintStream.print(String.format("%s;%s;%s;%s;%s", DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY), localizedOutilPrelevementName, localizedOutilMesureName, depthMin, depthMax));

                        for (DatatypeVariableUniteGLACPE dvu : dvus) {
                            variableAggregatedDatas = Optional.ofNullable(mapByVariable.get(dvu));
                            dvuOpt = Optional.ofNullable(dvu);
                            if (!variableAggregatedDatas.isPresent()) {
                                rawDataPrintStream.print(";");
                            } else if (dvu.getVariable().getName().equalsIgnoreCase("pH")) {
                                rawDataPrintStream.print(String.format(";%s", "NC"));
                            } else {
                                rawDataPrintStream.print(String.format(";%s", variableAggregatedDatas.get().getAverageAggregatedData().getValuesByDatesMap().get(date)));
                            }
                        }
                        rawDataPrintStream.println();
                        iterator3.remove();
                    }
                    iterator2.remove();
                }
                iterator1.remove();
            }
            iterator.remove();
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> buildVariablesMeanDatas(List<IGLACPEAggregateData> valeursMesures) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedByLocalDatesVariablesAndLocalDates = buildValeursMesuresReorderedByLocalDatesAndVariables(valeursMesures);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = new TreeMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>>();

        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>>> projetEntry : valeursMesuresReorderedByLocalDatesVariablesAndLocalDates.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>> mapBySite = projetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>> plateformeEntry : mapBySite.entrySet()) {
                SiteGLACPE site = plateformeEntry.getKey();
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>> mapByplatform = plateformeEntry.getValue();
                for (Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>> platformEntry : mapByplatform.entrySet()) {
                    Plateforme plateform = platformEntry.getKey();
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>> mapByDate = platformEntry.getValue();
                    for (Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>> dateEntry : mapByDate.entrySet()) {
                        LocalDate date = dateEntry.getKey();
                        SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>> value = dateEntry.getValue();
                        for (Map.Entry<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>> variableEntry : value.entrySet()) {
                            DatatypeVariableUniteGLACPE dvu = variableEntry.getKey();
                            SortedSet<IGLACPEAggregateData> aggregateDatas = variableEntry.getValue();
                            VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getName(), plateform.getName(), dvu.getVariable().getName(), dvu.getVariable().getOrdreAffichageGroupe());
                            fillVariableAggregatedDatas(variableAggregatedDatas, date, aggregateDatas);
                            variablesAggregatedDatas
                                    .computeIfAbsent(projet, k -> new TreeMap<>())
                                    .computeIfAbsent(site, k -> new TreeMap<>())
                                    .computeIfAbsent(plateform, k -> new TreeMap<>())
                                    .computeIfAbsent(date, k -> new TreeMap<>())
                                    .put(dvu, variableAggregatedDatas);

                        }
                    }
                }
            }
        }

        return variablesAggregatedDatas;
    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureLocalDates
     */
    protected void fillVariableAggregatedDatas(VariableAggregatedDatas variableAggregatedDatas, LocalDate dateKey, SortedSet<IGLACPEAggregateData> valeursMesureLocalDates) {

        Float m = 0f;
        Float intermediateAverage = 0f;
        Float meanDepth = (valeursMesureLocalDates.last().getDepth() - valeursMesureLocalDates.first().getDepth());

        if (valeursMesureLocalDates.first().getValue() != null) {
            variableAggregatedDatas.setOutilsPrelevement(valeursMesureLocalDates.first().getOutilsPrelevement());

            if (valeursMesureLocalDates.size() == 1) {
                if (valeursMesureLocalDates.first().getValue() != null) {
                    intermediateAverage = valeursMesureLocalDates.first().getValue();
                } else {
                    intermediateAverage = null;
                }
            } else {
                Iterator<IGLACPEAggregateData> iterator = valeursMesureLocalDates.iterator();
                IGLACPEAggregateData valeurMesure = iterator.next();
                Float value = 0.0f;
                Float depth = 0.0f;
                while (iterator.hasNext()) {
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                    if (valeurMesure.getValue() != null) {
                        value = valeurMesure.getValue();
                        depth = valeurMesure.getDepth();
                    }
                    valeurMesure = iterator.next();

                    if (value != null && valeurMesure.getValue() != null) {
                        Float indexValue = valeurMesure.getValue();
                        Float indexDepth = valeurMesure.getDepth();
                        m += ((value + indexValue) * (indexDepth - depth)) / 2;
                    }
                }

                intermediateAverage = m / meanDepth;
            }
            variableAggregatedDatas.getAverageAggregatedData().getValuesByDatesMap().put(dateKey, intermediateAverage);
        }
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>>> buildValeursMesuresReorderedByLocalDatesAndVariables(List<IGLACPEAggregateData> valeursMesures) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedBySitesPlateformesVariablesLocalDates = new TreeMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>>>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {
            valeursMesuresReorderedBySitesPlateformesVariablesLocalDates
                    .computeIfAbsent(valeurMesure.getProjet(), k -> new TreeMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>>())
                    .computeIfAbsent(valeurMesure.getSite(), k -> new TreeMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>>())
                    .computeIfAbsent(valeurMesure.getPlateforme(), k -> new TreeMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>>())
                    .computeIfAbsent(valeurMesure.getDate(), k -> new TreeMap<DatatypeVariableUniteGLACPE, SortedSet<IGLACPEAggregateData>>())
                    .computeIfAbsent((DatatypeVariableUniteGLACPE) valeurMesure.getRealNode().getNodeable(), k -> new TreeSet<IGLACPEAggregateData>())
                    .add(valeurMesure);
        }
        return valeursMesuresReorderedBySitesPlateformesVariablesLocalDates;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

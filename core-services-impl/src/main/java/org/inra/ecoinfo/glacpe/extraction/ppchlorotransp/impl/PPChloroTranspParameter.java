package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 *
 * @author ptcherniati
 */
public class PPChloroTranspParameter extends DefaultParameter implements IParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_PP_CHLORO_TRANSP = "production_primaire_chlorophylle_et_transparence";

    /**
     *
     */
    public static final String CHLOROPHYLLE = "chlorophylle";

    /**
     *
     */
    public static final String PRODUCTION_PRIMAIRE = "production_primaire";

    /**
     *
     */
    public static final String TRANSPARENCE = "transparence";

    /**
     *
     * @param map
     */
    public PPChloroTranspParameter(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_PP_CHLORO_TRANSP;
    }
}

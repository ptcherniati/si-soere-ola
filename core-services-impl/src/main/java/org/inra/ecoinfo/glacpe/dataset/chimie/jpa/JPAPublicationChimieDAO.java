package org.inra.ecoinfo.glacpe.dataset.chimie.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationChimieDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_CHIMIE_BY_IDS = "delete from SequenceChimie sce where sce.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_CHIMIE_BY_IDS = "delete from SousSequenceChimie ssce where schimie_id in (select id from SequenceChimie sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_CHIMIE_BY_IDS = "delete from MesureChimie mce where sschimie_id in (select id from SousSequenceChimie ssce where schimie_id in (select id from SequenceChimie sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_CHIMIE_BY_IDS = "delete from ValeurMesureChimie vmce where mchimie_id in (select id from MesureChimie mce where sschimie_id in (select id from SousSequenceChimie ssce where schimie_id in (select id from SequenceChimie sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_CHIMIE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_CHIMIE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_CHIMIE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_CHIMIE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage(), e);
            throw new PersistenceException(e);
        }
    }

}

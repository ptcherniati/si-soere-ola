/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author afiocca
 */
public class SondeMultiOutputBuilderSandre extends AbstractSondeMultiRawsDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.sondemulti.messages";
    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER_SANDRE";
    private static final String FILENAME_INFO_SUP = "Informations_supplementaires";
    private static final String CODE_DATATYPE_SONDE_MULTI = "sonde_multiparametres";

    protected IPlateformeDAO plateformeDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        Properties propertiesNomProjet = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesNomPlateforme = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesNomSite = getLocalizationManager().newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        try {

            Iterator<MesureSondeMulti> mesuresSondeMulti = resultsDatasMap.get(MAP_INDEX_0).iterator();
            DataType datatype = datatypeDAO.getByCode(CODE_DATATYPE_SONDE_MULTI).orElseThrow(() -> new PersistenceException("can't find datatype"));

            Map<String, String> variablesMapSandreElement = new HashMap<>();
            Map<String, String> variablesMapSandreContext = new HashMap<>();

            Map<String, String> sitesMapSandreElement = new HashMap<>();
            Map<String, String> sitesMapSandreContext = new HashMap<>();

            Map<String, String> platformsMapSite = new HashMap<>();
            Map<String, String> platformsMapSandreElement = new HashMap<>();
            Map<String, String> platformsMapSandreContext = new HashMap<>();
            Map<String, Float> platformsMapLongitude = new HashMap<>();
            Map<String, Float> platformsMapLatitude = new HashMap<>();
            Map<String, Float> platformsMapAltitude = new HashMap<>();

            Map<String, File> reminderMap = new HashMap<String, File>();
            File reminderFile = buildOutputFile(FILENAME_INFO_SUP, EXTENSION_CSV);
            PrintStream printStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER));

            while (mesuresSondeMulti.hasNext()) {
                final MesureSondeMulti mesureSondeMulti = mesuresSondeMulti.next();

                Iterator<ValeurMesureSondeMulti> valeursMesureSondeMulti = mesureSondeMulti.getValeurs().iterator();
                while (valeursMesureSondeMulti.hasNext()) {
                    ValeurMesureSondeMulti valeurMesureSondeMulti = valeursMesureSondeMulti.next();
                    final DatatypeVariableUniteGLACPE dvu = (DatatypeVariableUniteGLACPE) valeurMesureSondeMulti.getRealNode().getNodeable();
                    VariableGLACPE variable = dvu.getVariable();
                    final String nameVariable = variable.getCode();
                    final String codeSandreElementVariable = variable.getCodeSandre();
                    final String codeSandreContextVariable = variable.getContexte();

                    final String nameSite = propertiesNomSite.getProperty(valeurMesureSondeMulti.getSite().getCode(), valeurMesureSondeMulti.getSite().getCode());
                    final String codeSandreElementSite = ((SiteGLACPE) valeurMesureSondeMulti.getSite()).getCodeSandrePe();
                    final String codeSandreContextSite = ((SiteGLACPE) valeurMesureSondeMulti.getSite()).getCodeSandreMe();

                    final String namePlatform = propertiesNomPlateforme.getProperty(valeurMesureSondeMulti.getPlateforme().getCode(), valeurMesureSondeMulti.getPlateforme().getCode());
                    final String codeSandreElementPlatform = valeurMesureSondeMulti.getPlateforme().getCodeSandre();
                    final String codeSandreContextPlatform = valeurMesureSondeMulti.getPlateforme().getContexte();
                    final Float longitudePlatform = valeurMesureSondeMulti.getPlateforme().getLongitude();
                    final Float latitudePlatform = valeurMesureSondeMulti.getPlateforme().getLatitude();
                    final Float altitudePlatform = valeurMesureSondeMulti.getPlateforme().getAltitude();

                    variablesMapSandreElement.put(nameVariable, codeSandreElementVariable);
                    variablesMapSandreContext.put(nameVariable, codeSandreContextVariable);

                    sitesMapSandreElement.put(nameSite, codeSandreElementSite);
                    sitesMapSandreContext.put(nameSite, codeSandreContextSite);

                    platformsMapSite.put(namePlatform, nameSite);
                    platformsMapSandreElement.put(namePlatform, codeSandreElementPlatform);
                    platformsMapSandreContext.put(namePlatform, codeSandreContextPlatform);
                    platformsMapLongitude.put(namePlatform, longitudePlatform);
                    platformsMapLatitude.put(namePlatform, latitudePlatform);
                    platformsMapAltitude.put(namePlatform, altitudePlatform);

                }
                mesuresSondeMulti.remove();
            }
            mapSitesSandreCSV(sitesMapSandreElement, sitesMapSandreContext, printStream);

            mapPlatformsCoordCSV(platformsMapSite, platformsMapSandreElement, platformsMapSandreContext, platformsMapLongitude, platformsMapLatitude, platformsMapAltitude, printStream);

            mapVariablesSandreCSV(variablesMapSandreElement, variablesMapSandreContext, printStream);

            mapUnitSandreCSV(variablesMapSandreElement, datatype, printStream);

            reminderMap.put(FILENAME_INFO_SUP, reminderFile);

            printStream.flush();
            printStream.close();

            return reminderMap;
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param variablesMapSandreElement
     * @param variablesMapSandreContext
     * @param printStream
     */
    private void mapVariablesSandreCSV(Map<String, String> variablesMapSandreElement, Map<String, String> variablesMapSandreContext, PrintStream printStream) {

        for (String name : variablesMapSandreElement.keySet()) {

            if (variablesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"variable\";\"%s\";\"%s\";\"%s\";", name, variablesMapSandreElement.get(name), variablesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"variable\";\"%s\";", name));
            }
        }
    }

    /**
     *
     * @param sitesMapSandreElem
     * @param sitesMapSandreCont
     * @param printStream
     */
    private void mapSitesSandreCSV(Map<String, String> sitesMapSandreElement, Map<String, String> sitesMapSandreContext, PrintStream printStream) {
        for (String name : sitesMapSandreElement.keySet()) {
            if (sitesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"site\";\"%s\";\"%s\";\"%s\";", name, sitesMapSandreElement.get(name), sitesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"site\";\"%s\";", name));
            }
        }
    }

    /**
     *
     * @param sitesMapSandreElem
     * @param platformsMapLongitude
     * @param platformsMapLatitude
     * @param platformsMapAltitude
     * @param printStream
     */
    private void mapPlatformsCoordCSV(Map<String, String> platformsMapSite, Map<String, String> platformsMapSandreElement, Map<String, String> platformsMapSandreContext, Map<String, Float> platformsMapLongitude, Map<String, Float> platformsMapLatitude, Map<String, Float> platformsMapAltitude, PrintStream printStream) {

        for (String name : platformsMapLongitude.keySet()) {

            if (platformsMapLongitude.get(name) != null) {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";\"%s\";\"%s\";\"%s\";\"%s\";\"%s\";", name, platformsMapSite.get(name), platformsMapSandreElement.get(name), platformsMapSandreContext.get(name), platformsMapLongitude.get(name), platformsMapLatitude.get(name), platformsMapAltitude.get(name)));
            } else {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";", name, platformsMapSite.get(name)));
            }
        }
    }

    /**
     *
     * @param variablesMap
     * @param datatype
     * @param printStream
     * @throws PersistenceException
     */
    private void mapUnitSandreCSV(Map<String, String> variablesMap, DataType datatype, PrintStream printStream) throws PersistenceException {

        for (String name : variablesMap.keySet()) {

            DatatypeVariableUniteGLACPE datatypeVariableUnite = datatypeVariableUniteDAO.getByDatatypeAndVariable(datatype.getCode(), Utils.createCodeFromString(name)).orElseThrow(() -> new PersistenceException("can't find dvu"));

            if (datatypeVariableUnite.getCodeSandre() != null) {
                printStream.println(String.format("\"Unité(variable)\";\"%s(%s)\";\"%s\";\"%s\"", datatypeVariableUnite.getUnite().getName(), name, datatypeVariableUnite.getCodeSandre(), datatypeVariableUnite.getContexte()));
            } else {
                printStream.println(String.format("\"Unité(variable)\";\"%s(%s)\"", datatypeVariableUnite.getUnite().getName(), name));
            }
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, SondeMultiRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

}

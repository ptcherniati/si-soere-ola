package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import com.google.common.collect.Lists;
import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.AggregatedData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas.ValueAggregatedData;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiAggregatedDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_SONDEMULTI = "org.inra.ecoinfo.glacpe.extraction.sondemulti.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";
    private static final String MAP_INDEX_0 = "0";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_AGGREGATED_DATA";
    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_AGGREGATED_DATA_ALLDEPTH";
    private static final String TARGET_VALUE = "PROPERTY_MSG_HEADER_TARGET_VALUE";

    private static final String CODE_DATATYPE_SONDE_MULTI = "sonde_multiparametres";

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatas);

        List<ExtractionParametersVO> variablesExtractionParameters = datasRequestParamVO.getExtractionParameters();

        StringBuilder stringBuilder = new StringBuilder();

        if (depthRequestParamVO.getAllDepth()) {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA_ALLDEPTH)));
        } else {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, HEADER_RAW_DATA)));
        }

        for (ExtractionParametersVO variableExtractionParameters : variablesExtractionParameters) {
            DatatypeVariableUniteGLACPE dvu = dvus.stream()
                    .filter(v -> v.equals(variableExtractionParameters.getDvu()))
                    .findFirst()
                    .orElse(null);

            String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
            String uniteNom = dvu.getUnite().getName();
            if (variableExtractionParameters.getValueMin()) {
                stringBuilder.append(String.format(";\"minimum(%s)%n%s\"", localizedVariableName, uniteNom));
            }
            if (variableExtractionParameters.getValueMax()) {
                stringBuilder.append(String.format(";\"maximum(%s)%n%s\"", localizedVariableName, uniteNom));
            }

            if (!variableExtractionParameters.getTargetValues().isEmpty()) {
                for (int i = 0; i < variableExtractionParameters.getTargetValues().size(); i++) {
                    if (!variableExtractionParameters.getTargetValues().get(i).equals("-1")) {
                        stringBuilder.append(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_SONDEMULTI, TARGET_VALUE), localizedVariableName, variableExtractionParameters.getTargetValues().get(i), variableExtractionParameters
                                .getUncertainties().get(i)));
                    }
                }
            }
        }
        return stringBuilder.toString();

    }

    @SuppressWarnings({"rawtypes", "unchecked", "unused"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(requestMetadatasMap);
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        List<ExtractionParametersVO> variablesExtractionParameters = datasRequestParamVO.getExtractionParameters();

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        List<ExtractionParametersVO> variablesExtraction = Lists.newLinkedList();
        for (IGLACPEAggregateData valeurMesure : valeursMesures) {
            for (ExtractionParametersVO variableExtractionParameters : variablesExtractionParameters) {
                if (valeurMesure.getRealNode().getNodeable().equals(variableExtractionParameters.getDvu()) || variableExtractionParameters.getValueMin() || variableExtractionParameters.getValueMax() || variableExtractionParameters.getTargetValues().size() > 0) {
                    if (!variablesExtraction.contains(variableExtractionParameters)) {
                        variablesExtraction.add(variableExtractionParameters);
                    }
                }
            }
        }
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_AGGREGATED);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_AGGREGATED);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> variablesAggregatedDatas = buildVariablesAggregatedDatas(valeursMesures, variablesExtraction);
        for (Iterator<Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> iterator = variablesAggregatedDatas.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> mapBySite = projetEntry.getValue();
            for (Iterator<Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapByPlateforme = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_AGGREGATED));
                for (Iterator<Map.Entry<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> plateformeEntrye = iterator2.next();
                    Plateforme plateforme = plateformeEntrye.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas> mapByDvu = plateformeEntrye.getValue();
                    for (ExtractionParametersVO extractionParametersVO : variablesExtraction) {
                        DatatypeVariableUniteGLACPE dvu = extractionParametersVO.getDvu();
                        VariableAggregatedDatas variableAggregatedDatas = mapByDvu.get(dvu);if(variableAggregatedDatas==null){
                            continue;
                        }
                        String localizedOutilName = propertiesOutilName.getProperty(variableAggregatedDatas.getOutilsMesure()!=null?variableAggregatedDatas.getOutilsMesure().getNom():"null", variableAggregatedDatas.getOutilsMesure()!=null?variableAggregatedDatas.getOutilsMesure().getNom():"");
                        if (variableAggregatedDatas != null) {
                            if (extractionParametersVO.getValueMin()) {
                                AggregatedData aggregatedData = variableAggregatedDatas.getMinAggregatedData();
                                TreeSet<LocalDate> datesKeys = new TreeSet<LocalDate>(aggregatedData.getValuesAggregatedDatasByDatesMap().keySet());
                                for (LocalDate dateKey : datesKeys) {
                                    for (ValueAggregatedData value : aggregatedData.getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                        if (value != null) {

                                            rawDataPrintStream.print(String.format(
                                                    "%s;%s;%s;%s;%s;%s;",
                                                    localizedProjetName,
                                                    localizedSiteName,
                                                    localizedPlateformeName,
                                                    localizedOutilName,
                                                    DateUtil.getUTCDateTextFromLocalDateTime(dateKey, DateUtil.DD_MM_YYYY),
                                                    value.getHeure() != null ? DateUtil.getUTCDateTextFromLocalDateTime(value.getHeure(), DateUtil.HH_MM_SS) : "")
                                            );

                                            if (depthRequestParamVO.getAllDepth()) {
                                                rawDataPrintStream.print(String.format("%s;", value.getDepth()));
                                            } else {
                                                rawDataPrintStream.print(String.format("%s;%s;%s;", depthMin, depthMax, value.getDepth()));
                                            }

                                            for (ExtractionParametersVO variableExtractionParameters : variablesExtraction) {
                                                String minSeparator = buildCSVSeparator(variableExtractionParameters.getValueMin());
                                                String maxSeparator = buildCSVSeparator(variableExtractionParameters.getValueMax());
                                                String targetSeparator = "";
                                                for (String targetValue : variableExtractionParameters.getTargetValues()) {
                                                    targetSeparator = targetSeparator + ";";
                                                }
                                                String variableExtractionParametersCodes = variableExtractionParameters.getLocalizedName();
                                                if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(Utils.createCodeFromString(variableExtractionParametersCodes))) {
                                                    rawDataPrintStream.print(String.format("%s%s%s%s", value.getValue(), minSeparator, maxSeparator, targetSeparator));
                                                    break;
                                                } else {
                                                    rawDataPrintStream.print(String.format("%s%s%s", minSeparator, maxSeparator, targetSeparator));
                                                }
                                            }
                                            rawDataPrintStream.println();
                                        }
                                    }
                                }
                            }

                            if (extractionParametersVO.getValueMax()) {
                                AggregatedData aggregatedData = variableAggregatedDatas.getMaxAggregatedData();
                                TreeSet<LocalDate> datesKeys = new TreeSet<LocalDate>(aggregatedData.getValuesAggregatedDatasByDatesMap().keySet());
                                for (LocalDate dateKey : datesKeys) {
                                    for (ValueAggregatedData value : aggregatedData.getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                        if (value != null) {

                                            rawDataPrintStream.print(String.format(
                                                    "%s;%s;%s;%s;%s;%s;",
                                                    localizedProjetName,
                                                    localizedSiteName,
                                                    localizedPlateformeName,
                                                    localizedOutilName,
                                                    DateUtil.getUTCDateTextFromLocalDateTime(dateKey, DateUtil.DD_MM_YYYY),
                                                    value.getHeure() != null ? DateUtil.getUTCDateTextFromLocalDateTime(value.getHeure(), DateUtil.HH_MM_SS) : "")
                                            );

                                            if (depthRequestParamVO.getAllDepth()) {
                                                rawDataPrintStream.print(String.format("%s;", value.getDepth()));
                                            } else {
                                                rawDataPrintStream.print(String.format("%s;%s;%s;", depthMin, depthMax, value.getDepth()));
                                            }

                                            for (ExtractionParametersVO variableExtractionParameters : variablesExtraction) {
                                                String minSeparator = buildCSVSeparator(variableExtractionParameters.getValueMin());
                                                String maxSeparator = buildCSVSeparator(variableExtractionParameters.getValueMax());
                                                String targetSeparator = "";
                                                for (String targetValue : variableExtractionParameters.getTargetValues()) {
                                                    targetSeparator = targetSeparator + ";";
                                                }
                                                String variableExtractionParametersCodes = variableExtractionParameters.getLocalizedName();
                                                if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(variableExtractionParametersCodes)) {
                                                    rawDataPrintStream.print(String.format("%s%s%s%s", minSeparator, value.getValue(), maxSeparator, targetSeparator));
                                                    break;
                                                } else {
                                                    rawDataPrintStream.print(String.format("%s%s%s", minSeparator, maxSeparator, targetSeparator));
                                                }
                                            }
                                            rawDataPrintStream.println();
                                        }
                                    }
                                }
                            }
                        }
                        for (String targetValue : extractionParametersVO.getTargetValues()) {
                            for (AggregatedData aggregatedData : variableAggregatedDatas.getTargetAggregatedDatas()) {
                                TreeSet<LocalDate> datesKeys = new TreeSet<LocalDate>(aggregatedData.getValuesAggregatedDatasByDatesMap().keySet());
                                for (LocalDate dateKey : datesKeys) {
                                    for (ValueAggregatedData value : aggregatedData.getValuesAggregatedDatasByDatesMap().get(dateKey)) {
                                        if (value.getHeure() == null) {
                                            rawDataPrintStream.print(String.format(
                                                    "%s;%s;%s;%s;%s;",
                                                    localizedProjetName,
                                                    localizedSiteName,
                                                    localizedPlateformeName,
                                                    localizedOutilName,
                                                    DateUtil.getUTCDateTextFromLocalDateTime(dateKey, DateUtil.DD_MM_YYYY))
                                            );
                                        } else {
                                            rawDataPrintStream.print(String.format(
                                                    "%s;%s;%s;%s;%s;%s;",
                                                    localizedProjetName,
                                                    localizedSiteName,
                                                    localizedPlateformeName,
                                                    localizedOutilName,
                                                    DateUtil.getUTCDateTextFromLocalDateTime(dateKey, DateUtil.DD_MM_YYYY),
                                                    value.getHeure() != null ? DateUtil.getUTCDateTextFromLocalDateTime(value.getHeure(), DateUtil.HH_MM_SS) : "")
                                            );
                                        }
                                        if (depthRequestParamVO.getAllDepth()) {
                                            rawDataPrintStream.print(String.format("%s;", value.getDepth()));
                                        } else {
                                            rawDataPrintStream.print(String.format("%s;%s;%s;", depthMin, depthMax, value.getDepth()));
                                        }

                                        for (ExtractionParametersVO variableExtractionParameters : variablesExtraction) {
                                            String minSeparator = buildCSVSeparator(variableExtractionParameters.getValueMin());
                                            String maxSeparator = buildCSVSeparator(variableExtractionParameters.getValueMax());
                                            String targetBeforeSeparator = "";
                                            String targetAfterSeparator = "";
                                            for (String targetValueBuilder : variableExtractionParameters.getTargetValues()) {
                                                if (variableExtractionParameters.getTargetValues().indexOf(targetValueBuilder) < extractionParametersVO.getTargetValues().indexOf(targetValue)) {
                                                    targetBeforeSeparator = targetBeforeSeparator + ";";
                                                } else {
                                                    targetAfterSeparator = targetAfterSeparator + ";";
                                                }
                                            }
                                            String variableExtractionParametersCodes = variableExtractionParameters.getLocalizedName();
                                            if (Utils.createCodeFromString(variableAggregatedDatas.getVariableName()).equals(Utils.createCodeFromString(variableExtractionParametersCodes))) {
                                                rawDataPrintStream.print(String.format("%s%s%s%s%s", minSeparator, maxSeparator, targetBeforeSeparator, value.getValue(), targetAfterSeparator));
                                            } else {
                                                rawDataPrintStream.print(String.format("%s%s%s%s", minSeparator, maxSeparator, targetBeforeSeparator, targetAfterSeparator));
                                            }
                                        }

                                        rawDataPrintStream.println();
                                    }
                                }
                            }
                        }
                        //variablesAggregatedDatas.remove(variableAggregatedDatas);
                    }

                }
            }
        }
        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>> buildValeursMesuresReorderedByDatesAndVAriables(List<IGLACPEAggregateData> valeursMesures) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedBySitesProjetPlateformesVariablesDates = new TreeMap<>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {
            valeursMesuresReorderedBySitesProjetPlateformesVariablesDates
                    .computeIfAbsent(valeurMesure.getProjet(), k -> new TreeMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>())
                    .computeIfAbsent(valeurMesure.getSite(), k -> new TreeMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>())
                    .computeIfAbsent(valeurMesure.getPlateforme(), k -> new TreeMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>())
                    .computeIfAbsent(valeurMesure.getRealNode(), k -> new TreeMap<>())
                    .computeIfAbsent(valeurMesure.getDate(), k -> new TreeSet<>(
                    (r1, r2) -> ((DatatypeVariableUniteGLACPE) r1.getRealNode().getNodeable()).getVariable().compareTo(((DatatypeVariableUniteGLACPE) r2.getRealNode().getNodeable()).getVariable()))
                    )
                    .add(valeurMesure);
        }
        return valeursMesuresReorderedBySitesProjetPlateformesVariablesDates;
    }

    /**
     *
     * @param valeursMesures
     * @param extractionParametersVO
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> buildVariablesAggregatedDatas(List<IGLACPEAggregateData> valeursMesures, List<ExtractionParametersVO> extractionParametersVO) {
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByDatesAndVAriables(valeursMesures);
        TreeMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> variablesAggregatedDatas = new TreeMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>();
        for (Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>> projetEntry : valeursMesuresReorderedByDatesVariablesAndDates.entrySet()) {
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>> mapBySite = projetEntry.getValue();
            for (Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>> siteEntry : mapBySite.entrySet()) {
                SiteGLACPE site = siteEntry.getKey();
                SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>> mapByPlatform = siteEntry.getValue();
                for (Map.Entry<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>> platformEntry : mapByPlatform.entrySet()) {
                    Plateforme platform = platformEntry.getKey();
                    SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>> mapByVariable = platformEntry.getValue();
                    for (Map.Entry<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>> variableEntry : mapByVariable.entrySet()) {
                        RealNode realNode = variableEntry.getKey();
                        SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>> mapByDate = variableEntry.getValue();
                        for (Map.Entry<LocalDate, SortedSet<IGLACPEAggregateData>> dateEntry : mapByDate.entrySet()) {
                            LocalDate date = dateEntry.getKey();
                            SortedSet<IGLACPEAggregateData> valeursMesureDates = dateEntry.getValue();
                            VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getName(), platform.getName(), ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable().getName());
                            ExtractionParametersVO extractionParam = extractionParametersVO.stream()
                                    .filter(ep -> ep.getDvu().equals(realNode.getNodeable()))
                                    .findFirst()
                                    .orElse(new ExtractionParametersVO((DatatypeVariableUniteGLACPE) realNode.getNodeable(), localizationManager));
                            fillVariableAggregatedDatas(variableAggregatedDatas, date, valeursMesureDates, extractionParam);

                            variablesAggregatedDatas
                                    .computeIfAbsent(projet, k -> new TreeMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>())
                                    .computeIfAbsent(site, k -> new TreeMap<Plateforme, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>())
                                    .computeIfAbsent(platform, k -> new TreeMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>())
                                    .put((DatatypeVariableUniteGLACPE) realNode.getNodeable(), variableAggregatedDatas);
                        }
                    }
                }
            }
        }
        return variablesAggregatedDatas;

    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     * @param extractionParametersVO
     */
    protected void fillVariableAggregatedDatas(VariableAggregatedDatas variableAggregatedDatas, LocalDate dateKey, SortedSet<IGLACPEAggregateData> valeursMesureDates, ExtractionParametersVO extractionParametersVO) {

        Map<String, IGLACPEAggregateData> approximativeTargetValues = new HashMap<String, IGLACPEAggregateData>();

        for (int i = 0; i < extractionParametersVO.getTargetValues().size(); i++) {
            AggregatedData listAggregatedDatas = variableAggregatedDatas.new AggregatedData();
            variableAggregatedDatas.getTargetAggregatedDatas().add(listAggregatedDatas);
            approximativeTargetValues.put(extractionParametersVO.getTargetValues().get(i), null);
        }

        Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
        while (iterator.hasNext()) {
            IGLACPEAggregateData valeurMesure = iterator.next();
            if (valeurMesure.getValue() != null) {
                if (valeurMesure.getOutilsMesure() != null) {
                    variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                }

                if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, values);

                }
                if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).isEmpty()) {
                    ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                    valueAggregatedData.setHeure(valeurMesure.getHeure());
                    variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                } else if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() != null) {
                    if (variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() > valeurMesure.getValue()) {
                        variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    } else if (Math.abs(variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMinAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    }
                }

                if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                    List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().put(dateKey, values);

                }
                if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).isEmpty()) {
                    ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                    valueAggregatedData.setHeure(valeurMesure.getHeure());
                    variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                } else if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() != null) {
                    if (variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() < valeurMesure.getValue()) {
                        variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).clear();
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    } else if (Math.abs(variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).get(0).getValue() - valeurMesure.getValue()) < Math.pow(10, -10)) {
                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        variableAggregatedDatas.getMaxAggregatedData().getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                    }
                }

                for (int i = 0; i < extractionParametersVO.getTargetValues().size(); i++) {
                    if (valeurMesure.getValue() <= Float.parseFloat(extractionParametersVO.getTargetValues().get(i)) + Float.parseFloat(extractionParametersVO.getUncertainties().get(i))
                            && valeurMesure.getValue() >= Float.parseFloat(extractionParametersVO.getTargetValues().get(i)) - Float.parseFloat(extractionParametersVO.getUncertainties().get(i))) {
                        AggregatedData aggregatedDatas;

                        aggregatedDatas = variableAggregatedDatas.getTargetAggregatedDatas().get(i);
                        variableAggregatedDatas.getTargetAggregatedDatas().remove(i);

                        ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                        valueAggregatedData.setHeure(valeurMesure.getHeure());
                        if (aggregatedDatas.getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                            List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                            aggregatedDatas.getValuesAggregatedDatasByDatesMap().put(dateKey, values);
                        }
                        aggregatedDatas.getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
                        variableAggregatedDatas.getTargetAggregatedDatas().add(i, aggregatedDatas);
                    } else if (variableAggregatedDatas.getTargetAggregatedDatas().get(i) == null) {
                        if (approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i)) != null) {
                            if (Math.abs(valeurMesure.getValue() - Float.parseFloat(extractionParametersVO.getTargetValues().get(i))) < Math.abs(approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i)).getValue()
                                    - Float.parseFloat(extractionParametersVO.getTargetValues().get(i)))) {
                                approximativeTargetValues.put(extractionParametersVO.getTargetValues().get(i), valeurMesure);
                            }
                        } else {
                            approximativeTargetValues.put(extractionParametersVO.getTargetValues().get(i), valeurMesure);
                        }
                    }
                }
            }
            iterator.remove();
        }
        for (int i = 0; i < extractionParametersVO.getTargetValues().size(); i++) {
            if (variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().get(dateKey) == null) {
                List<ValueAggregatedData> values = new LinkedList<ValueAggregatedData>();
                variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().put(dateKey, values);
            }
            if (variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().get(dateKey).isEmpty() && approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i)) != null) {
                IGLACPEAggregateData valeurMesure = approximativeTargetValues.get(extractionParametersVO.getTargetValues().get(i));

                ValueAggregatedData valueAggregatedData = variableAggregatedDatas.new ValueAggregatedData(valeurMesure.getDepth(), valeurMesure.getValue());
                valueAggregatedData.setHeure(valeurMesure.getHeure());
                variableAggregatedDatas.getTargetAggregatedDatas().get(i).getValuesAggregatedDatasByDatesMap().get(dateKey).add(valueAggregatedData);
            }
        }

    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, SondeMultiAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

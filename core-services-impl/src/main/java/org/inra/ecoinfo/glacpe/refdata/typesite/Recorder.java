/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.typesite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeSite> {

    /**
     *
     */
    protected ITypeSiteDAO typeSiteDAO;
    private Properties propertiesNomFR;
    private Properties propertiesNomEN;
    private Properties propertiesDescriptionEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(TypeSite.class));

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken(true);
                String description = tokenizerValues.nextToken();

                TypeSite typeSite = new TypeSite(nom, description);
                TypeSite dbTypeSite = typeSiteDAO.getByCode(typeSite.getCode()).orElse(null);

                // afiocca
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();

                // Enregistre un site uniquement s'il n'existe pas en BD ou bien
                // s'il est considéré comme une mise à jour
                if (dbTypeSite == null) {
                    typeSite.setCodeSandre(codeSandre);
                    typeSite.setContexte(contexte);
                    typeSiteDAO.saveOrUpdate(typeSite);

                } else {
                    dbTypeSite.setDescription(typeSite.getDescription());
                    dbTypeSite.setCodeSandre(codeSandre);
                    dbTypeSite.setContexte(contexte);
                    typeSiteDAO.saveOrUpdate(dbTypeSite);
                }

            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                mgaServiceBuilder.getRecorder().remove(typeSiteDAO.getByCode(code)
                        .orElseThrow(() -> new PersistenceException("can't find typeSite")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param typeSiteDAO
     */
    public void setTypeSiteDAO(ITypeSiteDAO typeSiteDAO) {
        this.typeSiteDAO = typeSiteDAO;
    }

    /**
     *
     * @param typeSite
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeSite typeSite) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : typeSite.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : propertiesNomFR.get(typeSite.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : propertiesNomEN.get(typeSite.getCode()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : typeSite.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : propertiesDescriptionEN.get(typeSite.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : typeSite.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeSite == null ? EMPTY_STRING : typeSite.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<TypeSite> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(TypeSite.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(TypeSite.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        propertiesDescriptionEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(TypeSite.class), "description", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    @Override
    protected List<TypeSite> getAllElements() {
        return typeSiteDAO.getAllBy(TypeSite.class, TypeSite::getCode);
    }

}

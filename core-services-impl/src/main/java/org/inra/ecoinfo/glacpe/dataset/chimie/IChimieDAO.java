package org.inra.ecoinfo.glacpe.dataset.chimie;


import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public interface IChimieDAO extends IDAO<Object> {

    /**
     *
     * @param user
     * @return
     */
    Collection<PlateformeProjetVO> getAvailablePlatforms(IUser user);

    /**
     *
     * @param currentUser
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    List<DatatypeVariableUniteGLACPE> getAvailableVariables(IUser currentUser, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates);

    /**
     *
     * @param currentUser
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    Collection<Float> getAvailableDepths(IUser currentUser, List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables);

    /**
     *
     * @param currentUser
     * @param selectedPlateformes
     * @param datesRequestParamVO
     * @param depthRequestParamVO
     * @param dvus
     * @return
     */
    List<MesureChimie> extractDatas(IUser currentUser, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes, List<IntervalDate> datesRequestParamVO, DepthRequestParamVO depthRequestParamVO, List<DatatypeVariableUniteGLACPE> dvus);

    /**
     *
     * @param currentUser
     * @param selectedPlateformes
     * @param datesRequestParamVO
     * @param depthRequestParamVO
     * @param dvus
     * @return
     */
    List<ValeurMesureChimie> extractValueDatas(IUser currentUser, SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes, List<IntervalDate> datesRequestParamVO, DepthRequestParamVO depthRequestParamVO, List<DatatypeVariableUniteGLACPE> dvus);

}

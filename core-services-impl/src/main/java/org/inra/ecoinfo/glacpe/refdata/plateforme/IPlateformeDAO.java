/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IPlateformeDAO extends IDAO<Plateforme> {

    /**
     * 
     * @param codePlateforme
     * @param codeSite
     * @return Null if there is no result
     */
    public Optional<Plateforme> getByNKey(String codePlateforme, String codeSite);
}

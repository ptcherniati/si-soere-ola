package org.inra.ecoinfo.glacpe.dataset.impl;

import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * The Class VariableValue.
 */
public class VariableValue {

    /** The variableGLACPE @link(VariableGLACPE). */
    private VariableGLACPE variableGLACPE;

    /** The variableGLACPE @link(VariableGLACPE). */
    private RealNode realNode;

    /** The value @link(String). */
    private String value;

    /**
     * Instantiates a new variable value.
     * 
     * @param vglcp
     * @param variableGLACPE
     * @param string
     * @param value
     *            the value
     * @param datatypeVariableUnite
     *            the datatypeVariableUnite
     */
    public VariableValue(final VariableGLACPE variableGLACPE, final String value) {
        this.variableGLACPE = variableGLACPE;
        this.value = value;
    }

    /**
     * Instantiates a new variable value.
     * 
     * @param rn
     * @param realNode
     * @param string
     * @param value
     *            the value
     * @param datatypeVariableUnite
     *            the datatypeVariableUnite
     */
    public VariableValue(final RealNode realNode, final String value) {
        this.realNode = realNode;
        this.value = value;
        this.variableGLACPE = realNode==null?null:((DatatypeVariableUniteGLACPE)realNode.getNodeable()).getVariable();
    }

    /**
     * Gets the datatypeVariableUnite.
     * 
     * @return the datatypeVariableUnite
     */
    public VariableGLACPE getVariableGLACPE() {
        return variableGLACPE;
    }

    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the datatypeVariableUnite.
     * 
     * @param variableGLACPE
     */
    public void setVariableGLACPE(final VariableGLACPE variableGLACPE) {
        this.variableGLACPE = variableGLACPE;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }
}

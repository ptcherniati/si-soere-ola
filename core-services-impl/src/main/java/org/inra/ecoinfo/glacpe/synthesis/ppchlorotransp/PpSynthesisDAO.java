/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.ppchlorotransp;

import java.util.stream.Stream;
import org.inra.ecoinfo.glacpe.synthesis.pp.SynthesisDatatype;
import org.inra.ecoinfo.glacpe.synthesis.pp.SynthesisValue;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class PpSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     *
     */
    public static String SYNTHESIS_REQUEST = "insert into ppsynthesisvalue(id, date, idnode, ismean, site, valuefloat, valuestring, variable, siteid, projetid, plateformeid, variableid, profondeurs)\n"
            + "(select \n"
            + "nextval('hibernate_sequence') id,\n"
            + "s.date date,\n"
            + "dnv.branch_node_id idNode,\n"
            + "false isMean,\n"
            + "rns.path site,\n"
            + "avg(v.valeur) valueFloat,\n"
            + "null valueString,\n"
            + "nddvu.code variable,\n"
            + "rns.id_nodeable siteid,\n"
            + "rnp.id_nodeable projetid,\n"
            + "ss.loc_id plateformeid,\n"
            + "dvu.var_id variableid,\n"
            + "array_agg(distinct m.profondeur order by m.profondeur)::::text profondeurs\n"
            + "from valeur_mesure_production_primaire_vmpp v\n"
            + "join composite_node_data_set dnv on dnv.realnode= v.id\n"
            + "join mesure_production_primaire_mpp m ON m.mpp_id = v.mpp_id\n"
            + "join sous_sequence_production_primaire_sspp ss ON ss.sspp_id = m.sspp_id\n"
            + "join sequence_production_primaire_spp s ON s.spp_id = ss.spp_id\n"
            + "\n"
            + "JOIN realnode rnv ON rnv.id = v.id\n"
            + "join composite_nodeable nddvu on nddvu.id = rnv.id_nodeable\n"
            + "join datatype_variable_unite_glacpe_dvug dvu ON rnv.id_nodeable = vdt_id\n"
            + "join realnode rnd ON rnd.id = rnv.id_parent_node\n"
            + "JOIN realnode rnt ON rnt.id = rnd.id_parent_node\n"
            + "JOIN realnode rns ON rns.id = rnt.id_parent_node\n"
            + "JOIN realnode rnts ON rnts.id = rns.id_parent_node\n"
            + "JOIN realnode rnp ON rnp.id = rnts.id_parent_node\n"
            + "group by s.date, rns.path, dnv.branch_node_id, nddvu.code, ss.loc_id, rns.id_nodeable, rnp.id_nodeable, rnp.id_nodeable,  dvu.var_id)";

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        entityManager.createNativeQuery(SYNTHESIS_REQUEST).executeUpdate();
        return Stream.empty();
    }

}

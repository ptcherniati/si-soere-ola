package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.zooplancton.IZooplanctonDAO;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.zooplancton.ResultExtractionZooplanctonBiovolume;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class OutputBuilderZooBiovolume extends AbstractOutputBuilder {

    private static final String PROPERTY_MSG_HEADER_BIOVOLUME = "MSG_HEADER_BIOVOLUME";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_ZOOPLANKTON = "org.inra.ecoinfo.glacpe.extraction.zooplancton.messages";
    private String SUFFIX_FILENAME = "biovolumes";

    /**
     *
     */
    protected IZooplanctonDAO zooplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    protected IProjetDAO projetDAO;
    protected ISiteGLACPEDAO siteGLACPEDAO;

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    /**
     *
     * @param siteGLACPEDAO
     */
    public void setSiteGLACPEDAO(ISiteGLACPEDAO siteGLACPEDAO) {
        this.siteGLACPEDAO = siteGLACPEDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Map<String, File> buildOutput(final IParameter parameters, final String cstResultExtractionCode) throws BusinessException, NoExtractionResultException {

        final Map<String, List> results = parameters.getResults().get(cstResultExtractionCode);
        if (results == null || (results != null && results.get(AbstractOutputBuilder.MAP_INDEX_0).isEmpty())) {
            throw new NoExtractionResultException(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
        }
        final String resultsHeader = buildHeader(parameters.getParameters());
        return buildBody(resultsHeader, results, parameters.getParameters());
    }

    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(buildOutput(parameters, ExtractorZooBiovolume.CST_RESULT_EXTRACTION_CODE));

        return null;
    }

    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {

        StringBuilder stringBuilder = new StringBuilder();

        try {
            stringBuilder.append(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_ZOOPLANKTON, PROPERTY_MSG_HEADER_BIOVOLUME));
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        // List<VariableVO> selectedVariables = (List<VariableVO>) requestMetadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_VARIABLES);

        List<ResultExtractionZooplanctonBiovolume> resultsExtraction = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);

        // Map<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>> sumBiovolumeMap = new HashMap<Date, Map<String, Map<Long, List<MesurePhytoplancton>>>>();
        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>>> extractMap = resultsExtraction.stream()
                .collect(
                        Collectors.groupingBy(
                                ResultExtractionZooplanctonBiovolume::getProjet,
                                TreeMap::new,
                                Collectors.groupingBy(
                                        ResultExtractionZooplanctonBiovolume::getSite,
                                        TreeMap::new,
                                        Collectors.groupingBy(
                                                ResultExtractionZooplanctonBiovolume::getPlateforme,
                                                TreeMap::new,
                                                Collectors.groupingBy(
                                                        ResultExtractionZooplanctonBiovolume::getDate,
                                                        TreeMap::new,
                                                        Collectors.groupingBy(
                                                                ResultExtractionZooplanctonBiovolume::getOutilPrelevement,
                                                                TreeMap::new,
                                                                Collectors.toCollection(TreeSet::new)
                                                        )
                                                )
                                        )
                                )
                        )
                );
        for (Iterator<Map.Entry<String, TreeMap<String, TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>>>> iterator = extractMap.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, TreeMap<String, TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>>> projectEntry = iterator.next();
            String projetName = projectEntry.getKey();
            TreeMap<String, TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>> mapBySite = projectEntry.getValue();
            String localizedNameProjet = propertiesProjetName.getProperty(Utils.createCodeFromString(projetName), projetName);
            for (Iterator<Map.Entry<String, TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<String, TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>> siteEntry = iterator1.next();
                String siteName = siteEntry.getKey();
                TreeMap<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>> mapByPlateforme = siteEntry.getValue();
                String localizedNameSite = propertiesSiteName.getProperty(siteName, siteName);
                Projet projet = projetDAO.getByCode(Utils.createCodeFromString(projetName)).orElseThrow(() -> new BusinessException("can't load projet"));
                Site site = siteGLACPEDAO.getByCode(siteName).orElseThrow(() -> new BusinessException("can't load site"));
                final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, (SiteGLACPE) site, SUFFIX_FILENAME));
                rawDataPrintStream.println(headers);
                for (Iterator<Map.Entry<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<String, TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>> plateformeEntry = iterator2.next();
                    String plateformeCode = plateformeEntry.getKey();
                    TreeMap<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>> mapByDate = plateformeEntry.getValue();
                    String localizedNamePlateforme = propertiesPlateformeName.getProperty(plateformeCode, plateformeCode);
                    for (Iterator<Map.Entry<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>>> iterator3 = mapByDate.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>>> dateEntry = iterator3.next();
                        LocalDate date = dateEntry.getKey();
                        TreeMap<String, TreeSet<ResultExtractionZooplanctonBiovolume>> mapByOutil = dateEntry.getValue();
                        String dateString = DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY);
                        for (Iterator<Map.Entry<String, TreeSet<ResultExtractionZooplanctonBiovolume>>> iterator4 = mapByOutil.entrySet().iterator(); iterator4.hasNext();) {
                            Map.Entry<String, TreeSet<ResultExtractionZooplanctonBiovolume>> outilEntry = iterator4.next();
                            String outilName = outilEntry.getKey();
                            TreeSet<ResultExtractionZooplanctonBiovolume> results = outilEntry.getValue();
                            String localizedNameOutilPrelevement = propertiesOutilName.getProperty(outilName, outilName);
                            for (Iterator<ResultExtractionZooplanctonBiovolume> iterator5 = results.iterator(); iterator5.hasNext();) {
                                ResultExtractionZooplanctonBiovolume result = iterator5.next();
                                final String outilMesurename = result.getOutilMesure();
                                String localizedNameOutilMesure = propertiesOutilName.getProperty(outilMesurename, outilMesurename);
                                String line = String.format(
                                        "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                                        localizedNameProjet,
                                        localizedNameSite,
                                        localizedNamePlateforme,
                                        dateString,
                                        localizedNameOutilPrelevement,
                                        localizedNameOutilMesure,
                                        result.getProfondeurMin(),
                                        result.getProfondeurMax(),
                                        result.getNomDeterminateur(),
                                        result.getVolumeSedimente() == null ? "" : result.getVolumeSedimente()
                                );
                                outputPrintStreamMap.get(String.format("%1$s/%2$s/%1$s_%2$s_%3$s",
                                        Utils.createCodeFromString(result.getProjet()),
                                        Utils.createCodeFromString(result.getSite()),
                                        SUFFIX_FILENAME
                                )).println(line);

                            }
                            iterator4.remove();
                        }
                        iterator3.remove();
                    }
                    iterator2.remove();
                }
                iterator1.remove();
            }
            iterator.remove();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param zooplanctonDAO
     */
    public void setZooplanctonDAO(IZooplanctonDAO zooplanctonDAO) {
        this.zooplanctonDAO = zooplanctonDAO;
    }

}

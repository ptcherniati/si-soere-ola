package org.inra.ecoinfo.glacpe.dataset;

import org.inra.ecoinfo.dataset.IRecorder;

/**
 *
 * @author Antoine Schellenberger
 */
public interface IRecorderGLACPE extends IRecorder {

    /**
     * Sets the delete record.
     * 
     * @param deleteRecord
     *            the new delete record {@link IDeleteRecord} setter for an object use to delete a version of file
     */
    void setDeleteRecord(IDeleteRecord deleteRecord);

    /**
     * Sets the process record.
     * 
     * @param processRecord
     *            the new process record {@link IProcessRecord} setter for an object use to process the record of file
     */
    void setProcessRecord(IProcessRecord processRecord);

    /**
     * Sets the test format.
     * 
     * @param testFormat
     *            the new test format {@link ITestFormat} setter for an object use to test ht format of file
     */
    void setTestFormat(ITestFormat testFormat);
}

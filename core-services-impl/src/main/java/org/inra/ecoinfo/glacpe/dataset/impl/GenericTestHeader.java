package org.inra.ecoinfo.glacpe.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.ITestHeaders;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class GenericTestHeader implements ITestHeaders {

    /**
     * The Constant KERNEL_BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String KERNEL_BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.impl.messages";

    /**
     * The Constant PROPERTY_MSG_MISMATCH_COLUMN_HEADER @link(String).
     */
    protected static final String PROPERTY_MSG_MISMATCH_COLUMN_HEADER = "PROPERTY_MSG_MISMATCH_COLUMN_HEADER";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The logger.
     */
    final protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * The localization manager @link(ILocalizationManager).
     */
    private ILocalizationManager localizationManager;
    private IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;

    /**
     * Instantiates a new generic test header.
     */
    public GenericTestHeader() {
        super();
    }

    /**
     *      * sanitize the data of the file.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @param logger
     * @link(Logger) the logger
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @return the byte[]
     * @throws BadFormatException the bad format exception {@link VersionFile} the version file {@link Logger} the logger {@link BadsFormatsReport} the bads formats report
     * @see org.inra.ecoinfo.Utils$sanitizeData(org.inra.ecoinfo.dataset.versioning .entity.VersionFile)
     */
    protected byte[] sanitizeData(final VersionFile versionFile, final Logger logger, final BadsFormatsReport badsFormatsReport) throws BadFormatException {
        byte[] datasSanitized = null;

        try {
            datasSanitized = Utils.sanitizeData(versionFile.getData(), localizationManager);
        } catch (final Exception e) {
            logger.debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
            throw new BadFormatException(badsFormatsReport);
        }
        return datasSanitized;
    }

    /**
     * Test headers.
     *
     * @param parser the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(ISessionPropertiesMonSoere) the session properties
     * @param encoding the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptor) the dataset descriptor
     * @return the long
     * @throws BusinessException the business exception {@link VersionFile} the version file {@link ISessionPropertiesGLACPE} the session properties {@link BadsFormatsReport} the bads formats report {@link DatasetDescriptor} the dataset descriptor
     * @see org.inra.ecoinfo.glacpe.dataset.impl.ITestHeaders#testHeaders(com.Ostermiller.util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile, org.inra.ecoinfo.glacpe.dataset.impl.ISessionPropertiesMonSoere, java.lang.String)
     */
    @SuppressWarnings("unused")
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile, final String encoding, final BadsFormatsReport badsFormatsReport, final DatasetDescriptor datasetDescriptor) throws BusinessException {
        String datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<String, Map<String, List<RealNode>>>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        try {
            final byte[] datasSanitized = sanitizeData(versionFile, logger, badsFormatsReport);
        } catch (final Exception e) {
            badsFormatsReport.addException(e);
        }

        int lineNumber = 0;
        String[] values;
        try {
            values = parser.getLine();
            lineNumber++;
            int index = 0;

            // On parcourt chaque colonne d'une ligne
            for (String value = values[index]; index < values.length; index++) {
                value = values[index].trim();
                final String code = Utils.createCodeFromString(value);
                if (index > datasetDescriptor.getColumns().size() - 1) {
                    Column column = new Column();
                    column.setName(value);
                    column.setVariable(code);
                    column.setVariable(!realNodes.getOrDefault(code, new LinkedList<RealNode>()).isEmpty());
                    datasetDescriptor.getColumns().add(column);
                    continue;
                }
                final Column column = datasetDescriptor.getColumns().get(index);
                final String codifiedConfigColumnNameHeader = Utils.createCodeFromString(column.getName());
                final String codifiedFileColumnNameHeader = code;
                if (!codifiedConfigColumnNameHeader.equals(codifiedFileColumnNameHeader)) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(localizationManager.getMessage(KERNEL_BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISMATCH_COLUMN_HEADER), index + 1, value, column.getName())));
                }
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        return lineNumber;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }
}

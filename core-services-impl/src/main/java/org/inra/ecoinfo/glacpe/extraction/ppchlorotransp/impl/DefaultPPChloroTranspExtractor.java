package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.dataset.chloro.IChlorophylleDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class DefaultPPChloroTranspExtractor extends MO implements IExtractor {

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";
    // private static final String MSG_EXTRACTION_ABORTED = Messages.getString("PROPERTY_MSG_FAILED_EXTRACT");
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "Failure to extract data - Echec de l'extraction des données";
    //private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";

    /**
     *
     */
    protected IExtractor ppRawsDatasExtractor;

    /**
     *
     */
    protected IExtractor chloroRawsDatasExtractor;

    /**
     *
     */
    protected IExtractor transpRawsDatasExtractor;

    /**
     *
     */
    protected IExtractor transpAggregatedDatasExtractor;

    /**
     *
     */
    protected IExtractor ppAggregatedDatasExtractor;

    /**
     *
     */
    protected IExtractor chloroAggregatedDatasExtractor;

    /**
     *
     */
    protected IPPDAO productionPrimaireDAO;

    /**
     *
     */
    protected IChlorophylleDAO chlorophylleDAO;


    @Override
    public void setExtraction(Extraction extraction) {

    }

    private void addOutputBuilderByCondition(Boolean condition, IExtractor extractor, List<IExtractor> extractors) {
        if (condition) {
            extractors.add(extractor);
        }
    }

    /**
     *
     * @param metadatasMap
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<IExtractor> resolveExtractors(Map<String, Object> metadatasMap) {
        List<DatatypeVariableUniteGLACPE> dvUs = AbstractGLACPEExtractor.getDVUs(metadatasMap);
        boolean hasVariablespp = AbstractGLACPEExtractor.hasVariable(dvUs,PPChloroTranspParameter.PRODUCTION_PRIMAIRE) ;
        boolean hasVariablesChloro = AbstractGLACPEExtractor.hasVariable(dvUs,PPChloroTranspParameter.CHLOROPHYLLE);
        boolean hasVariablesTransp = dvUs.stream()
                .anyMatch(dvu -> dvu.getCode().startsWith("conditions_prelevements_transparence"));

        List<IExtractor> extractors = new LinkedList<IExtractor>();
        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(metadatasMap);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablespp, ppRawsDatasExtractor, extractors);
        addOutputBuilderByCondition((datasRequestParamVO.getDataBalancedByDepth() || datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()) && hasVariablespp, ppAggregatedDatasExtractor,
                extractors);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablesChloro, chloroRawsDatasExtractor, extractors);
        addOutputBuilderByCondition((datasRequestParamVO.getDataBalancedByDepth() || datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()) && hasVariablesChloro, chloroAggregatedDatasExtractor,
                extractors);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablesTransp, transpRawsDatasExtractor, extractors);
        addOutputBuilderByCondition((datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()) && hasVariablesTransp, transpAggregatedDatasExtractor, extractors);
        return extractors;
    }

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        int noResultExtractionCount = 0;
        List<IExtractor> extractors = resolveExtractors(parameters.getParameters());
        NoExtractionResultException exception = null;
        for (IExtractor extractor : extractors) {
            try {
                extractor.extract(parameters);
            } catch (NoExtractionResultException e) {
                noResultExtractionCount++;
                if (exception == null) {
                    exception = e;
                }
            } catch (BusinessException e) {
                throw e;
            }
        }
        if (noResultExtractionCount == extractors.size()) {
            throw new BusinessException(String.format(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT), PPChloroTranspParameter.CODE_EXTRACTIONTYPE_PP_CHLORO_TRANSP), exception);
        }
    }

    /**
     *
     * @param ppRawsDatasExtractor
     */
    public void setPpRawsDatasExtractor(IExtractor ppRawsDatasExtractor) {
        this.ppRawsDatasExtractor = ppRawsDatasExtractor;
    }

    /**
     *
     * @param chloroRawsDatasExtractor
     */
    public void setChloroRawsDatasExtractor(IExtractor chloroRawsDatasExtractor) {
        this.chloroRawsDatasExtractor = chloroRawsDatasExtractor;
    }

    /**
     *
     * @param transpRawsDatasExtractor
     */
    public void setTranspRawsDatasExtractor(IExtractor transpRawsDatasExtractor) {
        this.transpRawsDatasExtractor = transpRawsDatasExtractor;
    }

    /**
     *
     * @param ppAggregatedDatasExtractor
     */
    public void setPpAggregatedDatasExtractor(IExtractor ppAggregatedDatasExtractor) {
        this.ppAggregatedDatasExtractor = ppAggregatedDatasExtractor;
    }

    /**
     *
     * @param chloroAggregatedDatasExtractor
     */
    public void setChloroAggregatedDatasExtractor(IExtractor chloroAggregatedDatasExtractor) {
        this.chloroAggregatedDatasExtractor = chloroAggregatedDatasExtractor;
    }

    /**
     *
     * @param productionPrimaireDAO
     */
    public void setProductionPrimaireDAO(IPPDAO productionPrimaireDAO) {
        this.productionPrimaireDAO = productionPrimaireDAO;
    }

    /**
     *
     * @param chlorophylleDAO
     */
    public void setChlorophylleDAO(IChlorophylleDAO chlorophylleDAO) {
        this.chlorophylleDAO = chlorophylleDAO;
    }

    /**
     *
     * @param transpAggregatedDatasExtractor
     */
    public void setTranspAggregatedDatasExtractor(IExtractor transpAggregatedDatasExtractor) {
        this.transpAggregatedDatasExtractor = transpAggregatedDatasExtractor;
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        // TODO Auto-generated method stub
        return 0;
    }

}

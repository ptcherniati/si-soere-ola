package org.inra.ecoinfo.glacpe.dataset.chloro;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.glacpe.dataset.chloro.entity.SequenceChloro;

/**
 *
 * @author ptcherniati
 */
public interface ISequenceChloroDAO extends IDAO<SequenceChloro> {

    /**
     *
     * @param date
     * @param plateformeCode
     * @param siteCode
     * @return
     */
    Optional<SequenceChloro> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate date, String plateformeCode, String siteCode);

}

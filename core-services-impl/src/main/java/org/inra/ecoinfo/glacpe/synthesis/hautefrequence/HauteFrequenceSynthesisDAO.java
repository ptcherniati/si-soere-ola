/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.hautefrequence;

import java.util.stream.Stream;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class HauteFrequenceSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype>{

    /**
     *
     */
    public static String SYNTHESIS_REQUEST = "insert into hautefrequencesynthesisvalue(id, date, idnode, ismean, site, valuefloat, valuestring, variable, siteid, projetid, plateformeid, variableid, profondeurs)\n"
            + "(SELECT \n"
            + "nextval('hibernate_sequence') id,								  \n"
            + "s.date_prelevement date,\n"
            + "dnv.branch_node_id idNode,\n"
            + "false isMean,\n"
            + "rns.path site,\n"
            + "avg(v.valeur) valueFloat,\n"
            + "null valueString,\n"
            + "nddvu.code variable,\n"
            + "rns.id_nodeable siteid,\n"
            + "rnp.id_nodeable projetid,\n"
            + "ss.loc_id plateformeid,\n"
            + "dvu.var_id variableid,\n"
            + "array_agg(distinct dim.zreleve order by dim.zreleve)::::text profondeurs\n"
            + "from valeur_mesure_haute_frequence_vmhautefrequence v\n"
            + "join composite_node_data_set dnv on dnv.realnode= v.id\n" 
            + "join mesure_haute_frequence_mhautefrequence m ON m.mhautefrequence_id = v.mhautefrequence_id\n" 
            + "join sous_sequence_haute_frequence_sshautefrequence ss ON ss.sshautefrequence_id = m.sshautefrequence_id\n" 
            + "join dimension_dim dim ON dim.dim_id = ss.dim_id\n"  
            + "join sequence_haute_frequence_shautefrequence s ON s.shautefrequence_id = ss.shautefrequence_id"
            + "\n"
            + "join realnode rnv ON rnv.id = v.id\n"
            + "join composite_nodeable nddvu on nddvu.id = rnv.id_nodeable\n"
            + "join datatype_variable_unite_glacpe_dvug dvu ON rnv.id_nodeable = vdt_id\n"
            + "join realnode rnd ON rnd.id = rnv.id_parent_node\n"
            + "JOIN realnode rnt ON rnt.id = rnd.id_parent_node\n"
            + "JOIN realnode rns ON rns.id = rnt.id_parent_node\n"
            + "JOIN realnode rnts ON rnts.id = rns.id_parent_node\n"
            + "JOIN realnode rnp ON rnp.id = rnts.id_parent_node\n"
            + "group by s.date_prelevement, rns.path, dnv.branch_node_id, nddvu.code, ss.loc_id, rns.id_nodeable, rnp.id_nodeable, rnp.id_nodeable,  dvu.var_id\n"
            + ")";

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        entityManager.createNativeQuery(SYNTHESIS_REQUEST).executeUpdate();
        return Stream.empty();
    }
    
}

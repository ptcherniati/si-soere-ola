package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class JPAPublicationContenuStomacauxDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_CONTENU_STOMACAUX_BY_IDS = "delete from SequenceContenuStomacaux sce where sce.versionFile = :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_CONTENU_STOMACAUX_BY_IDS = "delete from SousSequenceContenuStomacaux ssce where scontenustomacaux_id in (select id from SequenceContenuStomacaux sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_CONTENU_STOMACAUX_BY_IDS = "delete from MesureContenuStomacaux mce where sscontenustomacaux_id in (select id from SousSequenceContenuStomacaux ssce where scontenustomacaux_id in (select id from SequenceContenuStomacaux sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_CONTENU_STOMACAUX_BY_IDS = "delete from ValeurMesureContenuStomacaux vmce where mcontenustomacaux_id in (select id from MesureContenuStomacaux mce where sscontenustomacaux_id in (select id from SousSequenceContenuStomacaux ssce where scontenustomacaux_id in (select id from SequenceContenuStomacaux sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_CONTENU_STOMACAUX_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_CONTENU_STOMACAUX_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_CONTENU_STOMACAUX_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_CONTENU_STOMACAUX_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            JPAVersionFileDAO.LOGGER.error(e.getMessage());
        }
    }
}

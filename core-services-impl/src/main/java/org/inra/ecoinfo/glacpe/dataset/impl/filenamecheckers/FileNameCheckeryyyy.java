package org.inra.ecoinfo.glacpe.dataset.impl.filenamecheckers;

import java.util.regex.Matcher;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import static org.inra.ecoinfo.glacpe.dataset.impl.filenamecheckers.AbstractGLACPEFileNameChecker.CST_STRING_EMPTY;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckeryyyy extends AbstractGLACPEFileNameChecker {

    private static final String DATE_PATTERN = DateUtil.YYYY;


    @Override
    public String getFilePath(VersionFile version) {
        String currentSite = LacsUtils.getSiteFromDataset(version.getDataset()).getCode();
        if (configuration.getSiteSeparatorForFileNames() != null && !CST_STRING_EMPTY.equals(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(PatternConfigurator.ANCESTOR_SEPARATOR, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(PATTERN_FILE_NAME_PATH,
                LacsUtils.getProjetFromDataset(version.getDataset()).getCode(),
                currentSite,
                LacsUtils.getDatatypeFromDataset(version.getDataset()).getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), getDatePattern()),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), getDatePattern()),
                version.getVersionNumber());
    }

    /**
     *
     * @param dataset
     * @return
     */
    @Override
    public String getFilePath(Dataset dataset) {
        String currentSite = LacsUtils.getSiteFromDataset(dataset).getCode();
        if (configuration.getSiteSeparatorForFileNames() != null && !CST_STRING_EMPTY.equals(configuration.getSiteSeparatorForFileNames())) {
            currentSite = currentSite.replaceAll(PatternConfigurator.ANCESTOR_SEPARATOR, configuration.getSiteSeparatorForFileNames());
        }
        return String.format(PATTERN_FILE_NAME_PATH,
                LacsUtils.getProjetFromDataset(dataset).getCode(),
                LacsUtils.getSiteFromDataset(dataset).getCode(),
                LacsUtils.getDatatypeFromDataset(dataset).getCode(),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), getDatePattern()),
                DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), getDatePattern()),
                dataset.getVersions().size()+1);
    }

    @Override
    protected String getDatePattern() {
        return DATE_PATTERN;
    }

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    @Override
    protected void testDates(VersionFile version, String currentProject, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateyyyy(splitFilename.group(4), splitFilename.group(5));
        } catch (Exception e1) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, DATE_PATTERN, DATE_PATTERN, FILE_FORMAT));
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }

    @Override
    protected void testDates(VersionFile version, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        // Non utilisé
    }
}

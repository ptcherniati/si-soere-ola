/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.typeplateforme.ITypePlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.typeplateforme.TypePlateforme;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<Plateforme> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";

    private static final String MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_CODE_TYPE_PLATEFORME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TYPE_PLATEFORME_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected ITypePlateformeDAO typePlateformeDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected ISiteDAO siteDAO;
    private String[] namesTypesPlateformesPossibles;
    private String[] namesSitesPossibles;

    private Properties propertiesNomFR;
    private Properties propertiesNomEN;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(TypePlateforme.class));

                // On parcourt chaque colonne d'une ligne
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String nom = tokenizerValues.nextToken(true);
                Float latitude = tokenizerValues.nextTokenFloat();
                Float longitude = tokenizerValues.nextTokenFloat();
                Float altitude = tokenizerValues.nextTokenFloat();
                String typePlateformeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                
                String codeSandre = tokenizerValues.nextToken();
                String contexte = tokenizerValues.nextToken();
                
                

                persistPlateforme(errorsReport, siteCode, nom, latitude, longitude, altitude, typePlateformeCode, codeSandre, contexte);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String codeSite = tokenizerValues.nextToken();
                String codePlateforme = Utils.createCodeFromString(tokenizerValues.nextToken());
                mgaServiceBuilder.getRecorder().remove(plateformeDAO.getByNKey(codePlateforme, codeSite).orElseThrow(()->new PersistenceException("Can't found plateforme")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistPlateforme(ErrorsReport errorsReport, String siteCode, String nom, Float latitude, Float longitude, Float altitude, String typePlateformeCode, String codeSandre, String contexte) throws PersistenceException {
        TypePlateforme dbTypePlateforme = retrieveDBTypePlateforme(errorsReport, typePlateformeCode);
        SiteGLACPE dbSite = retrieveDBSite(errorsReport, siteCode);

        if (dbSite != null && dbTypePlateforme != null) {
            Plateforme dbPlateforme = plateformeDAO.getByNKey(nom, dbSite.getCode()).orElse(null);
            createOrUpdatePlateforme(nom, latitude, longitude, altitude, dbTypePlateforme, dbSite, dbPlateforme, codeSandre, contexte);
        }
    }

    private void createOrUpdatePlateforme(String nom, Float latitude, Float longitude, Float altitude, TypePlateforme dbTypePlateforme, SiteGLACPE dbSite, Plateforme dbPlateforme, String codeSandre, String contexte) throws PersistenceException {
        if (dbPlateforme == null) {
            createPlateforme(nom, latitude, longitude, altitude, dbTypePlateforme, dbSite, codeSandre, contexte);

        } else {
            updatePlateforme(latitude, longitude, altitude, dbTypePlateforme, dbSite, dbPlateforme, codeSandre, contexte);
        }
    }

    private void updatePlateforme(Float latitude, Float longitude, Float altitude, TypePlateforme dbTypePlateforme, SiteGLACPE dbSite, Plateforme dbPlateforme, String codeSandre, String contexte) throws PersistenceException {
        dbPlateforme.setLatitude(latitude);
        dbPlateforme.setAltitude(altitude);
        dbPlateforme.setLongitude(longitude);

        dbPlateforme.setTypePlateforme(dbTypePlateforme);
        dbTypePlateforme.getPlateformes().add(dbPlateforme);

        dbPlateforme.setSite(dbSite);
        dbSite.getPlateformes().add(dbPlateforme);
        dbPlateforme.setCodeSandre(codeSandre);
        dbPlateforme.setContexte(contexte);
        plateformeDAO.saveOrUpdate(dbPlateforme);
    }

    private void createPlateforme(String nom, Float latitude, Float longitude, Float altitude, TypePlateforme dbTypePlateforme, SiteGLACPE dbSite, String codeSandre, String contexte) throws PersistenceException {
        Plateforme dbPlateforme = new Plateforme(dbSite, nom, latitude, longitude, altitude);
        dbPlateforme.setTypePlateforme(dbTypePlateforme);
        dbTypePlateforme.getPlateformes().add(dbPlateforme);
        dbSite.getPlateformes().add(dbPlateforme);
        dbPlateforme.setCodeSandre(codeSandre);
        dbPlateforme.setContexte(contexte);
        plateformeDAO.saveOrUpdate(dbPlateforme);
    }

    private TypePlateforme retrieveDBTypePlateforme(ErrorsReport errorsReport, String typePlateformeCode) {
        TypePlateforme typePlateforme = typePlateformeDAO.getByCode(typePlateformeCode).orElse(null);
        if (typePlateforme == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_TYPE_PLATEFORME_NOT_FOUND_IN_DB), typePlateformeCode));
        }
        return typePlateforme;
    }

    private SiteGLACPE retrieveDBSite(ErrorsReport errorsReport, String siteCode) {
        SiteGLACPE site = (SiteGLACPE) siteDAO.getByPath(siteCode).orElse(null);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), siteCode));
        }
        return site;
    }

    private void updateNamesTypesPlateformesPossibles() {
        List<TypePlateforme> typesPlateformes = typePlateformeDAO.getAll(TypePlateforme.class);
        String[] namesTypesPlateformesPossibles = new String[typesPlateformes.size()];
        int index = 0;
        for (TypePlateforme typePlateforme : typesPlateformes) {
            namesTypesPlateformesPossibles[index++] = typePlateforme.getName();
        }
        this.namesTypesPlateformesPossibles = namesTypesPlateformesPossibles;

    }

    private void updateNamesSitesPossibles() {
        List<SiteGLACPE> sites = siteDAO.getAll(SiteGLACPE.class);
        String[] namesSitesPossibles = new String[sites.size()];
        int index = 0;
        for (Site site : sites) {
            namesSitesPossibles[index++] = site.getName();
        }
        this.namesSitesPossibles = namesSitesPossibles;
    }

    /**
     *
     * @param typePlateformeDAO
     */
    public void setTypePlateformeDAO(ITypePlateformeDAO typePlateformeDAO) {
        this.typePlateformeDAO = typePlateformeDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param plateforme
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Plateforme plateforme) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getSite().getName(), namesSitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : propertiesNomFR.get(Utils.createCodeFromString(plateforme.getName())), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : propertiesNomEN.get(Utils.createCodeFromString(plateforme.getName())), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getLatitude() != null ? plateforme.getLatitude().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getLongitude() != null ? plateforme.getLongitude().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getAltitude() != null ? plateforme.getAltitude().toString() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getTypePlateforme().getName(), namesTypesPlateformesPossibles, null, false, false, true));

        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(plateforme == null ? EMPTY_STRING : plateforme.getContexte(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    @Override
    protected List<Plateforme> getAllElements() {
        return plateformeDAO.getAllBy(Plateforme.class, Plateforme::getCode);
    }

    @Override
    protected ModelGridMetadata<Plateforme> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME, Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        updateNamesSitesPossibles();
        updateNamesTypesPlateformesPossibles();
        return super.initModelGridMetadata();
    }

}

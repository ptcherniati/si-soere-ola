/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.variablenorme;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public interface IVariableNormeDAO extends IDAO<VariableNorme> {

    /**
     *
     * @param nom
     * @return
     */
    public Optional<VariableNorme> getByCode(String nom);
}

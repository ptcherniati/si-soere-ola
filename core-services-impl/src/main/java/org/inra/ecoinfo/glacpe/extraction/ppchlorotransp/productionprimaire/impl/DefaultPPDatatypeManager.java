package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl;

import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDAO;
import org.inra.ecoinfo.glacpe.dataset.productionprimaire.IPPDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class DefaultPPDatatypeManager extends MO implements IPPDatatypeManager {

    /**
     *
     */
    protected IPPDAO productionPrimaireDAO;
    /**
     *
     * @param productionPrimaireDAO
     */
    public void setProductionPrimaireDAO(IPPDAO productionPrimaireDAO) {
        this.productionPrimaireDAO = productionPrimaireDAO;
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return productionPrimaireDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    @Override
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return productionPrimaireDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @param selectedVariables
     * @return
     */
    @Override
    public Collection<Float> getAvailableDepths(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates, List<DatatypeVariableUniteGLACPE> selectedVariables) {
        return productionPrimaireDAO.getAvailableDepths(policyManager.getCurrentUser(), selectedPlatforms, selectedDates, selectedVariables);
    }

}

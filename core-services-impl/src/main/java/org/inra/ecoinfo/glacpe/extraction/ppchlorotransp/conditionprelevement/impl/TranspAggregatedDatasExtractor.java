package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.IConditionGeneraleDAO;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.DefaultPPChloroTranspExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class TranspAggregatedDatasExtractor extends AbstractGLACPEAggregatedDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "transpAggregatedDatas";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     */
    protected IConditionGeneraleDAO conditionGeneraleDAO;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
        if (((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE) == null || ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).isEmpty()) {
            ((PPChloroTranspParameter) parameters).getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
        } else {
            ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));
        }
        if (((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS) == null) {
            Map<String, Boolean> resultTrack = new HashMap<String, Boolean>();
            resultTrack.put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
            ((PPChloroTranspParameter) parameters).getParameters().put(DefaultPPChloroTranspExtractor.CST_RESULTS, resultTrack);
        } else {
            ((Map<String, Boolean>) ((PPChloroTranspParameter) parameters).getParameters().get(DefaultPPChloroTranspExtractor.CST_RESULTS)).put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap.get(CST_RESULT_EXTRACTION_CODE) != null);
        }
        ((PPChloroTranspParameter) parameters).getResults().get(CST_RESULT_EXTRACTION_CODE).put(MAP_INDEX_0, resultsDatasMap.get(CST_RESULT_EXTRACTION_CODE));

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        List<DatatypeVariableUniteGLACPE> selectedVariablesList = getDVUs(requestMetadatasMap, PPChloroTranspParameter.TRANSPARENCE);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        List<IntervalDate> intevalDates = getIntervalsDates(requestMetadatasMap);
        List<ValeurConditionGenerale> valeursConditionGenerales = conditionGeneraleDAO.extractValueDatas(policyManager.getCurrentUser(), selectedPlateformes, intevalDates, selectedVariablesList);
        extractedDatasMap.put(CST_RESULT_EXTRACTION_CODE, valeursConditionGenerales);
        if (valeursConditionGenerales == null || valeursConditionGenerales.isEmpty()) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        return extractedDatasMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        //
    }
    /**
     *
     * @return
     */
    public IConditionGeneraleDAO getConditionGeneraleDAO() {
        return conditionGeneraleDAO;
    }

    /**
     *
     * @param conditionGeneraleDAO
     */
    public void setConditionGeneraleDAO(IConditionGeneraleDAO conditionGeneraleDAO) {
        this.conditionGeneraleDAO = conditionGeneraleDAO;
    }
}

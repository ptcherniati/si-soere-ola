/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.groupevariable;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public interface IGroupeVariableDAO extends IDAO<GroupeVariable> {

    /**
     *
     * @param nom
     * @return
     */
    Optional<GroupeVariable> getByCode(String nom);

}

package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ConditionGeneraleDatasExtractor extends AbstractConditionGeneraleDatasExtractor {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "conditionGeneraleDatas";
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";

    @SuppressWarnings("rawtypes")
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        try {
            prepareRequestMetadatas(parameters.getParameters());
            Map<String, List> resultsDatasMap = extractDatas(parameters.getParameters());
            parameters.getResults().put(CST_RESULT_EXTRACTION_CODE, resultsDatasMap);
        } catch (NoExtractionResultException e) {
            throw new BusinessException(String.format(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT), DefaultConditionGeneraleDatatypeManager.CODE_EXTRACTIONTYPE_CONDITION_PRELEVEMENTS), e);
        }
    }
}

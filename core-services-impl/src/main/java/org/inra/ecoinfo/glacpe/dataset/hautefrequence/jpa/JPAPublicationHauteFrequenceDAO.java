package org.inra.ecoinfo.glacpe.dataset.hautefrequence.jpa;

import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.glacpe.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author mylene
 */
public class JPAPublicationHauteFrequenceDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {
    
    private static final String REQUEST_NATIVE_DELETE_SEQUENCE_HAUTEFREQUENCE_BY_IDS = "delete from SequenceHauteFrequence sce where sce.versionFile= :version";
    private static final String REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_HAUTEFREQUENCE_BY_IDS = "delete from SousSequenceHauteFrequence ssce where shautefrequence_id in (select id from SequenceHauteFrequence sce where sce.versionFile = :version)";
    private static final String REQUEST_NATIVE_DELETE_MESURE_HAUTEFREQUENCE_BY_IDS = "delete from MesureHauteFrequence mce where sshautefrequence_id in (select id from SousSequenceHauteFrequence ssce where shautefrequence_id in (select id from SequenceHauteFrequence sce where sce.versionFile = :version))";
    private static final String REQUEST_NATIVE_DELETE_VALEUR_MESURE_HAUTEFREQUENCE_BY_IDS = "delete from ValeurMesureHauteFrequence vmce where mhautefrequence_id in (select id from MesureHauteFrequence mce where sshautefrequence_id in (select id from SousSequenceHauteFrequence ssce where shautefrequence_id in (select id from SequenceHauteFrequence sce where sce.versionFile = :version)))";

    @Override
    public void removeVersion(VersionFile version) throws PersistenceException {
        try {
            Query query = entityManager.createQuery(REQUEST_NATIVE_DELETE_VALEUR_MESURE_HAUTEFREQUENCE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_MESURE_HAUTEFREQUENCE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SOUS_SEQUENCE_HAUTEFREQUENCE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

            query = entityManager.createQuery(REQUEST_NATIVE_DELETE_SEQUENCE_HAUTEFREQUENCE_BY_IDS);
            query.setParameter("version", version);
            query.executeUpdate();

        } catch (Exception e) {
            AbstractJPADAO.LOGGER.error(e.getMessage(), e);
            throw new PersistenceException(e);
        }

    }
    
}

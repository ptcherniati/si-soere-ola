/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.MesureConditionGenerale;
import org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity.ValeurConditionGenerale;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl.TranspRawsDatasExtractor;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author afiocca
 */
public class PPChloroTranspOutputBuilderSandreTrans extends AbstractPPChloroTranspRawsDatasOutputBuilder {
    
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.messages";
    private static final String PROPERTY_MSG_HEADER = "MSG_HEADER_SANDRE";
    private static final String FILENAME_INFO_SUP = "Informations_supplementaires_trans";

    /**
     *
     */
    public static final String TRANSPARENCE = Utils.createCodeFromString("conditions prélèvements");
    
    protected IPlateformeDAO plateformeDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        try {
            Iterator<MesureConditionGenerale> mesuresTrans = resultsDatasMap.get(MAP_INDEX_0).iterator();
            

            DataType datatypeTrans = datatypeDAO.getByCode(TRANSPARENCE).orElseThrow(()->new PersistenceException("datatype not found"));

            
            Map<String, String> variablesTransMapSandreElement = new HashMap<>(); 
            Map<String, String> variablesTransMapSandreContext = new HashMap<>();
            
           
            Map<String, String> sitesMapSandreElement = new HashMap<>(); 
            Map<String, String> sitesMapSandreContext = new HashMap<>(); 
            
            Map<String, String> platformsMapSite = new HashMap<>();
            Map<String, String> platformsMapSandreElement = new HashMap<>(); 
            Map<String, String> platformsMapSandreContext = new HashMap<>(); 
            Map<String, Float> platformsMapLongitude = new HashMap<>(); 
            Map<String, Float> platformsMapLatitude = new HashMap<>(); 
            Map<String, Float> platformsMapAltitude = new HashMap<>(); 
            

            Map<String, File> reminderMap = new HashMap<String, File>();
            File reminderFile = buildOutputFile(FILENAME_INFO_SUP, EXTENSION_CSV);
            PrintStream printStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, PROPERTY_MSG_HEADER));

            while (mesuresTrans.hasNext()) {

                final MesureConditionGenerale mesureTrans = mesuresTrans.next();

                Iterator<ValeurConditionGenerale> valeursTrans = mesureTrans.getValeurs().iterator();

                while (valeursTrans.hasNext()) {
                    
                    ValeurConditionGenerale valeurMesureTrans = valeursTrans.next();

                    final String nameVariableTrans = ((DatatypeVariableUniteGLACPE)valeurMesureTrans.getRealNode().getNodeable()).getVariable().getCode();
                    final String codeSandreElementVariableTrans = ((DatatypeVariableUniteGLACPE)valeurMesureTrans.getRealNode().getNodeable()).getVariable().getCodeSandre();  
                    final String codeSandreContextVariableTrans = ((DatatypeVariableUniteGLACPE)valeurMesureTrans.getRealNode().getNodeable()).getVariable().getContexte();



                    final String nameSite = valeurMesureTrans.getMesure().getSite().getCode();
                    final String codeSandreElementSite = ((SiteGLACPE) valeurMesureTrans.getMesure().getSite()).getCodeSandrePe();
                    final String codeSandreContextSite = ((SiteGLACPE) valeurMesureTrans.getMesure().getSite()).getCodeSandreMe();
    
                    final String namePlatform = valeurMesureTrans.getMesure().getPlateforme().getCode(); 
                    final String codeSandreElementPlatform = valeurMesureTrans.getMesure().getPlateforme().getCodeSandre();
                    final String codeSandreContextPlatform = valeurMesureTrans.getMesure().getPlateforme().getContexte();
                    final Float longitudePlatform = valeurMesureTrans.getMesure().getPlateforme().getLongitude();
                    final Float latitudePlatform = valeurMesureTrans.getMesure().getPlateforme().getLatitude();
                    final Float altitudePlatform = valeurMesureTrans.getMesure().getPlateforme().getAltitude();
                    
                    variablesTransMapSandreElement.put(nameVariableTrans, codeSandreElementVariableTrans);
                    variablesTransMapSandreContext.put(nameVariableTrans, codeSandreContextVariableTrans);
                 
                    sitesMapSandreElement.put(nameSite, codeSandreElementSite);
                    sitesMapSandreContext.put(nameSite, codeSandreContextSite);
                    
                    platformsMapSite.put(namePlatform,nameSite);
                    platformsMapSandreElement.put(namePlatform, codeSandreElementPlatform);
                    platformsMapSandreContext.put(namePlatform, codeSandreContextPlatform);
                    platformsMapLongitude.put(namePlatform, longitudePlatform);
                    platformsMapLatitude.put(namePlatform, latitudePlatform);
                    platformsMapAltitude.put(namePlatform, altitudePlatform);
                    
                }
                mesuresTrans.remove();
            }
            

            mapSitesSandreCSV(sitesMapSandreElement, sitesMapSandreContext,printStream);
            
            mapPlatformsCoordCSV(platformsMapSite,platformsMapSandreElement,platformsMapSandreContext, platformsMapLongitude, platformsMapLatitude, platformsMapAltitude, printStream);


            mapVariablesSandreCSV(variablesTransMapSandreElement,variablesTransMapSandreContext, printStream);
            
            mapUnitSandreCSV(variablesTransMapSandreElement, datatypeTrans, printStream);


            reminderMap.put(FILENAME_INFO_SUP, reminderFile);

            printStream.flush();
            printStream.close();

            return reminderMap;
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    
    
    /**
     * 
     * @param variablesMapSandreElement
     * @param variablesMapSandreContext
     * @param printStream 
     */
    private void mapVariablesSandreCSV(Map<String, String> variablesMapSandreElement, Map<String, String> variablesMapSandreContext, PrintStream printStream) {
        
        for (String name : variablesMapSandreElement.keySet()) {
            
            if (variablesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"variable\";\"%s\";\"%s\";\"%s\";", name, variablesMapSandreElement.get(name),variablesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"variable\";\"%s\";", name));
            }
        }
    }

    /**
     * 
     * @param sitesMapSandreElem
     * @param sitesMapSandreCont
     * @param printStream 
     */
    private void mapSitesSandreCSV(Map<String, String> sitesMapSandreElement, Map<String, String> sitesMapSandreContext, PrintStream printStream) {
        for (String name : sitesMapSandreElement.keySet()) {
            if (sitesMapSandreElement.get(name) != null) {
                printStream.println(String.format("\"site\";\"%s\";\"%s\";\"%s\";", name, sitesMapSandreElement.get(name),sitesMapSandreContext.get(name)));
            } else {
                printStream.println(String.format("\"site\";\"%s\";", name));
            }
        }
    }
    /**
     * 
     * @param sitesMapSandreElem
     * @param platformsMapLongitude
     * @param platformsMapLatitude
     * @param platformsMapAltitude
     * @param printStream 
     */
    private void mapPlatformsCoordCSV(Map<String, String> platformsMapSite, Map<String, String> platformsMapSandreElement, Map<String, String> platformsMapSandreContext, Map<String, Float> platformsMapLongitude, Map<String, Float> platformsMapLatitude, Map<String, Float> platformsMapAltitude, PrintStream printStream) {
        
        for (String name : platformsMapLongitude.keySet()) {

            if (platformsMapLongitude.get(name) != null) {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";\"%s\";\"%s\";\"%s\";\"%s\";\"%s\";", name, platformsMapSite.get(name),platformsMapSandreElement.get(name),platformsMapSandreContext.get(name), platformsMapLongitude.get(name), platformsMapLatitude.get(name), platformsMapAltitude.get(name)));
            } else {
                printStream.println(String.format("\"plateforme(site)\";\"%s(%s)\";", name, platformsMapSite.get(name)));
            }
        }
    }

    /**
     *
     * @param variablesMap 
     * @param datatypeChimie
     * @param printStream
     * @throws PersistenceException
     */
    private void mapUnitSandreCSV(Map<String, String> variablesMap, DataType datatype, PrintStream printStream) throws PersistenceException {

        for (String name : variablesMap.keySet()) {

            DatatypeVariableUniteGLACPE datatypeVariableUnite = datatypeVariableUniteDAO.getByDatatypeAndVariable(datatype.getCode(), Utils.createCodeFromString(name)).orElseThrow(()->new PersistenceException("variable not found"));

            
            if (datatypeVariableUnite != null){

                 if (datatypeVariableUnite.getCodeSandre() != null) {
                    printStream.println(String.format("\"Unité (variable)\";\"%s (%s)\";\"%s\";\"%s\"", datatypeVariableUnite.getUnite().getCode(), name, datatypeVariableUnite.getCodeSandre(),datatypeVariableUnite.getContexte()));
                } else {
                    printStream.println(String.format("\"Unité (variable)\";\"%s (%s)\"", datatypeVariableUnite.getUnite().getCode(), name));
                }
            }
        }
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {    
       ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

}
                  

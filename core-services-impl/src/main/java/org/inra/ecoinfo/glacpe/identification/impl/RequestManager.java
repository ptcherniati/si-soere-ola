/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.identification.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import org.inra.ecoinfo.glacpe.identification.entity.RightRequestGLACPE;
import org.inra.ecoinfo.identification.IRequestManager;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.identification.impl.AbstractRequestManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ptcherniati
 */
public class RequestManager extends AbstractRequestManager<RightRequestGLACPE> implements IRequestManager<RightRequestGLACPE> {

    /**
     * The Constant PROPERTY_MSG_RIGHT_DEMAND.
     */
    public static final String PROPERTY_MSG_RIGHT_DEMAND = "PROPERTY_MSG_RIGHT_DEMAND";

    /**
     *
     */
    public static final String BUNDLE_NAME = "org.inra.ecoinfo.identification.messages";

    /**
     *
     */
    public static final String LOCAL_BUNDLE_NAME = "org.inra.ecoinfo.glacpe.identification.message";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT.
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT = "PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST = "PROPERTY_MSG_NEW_RIGHT_REQUEST";
    /**
     * The Constant PROPERTY_MSG_NEW_RIGHT_REQUEST
     */
    public static final String PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY = "PROPERTY_MSG_NEW_RIGHT_REQUEST_BODY";

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(RequestManager.class);

    /**
     * 
     * 
     * PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT=<html><head> <meta http-equiv="Content-Type" 
     * content="text/html;charset=utf-8"/><style>table {max-width:80%%} table td{width:50%%} 
     * table, table td, table tr{border:1px solid #999;border-collapse:collapse} table 
     * td{padding: 5px;}</style></head><body><p>Bonjour %s %s,</p> \
     * <p>L'utilisateur <span style="font-weight:bold">%s</span> a envoyé une demande avec les informations suivantes:</p>%s</body></html>
     * 
     * 
     * 
     * @param admin
     * @param utilisateur
     * @param rightRequest
     * @return
     */
    @Override
    public String getMailMessageForRequest(Utilisateur admin, Utilisateur utilisateur, RightRequestGLACPE rightRequest) {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(LOCAL_BUNDLE_NAME, utilisateur.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(utilisateur.getLanguage()));
        String message = resourceBundle.getString(PROPERTY_MSG_NEW_RIGHT_REQUEST_MAIL_CONTENT);        
        
        message = String.format(message,
                                admin.getPrenom(), admin.getNom(),
                                utilisateur.getLogin(),rightRequest.toString());
        return message;


    }

    /**
     * @param rightRequest
     * @param utilisateur
     */
    @Override
    protected void setRightRequestUser(RightRequestGLACPE rightRequest, Utilisateur utilisateur) {
        rightRequest.setUser(utilisateur);
    }

    /**
     * @param rightRequest
     * @return
     */
    @Override
    protected Optional<Utilisateur> getUtilisateurForRequest(RightRequestGLACPE rightRequest) {
        return Optional.ofNullable(rightRequest).map(rr -> rr.getUser());
    }

}

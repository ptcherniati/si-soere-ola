package org.inra.ecoinfo.glacpe.extraction.sondemulti.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.ExtractionParametersVO;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class SondeMultiOutputsBuildersResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {

    private static final String EXTRACTION = "extraction";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     *
     */
    protected static final String SEPARATOR_TEXT = "_";

    /**
     *
     */
    protected static final String EXTENSION_ZIP = ".zip";

    /**
     *
     */
    protected IOutputBuilder sondeMultiRawsDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder sondeMultiAggregatedDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder requestReminderOutputBuilder;

    protected IOutputBuilder sondeMultiOutputBuilderSandre;
    /**
     * The configuration.
     */
    private IFileCompConfiguration fileCompConfiguration;

    private void addOutputBuilderByCondition(Boolean condition, IOutputBuilder outputBuilder, List<IOutputBuilder> outputsBuilders) {
        if (condition && !outputsBuilders.contains(outputBuilder)) {
            outputsBuilders.add(outputBuilder);
        }
    }

    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        List<IOutputBuilder> outputsBuilders = new LinkedList<IOutputBuilder>();
        DatasRequestParamVO datasRequestParamVO = (DatasRequestParamVO) metadatasMap.get(DatasRequestParamVO.class.getSimpleName());

        addOutputBuilderByCondition(datasRequestParamVO.getRawData(), sondeMultiRawsDatasOutputBuilder, outputsBuilders);

        for (ExtractionParametersVO extractionParametersVO : datasRequestParamVO.getExtractionParameters()) {
            if (extractionParametersVO.getValueMin()) {
                addOutputBuilderByCondition(true, sondeMultiAggregatedDatasOutputBuilder, outputsBuilders);
                break;
            }
            if (extractionParametersVO.getValueMax()) {
                addOutputBuilderByCondition(true, sondeMultiAggregatedDatasOutputBuilder, outputsBuilders);
                break;
            }
            if (extractionParametersVO.getTargetValues() != null && extractionParametersVO.getTargetValues().size() > 0) {
                addOutputBuilderByCondition(true, sondeMultiAggregatedDatasOutputBuilder, outputsBuilders);
                break;
            }
        }

        outputsBuilders.add(requestReminderOutputBuilder);

        addOutputBuilderByCondition(datasRequestParamVO.getRawData(), sondeMultiOutputBuilderSandre, outputsBuilders);

        return outputsBuilders;
    }

    /**
     *
     * @param sondeMultiRawsDatasOutputBuilder
     */
    public void setSondeMultiRawsDatasOutputBuilder(IOutputBuilder sondeMultiRawsDatasOutputBuilder) {
        this.sondeMultiRawsDatasOutputBuilder = sondeMultiRawsDatasOutputBuilder;
    }

    /**
     *
     * @param sondeMultiOutputBuilderSandre
     */
    public void setSondeMultiOutputBuilderSandre(IOutputBuilder sondeMultiOutputBuilderSandre) {
        this.sondeMultiOutputBuilderSandre = sondeMultiOutputBuilderSandre;
    }

    /**
     *
     * @param sondeMultiAggregatedDatasOutputBuilder
     */
    public void setSondeMultiAggregatedDatasOutputBuilder(IOutputBuilder sondeMultiAggregatedDatasOutputBuilder) {
        this.sondeMultiAggregatedDatasOutputBuilder = sondeMultiAggregatedDatasOutputBuilder;
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     */
    protected void buidOutputFile(final IParameter parameters) throws BusinessException {
        final Set<String> datatypeNames = new HashSet<>();
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    @SuppressWarnings("unchecked")
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        NoExtractionResultException noDataToExtract = null;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            List<IOutputBuilder> outputBuilders = resolveOutputsBuilders(parameters.getParameters());
            for (IOutputBuilder outputBuilder : outputBuilders) {
                try {
                    outputBuilder.buildOutput(parameters);
                } catch (NoExtractionResultException e) {
                    noDataToExtract = e;
                }
            }
            parameters.getParameters().put(AbstractGLACPEExtractor.CST_RESULTS, parameters.getResults());
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
        } catch (FileNotFoundException e1) {
            throw new BusinessException("FileNotFoundException", e1);
        } catch (IOException e) {
            throw new BusinessException("IOException", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }
    
    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }
    
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }
    
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

}

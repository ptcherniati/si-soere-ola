package org.inra.ecoinfo.glacpe.extraction.impl;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.site.Site;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEAggregatedDatasExtractor extends AbstractGLACPEExtractor{

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_AGGREGATED = "aggregated";


    /**
     *
     * @param variablesAggregatedDatas
     */
    protected void sortVariablesAggregatedDatas(List<VariableAggregatedDatas> variablesAggregatedDatas) {
        Collections.sort(variablesAggregatedDatas, new Comparator<VariableAggregatedDatas>() {

            @Override
            public int compare(VariableAggregatedDatas o1, VariableAggregatedDatas o2) {
                String str1 = o1.getSiteName().concat(o1.getPlateformeName());
                String str2 = o2.getSiteName().concat(o2.getPlateformeName());
                return str1.compareTo(str2);
            }

        });
    }

    /**
     *
     * @param valeursMesureDates
     */
    protected void sortValeursMesuresDatesByDepth(List<IGLACPEAggregateData> valeursMesureDates) {
        Collections.sort(valeursMesureDates, new Comparator<IGLACPEAggregateData>() {

            @Override
            public int compare(IGLACPEAggregateData o1, IGLACPEAggregateData o2) {
                return o1.getDepth().compareTo(o2.getDepth());
            }
        });
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<Site, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>> buildValeursMesuresReorderedByVariablesAndDates(List<IGLACPEAggregateData> valeursMesures) {
        SortedMap<Projet, SortedMap<Site, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedBySitesPlateformesVariablesDates = new TreeMap<Projet, SortedMap<Site, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>>();

        for (IGLACPEAggregateData valeurMesure : valeursMesures) {
            valeursMesuresReorderedBySitesPlateformesVariablesDates
                    .computeIfAbsent(valeurMesure.getProjet(),k-> new TreeMap<Site, SortedMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>>())
                    .computeIfAbsent(valeurMesure.getSite(),k-> new TreeMap<Plateforme, SortedMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>>())
                    .computeIfAbsent(valeurMesure.getPlateforme(),k-> new TreeMap<RealNode, SortedMap<LocalDate, SortedSet<IGLACPEAggregateData>>>())
                    .computeIfAbsent(valeurMesure.getRealNode(),k-> new TreeMap<LocalDate, SortedSet<IGLACPEAggregateData>>())
                    .computeIfAbsent(valeurMesure.getDate(),k-> new TreeSet<IGLACPEAggregateData>())
                    .add(valeurMesure);
        }
        return valeursMesuresReorderedBySitesPlateformesVariablesDates;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        // TODO Auto-generated method stub

    }

}

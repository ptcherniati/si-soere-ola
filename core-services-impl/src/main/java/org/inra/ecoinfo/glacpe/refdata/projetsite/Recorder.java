/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.projetsite;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.ISiteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProjetSite> {

    private static final String BUNDLE_NAME = "org.inra.ecoinfo.glacpe.refdata.messages";
    private static final String PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB";
    private static final String PROPERTY_MSG_ERROR_CODE_TYPESITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_TYPESITE_NOT_FOUND_IN_DB";
    private static final String PROPERTY_MSG_BAD_DATE_FORMAT = "PROPERTY_MSG_BAD_DATE_FORMAT";
    private static final String MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB";

    /**
     *
     */
    protected ISiteDAO siteDAO;

    protected IProjetSiteDAO projetSiteDAO;
    protected IProjetDAO projetDAO;
    protected ISiteGLACPEDAO siteGLACPEDAO;

    private String[] namesSitesPossibles;
    private String[] namesProjetsPossibles;


    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();

        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String projetNom = tokenizerValues.nextToken();
                String projetCode = Utils.createCodeFromString(projetNom);
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String commanditaire = tokenizerValues.nextToken();
                String dateDebutString = tokenizerValues.nextToken();
                LocalDate dateDebut = getDate(errorsReport, dateDebutString, datasetDescriptor.getColumns().get(3).getFormatType());
                String dateFinString = tokenizerValues.nextToken();
                LocalDate dateFin = getDate(errorsReport, dateDebutString, datasetDescriptor.getColumns().get(4).getFormatType());
                String commentaire = tokenizerValues.nextToken();
                persistProjetSite(errorsReport, projetCode, siteCode, commanditaire, dateDebut, dateFin, commentaire, projetNom);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (DateTimeParseException e) {
            throw new BusinessException(e);
        }
    }
    
    LocalDate getDate(ErrorsReport errorsReport, String date, String formatDate){
        try{
            return Strings.isNullOrEmpty(date) ? null: DateUtil.readLocalDateFromText(formatDate, date);
        }catch(DateTimeParseException e){
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, PROPERTY_MSG_BAD_DATE_FORMAT), date, formatDate));
        }
        return null;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String projetCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String siteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                ProjetSite ps = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode)
                        .orElseThrow(()->new PersistenceException("can't retrieve projet site"));
                projetSiteDAO.remove(ps);
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistProjetSite(ErrorsReport errorsReport, String projetCode, String siteCode, String commanditaire, LocalDate dateDebut, LocalDate dateFin, String commentaire, String projetNom) throws PersistenceException {
        SiteGLACPE site = retrieveDBSite(errorsReport, siteCode);
        Projet projet = retrieveDBProjet(errorsReport, projetCode);

        if (site != null && projet != null) {
            ProjetSite projetSite = retrieveOrCreateProjetSite(projetCode, siteCode, site, projet);
            projetSite.setCommanditaire(commanditaire);
            projetSite.setCommentaire(commentaire);
            projetSite.setDebut(dateDebut);
            projetSite.setFin(dateFin);
        }
    }

    private ProjetSite retrieveOrCreateProjetSite(String projetCode, String siteCode, SiteGLACPE site, Projet projet) throws PersistenceException {
        ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode).orElse(null);
        if (projetSite == null) {
            projetSite = new ProjetSite(site, projet);
            projetSiteDAO.saveOrUpdate(projetSite);
        }
        return projetSite;
    }

    private Projet retrieveDBProjet(ErrorsReport errorsReport, String projetCode) throws PersistenceException {
        Projet projet = projetDAO.getByCode(projetCode).orElse(null);
        if (projet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_PROJET_NOT_FOUND_IN_DB), projetCode));
        }
        return projet;
    }

    private SiteGLACPE retrieveDBSite(ErrorsReport errorsReport, String siteCode) throws PersistenceException {
        SiteGLACPE site = (SiteGLACPE) siteDAO.getByPath(siteCode).orElse(null);
        if (site == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_NAME, MSG_ERROR_CODE_SITE_NOT_FOUND_IN_DB), siteCode));
        }
        return site;
    }

    private void updateNamesSitesPossibles() throws PersistenceException {
        this.namesSitesPossibles= siteDAO.getAllBy(SiteGLACPE.class, SiteGLACPE::getName)
                .stream()
                .map(SiteGLACPE::getName)
                .collect(Collectors.toList())
                .toArray(new String[0]);
    }

    private void updateNamesProjetsPossibles() throws PersistenceException {
        this.namesProjetsPossibles = projetDAO.getAllBy(Projet.class, Projet::getName)
                .stream()
                .map(Projet::getName)
                .collect(Collectors.toList())
                .toArray(new String[0]);
    }

    private String buildStringDateDebut(ProjetSite projetSite) {
        String dateDebut = "";
        if (projetSite != null && projetSite.getDebut() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(projetSite.getDebut(), datasetDescriptor.getColumns().get(3).getFormatType());
        }
        return dateDebut;
    }

    private String buildStringDateFin(ProjetSite projetSite) {
        String dateFin = "";
        if (projetSite != null && projetSite.getFin() != null) {
            dateFin = DateUtil.getUTCDateTextFromLocalDateTime(projetSite.getDebut(), datasetDescriptor.getColumns().get(4).getFormatType());
        }
        return dateFin;
    }

    @SuppressWarnings("unused")
    private Site retrieveSiteFromProjetSite(ProjetSite projetSite) {
        Site site = null;
        if (projetSite != null) {
            site = projetSite.getSite();
        }
        return site;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProjetSite projetSite) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSite == null ? EMPTY_STRING : projetSite.getProjet().getName(), namesProjetsPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSite == null || projetSite.getSite() == null ? EMPTY_STRING : projetSite.getSite().getName(), namesSitesPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(projetSite == null ? EMPTY_STRING : projetSite != null ? projetSite.getCommanditaire() : "", ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        String dateDebut = projetSite == null ? EMPTY_STRING : buildStringDateDebut(projetSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(dateDebut, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        String dateFin = projetSite == null ? EMPTY_STRING : buildStringDateFin(projetSite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(dateFin, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(projetSite == null ? EMPTY_STRING : projetSite.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    protected List<ProjetSite> getAllElements(){
        return projetSiteDAO.getAll(ProjetSite.class)
                .stream()
                .sorted(((a,b)->a.getSite().getName().compareTo(b.getSite().getName())))
                .sorted(((a,b)->a.getProjet().getName().compareTo(b.getProjet().getName())))
                .collect(Collectors.toList());
    }

    @Override
    protected ModelGridMetadata<ProjetSite> initModelGridMetadata() {
        try {
            updateNamesProjetsPossibles();
            updateNamesSitesPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();
    }
}

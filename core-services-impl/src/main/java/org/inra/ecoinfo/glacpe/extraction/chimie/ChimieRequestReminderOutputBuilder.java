package org.inra.ecoinfo.glacpe.extraction.chimie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDAO;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGlacpeRequestReminder;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ChimieRequestReminderOutputBuilder extends AbstractGlacpeRequestReminder {

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "chimieRequestReminder";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.messages";

    private static final String MSG_HEADER = "PROPERTY_MSG_HEADER";

    /**
     *
     */
    protected IChimieDAO chimieDAO;

    /**
     *
     * @param chimieDAO
     */
    public void setChimieDAO(IChimieDAO chimieDAO) {
        this.chimieDAO = chimieDAO;
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        return getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_HEADER);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {

        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_REMINDER, EXTENSION_TXT);
        PrintStream reminderPrintStream;

        try {

            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();

            printPlateformesSummary(requestMetadatasMap, reminderPrintStream);
            printVariablesSummary(requestMetadatasMap, reminderPrintStream);
            AbstractGLACPEExtractor.getDepthsRequestParam(requestMetadatasMap).buildSummary(reminderPrintStream);
            AbstractGLACPEExtractor.getDatasRequestParam(requestMetadatasMap).buildSummary(reminderPrintStream);
            printDatesSummary(requestMetadatasMap, reminderPrintStream);
            printComment(reminderPrintStream, requestMetadatasMap);

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderMap.put(FILENAME_REMINDER, reminderFile);

        reminderPrintStream.flush();
        reminderPrintStream.close();

        return reminderMap;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;
    }

}

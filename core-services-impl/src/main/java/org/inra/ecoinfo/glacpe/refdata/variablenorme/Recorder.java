/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.variablenorme;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariableNorme> {

    /**
     *
     */
    protected IVariableNormeDAO variableNormeDAO;

    /**
     * @param parser
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String definition = tokenizerValues.nextToken();

                VariableNorme dbVariableNorme = variableNormeDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);

                // Enregistre une norme de variable uniquement si elle n'existe
                // pas en BD ou
                // bien si elle est considérée comme une mise à jour
                if (dbVariableNorme == null) {
                    variableNormeDAO.saveOrUpdate(new VariableNorme(nom, definition));

                } else {
                    dbVariableNorme.setDefinition(definition);
                }
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                variableNormeDAO.remove(variableNormeDAO.getByCode(code)
                .orElseThrow(()->new PersistenceException("can't retrieve VariableNorme")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param variableNormeDAO
     */
    public void setVariableNormeDAO(IVariableNormeDAO variableNormeDAO) {
        this.variableNormeDAO = variableNormeDAO;
    }

    /**
     *
     * @param variableNorme
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(VariableNorme variableNorme) {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variableNorme == null ? EMPTY_STRING : variableNorme.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variableNorme == null ? EMPTY_STRING : variableNorme.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        return lineModelGridMetadata;
    }

    @Override
    protected List<VariableNorme> getAllElements() {
        return variableNormeDAO.getAllBy(VariableNorme.class, VariableNorme::getCode);
    }

}

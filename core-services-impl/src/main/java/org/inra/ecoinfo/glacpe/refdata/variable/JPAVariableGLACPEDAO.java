package org.inra.ecoinfo.glacpe.refdata.variable;

import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.refdata.variable.JPAVariableDAO;

/**
 *
 * @author ptcherniati
 */
public class JPAVariableGLACPEDAO extends JPAVariableDAO implements IVariableGLACPEDAO {

    /**
     *
     * @return
     */
    @Override
    public List<VariableGLACPE> getAllQualitativeValues() {
        CriteriaQuery<VariableGLACPE> query = builder.createQuery(VariableGLACPE.class);
        Root<VariableGLACPE> variableGLACPE = query.from(VariableGLACPE.class);
        query
                .select(variableGLACPE)
                .where(builder.equal(variableGLACPE.get(VariableGLACPE_.IS_QUALITATIVE), true));
        return getResultList(query);
    }

}

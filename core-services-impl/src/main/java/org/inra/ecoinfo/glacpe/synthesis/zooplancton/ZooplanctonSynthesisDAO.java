/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.synthesis.zooplancton;

import java.util.stream.Stream;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class ZooplanctonSynthesisDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    /**
     *
     */
    public static String SYNTHESIS_REQUEST = "insert into zooplanctonsynthesisvalue(id, date, idnode, ismean, site, valuefloat, valuestring, variable, siteid, projetid, plateformeid, variableid, profondeurs, taxons)\n"
            + "(SELECT \n"
            + "            nextval('hibernate_sequence') id,            "
            + "            m.date,\n"
            + "                        dnv.branch_node_id idNode,\n"
            + "                        false isMean,\n"
            + "                        rns.path site,\n"
            + "                        avg(v.\"value\") valueFloat,\n"
            + "                        null valueString,\n"
            + "                        nddvu.code variable,\n"
            + "                        rns.id_nodeable siteid,\n"
            + "                        rnp.id_nodeable projetid,\n"
            + "						pla.loc_id,\n"
            + "                        dvu.var_id variableid,\n"
            + "                        array_agg(distinct m.profondeur_max order by m.profondeur_max)::::text profondeurs,\n"
            + "                        array_agg(distinct tax.code order by tax.code)::::text profondeurs\n"
            + "                        from data_zooplancton_dz v\n"
            + "                        join composite_node_data_set dnv on dnv.realnode= v.id\n"
            + "                        join metadata_zooplancton_mtdz m ON m.mtdz_id = v.mtdz_id\n"
            + "                        JOIN taxon_tax tax ON tax.tax_id = v.tax_id\n"
            + "                        join realnode rnv ON rnv.id = v.id\n"
            + "                        join composite_nodeable nddvu on nddvu.id = rnv.id_nodeable\n"
            + "                        join datatype_variable_unite_glacpe_dvug dvu ON rnv.id_nodeable = vdt_id\n"
            + "                        join realnode rnd ON rnd.id = rnv.id_parent_node\n"
            + "                        JOIN realnode rnt ON rnt.id = rnd.id_parent_node\n"
            + "                        JOIN realnode rns ON rns.id = rnt.id_parent_node\n"
            + "						join plateforme_pla  pla ON pla.id = rns.id_nodeable\n"
            + "                        JOIN realnode rnts ON rnts.id = rns.id_parent_node\n"
            + "                        JOIN realnode rnp ON rnp.id = rnts.id_parent_node\n"
            + "                        group by m.date, rns.path, pla.loc_id, dnv.branch_node_id, nddvu.code, rns.id_nodeable, rnp.id_nodeable, rnp.id_nodeable,  dvu.var_id"
            + ")";

    /**
     *
     * @return
     */
    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        entityManager.createNativeQuery(SYNTHESIS_REQUEST).executeUpdate();
        return Stream.empty();
    }

}

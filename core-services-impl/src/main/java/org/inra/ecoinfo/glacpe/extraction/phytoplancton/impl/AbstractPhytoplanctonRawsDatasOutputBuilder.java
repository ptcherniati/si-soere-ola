package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.IPhytoplanctonDAO;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractPhytoplanctonRawsDatasOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.extraction.phytoplancton.messages";
    protected static final String SUFFIX_RAW_DATA = "raw";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";
    protected static final String BIOVOLUME_DE_L_ESPECE = "biovolume_de_l_espece_dans_l_echantillon";

    /**
     *
     */
    protected IPhytoplanctonDAO phytoplanctonDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    /**
     *
     * @param phytoplanctonDAO
     */
    public void setPhytoplanctonDAO(IPhytoplanctonDAO phytoplanctonDAO) {
        this.phytoplanctonDAO = phytoplanctonDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PhytoplanctonRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    /**
     *
     * @param sumBiovolume
     * @param mesuresPhytoplancton
     */
    protected void prepareBiovolumeSum(SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>>> sumBiovolume, List<MesurePhytoplancton> mesuresPhytoplancton) {
        for (MesurePhytoplancton mesurePhytoplancton : mesuresPhytoplancton) {
            reccursiveSumTaxon(sumBiovolume, mesurePhytoplancton.getTaxon(), mesurePhytoplancton);
        }
    }

    private void reccursiveSumTaxon(SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>>> sumBiovolume, Taxon taxon, MesurePhytoplancton mesurePhyto) {
        final LocalDate date = mesurePhyto.getSousSequence().getSequence().getDatePrelevement();
        sumBiovolume
                .computeIfAbsent(LacsUtils.getProjetFromDataset(mesurePhyto.getSousSequence().getSequence().getVersionFile().getDataset()), k -> new TreeMap<>())
                .computeIfAbsent(LacsUtils.getSiteFromDataset(mesurePhyto.getSousSequence().getSequence().getVersionFile().getDataset()), k -> new TreeMap<>())
                .computeIfAbsent(date, k -> new TreeMap<>())
                .computeIfAbsent(mesurePhyto.getSousSequence().getPlateforme(), k -> new TreeMap<>())
                .computeIfAbsent(taxon.getId(), k -> new TreeSet<>())
                .add(mesurePhyto);

        if (taxon.getTaxonParent() != null) {
            reccursiveSumTaxon(sumBiovolume, taxon.getTaxonParent(), mesurePhyto);
        }

    }

    /**
     *
     * @param mesuresPhytoplancton
     * @return
     */
    protected Float sumBiovolume(SortedSet<MesurePhytoplancton> mesuresPhytoplancton) {
        Float biovolume = 0f;
        for (MesurePhytoplancton mesurePhytoplancton : mesuresPhytoplancton) {
            for (ValeurMesurePhytoplancton valeurPhyto : mesurePhytoplancton.getValeurs()) {
                if (((DatatypeVariableUniteGLACPE) valeurPhyto.getRealNode().getNodeable()).getVariable().getCode().equals(BIOVOLUME_DE_L_ESPECE)) {
                    biovolume += valeurPhyto.getValeur();
                }
            }
        }
        return biovolume;
    }
}

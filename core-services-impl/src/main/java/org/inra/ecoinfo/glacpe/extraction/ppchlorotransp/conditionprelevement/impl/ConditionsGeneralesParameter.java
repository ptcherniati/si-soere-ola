package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl;

import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;

/**
 *
 * @author ptcherniati
 */
public class ConditionsGeneralesParameter extends DefaultParameter implements IParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_CONDITIONS_PRELEVEMENTS = "conditions_prelevements";
    /**
     *
     */
    public static final String CONDITIONS_PRELEVEMENTS = "conditions_prelevements";

    /**
     *
     * @param map
     */
    public ConditionsGeneralesParameter(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_CONDITIONS_PRELEVEMENTS;
    }
}

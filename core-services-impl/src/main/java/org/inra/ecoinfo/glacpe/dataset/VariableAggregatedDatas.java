package org.inra.ecoinfo.glacpe.dataset;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;


/**
 * Cette classe aggrège quelques statistiques liées à une variable donnée dans un contexte de type de données particulier
 * 
 * @author antoine schellenberger *
 */
public class VariableAggregatedDatas implements Comparable<VariableAggregatedDatas>{

    private String variableName;
    private Integer displayOrder;
    private String siteName;
    private String plateformeName;
    private OutilsMesure outilsMesure;
    private OutilsMesure outilsPrelevement;
    private Date heure;

    private AggregatedData minAggregatedData = new AggregatedData();
    private AggregatedData maxAggregatedData = new AggregatedData();
    private AggregatedData averageAggregatedData = new AggregatedData();
    private List<AggregatedData> targetAggregatedDatas = new LinkedList<AggregatedData>();

    /**
     *
     * @param siteName
     * @param plateformeName
     * @param string2
     * @param variableName
     * @param intgr
     * @param displayOrder
     */
    public VariableAggregatedDatas(String siteName, String plateformeName, String variableName, Integer displayOrder) {
        super();
        this.variableName = variableName;
        this.displayOrder = displayOrder;
        this.siteName = siteName;
        this.plateformeName = plateformeName;
    }

    /**
     *
     * @param siteName
     * @param string1
     * @param plateformeName
     * @param string2
     * @param variableName
     */
    public VariableAggregatedDatas(String siteName, String plateformeName, String variableName) {
        super();
        this.variableName = variableName;
        this.siteName = siteName;
        this.plateformeName = plateformeName;
    }

    /**
     *
     * @return
     */
    public String getVariableName() {
        return variableName;
    }

    /**
     *
     * @return
     */
    public Integer getDisplayOrder() {
        return displayOrder;
    }


    /**
     *
     * @return
     */
    public AggregatedData getMinAggregatedData() {
        return minAggregatedData;
    }

    /**
     *
     * @param minAggregatedData
     */
    public void setMinAggregatedData(AggregatedData minAggregatedData) {
        this.minAggregatedData = minAggregatedData;
    }

    /**
     *
     * @return
     */
    public AggregatedData getMaxAggregatedData() {
        return maxAggregatedData;
    }

    /**
     *
     * @param maxAggregatedData
     */
    public void setMaxAggregatedData(AggregatedData maxAggregatedData) {
        this.maxAggregatedData = maxAggregatedData;
    }

    /**
     *
     * @return
     */
    public AggregatedData getAverageAggregatedData() {
        return averageAggregatedData;
    }

    /**
     *
     * @param averageAggregatedData
     */
    public void setAverageAggregatedData(AggregatedData averageAggregatedData) {
        this.averageAggregatedData = averageAggregatedData;
    }

    /**
     *
     * @return
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     *
     * @return
     */
    public String getPlateformeName() {
        return plateformeName;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public Date getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(Date heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public List<AggregatedData> getTargetAggregatedDatas() {
        return targetAggregatedDatas;
    }

    /**
     *
     * @param targetAggregatedDatas
     */
    public void setTargetAggregatedDatas(List<AggregatedData> targetAggregatedDatas) {
        this.targetAggregatedDatas = targetAggregatedDatas;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsPrelevement() {
        return outilsPrelevement;
    }

    /**
     *
     * @param outilsPrelevement
     */
    public void setOutilsPrelevement(OutilsMesure outilsPrelevement) {
        this.outilsPrelevement = outilsPrelevement;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.siteName);
        hash = 59 * hash + Objects.hashCode(this.plateformeName);
        hash = 59 * hash + Objects.hashCode(this.outilsMesure);
        hash = 59 * hash + Objects.hashCode(this.outilsPrelevement);
        hash = 59 * hash + Objects.hashCode(this.heure);
        hash = 59 * hash + Objects.hashCode(this.minAggregatedData);
        hash = 59 * hash + Objects.hashCode(this.maxAggregatedData);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VariableAggregatedDatas other = (VariableAggregatedDatas) obj;
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.plateformeName, other.plateformeName)) {
            return false;
        }
        if (!Objects.equals(this.outilsMesure, other.outilsMesure)) {
            return false;
        }
        if (!Objects.equals(this.outilsPrelevement, other.outilsPrelevement)) {
            return false;
        }
        if (!Objects.equals(this.heure, other.heure)) {
            return false;
        }
        if (!Objects.equals(this.minAggregatedData, other.minAggregatedData)) {
            return false;
        }
        if (!Objects.equals(this.maxAggregatedData, other.maxAggregatedData)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(VariableAggregatedDatas t) {
        if(t==null){
            return -1;
        }
        Integer compareSiteName = Optional.ofNullable(siteName).map(s->s.compareTo(t.siteName)).orElse(0);
        if(compareSiteName!=0){
            return compareSiteName;
        }
        Integer comparePlateformeName = Optional.ofNullable(plateformeName).map(s->s.compareTo(t.plateformeName)).orElse(0);
        if(compareSiteName!=0){
            return comparePlateformeName;
        }
        Integer compareOutilsMesure = Optional.ofNullable(outilsMesure).map(s->s.compareTo(t.outilsMesure)).orElse(0);
        if(compareSiteName!=0){
            return compareOutilsMesure;
        }
        Integer compareOutilsPrelevement = Optional.ofNullable(outilsPrelevement).map(s->s.compareTo(t.outilsPrelevement)).orElse(0);
        if(compareSiteName!=0){
            return compareOutilsPrelevement;
        }
        Integer compareHeure = Optional.ofNullable(heure).map(s->s.compareTo(t.heure)).orElse(0);
        return compareHeure;
    }
    /**
     *
     */
    public class ValueAggregatedData {
        
        private Float depth;
        private Float value;
        private LocalTime heure;
        
        /**
         *
         * @param f
         * @param depth
         * @param f1
         * @param value
         */
        public ValueAggregatedData(Float depth, Float value) {
            super();
            this.depth = depth;
            this.value = value;
        }
        
        /**
         *
         * @return
         */
        public Float getDepth() {
            return depth;
        }
        
        /**
         *
         * @return
         */
        public Float getValue() {
            return value;
        }
        
        /**
         *
         * @param depth
         */
        public void setDepth(Float depth) {
            this.depth = depth;
        }
        
        /**
         *
         * @param value
         */
        public void setValue(Float value) {
            this.value = value;
        }
        
        /**
         *
         * @return
         */
        public LocalTime getHeure() {
            return heure;
        }
        
        /**
         *
         * @param heure
         */
        public void setHeure(LocalTime heure) {
            this.heure = heure;
        }
    }
    /**
     *
     */
    public class AggregatedData {
        
        private Map<LocalDate, List<ValueAggregatedData>> valuesAggregatedDatasByDatesMap = new HashMap<LocalDate, List<ValueAggregatedData>>();
        private Map<LocalDate, Float> valuesByDatesMap = new HashMap<LocalDate, Float>();
        
        /**
         *
         */
        public AggregatedData() {
            super();
        }
        
        /**
         *
         * @return
         */
        public Map<LocalDate, Float> getValuesByDatesMap() {
            return valuesByDatesMap;
        }
        
        /**
         *
         * @return
         */
        public Map<LocalDate, List<ValueAggregatedData>> getValuesAggregatedDatasByDatesMap() {
            return valuesAggregatedDatasByDatesMap;
        }
        
    }
}

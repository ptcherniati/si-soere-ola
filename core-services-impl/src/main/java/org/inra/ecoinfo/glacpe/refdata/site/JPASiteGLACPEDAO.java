package org.inra.ecoinfo.glacpe.refdata.site;


import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.glacpe.refdata.typesite.TypeSite;
import org.inra.ecoinfo.glacpe.refdata.typesite.TypeSite_;
import org.inra.ecoinfo.mga.business.Utils;
import org.inra.ecoinfo.refdata.site.JPASiteDAO;

/**
 *
 * @author ptcherniati
 */
public class JPASiteGLACPEDAO extends JPASiteDAO implements ISiteGLACPEDAO {

    /**
     *
     * @param id
     * @return
     */
    @Override
    public List<SiteGLACPE> getByTyepsiteId(Long id) {
        CriteriaQuery<SiteGLACPE> query = builder.createQuery(SiteGLACPE.class);
        Root<SiteGLACPE> site = query.from(SiteGLACPE.class);
        Join<TypeSite,SiteGLACPE > typeSite = site.join(SiteGLACPE_.TYPE_SITE);
        query
                .select(site)
                .where(builder.equal(typeSite.get(TypeSite_.id), id));
        return getResultList(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<SiteGLACPE> getAllSitesGLACPE() {
        return getAllBy(SiteGLACPE.class, SiteGLACPE::getCode);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<SiteGLACPE> getByCode(String code) {
        CriteriaQuery<SiteGLACPE> query = builder.createQuery(SiteGLACPE.class);
        Root<SiteGLACPE> site = query.from(SiteGLACPE.class);
        query
                .select(site)
                .where(builder.equal(site.get(SiteGLACPE_.code), Utils.createCodeFromString(code)));
        return getOptional(query);
        
    }
}

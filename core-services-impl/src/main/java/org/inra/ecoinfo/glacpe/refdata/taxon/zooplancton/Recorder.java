/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.taxon.zooplancton;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.AbstractTaxonRecorder;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Antoine Schellenberger"
 * @author "Guillaume Enrico"
 */
public class Recorder extends AbstractTaxonRecorder {

    private static final String TAXON_ZOO = "Zooplancton";

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    protected List<String> getAllElementsByTheme() throws PersistenceException {
        return taxonDAO.getTaxonNameByTheme(TAXON_ZOO);
    }

    /**
     *
     * @throws PersistenceException
     */
    @Override
    protected void updateNamesProprieteTaxonPossibles() throws PersistenceException {
        List<ProprieteTaxon> proprieteTaxons = proprieteTaxonDAO.getAllProprietesTaxonsByType(TAXON_ZOO);
        Collections.sort(proprieteTaxons, new Comparator<ProprieteTaxon>() {

            @Override
            public int compare(ProprieteTaxon o1, ProprieteTaxon o2) {
                return o1.getOrdreAffichage().compareTo(o2.getOrdreAffichage());
            }
        });
        String[] namesProprieteTaxonPossibles = new String[proprieteTaxons.size()];
        int index = 0;
        for (ProprieteTaxon ptaxon : proprieteTaxons) {
            namesProprieteTaxonPossibles[index++] = ptaxon.getNom();
        }
        this.namesProprieteTaxonsPossibles = namesProprieteTaxonPossibles;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getTheme() {
        return TAXON_ZOO;
    }

    @Override
    protected List<Taxon> getAllElements() {
        return taxonDAO.getTaxonByTheme(TAXON_ZOO);
    }
}

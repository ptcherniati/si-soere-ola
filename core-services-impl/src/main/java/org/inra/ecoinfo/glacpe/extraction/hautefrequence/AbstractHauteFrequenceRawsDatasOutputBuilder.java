package org.inra.ecoinfo.glacpe.extraction.hautefrequence;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.IHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author mylene
 */
public abstract class AbstractHauteFrequenceRawsDatasOutputBuilder extends AbstractOutputBuilder {

     /**
     *
     */
//    protected static final String BUNDLE_SOURCE_PATH_SONDEMULTI = "org.inra.ecoinfo.glacpe.extraction.sondemulti.messages";
// voir si le message sonde suffit ou s'il faut en créer un pour hautefrequence -> dossier org.inra.ecoinfo.glacpe.extraction.hautefrequence existe déjà
    protected static final String BUNDLE_SOURCE_PATH_HAUTEFREQUENCE = "org.inra.ecoinfo.glacpe.extraction.hautefrequence.messages";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected IHauteFrequenceDAO hauteFrequenceDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;
    
    /**
     *
     * @param hauteFrequenceDAO
     */
    public void setHauteFrequenceDAO(IHauteFrequenceDAO hauteFrequenceDAO) {
        this.hauteFrequenceDAO = hauteFrequenceDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
    
    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeProjetVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeProjetVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getSite().getCode()));
        }
        return sitesNames;
    }

}

package org.inra.ecoinfo.glacpe.dataset.chimie.impl;


import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.exception.PersistenceConstraintException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import static org.inra.ecoinfo.glacpe.dataset.IProcessRecord.DATASET_DESCRIPTOR_XML;
import org.inra.ecoinfo.glacpe.dataset.chimie.ISequenceChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.SousSequenceChimie;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.transaction.TransactionStatus;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_SOURCE_PATH_CHIMIE = "org.inra.ecoinfo.glacpe.dataset.chimie.messages";

    private static final String MSG_CONFLICT_BD_SEQUENCE_CHIMIE = "PROPERTY_MSG_CONFLICT_BD_SEQUENCE_CHIMIE";

    private static final String PROPERTY_MSG_MISSING_DATE_PRELEVEMENT = "PROPERTY_MSG_MISSING_DATE_PRELEVEMENT";

    private static final String CODE_PROJET_SUIVI_RIVIERE = "suivi_des_rivieres";

    private static final String PROPERTY_MSG_SITE_EXPECTED = "PROPERTY_MSG_SITE_EXPECTED";

    private static final String PROPERTY_MSG_PROJET_EXPECTED = "PROPERTY_MSG_PROJET_EXPECTED";

    private static final String PROPERTY_MSG_INVALID_DATE = "PROPERTY_MSG_INVALID_DATE";
    private static final String VARIABLE_PROFONDEUR_MINIMUM = "profondeur_minimum";
    private static final String VARIABLE_PROFONDEUR_MAXIMUM = "profondeur_maximum";
    private static final String VARIABLE_PROFONDEUR_MESUREE = "profondeur_mesuree";

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected ISequenceChimieDAO sequenceChimieDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;
    protected String datatypeCode;

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        // Here we use the ISequenceChimieDAO class in order to access the dataset-descriptor.xml because they are in the same package and above all the this.getClass().getResource("../dataset-descriptor.xml") dont work!!
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISequenceChimieDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    /**
     * @param column
     * @return
     */
    protected Optional<VariableGLACPE> readVariableFromColumn(final Column column) {
        return this.variableDAO.getByCode(column.getName())
                .map(v -> (VariableGLACPE) v);
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());
        datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datasetDescriptor)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        try {
            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();

            String[] values = null;
            long lineCount = 1;
            // Saute la 1ere ligne
            parser.getLine();
            SortedMap<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> sequencesMapLines = new TreeMap<>();

            String projetCode;
            String nomSite;
            String plateformeCode;
            LocalDate datePrelevement;
            LocalDate dateDebutCampagne;
            LocalDate dateFinCampagne;
            LocalDate dateReception;
            Float profondeurMin;
            Float profondeurMax;
            Float profondeurReelle;
            String outilPrelevementCode;
            String variableCode;
            VariableGLACPE variable;
            String value = null;
            // String flag = null;
            // On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;

                // On parcourt chaque colonne d'une ligne
                projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                if (!Utils.createCodeFromString(projetCode).equals(projetCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), projetCodeFromVersion, lineCount, 1, projetCode)));
                }
                nomSite = cleanerValues.nextToken();
                if (!Utils.createCodeFromString(nomSite).equals(siteCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, lineCount, 2, nomSite)));
                }
                plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                datePrelevement = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);
                dateDebutCampagne = readDate(errorsReport, lineCount, 5, cleanerValues, datasetDescriptor);
                dateFinCampagne = readDate(errorsReport, lineCount, 6, cleanerValues, datasetDescriptor);
                dateReception = readDate(errorsReport, lineCount, 7, cleanerValues, datasetDescriptor);
                outilPrelevementCode = Utils.createCodeFromString(cleanerValues.nextToken());

                profondeurMin = readDepth(cleanerValues, "profondeur_minimum", errorsReport, controlesCoherenceMap, lineCount, 9);
                profondeurMax = readDepth(cleanerValues, "profondeur_maximum", errorsReport, controlesCoherenceMap, lineCount, 10);
                profondeurReelle = readDepth(cleanerValues, "profondeur_mesuree", errorsReport, controlesCoherenceMap, lineCount, 11);

                //Comparaison profondeurs Min et Max
                if (profondeurMin != null && profondeurMax != null && profondeurMax < profondeurMin) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH_INTERVAL), profondeurMin, profondeurMax, lineCount, 9, 10)));
                }

                variableCode = Utils.createCodeFromString(cleanerValues.nextToken());
                value = readValue(errorsReport, controlesCoherenceMap, lineCount, variableCode, value, cleanerValues);
                datePrelevement = retrieveSpecificDatePrelevement(projetCode, datePrelevement, dateFinCampagne, lineCount, errorsReport);

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, dateDebutCampagne, dateFinCampagne, dateReception, outilPrelevementCode, profondeurMin, profondeurMax, profondeurReelle, variableCode, value, lineCount,
                        projetCode, variableCode);
                if (datePrelevement != null) {
                    sequencesMapLines
                            .computeIfAbsent(datePrelevement, k-> new TreeMap<String, SortedMap<Float, List<LineRecord>>>())
                            .computeIfAbsent(String.format("%s,%s", plateformeCode, outilPrelevementCode), k -> new TreeMap<Float, List<LineRecord>>())
                            .computeIfAbsent(line.getProjetCode().equals(CODE_PROJET_SUIVI_RIVIERE) ? 0f : line.getProfondeurMax(), k -> new LinkedList<LineRecord>())
                            .add(line);
                }

            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
            persistLines(projetCodeFromVersion, siteCodeFromVersion, realNodes, errorsReport, versionFile, sequencesMapLines);

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }
        } catch (PersistenceException
                | IOException
                | InterruptedException e) {
            // TODO Auto-generated catch block
            throw new BusinessException(e);
        }

    }

    private Float readDepth(CleanerValues cleanerValues, String variableCode, ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, int columnsCount) {
        String profondeurString = cleanerValues.nextToken();
        Float value = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
        if (value != null) {
            try {
                value = Float.parseFloat(profondeurString);

                if (!controlesCoherenceMap.isEmpty() && controlesCoherenceMap.get(variableCode) != null) {
                    testValueCoherence(
                            value,
                            controlesCoherenceMap.get(variableCode).getValeurMin(),
                            controlesCoherenceMap.get(variableCode).getValeurMax(),
                            lineCount,
                            columnsCount);
                }
            } catch (BadExpectedValueException e) {
                errorsReport.addException(e);

            } catch (Exception e) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, columnsCount)));
            }
        }
        return value;
    }

    private String readValue(ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, String variableCode, String value, CleanerValues cleanerValues) {
        try {
            value = cleanerValues.nextToken();
            if (controlesCoherenceMap.get(variableCode) != null && value != null && !value.trim().isEmpty()) {
                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variableCode).getValeurMin(), controlesCoherenceMap.get(variableCode).getValeurMax(), lineCount, 13);
            }
        } catch (NumberFormatException e) {
            errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, 13, value)));
        } catch (BadExpectedValueException e) {
            errorsReport.addException(e);
        }
        return value;
    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    private VariableGLACPE retrieveVariable(Map<String, VariableGLACPE> measuredVariablesMap, String variableCode, ErrorsReport errorReport) throws PersistenceException {
        VariableGLACPE variable = null;

        if (!measuredVariablesMap.containsKey(variableCode)) {
            variable = (VariableGLACPE) variableDAO.getByCode(variableCode).orElse(null);
            if (variable == null) {
                errorReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), variableCode)));
            }
            measuredVariablesMap.put(variableCode, variable);
        } else {
            variable = measuredVariablesMap.get(variableCode);
        }

        return variable;
    }

    private LocalDate retrieveSpecificDatePrelevement(String projetCode, LocalDate datePrelevement, LocalDate dateFinCampagne, long lineCount, ErrorsReport errorsReport) {
        if (datePrelevement == null) {
            if (projetCode.equals(CODE_PROJET_SUIVI_RIVIERE)) {
                datePrelevement = dateFinCampagne;
            } else {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_DATE_PRELEVEMENT), lineCount)));
            }
        }
        return datePrelevement;
    }

    private void testConflictsSequenceChimie(LocalDate datePrelevement, String projetCode, String siteCode, VersionFile versionFile, boolean debugMode) throws PersistenceException, InsertionDatabaseException {
        SequenceChimie sequenceChimie;
        if (debugMode) {
            sequenceChimie = sequenceChimieDAO.getByDatePrelevementProjetCodeAndSiteCode(datePrelevement, projetCode, siteCode).orElse(null);
            if (sequenceChimie != null) {
                throw new PersistenceConstraintException(String.format(getGLACPEMessageWithBundle(getLocalizationManager(), BUNDLE_SOURCE_PATH_CHIMIE, MSG_CONFLICT_BD_SEQUENCE_CHIMIE), datePrelevement.toString(), versionFile.getDataset().getDateDebutPeriode().toString()
                        .concat("-").concat(versionFile.getDataset().getDateFinPeriode().toString())));
            }
        }
    }

    private void persistLines(String projetCode, String siteCode, Map<String, List<RealNode>> realNodes, ErrorsReport errorsReport, VersionFile versionFile, SortedMap<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> sequencesMapLines) throws PersistenceException, InterruptedException {
        Allocator allocator = Allocator.getInstance();
        LocalDate datePrelevement;
        int count = 0;
        allocator.allocate("publish", versionFile.getFileSize());
        Boolean debugMode = false;
        Iterator<Entry<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>>> iterator = sequencesMapLines.entrySet().iterator();
        while (iterator.hasNext()) {
            final Entry<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> entry = iterator.next();
            SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines = sequencesMapLines.get(entry.getKey());
            if (!sequenceLines.isEmpty()) {
                datePrelevement = entry.getKey();

                debugMode = processSequence(realNodes, errorsReport, versionFile, debugMode, sequenceLines, datePrelevement, projetCode, siteCode);
                logger.debug(String.format("%d - %s", count++, datePrelevement));
                sequenceChimieDAO.flush();
                versionFile = versionFileDAO.merge(versionFile);
            }
            // Très important à cause de problèmes de performances
            iterator.remove();
        }
    }

    private Boolean processSequence(Map<String, List<RealNode>> realNodes, ErrorsReport errorsReport, VersionFile versionFile, Boolean debugMode, SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines, LocalDate datePrelevement, String projetCode, String siteCode) throws PersistenceException {
        try {
            processSequenceInNormalMode(realNodes, errorsReport, versionFile, debugMode, sequenceLines, datePrelevement, projetCode, siteCode);
        } catch (InsertionDatabaseException e) {
            errorsReport.addException(e);

        } catch (Exception e) {
            debugMode = processSequenceInDebugMode(realNodes, errorsReport, versionFile, debugMode, sequenceLines, datePrelevement, projetCode, siteCode, e);
        } finally {
            if (!debugMode) {
                versionFileDAO.flush();
                versionFile = versionFileDAO.merge(versionFile);
            }
        }
        return debugMode;

    }

    private void processSequenceInNormalMode(Map<String, List<RealNode>> realNodes, ErrorsReport errorsReport, VersionFile versionFile, Boolean debugMode, SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines, LocalDate datePrelevement, String projetCode, String siteCode) throws PersistenceException,
            InsertionDatabaseException {
        if (debugMode) {
            TransactionStatus transactionStatus = versionFileDAO.beginNewTransaction();
            buildSequence(realNodes, datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, false);
            versionFileDAO.rollbackTransaction(transactionStatus);
        } else {
            buildSequence(realNodes, datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, false);
        }
    }

    private Boolean processSequenceInDebugMode(Map<String, List<RealNode>> realNodes, ErrorsReport errorsReport, VersionFile versionFile, Boolean debugMode, SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines, LocalDate datePrelevement, String projetCode, String siteCode, Exception e) throws PersistenceException {
        if (Utils.matchExceptionCause(e, ConstraintViolationException.class
        )) {
            debugMode = new Boolean(true);
            TransactionStatus transactionStatus = versionFileDAO.beginNewTransaction();

            try {
                buildSequence(realNodes, datePrelevement, projetCode, siteCode, sequenceLines, versionFile, errorsReport, true);
            } catch (PersistenceConstraintException e1) {
                errorsReport.addException(e1);
            } catch (InsertionDatabaseException e1) {
                // déjà catché plus haut
            }
            versionFileDAO.rollbackTransaction(transactionStatus);
        } else {
            throw new PersistenceException(e);
        }
        return debugMode;
    }

    private void buildSequence(Map<String, List<RealNode>> realNodes, LocalDate datePrelevement, String projetCode, String siteCode, SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport, boolean debugMode) throws PersistenceException, InsertionDatabaseException {

        LineRecord firstLine = sequenceLines.values().stream()
                .map(m -> m.values()
                .stream()
                .map(l -> l
                .stream()
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(null);
        SequenceChimie sequenceChimie = new SequenceChimie();

        testConflictsSequenceChimie(datePrelevement, projetCode, siteCode, versionFile, debugMode);

        sequenceChimie.setDatePrelevement(datePrelevement);
        sequenceChimie.setDateDebutCampagne(firstLine.getDateDebutCampagne());
        sequenceChimie.setDateFinCampagne(firstLine.getDateFinCampagne());
        sequenceChimie.setDateReception(firstLine.getDateReception());

        sequenceChimie.setVersionFile(versionFile);

        for (Entry<String, SortedMap<Float, List<LineRecord>>> entry : sequenceLines.entrySet()) {
            String key = entry.getKey();
            String[] split = key.split(",");
            String platformeCode = split.length==2?split[0]:"";
            String outilCode = split.length==2?split[1]:"";
            SortedMap<Float, List<LineRecord>> value = entry.getValue();
            try {
                buildSousSequence(firstLine.getOriginalLineNumber(), realNodes, siteCode, platformeCode, value, outilCode, sequenceChimie, errorsReport);
            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }

        }

        if (!errorsReport.hasErrors()) {
            try {
                sequenceChimieDAO.saveOrUpdate(sequenceChimie);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getUTCDateTextFromLocalDateTime(datePrelevement, DateUtil.DD_MM_YYYY_HH_MM));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(Long firstLineNumber, Map<String, List<RealNode>> realNodes, String siteCode, String plateformeCode, SortedMap<Float, List<LineRecord>> sousSequenceLines, String outilPrelevementCode, SequenceChimie sequenceChimie, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        SousSequenceChimie sousSequenceChimie = new SousSequenceChimie();

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, Utils.createCodeFromString(siteCode)).orElse(null);
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLineNumber));
            throw insertionDatabaseException;
        }

        if (!plateforme.getSite().getCode().equals(Utils.createCodeFromString(siteCode))) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH), plateforme.getName(), firstLineNumber,
                    siteCode));
            throw insertionDatabaseException;
        }

        OutilsMesure outilPrelevement = outilsMesureDAO.getByCode(outilPrelevementCode).orElse(null);
        if (outilPrelevement == null) {
            throw new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilPrelevementCode));
        }

        sousSequenceChimie.setPlateforme(plateforme);
        sousSequenceChimie.setOutilsMesure(outilPrelevement);
        sousSequenceChimie.setSequence(sequenceChimie);
        sequenceChimie.getSousSequences().add(sousSequenceChimie);

        
        for (Entry<Float, List<LineRecord>> entry : sousSequenceLines.entrySet()) {
            Float profondeur = entry.getKey();
            List<LineRecord> mesureLines = entry.getValue();
            try {
                buildMesure(realNodes, profondeur, mesureLines.get(0).getProfondeurMin(), mesureLines.get(0).getProfondeurReelle(), mesureLines, sousSequenceChimie, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
    }

    private void buildMesure(Map<String, List<RealNode>> realNodes, Float profondeurMax, Float profondeurMin, Float profondeurReelle, List<LineRecord> profondeurLines, SousSequenceChimie sousSequenceChimie, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException {

        List<String> variableName = Lists.newArrayList();
        for (LineRecord profondeurLine : profondeurLines) {
            if (!variableName.contains(profondeurLine.getVariableCode())) {
                variableName.add(profondeurLine.getVariableCode());
            } else {
                errorsReport.addException(new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DOUBLON_LINE), profondeurLine.getOriginalLineNumber())));
            }
        }
        MesureChimie mesureChimie = new MesureChimie();
        mesureChimie.setSousSequence(sousSequenceChimie);
        sousSequenceChimie.getMesures().add(mesureChimie);
        mesureChimie.setProfondeurMin(profondeurMin);
        mesureChimie.setProfondeurMax(profondeurMax);
        mesureChimie.setProfondeurReelle(profondeurReelle);
        RealNode realNode = null;

        for (LineRecord profondeurLine : profondeurLines) {

            ValeurMesureChimie valeurMesureChimie = new ValeurMesureChimie();
            if (!realNodes.containsKey(profondeurLine.getVariableCode()) || realNodes.get(profondeurLine.getVariableCode()).isEmpty()) {
                String errorMessage = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS), profondeurLine.getVariableCode());
                throw new InsertionDatabaseException(errorMessage);
            } else {
                realNode = realNodes.get(profondeurLine.getVariableCode()).get(0);
            }

            valeurMesureChimie.setRealNode(datatypeVariableUniteGLACPEDAO.mergeRealNode(realNode));

            if (profondeurLine.getValue() == null || profondeurLine.getValue().length() == 0) {
                valeurMesureChimie.setValeur(null);
            } else {
                Float value = Float.parseFloat(profondeurLine.getValue());
                valeurMesureChimie.setValeur(value);
            }
            valeurMesureChimie.setLigneFichierEchange(profondeurLine.getOriginalLineNumber());
            valeurMesureChimie.setMesure(mesureChimie);
            mesureChimie.getValeurs().add(valeurMesureChimie);
        }

    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param sequenceChimieDAO
     */
    public void setSequenceChimieDAO(ISequenceChimieDAO sequenceChimieDAO) {
        this.sequenceChimieDAO = sequenceChimieDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }

    /**
     *
     * @param datatypeName
     */
    public void setDatatypeName(String datatypeName) {
        this.datatypeCode = datatypeName;
    }
}

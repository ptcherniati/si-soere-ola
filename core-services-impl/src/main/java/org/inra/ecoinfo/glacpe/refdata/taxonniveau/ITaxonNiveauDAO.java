/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.glacpe.refdata.taxonniveau;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author "Antoine Schellenberger"
 *
 */
public interface ITaxonNiveauDAO extends IDAO<TaxonNiveau> {

    /**
     *
     * @param code
     * @return
     */
    Optional<TaxonNiveau> getByCode(String code);
}

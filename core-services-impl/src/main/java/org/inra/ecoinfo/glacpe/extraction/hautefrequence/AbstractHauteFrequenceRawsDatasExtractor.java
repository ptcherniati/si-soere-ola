package org.inra.ecoinfo.glacpe.extraction.hautefrequence;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.IHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.MesureHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SousSequenceHauteFrequence;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatesRequestParamVO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.extraction.vo.VariableVO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author mylene
 */
public abstract class AbstractHauteFrequenceRawsDatasExtractor extends AbstractExtractor {
    
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    
    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected IHauteFrequenceDAO hauteFrequenceDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;
    
    /**
     *
     * @param hauteFrequenceDAO
     */
    public void setHauteFrequenceDAO(IHauteFrequenceDAO hauteFrequenceDAO) {
        this.hauteFrequenceDAO = hauteFrequenceDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }
    
// A adapter pour les données Haute Fréquence :
//      - extractDatas
//      - buildMesuresHauteFrequence
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        List<Long> depthRequestParamVO = AbstractGLACPEExtractor.getDepths(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = AbstractGLACPEExtractor.getPlatforms(requestMetadatas);
        DatesRequestParamVO datesRequestParamVO = AbstractGLACPEExtractor.getDatesRequestParam(requestMetadatas);

        List<MesureHauteFrequence> mesuresHauteFrequence = null;
        /*if (depthRequestParamVO.getAllDepth()) {
            mesuresHauteFrequence = (List<MesureHauteFrequence>) hauteFrequenceDAO.extractDatasForAllDepth(plateformesIds, projetSiteIds, datesRequestParamVO);
        } else if (depthRequestParamVO.getDepthMin().equals(depthRequestParamVO.getDepthMax())) {
            List<SousSequenceHauteFrequence> sousSequencesHauteFrequence = hauteFrequenceDAO.extractDatasForUniqueDepth(plateformesIds, projetSiteIds, datesRequestParamVO);
            mesuresHauteFrequence = buildMesuresHauteFrequence(sousSequencesHauteFrequence, depthRequestParamVO.getDepthMin());
        } else {
            mesuresHauteFrequence = (List<MesureHauteFrequence>) hauteFrequenceDAO.extractDatasForRangeDepth(plateformesIds, projetSiteIds, datesRequestParamVO, depthRequestParamVO.getDepthMin(), depthRequestParamVO.getDepthMax());
        }
        if (mesuresHauteFrequence == null || mesuresHauteFrequence.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT));
        }*/
        extractedDatasMap.put(MAP_INDEX_0, mesuresHauteFrequence);
        return extractedDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        sortSelectedVariables(requestMetadatasMap);
    }

    @SuppressWarnings("unchecked")
    private void sortSelectedVariables(Map<String, Object> requestMetadatasMap) {
        Collections.sort((List<VariableVO>) requestMetadatasMap.get(VariableVO.class.getSimpleName()), new Comparator<VariableVO>() {

            @Override
            public int compare(VariableVO o1, VariableVO o2) {
                if (o1.getOrdreAffichageGroupe() == null) {
                    o1.setOrdreAffichageGroupe(0);
                }
                if (o2.getOrdreAffichageGroupe() == null) {
                    o2.setOrdreAffichageGroupe(0);
                }
                return o1.getOrdreAffichageGroupe().toString().concat(o1.getCode()).compareTo(o2.getOrdreAffichageGroupe().toString().concat(o2.getCode()));
            }
        });

    }

    /**
     *
     * @param selectedPlateformes
     * @return
     */
    protected Set<String> retrieveSitesCodes(List<PlateformeProjetVO> selectedPlateformes) {
        Set<String> sitesNames = new HashSet<String>();
        for (PlateformeProjetVO plateforme : selectedPlateformes) {
            sitesNames.add(Utils.createCodeFromString(plateforme.getSite().getCode()));
        }
        return sitesNames;
    }

    private List<MesureHauteFrequence> buildMesuresHauteFrequence(List<SousSequenceHauteFrequence> sousSequenceHauteFrequence, Float depth) {
        List<MesureHauteFrequence> mesuresHauteFrequence = new LinkedList<MesureHauteFrequence>();
        for (SousSequenceHauteFrequence ssq : sousSequenceHauteFrequence) {
            MesureHauteFrequence currentMesure = null;
            for (MesureHauteFrequence mesureHauteFrequence : ssq.getMesures()) {
                if (currentMesure == null) {
                    currentMesure = mesureHauteFrequence;
                } else if (Math.abs(depth - mesureHauteFrequence.getSousSequence().getDimension().getZReleve()) < Math.abs(depth - currentMesure.getSousSequence().getDimension().getZReleve())) {
                    currentMesure = mesureHauteFrequence;
                }
            }
            mesuresHauteFrequence.add(currentMesure);
        }
        return mesuresHauteFrequence;
    }

    @Override
    public void setExtraction(Extraction extraction) {
        // TODO Auto-generated method stub

    }
    
}

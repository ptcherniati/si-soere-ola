package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.config.impl.CoreConfiguration;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author Antoine Schellenberger
 */
public class CompositeZooplanctonOutputBuilder extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {

    private static final String EXTRACTION = "extraction";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     *
     */
    protected static final String SEPARATOR_TEXT = "_";

    /**
     *
     */
    protected static final String EXTENSION_ZIP = ".zip";

    /**
     *
     */
    protected IOutputBuilder zooplanctonOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder outputBuilderZooBiovolume;

    /**
     *
     */
    protected IOutputBuilder requestReminderOutputBuilder;

    protected IOutputBuilder zooplanctonOutputBuilderSandre;
    private IFileCompConfiguration fileCompConfiguration;

    private void addOutputBuilderByCondition(Boolean condition, IOutputBuilder outputBuilder, List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        List<IOutputBuilder> outputsBuilders = new LinkedList<IOutputBuilder>();
        List<DatatypeVariableUniteGLACPE> dvUs = AbstractGLACPEExtractor.getDVUs(metadatasMap);
        addOutputBuilderByCondition(!dvUs.isEmpty(), zooplanctonOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition(((Boolean) metadatasMap.get(ZooplanctonParameters.KEY_MAP_SELECTION_BIOVOLUME)), outputBuilderZooBiovolume, outputsBuilders);
        outputsBuilders.add(requestReminderOutputBuilder);

        addOutputBuilderByCondition(!dvUs.isEmpty(), zooplanctonOutputBuilderSandre, outputsBuilders);
        return outputsBuilders;
    }

    @SuppressWarnings("unchecked")
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        RObuildZipOutputStream rObuildZipOutputStream = super.buildOutput(parameters);
        List<IOutputBuilder> roBuildZipOutputStream = resolveOutputsBuilders(parameters.getParameters());
        NoExtractionResultException noDataToExtract = null;
        try {
            for (IOutputBuilder iOutputBuilder : roBuildZipOutputStream) {
                try {
                    iOutputBuilder.buildOutput(parameters);
                } catch (NoExtractionResultException e) {
                    noDataToExtract = e;
                }
            }
            parameters.getParameters().put(AbstractGLACPEExtractor.CST_RESULTS, parameters.getResults());
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                try {
                    this.buidOutputFile(parameters);
                } catch (final NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager()
                            .getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                                    NoExtractionResultException.ERROR));
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
        } catch (FileNotFoundException e1) {
            throw new BusinessException("FileNotFoundException", e1);
        } catch (IOException e) {
            throw new BusinessException("IOException", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }

    /**
     * Buid output file.
     *
     * @param parameters the parameters
     * @throws BusinessException the business exception
     */
    protected void buidOutputFile(final IParameter parameters) throws BusinessException {
        final Set<String> datatypeNames = new HashSet<>();
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames,
                SUFFIX_FILENAME_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this
                .buildOutputPrintStreamMap(filesMap);
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     *
     * @param zooplanctonOutputBuilderSandre
     */
    public void setZooplanctonOutputBuilderSandre(IOutputBuilder zooplanctonOutputBuilderSandre) {
        this.zooplanctonOutputBuilderSandre = zooplanctonOutputBuilderSandre;
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(CoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param outputBuilderZooBiovolume
     */
    public void setOutputBuilderZooBiovolume(IOutputBuilder outputBuilderZooBiovolume) {
        this.outputBuilderZooBiovolume = outputBuilderZooBiovolume;
    }

    /**
     *
     * @param zooplanctonOutputBuilder
     */
    public void setZooplanctonOutputBuilder(IOutputBuilder zooplanctonOutputBuilder) {
        this.zooplanctonOutputBuilder = zooplanctonOutputBuilder;
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

}

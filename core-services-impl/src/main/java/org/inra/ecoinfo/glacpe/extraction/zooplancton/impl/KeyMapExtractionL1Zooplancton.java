package org.inra.ecoinfo.glacpe.extraction.zooplancton.impl;


import java.time.LocalDate;
import org.inra.ecoinfo.utils.DateUtil;

class KeyMapExtractionL1Zooplancton implements Comparable<KeyMapExtractionL1Zooplancton> {

    private String plateforme;

    private String site;

    private LocalDate date;

    private String projet;

    private String outilPrelevement;

    private String outilMesure;

    private Float volumeSedimente;

    private Float profondeurMin;

    private Float profondeurMax;

    private String nomDeterminateur;

    public KeyMapExtractionL1Zooplancton(String plateforme,
                                         LocalDate date,
                                         String projet,
                                         String site,
                                         String outilPrelevement,
                                         String outilMesure,
                                         Float volumeSedimente,
                                         Float profondeurMin,
                                         Float profondeurMax,
                                         String nomDeterminateur) {
        super();
        this.plateforme = plateforme;
        this.date = date;
        this.projet = projet;
        this.site = site;
        this.outilPrelevement = outilPrelevement;
        this.outilMesure = outilMesure;
        this.volumeSedimente = volumeSedimente;
        this.profondeurMin = profondeurMin;
        this.profondeurMax = profondeurMax;
        this.nomDeterminateur = nomDeterminateur;
    }

    public String getPlateforme() {
        return plateforme;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getProjet() {
        return projet;
    }

    public void setPlateforme(String plateforme) {
        this.plateforme = plateforme;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setProjet(String projet) {
        this.projet = projet;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Override
    public String toString() {
        return String.format("%s_%s_%s",
                             plateforme,
                             projet,
                             DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY));

    }

    @Override
    public int compareTo(KeyMapExtractionL1Zooplancton keyMapToCompare) {
        return toString().
                compareTo(keyMapToCompare.toString());
    }

    public String getOutilPrelevement() {
        return outilPrelevement;
    }

    public void setOutilPrelevement(String outilPrelevement) {
        this.outilPrelevement = outilPrelevement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;

        int result = 1;

        result = prime * result + ((date == null) ? 0 : date.hashCode());

        result = prime * result + ((plateforme == null) ? 0 : plateforme.
                hashCode());

        result = prime * result + ((projet == null) ? 0 : projet.hashCode());

        result = prime * result + ((outilPrelevement == null) ? 0 : outilPrelevement.
                hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        KeyMapExtractionL1Zooplancton other = (KeyMapExtractionL1Zooplancton) obj;
        if (date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!date.equals(other.date)) {
            return false;
        }
        if (plateforme == null) {
            if (other.plateforme != null) {
                return false;
            }
        } else if (!plateforme.equals(other.plateforme)) {
            return false;
        }
        if (projet == null) {
            if (other.projet != null) {
                return false;
            }
        } else if (!projet.equals(other.projet)) {
            return false;
        }
        if (outilPrelevement == null) {
            if (other.outilPrelevement != null) {
                return false;
            }
        } else if (!outilPrelevement.equals(other.outilPrelevement)) {
            return false;
        }
        return true;
    }

    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    public Float getProfondeurMin() {
        return profondeurMin;
    }

    public Float getProfondeurMax() {
        return profondeurMax;
    }

    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    public String getNomDeterminateur() {
        return nomDeterminateur;
    }

    public void setNomDeterminateur(String nomDeterminateur) {
        this.nomDeterminateur = nomDeterminateur;
    }

    public String getOutilMesure() {
        return outilMesure;
    }

    public void setOutilMesure(String outilMesure) {
        this.outilMesure = outilMesure;
    }

}

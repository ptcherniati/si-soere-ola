/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATypeOutilsMesureDAO extends AbstractJPADAO<TypeOutilsMesure> implements ITypeOutilsMesureDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<TypeOutilsMesure> getByCode(String code) {
        CriteriaQuery<TypeOutilsMesure> query = builder.createQuery(TypeOutilsMesure.class);
        Root<TypeOutilsMesure> typeOutilsMesure = query.from(TypeOutilsMesure.class);
        query
                .select(typeOutilsMesure)
                .where(builder.equal(typeOutilsMesure.get(TypeOutilsMesure_.code), code));
        return getOptional(query);
    }

}

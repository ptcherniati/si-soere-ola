package org.inra.ecoinfo.glacpe.extraction.chimie;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class DefaultChimieExtractor extends MO implements IExtractor {
    //private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    private static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "Failure to extract data - Echec de l'extraction des données";

    /**
     *
     */
    protected IExtractor chimieRawsDatasExtractor;

    /**
     *
     */
    protected IExtractor chimieAggregatedDatasExtractor;

    @Override
    public void setExtraction(Extraction extraction) {

    }

    private void addExtractorByCondition(Boolean condition, IExtractor extractor, List<IExtractor> extractors) {
        if (condition) {
            extractors.add(extractor);
        }
    }

    /**
     *
     * @param metadatasMap
     * @return
     */
    public List<IExtractor> resolveExtractors(Map<String, Object> metadatasMap) {
        List<IExtractor> extractors = new LinkedList<>();
        DatasRequestParamVO datasRequestParamVO = AbstractGLACPEExtractor.getDatasRequestParam(metadatasMap);

        addExtractorByCondition(datasRequestParamVO.getRawData(), chimieRawsDatasExtractor, extractors);
        addExtractorByCondition(datasRequestParamVO.getDataBalancedByDepth() || datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth(), chimieAggregatedDatasExtractor, extractors);
        return extractors;
    }

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        for (IExtractor extractor : resolveExtractors(parameters.getParameters())) {
            try {
                extractor.extract(parameters);
            } catch (NoExtractionResultException e) {
                throw new BusinessException(String.format(localizationManager.getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, PROPERTY_MSG_NO_RESULT_EXTRACT), ChimieParameters.CODE_EXTRACTIONTYPE_CHIMIE), e);
            } catch (BusinessException e) {
                throw e;
            }
        }
    }

    /**
     *
     * @param chimieRawsDatasExtractor
     */
    public void setChimieRawsDatasExtractor(IExtractor chimieRawsDatasExtractor) {
        this.chimieRawsDatasExtractor = chimieRawsDatasExtractor;
    }

    /**
     *
     * @param chimieAggregatedDatasExtractor
     */
    public void setChimieAggregatedDatasExtractor(IExtractor chimieAggregatedDatasExtractor) {
        this.chimieAggregatedDatasExtractor = chimieAggregatedDatasExtractor;
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        // TODO Auto-generated method stub
        return 0;
    }

}

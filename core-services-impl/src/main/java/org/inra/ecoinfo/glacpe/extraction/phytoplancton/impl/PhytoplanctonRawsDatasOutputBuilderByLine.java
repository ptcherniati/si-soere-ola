package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.MesurePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.SousSequencePhytoplancton;
import org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity.ValeurMesurePhytoplancton;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PhytoplanctonRawsDatasOutputBuilderByLine extends AbstractPhytoplanctonRawsDatasOutputBuilder {

    private static final String HEADER_RAW_DATA_ALLDEPTH = "PROPERTY_MSG_HEADER_RAW_DATA_ROW";
    private static final String CODE_DATATYPE_PHYTOPLANCTON = "phytoplancton";

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        StringBuilder stringBuilder = new StringBuilder();

        try {
            stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, HEADER_RAW_DATA_ALLDEPTH)));
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap);
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        Boolean sommation = (Boolean) requestMetadatasMap.get(PhytoplanctonParameters.SOMMATION);
        List<MesurePhytoplancton> mesuresPhytoplancton = resultsDatasMap.get(MAP_INDEX_0);

        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesOutilName = localizationManager.newProperties(OutilsMesure.TABLE_NAME, "nom");

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);
        Set<String> sitesNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_RAW_DATA);
        Map<String, File> filesMap = buildOutputsFiles(sitesNames, SUFFIX_FILENAME_CSV);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);
        for (String projetSitenames : outputPrintStreamMap.keySet()) {
            outputPrintStreamMap.get(projetSitenames).println(headers);
        }

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>>> sumBiovolumeMap = new TreeMap<>();
        prepareBiovolumeSum(sumBiovolumeMap, mesuresPhytoplancton);

        for (Iterator<Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>>>> iterator = sumBiovolumeMap.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>> mapBySite = projetEntry.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getCode());
            for (Iterator<Map.Entry<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                SortedMap<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>> mapByDate = siteEntry.getValue();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getCode());
                final PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_RAW_DATA));
                for (Iterator<Map.Entry<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>>> iterator2 = mapByDate.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<LocalDate, SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>> dateEntry = iterator2.next();
                    LocalDate localDate = dateEntry.getKey();
                    String date = DateUtil.getUTCDateTextFromLocalDateTime(localDate, DateUtil.DD_MM_YYYY);
                    SortedMap<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>> mapByPlateforme = dateEntry.getValue();
                    for (Iterator<Map.Entry<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>>> iterator3 = mapByPlateforme.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<Plateforme, SortedMap<Long, SortedSet<MesurePhytoplancton>>> plateformeEntry = iterator3.next();
                        Plateforme plateforme = plateformeEntry.getKey();
                        String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                        SortedMap<Long, SortedSet<MesurePhytoplancton>> mapByTaxon = plateformeEntry.getValue();
                        for (Taxon taxon : selectedTaxons) {
                            SortedSet<MesurePhytoplancton> mesures = mapByTaxon.get(taxon.getId());
                            Optional<MesurePhytoplancton> firstMesure = mesures
                                    .stream()
                                    .filter(m -> m != null)
                                    .findFirst();
                            String localizedOutilNamePrelev = firstMesure
                                    .map(m -> m.getSousSequence().getOutilsPrelevement())
                                    .map(o -> propertiesOutilName.getProperty(o.getNom(), o.getNom()))
                                    .orElse("");
                            String localizedOutilNameMesure = firstMesure
                                    .map(m -> m.getSousSequence().getOutilsMesure())
                                    .map(o -> propertiesOutilName.getProperty(o.getNom(), o.getNom()))
                                    .orElse("");
                            final Optional<MesurePhytoplancton> fisrtMesureOpt = mesures
                                    .stream()
                                    .findFirst();
                            Map<DatatypeVariableUniteGLACPE, Float> valeursMesuresPhytoplancton = fisrtMesureOpt
                                    .map(mesure -> buildValeurs(mesure.getValeurs()))
                                    .orElse(new HashMap<DatatypeVariableUniteGLACPE, Float>());
                            Optional<SousSequencePhytoplancton> sequenceOpt = fisrtMesureOpt
                                    .map(m -> m.getSousSequence());
                            String lineSkel = String.format(
                                    "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
                                    localizedProjetName,
                                    localizedSiteName,
                                    localizedPlateformeName,
                                    date,
                                    localizedOutilNamePrelev,
                                    localizedOutilNameMesure,
                                    firstMesure.map(m -> m.getSousSequence().getProfondeurMin().toString()).orElse(""),
                                    firstMesure.map(m -> m.getSousSequence().getProfondeurMax().toString()).orElse(""),
                                    firstMesure.map(m -> m.getSousSequence().getSequence().getNomDeterminateur()).orElse(""),
                                    firstMesure.map(m -> m.getSousSequence().getSequence().getVolumeSedimente().toString()).orElse(""),
                                    firstMesure.map(m -> m.getSousSequence().getSurfaceComptage().toString()).orElse(""),
                                    taxon.getNomLatin());
                            for (DatatypeVariableUniteGLACPE dvu : dvus) {
                                String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());
                                String uniteNom = dvu.getUnite().getCode();

                                if (sommation && mesures.size() > 1 && dvu.getVariable().getCode().equals(BIOVOLUME_DE_L_ESPECE)) {
                                    Float biovolume = sumBiovolume(mesures);
                                    String line = lineSkel.concat(String.format(";%s;%s;%s", localizedVariableName, biovolume, uniteNom));
                                    rawDataPrintStream.print(line);
                                    rawDataPrintStream.println();
                                } else if (mesures.size() == 1) {
                                    Float valeur = valeursMesuresPhytoplancton.get(dvu);
                                    if (valeur != null) {
                                        String line = lineSkel.concat(String.format(";%s;%s;%s", localizedVariableName, valeur, uniteNom));
                                        rawDataPrintStream.print(line);
                                        rawDataPrintStream.println();
                                    }
                                }

                            }
                        }
                        iterator3.remove();
                    }
                    iterator2.remove();
                }
                iterator1.remove();
            }
            iterator.remove();
        }

        return filesMap;
    }

    private Map<DatatypeVariableUniteGLACPE, Float> buildValeurs(List<ValeurMesurePhytoplancton> valeurs) {
        return valeurs.stream()
                .collect(
                        Collectors.toMap(
                                v -> (DatatypeVariableUniteGLACPE) v.getRealNode().getNodeable(),
                                ValeurMesurePhytoplancton::getValeur
                        )
                );
    }
}

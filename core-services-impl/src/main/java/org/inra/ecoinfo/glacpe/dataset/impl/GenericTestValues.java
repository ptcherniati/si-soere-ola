package org.inra.ecoinfo.glacpe.dataset.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;
import static org.inra.ecoinfo.dataset.AbstractRecorder.KERNEL_BUNDLE_SOURCE_PATH;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.ITestValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadIntervalDateException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class GenericTestValues implements ITestValues {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final static String MSG_INCONSISTENT_COLUMNS_COUNT = "MSG_INCONSISTENT_COLUMNS_COUNT";

    /**
     * The logger.
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Instantiates a new generic test values.
     */
    public GenericTestValues() {
        super();
    }

    /**
     * Test values.
     *
     * @param startline
     * <long> the startline
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(ISessionPropertiesMonSoere) the session properties
     * @param encoding
     * @link(String) the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptor) the dataset descriptor
     * @param datatypeName
     * @link(String) the datatype name
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.glacpe.dataset.ITestValues#testValues(long,
     * com.Ostermiller.util.CSVParser,
     * org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * org.inra.ecoinfo.glacpe.dataset.impl.ISessionPropertiesMonSoere,
     * java.lang.String, org.inra.ecoinfo.dataset.BadsFormatsReport)
     */
    @Override
    public void testValues(long startline, CSVParser parser, VersionFile versionFile, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor, String datatypeName) throws BusinessException {
        long lineNumber = startline;
        final long headerCountLine = lineNumber;

        String[] values;
        // On parcourt chaque ligne du fichier
        try {
            Integer nbTotalColumns = null;
            while ((values = parser.getLine()) != null) {
                int index = 0;
                lineNumber++;
                if (nbTotalColumns == null) {
                    nbTotalColumns = values.length;
                }
                if (values.length != nbTotalColumns) {
                    badsFormatsReport.addException(new BusinessException("<li>" + String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), MSG_INCONSISTENT_COLUMNS_COUNT), lineNumber) + "</li>"));
                }
                // On parcourt chaque colonne d'une ligne
                for (String value = values[index]; index < values.length; index++) {

                    if (index > datasetDescriptor.getColumns().size() - 1) {
                        break;
                    }
                    value = values[index];
                    final Column column = datasetDescriptor.getColumns().get(index);
                    checkValue(badsFormatsReport, versionFile, lineNumber, index, value, column);
                }
            }
        } catch (final IOException e) {
            logger.debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }

        if (lineNumber == headerCountLine) {
            badsFormatsReport.addException(new BusinessException(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_NO_DATA)));
        }

    }

    /**
     * Clean value.
     *
     * @param value
     * @link(String) the value
     * @return the string {@link String} the value
     */
    protected String cleanValue(final String value) {
        String returnValue;
        returnValue = value.replaceAll(CST_COMMA, CST_DOT);
        returnValue = value.replaceAll(CST_SPACE, CST_STRING_EMPTY);
        return returnValue;
    }

    /**
     * Check float type value.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index int the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @return the float {@link String[]} the values {@link BadsFormatsReport} the bads formats report {@link String} the value {@link Column}
     */
    protected Float checkFloatTypeValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, final Column column) {
        Float floatValue = null;
        try {
            floatValue = Float.parseFloat(value);
        } catch (final NumberFormatException e) {
            badsFormatsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_INVALID_FLOAT_VALUE), lineNumber, index + 1, column.getName(), value)));
        }
        return floatValue;
    }

    /**
     * Check date type value.
     *
     * @param badsFormatsReport
     * @param intervalDate
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index int the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     * @return the date {@link String[]} the values {@link BadsFormatsReport} the bads formats report {@link String} the value {@link Column} the column {@link DatatypeUniteVariableACBB} the variables types donnees
     */
    protected LocalDate checkDateTypeValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, final Column column, final IntervalDate intervalDate) {
        String dateFormat = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType();
        try {
            final LocalDate date = DateUtil.readLocalDateFromText(dateFormat, value);
            if (!value.equals(DateUtil.getUTCDateTextFromLocalDateTime(date, dateFormat))) {
                badsFormatsReport.addException(new NullValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_INVALID_DATE), value, lineNumber, index + 1, column.getName(), dateFormat)));
                return null;
            }
            if (!intervalDate.isInto(date.atStartOfDay())) {
                badsFormatsReport.addException(new BadIntervalDateException(String.format(getGLACPEMessageWithBundle(RecorderGLACPE.getStaticLocalizationManager(), KERNEL_BUNDLE_SOURCE_PATH, PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE), value, lineNumber, index + 1, intervalDate.getBeginDateToString(),
                        intervalDate.getEndDateToString())));
            }
            return date;
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new NullValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_INVALID_DATE), value, lineNumber, index + 1, dateFormat, column.getName())));
            return null;
        }
    }

    /**
     * Check time type value.
     *
     * @param value
     * @link(String[]) the values
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @link(String) the value
     * @param column
     * @link(Column)
     * @link(Map<String,DatatypeVariableUniteACBB>)
     * @link(DatasetDescriptorACBB)
     * @link(IRequestPropertiesACBB) the request properties acbb
     * @return the date {@link String[]} the values {@link BadsFormatsReport} the bads formats
     *         report {@link String} the value {@link Column} the column
     *         {@link DatatypeVariableUniteACBB} the variables types donnees
     * @link(String[]) the values
     * @link(BadsFormatsReport) the bads formats report
     * @link(String) the value
     * @link(Column) the column
     * @link(Map<String,DatatypeVariableUniteACBB>) the variables types donnees
     * @link(DatasetDescriptorACBB) the dataset descriptor
     * @link(IRequestPropertiesACBB) the request properties acbb
     */
    protected LocalTime checkTimeTypeValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, final Column column) {
        String dateFormat = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.HH_MM_SS : column.getFormatType();
        try {
            final LocalTime time = DateUtil.readLocalTimeFromText(dateFormat, value);
            if (!value.equals(DateUtil.getUTCDateTextFromLocalDateTime(time, dateFormat))) {
                badsFormatsReport.addException(new NullValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_INVALID_DATE), value, lineNumber, index + 1, column.getName(), dateFormat)));
                return null;
            }
            return time;
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new NullValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_INVALID_DATE), value, lineNumber, index + 1, column.getName(), dateFormat)));
            return null;
        }
    }
    /**
     * Check project type value.
     *
     * @param badsFormatsReport
     * @param projetCode
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column)
     * @link(Map<String,DatatypeUniteVariableMonSoere>) the variables types
     * donnees
     */
    protected void checkProjectTypeValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, final Column column, final String projetCode) {
        if (!projetCode.equals(Utils.createCodeFromString(value))) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), projetCode, lineNumber, 1, value)));
        }
    }

    /**
     * Check site type value.
     *
     * @param badsFormatsReport
     * @param siteCode
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column)
     * @link(Map<String,DatatypeUniteVariableMonSoere>) the variables types
     * donnees
     */
    protected void checkSiteTypeValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, final Column column, final String siteCode) {
        if (!siteCode.equals(Utils.createCodeFromString(value))) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCode, lineNumber, 2, value)));
        }
    }

    /**
     * Check other type value.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param index the column index
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column)
     * @link(Map<String,DatatypeUniteVariableMonSoere>) the variables types
     * donnees
     */
    protected void checkOtherTypeValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, final Column column) {

    }

    /**
     * Check value.
     *
     * @param badsFormatsReport
     * @param versionFile
     * @param i
     * @link(BadsFormatsReport) the bads formats report
     * @param lineNumber int the line number
     * @param value
     * @link(String) the value
     * @param column
     * @link(Column) the column
     */
    protected void checkValue(final BadsFormatsReport badsFormatsReport, final VersionFile versionFile, final long lineNumber, final int i, String value, final Column column) {
        String cleanValue = value;
        try {
            if (!column.isNullable() && (value == null || value.length() == 0)) {
                throw new NullValueException(String.format(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_VALUE_EXPECTED), lineNumber, i + 1, column.getName()));
            }
        } catch (NullValueException e) {
            badsFormatsReport.addException(e);
        }

        IntervalDate intervalDate = null;
        try {
            intervalDate = versionFile.getDataset().getIntervalDate();
        } catch (BadExpectedValueException e) {
            badsFormatsReport.addException(new BusinessException(getGLACPEMessage(RecorderGLACPE.getStaticLocalizationManager(), PROPERTY_MSG_BAD_INTERVAL)));
        }

        String projetCode = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCode = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();

        if (cleanValue != null) {
            cleanValue = cleanValue(cleanValue);
        }

        String valueType = column.getValueType();
        if (valueType != null && cleanValue.length() > 0) {
            if (PROPERTY_CST_DATE_TYPE.equalsIgnoreCase(valueType)) {
                checkDateTypeValue(badsFormatsReport, lineNumber, i, cleanValue, column, intervalDate);
            } else if (PROPERTY_CST_TIME_TYPE.equalsIgnoreCase(valueType)) {
                checkTimeTypeValue(badsFormatsReport, lineNumber, i, value, column);
            } else if (PROPERTY_CST_FLOAT_TYPE.equalsIgnoreCase(valueType)) {
                checkFloatTypeValue(badsFormatsReport, lineNumber, i, cleanValue, column);
            } else if (PROPERTY_CST_PROJECT_TYPE.equalsIgnoreCase(valueType)) {
                checkProjectTypeValue(badsFormatsReport, lineNumber, i, value, column, projetCode);
            } else if (PROPERTY_CST_SITE_TYPE.equalsIgnoreCase(valueType)) {
                checkSiteTypeValue(badsFormatsReport, lineNumber, i, value, column, siteCode);
            } else {
                checkOtherTypeValue(badsFormatsReport, lineNumber, i, cleanValue, column);
            }
        }
    }
}

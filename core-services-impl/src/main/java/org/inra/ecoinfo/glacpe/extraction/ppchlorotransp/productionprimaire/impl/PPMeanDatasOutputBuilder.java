package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.dataset.VariableAggregatedDatas;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEAggregatedDatasOutputBuilder;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl.PPChloroTranspParameter;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPMeanDatasOutputBuilder extends AbstractGLACPEAggregatedDatasOutputBuilder {

    private static final String BUNDLE_SOURCE_PATH_PP = "org.inra.ecoinfo.glacpe.dataset.productionprimaire.messages";

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_MEAN = "mean";
    private static final String HEADER_RAW_DATA = "PROPERTY_MSG_HEADER_MEAN";
    private static final String MSG_MEAN = "PROPERTY_MSG_MEAN";

    private static final String CODE_DATATYPE_PP = "production_primaire";
    /**
     *
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new LinkedList<T>(c);
        Collections.sort(list);
        return list;
    }

    /**
     *
     */
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO;

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatas) throws BusinessException {
        Properties propertiesVariableName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, HEADER_RAW_DATA)));
        for (DatatypeVariableUniteGLACPE dvu : dvus) {

            String localizedVariableName = propertiesVariableName.getProperty(dvu.getVariable().getCode(), dvu.getVariable().getCode());

            stringBuilder.append(String.format(";\"%s(%s)%n%s\"", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PP, MSG_MEAN), localizedVariableName, dvu.getUnite().getName()));

        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatasMap, PPChloroTranspParameter.PRODUCTION_PRIMAIRE);

        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatasMap);

        String depthMin = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMin().toString();
        String depthMax = depthRequestParamVO.getAllDepth() ? "" : depthRequestParamVO.getDepthMax().toString();

        List<IGLACPEAggregateData> valeursMesures = resultsDatasMap.get(PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE);

        Properties propertiesProjetName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Projet.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesPlateformeName = localizationManager.newProperties(Nodeable.getLocalisationEntite(Plateforme.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties propertiesSiteName = localizationManager.newProperties(Nodeable.getLocalisationEntite(SiteGLACPE.class), Nodeable.ENTITE_COLUMN_NAME);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatasMap);

        Set<String> projetSiteNames = getProjetSiteNames(requestMetadatasMap, SUFFIX_FILENAME_MEAN);
        Map<String, File> filesMap = buildOutputsFiles(projetSiteNames, SUFFIX_FILENAME_MEAN);
        Map<String, PrintStream> outputPrintStreamMap = buildOutputPrintStreamMap(filesMap);

        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = buildVariablesMeanDatas(valeursMesures);

        for (Iterator<Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>>> iterator = variablesAggregatedDatas.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> mapBySite = projetEntry.getValue();
            String localizedProjetName = propertiesProjetName.getProperty(Utils.createCodeFromString(projet.getName()), projet.getName());
            for (Iterator<Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> iterator1 = mapBySite.entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                String localizedSiteName = propertiesSiteName.getProperty(site.getCode(), site.getName());
                SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> mapByPlateforme = siteEntry.getValue();
                final PrintStream projetSiteOutputStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_MEAN));
                projetSiteOutputStream.println(headers);
                PrintStream rawDataPrintStream = outputPrintStreamMap.get(getProjetSiteName(projet, site, SUFFIX_FILENAME_MEAN));
                for (Iterator<Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>> iterator2 = mapByPlateforme.entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> platformEntry = iterator2.next();
                    Plateforme plateforme = platformEntry.getKey();
                    String localizedPlateformeName = propertiesPlateformeName.getProperty(plateforme.getCode(), plateforme.getName());
                    SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> mapByDate = platformEntry.getValue();
                    for (Iterator<Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>> iterator3 = mapByDate.entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>> dateEntry = iterator3.next();
                        LocalDate date = dateEntry.getKey();
                        SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas> aggregatedByVariable = dateEntry.getValue();
                        for (DatatypeVariableUniteGLACPE dvu : dvus) {
                            VariableAggregatedDatas variableAggregatedDatas = aggregatedByVariable.get(dvu);
                            if(variableAggregatedDatas==null){
                                continue;
                            }

                            rawDataPrintStream.print(String.format("%s;%s;%s;", localizedProjetName, localizedSiteName, localizedPlateformeName));
                            rawDataPrintStream.print(String.format("%s;%s;%s", DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY), depthMin, depthMax));

                            for (DatatypeVariableUniteGLACPE dvu2 : dvus) {
                                if (dvu2.equals(dvu)) {
                                    rawDataPrintStream.print(String.format(";%s", variableAggregatedDatas.getAverageAggregatedData().getValuesByDatesMap().get(date)));
                                } else {
                                    rawDataPrintStream.print(";");
                                }
                            }
                            rawDataPrintStream.println();
                        }
                        iterator3.remove();
                    }
                    iterator2.remove();
                }
                iterator1.remove();
            }
            iterator.remove();
        }

        closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> buildVariablesMeanDatas(List<IGLACPEAggregateData> valeursMesures) {
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedByDatesVariablesAndDates = buildValeursMesuresReorderedByDatesAndVariables(valeursMesures);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>> variablesAggregatedDatas = new TreeMap<Projet, SortedMap<SiteGLACPE, SortedMap<Plateforme, SortedMap<LocalDate, SortedMap<DatatypeVariableUniteGLACPE, VariableAggregatedDatas>>>>>();
        for (Iterator<Map.Entry<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>>>> iterator = valeursMesuresReorderedByDatesVariablesAndDates.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>>> projetEntry = iterator.next();
            Projet projet = projetEntry.getKey();
            for (Iterator<Map.Entry<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>>> iterator1 = projetEntry.getValue().entrySet().iterator(); iterator1.hasNext();) {
                Map.Entry<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>> siteEntry = iterator1.next();
                SiteGLACPE site = siteEntry.getKey();
                for (Iterator<Map.Entry<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>> iterator2 = siteEntry.getValue().entrySet().iterator(); iterator2.hasNext();) {
                    Map.Entry<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>> platformEntry = iterator2.next();
                    Plateforme plateforme = platformEntry.getKey();
                    for (Iterator<Map.Entry<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>> iterator3 = platformEntry.getValue().entrySet().iterator(); iterator3.hasNext();) {
                        Map.Entry<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>> dateEntry = iterator3.next();
                        LocalDate dateKey = dateEntry.getKey();
                        for (Iterator<Map.Entry<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>> iterator4 = dateEntry.getValue().entrySet().iterator(); iterator4.hasNext();) {
                            Map.Entry<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>> variableEntry = iterator4.next();
                            DatatypeVariableUniteGLACPE dvu = variableEntry.getKey();
                            VariableAggregatedDatas variableAggregatedDatas = new VariableAggregatedDatas(site.getCode(), plateforme.getCode(), dvu.getVariable().getCode(), dvu.getVariable().getOrdreAffichageGroupe());
                            TreeSet<IGLACPEAggregateData> valeursMesureDates = variableEntry.getValue();
                            fillVariableMeanDatas(variableAggregatedDatas, dateKey, valeursMesureDates);
                            variablesAggregatedDatas
                                    .computeIfAbsent(projet, k ->  new TreeMap<>())
                                    .computeIfAbsent(site,  k -> new TreeMap<>())
                                    .computeIfAbsent(plateforme,  k -> new TreeMap<>())
                                    .computeIfAbsent(dateKey,  k -> new TreeMap<>())
                                    .put(dvu, variableAggregatedDatas);
                        }
                    }
                }
            }
        }

        return variablesAggregatedDatas;
    }

    /**
     *
     * @param variableAggregatedDatas
     * @param dateKey
     * @param valeursMesureDates
     */
    protected void fillVariableMeanDatas(VariableAggregatedDatas variableAggregatedDatas, LocalDate dateKey, SortedSet<IGLACPEAggregateData> valeursMesureDates) {

        Float m = 0f;
        Float intermediateAverage = 0f;
        Float meanDepth = (valeursMesureDates.last().getDepth() - valeursMesureDates.first().getDepth());
        
        if (valeursMesureDates.first().getValue() != null) {
            variableAggregatedDatas.setOutilsPrelevement(valeursMesureDates.first().getOutilsPrelevement());

            if (valeursMesureDates.size() == 1) {
                if (valeursMesureDates.first().getValue() != null) {
                    intermediateAverage = valeursMesureDates.first().getValue();
                } else {
                    intermediateAverage = null;
                }
            } else {
                Iterator<IGLACPEAggregateData> iterator = valeursMesureDates.iterator();
                IGLACPEAggregateData valeurMesure = iterator.next();
                Float value = 0.0f;
                Float depth = 0.0f;
                while (iterator.hasNext()) {
                    if (variableAggregatedDatas.getOutilsPrelevement() == null && valeurMesure.getOutilsPrelevement() != null) {
                        variableAggregatedDatas.setOutilsPrelevement(valeurMesure.getOutilsPrelevement());
                    }
                    if (variableAggregatedDatas.getOutilsMesure() == null && valeurMesure.getOutilsMesure() != null) {
                        variableAggregatedDatas.setOutilsMesure(valeurMesure.getOutilsMesure());
                    }
                    if (valeurMesure.getValue() != null) {
                        value = valeurMesure.getValue();
                        depth = valeurMesure.getDepth();
                    }
                    valeurMesure = iterator.next();

                    if (value != null && valeurMesure.getValue() != null) {
                        Float indexValue = valeurMesure.getValue();
                        Float indexDepth = valeurMesure.getDepth();
                        m += ((value + indexValue) * (indexDepth - depth)) / 2;
                    }
                }

                intermediateAverage = m / meanDepth;
            }
            variableAggregatedDatas.getAverageAggregatedData().getValuesByDatesMap().put(dateKey, intermediateAverage);
        }
    }

    /**
     *
     * @param valeursMesures
     * @return
     */
    protected TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>>> buildValeursMesuresReorderedByDatesAndVariables(List<IGLACPEAggregateData> valeursMesures) {
        TreeMap<Projet, TreeMap<SiteGLACPE, TreeMap<Plateforme, TreeMap<LocalDate, Map<DatatypeVariableUniteGLACPE, TreeSet<IGLACPEAggregateData>>>>>> valeursMesuresReorderedByDatesAndVariables = valeursMesures.stream()
                .collect(
                        Collectors.groupingBy(
                                m -> m.getProjet(),
                                TreeMap::new,
                                Collectors.groupingBy(
                                        m -> m.getSite(),
                                        TreeMap::new,
                                        Collectors.groupingBy(
                                                m -> m.getPlateforme(),
                                                TreeMap::new,
                                                Collectors.groupingBy(
                                                        m -> m.getDate(),
                                                        TreeMap::new,
                                                        Collectors.groupingBy(
                                                                m -> (DatatypeVariableUniteGLACPE) m.getRealNode().getNodeable(),
                                                                Collectors.toCollection(
                                                                        () -> new TreeSet<>())
                                                        )
                                                )
                                        )
                                )
                        ));
        return valeursMesuresReorderedByDatesAndVariables;
    }


    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
}

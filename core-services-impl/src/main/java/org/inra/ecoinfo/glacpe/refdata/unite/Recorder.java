package org.inra.ecoinfo.glacpe.refdata.unite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author afiocca
 */
public class Recorder extends AbstractCSVMetadataRecorder<UniteGLACPE> {

    /**
     *
     */
    protected IUniteGLACPEDAO uniteGLACPEDAO;
    private Properties propertiesNomFR;
    private Properties propertiesNomEN;

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {

            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = null;

            while ((values = parser.getLine()) != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values, Unite.NAME_ENTITY_JPA);

                // On parcourt chaque colonne d'une ligne
                String nom = tokenizerValues.nextToken();
                String code = tokenizerValues.nextToken();
                String codeSandre = tokenizerValues.nextToken();

                createOrUpdateUnite(code, nom, codeSandre);

            }

            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(tokenizerValues.nextToken());
                uniteGLACPEDAO.remove(uniteGLACPEDAO.getByCode(code)
                        .orElseThrow(() -> new PersistenceException("can(t retrieve unite")));
            }
        } catch (IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<UniteGLACPE> getAllElements() {
        return uniteGLACPEDAO.getAllBy(UniteGLACPE.class, Unite::getCode);
    }

    /**
     *
     * @param unite
     * @return
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(UniteGLACPE unite) {

        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? EMPTY_STRING : unite.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? EMPTY_STRING : unite.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? EMPTY_STRING : propertiesNomFR.get(unite.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? EMPTY_STRING : propertiesNomEN.get(unite.getName()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unite == null ? EMPTY_STRING : unite.getCodeSandre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    private void createOrUpdateUnite(String code, String nom, String codeSandre) throws PersistenceException {

        UniteGLACPE dbUnite = (UniteGLACPE) uniteGLACPEDAO.getByCode(Utils.createCodeFromString(nom)).orElse(null);
        if (dbUnite == null) {
            createUnite(code, nom, codeSandre);
        } else {
            updateUnite(code, nom, dbUnite, codeSandre);
        }

    }

    private void createUnite(String code, String nom, String codeSandre) {

        UniteGLACPE unite = new UniteGLACPE(code, nom);
        unite.setCodeSandre(codeSandre);

    }

    private void updateUnite(String code, String nom, UniteGLACPE dbUnite, String codeSandre) {
        dbUnite.setCode(code);
        dbUnite.setName(nom);
        dbUnite.setCodeSandre(codeSandre);
    }

    @Override
    protected ModelGridMetadata<UniteGLACPE> initModelGridMetadata() {
        propertiesNomFR = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, "nom", Locale.FRENCH);
        propertiesNomEN = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, "nom", Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}

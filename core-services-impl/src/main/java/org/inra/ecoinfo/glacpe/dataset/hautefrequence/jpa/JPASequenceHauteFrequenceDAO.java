package org.inra.ecoinfo.glacpe.dataset.hautefrequence.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.ISequenceHauteFrequenceDAO;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SequenceHauteFrequence;
import org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity.SequenceHauteFrequence_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;

/**
 *
 * @author mylene
 */
public class JPASequenceHauteFrequenceDAO extends AbstractJPADAO<SequenceHauteFrequence> implements ISequenceHauteFrequenceDAO {
    
    /**
     *
     * @param datePrelevement
     * @param projetCode
     * @param siteCode
     * @return
     */
    @Override
    public Optional<SequenceHauteFrequence> getByDatePrelevementAndProjetCodeAndSiteCode(LocalDate datePrelevement, String projetCode, String siteCode)  {
        CriteriaQuery<SequenceHauteFrequence> query = builder.createQuery(SequenceHauteFrequence.class);
        Root<SequenceHauteFrequence> sequence = query.from(SequenceHauteFrequence.class);
        Join<RealNode, RealNode> realNodeSite = sequence
                .join(SequenceHauteFrequence_.versionFile)
                .join(VersionFile_.dataset)
                .join(Dataset_.realNode)
                .join(RealNode_.parent)
                .join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        Join<RealNode, Nodeable> projet = realNodeSite.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.nodeable);
        query
                .select(sequence)
                .where(
                        builder.equal(sequence.get(SequenceHauteFrequence_.datePrelevement), datePrelevement),
                        builder.equal(projet.get(Nodeable_.code), projetCode),
                        builder.equal(site.get(Nodeable_.code), siteCode)
                );
        return getOptional(query);
    }
}

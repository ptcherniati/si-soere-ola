package org.inra.ecoinfo.glacpe.dataset.sondemulti.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import org.hibernate.exception.ConstraintViolationException;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.impl.CleanerValues;
import static org.inra.ecoinfo.glacpe.dataset.impl.RecorderGLACPE.*;
import org.inra.ecoinfo.glacpe.dataset.impl.VariableValue;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISequenceSondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.ISondeMultiDAO;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.MesureSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.SousSequenceSondeMulti;
import org.inra.ecoinfo.glacpe.dataset.sondemulti.entity.ValeurMesureSondeMulti;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.IControleCoherenceDAO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.IDatatypeVariableUniteGLACPEDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.IOutilsMesureDAO;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.IPlateformeDAO;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projetsite.IProjetSiteDAO;
import org.inra.ecoinfo.glacpe.refdata.projetsite.ProjetSite;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author ptcherniati
 */
public class ProcessRecord extends AbstractProcessRecord {

    private static final long serialVersionUID = 1L;
    private static final String VARIABLE_PROFONDEUR_MESUREE = "profondeur_mesuree";

    /**
     *
     */
    protected ISequenceSondeMultiDAO sequenceSondeMultiDAO;

    /**
     *
     */
    protected ISondeMultiDAO sondeMultiDAO;

    /**
     *
     */
    protected IControleCoherenceDAO controleCoherenceDAO;

    /**
     *
     */
    protected IProjetSiteDAO projetSiteDAO;

    /**
     *
     */
    protected IPlateformeDAO plateformeDAO;

    /**
     *
     */
    protected IOutilsMesureDAO outilsMesureDAO;
    protected IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO;
    protected String datatypeCode;

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {

        ErrorsReport errorsReport = new ErrorsReport(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR));
        Utils.testNotNullArguments(getLocalizationManager(), versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, getLocalizationManager());
        datatypeCode = LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode();
        String projetCodeFromVersion = LacsUtils.getProjetFromDataset(versionFile.getDataset()).getCode();
        String siteCodeFromVersion = LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode();
        Map<String, List<RealNode>> realNodes = buildVariablesHeaderAndSkipHeader(parser, versionFile.getDataset().getRealNode(), datasetDescriptor)
                .computeIfAbsent(projetCodeFromVersion, k -> new HashMap<>())
                .computeIfAbsent(siteCodeFromVersion, k -> new HashMap<String, List<RealNode>>());
        try {
            SortedMap<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> sequencesMapLines = new TreeMap<>();
            parser.getLine();
            String[] values = null;
            long lineCount = 1;
            int variableHeaderIndex = datasetDescriptor.getUndefinedColumn();

            Map<String, ControleCoherence> controlesCoherenceMap = controleCoherenceDAO.getControleCoherenceBySitecodeAndDatatypecode(LacsUtils.getSiteFromDataset(versionFile.getDataset()).getCode(), LacsUtils.getDatatypeFromDataset(versionFile.getDataset()).getCode());
            Map<String, VariableGLACPE> measuredVariablesMap = new HashMap<String, VariableGLACPE>();
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getMessages());
            }
// On parcourt chaque ligne du fichier
            while ((values = parser.getLine()) != null) {

                CleanerValues cleanerValues = new CleanerValues(values);
                lineCount++;
                List<VariableValue> variablesValues = new LinkedList<VariableValue>();

                // On parcourt chaque colonne d'une ligne
                String projetCode = Utils.createCodeFromString(cleanerValues.nextToken());
                if (!Utils.createCodeFromString(projetCode).equals(projetCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_PROJET_EXPECTED), projetCodeFromVersion, lineCount, 1, projetCode)));
                }
                String nomSite = cleanerValues.nextToken();
                if (!Utils.createCodeFromString(nomSite).equals(siteCodeFromVersion)) {
                    errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_SITE_EXPECTED), siteCodeFromVersion, lineCount, 2, nomSite)));
                }
                String plateformeCode = Utils.createCodeFromString(cleanerValues.nextToken());
                LocalDate datePrelevement = readDate(errorsReport, lineCount, 4, cleanerValues, datasetDescriptor);

                String outilMesureCode = Utils.createCodeFromString(cleanerValues.nextToken());
                String commentaires = cleanerValues.nextToken();
                commentaires = commentaires.substring(0, Math.min(254, commentaires.length()));
                LocalTime heure = readTime(errorsReport, lineCount, 7, cleanerValues, datasetDescriptor);

                //Traitement de la profondeur
                Float profondeur = readDepth(errorsReport, controlesCoherenceMap, lineCount, 8, cleanerValues);

                for (int actualVariableArray = variableHeaderIndex; actualVariableArray < values.length; actualVariableArray++) {
                    String value = values[actualVariableArray].replaceAll(" ", "");
                    try {
                        RealNode realNode = Optional
                                .of(this.datasetDescriptor.getColumns().get(actualVariableArray))
                                .map(c -> Utils.createCodeFromString(c.getName()))
                                .map(n -> realNodes.get(n))
                                .map(v -> v.get(0))
                                .orElse(null);
                        if (realNode != null) {

                            VariableGLACPE variable = ((DatatypeVariableUniteGLACPE) realNode.getNodeable()).getVariable();
                            if (controlesCoherenceMap.get(variable.getCode()) != null && value != null && !value.trim().isEmpty()) {
                                testValueCoherence(Float.valueOf(value), controlesCoherenceMap.get(variable.getCode()).getValeurMin(), controlesCoherenceMap.get(variable.getCode()).getValeurMax(), lineCount, actualVariableArray + 1);
                            }
                            variablesValues.add(new VariableValue(realNode, values[actualVariableArray].replaceAll(" ", "")));
                        } else {
                            errorsReport.addException(new BadValueTypeException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE), actualVariableArray + 1, this.datasetDescriptor.getColumns().get(actualVariableArray).getName(), datatypeCode, projetCodeFromVersion, siteCodeFromVersion)));

                        }
                    } catch (NumberFormatException e) {
                        errorsReport.addException(new BadExpectedValueException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_FLOAT_VALUE_EXPECTED), lineCount, actualVariableArray + 1, value)));
                    } catch (BadExpectedValueException e) {
                        errorsReport.addException(e);
                    }
                }

                LineRecord line = new LineRecord(nomSite, plateformeCode, datePrelevement, outilMesureCode, commentaires, profondeur, heure, variablesValues, lineCount, projetCode);
                sequencesMapLines
                        .computeIfAbsent(datePrelevement, k -> new TreeMap<>())
                        .computeIfAbsent(String.format("%s,%s", plateformeCode, outilMesureCode), k -> new TreeMap<>())
                        .computeIfAbsent(profondeur, k -> new LinkedList<>())
                        .add(line);

            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.buildHTMLMessages());
            }

            Allocator allocator = Allocator.getInstance();

            int count = 0;
            allocator.allocate("publish", versionFile.getFileSize());
            Iterator<Entry<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>>> iterator = sequencesMapLines.entrySet().iterator();
            while (iterator.hasNext()) {

                final Entry<LocalDate, SortedMap<String, SortedMap<Float, List<LineRecord>>>> entry = iterator.next();
                SortedMap<String, SortedMap<Float, List<LineRecord>>> sequenceLines = entry.getValue();
                LocalDate datePrelevement = entry.getKey();
                if (!sequenceLines.isEmpty()) {
                    try {
                        buildSequence(datePrelevement, projetCodeFromVersion, siteCodeFromVersion, sequenceLines, versionFile, errorsReport);
                        logger.debug(String.format("%d - %s", count++, datePrelevement));

                        // Très important à cause de problèmes de performances
                        sequenceSondeMultiDAO.flush();
                        versionFile = (VersionFile) versionFileDAO.merge(versionFile);
                    } catch (InsertionDatabaseException e) {
                        errorsReport.addException(e);
                    }
                }
                iterator.remove();
            }

            if (errorsReport.hasErrors()) {
                throw new PersistenceException(errorsReport.buildHTMLMessages());
            }

        } catch (Exception e) {

            throw new BusinessException(e);
        }

    }

    private Float readDepth(ErrorsReport errorsReport, Map<String, ControleCoherence> controlesCoherenceMap, long lineCount, int columnNumber, CleanerValues cleanerValues) {
        String profondeurString = cleanerValues.nextToken();
        Float profondeur = profondeurString.length() > 0 ? Float.parseFloat(profondeurString) : null;
        if (profondeur != null) {
            try {
                profondeur = Float.parseFloat(profondeurString);

                if (!controlesCoherenceMap.isEmpty() && controlesCoherenceMap.get(VARIABLE_PROFONDEUR_MESUREE) != null) {
                    testValueCoherence(
                            profondeur,
                            controlesCoherenceMap.get(VARIABLE_PROFONDEUR_MESUREE).getValeurMin(),
                            controlesCoherenceMap.get(VARIABLE_PROFONDEUR_MESUREE).getValeurMax(),
                            lineCount,
                            8);
                }
            } catch (BadExpectedValueException e) {
                errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DEPTH), profondeurString, lineCount, columnNumber)));
            }
        }
        return profondeur;
    }

    private LocalTime readTime(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String timeString;
        LocalTime time = null;
        timeString = cleanerValues.nextToken();
        try {
            time = Strings.isNullOrEmpty(timeString) ? null
                    : DateUtil.readLocalTimeFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), timeString);
        } catch (DateTimeParseException e) {
            if (Strings.isNullOrEmpty(timeString)) {
                return null;
            }
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_TIME), timeString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return time;

    }

    private LocalDate readDate(ErrorsReport errorsReport, long lineCount, int columnNumber, CleanerValues cleanerValues, DatasetDescriptor datasetDescriptor) {
        String dateString;
        LocalDate date = null;
        dateString = cleanerValues.nextToken();
        try {
            //PROPERTY_MSG_INVALID_DATE=%s n'est pas un format de date valide à la ligne %d colonne %d. La date doit-être au format %s
            date = Strings.isNullOrEmpty(dateString) ? null
                    : DateUtil.readLocalDateFromText(datasetDescriptor.getColumns().get(cleanerValues.currentTokenIndex() - 1).getFormatType(), dateString);
        } catch (DateTimeParseException e) {
            errorsReport.addException(new BusinessException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_INVALID_DATE), dateString, lineCount, columnNumber, DateUtil.DD_MM_YYYY)));
        }
        return date;

    }

    /**
     * @param parser
     * @param columns
     * @param datasetDescriptorGLACPE
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected Map<String, Map<String, Map<String, List<RealNode>>>> buildVariablesHeaderAndSkipHeader(final CSVParser parser, final RealNode realNode, final DatasetDescriptor datasetDescriptorGLACPE) {
        return datatypeVariableUniteGLACPEDAO.getRealNodesFromDatatypeCode(datatypeCode);
    }

    private void buildSequence(LocalDate datePrelevement, String projetCode, String siteCode, SortedMap<String, SortedMap<Float, List<LineRecord>>> sousSequencesMap, VersionFile versionFile, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        ProjetSite projetSite = projetSiteDAO.getBySiteCodeAndProjetCode(siteCode, projetCode).orElse(null);
        SequenceSondeMulti sequenceSondeMulti = new SequenceSondeMulti();
        sequenceSondeMulti.setDatePrelevement(datePrelevement);

        sequenceSondeMulti.setVersionFile(versionFile);

        for (Entry<String, SortedMap<Float, List<LineRecord>>> sousSequenceEntry : sousSequencesMap.entrySet()) {
            try {
                SortedMap<Float, List<LineRecord>> mesureMapLine = sousSequenceEntry.getValue();
                String key = sousSequenceEntry.getKey();
                String[] split = key.split(",");
                String platformeCode = split.length == 2 ? split[0] : "";
                String outilCode = split.length == 2 ? split[1] : "";
                buildSousSequence(siteCode, platformeCode, outilCode, mesureMapLine, sequenceSondeMulti, errorsReport);

            } catch (InsertionDatabaseException e) {
                errorsReport.addException(e);
            }
        }
        if (!errorsReport.hasErrors()) {
            try {
                sequenceSondeMultiDAO.saveOrUpdate(sequenceSondeMulti);
            } catch (ConstraintViolationException e) {
                String message = String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getUTCDateTextFromLocalDateTime(datePrelevement, DateUtil.DD_MM_YYYY));
                errorsReport.addException(new BusinessException(message));
            }
        }
    }

    private void buildSousSequence(String siteCode, String plateformeCode, String outilsMesureCode, SortedMap<Float, List<LineRecord>> mesureMapLine, SequenceSondeMulti sequenceSondeMulti, ErrorsReport errorsReport) throws PersistenceException,
            InsertionDatabaseException {
        LineRecord firstLine = mesureMapLine.values()
                .stream()
                .map(
                        l -> l
                                .stream()
                                .filter(li -> li != null)
                                .findAny()
                                .orElse(null)
                )
                .filter(li -> li != null)
                .findAny()
                .orElse(new LineRecord());

        Plateforme plateforme = null;
        plateforme = plateformeDAO.getByNKey(plateformeCode, siteCode).orElse(null);
        if (plateforme == null) {
            InsertionDatabaseException insertionDatabaseException = new InsertionDatabaseException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_ERROR_PLATEFORME_INVALID), plateformeCode, firstLine.getOriginalLineNumber()));
            throw insertionDatabaseException;
        }

        OutilsMesure outilsMesure = outilsMesureDAO.getByCode(outilsMesureCode).orElse(null);
        if (outilsMesure == null) {
            errorsReport.addException((new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode))));
            throw new PersistenceException(String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS), outilsMesureCode));
        }

        SousSequenceSondeMulti sousSequenceSondeMulti = new SousSequenceSondeMulti();
        sousSequenceSondeMulti.setPlateforme(plateforme);
        sousSequenceSondeMulti.setSequence(sequenceSondeMulti);
        sousSequenceSondeMulti.setCommentaires(firstLine.getCommentaires());
        sousSequenceSondeMulti.setOutilsMesure(outilsMesure);
        sequenceSondeMulti.getSousSequences().add(sousSequenceSondeMulti);

        for (Entry<Float, List<LineRecord>> profondeurEntry : mesureMapLine.entrySet()) {
            Float profondeur = profondeurEntry.getKey();
            List<LineRecord> profondeurLines = profondeurEntry.getValue();
            buildMesure(profondeur, profondeurLines.get(0).getHeure(), profondeurLines, sousSequenceSondeMulti, errorsReport);
        }
    }

    private void buildMesure(Float profondeur, LocalTime heure, List<LineRecord> profondeurLines, SousSequenceSondeMulti sousSequenceSondeMulti, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {
        MesureSondeMulti mesureSondeMulti = null;

        mesureSondeMulti = new MesureSondeMulti();
        mesureSondeMulti.setSousSequence(sousSequenceSondeMulti);
        sousSequenceSondeMulti.getMesures().add(mesureSondeMulti);
        mesureSondeMulti.setProfondeur(profondeur);
        mesureSondeMulti.setHeure(heure);
        mesureSondeMulti.setLigneFichierEchange(profondeurLines.get(0).getOriginalLineNumber());

        if (profondeurLines.size() > 1) {
            errorsReport.addException(new BusinessException((String.format(getGLACPEMessage(getLocalizationManager(), PROPERTY_MSG_DOUBLON_LINE), mesureSondeMulti.getLigneFichierEchange()))));
        } else {
            for (LineRecord profondeurLine : profondeurLines) {
                for (VariableValue variableValue : profondeurLine.getVariablesValues()) {
                    ValeurMesureSondeMulti valeurMesureSondeMulti = new ValeurMesureSondeMulti();
                    valeurMesureSondeMulti.setRealNode(mgaRecorder.getRealNodeById(variableValue.getRealNode().getId()).orElse(null));

                    if (variableValue.getValue() == null || variableValue.getValue().length() == 0) {
                        valeurMesureSondeMulti.setValeur(null);
                    } else {

                        Float value = Float.parseFloat(variableValue.getValue());
                        valeurMesureSondeMulti.setValeur(value);
                    }

                    valeurMesureSondeMulti.setMesure(mesureSondeMulti);
                    mesureSondeMulti.getValeurs().add(valeurMesureSondeMulti);

                }
            }
        }
    }

    /**
     *
     * @return @throws IOException
     * @throws SAXException
     */
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(ISondeMultiDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    /**
     *
     * @param sequenceSondeMultiDAO
     */
    public void setSequenceSondeMultiDAO(ISequenceSondeMultiDAO sequenceSondeMultiDAO) {
        this.sequenceSondeMultiDAO = sequenceSondeMultiDAO;
    }

    /**
     *
     * @param sondeMultiDAO
     */
    public void setSondeMultiDAO(ISondeMultiDAO sondeMultiDAO) {
        this.sondeMultiDAO = sondeMultiDAO;
    }

    /**
     *
     * @param controleCoherenceDAO
     */
    public void setControleCoherenceDAO(IControleCoherenceDAO controleCoherenceDAO) {
        this.controleCoherenceDAO = controleCoherenceDAO;
    }

    /**
     *
     * @param projetSiteDAO
     */
    public void setProjetSiteDAO(IProjetSiteDAO projetSiteDAO) {
        this.projetSiteDAO = projetSiteDAO;
    }

    /**
     *
     * @param plateformeDAO
     */
    public void setPlateformeDAO(IPlateformeDAO plateformeDAO) {
        this.plateformeDAO = plateformeDAO;
    }

    /**
     *
     * @param outilsMesureDAO
     */
    public void setOutilsMesureDAO(IOutilsMesureDAO outilsMesureDAO) {
        this.outilsMesureDAO = outilsMesureDAO;
    }

    /**
     *
     * @param datatypeVariableUniteGLACPEDAO
     */
    public void setDatatypeVariableUniteGLACPEDAO(IDatatypeVariableUniteGLACPEDAO datatypeVariableUniteGLACPEDAO) {
        this.datatypeVariableUniteGLACPEDAO = datatypeVariableUniteGLACPEDAO;
    }
}

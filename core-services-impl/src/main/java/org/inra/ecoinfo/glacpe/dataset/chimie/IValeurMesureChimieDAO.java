package org.inra.ecoinfo.glacpe.dataset.chimie;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.ValeurMesureChimie;

/**
 *
 * @author ptcherniati
 */
public interface IValeurMesureChimieDAO {

    /**
     *
     * @param variableCode
     * @param profondeurMin
     * @param profondeurMax
     * @param projetCode
     * @param date
     * @param plateformeCode
     * @return
     */
    Optional<ValeurMesureChimie> getByVariableCodeAndProfondeurAndProjetCodeAndDateAndPlateformeCode(String variableCode, Float profondeurMin, Float profondeurMax, String projetCode, LocalDate date, String plateformeCode);

}

/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.typeplateforme;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPATypePlateformeDAO extends AbstractJPADAO<TypePlateforme> implements ITypePlateformeDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<TypePlateforme> getByCode(String code) {
        CriteriaQuery<TypePlateforme> query = builder.createQuery(TypePlateforme.class);
        Root<TypePlateforme> typePlateforme = query.from(TypePlateforme.class);
        query
                .select(typePlateforme)
                .where(builder.equal(typePlateforme.get(TypePlateforme_.code), Utils.createCodeFromString(code)));
        return getOptional(query);
    }

}

package org.inra.ecoinfo.glacpe.extraction.contenustomacaux.impl;

import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.glacpe.dataset.contenustomacaux.IContenuStomacauxDatatypeManager;
import org.inra.ecoinfo.glacpe.extraction.contenustomacaux.IContenuStomacauxDAO;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class DefaultContenuStomacauxDatatypeManager extends MO implements IContenuStomacauxDatatypeManager {

    /**
     *
     */
    protected IContenuStomacauxDAO contenuStomacauxDAO;

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    

    /**
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Collection<PlateformeProjetVO> getAvailablePlatforms() {
        return contenuStomacauxDAO.getAvailablePlatforms(policyManager.getCurrentUser());
    }

    /**
     *
     * @param selectedPlatforms
     * @param plateformesIds
     * @param firstDate
     * @param endDate
     * @return
     * @throws BusinessException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates) {
        return contenuStomacauxDAO.getAvailableVariables(policyManager.getCurrentUser(), selectedPlatforms, selectedDates);
    }
}

/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.glacpe.refdata.taxon.phytoplancton;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.AbstractTaxonRecorder;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author "Guillaume Enrico"
 * 
 */
public class Recorder extends AbstractTaxonRecorder {

    private static final String TAXON_PHYTO = "Phytoplancton";

    /**
     *
     * @return
     */
    @Override
    protected String getTheme() {
        return TAXON_PHYTO;
    }

    /**
     *
     * @return
     * @throws PersistenceException
     */
    @Override
    protected List<String> getAllElementsByTheme() throws PersistenceException {
        return taxonDAO.getTaxonNameByTheme(TAXON_PHYTO);
    }

    /**
     *
     * @throws PersistenceException
     */
    @Override
    protected void updateNamesProprieteTaxonPossibles() throws PersistenceException {
        List<ProprieteTaxon> proprieteTaxons = proprieteTaxonDAO.getAllProprietesTaxonsByType(TAXON_PHYTO);
        Collections.sort(proprieteTaxons, new Comparator<ProprieteTaxon>() {

            @Override
            public int compare(ProprieteTaxon o1, ProprieteTaxon o2) {
                return o1.getOrdreAffichage().compareTo(o2.getOrdreAffichage());
            }
        });
        String[] namesProprieteTaxonPossibles = new String[proprieteTaxons.size()];
        int index = 0;
        for (ProprieteTaxon ptaxon : proprieteTaxons) {
            namesProprieteTaxonPossibles[index++] = ptaxon.getNom();
            if (ptaxon.getIsQualitative()) {
                List<ValeurQualitative> valuesProprietePossibles = valeurQualitativeDAO.getByCode(ptaxon.getCode());
                String[] valeursProprieteTaxonPossibles = new String[valuesProprietePossibles.size() + 1];
                valeursProprieteTaxonPossibles[0] = EMPTY_STRING;
                int index2 = 1;
                for (ValeurQualitative valueProprietePossible : valuesProprietePossibles) {
                    valeursProprieteTaxonPossibles[index2++] = valueProprietePossible.getValeur();
                }
                mapListProprietesPossibles.put(ptaxon.getNom(), valeursProprieteTaxonPossibles);
            }
        }
        this.namesProprieteTaxonsPossibles = namesProprieteTaxonPossibles;
    }

    @Override
    protected List<Taxon> getAllElements() {
        return taxonDAO.getTaxonByTheme(TAXON_PHYTO);
    }
}

package org.inra.ecoinfo.glacpe.dataset;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface ITestValues extends Serializable {

    /**
     *      * test the data of a file.
     * 
     * @param startline
     *            int the startline
     * @param parser
     *            the parser
     * @param versionFile
     * @link(VersionFile)
     * @link(ISessionPropertiesGLACPE) the session properties
     * @param encoding
     *            the encoding
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param datasetDescriptor
     * @link(DatasetDescriptorGLACPE) the dataset descriptor
     * @param datatypeName
     * @link(String) the datatype name
     * @throws BusinessException
     *             the business exception {@link VersionFile} the version file {@link ISessionPropertiesGLACPE} the session properties {@linkBadsFormatsReport} the bads formats report {@link DatasetDescriptorGLACPE} the dataset descriptor
     *             {@link String} the datatype name
     */
    void testValues(long startline, CSVParser parser, VersionFile versionFile, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor, String datatypeName) throws BusinessException;
}
package org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import static org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor.*;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl.ChloroAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.chloro.impl.ChloroRawsDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl.TranspAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.conditionprelevement.impl.TranspRawsDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl.PPAggregatedDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.ppchlorotransp.productionprimaire.impl.PPRawsDatasExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DatasRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class PPChloroTranspOutputsBuildersResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder {

    private static final String EXTRACTION = "extraction";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     *
     */
    protected static final String SEPARATOR_TEXT = "_";

    /**
     *
     */
    protected static final String EXTENSION_ZIP = ".zip";

    /**
     *
     */
    protected IOutputBuilder chloroRawsDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder ppRawsDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder ppAggregatedDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder ppMeanDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder chloroAggregatedDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder chloroMeanDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder transpRawsDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder transpAggregatedDatasOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder requestReminderOutputBuilder;

    /**
     *
     */
    protected IOutputBuilder ppChloroTranspOutputBuilderSandreChloro;

    /**
     *
     */
    protected IOutputBuilder ppChloroTranspOutputBuilderSandrePP;

    /**
     *
     */
    protected IOutputBuilder ppChloroTranspOutputBuilderSandreTrans;
    /**
     * The configuration.
     */
    private IFileCompConfiguration fileCompConfiguration;

    private void addOutputBuilderByCondition(Boolean condition, IOutputBuilder outputBuilder, List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(metadatasMap);
        boolean hasVariablespp = hasVariable(dvus ,PPChloroTranspParameter.PRODUCTION_PRIMAIRE);
        boolean hasVariablesChloro =  hasVariable(dvus ,PPChloroTranspParameter.CHLOROPHYLLE) ;
        boolean hasVariablesTransp = dvus.stream()
                .anyMatch(dvu -> dvu.getCode().startsWith("conditions_prelevements_transparence"));
        Map<String, Boolean> results = (Map<String, Boolean>) metadatasMap.get(DefaultPPChloroTranspExtractor.CST_RESULTS);
        List<IOutputBuilder> outputsBuilders = new LinkedList<IOutputBuilder>();

        outputsBuilders.add(requestReminderOutputBuilder);

        DatasRequestParamVO datasRequestParamVO = getDatasRequestParam(metadatasMap);

        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablespp && results.get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE), ppRawsDatasOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition((datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()) && hasVariablespp && results.get(PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE),
                ppAggregatedDatasOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition(datasRequestParamVO.getDataBalancedByDepth() && hasVariablespp && results.get(PPAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE), ppMeanDatasOutputBuilder, outputsBuilders);

        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablesChloro && results.get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE), chloroRawsDatasOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition((datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()) && hasVariablesChloro && results.get(ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE),
                chloroAggregatedDatasOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition(datasRequestParamVO.getDataBalancedByDepth() && hasVariablesChloro && results.get(ChloroAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE), chloroMeanDatasOutputBuilder, outputsBuilders);

        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablesTransp && results.get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE), transpRawsDatasOutputBuilder, outputsBuilders);
        addOutputBuilderByCondition((datasRequestParamVO.getMaxValueAndAssociatedDepth() || datasRequestParamVO.getMinValueAndAssociatedDepth()) && hasVariablesTransp && results.get(TranspAggregatedDatasExtractor.CST_RESULT_EXTRACTION_CODE),
                transpAggregatedDatasOutputBuilder, outputsBuilders);

        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablespp && results.get(PPRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE), ppChloroTranspOutputBuilderSandrePP, outputsBuilders);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablesChloro && results.get(ChloroRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE), ppChloroTranspOutputBuilderSandreChloro, outputsBuilders);
        addOutputBuilderByCondition(datasRequestParamVO.getRawData() && hasVariablesTransp && results.get(TranspRawsDatasExtractor.CST_RESULT_EXTRACTION_CODE), ppChloroTranspOutputBuilderSandreTrans, outputsBuilders);

        return outputsBuilders;
    }

    /**
     *
     * @param ppChloroTranspOutputBuilderSandreChloro
     */
    public void setPpChloroTranspOutputBuilderSandreChloro(IOutputBuilder ppChloroTranspOutputBuilderSandreChloro) {
        this.ppChloroTranspOutputBuilderSandreChloro = ppChloroTranspOutputBuilderSandreChloro;
    }

    /**
     *
     * @param ppChloroTranspOutputBuilderSandrePP
     */
    public void setPpChloroTranspOutputBuilderSandrePP(IOutputBuilder ppChloroTranspOutputBuilderSandrePP) {
        this.ppChloroTranspOutputBuilderSandrePP = ppChloroTranspOutputBuilderSandrePP;
    }

    /**
     *
     * @param ppChloroTranspOutputBuilderSandreTrans
     */
    public void setPpChloroTranspOutputBuilderSandreTrans(IOutputBuilder ppChloroTranspOutputBuilderSandreTrans) {
        this.ppChloroTranspOutputBuilderSandreTrans = ppChloroTranspOutputBuilderSandreTrans;
    }

    /**
     *
     * @param chloroRawsDatasOutputBuilder
     */
    public void setChloroRawsDatasOutputBuilder(IOutputBuilder chloroRawsDatasOutputBuilder) {
        this.chloroRawsDatasOutputBuilder = chloroRawsDatasOutputBuilder;
    }

    /**
     *
     * @param ppRawsDatasOutputBuilder
     */
    public void setPpRawsDatasOutputBuilder(IOutputBuilder ppRawsDatasOutputBuilder) {
        this.ppRawsDatasOutputBuilder = ppRawsDatasOutputBuilder;
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     *
     * @param ppAggregatedDatasOutputBuilder
     */
    public void setPpAggregatedDatasOutputBuilder(IOutputBuilder ppAggregatedDatasOutputBuilder) {
        this.ppAggregatedDatasOutputBuilder = ppAggregatedDatasOutputBuilder;
    }

    /**
     *
     * @param transpRawsDatasOutputBuilder
     */
    public void setTranspRawsDatasOutputBuilder(IOutputBuilder transpRawsDatasOutputBuilder) {
        this.transpRawsDatasOutputBuilder = transpRawsDatasOutputBuilder;
    }

    @SuppressWarnings("unchecked")
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        RObuildZipOutputStream rObuildZipOutputStream = super.buildOutput(parameters);
        List<IOutputBuilder> roBuildZipOutputStream = resolveOutputsBuilders(parameters.getParameters());
        ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream();
        NoExtractionResultException noDataToExtract = null;
        for (IOutputBuilder iOutputBuilder : roBuildZipOutputStream) {
            try {
                iOutputBuilder.buildOutput(parameters);
            } catch (NoExtractionResultException e) {
                noDataToExtract = e;
            }
        }
        for (Map<String, File> filesMap : ((DefaultParameter) parameters).getFilesMaps()) {
            try {
                embedInZip(zipOutputStream, filesMap);
            } catch (IOException e) {
                throw new BusinessException("file not found ", e);
            }
        }
        List<FileComp> fileComps = (List<FileComp>) parameters.getParameters().get(FileComp.class.getSimpleName());
        Map<String, File> filecompsToEmbed = new TreeMap<String, File>();
        if (fileComps != null && !fileComps.isEmpty()) {
            for (FileComp fileComp : fileComps) {
                String path = fileComp.getAbsoluteFilePath(fileCompConfiguration.getRepositoryFiles());
                File fileToEmbed = new File(path);
                if (fileToEmbed.exists() && fileToEmbed.canRead()) {
                    String fileName = "tmp/".concat(fileComp.getFileName());
                    int count = 1;
                    while (filecompsToEmbed.containsKey(fileName)) {
                        count++;
                        fileName = fileName.matches("(.*)( \\([0-9]*\\))$") ? fileName.replaceAll("(.*)(\\([0-9]*\\))$", String.format("$1 (%d)", count)) : fileName.concat(String.format(" (%d)", count));
                    }
                    File copyFile = new File(fileName);
                    try {
                        FileUtils.copyFile(fileToEmbed, copyFile);
                        fileToEmbed = copyFile;
                    } catch (IOException e) {
                        Logger.getAnonymousLogger().info("can't copy file " + path);
                        continue;
                    }
                    filecompsToEmbed.put(fileName, fileToEmbed);
                }
            }
            try {
                if (!filecompsToEmbed.isEmpty()) {
                    embedInZip(zipOutputStream, filecompsToEmbed);
                }
            } catch (final IOException e) {
                throw new BusinessException("file not found ", e);
            }
        }

        try {
            zipOutputStream.flush();
            zipOutputStream.close();
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        }
        if (noDataToExtract != null) {
            throw noDataToExtract;
        }
        return rObuildZipOutputStream;
    }

    private void embedInZip(ZipOutputStream zipOutputStream, Map<String, File> filesMap) throws IOException {
        byte[] buf = new byte[1024];
        for (String fileName : filesMap.keySet()) {
            File file = filesMap.get(fileName);

            InputStream in = new BufferedInputStream(new FileInputStream(file));

            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            int len;
            while ((len = in.read(buf, 0, 1024)) > 0) {
                zipOutputStream.write(buf, 0, len);
            }
            file.delete();
            zipOutputStream.closeEntry();
            in.close();
        }
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     *
     * @param transpAggregatedDatasOutputBuilder
     */
    public void setTranspAggregatedDatasOutputBuilder(IOutputBuilder transpAggregatedDatasOutputBuilder) {
        this.transpAggregatedDatasOutputBuilder = transpAggregatedDatasOutputBuilder;
    }

    /**
     *
     * @param ppMeanDatasOutputBuilder
     */
    public void setPpMeanDatasOutputBuilder(IOutputBuilder ppMeanDatasOutputBuilder) {
        this.ppMeanDatasOutputBuilder = ppMeanDatasOutputBuilder;
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getChloroAggregatedDatasOutputBuilder() {
        return chloroAggregatedDatasOutputBuilder;
    }

    /**
     *
     * @param chloroAggregatedDatasOutputBuilder
     */
    public void setChloroAggregatedDatasOutputBuilder(IOutputBuilder chloroAggregatedDatasOutputBuilder) {
        this.chloroAggregatedDatasOutputBuilder = chloroAggregatedDatasOutputBuilder;
    }

    /**
     *
     * @return
     */
    public IOutputBuilder getChloroMeanDatasOutputBuilder() {
        return chloroMeanDatasOutputBuilder;
    }

    /**
     *
     * @param chloroMeanDatasOutputBuilder
     */
    public void setChloroMeanDatasOutputBuilder(IOutputBuilder chloroMeanDatasOutputBuilder) {
        this.chloroMeanDatasOutputBuilder = chloroMeanDatasOutputBuilder;
    }

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }
}

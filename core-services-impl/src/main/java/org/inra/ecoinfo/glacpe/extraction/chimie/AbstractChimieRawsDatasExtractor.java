package org.inra.ecoinfo.glacpe.extraction.chimie;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.glacpe.dataset.chimie.IChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.IValeurMesureChimieDAO;
import org.inra.ecoinfo.glacpe.dataset.chimie.entity.MesureChimie;
import org.inra.ecoinfo.glacpe.extraction.impl.AbstractGLACPEExtractor;
import org.inra.ecoinfo.glacpe.extraction.vo.DepthRequestParamVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractChimieRawsDatasExtractor extends AbstractGLACPEExtractor {

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";
    /**
     *
     */
    protected IChimieDAO chimieDAO;

    /**
     *
     */
    protected IValeurMesureChimieDAO valeurMesureChimieDAO;

    /**
     *
     * @param chimieDAO
     */
    public void setChimieDAO(IChimieDAO chimieDAO) {
        this.chimieDAO = chimieDAO;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatas) throws NoExtractionResultException, BusinessException {

        Map<String, List> extractedDatasMap = new HashMap<String, List>();
        DepthRequestParamVO depthRequestParamVO = getDepthsRequestParam(requestMetadatas);
        SortedMap<Projet, SortedMap<SiteGLACPE, SortedSet<Plateforme>>> selectedPlateformes = getPlatforms(requestMetadatas);
        List<IntervalDate> datesRequestParamVO = getIntervalsDates(requestMetadatas);
        List<DatatypeVariableUniteGLACPE> dvus = getDVUs(requestMetadatas);

        List<MesureChimie> mesuresChimies = chimieDAO.extractDatas(policyManager.getCurrentUser(), selectedPlateformes, datesRequestParamVO, depthRequestParamVO, dvus);
        if (mesuresChimies == null || mesuresChimies.size() == 0) {
            throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        extractedDatasMap.put(MAP_INDEX_0, mesuresChimies);
        return extractedDatasMap;
    }

    /**
     *
     * @param valeurMesureChimieDAO
     */
    public void setValeurMesureChimieDAO(IValeurMesureChimieDAO valeurMesureChimieDAO) {
        this.valeurMesureChimieDAO = valeurMesureChimieDAO;
    }

    @Override
    public void setExtraction(Extraction extraction) {

    }
    
}

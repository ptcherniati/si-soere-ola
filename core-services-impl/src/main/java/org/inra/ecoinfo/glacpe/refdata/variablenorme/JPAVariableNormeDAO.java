/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.variablenorme;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class JPAVariableNormeDAO extends AbstractJPADAO<VariableNorme> implements IVariableNormeDAO {

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<VariableNorme> getByCode(String code) {
        CriteriaQuery<VariableNorme> query = builder.createQuery(VariableNorme.class);
        Root<VariableNorme> variableNorme = query.from(VariableNorme.class);
        query
                .select(variableNorme)
                .where(builder.equal(variableNorme.get(VariableNorme_.code), code));
        return getOptional(query);
    }

}

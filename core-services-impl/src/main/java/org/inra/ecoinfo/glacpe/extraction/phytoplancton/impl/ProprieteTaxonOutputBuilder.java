package org.inra.ecoinfo.glacpe.extraction.phytoplancton.impl;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.ITaxonDAO;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ProprieteTaxonOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH_PHYTOPLANCTON = "org.inra.ecoinfo.glacpe.extraction.phytoplancton.messages";

    private static final String HEADER_DATA = "PROPERTY_MSG_HEADER_PROPERTY_DATA";
    private static final String FILENAME_PROPRIETETAXON = "propriete_taxon";

    /**
     *
     */
    public static String CST_RESULT_EXTRACTION_CODE = "proprieteTaxonOutputBuilder";

    /**
     *
     */
    protected List<String> proprieteTaxonName;

    /**
     *
     */
    protected ITaxonDAO taxonDAO;

    @SuppressWarnings("unchecked")
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        proprieteTaxonName = Lists.newArrayList();
        Properties propertiesProprieteName = localizationManager.newProperties(ProprieteTaxon.TABLE_NAME, "nom");
        List<Taxon> selectedTaxon = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH_PHYTOPLANCTON, HEADER_DATA)));
        for (Taxon taxon : selectedTaxon) {
            List<TaxonProprieteTaxon> taprotas = taxonDAO.getTaxonProprieteTaxonByTaxonId(taxon.getId());
            if (taprotas != null && !taprotas.isEmpty()) {
                for (TaxonProprieteTaxon taprota : taprotas) {
                    if (!proprieteTaxonName.contains(taprota.getProprieteTaxon().getNom())) {
                        proprieteTaxonName.add(taprota.getProprieteTaxon().getNom());
                    }
                }
            }
        }
        for (String proname : proprieteTaxonName) {
            String localizedProprieteName = propertiesProprieteName.getProperty(proname);
            if (Strings.isNullOrEmpty(localizedProprieteName)) {
                localizedProprieteName = proname;
            }
            stringBuilder.append(String.format(";%s", localizedProprieteName));
        }
        return stringBuilder.toString();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        List<Taxon> selectedTaxons = (List<Taxon>) requestMetadatasMap.get(Taxon.class.getSimpleName());
        Properties propertiesValeurQualitativeValue = localizationManager.newProperties(ValeurQualitative.NAME_ENTITY_JPA, "value");
        Map<String, File> reminderMap = new HashMap<String, File>();
        File reminderFile = buildOutputFile(FILENAME_PROPRIETETAXON, EXTENSION_CSV);
        PrintStream proprietePrintStream;

        try {
            proprietePrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.name());
            proprietePrintStream.println(headers);

            for (Taxon taxon : selectedTaxons) {
                List<TaxonProprieteTaxon> taprotas = taxonDAO.getTaxonProprieteTaxonByTaxonId(taxon.getId());
                Map<String, TaxonProprieteTaxon> mapProprietesTaxons = new HashMap<String, TaxonProprieteTaxon>();
                for (TaxonProprieteTaxon tpt : taprotas) {
                    mapProprietesTaxons.put(tpt.getProprieteTaxon().getCode(), tpt);
                }
                if (taprotas != null && !taprotas.isEmpty()) {
                    proprietePrintStream.print(String.format("%s;", taxon.getNomLatin()));
                    for (String prota : proprieteTaxonName) {
                        TaxonProprieteTaxon taxprotax = mapProprietesTaxons.get(Utils.createCodeFromString(prota));
                        if (taxprotax != null) {
                            if (taxprotax.getProprieteTaxon().getValueType()) {
                                if (taxprotax.getValeurProprieteTaxon().getFloatValue() != null) {
                                    proprietePrintStream.print(String.format("%s;", taxprotax.getValeurProprieteTaxon().getFloatValue()));
                                } else {
                                    proprietePrintStream.print(";");
                                }
                            } else {
                                if (taxprotax.getProprieteTaxon().getIsQualitative()) {
                                    if (taxprotax.getValeurProprieteTaxon().getQualitativeValue() != null) {
                                        String localizedValue = propertiesValeurQualitativeValue.getProperty(taxprotax.getValeurProprieteTaxon().getQualitativeValue().getValeur());
                                        if (Strings.isNullOrEmpty(localizedValue)) {
                                            localizedValue = taxprotax.getValeurProprieteTaxon().getQualitativeValue().getValeur();
                                        }
                                        proprietePrintStream.print(String.format("%s;", localizedValue));
                                    } else {
                                        proprietePrintStream.print(";");
                                    }
                                } else {
                                    if (taxprotax.getValeurProprieteTaxon().getStringValue() != null) {
                                        proprietePrintStream.print(String.format("%s;", taxprotax.getValeurProprieteTaxon().getStringValue()));
                                    } else {
                                        proprietePrintStream.print(";");
                                    }
                                }
                            }
                        } else {
                            proprietePrintStream.print(";");
                        }
                    }
                    proprietePrintStream.println();
                }
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }

        reminderMap.put(FILENAME_PROPRIETETAXON, reminderFile);

        proprietePrintStream.flush();
        proprietePrintStream.close();

        return reminderMap;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        ((DefaultParameter) parameters).getFilesMaps().add(super.buildOutput(parameters, CST_RESULT_EXTRACTION_CODE));
        return null;

    }

    /**
     *
     * @param taxonDAO
     */
    public void setTaxonDAO(ITaxonDAO taxonDAO) {
        this.taxonDAO = taxonDAO;
    }
}

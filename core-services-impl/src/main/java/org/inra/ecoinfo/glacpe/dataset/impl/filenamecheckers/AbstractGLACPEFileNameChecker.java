package org.inra.ecoinfo.glacpe.dataset.impl.filenamecheckers;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.IFileNameChecker;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projet.IProjetDAO;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractGLACPEFileNameChecker extends AbstractFileNameChecker implements IFileNameChecker{

    /**
     *
     */
    protected static final String INVALID_FILE_NAME = "%s_%s_%s_%s_%s.csv";

    /**
     *
     */
    protected static final String PATTERN = "^(%s|.*?)_(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";
    /**
     * The pattern file name path @link(String).
     */
    protected static final String PATTERN_FILE_NAME_PATH = "%s_%s_%s_%s_%s#V%d#.csv";

    /**
     * The Constant CST_STRING_EMPTY @link(String).
     */
    protected static final String CST_STRING_EMPTY = "";
    static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileNameChecker.class.getName());

    /**
     *
     */
    protected IProjetDAO projetDAO;

    @Override
    public boolean isValidFileName(String fileName, VersionFile version) throws InvalidFileNameException {
        fileName = cleanFileName(fileName);
        String currentProject = LacsUtils.getProjetFromDataset(version.getDataset()).getCode();
        String currentSite = reWriteSitePath(LacsUtils.getSiteFromDataset(version.getDataset()).getCode());
        String currentDatatype = LacsUtils.getDatatypeFromDataset(version.getDataset()).getCode();
        Matcher splitFilename = Pattern.compile(String.format(PATTERN, currentProject, currentSite, currentDatatype)).matcher(fileName);
        testPath(currentProject, currentSite, currentDatatype, splitFilename);
        String projetName = Utils.createCodeFromString(splitFilename.group(1));
        testProject(currentProject, currentSite, currentDatatype, projetName);
        String siteName = reWriteSitePath(Utils.createCodeFromString(splitFilename.group(2)));
        testSite(version, currentProject, currentSite, currentDatatype, siteName);
        String datatypeName = Utils.createCodeFromString(splitFilename.group(3));
        testDatatype(currentProject, currentSite, currentDatatype, datatypeName);
        testDates(version, currentProject, currentSite, currentDatatype, splitFilename);
        return true;
    }

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param siteName
     * @throws InvalidFileNameException
     */
    protected void testSite(VersionFile version, String currentProject, String currentSite, String currentDatatype, String siteName) throws InvalidFileNameException {
        final Optional<Site> site = siteDAO.getByPath(Utils.createCodeFromString(siteName));
        if (!site.isPresent() && site.get().getCode().equals(siteName)) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }

    /**
     *
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected void testPath(String currentProject, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }

    /**
     *
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param projectName
     * @throws InvalidFileNameException
     */
    protected void testProject(String currentProject, String currentSite, String currentDatatype, String projectName) throws InvalidFileNameException {
        final Optional<Projet> projet = projetDAO.getByCode(projectName);
        if (!projet.isPresent()) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }


    /**
     *
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param datatypeName
     * @throws InvalidFileNameException
     */
    protected void testDatatype(String currentProject, String currentSite, String currentDatatype, String datatypeName) throws InvalidFileNameException {

        if (!datatypeDAO.getByCode(datatypeName).isPresent()) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentProject, currentSite, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }

    /**
     *
     * @param version
     * @param currentProject
     * @param currentSite
     * @param currentDatatype
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected abstract void testDates(VersionFile version, String currentProject, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException;

    /**
     *
     * @param projetDAO
     */
    public void setProjetDAO(IProjetDAO projetDAO) {
        this.projetDAO = projetDAO;
    }
}

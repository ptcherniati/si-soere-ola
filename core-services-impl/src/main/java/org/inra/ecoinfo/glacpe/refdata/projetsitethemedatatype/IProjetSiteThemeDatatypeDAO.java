package org.inra.ecoinfo.glacpe.refdata.projetsitethemedatatype;

import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
public interface IProjetSiteThemeDatatypeDAO extends IDAO<RealNode> {

    /**
     *
     * @return
     */
    List<RealNode> loadRealDatatypeNodes();
}

package org.inra.ecoinfo.glacpe.dataset.impl;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.inra.ecoinfo.dataset.AbstractRecorder;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.dataset.IDeleteRecord;
import org.inra.ecoinfo.glacpe.dataset.IProcessRecord;
import org.inra.ecoinfo.glacpe.dataset.IRecorderGLACPE;
import org.inra.ecoinfo.glacpe.dataset.ITestFormat;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.xml.sax.SAXException;

/**
 * The Class RecorderGLACPE.
 *  * This class is used to delete, test, upload and publish files
 *
 * @author Enrico Guillaume
 */
public class RecorderGLACPE extends AbstractRecorder implements IRecorderGLACPE {

    /**
     * The Constant GLACPE_DATASET_BUNDLE_NAME.
     */
    public static final String GLACPE_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.glacpe.dataset.messages";

    /**
     * The Constant CST_STRING_EMPTY.
     */
    public static final String CST_STRING_EMPTY = "";

    /**
     * The Constant CST_SPACE.
     */
    public static final String CST_SPACE = " ";

    /**
     * The Constant CST_DOT.
     */
    public static final String CST_DOT = ".";

    /**
     * The Constant CST_COMMA.
     */
    public static final String CST_COMMA = ",";

    // dataset-descripors_types
    /**
     * The Constant PROPERTY_CST_DATE_TYPE.
     */
    public static final String PROPERTY_CST_DATE_TYPE = "date";

    // dataset-descripors_types
    /**
     * The Constant PROPERTY_CST_DATE_TYPE.
     */
    public static final String PROPERTY_CST_TIME_TYPE = "time";

    /**
     * The Constant PROPERTY_CST_INTEGER_TYPE.
     */
    public static final String PROPERTY_CST_INTEGER_TYPE = "integer";

    /**
     * The Constant PROPERTY_CST_FLOAT_TYPE.
     */
    public static final String PROPERTY_CST_FLOAT_TYPE = "float";

    /**
     * The Constant PROPERTY_CST_FLOAT_TYPE.
     */
    public static final String PROPERTY_CST_PROJECT_TYPE = "projet";

    /**
     * The Constant PROPERTY_CST_FLOAT_TYPE.
     */
    public static final String PROPERTY_CST_SITE_TYPE = "site";

    /**
     * The Constant PROPERTY_CST_FLOAT_TYPE.
     */
    public static final String PROPERTY_CST_PLATFORM_TYPE = "plateforme";

    /**
     * The Constant PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS.
     */
    public static final String PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS";

    /**
     * The Constant PROPERTY_MSG_NO_DATA.
     */
    public static final String PROPERTY_MSG_NO_DATA = "PROPERTY_MSG_NO_DATA";

    /**
     * The Constant PROPERTY_MSG_INVALID_FLOAT_VALUE.
     */
    public static final String PROPERTY_MSG_INVALID_FLOAT_VALUE = "PROPERTY_MSG_INVALID_FLOAT_VALUE";

    /**
     * The Constant PROPERTY_MSG_INVALID_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_DATE = "PROPERTY_MSG_INVALID_DATE";

    /**
     * The Constant PROPERTY_MSG_INVALID_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_TIME = "PROPERTY_MSG_INVALID_TIME";

    /**
     * The Constant PROPERTY_MSG_INVALID_DATE.
     */
    public static final String PROPERTY_MSG_INVALID_ZONE = "PROPERTY_MSG_INVALID_ZONE";

    /**
     * The Constant PROPERTY_MSG_ERROR.
     */
    public static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";

    /**
     * The Constant PROPERTY_MSG_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_VALUE_EXPECTED = "PROPERTY_MSG_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_ERROR_BAD_FORMAT.
     */
    public static final String PROPERTY_MSG_ERROR_BAD_FORMAT = "PROPERTY_MSG_ERROR_BAD_FORMAT";

    /**
     * The Constant PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE, comming from KERNEL.
     */
    public static final String PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE = "PROPERTY_MSG_BAD_INTERVAL_DATE_VALUE";

    /**
     * The Constant PROPERTY_MSG_FLOAT_VALUE_EXPECTED.
     */
    public static final String PROPERTY_MSG_FLOAT_VALUE_EXPECTED = "PROPERTY_MSG_FLOAT_VALUE_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_PROJET_EXPECTED.
     */
    public static final String PROPERTY_MSG_PROJET_EXPECTED = "PROPERTY_MSG_PROJET_EXPECTED";

    /**
     * The Constant PROPERTY_MSG_SITE_EXPECTED.
     */
    public static final String PROPERTY_MSG_SITE_EXPECTED = "PROPERTY_MSG_SITE_EXPECTED";

    /**
     *
     */
    public static final String PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE = "PROPERTY_MSG_MISSING_VARIABLE_FOR_SITE_PROJECT_DATATYPE";

    /**
     * The Constant PROPERTY_MSG_FLOAT_VALUE_OUT_OF_RANGE.
     */
    public static final String PROPERTY_MSG_FLOAT_VALUE_OUT_OF_RANGE = "PROPERTY_MSG_FLOAT_VALUE_OUT_OF_RANGE";

    /**
     * The Constant PROPERTY_MSG_BAD_INTERVAL.
     */
    public static final String PROPERTY_MSG_BAD_INTERVAL = "PROPERTY_MSG_BAD_INTERVAL";

    /**
     * The Constant PROPERTY_MSG_INVALID_DEPTH.
     */
    public static final String PROPERTY_MSG_INVALID_DEPTH = "PROPERTY_MSG_INVALID_DEPTH";

    /**
     *
     */
    public static final String PROPERTY_MSG_TWICE_DATA = "MSG_TWICE_DATA";

    /**
     * The Constant PROPERTY_MSG_INVALID_DEPTH_INTERVAL.
     */
    public static final String PROPERTY_MSG_INVALID_DEPTH_INTERVAL = "PROPERTY_MSG_INVALID_DEPTH_INTERVAL";

    /**
     * The Constant PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH.
     */
    public static final String PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH = "PROPERTY_MSG_ERROR_PLATEFORME_SITE_MISMATCH";

    /**
     * The Constant PROPERTY_MSG_ERROR_PLATEFORME_INVALID.
     */
    public static final String PROPERTY_MSG_ERROR_PLATEFORME_INVALID = "PROPERTY_MSG_ERROR_PLATEFORME_INVALID";

    /**
     * The Constant PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS.
     */
    public static final String PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_PROJET_SITE_IN_REFERENCES_DATAS";

    /**
     * The Constant PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS.
     */
    public static final String PROPERTY_MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS = "MSG_MISSING_PLATEFORME_IN_REFERENCES_DATAS";

    /**
     * The Constant PROPERTY_MSG_DUPLICATE_SEQUENCE.
     */
    public static final String PROPERTY_MSG_DUPLICATE_SEQUENCE = "PROPERTY_MSG_DUPLICATE_SEQUENCE";

    /**
     * The Constant PROPERTY_MSG_DOUBLON_LINE.
     */
    public static final String PROPERTY_MSG_DOUBLON_LINE = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     * The Constant PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS.
     */
    public static final String PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS = "PROPERTY_MSG_MISSING_OUTILS_MESURE_IN_REFERENCES_DATAS";

    /**
     * The Constant PROPERTY_MSG_NOT_IN_LIST.
     */
    public static final String PROPERTY_MSG_NOT_IN_LIST = "PROPERTY_MSG_NOT_IN_LIST";

    /**
     * The Constant PROPERTY_CST_VARIABLE_TYPE.
     */
    public static final String PROPERTY_CST_VARIABLE_TYPE = "variable";

    /**
     * The Constant PROPERTY_CST_VARIABLE_TYPE.
     */
    public static final String PROPERTY_CST_VALEUR_QUALITATIVE_TYPE = "valeur_qualitative";

    /**
     * The Constant PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE @link(String).
     */
    public static final String PROPERTY_CST_LIST_VALEURS_QUALITATIVES_TYPE = "liste_valeurs_qualitatives";


    /**
     * The localization manager @link(ILocalizationManager).
     */
    private static volatile ILocalizationManager localizationManager;

    /**
     * Gets the monSoere message.
     *
     * @param localizationManager
     * @param key {@link String} the key
     * @return the MONSOERE message from key
     */
    public static final String getGLACPEMessage(ILocalizationManager localizationManager, final String key) {
        return getGLACPEMessageWithBundle(localizationManager, GLACPE_DATASET_BUNDLE_NAME, key);
    }

    /**
     * Gets the monSoere message.
     *
     * @param localizationManager
     * @param bundlePath
     * @link(String) the bundle path
     * @param key
     *            {@link String} the key
     * @return the MONSOERE message from key
     */
    public static final String getGLACPEMessageWithBundle(ILocalizationManager localizationManager, final String bundlePath, final String key) {
        return localizationManager == null ? key : localizationManager.getMessage(bundlePath, key);
    }

    /**
     *
     * @return
     */
    public static ILocalizationManager getStaticLocalizationManager() {
        return localizationManager;
    }
    /**
     * The delete record @link(IDeleteRecord).
     */
    private IDeleteRecord deleteRecord;
    /**
     * The test format @link(ITestFormat).
     */
    private ITestFormat testFormat;
    /**
     * The process record @link(IProcessRecord).
     */
    private IProcessRecord processRecord;
    /**
     * The dataset descriptor path @link(String).
     */
    private String datasetDescriptorPath;

    /**
     * Delete records.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.dataset.IRecorder#deleteRecords(org.inra.ecoinfo.dataset
     * .versioning.entity.VersionFile)
     */
    @Override
    public void deleteRecords(final VersionFile versionFile) throws BusinessException {
        deleteRecord.deleteRecord(versionFile);
    }

    /**
     *      * process the record of the versionFile.
     *
     * @param parser the parser
     * @param versionFile {@link VersionFile}
     * @param sessionProperties {@link ISessionPropertiesMonSoere}
     * @param fileEncoding the file encoding
     * @param datasetDescriptorACBB {@link DatasetDescriptorMonSoere} the
     * dataset descriptor acbb
     * @throws BusinessException the business exception
     */
    private void processRecord(final CSVParser parser, final VersionFile versionFile, final String fileEncoding, final DatasetDescriptor datasetDescriptorMonSoere) throws BusinessException {
        processRecord.processRecord(parser, versionFile, fileEncoding, datasetDescriptorMonSoere);
    }

    /**
     * Sets the delete record.
     *
     * @param deleteRecord the new delete record
     * @see
     * org.inra.ecoinfo.glacpe.dataset.impl.IRecorderMonSoere#setDeleteRecord(org.inra.
     * ecoinfo.acbb.dataset.IDeleteRecord)
     */
    @Override
    public void setDeleteRecord(final IDeleteRecord deleteRecord) {
        this.deleteRecord = deleteRecord;
    }

    /**
     * Sets the process record.
     *
     * @param processRecord the new process record
     * @see
     * org.inra.ecoinfo.glacpe.dataset.impl.IRecorderMonSoere#setProcessRecord(org.inra
     * .ecoinfo.acbb.dataset.IProcessRecord)
     */
    @Override
    public void setProcessRecord(final IProcessRecord processRecord) {
        this.processRecord = processRecord;
    }

    /**
     * Sets the test format.
     *
     * @param testFormat the new test format
     * @see
     * org.inra.ecoinfo.glacpe.dataset.impl.IRecorderMonSoere#setTestFormat(org.inra.ecoinfo
     * .acbb.dataset.ITestFormat)
     */
    @Override
    public void setTestFormat(final ITestFormat testFormat) {
        this.testFormat = testFormat;
    }

    /**
     * Process record.
     *
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     * @link(VersionFile) the version file
     * @param fileEncoding
     * @link(String) the file encoding
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.dataset.impl.AbstractRecorder#processRecord(com.Ostermiller
     * .util.CSVParser, org.inra.ecoinfo.dataset.versioning.entity.VersionFile,
     * java.lang.String) does nothing because of overload record ()
     */
    @Override
    protected void processRecord(final CSVParser parser, final VersionFile versionFile, final String fileEncoding) throws BusinessException {
        return;
    }

    /**
     * Record.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @param fileEncoding
     * @link(String) the file encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void record(final VersionFile versionFile, final String fileEncoding) throws BusinessException {
        try {
            final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(getLocalizationManager().getMessage(GLACPE_DATASET_BUNDLE_NAME, PROPERTY_MSG_ERROR_BAD_FORMAT));
            byte[] datasSanitized = null;

            try {
                datasSanitized = Utils.sanitizeData(versionFile.getData(), getLocalizationManager());
            } catch (final BusinessException e) {
                AbstractRecorder.LOGGER.debug(e.getMessage(), e);
                badsFormatsReport.addException(e);
                LOGGER.info(new BadFormatException(badsFormatsReport).getMessage());
            }
            CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(datasSanitized), fileEncoding), SEPARATOR);

            final DatasetDescriptor datasetDescriptor = processRecord.holdDatasetDescriptor();
            testFormat(parser, versionFile, fileEncoding, datasetDescriptor);
            parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(versionFile.getData()), fileEncoding), SEPARATOR);
            processRecord(parser, versionFile, fileEncoding, datasetDescriptor);
        } catch (final BadDelimiterException | BadFormatException | IOException | SAXException e) {
            AbstractRecorder.LOGGER.error(e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     * Sets the localization manager.
     *
     * @param localizationManager the new localization manager
     * @see
     * org.inra.ecoinfo.dataset.impl.AbstractRecorder#setLocalizationManager(org.inra.ecoinfo.localization.ILocalizationManager)
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        RecorderGLACPE.localizationManager = localizationManager;
        super.setLocalizationManager(localizationManager);
    }

    /**
     * Test format.
     *
     * @param parser
     * @link(CSVParser) the parser
     * @param versionFile
     *            {@link VersionFile}
     * @param sessionProperties
     *            {@link ISessionPropertiesMonSoere}
     * @param encoding
     * @link(String) the encoding
     * @param datasetDescriptor
     *            {@link DatasetDescriptorMonSoere}
     * @throws BadFormatException the bad format exception
     * @throws BusinessException call test the format of a version file
     */
    private void testFormat(final CSVParser parser, final VersionFile versionFile, final String encoding, final DatasetDescriptor datasetDescriptor) throws BusinessException, BadFormatException {
        testFormat.testFormat(parser, versionFile, encoding, datasetDescriptor);
    }

    /**
     * Test format.
     *
     * @param versionFile
     * @link(VersionFile) the version file
     * @param encoding
     * @link(String) the encoding
     * @throws BusinessException the business exception
     */
    @Override
    public void testFormat(final VersionFile versionFile, final String encoding) throws BusinessException {
        CSVParser parser = null;
        DatasetDescriptor datasetDescriptor;

        BadsFormatsReport badsFormatsReport = new BadsFormatsReport(getLocalizationManager().getMessage(GLACPE_DATASET_BUNDLE_NAME, PROPERTY_MSG_ERROR_BAD_FORMAT));
        byte[] datasSanitized = null;

        try {
            datasSanitized = Utils.sanitizeData(versionFile.getData(), getLocalizationManager());
        } catch (final BusinessException e) {
            AbstractRecorder.LOGGER.debug(e.getMessage(), e);
            badsFormatsReport.addException(e);
        }

        try {
            parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(datasSanitized), encoding), SEPARATOR);
            datasetDescriptor = processRecord.holdDatasetDescriptor();
            testFormat(parser, versionFile, encoding, datasetDescriptor);
        } catch (final BadDelimiterException | SAXException | BadFormatException | IOException e) {
            AbstractRecorder.LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }

    }

    /**
     * Test header.
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @param parser
     * @link(CSVParser) the parser
     * @return the int
     * @throws BusinessException the business exception @see
     * org.inra.ecoinfo.dataset.impl.AbstractRecorder#testHeader(org.inra.ecoinfo
     * .dataset.BadsFormatsReport, com.Ostermiller.util.CSVParser) does nothing
     * because of overload record ()
     */
    protected int testHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser) throws BusinessException {
        return 0;
    }

    /**
     * Sets the version file dao.
     *
     * @param versionFileDAO {@link IVersionFileDAO} the new version file dao
     */
    @Override
    public void setVersionFileDAO(final IVersionFileDAO versionFileDAO) {
    }

    /**
     *
     * @param datasetDescriptorPath
     */
    public void setDatasetDescriptorPath(final String datasetDescriptorPath) {
        this.datasetDescriptorPath = datasetDescriptorPath;
    }
}

package org.inra.ecoinfo.glacpe.dataset.chimie;

/**
 *
 * @author ptcherniati
 */
public class ConstantsChimie {

    /**
     *
     */
    public static final String KEY_DATATYPE_PHYSICO_CHIMIE = "physico_chimie";

    /**
     *
     */
    public static final String KEY_VARIABLE_CA = "calcium";

    /**
     *
     */
    public static final String KEY_VARIABLE_CL = "chlorures";

    /**
     *
     */
    public static final String KEY_VARIABLE_K = "potassium";

    /**
     *
     */
    public static final String KEY_VARIABLE_MG = "magnesium";

    /**
     *
     */
    public static final String KEY_VARIABLE_NA = "sodium";

    /**
     *
     */
    public static final String KEY_VARIABLE_NH4 = "azote_ammonium";

    /**
     *
     */
    public static final String KEY_VARIABLE_NO3 = "azote_nitrates";

    /**
     *
     */
    public static final String KEY_VARIABLE_NO2 = "azote_nitrites";

    /**
     *
     */
    public static final String KEY_VARIABLE_PO4 = "phosphore_orthophosphates";

    /**
     *
     */
    public static final String KEY_VARIABLE_SO4 = "sulfates";

    /**
     *
     */
    public static final String KEY_VARIABLE_TAC = "titre_alcalimetrique_complet";

    /**
     *
     */
    public static final String KEY_VARIABLE_BALANCE_IONIQUE = "balance_ionique";

}

/**
 * OREILacs project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.utils.Utils;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class JPAPlateformeDAO extends AbstractJPADAO<Plateforme> implements IPlateformeDAO {

    /**
     *
     * @param codePlateforme
     * @param codeSite
     * @return
     */
    @Override
    public Optional<Plateforme> getByNKey(String codePlateforme, String codeSite) {
        codePlateforme=Utils.createCodeFromString(codePlateforme);
        codeSite=Utils.createCodeFromString(codeSite);
        CriteriaQuery<Plateforme> query = builder.createQuery(Plateforme.class);
        Root<Plateforme> plateforme = query.from(Plateforme.class);
        Join<Plateforme, SiteGLACPE> site = plateforme.join(Plateforme_.site);
        query
                .select(plateforme)
                .where(
                        builder.and(
                                builder.equal(plateforme.get(Nodeable_.code), Plateforme.buildCode(codeSite, codePlateforme))
                        ));
        return getOptional(query);
    }

}

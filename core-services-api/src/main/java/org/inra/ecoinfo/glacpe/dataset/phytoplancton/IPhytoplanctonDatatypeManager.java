package org.inra.ecoinfo.glacpe.dataset.phytoplancton;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPhytoTaxonManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IPhytoplanctonDatatypeManager  extends IPlatformManager, IVariableManager, IPhytoTaxonManager{
    /**
     *
     * @return
     */
    List<Taxon> retrievePhytoplanctonRootTaxon();

    /**
     *
     * @param taxonId
     * @return
     */
    List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId);

    /**
     *
     * @param taxonCode
     * @return
     */
    Optional<Taxon> retrieveTaxonByCode(String taxonCode);
}

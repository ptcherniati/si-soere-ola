package org.inra.ecoinfo.glacpe.dataset.productionprimaire;

import org.inra.ecoinfo.glacpe.extraction.jsf.IDepthManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;


/**
 *
 * @author ptcherniati
 */
public interface IPPDatatypeManager extends IPlatformManager, IVariableManager, IDepthManager {

    /**
     *
     */
    String CODE_DATATYPE_PP = "";

}

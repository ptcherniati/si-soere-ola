/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;

/**
 *
 * @author ptcherniati
 */
public class PlateformeProjetVO implements Comparable<PlateformeProjetVO>{
    private Plateforme plateforme;
    private Projet projet;
    private SiteGLACPE site;

    /**
     *
     * @param pltfrm
     * @param projet
     * @param sglcp
     */
    public PlateformeProjetVO(Plateforme plateforme, Projet projet, SiteGLACPE siteNode) {
        this.plateforme = plateforme;
        this.projet = projet;
        this.site = siteNode;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

    /**
     *
     * @return
     */
    public Long getSiteNodeId() {
        return site.getId();
    }
    
    /**
     *
     * @return
     */
    public SiteGLACPE getSite(){
        return site;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(PlateformeProjetVO o) {
        if(getProjet().compareTo(o.getProjet())!=0){
            return getProjet().compareTo(o.getProjet());
        }
        if(getSite().compareTo(o.getSite())!=0){
            return getSite().compareTo(o.getSite());
        }
        return getPlateforme().getCode().compareTo(o.getPlateforme().getCode());
    }
    
}

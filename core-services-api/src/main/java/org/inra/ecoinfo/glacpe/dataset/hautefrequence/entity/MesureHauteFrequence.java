package org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.site.Site;

/**
 *
 * @author mylene
 */
@Entity
@Table(name = MesureHauteFrequence.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {SousSequenceHauteFrequence.ID_JPA, "heure"}),
        indexes = {
            @Index(name = SousSequenceHauteFrequence.ID_JPA, columnList = SousSequenceHauteFrequence.ID_JPA)
        })
@org.hibernate.annotations.Table(appliesTo = MesureHauteFrequence.TABLE_NAME)
public class MesureHauteFrequence implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_haute_frequence_mhautefrequence";

    /**
     *
     */
    static public final String ID_JPA = "mhautefrequence_id";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceHauteFrequence.ID_JPA, referencedColumnName = SousSequenceHauteFrequence.ID_JPA, nullable = false)
    private SousSequenceHauteFrequence sousSequence;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    private List<ValeurMesureHauteFrequence> valeurs = new LinkedList<ValeurMesureHauteFrequence>();

    @Column(name = "heure", nullable = true)
    private LocalTime heure;

    /**
     *
     */
    public MesureHauteFrequence() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesureHauteFrequence> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureHauteFrequence> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public SousSequenceHauteFrequence getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequenceHauteFrequence sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

// Présents dans MesureSondeMulti :
//      Site getSite()
//      Date getDatePrelevement()
//      List<ISecurityPathWithVariable> getChildren()
//      setChildren(List<ISecurityPathWithVariable> children)
//      String getSecurityPath()
//-> utilité pour MesureHauteFrequence ?
    /**
     * Classe raccourcis
     *
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:16:22
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.site.Site;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = MesureChimie.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SousSequenceChimie.ID_JPA, MesureChimie.NAME_ATTRIBUTS_PROFONDEUR_MAX}),
        indexes = {
            @Index(name = MesureChimie.NAME_ATTRIBUTS_PROFONDEUR_MAX, columnList = MesureChimie.NAME_ATTRIBUTS_PROFONDEUR_MAX),
            @Index(name = MesureChimie.NAME_ATTRIBUTS_PROFONDEUR_MIN, columnList = MesureChimie.NAME_ATTRIBUTS_PROFONDEUR_MIN),
            @Index(name = SousSequenceChimie.ID_JPA, columnList =SousSequenceChimie.ID_JPA)
        })
public class MesureChimie implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     *
     */
    public static final String NAME_ATTRIBUTS_PROFONDEUR_MIN = "profondeur_max";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_PROFONDEUR_MAX = "profondeur_min";

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_chimie_mchimie";//

    /**
     *
     */
    static public final String ID_JPA = "mchimie_id";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceChimie.ID_JPA, referencedColumnName = SousSequenceChimie.ID_JPA, nullable = false)
    private SousSequenceChimie sousSequence;

    @OneToMany(mappedBy = "mesure", cascade = {CascadeType.ALL})
    @OrderBy(RealNode.ID_JPA)
    private List<ValeurMesureChimie> valeurs = new LinkedList<ValeurMesureChimie>();


    @Column(name = NAME_ATTRIBUTS_PROFONDEUR_MIN)
    private Float profondeurMin;

    @Column(name = NAME_ATTRIBUTS_PROFONDEUR_MAX)
    private Float profondeurMax;

    @Column
    private Float profondeurReelle;
    /**
     *
     */
    public MesureChimie() {
        super();
    }
    /**
     * Classe raccourcis
     *
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesureChimie> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureChimie> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeurMin(Float profondeur) {
        this.profondeurMin = profondeur;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeurMax(Float profondeur) {
        this.profondeurMax = profondeur;
    }

    /**
     *
     * @return
     */
    public SousSequenceChimie getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequenceChimie sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurReelle() {
        return profondeurReelle;
    }

    /**
     *
     * @param profondeurReelle
     */
    public void setProfondeurReelle(Float profondeurReelle) {
        this.profondeurReelle = profondeurReelle;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;


import java.util.Collection;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;

/**
 * @author tcherniatinsky
 */
public interface IPlatformManager {

    /**
     * @return
     */
    Collection<PlateformeProjetVO> getAvailablePlatforms();

}

package org.inra.ecoinfo.glacpe.refdata.projetsite;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProjetSite.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {SiteGLACPE.ID_JPA, Projet.ID_JPA}))
public class ProjetSite implements Serializable {

    /**
     *
     */
    public static final String ID_JPA = "psi_id";

    /**
     *
     */
    public static final String TABLE_NAME = "projet_site_psi";

    private static final long serialVersionUID = 1L;

    @Column(name = "commanditaire_projet")
    private String commanditaire;

    @Column(name = "commentaire_projet")
    private String commentaire;

    @Column(name = "date_debut")
    private LocalDate debut;

    @Column(name = "date_fin")
    private LocalDate fin;

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = SiteGLACPE.ID_JPA, referencedColumnName = SiteGLACPE.ID_JPA, nullable = false)
    private SiteGLACPE site;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Projet.ID_JPA, referencedColumnName = Projet.ID_JPA, nullable = false)
    private Projet projet;
    /**
     *
     */
    public ProjetSite() {
        super();
    }
    /**
     *
     * @param site
     * @param projet
     */
    public ProjetSite(SiteGLACPE site, Projet projet) {
        this.site = site;
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public void setSite(SiteGLACPE site) {
        this.site = site;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public String getCommanditaire() {
        return commanditaire;
    }

    /**
     *
     * @param commanditaire
     */
    public void setCommanditaire(String commanditaire) {
        this.commanditaire = commanditaire;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public LocalDate getDebut() {
        return debut;
    }

    /**
     *
     * @param debut
     */
    public void setDebut(LocalDate debut) {
        this.debut = debut;
    }

    /**
     *
     * @return
     */
    public LocalDate getFin() {
        return fin;
    }

    /**
     *
     * @param fin
     */
    public void setFin(LocalDate fin) {
        this.fin = fin;
    }
}

package org.inra.ecoinfo.glacpe.dataset;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * Cette interface permet de marquer les classes qui seront sujettent à des calculs de données aggrégées
 * 
 * @author antoine schellenberger
 * @param <T>
 * 
 */
public interface IGLACPEAggregateData<T extends IGLACPEAggregateData> extends Comparable<T> {

    /**
     *
     * @param t
     * @return
     */
    @Override
    public default int compareTo(T t){
        if(t == null){
            return -1;
        }
        if(getProjet().compareTo(t.getProjet())!=0){
            return getProjet().compareTo(t.getProjet());
        }
        if(getSite().compareTo(t.getSite())!=0){
            return getSite().compareTo(t.getSite());
        }
        if(getPlateforme().compareTo(t.getPlateforme())!=0){
            return getPlateforme().compareTo(t.getPlateforme());
        }
        if(getDate().compareTo(t.getDate())!=0){
            return getDate().compareTo(t.getDate());
        }
        if(getDepthMin()!=null && getDepthMin().compareTo(t.getDepthMin())!=0){
            return getDepthMin().compareTo(t.getDepthMin());
        }
        return getDepthMax()!=null?getDepthMax().compareTo(t.getDepthMax()):0;
    }

    /**
     *
     * @return
     */
    Float getValue();

    /**
     *
     * @return
     */
    Float getDepth();

    /**
     *
     * @return
     */
    Float getDepthMin();

    /**
     *
     * @return
     */
    Float getDepthMax();

    /**
     *
     * @return
     */
    LocalDate getDate();

    /**
     *
     * @return
     */
    RealNode getRealNode();

    /**
     *
     * @return
     */
    Plateforme getPlateforme();

    /**
     *
     * @return
     */
    SiteGLACPE getSite();

    /**
     *
     * @return
     */
    Projet getProjet();

    /**
     *
     * @return
     */
    OutilsMesure getOutilsMesure();

    /**
     *
     * @return
     */
    OutilsMesure getOutilsPrelevement();

    /**
     *
     * @return
     */
    LocalTime getHeure();
}

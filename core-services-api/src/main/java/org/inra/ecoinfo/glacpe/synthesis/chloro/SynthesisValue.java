/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.glacpe.synthesis.chloro;


import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity(name = "ChloroSynthesisValue")
@Table(indexes = {
        @Index(name = "ChloroSynthesisValue_site_idx", columnList = "site"),
        @Index(name = "ChloroSynthesisValue_site_variable_idx", columnList = "site,variable"),
        @Index(name = "ChloroSynthesisValue_projet_plateforme_date_idx", columnList = "projetid,plateformeid,date")})
public class SynthesisValue extends GenericSynthesisValue {
    private static final long serialVersionUID = 1L;
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = "siteId",referencedColumnName = SiteGLACPE.ID_JPA, nullable = false)
    private SiteGLACPE siteId;
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = "projetId", referencedColumnName = Projet.ID_JPA, nullable = false)
    private Projet projetId;
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = "plateformeId", referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateformeId;
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = "variableId", referencedColumnName = VariableGLACPE.ID_JPA, nullable = false)
    private VariableGLACPE variableId;
 
    @Column(columnDefinition = "TEXT")
    private String profondeurs;
    


    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueFloat
     * @param idNode
     */
    public SynthesisValue(LocalDate date, String site, String variable, Double valueFloat, Long idNode, SiteGLACPE siteId, Projet projetId, Plateforme plateformeId, VariableGLACPE variableId, String profondeurs) {
        super(date.atStartOfDay(), site, variable,  valueFloat == null ? null : valueFloat.floatValue(), null, idNode, false);
        this.siteId = siteId;
        this.projetId = projetId;
        this.plateformeId = plateformeId;
        this.variableId = variableId;
        this.profondeurs = profondeurs;
    }

    /**
     *
     */
    public SynthesisValue() {
    }

    public SiteGLACPE getSiteId() {
        return siteId;
    }

    public Projet getProjetId() {
        return projetId;
    }

    public Plateforme getPlateformeId() {
        return plateformeId;
    }

    public VariableGLACPE getVariableId() {
        return variableId;
    }

    public String getProfondeurs() {
        return profondeurs;
    }
}

package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.jfree.data.json.impl.JSONObject;
import org.slf4j.MDC;

/**
 *
 * @author ptcherniati
 */
public class DatasRequestParamVO implements Serializable, Cloneable {

    private static final String PATTERN_STRING_EXTRACTION_TYPE = "   %s";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.ui.messages";

    public static final String INPUT_RAW_DATA = "inputRawData";
    public static final String INPUT_DATA_BALANCED_BY_DEPTH = "inputDataBalancedByDepth";
    public static final String INPUT_MIN_MAX_AND_ASSOCIATED_DEPTH = "inputMinValueAndAssociatedDepth";
    public static final String INPUT_TARGETED_VALUES_AND_DEPHTH = "targetedValuesAndDepths";
    public static final String INPUT_DATAS_REQUEST_PARAMS = "datasRequestParams";

    private static final String MSG_MAX_VALUE_EXTRACTION_TYPE = "PROPERTY_MSG_MAX_VALUE_EXTRACTION_TYPE";
    private static final String MSG_MAX_VALUE_EXTRACTION_TYPE_BY_VARIABLE = "PROPERTY_MSG_MAX_VALUE_EXTRACTION_TYPE_BY_VARIABLE";
    private static final String MSG_MIN_VALUE_EXTRACTION_TYPE = "PROPERTY_MSG_MIN_VALUE_EXTRACTION_TYPE";
    private static final String MSG_MIN_VALUE_EXTRACTION_TYPE_BY_VARIABLE = "PROPERTY_MSG_MIN_VALUE_EXTRACTION_TYPE_BY_VARIABLE";
    private static final String MSG_DATA_BALANDED_BY_DEPTH_EXTRACTION_TYPE = "PROPERTY_MSG_DATA_BALANDED_BY_DEPTH_EXTRACTION_TYPE";
    private static final String MSG_RAW_EXTRACTION_TYPE = "PROPERTY_MSG_RAW_EXTRACTION_TYPE";
    private static final String MSG_EXTRACTIONS_TYPES = "PROPERTY_MSG_EXTRACTIONS_TYPES";
    private static final String MSG_TARGET_VALUE_TYPE = "PROPERTY_MSG_TARGET_VALUE_TYPE";
    private static final String CST_UL = "<ul>";
    private static final String CST_UL_END = "</ul>";
    private static final String CST_LI = "<li>";
    private static final String CST_LI_END = "</li>";
    private static final String CST_DOUBLE_DOT = " : ";
    private static final String CST_TAB = "&nbsp;&nbsp;&nbsp;";
    private static final String MSG_MIN = "min";
    private static final String MSG_MAX = "max ";
    private static final long serialVersionUID = 1L;

    private Boolean rawData = false;
    private Boolean dataBalancedByDepth = false;
    private Boolean minValueAndAssociatedDepth = false;
    private Boolean maxValueAndAssociatedDepth = false;
    private Boolean targetedValuesAndDepths = false;
    private List<ExtractionParametersVO> extractionParameters = new LinkedList<ExtractionParametersVO>();
    private Map<String, Boolean> datasInputs = new HashMap();

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    public DatasRequestParamVO() {
        super();
        datasInputs.put(INPUT_RAW_DATA, true);
        datasInputs.put(INPUT_DATA_BALANCED_BY_DEPTH, true);
        datasInputs.put(INPUT_MIN_MAX_AND_ASSOCIATED_DEPTH, true);
    }

    /**
     *
     * @param localizationManager
     */
    public DatasRequestParamVO(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    public void updateDatasRequestParamWithVariables(List<DatatypeVariableUniteGLACPE> dvus) {

        List<ExtractionParametersVO> extractionParameters = getExtractionParameters();
        extractionParameters.clear();

        for (DatatypeVariableUniteGLACPE dvu : dvus) {
            ExtractionParametersVO extractionParameter = new ExtractionParametersVO(dvu, localizationManager);
            extractionParameters.add(extractionParameter);
        }
    }

    /**
     *
     * @return
     */
    public String getExtractionParametersToString() {
        if (extractionParameters.isEmpty()) {
            return "";
        }
        StringBuilder returnString = new StringBuilder(CST_UL);
        for (ExtractionParametersVO extractionParametersVO : extractionParameters) {
            if (extractionParametersVO.getValueMin() || extractionParametersVO.getValueMax() || !extractionParametersVO.getTargetValues().isEmpty()) {
                returnString = returnString.append(CST_LI).append(extractionParametersVO.getLocalizedName()).append(CST_DOUBLE_DOT);
                if (extractionParametersVO.getValueMin()) {
                    returnString = returnString.append(MSG_MIN);
                }
                if (extractionParametersVO.getValueMin() && extractionParametersVO.getValueMax()) {
                    returnString = returnString.append(CST_TAB);
                }
                if (extractionParametersVO.getValueMax()) {
                    returnString = returnString.append(MSG_MAX);
                }
                if (!extractionParametersVO.getTargetValues().isEmpty()) {
                    for (String targetValue : extractionParametersVO.getTargetValues()) {
                        returnString.append(CST_TAB).append(targetValue);
                    }
                }
                returnString = returnString.append(CST_LI_END);
            }
        }
        return returnString.append(CST_UL_END).toString();
    }

    /**
     *
     * @return
     */
    public List<ExtractionParametersVO> getExtractionParameters() {
        return extractionParameters;
    }

    /**
     *
     * @param extractionParameters
     */
    public void setExtractionParameters(List<ExtractionParametersVO> extractionParameters) {
        this.extractionParameters = extractionParameters;
    }

    public Boolean getTargetedValuesAndDepths() {
        return targetedValuesAndDepths;
    }

    public void setTargetedValuesAndDepths(Boolean targetedValuesAndDepths) {
        this.targetedValuesAndDepths = targetedValuesAndDepths;
    }

    /**
     *
     * @return
     */
    public Boolean getDataBalancedByDepth() {
        return dataBalancedByDepth;
    }

    /**
     *
     * @param dataBalancedByDepth
     */
    public void setDataBalancedByDepth(Boolean dataBalancedByDepth) {
        this.dataBalancedByDepth = dataBalancedByDepth;
    }

    /**
     *
     * @return
     */
    public Boolean getMinValueAndAssociatedDepth() {
        return minValueAndAssociatedDepth;
    }

    /**
     *
     * @param minValueAndAssociatedDepth
     */
    public void setMinValueAndAssociatedDepth(Boolean minValueAndAssociatedDepth) {
        this.minValueAndAssociatedDepth = minValueAndAssociatedDepth;
    }

    /**
     *
     * @return
     */
    public Boolean getMaxValueAndAssociatedDepth() {
        return maxValueAndAssociatedDepth;
    }

    /**
     *
     * @param maxValueAndAssociatedDepth
     */
    public void setMaxValueAndAssociatedDepth(Boolean maxValueAndAssociatedDepth) {
        this.maxValueAndAssociatedDepth = maxValueAndAssociatedDepth;
    }

    /**
     *
     * @return
     */
    public Boolean getRawData() {
        return this.rawData;
    }

    /**
     *
     * @param rawData
     */
    public void setRawData(Boolean rawData) {
        this.rawData = rawData;
    }

    /**
     *
     * @param printStream
     */
    public void buildSummary(PrintStream printStream) {
        printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTIONS_TYPES));
        processExtractionType("extract.rawData",getRawData(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_RAW_EXTRACTION_TYPE), printStream);
        processExtractionType("extract.dataBalancedByDepth",getDataBalancedByDepth() || getTargetedValuesAndDepths(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_DATA_BALANDED_BY_DEPTH_EXTRACTION_TYPE), printStream);
        processExtractionType("extract.minValueAndAssociatedDepth",getMinValueAndAssociatedDepth(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN_VALUE_EXTRACTION_TYPE), printStream);
        processExtractionType("extract.maxValueAndAssociatedDepth",getMaxValueAndAssociatedDepth(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX_VALUE_EXTRACTION_TYPE), printStream);
        SortedMap<String, SortedMap<String, String>> extractionParameters = new TreeMap<>();
        for (ExtractionParametersVO extractionParametersVO : getExtractionParameters()) {
            processExtractionType(extractionParametersVO.getValueMin(), String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN_VALUE_EXTRACTION_TYPE_BY_VARIABLE), extractionParametersVO.getLocalizedName()), printStream);
            processExtractionType(extractionParametersVO.getValueMax(), String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX_VALUE_EXTRACTION_TYPE_BY_VARIABLE), extractionParametersVO.getLocalizedName()), printStream);
            for (String targetValue : extractionParametersVO.getTargetValues()) {
                extractionParameters
                        .computeIfAbsent(extractionParametersVO.getLocalizedName(), k->new TreeMap<>())
                        .put("minValue", getMinValueAndAssociatedDepth().toString());
                extractionParameters
                        .computeIfAbsent(extractionParametersVO.getLocalizedName(), k->new TreeMap<>())
                        .put("maxValue", getMaxValueAndAssociatedDepth().toString());
                processExtractionType(
                        true,
                        String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_TARGET_VALUE_TYPE), targetValue, extractionParametersVO.getLocalizedName(),
                                extractionParametersVO.getUncertainties().get(extractionParametersVO.getTargetValues().indexOf(targetValue))), printStream);
                extractionParameters
                        .computeIfAbsent(extractionParametersVO.getLocalizedName(), k->new TreeMap<>())
                        .put("targetValue", extractionParametersVO.getUncertainties().get(extractionParametersVO.getTargetValues().indexOf(targetValue)));
                
            }
        }
        if(!extractionParameters.isEmpty()){
            MDC.put("extract.targetedValues", JSONObject.toJSONString(extractionParameters));
        }
        printStream.println();

    }

    private void processExtractionType(Boolean condition, String message, PrintStream reminderPrintStream) {
        if (condition) {
            reminderPrintStream.println(String.format(PATTERN_STRING_EXTRACTION_TYPE, message));
        }
    }

    private void processExtractionType(String code, Boolean condition, String message, PrintStream reminderPrintStream) {
        MDC.put(code, String.format("%s : %s", message, condition));
        if (condition) {
            reminderPrintStream.println(String.format(PATTERN_STRING_EXTRACTION_TYPE, message));
        }
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public DatasRequestParamVO clone() {
        DatasRequestParamVO returnVO = new DatasRequestParamVO(this.localizationManager);
        returnVO.setRawData(new Boolean(this.rawData));
        returnVO.setDataBalancedByDepth(new Boolean(this.dataBalancedByDepth));
        returnVO.setMinValueAndAssociatedDepth(new Boolean(this.minValueAndAssociatedDepth));
        returnVO.setMaxValueAndAssociatedDepth(new Boolean(this.maxValueAndAssociatedDepth));
        returnVO.setTargetedValuesAndDepths(new Boolean(this.targetedValuesAndDepths));
        List<ExtractionParametersVO> parametersVO = new LinkedList<ExtractionParametersVO>();
        for (ExtractionParametersVO parameter : this.extractionParameters) {
            parametersVO.add(parameter.clone());
        }
        returnVO.setExtractionParameters(parametersVO);
        return returnVO;
    }

    public Map<String, Boolean> getDatasInputs() {
        return datasInputs;
    }

}

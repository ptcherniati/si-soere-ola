package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureConditionGenerale.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {Plateforme.ID_JPA, "date_prelevement", VersionFile.ID_JPA}),
        indexes = {
            @Index(name = MesureConditionGenerale.INDEX_ATTRIBUTS_PLATEFORME, columnList = Plateforme.ID_JPA),
            @Index(name = MesureConditionGenerale.INDEX_ATTRIBUTS_IVF, columnList = VersionFile.ID_JPA)
        })
public class MesureConditionGenerale implements Serializable, Comparable<MesureConditionGenerale> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "mconditionprelevement_id";

    /**
     *
     */
    public static final String TABLE_NAME = "mesure_condition_prelevement_mconditionprelevement";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_condition_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_condition_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_condition_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_condition_ikey";


    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private LocalDate datePrelevement;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    private List<ValeurConditionGenerale> valeurs = new LinkedList<ValeurConditionGenerale>();

    
    @Column(name = "heure", nullable = true)
    private LocalTime heure;

    @Column(name = "commentaire")
    private String commentaire;
    /**
     *
     */
    public MesureConditionGenerale() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(LocalDate datePrelevement) {
        this.datePrelevement = datePrelevement;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(versionFile.getDataset());
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(versionFile.getDataset());
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public List<ValeurConditionGenerale> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurConditionGenerale> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(MesureConditionGenerale t) {
        if(t==null){
            return -1;
        }
        final int compareVersion = versionFile.compareTo(t.versionFile);
        if(compareVersion!=0){
            return compareVersion;
        }
        final int comparePlatform = plateforme.compareTo(t.plateforme);
        if(comparePlatform!=0){
            return comparePlatform;
        }
        final int compareDate = datePrelevement.compareTo(t.datePrelevement);
        if(compareDate!=0){
            return compareDate;
        }
        if(heure==null){
            return t.heure==null?0:1;
        }
        return heure.compareTo(t.heure);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.versionFile);
        hash = 83 * hash + Objects.hashCode(this.plateforme);
        hash = 83 * hash + Objects.hashCode(this.datePrelevement);
        hash = 83 * hash + Objects.hashCode(this.heure);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MesureConditionGenerale other = (MesureConditionGenerale) obj;
        if (!Objects.equals(this.versionFile, other.versionFile)) {
            return false;
        }
        if (!Objects.equals(this.plateforme, other.plateforme)) {
            return false;
        }
        if (!Objects.equals(this.datePrelevement, other.datePrelevement)) {
            return false;
        }
        if (!Objects.equals(this.heure, other.heure)) {
            return false;
        }
        return true;
    }
}

package org.inra.ecoinfo.glacpe.refdata.proprietetaxon;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProprieteTaxon.TABLE_NAME)
public class ProprieteTaxon implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "prota_id";

    /**
     *
     */
    public static final String TABLE_NAME = "propriete_taxon_prota";


    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @Column(name = "nom", nullable = false, unique = true)
    private String nom;

    @Column(name = "prota_code", nullable = false, unique = true)
    private String code;

    @Column(name = "prota_valueType", nullable = false)
    private Boolean valueType;

   
    @Column(name = "definition", columnDefinition = "TEXT")
    private String definition;

    @Column(name = "isQualitative", columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean isQualitative = false;

    @Column(name = "prota_type_taxon")
    private String typeTaxon;

    @Column(name = "prota_ordre_affichage", nullable = false)
    private Integer ordreAffichage;
    /**
     *
     */
    public ProprieteTaxon() {
        super();
        
    }
    /**
     *
     * @param nom
     * @param valueType
     * @param string1
     * @param definition
     */
    public ProprieteTaxon(String nom, Boolean valueType, String definition) {
        super();
        setNom(nom);
        this.valueType = valueType;
        this.definition = definition;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }


    /**
     *
     * @return
     */
    public String getDefinition() {
        return definition;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @return
     */
    public Boolean getValueType() {
        return valueType;
    }

    /**
     *
     * @param valueType
     */
    public void setValueType(Boolean valueType) {
        this.valueType = valueType;
    }

    /**
     *
     * @param definition
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public Boolean getIsQualitative() {
        return isQualitative;
    }

    /**
     *
     * @param isQualitative
     */
    public void setIsQualitative(Boolean isQualitative) {
        this.isQualitative = isQualitative;
    }

    /**
     *
     * @param ordreAffichage
     */
    public void setOrdreAffichage(Integer ordreAffichage) {
        this.ordreAffichage = ordreAffichage;
    }

    /**
     *
     * @return
     */
    public Integer getOrdreAffichage() {
        return ordreAffichage;
    }

    /**
     *
     * @param typeTaxon
     */
    public void setTypeTaxon(String typeTaxon) {
        this.typeTaxon = typeTaxon;
    }

    /**
     *
     * @return
     */
    public String getTypeTaxon() {
        return typeTaxon;
    }
    /**
     *
     */
    public static final class VALUES_ATTR_CODE {
        
        /**
         *
         */
        public static final String PRESELECTED = "preselected";
    }
}

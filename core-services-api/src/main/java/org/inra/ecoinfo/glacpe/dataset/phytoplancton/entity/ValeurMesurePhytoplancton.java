package org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * @author "Guillaume Enrico"
 *
 */
@Entity
@Table(name = ValeurMesurePhytoplancton.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA, MesurePhytoplancton.ID_JPA}),
        indexes = {
            @Index(name = RealNode.ID_JPA, columnList = RealNode.ID_JPA),
            @Index(name = MesurePhytoplancton.ID_JPA, columnList = MesurePhytoplancton.ID_JPA)
        })
public class ValeurMesurePhytoplancton implements Serializable, IGLACPEAggregateData<ValeurMesurePhytoplancton> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_phytoplancton_vmphytoplancton";

    /**
     *
     */
    static public final String ID_JPA = "vmphytoplancton_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_phyto_ikey";


    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesurePhytoplancton.ID_JPA, referencedColumnName = MesurePhytoplancton.ID_JPA, nullable = false)
    private MesurePhytoplancton mesure;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @Column(nullable = true)
    private Float valeur;
    /**
     *
     */
    public ValeurMesurePhytoplancton() {
        super();
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesurePhytoplancton mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesurePhytoplancton getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public LocalDate getDate() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return this.getMesure().getSousSequence().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return this.getMesure().getSousSequence().getOutilsMesure();
    }

    /**
     *
     * @return
     */
    @Override
    public LocalTime getHeure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        return this.getMesure().getSousSequence().getOutilsPrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(getMesure().getSousSequence().getSequence().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(getMesure().getSousSequence().getSequence().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMin() {
        return getMesure().getValeurs().stream().filter(v->v.getDepth()!=null).map(v->v.getDepth()).min(Float::compare).orElse(null);
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMax() {
        return getMesure().getValeurs().stream().filter(v->v.getDepth()!=null).map(v->v.getDepth()).min(Float::compare).orElse(null);
    }
}

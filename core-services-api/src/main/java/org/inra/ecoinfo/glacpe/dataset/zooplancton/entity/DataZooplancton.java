package org.inra.ecoinfo.glacpe.dataset.zooplancton.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * 
 * @author Antoine Schellenberger*
 * 
 */
@Entity
@Table(
        name = DataZooplancton.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(
                columnNames = {RealNode.ID_JPA, MetadataZooplancton.ID_JPA, Taxon.ID_JPA, StadeDeveloppement.ID_JPA}))
public class DataZooplancton implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "data_zooplancton_dz";

    /**
     *
     */
    static public final String ID_JPA = "dz_id";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;



    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = StadeDeveloppement.ID_JPA, referencedColumnName = StadeDeveloppement.ID_JPA, nullable = false)
    private StadeDeveloppement stadeDeveloppement;

    @Column(nullable = false)
    private Float value;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Taxon.ID_JPA, referencedColumnName = Taxon.ID_JPA, nullable = false)
    private Taxon taxon;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MetadataZooplancton.ID_JPA, referencedColumnName = MetadataZooplancton.ID_JPA, nullable = false)
    private MetadataZooplancton metadataZooplancton;

    /**
     *
     */
    public DataZooplancton() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public MetadataZooplancton getMetadataZooplancton() {
        return metadataZooplancton;
    }

    /**
     *
     * @return
     */
    public Taxon getTaxon() {
        return taxon;
    }

    /**
     *
     * @return
     */
    public Float getValue() {
        return value;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param metadataZooplancton
     */
    public void setMetadataZooplancton(MetadataZooplancton metadataZooplancton) {
        this.metadataZooplancton = metadataZooplancton;
    }

    /**
     *
     * @param taxon
     */
    public void setTaxon(Taxon taxon) {
        this.taxon = taxon;
    }

    /**
     *
     * @param value
     */
    public void setValue(Float value) {
        this.value = value;
    }

    public RealNode getRealNode() {
        return realNode;
    }

    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public StadeDeveloppement getStadeDeveloppement() {
        return stadeDeveloppement;
    }

    /**
     *
     * @param stadeDeveloppement
     */
    public void setStadeDeveloppement(StadeDeveloppement stadeDeveloppement) {
        this.stadeDeveloppement = stadeDeveloppement;
    }

}

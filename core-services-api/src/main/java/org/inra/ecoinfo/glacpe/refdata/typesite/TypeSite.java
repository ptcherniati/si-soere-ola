package org.inra.ecoinfo.glacpe.refdata.typesite;

import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeSite.TABLE_NAME)
@PrimaryKeyJoinColumn(name = TypeSite.ID_JPA)
public class TypeSite extends Nodeable {
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";
    

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "type_site_tsit";

    /**
     *
     */
    static public final String ID_JPA = "tsit_id";

    /**
     *
     */
    public static final String ENTITY_FIELD_NAME = "nom";

    /**
     *
     */
    public static final String CODE = "code";

    /**
     *
     */
    static public final String ENTITY_FIELD_DESCRIPTION = "description";


    @Column(name = ENTITY_FIELD_DESCRIPTION, nullable = true, columnDefinition = "TEXT")
    private String description;
    private String nom;
    @OneToMany(mappedBy = "typeSite", cascade = {MERGE, PERSIST, REFRESH})
    private List<SiteGLACPE> sites = new LinkedList<SiteGLACPE>();
    
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;
    /**
     *
     */
    public TypeSite() {
        super();
    }
    /**
     *
     * @param nom
     * @param description
     */
    public TypeSite(String nom, String description) {
        super(Utils.createCodeFromString(nom));
        this.setNom(nom);
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }
    /**
     *
     * @return
     */
    public String getName() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public List<SiteGLACPE> getSites() {
        return sites;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
    return (Class<T>) TypeSite.class;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.setCode(Utils.createCodeFromString(nom));
    }

}

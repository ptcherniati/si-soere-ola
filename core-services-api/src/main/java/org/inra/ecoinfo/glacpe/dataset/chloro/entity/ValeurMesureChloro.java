/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 2010 10:07:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author "Cédric Anache"
 * 
 */
@Entity
@Table(name = ValeurMesureChloro.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA, MesureChloro.ID_JPA}),
        indexes = {@Index(name = ValeurMesureChloro.INDEX_ATTRIBUTS_VAR, columnList = RealNode.ID_JPA)}
        )
public class ValeurMesureChloro implements Serializable, IGLACPEAggregateData<ValeurMesureChloro> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vmchloro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_mesure_chloro_vmchloro";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_chloro_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    // Associations des valeurs à une mesure
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureChloro.ID_JPA, referencedColumnName = MesureChloro.ID_JPA, nullable = false)
    private MesureChloro mesure;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;


    @Column(nullable = true)
    private Float valeur;

    /**
     *
     */
    public ValeurMesureChloro() {
        super();
    }

    /**
     *
     * @param mesure
     * @param variable
     * @param f
     * @param valeur
     */
    public ValeurMesureChloro(MesureChloro mesure, RealNode realNode, Float valeur) {
        super();
        this.mesure = mesure;
        this.valeur = valeur;
        this.realNode = realNode;
        if (!mesure.getValeurs().contains(this)) {
            mesure.getValeurs().add(this);
        }
    }

    // Id

    /**
     *
     * @return
     */
        public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    // MesureChloro

    /**
     *
     * @param mesure
     */
        public void setMesure(MesureChloro mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesureChloro getMesure() {
        return mesure;
    }

    // Valeur de la variable

    /**
     *
     * @return
     */
        public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return mesure.getProfondeur_max();
    }

    /**
     *
     * @return
     */
    @Override
    public LocalDate getDate() {
        return mesure.getSousSequenceChloro().getSequenceChloro().getDate();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return mesure.getSousSequenceChloro().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(getMesure().getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(getMesure().getSousSequenceChloro().getSequenceChloro().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public LocalTime getHeure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        return null;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMin() {
        return getMesure().getProfondeur_min();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMax() {
        return getMesure().getProfondeur_max();
    }

}// End of class

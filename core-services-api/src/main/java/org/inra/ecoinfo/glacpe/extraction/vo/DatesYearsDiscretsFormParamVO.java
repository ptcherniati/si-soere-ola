/**
 * OREILacs project - see LICENCE.txt for use created: 7 janv. 2010 09:55:01
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class DatesYearsDiscretsFormParamVO extends AbstractDatesFormParam implements Cloneable {

    private static final String MSG_YEAR_INVALID = "PROPERTY_MSG_YEAR_INVALID";
    private static final String MSG_BAD_SPLITTER = "PROPERTY_MSG_BAD_SPLITTER";

    private static final String SPLITTER = ",";

    /**
     *
     */
    public static final String LABEL = "DatesYearsDiscretsFormParam";

    private String years;

    /**
     *
     * @param ilm
     * @param localizationManager
     */
    public DatesYearsDiscretsFormParamVO(ILocalizationManager localizationManager) {
        super(localizationManager);
    }

    /**
     *
     */
    public DatesYearsDiscretsFormParamVO() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        for (Map<String, String> periodsMap : periods) {
            for (String key : periodsMap.keySet()) {
                if (testPeriodEmpty(periodsMap.get(key))) {
                    return true;
                }
            }
        }
        return false;

    }

    /**
     *
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(String HQLAliasDate) throws DateTimeException {
        String[] yearsArray = years.split(SPLITTER);
        int periodIndex = 0;
        for (int yearIndex = 0; yearIndex < yearsArray.length; yearIndex++) {
            int currentYear = Integer.parseInt(yearsArray[yearIndex].trim());
            for (Map<String, String> periodsMap : periods) {
                buildPeriod(currentYear, periodIndex++, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX), HQLAliasDate);
            }
        }

    }

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {

        String[] yearsArray = years.split(SPLITTER);

        for (int yearIndex = 0; yearIndex < yearsArray.length; yearIndex++) {
            int currentYear = Integer.parseInt(yearsArray[yearIndex].trim());
            for (Map<String, String> periodsMap : periods) {
                printStream.println(String.format("    %s", buildPeriodString(currentYear, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
            }
        }

    }

    /**
     *
     * @return @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(bos);

        if (!getIsValid()) {
            printValidityMessages(printStream);
            return bos.toString();
        }
        if (!years.matches("^( *[0-9]{4} *)(, *[0-9]{4} *)*$")) {
            validityMessages.add(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_BAD_SPLITTER));
            printValidityMessages(printStream);

            return bos.toString();
        }
        String[] yearsArray = years.split(SPLITTER);

        for (int yearIndex = 0; yearIndex < yearsArray.length; yearIndex++) {
            int currentYear = Integer.parseInt(yearsArray[yearIndex].trim());
            for (Map<String, String> periodsMap : periods) {
                if (periodsMap.get(START_INDEX) != null && !periodsMap.get(START_INDEX).trim().isEmpty() && periodsMap.get(END_INDEX) != null && !periodsMap.get(END_INDEX).trim().isEmpty()) {
                    printStream.println(String.format("    %s<br/>", buildPeriodString(currentYear, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
                }
            }
        }

        return bos.toString();

    }

    /**
     *
     */
    @Override
    public void customValidate() {

        if (years == null || years.isEmpty() || !years.matches("^( *[0-9]{4} *)(, *[0-9]{4} *)*$")) {
            validityMessages.add(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_YEAR_INVALID));
        }

    }

    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        return DATE_FORMAT_WT_YEAR;
    }

    /**
     *
     * @return
     */
    public String getYears() {
        return years;
    }

    /**
     *
     * @param years
     */
    public void setYears(String years) {
        this.years = years;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        List<Periode> periodes = new LinkedList<AbstractDatesFormParam.Periode>();
        String[] years = getYears().split(SPLITTER);
        for (int i = 0; i < years.length; i++) {
            for (Map<String, String> period : getPeriods()) {
                periodes.add(new Periode(period.get(AbstractDatesFormParam.START_INDEX).concat("/").concat(years[i]), period.get(AbstractDatesFormParam.END_INDEX).concat("/").concat(years[i].trim())));
            }
        }
        return periodes;
    }

    /**
     *
     * @return
     */
    public DatesYearsDiscretsFormParamVO clone() {
        DatesYearsDiscretsFormParamVO dateForm = new DatesYearsDiscretsFormParamVO(localizationManager);
        List<Map<String, String>> periodMap = new LinkedList<Map<String, String>>();
        for (Map<String, String> period : this.periods) {
            Map<String, String> periodIndex = new HashMap<String, String>();
            for (String key : period.keySet()) {
                periodIndex.put(key, period.get(key));
            }
            periodMap.add(periodIndex);
        }
        dateForm.setYears(this.getYears());
        dateForm.setPeriods(periodMap);
        return dateForm;
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public List<IntervalDate> getSelectedDates() throws DateTimeException {
        List<IntervalDate> intervalDates = new LinkedList<>();
        if (getIsValid()) {
            String[] yearsArray = years.split(SPLITTER);
            for (int yearIndex = 0; yearIndex < yearsArray.length; yearIndex++) {
                String currentYear = yearsArray[yearIndex].trim();
                for (Map<String, String> p : periods) {
                    if (p.get(START_INDEX) != null && !p.get(START_INDEX).trim().isEmpty() && p.get(END_INDEX) != null && !p.get(END_INDEX).trim().isEmpty()) {
                        try {
                            intervalDates.add(new IntervalDate(p.get(START_INDEX).concat("/").concat(currentYear), p.get(END_INDEX).concat("/").concat(currentYear), DateUtil.DD_MM_YYYY));
                        } catch (Exception ex) {
                            continue;
                        }
                    }
                }
            }
        }
        return intervalDates;
    }
}

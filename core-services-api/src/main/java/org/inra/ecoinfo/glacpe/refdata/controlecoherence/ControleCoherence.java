package org.inra.ecoinfo.glacpe.refdata.controlecoherence;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = ControleCoherence.TABLE_NAME)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "datatypeCDC")
public class ControleCoherence implements Serializable {

    /**
     *
     */
    public static final String ID_JPA = "cdc_id";

    /**
     *
     */
    public static final String TABLE_NAME = "controle_coherence_cdc";
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    private Long id;

    /**
     *
     */
    @Column(name = "valeur_min")
    protected Float valeurMin;

    /**
     *
     */
    @Column(name = "valeur_max")
    protected Float valeurMax;


    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = DatatypeVariableUniteGLACPE.ID_JPA, referencedColumnName = DatatypeVariableUniteGLACPE.ID_JPA, nullable = false)
    private DatatypeVariableUniteGLACPE datatypeVariableUnite;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = SiteGLACPE.ID_JPA, referencedColumnName = SiteGLACPE.ID_JPA, nullable = true)
    private SiteGLACPE site;
    /**
     *
     */
    public ControleCoherence() {
        super();
    }

    /**
     *
     * @param site
     * @param datatypeVariableUnite
     * @param minValue
     * @param maxValue
     */
    public ControleCoherence(SiteGLACPE site, DatatypeVariableUniteGLACPE datatypeVariableUnite, Float minValue, Float maxValue) {
        this.site = site;
        this.datatypeVariableUnite = datatypeVariableUnite;
        this.valeurMin = minValue;
        this.valeurMax = maxValue;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Float getValeurMin() {
        return valeurMin;
    }

    /**
     *
     * @param valeurMin
     */
    public void setValeurMin(Float valeurMin) {
        this.valeurMin = valeurMin;
    }

    /**
     *
     * @return
     */
    public Float getValeurMax() {
        return valeurMax;
    }

    /**
     *
     * @param valeurMax
     */
    public void setValeurMax(Float valeurMax) {
        this.valeurMax = valeurMax;
    }

    /**
     *
     * @return
     */
    public DatatypeVariableUniteGLACPE getDatatypeVariableUnite() {
        return datatypeVariableUnite;
    }

    /**
     *
     * @param datatypeVariableUnite
     */
    public void setDatatypeVariableUnite(DatatypeVariableUniteGLACPE datatypeVariableUnite) {
        this.datatypeVariableUnite = datatypeVariableUnite;
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public void setSite(SiteGLACPE site) {
        this.site = site;
    }

    /**
     *
     * @param value
     * @return
     */
    public boolean checkValue(Float value) {
        return value != null && value >= valeurMin && value <= valeurMax;
    }

}

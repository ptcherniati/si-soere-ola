package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.jboss.logging.MDC;

/**
 *
 * @author ptcherniati
 */
public class DepthRequestParamVO implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.ui.messages";

    private static final String MSG_RANGE_DEPTHS_SELECTED = "PROPERTY_MSG_RANGE_DEPTHS_SELECTED";
    private static final String MSG_ALL_DEPTHS_SELECTED = "PROPERTY_MSG_ALL_DEPTHS_SELECTED";
    private static final String PATTERN_STRING_DEPTH_SUMMARY = "   %s: %.2f";
    private static final String MSG_MAX = "PROPERTY_MSG_MAX";
    private static final String MSG_MIN = "PROPERTY_MSG_MIN";
    private static final String MSG_INTERVAL = "MSG_INTERVAL";

    private static final String PROPERTY_MSG_BAD_DEPTH = "PROPERTY_MSG_BAD_DEPTH";

    private static final String PROPERTY_MSG_BAD_RATE = "PROPERTY_MSG_BAD_RATE";

    private static final String MESSAGE_NO_VALUE_IN_INTERVAL = "MESSAGE_NO_VALUE_IN_INTERVAL";

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    private Float depthMin = 0f;
    private Float depthMax = 0f;
    private Float theDepthMin = 0f;
    private Float theDepthMax = 0f;
    private Boolean allDepth = true;
    private Boolean validMinMAxDepht = false;
    private List<Float> selectedDepths = new LinkedList();
    private List<Float> availablesDepths = new LinkedList();

    /**
     *
     * @param ilm
     * @param localizationManager
     */
    public DepthRequestParamVO(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    /**
     *
     */
    public DepthRequestParamVO() {
        super();
    }

    private List<Float> intervalOfDephts() {
        selectedDepths = availablesDepths.stream().filter(f -> f >= this.depthMin && f <= depthMax).collect(Collectors.toList());
        return selectedDepths;
    }

    /**
     *
     * @return
     */
    public Float getDepthMin() {
        return depthMin;
    }

    /**
     *
     * @param min
     * @param max
     * @return
     */
    public List<Float> getValues(Float min, Float max) {
        return availablesDepths.stream().filter(f -> f >= min && f <= max).collect(Collectors.toList());
    }

    /**
     *
     * @param depthMin
     */
    public void setDepthMin(Float depthMin) {
        if (this.depthMin.equals(depthMin)) {
            return;
        }
        List<Float> values = getValues(depthMin, depthMax);
        if (values.isEmpty()) {
            sendNoValueInIntervalMessage(depthMin, depthMax);
            this.depthMin = availablesDepths.stream()
                    .filter(v-> v<depthMin)
                    .max(Float::compareTo)
                    .orElse(this.theDepthMin);
                    
            return;
        }else{
            this.depthMin = values.stream()
                    .min(Float::compareTo)
                    .orElse(this.theDepthMin);
        }
        this.selectedDepths = getValues(this.depthMin, depthMax);
    }
    
    private void sendNoValueInIntervalMessage(Float minValue, Float maxValue ){
        String message = String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, MESSAGE_NO_VALUE_IN_INTERVAL), minValue, maxValue);
            FacesMessage facesMessage = new FacesMessage(message);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    /**
     *
     * @return
     */
    public Float getDepthMax() {
        return depthMax;
    }

    /**
     *
     * @param depthMax
     */
    public void setDepthMax(Float depthMax) {
        if (this.depthMax.equals(depthMax)) {
            return;
        }
        List<Float> values = getValues(depthMin, depthMax);
        if (values.isEmpty()) {
            sendNoValueInIntervalMessage(depthMin, depthMax);
            this.depthMax = availablesDepths.stream()
                    .filter(v-> v>depthMax)
                    .min(Float::compareTo)
                    .orElse(this.theDepthMax);
            return;
        }else{
            this.depthMax = values.stream()
                    .max(Float::compareTo)
                    .orElse(this.theDepthMax);
        }
        this.selectedDepths = getValues(depthMin, this.depthMax);
    }

    /**
     *
     * @return
     */
    public Float getTheDepthMin() {
        return theDepthMin;
    }

    /**
     *
     * @return
     */
    public Float getTheDepthMax() {
        return theDepthMax;
    }

    /**
     *
     * @return
     */
    public Boolean getAllDepth() {
        return allDepth;
    }

    /**
     *
     * @param allDepth
     */
    public void setAllDepth(Boolean allDepth) {
        this.allDepth = allDepth;
        depthMin = availablesDepths.stream().min(Float::compare).orElse(null);
        depthMax = availablesDepths.stream().max(Float::compare).orElse(null);
        selectedDepths = availablesDepths;
    }

    /**
     *
     * @return
     */
    public List<Float> getSelectedDepths() {
        return selectedDepths;
    }

    /**
     *
     * @param selectedDepths
     */
    public void setSelectedDepths(List<Float> selectedDepths) {
        this.selectedDepths = selectedDepths;
    }

    /**
     *
     * @return
     */
    public List<Float> getAvailablesDepths() {
        return availablesDepths;
    }

    /**
     *
     * @param availablesDepths
     */
    public void setAvailablesDepths(List<Float> availablesDepths) {
        this.availablesDepths = availablesDepths.stream().filter(f -> f != null).sorted(Float::compare).collect(Collectors.toList());
        theDepthMin = depthMin = availablesDepths.stream().min(Float::compare).orElse(null);
        theDepthMax = depthMax = availablesDepths.stream().max(Float::compare).orElse(null);
        intervalOfDephts();
    }

    /**
     *
     * @return
     */
    public String getSummaryHTML() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(bos);
        if (getAllDepth()) {
            printStream.println(String.format("<div style='display:block'>%s</div>", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_ALL_DEPTHS_SELECTED)));
        } else {
            printStream.println(String.format("<div style='display:block'>%s:</div>", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_RANGE_DEPTHS_SELECTED)));
            printStream.println(String.format("<ul><li>".concat(PATTERN_STRING_DEPTH_SUMMARY).concat("</li>"), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN), getDepthMin()));
            printStream.println(String.format("<li>".concat(PATTERN_STRING_DEPTH_SUMMARY).concat("</li></ul>"), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX), getDepthMax()));
        }
        printStream.println();
        try {
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bos.toString();
    }
    
    /**
     *
     * @return
     */
    public String getIntervalMessage(){
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, MSG_INTERVAL), depthMin, depthMax);
    }

    /**
     *
     * @param printStream
     */
    public void buildSummary(PrintStream printStream) {

        if (getAllDepth()) {
            final String depths = getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_ALL_DEPTHS_SELECTED);
            printStream.println(depths);
            MDC.put("extraction.depths", depths);
        } else {
            printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_RANGE_DEPTHS_SELECTED));
            final String depthMinString = String.format(PATTERN_STRING_DEPTH_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN), getDepthMin());
            printStream.println(depthMinString);
            final String depthMaxString = String.format(PATTERN_STRING_DEPTH_SUMMARY, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX), getDepthMax());
            printStream.println(depthMaxString);
            MDC.put("extraction.depths", String.format("%s-%s", depthMinString, depthMaxString));
        }
        printStream.println();

    }

    /**
     *
     * @return
     */
    public Boolean isValidDepht() {
        return !selectedDepths.isEmpty();
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @return
     */
    public DepthRequestParamVO clone() {
        DepthRequestParamVO depthVO = new DepthRequestParamVO(this.localizationManager);
        depthVO.allDepth= this.allDepth;
        depthVO.depthMin=this.depthMin;
        depthVO.depthMax=this.depthMax;
        depthVO.intervalOfDephts();
        return depthVO;
    }
}

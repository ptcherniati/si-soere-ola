package org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

/**
 *
 * @author mylene
 */
@Entity
@Table(name = SequenceHauteFrequence.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {VersionFile.ID_JPA, "date_prelevement"}),
        indexes = {
            @Index(name =SequenceHauteFrequence.INDEX_ATTRIBUTS_IVF , columnList = VersionFile.ID_JPA)
        })
public class SequenceHauteFrequence implements Serializable {
        

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sequence_haute_frequence_shautefrequence";

    /**
     *
     */
    static public final String ID_JPA = "shautefrequence_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_hautefrequence_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSITE = "psi_hautefrequence_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_hautefrequence_ikey";
    
    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private LocalDate datePrelevement;


    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @OneToMany(mappedBy = "sequence", cascade = ALL)
    private List<SousSequenceHauteFrequence> sousSequences = new LinkedList<SousSequenceHauteFrequence>();

    /**
     *
     */
    public SequenceHauteFrequence() {
        super();
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }    
    
    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }
    
    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(LocalDate datePrelevement) {
        this.datePrelevement = datePrelevement;
    }
        
    /**
     *
     * @return
     */
    public List<SousSequenceHauteFrequence> getSousSequences() {
        return sousSequences;
    }
    
    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequenceHauteFrequence> sousSequences) {
        this.sousSequences = sousSequences;
    }
    
    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }



}

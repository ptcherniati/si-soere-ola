package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = SousSequenceContenuStomacaux.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SequenceContenuStomacaux.ID_JPA, Plateforme.ID_JPA, SousSequenceContenuStomacaux.ESPECE_POISSON}),
        indexes = {
            @Index(name = SousSequenceContenuStomacaux.INDEX_ATTRIBUTS_PLATEFORME, columnList = Plateforme.ID_JPA),
            @Index(name = SequenceContenuStomacaux.ID_JPA, columnList = SequenceContenuStomacaux.ID_JPA),
            @Index(name = OutilsMesure.ID_JPA, columnList = OutilsMesure.ID_JPA)
        })
public class SousSequenceContenuStomacaux implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sous_sequence_contenus_stomacaux_sscontenustomacaux";

    /**
     *
     */
    static public final String ID_JPA = "sscontenustomacaux_id";

    /**
     *
     */
    static public final String MAILLE_FILET = "maille";

    /**
     *
     */
    static public final String ESPECE_POISSON = "espece";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_conditions_ikey";


    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequenceContenuStomacaux.ID_JPA, referencedColumnName = SequenceContenuStomacaux.ID_JPA, nullable = false)
    private SequenceContenuStomacaux sequence;

    @OneToMany(mappedBy = "sousSequence", cascade = ALL)
    private List<MesureContenuStomacaux> mesures = new LinkedList<MesureContenuStomacaux>();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = OutilsMesure.ID_JPA, referencedColumnName = OutilsMesure.ID_JPA, nullable = false)
    private OutilsMesure outilsMesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MAILLE_FILET, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative maille;

    @Column(name = ESPECE_POISSON, nullable = false)
    private String espece;
    /**
     *
     */
    public SousSequenceContenuStomacaux() {
        super();
    }

    /**
     *
     * @return
     */
    public SequenceContenuStomacaux getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     */
    public void setSequence(SequenceContenuStomacaux sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     */
    public List<MesureContenuStomacaux> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureContenuStomacaux> mesures) {
        this.mesures = mesures;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getMaille() {
        return maille;
    }

    /**
     *
     * @param maille
     */
    public void setMaille(ValeurQualitative maille) {
        this.maille = maille;
    }

    /**
     *
     * @return
     */
    public String getEspece() {
        return espece;
    }

    /**
     *
     * @param espece
     */
    public void setEspece(String espece) {
        this.espece = espece;
    }
}

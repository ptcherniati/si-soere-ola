package org.inra.ecoinfo.glacpe.extraction.vo;

import org.inra.ecoinfo.refdata.site.Site;

/**
 *
 * @author ptcherniati
 */
public class SiteVO {

    private String nom;
    private String description;
    private Long id;
    private String code;


    /**
     *
     */
    public SiteVO() {}

    /**
     *
     * @param site
     */
    public SiteVO(Site site) {
        super();
        this.nom = site.getName();
        this.description = site.getDescription();
        this.id = site.getId();
        this.code = site.getCode();
    }
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

}

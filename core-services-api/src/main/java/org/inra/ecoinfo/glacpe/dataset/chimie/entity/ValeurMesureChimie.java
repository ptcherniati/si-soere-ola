/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:18:53
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;


/**
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(name = ValeurMesureChimie.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA, MesureChimie.ID_JPA}),
        indexes = {
            @Index(name = MesureChimie.ID_JPA, columnList = MesureChimie.ID_JPA),
            @Index(name = ValeurMesureChimie.INDEX_ATTRIBUTS_VAR, columnList = RealNode.ID_JPA)
        })
public class ValeurMesureChimie implements Serializable, IGLACPEAggregateData<ValeurMesureChimie> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_chimie_vmchimie";// valeur_mesureChimie

    /**
     *
     */
    static public final String ID_JPA = "vmchimie_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_chimie_ikey";

    /**
     *
     */
    @Id
    @Column(name = "vmchimie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureChimie.ID_JPA, referencedColumnName = MesureChimie.ID_JPA, nullable = false)
    private MesureChimie mesure;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @Column(nullable = true)
    private Float valeur;

    @Column(nullable = true)
    private ValeurQualitative flag;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;
    /**
     *
     */
    public ValeurMesureChimie() {
        super();
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureChimie mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesureChimie getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }


    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return this.getMesure().getProfondeurReelle() != null ? this.getMesure().getProfondeurReelle() : this.getMesure().getProfondeurMax();
    }
    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return this.getMesure().getSousSequence().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(getMesure().getSousSequence().getSequence().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(getMesure().getSousSequence().getSequence().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return this.getMesure().getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals("mesure") ? this.getMesure().getSousSequence().getOutilsMesure() : null;
    }

    /**
     *
     * @return
     */
    @Override
    public LocalTime getHeure() {
        return  null;
    }

    /**
     *
     * @return
     */
    @Override
    public LocalDate getDate() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        return this.getMesure().getSousSequence().getOutilsMesure().getTypeOutilsMesure().getType().getValeur().equals("mesure") ? null : this.getMesure().getSousSequence().getOutilsMesure();
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getFlag() {
        return flag;
    }

    /**
     *
     * @param flag
     */
    public void setFlag(ValeurQualitative flag) {
        this.flag = flag;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMin() {
        return getMesure().getProfondeurMin();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMax() {
        return getMesure().getProfondeurMax();
    }
}

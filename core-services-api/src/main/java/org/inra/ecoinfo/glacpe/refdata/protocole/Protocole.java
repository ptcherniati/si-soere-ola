/**
 * OREILacs project - see LICENCE.txt for use created: 3 avr. 2009 10:48:45
 */
package org.inra.ecoinfo.glacpe.refdata.protocole;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;


/**
 * @author "Antoine Schellenberger"
 */
@Entity
@Table(name = Protocole.TABLE_NAME)
public class Protocole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "prot_id";

    /**
     *
     */
    public static final String TABLE_NAME = "protocole_prot";

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @Column(nullable = false)
    private String URL;

    @Column( columnDefinition = "TEXT")
    private String legende;

    @Column( columnDefinition = "TEXT")
    private String descriptif;

    @Column(nullable = false, unique = true)
    private String nom;

    @Column(nullable = false, unique = true)
    private String code;

    /**
     *
     */
    public Protocole() {
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getDescriptif() {
        return descriptif;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLegende() {
        return legende;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getURL() {
        return URL;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param descriptif
     */
    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param legende
     */
    public void setLegende(String legende) {
        this.legende = legende;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @param url
     */
    public void setURL(String url) {
        URL = url;
    }

}
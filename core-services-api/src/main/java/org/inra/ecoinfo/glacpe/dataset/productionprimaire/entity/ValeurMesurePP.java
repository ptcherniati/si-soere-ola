/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:18:53
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = ValeurMesurePP.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA, MesurePP.ID_JPA}),
        indexes = {
            @Index(name = ValeurMesurePP.INDEX_ATTRIBUTS_VAR,columnList = RealNode.ID_JPA)
        })
public class ValeurMesurePP implements Serializable, IGLACPEAggregateData<ValeurMesurePP> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vmpp_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_mesure_production_primaire_vmpp";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_pp_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesurePP.ID_JPA, referencedColumnName = MesurePP.ID_JPA, nullable = false)
    private MesurePP mesure;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @Column(nullable = true)
    private Float valeur;

    /**
     *
     */
    public ValeurMesurePP() {
        super();
    }

    /**
     *
     * @param mesure
     * @param variable
     * @param f
     * @param valeur
     */
    public ValeurMesurePP(MesurePP mesure, RealNode realNode, Float valeur) {
        super();
        this.mesure = mesure;
        this.realNode = realNode;
        this.valeur = valeur;
        if (!mesure.getValeurs().contains(this)) {
            mesure.getValeurs().add(this);
        }
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesurePP mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesurePP getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return mesure.getProfondeur();
    }

    /**
     *
     * @return
     */
    @Override
    public LocalDate getDate() {
        return mesure.getSousSequencePP().getSequencePP().getDate();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return mesure.getSousSequencePP().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(getMesure().getSousSequencePP().getSequencePP().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(getMesure().getSousSequencePP().getSequencePP().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public LocalTime getHeure() {
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMin() {
        return getMesure().getValeurs().stream().filter(v->v.getDepth()!=null).map(v->v.getDepth()).min(Float::compare).orElse(null);
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMax() {
        return getMesure().getValeurs().stream().filter(v->v.getDepth()!=null).map(v->v.getDepth()).min(Float::compare).orElse(null);
    }
}

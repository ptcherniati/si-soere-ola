/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = SequencePP.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {VersionFile.ID_JPA, SequencePP.NAME_ATTRIBUTS_DATE}),
        indexes = {
            @Index(name = SequencePP.INDEX_ATTRIBUTS_IVF, columnList = VersionFile.ID_JPA)
        })
public class SequencePP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "spp_id";

    /**
     *
     */
    public static final String TABLE_NAME = "sequence_production_primaire_spp";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_DATE = "date";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_pp_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_pp_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_pp_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(nullable = false, name = NAME_ATTRIBUTS_DATE)
    private LocalDate date;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @OneToMany(mappedBy = "sequencePP", cascade = ALL)
    private List<SousSequencePP> sousSequences = new LinkedList<SousSequencePP>();

    /**
     *
     */
    public SequencePP() {
        super();
    }

    /**
     *
     * @param date
     * @param versionFile
     */
    public SequencePP(LocalDate date, VersionFile versionFile) {
        super();
        this.date = date;
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<SousSequencePP> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequencePP> sousSequences) {
        this.sousSequences = sousSequences;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

}

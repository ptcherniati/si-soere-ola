/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:16:22
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = MesurePP.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SousSequencePP.ID_JPA, MesurePP.NAME_ATTRIBUTS_PROFONDEUR}),
        indexes = {
            @Index(name = MesurePP.NAME_ATTRIBUTS_PROFONDEUR, columnList = MesurePP.NAME_ATTRIBUTS_PROFONDEUR ),
            @Index(name = SousSequencePP.ID_JPA, columnList = SousSequencePP.ID_JPA )
        })
public class MesurePP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "mpp_id";

    /**
     *
     */
    public static final String TABLE_NAME = "mesure_production_primaire_mpp";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_PROFONDEUR = "profondeur";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_LIGNE_FICHIER_ECHANGE = "ligne_fichier_echange";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequencePP.ID_JPA, referencedColumnName = SousSequencePP.ID_JPA, nullable = false)
    private SousSequencePP sousSequencePP;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    private List<ValeurMesurePP> valeurs = new LinkedList<ValeurMesurePP>();

    @Column(name = NAME_ATTRIBUTS_LIGNE_FICHIER_ECHANGE)
    private Long ligneFichierEchange;

    @Column(name = NAME_ATTRIBUTS_PROFONDEUR, nullable = false)
    private Float profondeur;

    /**
     *
     */
    public MesurePP() {
        super();
    }

    /**
     *
     * @param sousSequencePP
     * @param ligneFichierEchange
     * @param f
     * @param profondeur
     */
    public MesurePP(SousSequencePP sousSequencePP, Long ligneFichierEchange, Float profondeur) {
        super();
        this.sousSequencePP = sousSequencePP;
        this.ligneFichierEchange = ligneFichierEchange;
        this.profondeur = profondeur;
        if (!sousSequencePP.getMesures().contains(this)) {
            sousSequencePP.getMesures().add(this);
        }
    }

    /**
     *
     * @return
     */
    public List<ValeurMesurePP> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesurePP> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public SousSequencePP getSousSequencePP() {
        return sousSequencePP;
    }

    /**
     *
     * @param sousSequencePP
     */
    public void setSousSequencePP(SousSequencePP sousSequencePP) {
        this.sousSequencePP = sousSequencePP;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Float profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
}

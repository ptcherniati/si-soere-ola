package org.inra.ecoinfo.glacpe.dataset.sondemulti;

import org.inra.ecoinfo.glacpe.extraction.jsf.IDepthManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;


/**
 *
 * @author ptcherniati
 */
public interface ISondeMultiDatatypeManager extends IPlatformManager, IVariableManager, IDepthManager{
}

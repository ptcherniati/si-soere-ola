package org.inra.ecoinfo.glacpe.dataset.sondemulti.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.dataset.IGLACPEAggregateData;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = ValeurMesureSondeMulti.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA, MesureSondeMulti.ID_JPA}),
        indexes = {
            @Index(name = ValeurMesureSondeMulti.INDEX_ATTRIBUTS_VAR, columnList = RealNode.ID_JPA)
        })
public class ValeurMesureSondeMulti implements Serializable, IGLACPEAggregateData<ValeurMesureSondeMulti> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_sonde_multi_vmsondemulti";

    /**
     *
     */
    static public final String ID_JPA = "vmsondemulti_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_sonde_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureSondeMulti.ID_JPA, referencedColumnName = MesureSondeMulti.ID_JPA, nullable = false)
    private MesureSondeMulti mesure;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @Column(nullable = true)
    private Float valeur;
    /**
     *
     */
    public ValeurMesureSondeMulti() {
        super();
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureSondeMulti mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public MesureSondeMulti getMesure() {
        return mesure;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }


    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getValue() {
        return getValeur();
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepth() {
        return this.getMesure().getProfondeur();
    }

    /**
     *
     * @return
     */
    @Override
    public LocalDate getDate() {
        return this.getMesure().getSousSequence().getSequence().getDatePrelevement();
    }

    /**
     *
     * @return
     */
    @Override
    public Plateforme getPlateforme() {
        return this.getMesure().getSousSequence().getPlateforme();
    }

    /**
     *
     * @return
     */
    @Override
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(getMesure().getSousSequence().getSequence().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(getMesure().getSousSequence().getSequence().getVersionFile().getDataset());
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsMesure() {
        return this.getMesure().getSousSequence().getOutilsMesure();
    }

    /**
     *
     * @return
     */
    @Override
    public LocalTime getHeure() {
        return this.getMesure().getHeure();
    }

    /**
     *
     * @return
     */
    @Override
    public OutilsMesure getOutilsPrelevement() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMin() {
        return getMesure().getValeurs().stream().filter(v->v.getDepth()!=null).map(v->v.getDepth()).min(Float::compare).orElse(null);
    }

    /**
     *
     * @return
     */
    @Override
    public Float getDepthMax() {
        return getMesure().getValeurs().stream().filter(v->v.getDepth()!=null).map(v->v.getDepth()).min(Float::compare).orElse(null);
    }
}

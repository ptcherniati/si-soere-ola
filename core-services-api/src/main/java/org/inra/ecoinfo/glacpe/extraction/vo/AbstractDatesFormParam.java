package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractDatesFormParam implements IDateFormParameter {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.extraction.vo.messages";

    private static final String MSG_END_DATE_INVALID = "PROPERTY_MSG_END_DATE_INVALID";
    private static final String MSG_START_DATE_INVALID = "PROPERTY_MSG_START_DATE_INVALID";
    private static final String MSG_END_DATE_EMPTY = "PROPERTY_MSG_END_DATE_EMPTY";
    private static final String MSG_START_DATE_EMPTY = "PROPERTY_MSG_START_DATE_EMPTY";
    private static final String MSG_BAD_PERIODE = "PROPERTY_MSG_BAD_PERIODE";

    /**
     *
     */
    protected static final String MSG_FROM = "PROPERTY_MSG_FROM";

    /**
     *
     */
    protected static final String MSG_TO = "PROPERTY_MSG_TO";

    private static final String OR_CONDITION = "or";

    /**
     *
     */
    protected static final String DATE_FORMAT = "dd/MM/yyyy";

    /**
     *
     */
    protected static final String DATE_FORMAT_WT_YEAR = "dd/MM";


    /**
     *
     */
    protected static final String INITIAL_SQL_CONDITION = "and (";

    /**
     *
     */
    protected static final String SQL_CONDITION = "%s between :%s and :%s or ";

    /**
     *
     */
    protected static final String END_PREFIX = "end";

    /**
     *
     */
    protected static final String START_PREFIX = "start";


    /**
     *
     */
    public static final String START_INDEX = "start";

    /**
     *
     */
    public static final String END_INDEX = "end";
    /**
     *
     */
    protected List<Map<String, String>> periods;

    /**
     *
     */
    protected ILocalizationManager localizationManager;


    /**
     *
     */
    protected String SQLCondition;

    /**
     *
     */
    protected Map<String, LocalDate> parametersMap = new HashMap<String, LocalDate>();

    /**
     *
     */
    protected List<String> validityMessages = new LinkedList<String>();
    /**
     *
     * @param ilm
     * @param localizationManager
     */
    public AbstractDatesFormParam(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }
    /**
     *
     */
    public AbstractDatesFormParam() {
        super();
    }
    /**
     *
     * @return
     */
    public abstract boolean isEmpty();
    /**
     *
     */
    public abstract void customValidate();

    /**
     *
     * @return
     * @throws DateTimeException
     */
    public abstract String getSummaryHTML() throws DateTimeException;

    /**
     *
     * @return
     * @throws DateTimeException
     */
    public abstract List<IntervalDate> getSelectedDates() throws DateTimeException;

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    public abstract void buildSummary(PrintStream printStream) throws DateTimeException;

    /**
     *
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    protected abstract void buildParameterMapAndSQLCondition(String HQLAliasDate) throws DateTimeException;

    /**
     *
     * @return
     */
    public abstract String getPatternDate();

    /**
     *
     * @param HQLAliasDate
     * @return
     * @throws DateTimeException
     */
    public String buildSQLCondition(String HQLAliasDate) throws DateTimeException {

        retrieveParametersMap(HQLAliasDate);
        return SQLCondition;
    }


    /**
     *
     * @param HQLAliasDate
     * @return
     * @throws DateTimeException
     */
    public Map<String, LocalDate> retrieveParametersMapAndBuildSQLCondition(String HQLAliasDate) throws DateTimeException {
        return retrieveParametersMap(HQLAliasDate);
    }

    private Map<String, LocalDate> retrieveParametersMap(String HQLAliasDate) throws DateTimeException {

        SQLCondition = INITIAL_SQL_CONDITION;
        buildParameterMapAndSQLCondition(HQLAliasDate);
        if (SQLCondition.lastIndexOf(OR_CONDITION) != -1) {
            SQLCondition = String.format("%s%s", SQLCondition.substring(0, SQLCondition.lastIndexOf(OR_CONDITION)), ")");
        } else {
            SQLCondition = String.format("%s%s", SQLCondition, ")");
        }

        return parametersMap;
    }

    /**
     *
     * @param period
     * @return
     */
    protected boolean testPeriodEmpty(String period) {
        if (period == null || period.trim().length() == 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param currentYear
     * @param periodStart
     * @param periodEnd
     * @return
     * @throws DateTimeException
     */
    protected String buildPeriodString(int currentYear, String periodStart, String periodEnd) throws DateTimeException {
        return String.format("%s %s/%d %s %s/%d", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_FROM), periodStart, currentYear, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_TO), periodEnd, currentYear);

    }

    /**
     *
     * @param currentYear
     * @param periodIndex
     * @param periodStart
     * @param periodEnd
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    protected void buildPeriod(int currentYear, int periodIndex, String periodStart, String periodEnd, String HQLAliasDate) throws DateTimeException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0 && periodEnd.trim().length() > 0) {
            String startParameterName = processDate(START_PREFIX, currentYear, periodIndex, periodStart);
            String endParameterName = processDate(END_PREFIX, currentYear, periodIndex, periodEnd);

            SQLCondition = SQLCondition.concat(String.format(SQL_CONDITION, HQLAliasDate, startParameterName, endParameterName));
        }

    }

    /**
     *
     * @param prefix
     * @param currentYear
     * @param periodIndex
     * @param date
     * @return
     * @throws DateTimeException
     */
    protected String processDate(String prefix, int currentYear, int periodIndex, String date) throws DateTimeException {
        String parameterName = String.format("%s%d%d", prefix, periodIndex, currentYear);
        final String dateString = String.format("%s/%d", date, currentYear);
        LocalDate formattedDate = DateUtil.readLocalDateFromText(DATE_FORMAT, dateString);
        parametersMap.put(parameterName, formattedDate);
        return parameterName;
    }

    /**
     *
     * @param printStream
     */
    protected void printValidityMessages(PrintStream printStream) {
        printStream.println("<ul>");
        for (String validityMessage : validityMessages) {
            printStream.println(String.format("<li>%s</li>", validityMessage));
        }
        printStream.println("</ul>");
    }

    /**
     *
     */
    protected void testValidityPeriods() {
        Integer index = 0;
        DateFormat dateFormat = new SimpleDateFormat(getPatternDate());
        for (Map<String, String> periodsMap : periods) {
            LocalDate startDate = null;
            LocalDate endDate = null;

            if (periodsMap.get(START_INDEX) == null || periodsMap.get(START_INDEX).trim().isEmpty()) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_START_DATE_EMPTY), index + 1, getPatternDate()));
            } else {
                try {
                    if (!periodsMap.get(START_INDEX).matches(getPatternDate().replaceAll("[^/]", "."))) {
                        throw new DateTimeException(START_INDEX);
                    }
                    startDate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, periodsMap.get(START_INDEX).concat("/1900").substring(0, 10));
                    if (periodsMap.get(START_INDEX).compareTo(DateUtil.getUTCDateTextFromLocalDateTime(startDate, getPatternDate())) != 0) {
                        validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_START_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(START_INDEX)));
                    }
                } catch (DateTimeException e) {
                    validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_START_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(START_INDEX)));
                }
            }

            if (periodsMap.get(END_INDEX) == null || periodsMap.get(END_INDEX).trim().isEmpty()) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_END_DATE_EMPTY), index + 1, getPatternDate()));
            } else {
                try {
                    if (!periodsMap.get(END_INDEX).matches(getPatternDate().replaceAll("[^/]", "."))) {
                        throw new DateTimeException(END_INDEX);
                    }
                    endDate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, periodsMap.get(END_INDEX).concat("/1900").substring(0, 10));
                    if (periodsMap.get(END_INDEX).compareTo(DateUtil.getUTCDateTextFromLocalDateTime(endDate, getPatternDate())) != 0) {
                        validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_END_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(END_INDEX)));
                    }
                } catch (DateTimeException e) {
                    validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_END_DATE_INVALID), index + 1, getPatternDate(), periodsMap.get(END_INDEX)));
                }
            }
            if (startDate != null && endDate != null && startDate.compareTo(endDate) > 0) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_BAD_PERIODE), index + 1));
            }

            index++;
        }
    }

    /**
     *
     * @return
     */
    public Boolean getIsValid() {
        validityMessages = new LinkedList<String>();

        customValidate();

        testValidityPeriods();

        if (validityMessages.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getPeriods() {
        return periods;
    }

    /**
     *
     * @param periods
     */
    public void setPeriods(List<Map<String, String>> periods) {
        this.periods = periods;
    }

    /**
     *
     * @return
     */
    public List<String> getValidityMessages() {
        return validityMessages;
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    public Map<String, LocalDate> retrieveParametersMap() throws DateTimeException {
        retrieveParametersMap("");
        return parametersMap;
    }


    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }
    /**
     *
     */
    public class Periode {
        
        private String dateStart;
        private String dateEnd;

        /**
         *
         * @param dateStart
         * @param string1
         * @param dateEnd
         */
        public Periode(String dateStart, String dateEnd) {
            super();
            this.dateStart = dateStart;
            this.dateEnd = dateEnd;
        }

        /**
         *
         * @param dateStart
         */
        public void setDateStart(String dateStart) {
            this.dateStart = dateStart;
        }

        /**
         *
         * @return
         */
        public String getDateStart() {
            return dateStart;
        }

        /**
         *
         * @param dateEnd
         */
        public void setDateEnd(String dateEnd) {
            this.dateEnd = dateEnd;
        }

        /**
         *
         * @return
         */
        public String getDateEnd() {
            return dateEnd;
        }
    }

}

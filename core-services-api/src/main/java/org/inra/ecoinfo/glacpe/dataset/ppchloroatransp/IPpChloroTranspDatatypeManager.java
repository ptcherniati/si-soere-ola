package org.inra.ecoinfo.glacpe.dataset.ppchloroatransp;

import org.inra.ecoinfo.glacpe.extraction.jsf.IDepthManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;

/**
 *
 * @author ptcherniati
 */
public interface IPpChloroTranspDatatypeManager extends IPlatformManager, IVariableManager, IDepthManager{

    /**
     *
     */
    public static final String CODE_DATATYPE_PP_CHLORO_TRANSP = "pp_chloro_transp";


}

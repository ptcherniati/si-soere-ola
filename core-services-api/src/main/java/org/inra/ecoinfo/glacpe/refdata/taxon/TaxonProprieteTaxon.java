package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.io.Serializable;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.proprietetaxon.ProprieteTaxon;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TaxonProprieteTaxon.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {Taxon.ID_JPA, ProprieteTaxon.ID_JPA}))
public class TaxonProprieteTaxon implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "taprota_id";

    /**
     *
     */
    public static final String TABLE_NAME = "taxon_propriete_taxon_taprota";// TaxonProprieteTaxon_taprota

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH})
    @JoinColumn(name = Taxon.ID_JPA, referencedColumnName = Taxon.ID_JPA, nullable = false)
    private Taxon taxon;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH})
    @JoinColumn(name = ProprieteTaxon.ID_JPA, referencedColumnName = ProprieteTaxon.ID_JPA, nullable = false)
    private ProprieteTaxon proprieteTaxon;

    @OneToOne(cascade = ALL)
    @JoinColumn(name = ValeurProprieteTaxon.ID_JPA, referencedColumnName = ValeurProprieteTaxon.ID_JPA, nullable = false)
    private ValeurProprieteTaxon valeurProprieteTaxon;

    /**
     *
     * @param taxon
     * @param pt
     * @param proprieteTaxon
     */
    public TaxonProprieteTaxon(Taxon taxon, ProprieteTaxon proprieteTaxon) {
        setTaxon(taxon);
        setProprieteTaxon(proprieteTaxon);
    }

    /**
     *
     */
    public TaxonProprieteTaxon() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Taxon getTaxon() {
        return taxon;
    }

    /**
     *
     * @param taxon
     */
    public void setTaxon(Taxon taxon) {
        this.taxon = taxon;
    }

    /**
     *
     * @return
     */
    public ProprieteTaxon getProprieteTaxon() {
        return proprieteTaxon;
    }

    /**
     *
     * @param proprieteTaxon
     */
    public void setProprieteTaxon(ProprieteTaxon proprieteTaxon) {
        this.proprieteTaxon = proprieteTaxon;
    }

    /**
     *
     * @return
     */
    public ValeurProprieteTaxon getValeurProprieteTaxon() {
        return valeurProprieteTaxon;
    }

    /**
     *
     * @param valeurProprieteTaxon
     */
    public void setValeurProprieteTaxon(ValeurProprieteTaxon valeurProprieteTaxon) {
        this.valeurProprieteTaxon = valeurProprieteTaxon;
    }

}

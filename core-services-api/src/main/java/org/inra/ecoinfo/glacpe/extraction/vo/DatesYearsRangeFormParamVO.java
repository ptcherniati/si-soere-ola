/**
 * OREILacs project - see LICENCE.txt for use created: 7 janv. 2010 09:55:01
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class DatesYearsRangeFormParamVO extends AbstractDatesFormParam implements Cloneable {

    private static final String MSG_DATE_END_FORMAT_INVALID = "PROPERTY_MSG_DATE_END_FORMAT_INVALID";
    private static final String MSG_DATE_START_FORMAT_INVALID = "PROPERTY_MSG_DATE_START_FORMAT_INVALID";
    private static final String MSG_DATE_START_EMPTY = "PROPERTY_MSG_DATE_START_EMPTY";
    private static final String MSG_DATE_END_EMPTY = "PROPERTY_MSG_DATE_END_EMPTY";

    /**
     *
     */
    public static final String LABEL = "DatesYearsRangeFormParam";

    private static final String PATTERN_DATE_YEAR = "yyyy";

    private List<Map<String, String>> years;

    /**
     *
     * @param ilm
     * @param localizationManager
     */
    public DatesYearsRangeFormParamVO(ILocalizationManager localizationManager) {
        super(localizationManager);
    }

    /**
     *
     */
    public DatesYearsRangeFormParamVO() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        for (Map<String, String> periodsMap : periods) {
            for (String key : periodsMap.keySet()) {
                if (testPeriodEmpty(periodsMap.get(key))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(String HQLAliasDate) throws DateTimeException {
        int periodIndex = 0;
        for (Map<String, String> yearMap : years) {

            for (Map<String, String> periodsMap : periods) {

                int startYear = Integer.parseInt(yearMap.get(START_INDEX));
                int endYear = Integer.parseInt(yearMap.get(END_INDEX));

                for (int currentYear = startYear; currentYear <= endYear; currentYear++) {
                    buildPeriod(currentYear, periodIndex++, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX), HQLAliasDate);
                }
            }
        }

    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getYears() {
        return years;
    }

    /**
     *
     * @param years
     */
    public void setYears(List<Map<String, String>> years) {
        this.years = years;
    }

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {

        for (Map<String, String> yearMap : years) {
            for (Map<String, String> periodsMap : periods) {

                int startYear = Integer.parseInt(yearMap.get(START_INDEX));
                int endYear = Integer.parseInt(yearMap.get(END_INDEX));

                for (int currentYear = startYear; currentYear <= endYear; currentYear++) {
                    printStream.println(String.format("    %s", buildPeriodString(currentYear, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
                }
            }
        }
    }

    /**
     *
     * @return @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(bos);

        if (!getIsValid()) {
            printValidityMessages(printStream);
            return bos.toString();
        }

        for (Map<String, String> yearMap : years) {
            for (Map<String, String> periodsMap : periods) {

                int startYear = Integer.parseInt(yearMap.get(START_INDEX));
                int endYear = Integer.parseInt(yearMap.get(END_INDEX));

                for (int currentYear = startYear; currentYear <= endYear; currentYear++) {
                    printStream.println(String.format("    %s <br/>", buildPeriodString(currentYear, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
                }
            }
        }
        return bos.toString();
    }

    /**
     *
     */
    @Override
    public void customValidate() {
        Integer index = 1;
        for (Map<String, String> yearMap : years) {
            if (yearMap.get(START_INDEX) == null || yearMap.get(START_INDEX).isEmpty()) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_DATE_START_EMPTY), index));
            } else {
                try {
                    DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "01/01/".concat(yearMap.get(START_INDEX)));
                } catch (DateTimeException e) {
                    validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_DATE_START_FORMAT_INVALID), index));
                }
            }

            if (yearMap.get(END_INDEX) == null || yearMap.get(END_INDEX).isEmpty()) {
                validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_DATE_END_EMPTY), index));
            } else {
                try {
                    DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "01/01/".concat(yearMap.get(END_INDEX)));
                } catch (DateTimeException e) {
                    validityMessages.add(String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_DATE_END_FORMAT_INVALID), index));
                }
            }

            index++;
        }

    }

    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        return DATE_FORMAT_WT_YEAR;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        List<Periode> periodes = new LinkedList<AbstractDatesFormParam.Periode>();
        for (Map<String, String> yearPeriod : getYears()) {
            for (int i = Integer.parseInt(yearPeriod.get(AbstractDatesFormParam.START_INDEX)); i <= Integer.parseInt(yearPeriod.get(AbstractDatesFormParam.END_INDEX)); i++) {
                for (Map<String, String> period : getPeriods()) {
                    periodes.add(new Periode(period.get(AbstractDatesFormParam.START_INDEX).concat("/").concat(Integer.toString(i)), period.get(AbstractDatesFormParam.END_INDEX).concat("/").concat(Integer.toString(i))));
                }
            }
        }
        return periodes;

    }

    /**
     *
     * @return
     */
    public DatesYearsRangeFormParamVO clone() {
        DatesYearsRangeFormParamVO dateForm = new DatesYearsRangeFormParamVO(localizationManager);
        List<Map<String, String>> periodMap = new LinkedList<Map<String, String>>();
        for (Map<String, String> period : this.periods) {
            Map<String, String> periodIndex = new HashMap<String, String>();
            for (String key : period.keySet()) {
                periodIndex.put(key, period.get(key));
            }
            periodMap.add(periodIndex);
        }
        dateForm.setPeriods(periodMap);
        dateForm.setYears(this.years);
        return dateForm;
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public List<IntervalDate> getSelectedDates() throws DateTimeException {
        List<IntervalDate> intervalDates = new LinkedList<>();
        if (getIsValid()) {
            for (Map<String, String> yearMap : years) {
                for (Map<String, String> p : periods) {

                    int startYear = Integer.parseInt(yearMap.get(START_INDEX));
                    int endYear = Integer.parseInt(yearMap.get(END_INDEX));

                    for (Integer currentYear = startYear; currentYear <= endYear; currentYear++) {
                        if (p.get(START_INDEX) != null && !p.get(START_INDEX).trim().isEmpty() && p.get(END_INDEX) != null && !p.get(END_INDEX).trim().isEmpty()) {
                            try {
                                intervalDates.add(new IntervalDate(p.get(START_INDEX).concat("/").concat(currentYear.toString()), p.get(END_INDEX).concat("/").concat(currentYear.toString()), DateUtil.DD_MM_YYYY));
                            } catch (Exception ex) {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return intervalDates;
    }
}

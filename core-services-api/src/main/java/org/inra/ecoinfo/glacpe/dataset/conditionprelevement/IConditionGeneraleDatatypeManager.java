package org.inra.ecoinfo.glacpe.dataset.conditionprelevement;

import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.glacpe.extraction.jsf.IDepthManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public interface IConditionGeneraleDatatypeManager extends IPlatformManager, IVariableManager, IDepthManager {

    /**
     *
     * @param selectedPlatforms
     * @param selectedDates
     * @return
     */
    Collection<DatatypeVariableUniteGLACPE> getAvailableVariablesTransparence(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates);

    /**
     *
     * @return
     */
    Collection<PlateformeProjetVO> getAvailablePlatformsTransparence();
}

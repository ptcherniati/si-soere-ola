package org.inra.ecoinfo.glacpe.synthesis.contenustomacaux;


import java.time.LocalDateTime;
import javax.persistence.Entity;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "ContenuStomacauxSynthesisDatatype")
public class SynthesisDatatype extends GenericSynthesisDatatype {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param site
     * @param minDate
     * @param maxDate
     * @param string1
     */
    
    public SynthesisDatatype(String site, LocalDateTime minDate, LocalDateTime maxDate, String idNodes) {
        super(minDate, maxDate, site, idNodes);
    }

    /**
     *
     */
    public SynthesisDatatype() {
        super();
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 2010 09:14:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.utils.LacsUtils;


/**
 * @author "Cédric Anache"
 * 
 */

@Entity
@Table(name = SequenceChloro.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {VersionFile.ID_JPA, SequenceChloro.NAME_ATTRIBUTS_DATE}),
        indexes = {
            @Index(name = SequenceChloro.INDEX_ATTRIBUTS_IVF, columnList = VersionFile.ID_JPA)
        })
public class SequenceChloro implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "schloro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "sequence_chloro_schloro";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_DATE = "date";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_chloro_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_chloro_ikey"; 

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_chloro_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = NAME_ATTRIBUTS_DATE, nullable = false)
    private LocalDate date;

    // Association des Sequences à un fichier d'échange
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    // Une séquence peut avoir plusieurs Mesures
    @OneToMany(mappedBy = "sequenceChloro", cascade = ALL)
    private List<SousSequenceChloro> sousSequences = new LinkedList<SousSequenceChloro>();

    /**
     *
     */
    public SequenceChloro() {
        super();
    }

    /**
     *
     * @param date
     * @param vf
     * @param projetSite
     * @param versionFile
     */
    public SequenceChloro(LocalDate date,VersionFile versionFile) {
        super();
        this.date = date;
        this.versionFile = versionFile;
    }

    // Id

    /**
     *
     * @return
     */
        public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    // Date

    /**
     *
     * @return
     */
        public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public List<SousSequenceChloro> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequenceChloro> sousSequences) {
        this.sousSequences = sousSequences;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return LacsUtils.getSiteFromDataset(versionFile.getDataset());
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return LacsUtils.getProjetFromDataset(versionFile.getDataset());
    }

}// end of class

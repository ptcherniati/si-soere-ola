package org.inra.ecoinfo.glacpe.dataset.sondemulti.entity;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.site.Site;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = MesureSondeMulti.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SousSequenceSondeMulti.ID_JPA, "profondeur"}),
        indexes = {
            @Index(name = SousSequenceSondeMulti.ID_JPA, columnList = SousSequenceSondeMulti.ID_JPA),
            @Index(name = "profondeur_sonde_ikey", columnList = "profondeur")
        })
public class MesureSondeMulti implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_sonde_multi_msondemulti";

    /**
     *
     */
    static public final String ID_JPA = "msondemulti_id";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceSondeMulti.ID_JPA, referencedColumnName = SousSequenceSondeMulti.ID_JPA, nullable = false)
    private SousSequenceSondeMulti sousSequence;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy(RealNode.ID_JPA)
    private List<ValeurMesureSondeMulti> valeurs = new LinkedList<ValeurMesureSondeMulti>();


    @Column(nullable = true)
    private Float profondeur;

    
    @Column(name = "heure", nullable = true)
    private LocalTime heure;
    /**
     *
     */
    public MesureSondeMulti() {
        super();
    }
    /**
     * Classe raccourcis
     *
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesureSondeMulti> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureSondeMulti> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Float profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public SousSequenceSondeMulti getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequenceSondeMulti sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }
}

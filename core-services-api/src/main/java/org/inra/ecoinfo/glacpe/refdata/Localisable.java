package org.inra.ecoinfo.glacpe.refdata;

/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:27:51
 */


import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;


/**
 * @author "Antoine Schellenberger"
 */
@MappedSuperclass
public class Localisable extends Nodeable{

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String LOCALISABLE_NAME_ID = "loc_id";

    /**
     *
     */
    @Column(nullable = true)
    protected Float latitude;

    /**
     *
     */
    @Column(nullable = true)
    protected Float longitude;

    /**
     *
     */
    @Column(nullable = true)
    protected Float altitude;

    /**
     *
     */
    public Localisable() {}

    /**
     * @param latitude2
     *            c'est la latitude
     * @param longitude
     * @param f2
     * @param altitude
     */
    public Localisable(final Float latitude2, Float longitude, Float altitude) {
        super();
        this.latitude = latitude2;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /**
     *
     * @return
     */
    public Float getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     */
    public Float getAltitude() {
        return altitude;
    }

    /**
     *
     * @param altitude
     */
    public void setAltitude(Float altitude) {
        this.altitude = altitude;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 7 janv. 2010 09:55:01
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.NotYetImplementedException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class DatesForm1ParamVO extends AbstractDatesFormParam {

    private String dateStart;
    private String dateEnd;

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return testPeriodEmpty(dateStart) && testPeriodEmpty(dateEnd);
    }

    /**
     *
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(String HQLAliasDate) throws DateTimeException {
        System.out.println("DatesForm1ParamVO - buildParameterMapAndSQLCondition ");
        if (dateStart != null && dateEnd != null && dateStart.trim().length() > 0 && dateEnd.trim().length() > 0) {
            String startParameterName = processDate(START_PREFIX, 0, 0, dateStart);
            String endParameterName = processDate(END_PREFIX, 0, 0, dateEnd);

            SQLCondition = SQLCondition.concat(String.format(SQL_CONDITION, HQLAliasDate, startParameterName, endParameterName));
        }
    }

    /**
     *
     * @param prefix
     * @param currentYear
     * @param periodIndex
     * @param date
     * @return
     * @throws DateTimeException
     */
    @Override
    protected String processDate(String prefix, int currentYear, int periodIndex, String date) throws DateTimeException {
        String parameterName = prefix;
        LocalDate p1start = DateUtil.readLocalDateFromText(DATE_FORMAT, date);
        parametersMap.put(parameterName, p1start);
        return parameterName;
    }

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {
        throw new NotYetImplementedException();
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {
        throw new NotYetImplementedException();

    }

    /**
     *
     */
    @Override
    public void customValidate() {
        throw new NotYetImplementedException();
    }

    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        throw new NotYetImplementedException();
    }

    /**
     *
     * @return
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     *
     * @return
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     *
     * @param dateStart
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     *
     * @param dateEnd
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public List<IntervalDate> getSelectedDates() throws DateTimeException {
        throw new NotYetImplementedException();
    }

}

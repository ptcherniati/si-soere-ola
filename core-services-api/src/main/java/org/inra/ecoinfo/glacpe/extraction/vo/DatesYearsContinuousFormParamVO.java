package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class DatesYearsContinuousFormParamVO extends AbstractDatesFormParam implements Cloneable {

    /**
     *
     */
    public static final String LABEL = "DatesYearsContinuousFormParam";

    /**
     *
     * @param ilm
     * @param localizationManager
     */
    public DatesYearsContinuousFormParamVO(ILocalizationManager localizationManager) {
        super(localizationManager);
    }

    /**
     *
     */
    public DatesYearsContinuousFormParamVO() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        for (Map<String, String> periodsMap : periods) {
            for (String key : periodsMap.keySet()) {
                if (testPeriodEmpty(periodsMap.get(key))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(String HQLAliasDate) throws DateTimeException {
        int periodIndex = 0;
        for (Map<String, String> periodsMap : periods) {
            buildContinuousPeriod(periodIndex++, periodsMap.get(START_INDEX), periodsMap.get(END_INDEX), HQLAliasDate);
        }
    }

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {

        for (Map<String, String> periodsMap : periods) {

            printStream.println(String.format("    %s", buildContinuousPeriodString(periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
        }
    }

    /**
     *
     * @return @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(bos);

        if (!getIsValid()) {
            printValidityMessages(printStream);
            return bos.toString();
        }

        for (Map<String, String> periodsMap : periods) {

            printStream.println(String.format("    %s<br/>", buildContinuousPeriodString(periodsMap.get(START_INDEX), periodsMap.get(END_INDEX))));
        }
        return bos.toString();
    }

    /**
     *
     * @param periodStart
     * @param periodEnd
     * @return
     * @throws DateTimeException
     */
    protected String buildContinuousPeriodString(String periodStart, String periodEnd) throws DateTimeException {
        return String.format("%s %s %s %s", getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_FROM), periodStart, getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_TO), periodEnd);
    }

    /**
     *
     * @param periodIndex
     * @param periodStart
     * @param periodEnd
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    protected void buildContinuousPeriod(int periodIndex, String periodStart, String periodEnd, String HQLAliasDate) throws DateTimeException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0 && periodEnd.trim().length() > 0) {
            LocalDate formattedStartDate = DateUtil.readLocalDateFromText(DATE_FORMAT, periodStart);

            String startParameterName = String.format("%s%d", START_PREFIX, periodIndex);
            String endParameterName = String.format("%s%d", END_PREFIX, periodIndex);

            parametersMap.put(startParameterName, formattedStartDate);

            LocalDate formattedEndDate = DateUtil.readLocalDateFromText(DATE_FORMAT, periodEnd);
            parametersMap.put(endParameterName, formattedEndDate);

            SQLCondition = SQLCondition.concat(String.format(SQL_CONDITION, HQLAliasDate, startParameterName, endParameterName));
        }

    }

    /**
     *
     */
    @Override
    public void customValidate() {

    }

    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        return DATE_FORMAT;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {

        List<Periode> periodes = new LinkedList<AbstractDatesFormParam.Periode>();
        for (Map<String, String> period : getPeriods()) {
            periodes.add(new Periode(period.get(AbstractDatesFormParam.START_INDEX), period.get(AbstractDatesFormParam.END_INDEX)));
        }
        return periodes;
    }

    /**
     *
     * @return
     */
    public DatesYearsContinuousFormParamVO clone() {
        DatesYearsContinuousFormParamVO dateForm = new DatesYearsContinuousFormParamVO(this.localizationManager);
        List<Map<String, String>> periodMap = new LinkedList<Map<String, String>>();
        for (Map<String, String> period : this.periods) {
            Map<String, String> periodIndex = new HashMap<String, String>();
            for (String key : period.keySet()) {
                periodIndex.put(key, period.get(key));
            }
            periodMap.add(periodIndex);
        }
        dateForm.setPeriods(periodMap);
        return dateForm;
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public List<IntervalDate> getSelectedDates() throws DateTimeException {
        if (getIsValid()) {
            return periods.stream()
                    .map(p -> {
                        try {
                            return new IntervalDate(p.get(START_INDEX), p.get(END_INDEX), DateUtil.DD_MM_YYYY);
                        } catch (Exception ex) {
                           return null;
                        }
                    })
                    .filter(i->i!=null)
                    .collect(Collectors.toList());
        }
        return new LinkedList<>();
    }
}

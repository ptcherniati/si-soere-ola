/**
 * OREILacs project - see LICENCE.txt for use created: 3 avr. 2009 10:38:17
 */
package org.inra.ecoinfo.glacpe.dataset.zooplancton.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = StadeDeveloppement.NAME_ENTITY_JPA)
public class StadeDeveloppement implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";

    /**
     *
     */
    public static final String ID_JPA = "sde_id";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "stadedeveloppement";

    /**
     *
     */
    public static final String FIELD_ENTITY_NAME = "nom";

    /**
     *
     */
    public static final String FIELD_ENTITY_DESCRIPTION = "description";

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @Column(nullable = false, unique = true, name = StadeDeveloppement.FIELD_ENTITY_NAME)
    private String nom;

    @Column(nullable = false, unique = true)
    private String code;

    
    @Column(name = StadeDeveloppement.FIELD_ENTITY_DESCRIPTION,columnDefinition = "TEXT")
    private String description;
    
    
    @Column(nullable = true,name = CODE_SANDRE, unique = true)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE , unique = true)
    private String contexte;
    /**
     *
     */
    public StadeDeveloppement() {
        super();
        
    }
    /**
     *
     * @param nom
     * @param string1
     * @param description
     */
    public StadeDeveloppement(String nom, String description) {
        super();
        setNom(nom);
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }
    
    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }


    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 29 oct. 2009 15:03:35
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import org.inra.ecoinfo.refdata.unite.Unite;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class UniteVO {

    private String code;
    private String nom;
    /**
     *
     * @param unite
     */
    public UniteVO(Unite unite) {
        super();
        this.code = unite.getCode();
        this.nom = unite.getName();
        
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

}

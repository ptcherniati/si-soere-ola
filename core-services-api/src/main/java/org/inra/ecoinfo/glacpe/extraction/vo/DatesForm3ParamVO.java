/**
 * OREILacs project - see LICENCE.txt for use created: 7 janv. 2010 09:55:01
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.PrintStream;
import java.time.DateTimeException;
import java.util.List;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.NotYetImplementedException;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class DatesForm3ParamVO extends AbstractDatesFormParam {

    private static final String SPLITTER = ",";
    private String period1Start;
    private String period1End;

    private String period2Start;
    private String period2End;

    private String period3Start;
    private String period3End;

    private String period4Start;
    private String period4End;

    private String years;

    /**
     *
     */
    public DatesForm3ParamVO() {}

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return testPeriodEmpty(period1Start) && testPeriodEmpty(period1End) && testPeriodEmpty(period2Start) && testPeriodEmpty(period2End) && testPeriodEmpty(period3Start) && testPeriodEmpty(period3End) && testPeriodEmpty(period4Start)
                && testPeriodEmpty(period4End);

    }

    /**
     *
     * @param HQLAliasDate
     * @throws DateTimeException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(String HQLAliasDate) throws DateTimeException {
        String[] yearsArray = years.split(SPLITTER);

        for (int i = 0; i < yearsArray.length; i++) {
            int currentYear = Integer.parseInt(yearsArray[i]);

            buildPeriod(currentYear, 1, period1Start, period1End, HQLAliasDate);
            buildPeriod(currentYear, 2, period2Start, period2End, HQLAliasDate);
            buildPeriod(currentYear, 3, period3Start, period3End, HQLAliasDate);
            buildPeriod(currentYear, 4, period4Start, period4End, HQLAliasDate);
        }

    }

    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {
        throw new NotYetImplementedException();
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {
        throw new NotYetImplementedException();

    }

    /**
     *
     */
    @Override
    public void customValidate() {
        throw new NotYetImplementedException();
    }

    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        throw new NotYetImplementedException();
    }

    /**
     *
     * @return
     */
    public String getPeriod1Start() {
        return period1Start;
    }

    /**
     *
     * @return
     */
    public String getPeriod1End() {
        return period1End;
    }

    /**
     *
     * @return
     */
    public String getPeriod2Start() {
        return period2Start;
    }

    /**
     *
     * @return
     */
    public String getPeriod2End() {
        return period2End;
    }

    /**
     *
     * @return
     */
    public String getPeriod3Start() {
        return period3Start;
    }

    /**
     *
     * @return
     */
    public String getPeriod3End() {
        return period3End;
    }

    /**
     *
     * @return
     */
    public String getPeriod4Start() {
        return period4Start;
    }

    /**
     *
     * @return
     */
    public String getPeriod4End() {
        return period4End;
    }

    /**
     *
     * @param period1Start
     */
    public void setPeriod1Start(String period1Start) {
        this.period1Start = period1Start;
    }

    /**
     *
     * @param period1End
     */
    public void setPeriod1End(String period1End) {
        this.period1End = period1End;
    }

    /**
     *
     * @param period2Start
     */
    public void setPeriod2Start(String period2Start) {
        this.period2Start = period2Start;
    }

    /**
     *
     * @param period2End
     */
    public void setPeriod2End(String period2End) {
        this.period2End = period2End;
    }

    /**
     *
     * @param period3Start
     */
    public void setPeriod3Start(String period3Start) {
        this.period3Start = period3Start;
    }

    /**
     *
     * @param period3End
     */
    public void setPeriod3End(String period3End) {
        this.period3End = period3End;
    }

    /**
     *
     * @param period4Start
     */
    public void setPeriod4Start(String period4Start) {
        this.period4Start = period4Start;
    }

    /**
     *
     * @param period4End
     */
    public void setPeriod4End(String period4End) {
        this.period4End = period4End;
    }

    /**
     *
     * @return
     */
    public String getYears() {
        return years;
    }

    /**
     *
     * @param years
     */
    public void setYears(String years) {
        this.years = years;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @return
     * @throws DateTimeException
     */
    @Override
    public List<IntervalDate> getSelectedDates() throws DateTimeException {
        throw new NotYetImplementedException();
    }

}

package org.inra.ecoinfo.glacpe.extraction.vo;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;

/**
 *
 * @author ptcherniati
 */
public class TaxonVO {

    private String nomLatin;
    private String rangTaxonomique;
    private String theme;
    private Long id;
    private String code;


    private List<TaxonVO> children = new LinkedList<TaxonVO>();

    /**
     *
     */
    public TaxonVO() {

    }

    /**
     *
     * @param taxon
     */
    public TaxonVO(Taxon taxon) {
        this.nomLatin = taxon.getNomLatin();
        this.rangTaxonomique = taxon.getTaxonNiveau().getNom();
        this.theme = taxon.getTheme();
        this.id = taxon.getId();
        this.code = taxon.getCode();

    }

    /**
     *
     * @param dbTaxon
     * @param bln
     * @param populateChildren
     */
    public TaxonVO(Taxon dbTaxon, boolean populateChildren) {
        this(dbTaxon);
        processTaxaChildren(dbTaxon, this);

    }
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    private void processTaxaChildren(Taxon dbTaxon, TaxonVO taxonParentVO) {

        if (dbTaxon.getTaxonsEnfants() != null && dbTaxon.getTaxonsEnfants().size() > 0) {

            for (Taxon subTaxon : dbTaxon.getTaxonsEnfants()) {
                TaxonVO subTaxonVO = new TaxonVO(subTaxon);
                processTaxaChildren(subTaxon, subTaxonVO);
                taxonParentVO.children.add(subTaxonVO);
            }
        } else {
            // Important sinon le TreeList de Flex ne considère pas le taxon comme
            // une feuille mais toujours comme un noeud

            taxonParentVO.children = null;
        }

    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNomLatin() {
        return nomLatin;
    }

    /**
     *
     * @param nomLatin
     */
    public void setNomLatin(String nomLatin) {
        this.nomLatin = nomLatin;
    }

    /**
     *
     * @return
     */
    public String getRangTaxonomique() {
        return rangTaxonomique;
    }

    /**
     *
     * @param rangTaxonomique
     */
    public void setRangTaxonomique(String rangTaxonomique) {
        this.rangTaxonomique = rangTaxonomique;
    }

    /**
     *
     * @return
     */
    public List<TaxonVO> getChildren() {
        return children;
    }

    /**
     *
     * @param children
     */
    public void setChildren(List<TaxonVO> children) {
        this.children = children;
    }

    /**
     *
     * @return
     */
    public String getTheme() {
        return theme;
    }

    /**
     *
     * @param theme
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }
}

package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class ExtractionParametersVO implements Serializable, Cloneable {
    
    private static final long serialVersionUID = 1L;
    private DatatypeVariableUniteGLACPE dvu;
    private String localizedName;
    private Boolean valueMin = false;
    private Boolean valueMax = false;
    private List<String> targetValues = new ArrayList<String>();
    private List<String> uncertainties = new ArrayList<String>();
    private ILocalizationManager localizationManager;

    /**
     *
     */
    public ExtractionParametersVO() {
        super();
    }

    /**
     *
     * @param name
     * @param ilm
     * @param unit
     */
    public ExtractionParametersVO(DatatypeVariableUniteGLACPE dvu, ILocalizationManager localizationManager) {
        super();
        this.localizationManager= localizationManager;
        setDvu(dvu);
    }

    /**
     *
     * @param dvu
     */
    public void setDvu(DatatypeVariableUniteGLACPE dvu) {
        this.dvu = dvu;
        localizedName = localizationManager.getLocalName(dvu.getVariable());
    }

    /**
     *
     * @return
     */
    public Boolean getValueMin() {
        return valueMin;
    }

    /**
     *
     * @param valueMin
     */
    public void setValueMin(Boolean valueMin) {
        this.valueMin = valueMin;
    }

    /**
     *
     * @return
     */
    public Boolean getValueMax() {
        return valueMax;
    }

    /**
     *
     * @param valueMax
     */
    public void setValueMax(Boolean valueMax) {
        this.valueMax = valueMax;
    }

    /**
     *
     * @return
     */
    public List<String> getTargetValues() {
        return targetValues;
    }

    /**
     *
     * @param targetValues
     */
    public void setTargetValues(List<String> targetValues) {
        this.targetValues = targetValues;
    }

    /**
     *
     * @param targetValue
     */
    public void addTargetValue(String targetValue) {
        this.targetValues.add(targetValue);
    }

    /**
     *
     * @return
     */
    public List<String> getUncertainties() {
        return uncertainties;
    }

    /**
     *
     * @param uncertainties
     */
    public void setUncertainties(List<String> uncertainties) {
        this.uncertainties = uncertainties;
    }

    /**
     *
     * @param uncertainty
     */
    public void addUncertainty(String uncertainty) {
        this.uncertainties.add(uncertainty);
    }

    /**
     *
     * @return
     */
    public String getUnit() {
        return dvu.getUnite().getName();
    }

    /**
     *
     * @return
     */
    public String getLocalizedName() {
        return localizedName;
    }

    /**
     *
     * @param localizedName
     */
    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    /**
     *
     * @return
     */
    public ExtractionParametersVO clone() {
        ExtractionParametersVO extractVO = new ExtractionParametersVO(dvu, localizationManager);
        extractVO.setLocalizedName(this.localizedName);
        extractVO.setValueMin(new Boolean(this.valueMin));
        extractVO.setValueMax(new Boolean(this.valueMax));
        List<String> targetVO = new ArrayList<String>();
        for (String target : targetValues) {
            targetVO.add(new String(target));
        }
        extractVO.setTargetValues(targetVO);
        List<String> uncertaintiesVO = new ArrayList<String>();
        for (String uncertainty : uncertainties) {
            uncertaintiesVO.add(new String(uncertainty));
        }
        extractVO.setUncertainties(uncertaintiesVO);
        return extractVO;
    }

    /**
     *
     * @return
     */
    public DatatypeVariableUniteGLACPE getDvu() {
        return dvu;
    }

}

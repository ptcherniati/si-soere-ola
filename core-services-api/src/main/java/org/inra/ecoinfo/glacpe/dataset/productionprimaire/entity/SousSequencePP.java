/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.glacpe.dataset.productionprimaire.entity;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = SousSequencePP.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SequencePP.ID_JPA, Plateforme.ID_JPA}),
        indexes = {
            @Index(name = SousSequencePP.INDEX_ATTRIBUTS_PLATEFORME, columnList = Plateforme.ID_JPA),
            @Index(name = SequencePP.ID_JPA, columnList = SequencePP.ID_JPA)
        })
public class SousSequencePP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "sspp_id";

    /**
     *
     */
    public static final String TABLE_NAME = "sous_sequence_production_primaire_sspp";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_pp_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequencePP.ID_JPA, referencedColumnName = SequencePP.ID_JPA, nullable = false)
    private SequencePP sequencePP;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @Column(name = "heure_debut_incub")
    private LocalTime heureDebutIncubation;

    @Column(name = "heure_fin_incub")
    
    private LocalTime heureFinIncubation;

    @Column(name = "duree_incub")
    
    private LocalTime dureeIncubation;

    @OneToMany(mappedBy = "sousSequencePP", cascade = ALL)
    private List<MesurePP> mesures = new LinkedList<MesurePP>();

    /**
     *
     */
    public SousSequencePP() {
        super();
    }

    /**
     *
     * @param sequencePP
     * @param plateforme
     * @param heureDebutIncubation
     * @param heureFinIncubation
     * @param lt2
     * @param dureeIncubation
     */
    public SousSequencePP(SequencePP sequencePP, Plateforme plateforme, LocalTime heureDebutIncubation, LocalTime heureFinIncubation, LocalTime dureeIncubation) {
        super();
        this.sequencePP = sequencePP;
        this.plateforme = plateforme;
        this.heureDebutIncubation = heureDebutIncubation;
        this.heureFinIncubation = heureFinIncubation;
        this.dureeIncubation = dureeIncubation;
        if (!sequencePP.getSousSequences().contains(this)) {
            sequencePP.getSousSequences().add(this);
        }
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public List<MesurePP> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesurePP> mesures) {
        this.mesures = mesures;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeureDebutIncubation() {
        return heureDebutIncubation;
    }

    /**
     *
     * @param heureDebutIncubation
     */
    public void setHeureDebutIncubation(LocalTime heureDebutIncubation) {
        this.heureDebutIncubation = heureDebutIncubation;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeureFinIncubation() {
        return heureFinIncubation;
    }

    /**
     *
     * @param heureFinIncubation
     */
    public void setHeureFinIncubation(LocalTime heureFinIncubation) {
        this.heureFinIncubation = heureFinIncubation;
    }

    /**
     *
     * @return
     */
    public LocalTime getDureeIncubation() {
        return dureeIncubation;
    }

    /**
     *
     * @param dureeIncubation
     */
    public void setDureeIncubation(LocalTime dureeIncubation) {
        this.dureeIncubation = dureeIncubation;
    }

    /**
     *
     * @return
     */
    public SequencePP getSequencePP() {
        return sequencePP;
    }

    /**
     *
     * @param sequencePP
     */
    public void setSequencePP(SequencePP sequencePP) {
        this.sequencePP = sequencePP;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }
}

/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 15:16:22
 */
package org.inra.ecoinfo.glacpe.dataset.chimie.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = SousSequenceChimie.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SequenceChimie.ID_JPA, Plateforme.ID_JPA, OutilsMesure.ID_JPA}),
        indexes = {
            @Index(name = SequenceChimie.ID_JPA, columnList = SequenceChimie.ID_JPA),
            @Index(name = SousSequenceChimie.INDEX_ATTRIBUTS_PLATEFORME, columnList = Plateforme.ID_JPA),
            @Index(name = OutilsMesure.ID_JPA, columnList = OutilsMesure.ID_JPA)
        })
public class SousSequenceChimie implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sous_sequence_chimie_sschimie";// sous_sequencechimie

    /**
     *
     */
    static public final String ID_JPA = "sschimie_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_chimie_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequenceChimie.ID_JPA, referencedColumnName = SequenceChimie.ID_JPA, nullable = false)
    private SequenceChimie sequence;

    @OneToMany(mappedBy = "sousSequence", cascade = ALL)
    private List<MesureChimie> mesures = new LinkedList<MesureChimie>();

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = OutilsMesure.ID_JPA, referencedColumnName = OutilsMesure.ID_JPA, nullable = true)
    private OutilsMesure outilsMesure;

    /**
     *
     */
    public SousSequenceChimie() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SequenceChimie getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     */
    public void setSequence(SequenceChimie sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     */
    public List<MesureChimie> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureChimie> mesures) {
        this.mesures = mesures;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }
}

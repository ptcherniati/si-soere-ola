package org.inra.ecoinfo.glacpe.identification.entity;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.inra.ecoinfo.identification.entity.RightRequest;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue("rightRequest_glacpe")
@Table(name = RightRequestGLACPE.NAME_ENTITY_JPA)
public class RightRequestGLACPE extends RightRequest {

    /**
     *
     */
    public static final String UTILISATEUR_ID_JPA = "usr_id";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "rightrequest_glacpe_rrg";
    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.identification.message";
    private static final String PROPERTY_MSG_REQUEST_TO_STRING = "PROPERTY_MSG_REQUEST_TO_STRING";

    @Column
    private String telephone;

    @NotEmpty
    @Column(nullable = false)
    private String variables;

    @NotEmpty
    @Size(min = 4, max = 254)
    @Column(nullable = false)
    private String sites;

    @NotEmpty
    @Size(min = 4, max = 254)
    @Column(nullable = false)
    private String dates;

    @Column
    private LocalDateTime dateRequest=LocalDateTime.now(DateUtil.getLocaleZoneId());

    /**
     *
     */
    public RightRequestGLACPE() {
        super();
    }

    /**
     *
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     */
    public String getVariables() {
        return variables;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(String variables) {
        this.variables = variables;
    }

    /**
     *
     * @return
     */
    public String getSites() {
        return sites;
    }

    /**
     *
     * @param sites
     */
    public void setSites(String sites) {
        this.sites = sites;
    }

    /**
     *
     * @return
     */
    public String getDates() {
        return dates;
    }

    /**
     *
     * @param dates
     */
    public void setDates(String dates) {
        this.dates = dates;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getDateRequest() {
        return dateRequest;
    }

    /**
     *
     * @param dateRequest
     */
    public void setDateRequest(LocalDateTime dateRequest) {
        this.dateRequest = dateRequest;
    }

    /**
     * 
     * 
     * PROPERTY_MSG_REQUEST_TO_STRING=<table><tr><td>e-mail</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Institution / Organisme / Autre</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Fonction (chercheur, professeur, doctorant, étudiant, autre)</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Encadrant</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Adresse professionnelle</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Téléphone</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Objets de la demande</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Collaboration avec un scientifique de l'observatoire OLA</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Variables demandées</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Sites demandés</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Périodes demandées</td><td style="font-weight:bold">%s</td></tr>\
     * <tr><td>Date de la demande</td><td style="font-weight:bold">%s</td></tr></table>
     * 
     * 
     * @return 
     */
    @Override
    public String toString() {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_SOURCE_PATH, user.getLanguage() == null ? new Locale(Localization.getDefaultLocalisation()) : new Locale(user.getLanguage()));
        String message = resourceBundle.getString(PROPERTY_MSG_REQUEST_TO_STRING);
        message = String.format(message,
                user.getEmail(),
                user.getEmploi(), 
                user.getPoste(), 
                getMotivation(), 
                getInfos(), 
                getTelephone(), 
                getRequest(), 
                getReference(), 
                getSites(),
                getVariables(), 
                getDates(), 
                DateUtil.getUTCDateTextFromLocalDateTime(getDateRequest(), DateUtil.DD_MM_YYYY));
        return message;

    }
}

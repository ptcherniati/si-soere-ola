/**
 * OREILacs project - see LICENCE.txt for use created: 7 janv. 2010 09:54:40
 */
package org.inra.ecoinfo.glacpe.extraction.vo;

import com.google.common.base.Strings;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class DatesRequestParamVO implements Cloneable {

    private DatesYearsDiscretsFormParamVO datesYearsDiscretsFormParam;
    private DatesYearsRangeFormParamVO datesYearsRangeFormParam;
    private DatesYearsContinuousFormParamVO datesYearsContinuousFormParam;

    private List<Map<String, String>> nonPeriodsYearsContinuous;

    private HelperForm helperForm = new HelperForm();

    private String selectedFormSelection;

    /**
     *
     */
    public DatesRequestParamVO() {
        super();

        setDatesYearsDiscretsFormParam(new DatesYearsDiscretsFormParamVO());
        getDatesYearsDiscretsFormParam().setPeriods(new LinkedList<Map<String, String>>());
        getDatesYearsDiscretsFormParam().getPeriods().add(buildNewMapPeriod());

        setDatesYearsRangeFormParam(new DatesYearsRangeFormParamVO());
        getDatesYearsRangeFormParam().setPeriods(new LinkedList<Map<String, String>>());
        getDatesYearsRangeFormParam().getPeriods().add(buildNewMapPeriod());
        getDatesYearsRangeFormParam().setYears(new LinkedList<Map<String, String>>());
        getDatesYearsRangeFormParam().getYears().add(buildNewMapPeriod());

        setDatesYearsContinuousFormParam(new DatesYearsContinuousFormParamVO());
        getDatesYearsContinuousFormParam().setPeriods(new LinkedList<Map<String, String>>());
        getDatesYearsContinuousFormParam().getPeriods().add(buildNewMapPeriod());
    }

    /**
     *
     * @param ilm
     * @param localizationManager
     */
    public DatesRequestParamVO(ILocalizationManager localizationManager) {
        super();

        setDatesYearsDiscretsFormParam(new DatesYearsDiscretsFormParamVO(localizationManager));
        getDatesYearsDiscretsFormParam().setPeriods(new LinkedList<Map<String, String>>());
        getDatesYearsDiscretsFormParam().getPeriods().add(buildNewMapPeriod());

        setDatesYearsRangeFormParam(new DatesYearsRangeFormParamVO(localizationManager));
        getDatesYearsRangeFormParam().setPeriods(new LinkedList<Map<String, String>>());
        getDatesYearsRangeFormParam().getPeriods().add(buildNewMapPeriod());
        getDatesYearsRangeFormParam().setYears(new LinkedList<Map<String, String>>());
        getDatesYearsRangeFormParam().getYears().add(buildNewMapPeriod());

        setDatesYearsContinuousFormParam(new DatesYearsContinuousFormParamVO(localizationManager));
        getDatesYearsContinuousFormParam().setPeriods(new LinkedList<Map<String, String>>());
        getDatesYearsContinuousFormParam().getPeriods().add(buildNewMapPeriod());
    }
    /**
     *
     * @return
     */
    public HelperForm getHelperForm() {
        return helperForm;
    }
    /**
     *
     * @param helperForm
     */
    public void setHelperForm(HelperForm helperForm) {
        this.helperForm = helperForm;
    }

    /**
     *
     * @return
     */
    public AbstractDatesFormParam getCurrentDatesFormParam() {
        if (getSelectedFormSelection() == null) {
            return null;
        }
        if (getSelectedFormSelection().equals(DatesYearsDiscretsFormParamVO.LABEL)) {
            return datesYearsDiscretsFormParam;
        } else if (getSelectedFormSelection().equals(DatesYearsRangeFormParamVO.LABEL)) {
            return datesYearsRangeFormParam;
        } else if (getSelectedFormSelection().equals(DatesYearsContinuousFormParamVO.LABEL)) {
            return datesYearsContinuousFormParam;
        }
        return null;
    }


    /**
     *
     * @return
     */
    public String getSelectedFormSelection() {
        return selectedFormSelection;
    }

    /**
     *
     * @param selectedFormSelection
     */
    public void setSelectedFormSelection(String selectedFormSelection) {
        this.selectedFormSelection = selectedFormSelection;
    }

    /**
     *
     * @return
     */
    public DatesYearsDiscretsFormParamVO getDatesYearsDiscretsFormParam() {
        return datesYearsDiscretsFormParam;
    }

    /**
     *
     * @param datesYearsDiscretsFormParam
     */
    public void setDatesYearsDiscretsFormParam(DatesYearsDiscretsFormParamVO datesYearsDiscretsFormParam) {
        this.datesYearsDiscretsFormParam = datesYearsDiscretsFormParam;
    }

    /**
     *
     * @return
     */
    public DatesYearsRangeFormParamVO getDatesYearsRangeFormParam() {
        return datesYearsRangeFormParam;
    }

    /**
     *
     * @param datesYearsRangeFormParam
     */
    public void setDatesYearsRangeFormParam(DatesYearsRangeFormParamVO datesYearsRangeFormParam) {
        this.datesYearsRangeFormParam = datesYearsRangeFormParam;
    }

    /**
     *
     * @return
     */
    public DatesYearsContinuousFormParamVO getDatesYearsContinuousFormParam() {
        return datesYearsContinuousFormParam;
    }

    /**
     *
     * @param datesYearsContinuousFormParam
     */
    public void setDatesYearsContinuousFormParam(DatesYearsContinuousFormParamVO datesYearsContinuousFormParam) {
        this.datesYearsContinuousFormParam = datesYearsContinuousFormParam;
    }

    /**
     *
     * @return
     */
    public List<Map<String, String>> getNonPeriodsYearsContinuous() {
        if (nonPeriodsYearsContinuous == null) {
            setNonPeriodsYearsContinuous(new LinkedList<Map<String, String>>());
            getNonPeriodsYearsContinuous().add(buildNewMapPeriod());
        }
        return nonPeriodsYearsContinuous;
    }

    /**
     *
     * @param nonPeriodsYearsContinuous
     */
    public void setNonPeriodsYearsContinuous(List<Map<String, String>> nonPeriodsYearsContinuous) {
        this.nonPeriodsYearsContinuous = nonPeriodsYearsContinuous;
        if (this.getCurrentDatesFormParam() != null) {
            this.getCurrentDatesFormParam().setPeriods(nonPeriodsYearsContinuous);
        }
    }

    /**
     *
     * @param size
     * @return
     */
    public String addPeriodYearsContinuous(long size) {
        if (getDatesYearsContinuousFormParam().getPeriods().size() == size) {
            getDatesYearsContinuousFormParam().getPeriods().add(buildNewMapPeriod());
        }
        return null;
    }

    /**
     *
     * @param size
     * @return
     */
    public String removePeriodYearsContinuous(long size) {
        if (getDatesYearsContinuousFormParam().getPeriods().size() <= 1 || getDatesYearsContinuousFormParam().getPeriods().size() != size) {
            return null;
        }
        List<Map<String, String>> periods = getDatesYearsContinuousFormParam().getPeriods();
        periods = periods.subList(0, periods.size() - 1);
        getDatesYearsContinuousFormParam().setPeriods(periods);
        return null;
    }

    /**
     *
     * @param size
     * @return
     */
    public String addNonPeriodsYearsContinuous(long size) {
        if (getNonPeriodsYearsContinuous().size() == size) {
            getNonPeriodsYearsContinuous().add(buildNewMapPeriod());
        }
        return null;
    }

    /**
     *
     * @param size
     * @return
     */
    public String removeNonPeriodsYearsContinuous(long size) {
        if (getNonPeriodsYearsContinuous().size() <= 1 || getNonPeriodsYearsContinuous().size() != size) {
            return null;
        }
        List<Map<String, String>> years = getNonPeriodsYearsContinuous();
        years = years.subList(0, years.size() - 1);
        setNonPeriodsYearsContinuous(years);
        return null;
    }

    /**
     *
     * @param size
     * @return
     */
    public String addPeriodsYearsRange(long size) {
        if (getDatesYearsRangeFormParam().getYears().size() == size) {
            getDatesYearsRangeFormParam().getYears().add(buildNewMapPeriod());
        }
        return null;
    }

    /**
     *
     * @param size
     * @return
     */
    public String removePeriodsYearsRange(long size) {
        if (getDatesYearsRangeFormParam().getYears().size() <= 1 || getDatesYearsRangeFormParam().getYears().size()!=size) {
            return null;
        }
        List<Map<String, String>> years = getDatesYearsRangeFormParam().getYears();
        years = years.subList(0, years.size() - 1);
        getDatesYearsRangeFormParam().setYears(years);
        return null;
    }

    /**
     *
     * @return
     */
    protected Map<String, String> buildNewMapPeriod() {
        Map<String, String> firstMap = new HashMap<String, String>();
        firstMap.put(DatesYearsDiscretsFormParamVO.START_INDEX, "");
        firstMap.put(DatesYearsDiscretsFormParamVO.END_INDEX, "");
        return firstMap;
    }

    /**
     *
     * @return
     */
    public String updateChoiceDate() {
        this.selectedFormSelection = helperForm.init().selectedFormSelection;
        return null;
    }

    /**
     *
     * @return
     */
    public String updateDateYearContinuous() {
        helperForm.init();
        String key = helperForm.getKey();
        Integer index = helperForm.getIndex();
        String value = helperForm.getValue();

        getDatesYearsContinuousFormParam().getPeriods().get(index).put(key, value);
        return null;
    }

    /**
     *
     * @return
     */
    public String updateNonPeriodsYearsContinuous() {
        helperForm.init();
        String key = helperForm.getKey();
        Integer index = helperForm.getIndex();
        String value = helperForm.getValue();

        getNonPeriodsYearsContinuous().get(index).put(key, value);
        getDatesYearsDiscretsFormParam().setPeriods(getNonPeriodsYearsContinuous());
        getDatesYearsRangeFormParam().setPeriods(getNonPeriodsYearsContinuous());
        return null;
    }

    /**
     *
     * @return
     */
    public String updateDatesYearsRange() {
        helperForm.init();
        String key = helperForm.getKey();
        Integer index = helperForm.getIndex();
        String value = helperForm.getValue();

        getDatesYearsRangeFormParam().getYears().get(index).put(key, value);
        getDatesYearsRangeFormParam().setPeriods(getNonPeriodsYearsContinuous());
        return null;
    }

    /**
     *
     * @return
     */
    public DatesRequestParamVO clone() {
        DatesRequestParamVO dateForm = new DatesRequestParamVO();
        dateForm.setDatesYearsContinuousFormParam(datesYearsContinuousFormParam.clone());
        dateForm.setDatesYearsDiscretsFormParam(datesYearsDiscretsFormParam.clone());
        dateForm.setDatesYearsRangeFormParam(datesYearsRangeFormParam.clone());
        List<Map<String, String>> periodMap = new LinkedList<Map<String, String>>();
        for (Map<String, String> period : this.nonPeriodsYearsContinuous) {
            Map<String, String> periodIndex = new HashMap<String, String>();
            for (String key : period.keySet()) {
                periodIndex.put(key, period.get(key));
            }
            periodMap.add(periodIndex);
        }
        dateForm.setNonPeriodsYearsContinuous(periodMap);
        dateForm.setSelectedFormSelection(new String(this.selectedFormSelection));
        return dateForm;
    }

    /**
     *
     */
    public class HelperForm {

        private static final String INDEX = "index";
        private static final String KEY = "key";
        private static final String VALUE = "value";
        private static final String SELECTED_FORM_SELECTION = "selectedFormSelection";

        private String selectedFormSelection;
        private String key;
        private Integer index;
        private String value;

        /**
         *
         * @return
         */
        public String getKey() {
            return key;
        }

        /**
         *
         * @param key
         */
        public void setKey(String key) {
            this.key = key;
        }

        /**
         *
         * @return
         */
        public Integer getIndex() {
            return index;
        }

        /**
         *
         * @param index
         */
        public void setIndex(Integer index) {
            this.index = index;
        }

        /**
         *
         * @return
         */
        public String getValue() {
            return value;
        }

        /**
         *
         * @param value
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         *
         * @return
         */
        public String getSelectedFormSelection() {
            return selectedFormSelection;
        }

        /**
         *
         * @param selectedFormSelection
         */
        public void setSelectedFormSelection(String selectedFormSelection) {
            this.selectedFormSelection = selectedFormSelection;
        }

        private HelperForm init() {
            Map<String, String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.selectedFormSelection = requestParameterMap.get(SELECTED_FORM_SELECTION);
            this.key = requestParameterMap.get(KEY);
            this.index = Strings.isNullOrEmpty(requestParameterMap.get(INDEX)) ? 0 : Integer.parseInt(requestParameterMap.get(INDEX));
            this.value = requestParameterMap.get(VALUE);
            return this;
        }

    }
}

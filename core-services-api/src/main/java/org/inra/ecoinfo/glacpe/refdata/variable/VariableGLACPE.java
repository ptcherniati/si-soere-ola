package org.inra.ecoinfo.glacpe.refdata.variable;

import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;
import org.inra.ecoinfo.glacpe.refdata.variablenorme.VariableNorme;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@DiscriminatorValue("variable_glacpe")
@SqlResultSetMapping(name = Variable.RESULTSET_MAPPING_JPA, entities = {@EntityResult(entityClass = VariableGLACPE.class)})
@Table(name = VariableGLACPE.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = VariableGLACPE.ID_JPA)
public class VariableGLACPE extends Variable {

    private static final long serialVersionUID = 1L;
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";

    /**
     *
     */
    static public final String NAME_ENTITY_JPA = "variable_glacpe_varg";

    private String name;
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = GroupeVariable.ID_JPA, referencedColumnName = GroupeVariable.ID_JPA, nullable = true)
    private GroupeVariable groupe;

    @Column(name = "ordre_affichage_groupe")
    private Integer ordreAffichageGroupe;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = VariableNorme.ID_JPA, referencedColumnName = VariableNorme.ID_JPA, nullable = true)
    private VariableNorme variableNorme;
    

    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;
    /**
     *
     */
    public VariableGLACPE() {
        super();
    }
    /**
     *
     * @param l
     * @param id
     */
    public VariableGLACPE(Long id) {
        getId();
    }
    /**
     *
     * @param nom
     * @param definition
     * @param affichage
     * @param ordreAffichageGroupe
     * @param bln
     * @param isQualitative
     */
    public VariableGLACPE(String nom, String definition, String affichage, Integer ordreAffichageGroupe, Boolean isQualitative) {
        super(nom, definition, affichage, isQualitative) ;
        setName(nom);
        setOrdreAffichageGroupe(ordreAffichageGroupe);
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }
    
    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     * @return
     */
    public Variable cloneOnlyAttributes() {
        Variable variable = new Variable();
        variable.setCode(getCode());
        variable.setDefinition(getDefinition());
        variable.setId(getId());
        return variable;

    }
    


    /**
     *
     * @return
     */
    public GroupeVariable getGroupe() {
        return groupe;
    }

    /**
     *
     * @return
     */
    public Integer getOrdreAffichageGroupe() {
        return ordreAffichageGroupe;
    }

    /**
     *
     * @return
     */
    public VariableNorme getVariableNorme() {
        return variableNorme;
    }

    /**
     *
     * @param groupe
     */
    public void setGroupe(GroupeVariable groupe) {
        this.groupe = groupe;
    }

    /**
     *
     * @param ordreAffichageGroupe
     */
    public void setOrdreAffichageGroupe(Integer ordreAffichageGroupe) {
        this.ordreAffichageGroupe = ordreAffichageGroupe;
    }

    /**
     *
     * @param variableNorme
     */
    public void setVariableNorme(VariableNorme variableNorme) {
        this.variableNorme = variableNorme;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(INodeable o) {
        if( o==null){
            return 1;
        }
        if(getGroupe()!=null && getGroupe().compareTo(groupe)!=0){
            return getGroupe().compareTo(groupe);
        }
        return getCode().compareTo(((VariableGLACPE)o).getCode());
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 51 * hash + Objects.hashCode(this.getGroupe());
        hash = 511 * hash + Objects.hashCode(this.getCode());
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VariableGLACPE other = (VariableGLACPE) obj;
        if (!Objects.equals(this.groupe, other.groupe)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
        setCode(Utils.createCodeFromString(name));
    }

}

package org.inra.ecoinfo.glacpe.refdata.taxon;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.taxonniveau.TaxonNiveau;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Taxon.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {Taxon.ATTRIBUTE_NOM_LATIN_JPA, Taxon.PARENT_ID_JPA}))
public class Taxon implements Serializable, Comparable<Taxon> {
    
    
    //afiocca

    /**
     *
     */
    
    public static final String CODE_SANDRE_TAX = "code_sandre_tax";

    /**
     *
     */
    public static final String CODE_SANDRE_SUP = "code_sandre_sup";


    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "tax_id";

    /**
     *
     */
    public static final String PARENT_ID_JPA = "taxonparent_id";

    /**
     *
     */
    public static final String ATTRIBUTE_NOM_LATIN_JPA = "nomLatin";

    /**
     *
     */
    public static final String ATTRIBUTE_THEME = "theme";

    /**
     *
     */
    public static final String TABLE_NAME = "taxon_tax";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = PARENT_ID_JPA, referencedColumnName = ID_JPA, nullable = true)
    private Taxon taxonParent;

    @OneToMany(fetch = FetchType.EAGER, cascade = {MERGE, PERSIST, REFRESH}, mappedBy = "taxonParent")
    @OrderBy("nomLatin")
    private List<Taxon> taxonsEnfants = new LinkedList<Taxon>();

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = TaxonNiveau.ID_JPA, referencedColumnName = TaxonNiveau.ID_JPA, nullable = false)
    private TaxonNiveau taxonNiveau;

    @Column(nullable = false, unique = true)
    private String code;
    

    @Column(nullable = true,name = CODE_SANDRE_TAX, unique = false)
    private String codeSandreTax;


    
    @Column(nullable = true,name = CODE_SANDRE_SUP , unique = false)
    private String codeSandreSup;
    @Column(nullable = false, unique = true, name = ATTRIBUTE_NOM_LATIN_JPA)
    private String nomLatin;
    @OneToMany(cascade = ALL, mappedBy = "taxon")
    private List<TaxonProprieteTaxon> taxonProprieteTaxon;
    @OrderColumn(name = ATTRIBUTE_THEME, nullable = false)
    private String theme;
    @Transient
    private Boolean isLeaf;
    /**
     *
     */
    public Taxon() {
        super();
    }
    /**
     *
     * @param nomLatin
     * @param string1
     * @param theme
     */
    public Taxon(String nomLatin, String theme) {
        super();
        setNomLatin(nomLatin);
        this.theme = theme;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreTax() {
        return codeSandreTax;
    }

    /**
     *
     * @param codeSandreTax
     */
    public void setCodeSandreTax(String codeSandreTax) {
        this.codeSandreTax = codeSandreTax;
    }

    /**
     *
     * @return
     */
    public String getCodeSandreSup() {
        return codeSandreSup;
    }

    /**
     *
     * @param codeSandreSup
     */
    public void setCodeSandreSup(String codeSandreSup) {
        this.codeSandreSup = codeSandreSup;
    }



    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     *
     * @return
     */
    public Taxon getTaxonParent() {
        return taxonParent;
    }

    /**
     *
     * @param taxonParent
     */
    public void setTaxonParent(Taxon taxonParent) {
        this.taxonParent = taxonParent;
    }

    /**
     *
     * @return
     */
    public TaxonNiveau getTaxonNiveau() {
        return taxonNiveau;
    }

    /**
     *
     * @param taxonNiveau
     */
    public void setTaxonNiveau(TaxonNiveau taxonNiveau) {
        this.taxonNiveau = taxonNiveau;
    }

    /**
     *
     * @param taxonsEnfants
     */
    public void setTaxonsEnfants(List<Taxon> taxonsEnfants) {
        this.taxonsEnfants = taxonsEnfants;
    }

    /**
     *
     * @return
     */
    public List<Taxon> getTaxonsEnfants() {
        return taxonsEnfants;
    }

    /**
     *
     * @return
     */
    public String getNomLatin() {
        return nomLatin;
    }

    /**
     *
     * @return
     */
    public String getTheme() {
        return theme;
    }


    /**
     *
     * @param nomLatin
     */
    public void setNomLatin(String nomLatin) {
        this.nomLatin = nomLatin;
        this.code = Utils.createCodeFromString(nomLatin);
    }

    /**
     *
     * @param theme
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     *
     * @return
     */
    public List<TaxonProprieteTaxon> getTaxonProprieteTaxon() {
        return taxonProprieteTaxon;
    }

    /**
     *
     * @param taxonProprieteTaxon
     */
    public void setTaxonProprieteTaxon(List<TaxonProprieteTaxon> taxonProprieteTaxon) {
        this.taxonProprieteTaxon = taxonProprieteTaxon;
    }

    /**
     *
     * @param taxon
     * @return
     */
    public boolean isParentOf(Taxon taxon) {

        if (taxon.getTaxonParent() == null) {
            return false;
        }

        if (taxon.getTaxonParent().getNomLatin().equals(this.getNomLatin())) {
            return true;
        }

        return this.isParentOf(taxon.getTaxonParent());

    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Taxon o) {
        return this.getNomLatin().compareTo(o.getNomLatin());
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    /**
     *
     * @return
     */
    public boolean isLeaf() {
        if (this.isLeaf == null) {
            isLeaf = getTaxonsEnfants().isEmpty();
        }
        return this.isLeaf;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Taxon other = (Taxon) obj;
        if (!Objects.equals(this.nomLatin, other.nomLatin)) {
            return false;
        }
        if (!Objects.equals(this.taxonParent, other.taxonParent)) {
            return false;
        }
        return true;
    }

    /**
     *
     */
    public static final class VALUES_ATTR_THEME {
        
        /**
         *
         */
        public static final String PHYTOPLANCTON = "Phytoplancton";
        
        /**
         *
         */
        public static final String ZOOPLANCTON = "Zooplancton";
    }

}

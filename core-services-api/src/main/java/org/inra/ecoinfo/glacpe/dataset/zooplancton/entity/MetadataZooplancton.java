package org.inra.ecoinfo.glacpe.dataset.zooplancton.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;

/**
 * @author Antoine Schellenberger
 */
@Entity
@Table(
        name = MetadataZooplancton.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(
                columnNames = {
                    Projet.ID_JPA, 
                    MetadataZooplancton.ATTR_JPA_DATE, 
                    Plateforme.ID_JPA, 
                    MetadataZooplancton.ATTR_JPA_PRELEVEMENT_TOOL}))
public class MetadataZooplancton implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "metadata_zooplancton_mtdz";

    /**
     *
     */
    static public final String ID_JPA = "mtdz_id";

    /**
     *
     */
    public static final String ATTR_JPA_DATE = "date";

    /**
     *
     */
    public static final String ATTR_JPA_NAME_DETERMINATOR = "nom_determinateur";

    /**
     *
     */
    public static final String ATTR_JPA_SEDIMENTED_VOLUME = "volume_sedimente";

    /**
     *
     */
    public static final String ATTR_JPA_PROFONDEUR_MIN = "profondeur_min";

    /**
     *
     */
    public static final String ATTR_JPA_PROFONDEUR_MAX = "profondeur_max";

    /**
     *
     */
    public static final String ATTR_JPA_PRELEVEMENT_TOOL = "outils_prelevement";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = ATTR_JPA_DATE, nullable = false)
    private LocalDate date;

    @Column(name = ATTR_JPA_NAME_DETERMINATOR, nullable = true)
    private String nomDeterminateur;

    @Column(name = ATTR_JPA_SEDIMENTED_VOLUME, nullable = true)
    private Float volumeSedimente;

    @Column(name = ATTR_JPA_PROFONDEUR_MIN, nullable = false)
    private Float profondeurMin;

    @Column(name = ATTR_JPA_PROFONDEUR_MAX, nullable = false)
    private Float profondeurMax;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Projet.ID_JPA, referencedColumnName = Projet.ID_JPA, nullable = false)
    private Projet projet;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = OutilsMesure.ID_JPA, referencedColumnName = OutilsMesure.ID_JPA, nullable = false)
    private OutilsMesure outilsMesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ATTR_JPA_PRELEVEMENT_TOOL, referencedColumnName = OutilsMesure.ID_JPA, nullable = false)
    private OutilsMesure outilsPrelevement;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @OneToMany(mappedBy = "metadataZooplancton", cascade = ALL)
    private List<DataZooplancton> datas = new LinkedList<DataZooplancton>();

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getNomDeterminateur() {
        return nomDeterminateur;
    }

    /**
     *
     * @param nomDeterminateur
     */
    public void setNomDeterminateur(String nomDeterminateur) {
        this.nomDeterminateur = nomDeterminateur;
    }

    /**
     *
     * @return
     */
    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    /**
     *
     * @param volumeSedimente
     */
    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsPrelevement() {
        return outilsPrelevement;
    }

    /**
     *
     * @param outilsPrelevement
     */
    public void setOutilsPrelevement(OutilsMesure outilsPrelevement) {
        this.outilsPrelevement = outilsPrelevement;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public List<DataZooplancton> getDatas() {
        return datas;
    }

    /**
     *
     * @param datas
     */
    public void setDatas(List<DataZooplancton> datas) {
        this.datas = datas;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMin() {
        return profondeurMin;
    }

    /**
     *
     * @return
     */
    public Float getProfondeurMax() {
        return profondeurMax;
    }

    /**
     *
     * @param profondeurMin
     */
    public void setProfondeurMin(Float profondeurMin) {
        this.profondeurMin = profondeurMin;
    }

    /**
     *
     * @param profondeurMax
     */
    public void setProfondeurMax(Float profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

}

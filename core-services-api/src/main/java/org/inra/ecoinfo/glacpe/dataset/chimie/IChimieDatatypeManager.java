package org.inra.ecoinfo.glacpe.dataset.chimie;

import org.inra.ecoinfo.glacpe.extraction.jsf.IDepthManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;

/**
 *
 * @author ptcherniati
 */
public interface IChimieDatatypeManager extends IPlatformManager, IVariableManager, IDepthManager{

    /**
     *
     */
    public static final String CODE_DATATYPE_CHIMIE = "physico_chimie";


}

package org.inra.ecoinfo.glacpe.refdata.datatypevariableunite;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlElement;
import org.inra.ecoinfo.glacpe.refdata.controlecoherence.ControleCoherence;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = DatatypeVariableUniteGLACPE.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {Variable.ID_JPA, DataType.ID_JPA, Unite.ID_JPA}),
        indexes = {
                @Index(name = "dvu_uni_idx", columnList = Unite.ID_JPA)
                ,
                @Index(name = "dvu_var_idx", columnList = Variable.ID_JPA)
                ,
                @Index(name = "dvu_dty_idx", columnList = DataType.ID_JPA)})
@PrimaryKeyJoinColumn(name = DatatypeVariableUniteGLACPE.ID_JPA)
public class DatatypeVariableUniteGLACPE extends Nodeable implements Serializable, Comparable<INodeable> {

    /**
     *
     */
    public static final String ID_JPA = "vdt_id";


    /**
     *
     */
    public static final String DISCRIMINATOR = "DatatypeVariableUnite";
    private static final long serialVersionUID = 1L;
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";

    /**
     *
     */
    public static final String TABLE_NAME = "datatype_variable_unite_glacpe_dvug";


    @OneToMany(mappedBy = "datatypeVariableUnite", cascade = {MERGE, PERSIST, REFRESH})
    @MapKey(name = "site")
    private Map<SiteGLACPE, ControleCoherence> controleCoherence = new HashMap<SiteGLACPE, ControleCoherence>();
   
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE, unique = false)
    private String contexte;
    /**
     * The datatype @link(DataType).
     */
    @XmlElement(name = "datatype")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = DataType.ID_JPA, referencedColumnName = DataType.ID_JPA, nullable = false)
    private DataType datatype;
    /**
     * The unite @link(Unite).
     */
    @XmlElement(name = "unite")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Unite.ID_JPA, referencedColumnName = Unite.ID_JPA, nullable = false)
    private Unite unite;
    /**
     * The variable @link(Variable).
     */
    @XmlElement(name = "variable")
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = Variable.ID_JPA, referencedColumnName = Variable.ID_JPA, nullable = false)
    private VariableGLACPE variable;
    /**
     *
     */
    public DatatypeVariableUniteGLACPE() {
        super();
    }
    /**
     *
     * @param dbDatatype
     * @param dbUnite
     * @param vglcp
     * @param dbVariable
     */
    public DatatypeVariableUniteGLACPE(final DataType datatype, final Unite unite,
            final VariableGLACPE variable) {
        super();
        this.datatype = datatype;
        this.unite = unite;
        this.variable = variable;
        setCode(String.format("%s_%s_%s", datatype.getCode(), variable.getCode(), unite.getCode()));
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }
    
    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(INodeable o) {
        if (o == null || !(o instanceof DatatypeVariableUniteGLACPE)) {
            return -1;
        }
        DatatypeVariableUniteGLACPE oo = (DatatypeVariableUniteGLACPE) o;
        if (this.getDatatype().compareTo(oo.getDatatype()) != 0) {
            return this.getDatatype().compareTo(oo.getDatatype());
        }
        if (this.getVariable().compareTo(oo.getVariable()) != 0) {
            return this.getVariable().compareTo(oo.getVariable());
        }
        return this.getUnite().getCode().compareTo(oo.getUnite().getCode());
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DatatypeVariableUniteGLACPE other = (DatatypeVariableUniteGLACPE) obj;
        return (Objects.equals(this.getDatatype(), other.getDatatype())
                && Objects.equals(this.getVariable(), other.getVariable())
                && Objects.equals(this.getUnite().getCode(), other.getUnite().getCode()));
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.getDatatype());
        hash = 311 * hash + Objects.hashCode(this.getVariable());
        hash = 3_111 * hash + Objects.hashCode(this.getUnite().getCode());
        return hash;
    }

    /**
     * Gets the datatype.
     *
     * @return the datatype
     */
    public DataType getDatatype() {
        return datatype;
    }

    /**
     * Sets the datatype.
     *
     * @param datatype the new datatype
     */
    public void setDatatype(final DataType datatype) {
        this.datatype = datatype;
    }

    /**
     * Gets the unite.
     *
     * @return the unite
     */
    public Unite getUnite() {
        return unite;
    }

    /**
     * Sets the unite.
     *
     * @param unite the new unite
     */
    public void setUnite(final Unite unite) {
        this.unite = unite;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public VariableGLACPE getVariable() {
        return variable;
    }

    /**
     * Sets the variable.
     *
     * @param variable the new variable
     */
    public void setVariable(final VariableGLACPE variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public Map<SiteGLACPE, ControleCoherence> getControleCoherence() {
        return controleCoherence;
    }

    /**
     *
     * @param controleCoherence
     */
    public void setControleCoherence(Map<SiteGLACPE, ControleCoherence> controleCoherence) {
        this.controleCoherence = controleCoherence;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) DatatypeVariableUniteGLACPE.class;
    }

    /**
     * @return
     */
    @Override
    public String getName() {
        return this.variable.getName();
    }

}

package org.inra.ecoinfo.glacpe.dataset.contenustomacaux;

import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;

/**
 *
 * @author ptcherniati
 */
public interface IContenuStomacauxDatatypeManager extends IPlatformManager, IVariableManager {

    /**
     *
     */
    public static final String CODE_DATATYPE_CONTENU_STOMACAUX = "contenus_stomacaux";


}

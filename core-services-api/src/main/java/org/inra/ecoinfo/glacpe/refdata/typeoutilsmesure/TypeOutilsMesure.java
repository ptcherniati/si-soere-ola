package org.inra.ecoinfo.glacpe.refdata.typeoutilsmesure;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeOutilsMesure.TABLE_NAME)
public class TypeOutilsMesure implements Serializable, Comparable<TypeOutilsMesure> {
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "tome_id";

    /**
     *
     */
    public static final String TABLE_NAME = "type_outils_mesure_tome";

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @OneToMany(mappedBy = "typeOutilsMesure", cascade = {MERGE, PERSIST, REFRESH})
    private List<OutilsMesure> outilsMesures = new LinkedList<OutilsMesure>();

    @Column(nullable = false, unique = true)
    private String nom;

    @Column(nullable = false, unique = true)
    private String code;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ValeurQualitative.ID_JPA, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative type;
    
    
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;
    /**
     *
     */
    public TypeOutilsMesure() {
        super();
    }
    /**
     *
     * @param nom
     * @param vq
     * @param type
     */
    public TypeOutilsMesure(String nom, ValeurQualitative type) {
        super();
        setNom(nom);
        this.type = type;
    }
    /**
     *
     * @param string
     * @param nom
     */
    public TypeOutilsMesure(String nom) {
        super();
        setNom(nom);
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }


    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public List<OutilsMesure> getOutilsMesures() {
        return outilsMesures;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @param outilsMesures
     */
    public void setOutilsMesures(List<OutilsMesure> outilsMesures) {
        this.outilsMesures = outilsMesures;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(ValeurQualitative type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.code);
        hash = 79 * hash + Objects.hashCode(this.type);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TypeOutilsMesure other = (TypeOutilsMesure) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(TypeOutilsMesure t) {
        if(type.getValeur().compareTo(t.type.getValeur())!=0){
            return type.getValeur().compareTo(t.type.getValeur());
        }
        return getCode().compareTo(t.getCode());
    }

}

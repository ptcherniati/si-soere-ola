package org.inra.ecoinfo.glacpe.refdata.projet;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;


/**
 * Représente un theme scientifique
 * 
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = Projet.NAME_TABLE)
@PrimaryKeyJoinColumn(name = Projet.ID_JPA)
public class Projet  extends Nodeable implements Comparable<INodeable>, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "pro_id";

    /**
     *
     */
    public static final String NAME_TABLE = "projet_pro";

    /**
     *
     */
    public static final String FIELD_ENTITY_NAME = "nom";

    /**
     *
     */
    static public final String FIELD_ENTITY_DESCRIPTION = "description_projet";
    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";
    
    private String name;

    @Column(name = FIELD_ENTITY_DESCRIPTION, columnDefinition = "TEXT")
    private String descriptionProjet;
    
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;

    /**
     *
     */
    public Projet() {}

    /**
     *
     * @return
     */
    public String getDescriptionProjet() {
        return descriptionProjet;
    }

    /**
     *
     * @param descriptionProjet
     */
    public void setDescriptionProjet(String descriptionProjet) {
        this.descriptionProjet = descriptionProjet;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.name = nom;
        this.code = Utils.createCodeFromString(nom);
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Projet.class;
    }

}

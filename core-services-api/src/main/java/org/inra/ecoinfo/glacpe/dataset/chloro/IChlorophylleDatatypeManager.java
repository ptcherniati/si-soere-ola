package org.inra.ecoinfo.glacpe.dataset.chloro;

import org.inra.ecoinfo.glacpe.extraction.jsf.IDepthManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;


/**
 *
 * @author ptcherniati
 */
public interface IChlorophylleDatatypeManager extends IPlatformManager, IVariableManager, IDepthManager{
    
}

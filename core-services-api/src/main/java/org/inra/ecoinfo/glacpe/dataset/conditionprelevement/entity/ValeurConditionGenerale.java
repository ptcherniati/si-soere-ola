package org.inra.ecoinfo.glacpe.dataset.conditionprelevement.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = ValeurConditionGenerale.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA, MesureConditionGenerale.ID_JPA}),
        indexes = {
            @Index(name = MesureConditionGenerale.ID_JPA, columnList = MesureConditionGenerale.ID_JPA),
            @Index(name = ValeurConditionGenerale.INDEX_ATTRIBUTS_VAR, columnList = RealNode.ID_JPA)
        })
public class ValeurConditionGenerale implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vconditionprelevement_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_condition_prelevement_vmconditionprelevement";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "var_conditions_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureConditionGenerale.ID_JPA, referencedColumnName = MesureConditionGenerale.ID_JPA, nullable = false)
    private MesureConditionGenerale mesure;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;

    @Column(nullable = true)
    private Float valeur;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = ValeurQualitative.ID_JPA, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative valeurQualitative;
    /**
     *
     */
    public ValeurConditionGenerale() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public MesureConditionGenerale getMesure() {
        return mesure;
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureConditionGenerale mesure) {
        this.mesure = mesure;
    }


    /**
     *
     * @return
     */
    public ValeurQualitative getValeurQualitative() {
        return valeurQualitative;
    }

    /**
     *
     * @param valeurQualitative
     */
    public void setValeurQualitative(ValeurQualitative valeurQualitative) {
        this.valeurQualitative = valeurQualitative;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }
}

package org.inra.ecoinfo.glacpe.dataset.contenustomacaux.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;


/**
 * @author "Guillaume Enrico"
 * 
 */

@Entity
@Table(name = ValeurMesureContenuStomacaux.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {ValeurMesureContenuStomacaux.PROIE, MesureContenuStomacaux.ID_JPA}),
        indexes = {
            @Index(name = MesureContenuStomacaux.ID_JPA, columnList = MesureContenuStomacaux.ID_JPA),
            @Index(name = ValeurMesureContenuStomacaux.INDEX_ATTRIBUTS_VAR,columnList = ValeurMesureContenuStomacaux.PROIE),
            @Index(name = RealNode.ID_JPA,columnList = RealNode.ID_JPA)
        })
public class ValeurMesureContenuStomacaux implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "valeur_mesure_contenus_stomacaux_vmcontenustomacaux";

    /**
     *
     */
    static public final String ID_JPA = "vmcontenustomacaux_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_VAR = "vq_contenus_ikey";

    /**
     *
     */
    static public final String PROIE = "proie";


    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = MesureContenuStomacaux.ID_JPA, referencedColumnName = MesureContenuStomacaux.ID_JPA, nullable = false)
    private MesureContenuStomacaux mesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = PROIE, referencedColumnName = ValeurQualitative.ID_JPA, nullable = true)
    private ValeurQualitative proie;


    /**
     * The datatype variable unite @link(RealNode).
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    RealNode realNode;


    @Column(nullable = true)
    private Float valeur;
    /**
     *
     */
    public ValeurMesureContenuStomacaux() {
        super();
    }

    /**
     *
     * @return
     */
    public MesureContenuStomacaux getMesure() {
        return mesure;
    }

    /**
     *
     * @param mesure
     */
    public void setMesure(MesureContenuStomacaux mesure) {
        this.mesure = mesure;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public ValeurQualitative getProie() {
        return proie;
    }

    /**
     *
     * @param proie
     */
    public void setProie(ValeurQualitative proie) {
        this.proie = proie;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }
}

package org.inra.ecoinfo.glacpe.dataset.hautefrequence;

import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;


/**
 *
 * @author mylene
 */
public interface IHauteFrequenceDatatypeManager extends  IPlatformManager, IVariableManager{
    
    /**
     *
     */
    public static final String CODE_DATATYPE_HAUTE_FREQUENCE = "haute_frequence";
    
}

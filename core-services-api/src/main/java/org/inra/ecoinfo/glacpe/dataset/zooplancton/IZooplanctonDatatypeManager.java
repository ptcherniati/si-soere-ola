package org.inra.ecoinfo.glacpe.dataset.zooplancton;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.glacpe.extraction.jsf.IPlatformManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.glacpe.extraction.jsf.IZooTaxonManager;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.glacpe.refdata.taxon.TaxonProprieteTaxon;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public interface IZooplanctonDatatypeManager extends IPlatformManager, IVariableManager, IZooTaxonManager {

    /**
     *
     */
    public static final String CODE_DATATYPE = "zooplancton";

    /**
     *
     * @return @throws BusinessException
     */
    List<Taxon> retrieveRootTaxon() throws BusinessException;

    /**
     *
     * @param taxonId
     * @return
     */
    List<TaxonProprieteTaxon> retrieveTaxonPropertyByTaxonId(long taxonId);

    /**
     *
     * @param taxonCode
     * @return
     */
    Optional<Taxon> retrieveTaxonByCode(String taxonCode);
}

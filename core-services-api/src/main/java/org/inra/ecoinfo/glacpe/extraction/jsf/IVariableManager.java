/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.extraction.jsf;


import java.util.Collection;
import java.util.List;
import org.inra.ecoinfo.glacpe.extraction.vo.PlateformeProjetVO;
import org.inra.ecoinfo.glacpe.refdata.datatypevariableunite.DatatypeVariableUniteGLACPE;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 * @author tcherniatinsky
 */
public interface IVariableManager {

    /**
     * @param intervalsDate
     * @param selectedDates
     * @return
     */
    Collection<DatatypeVariableUniteGLACPE> getAvailableVariables(List<PlateformeProjetVO> selectedPlatforms, List<IntervalDate> selectedDates);

}

package org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;


/**
 * @author "Guillaume Enrico"
 * 
 */
@Entity
@Table(name = SequencePhytoplancton.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {VersionFile.ID_JPA, "date_prelevement"}),
        indexes = {
            @Index(name = SequencePhytoplancton.INDEX_ATTRIBUTS_IVF, columnList = VersionFile.ID_JPA)
        }
        )
public class SequencePhytoplancton implements Serializable, Comparable<SequencePhytoplancton> {


    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sequence_phytoplancton_sphytoplancton";

    /**
     *
     */
    static public final String ID_JPA = "sphytoplancton_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_DATE = "date_phyto_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PSI = "psi_phyto_ikey";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_IVF = "ivf_phyto_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "date_prelevement", nullable = true)
    private LocalDate datePrelevement;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    private VersionFile versionFile;

    @OneToMany(mappedBy = "sequence", cascade = ALL)
    private List<SousSequencePhytoplancton> sousSequences = new LinkedList<SousSequencePhytoplancton>();

    @Column(name = "nom_determinateur", nullable = true)
    private String nomDeterminateur;

    @Column(name = "nom_sedimente", nullable = true)
    private Float volumeSedimente;
    /**
     *
     */
    public SequencePhytoplancton() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return datePrelevement;
    }

    /**
     *
     * @param datePrelevement
     */
    public void setDatePrelevement(LocalDate datePrelevement) {
        this.datePrelevement = datePrelevement;
    }

    /**
     *
     * @return
     */
    public List<SousSequencePhytoplancton> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequencePhytoplancton> sousSequences) {
        this.sousSequences = sousSequences;
    }

    /**
     *
     * @return
     */
    public String getNomDeterminateur() {
        return nomDeterminateur;
    }

    /**
     *
     * @param nomDeterminateur
     */
    public void setNomDeterminateur(String nomDeterminateur) {
        this.nomDeterminateur = nomDeterminateur;
    }

    /**
     *
     * @return
     */
    public Float getVolumeSedimente() {
        return volumeSedimente;
    }

    /**
     *
     * @param volumeSedimente
     */
    public void setVolumeSedimente(Float volumeSedimente) {
        this.volumeSedimente = volumeSedimente;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.datePrelevement);
        hash = 71 * hash + Objects.hashCode(this.versionFile);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SequencePhytoplancton other = (SequencePhytoplancton) obj;
        if (!Objects.equals(this.datePrelevement, other.datePrelevement)) {
            return false;
        }
        if (!Objects.equals(this.versionFile, other.versionFile)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(SequencePhytoplancton t) {
        if(t==null){
            return -1;
        }
        int compareToVersion = getVersionFile().compareTo(t.getVersionFile());
        if(compareToVersion!=0){
            return compareToVersion;
        }
        return getDatePrelevement().compareTo(t.getDatePrelevement());
    }

}
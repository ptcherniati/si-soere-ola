package org.inra.ecoinfo.glacpe.refdata.typeplateforme;

import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypePlateforme.TABLE_NAME)
@PrimaryKeyJoinColumn(name = TypePlateforme.ID_JPA)
public class TypePlateforme extends Nodeable {
    
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "tpla_id";

    /**
     *
     */
    public static final String TABLE_NAME = "type_plateforme_tpla";

    @OneToMany(mappedBy = "typePlateforme", cascade = {MERGE, PERSIST, REFRESH})
    private List<Plateforme> plateformes = new LinkedList<Plateforme>();
    private String name;
    
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;
    @Column(nullable = true,columnDefinition = "TEXT")
    
    private String description;
    /**
     *
     */
    public TypePlateforme() {
        super();
    }
    /**
     *
     * @param nom
     * @param string1
     * @param description
     */
    public TypePlateforme(String nom, String description) {
        super(Utils.createCodeFromString(nom));
        setName(nom);
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.code;
    }


    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public List<Plateforme> getPlateformes() {
        return plateformes;
    }

    /**
     *
     * @param plateformes
     */
    public void setPlateformes(List<Plateforme> plateformes) {
        this.plateformes = plateformes;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) TypePlateforme.class;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
        setCode(Utils.createCodeFromString(name));
    }

}

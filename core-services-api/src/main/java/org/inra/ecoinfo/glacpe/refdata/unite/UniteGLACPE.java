package org.inra.ecoinfo.glacpe.refdata.unite;

import javax.persistence.Column;
import org.inra.ecoinfo.refdata.unite.Unite;


/**
 *
 * @author afiocca
 */
/**
@Entity
@DiscriminatorValue("unite_glacpe")

@Table(name = UniteGLACPE.TABLE_NAME)
*/
public class UniteGLACPE extends Unite {
    
    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String TABLE_NAME = "unit_glacpe_unit";
    
    private static final long serialVersionUID = 1L;

    

    @Column(nullable = false,name = CODE_SANDRE, unique = false)
    private String codeSandre;

    /**
     *
     */
    public UniteGLACPE() {
        super();
    }

    /**
     *
     * @param code
     * @param nom
     */
    public UniteGLACPE(String code, String nom) {        
        super(code, nom);
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

}

package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.glacpe.refdata.groupevariable.GroupeVariable;

/**
 *
 * @author ptcherniati
 */
public class GroupeVariableVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nom;
    private String code;
    private List<VariableVO> variables = new LinkedList<VariableVO>();
    private List<GroupeVariableVO> children = new LinkedList<GroupeVariableVO>();
    private GroupeVariableVO parent;

    /**
     *
     */
    public GroupeVariableVO() {
        super();
    }

    /**
     *
     * @param gv
     * @param groupeVariable
     */
    public GroupeVariableVO(GroupeVariable groupeVariable) {
        this.id = groupeVariable.getId();
        this.nom = groupeVariable.getNom();
        this.code = groupeVariable.getCode();
    }

    /**
     *
     * @return
     */
    public List<VariableVO> getVariables() {
        return variables;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(List<VariableVO> variables) {
        this.variables = variables;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public List<GroupeVariableVO> getChildren() {
        return children;
    }

    /**
     *
     * @param children
     */
    public void setChildren(List<GroupeVariableVO> children) {
        this.children = children;
    }

    /**
     *
     * @return
     */
    public GroupeVariableVO getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    public void setParent(GroupeVariableVO parent) {
        this.parent = parent;
    }

}

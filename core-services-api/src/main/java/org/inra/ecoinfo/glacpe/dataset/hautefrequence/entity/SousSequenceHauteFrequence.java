package org.inra.ecoinfo.glacpe.dataset.hautefrequence.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.dimensions.Dimension;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;

/**
 *
 * @author mylene
 */
@Entity
@Table(name = SousSequenceHauteFrequence.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {SequenceHauteFrequence.ID_JPA, Plateforme.ID_JPA, OutilsMesure.ID_JPA, Dimension.ID_JPA}),
        indexes = {
            @Index(name = SousSequenceHauteFrequence.INDEX_ATTRIBUTS_PLATEFORME, columnList = Plateforme.ID_JPA),
            @Index(name = OutilsMesure.ID_JPA, columnList = OutilsMesure.ID_JPA),
            @Index(name = Dimension.ID_JPA, columnList = Dimension.ID_JPA),
            @Index(name = SequenceHauteFrequence.ID_JPA, columnList = SequenceHauteFrequence.ID_JPA)
        })
public class SousSequenceHauteFrequence implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "sous_sequence_haute_frequence_sshautefrequence";

    /**
     *
     */
    static public final String ID_JPA = "sshautefrequence_id";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_hautefrequence_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequenceHauteFrequence.ID_JPA, referencedColumnName = SequenceHauteFrequence.ID_JPA, nullable = false)
    private SequenceHauteFrequence sequence;

    @OneToMany(mappedBy = "sousSequence", cascade = ALL)
    private List<MesureHauteFrequence> mesures = new LinkedList<MesureHauteFrequence>();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = OutilsMesure.ID_JPA, referencedColumnName = OutilsMesure.ID_JPA, nullable = false)
    private OutilsMesure outilsMesure;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Dimension.ID_JPA, referencedColumnName = Dimension.ID_JPA, nullable = false)
    private Dimension dimension;

    @Column(name = "commentaire_hautefrequence", nullable = false)
    private String commentaireHauteFrequence;

    /**
     *
     */
    public SousSequenceHauteFrequence() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SequenceHauteFrequence getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     */
    public void setSequence(SequenceHauteFrequence sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     */
    public List<MesureHauteFrequence> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureHauteFrequence> mesures) {
        this.mesures = mesures;
    }

    /**
     *
     * @return
     */
    public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    /**
     *
     * @return
     */
    public OutilsMesure getOutilsMesure() {
        return outilsMesure;
    }

    /**
     *
     * @param outilsMesure
     */
    public void setOutilsMesure(OutilsMesure outilsMesure) {
        this.outilsMesure = outilsMesure;
    }

    /**
     *
     * @return
     */
    public Dimension getDimension() {
        return dimension;
    }

    /**
     *
     * @param dimension
     */
    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaireHauteFrequence;
    }

    /**
     *
     * @param commentaires
     */
    public void setCommentaires(String commentaires) {
        this.commentaireHauteFrequence = commentaires;
    }

}

/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 2010 09:14:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.plateforme.Plateforme;


/**
 * @author "Cédric Anache"
 * 
 */

@Entity
@Table(name = SousSequenceChloro.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {SequenceChloro.ID_JPA, Plateforme.ID_JPA}),
        indexes = {
            @Index(name = SequenceChloro.ID_JPA, columnList = SequenceChloro.ID_JPA),
            @Index(name = SousSequenceChloro.INDEX_ATTRIBUTS_PLATEFORME, columnList = Plateforme.ID_JPA)
        })
public class SousSequenceChloro implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "sschloro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "sous_sequence_chloro_sschloro";

    /**
     *
     */
    public static final String INDEX_ATTRIBUTS_PLATEFORME = "plateforme_chloro_ikey";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    // Association des mesures à une séquence
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SequenceChloro.ID_JPA, referencedColumnName = SequenceChloro.ID_JPA, nullable = false)
    private SequenceChloro sequenceChloro;

    // Association des Sequences à une plateforme
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = Plateforme.ID_JPA, referencedColumnName = Plateforme.ID_JPA, nullable = false)
    private Plateforme plateforme;

    // Une séquence peut avoir plusieurs Mesures
    @OneToMany(mappedBy = "sousSequenceChloro", cascade = ALL)
    private List<MesureChloro> mesures = new LinkedList<MesureChloro>();

    /**
     *
     */
    public SousSequenceChloro() {
        super();
    }

    /**
     *
     * @param sequenceChloro
     * @param plateforme
     */
    public SousSequenceChloro(SequenceChloro sequenceChloro, Plateforme plateforme) {
        super();
        this.sequenceChloro = sequenceChloro;
        this.plateforme = plateforme;
        if (!sequenceChloro.getSousSequences().contains(this)) {
            sequenceChloro.getSousSequences().add(this);
        }
    }

    // Id

    /**
     *
     * @return
     */
        public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SequenceChloro getSequenceChloro() {
        return sequenceChloro;
    }

    /**
     *
     * @param sequenceChloro
     */
    public void setSequenceChloro(SequenceChloro sequenceChloro) {
        this.sequenceChloro = sequenceChloro;
    }

    // Plateforme

    /**
     *
     * @return
     */
        public Plateforme getPlateforme() {
        return plateforme;
    }

    /**
     *
     * @param plateforme
     */
    public void setPlateforme(Plateforme plateforme) {
        this.plateforme = plateforme;
    }

    // Liste des (de la) mesure(s) de la séquence

    /**
     *
     * @return
     */
        public List<MesureChloro> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureChloro> mesures) {
        this.mesures = mesures;
    }

}// end of class

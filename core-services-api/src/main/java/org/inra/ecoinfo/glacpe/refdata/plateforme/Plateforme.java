/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 11:23:02
 */
package org.inra.ecoinfo.glacpe.refdata.plateforme;

import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.assertj.core.util.Strings;
import org.inra.ecoinfo.glacpe.refdata.Localisable;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.glacpe.refdata.typeplateforme.TypePlateforme;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.utils.Utils;


/**
 * @author "Antoine Schellenberger"
 * 
 */
@Entity
@Table(name = Plateforme.TABLE_NAME)
@PrimaryKeyJoinColumn(name = Plateforme.ID_JPA)
public class Plateforme extends Localisable {

    
    
    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "loc_id";

    /**
     *
     */
    public static final String TABLE_NAME = "plateforme_pla";
    /**
     *
     * @param namePlatforme
     * @return
     */
    
    public static String buildCode(String codeSite , String namePlatforme) {
        if (!Strings.isNullOrEmpty(codeSite)) {
            return String.format("%s,%s", codeSite, Utils.createCodeFromString(namePlatforme));
        } else {
            return Utils.createCodeFromString(namePlatforme);
        }
    }
     
    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;


    @Column(nullable = false)
    String name;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = TypePlateforme.ID_JPA, referencedColumnName = TypePlateforme.ID_JPA, nullable = false)
    private TypePlateforme typePlateforme;

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = SiteGLACPE.ID_JPA, referencedColumnName = SiteGLACPE.ID_JPA, nullable = false)
    private SiteGLACPE site;
    
    @Transient
    Long siteNodeId;
    
    @Transient
    Projet projet;
    /**
     *
     * @param nom
     * @param latitude
     * @param longitude
     * @param altitude
     * @param f2
     */
    public Plateforme(SiteGLACPE site, String nom, Float latitude, Float longitude, Float altitude) {
        super(latitude, longitude, altitude);
        setSite(site);
        setName(nom);
        this.setCode(buildCode(site==null?null:site.getCode(), getName()));
    }
    /**
     *
     */
    public Plateforme() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.site);
        hash = 59 * hash + Objects.hashCode(this.projet);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plateforme other = (Plateforme) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.site, other.site)) {
            return false;
        }
        if (!Objects.equals(this.projet, other.projet)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }

    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param nom
     */
    public void setName(String nom) {
        this.name=nom;
        this.setCode(buildCode(site==null?null:site.getCode(), getName()));
    }

    /**
     *
     * @return
     */
    public SiteGLACPE getSite() {
        return site;
    }

    /**
     *
     * @param site
     */
    public void setSite(SiteGLACPE site) {
        this.site = site;
        this.setCode(buildCode(site==null?null:site.getCode(), getName()));
    }


    /**
     *
     * @return
     */
    public TypePlateforme getTypePlateforme() {
        return typePlateforme;
    }

    /**
     *
     * @param typePlateforme
     */
    public void setTypePlateforme(TypePlateforme typePlateforme) {
        this.typePlateforme = typePlateforme;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) Plateforme.class;
    }

    /**
     *
     * @return
     */
    public Projet getProjet() {
        return projet;
    }

    /**
     *
     * @param projet
     */
    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    /**
     *
     * @return
     */
    public Long getSiteNodeId() {
        return siteNodeId;
    }

    /**
     *
     * @param siteNodeId
     */
    public void setSiteNodeId(Long siteNodeId) {
        this.siteNodeId = siteNodeId;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(INodeable o) {
        if(super.compareTo(o)!=0 ||projet==null){
            return super.compareTo(o);
        }
        if(o instanceof Plateforme ){
            return projet.compareTo(((Plateforme) o).projet);
        }
        return -1;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return getCode();
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.utils;

import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.glacpe.refdata.projet.Projet;
import org.inra.ecoinfo.glacpe.refdata.site.SiteGLACPE;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 *
 * @author ptcherniati
 */
public class LacsUtils {
    private static <T extends Nodeable> T getNodeableByType(Dataset dataset, Class<T> type) {
        final RealNode parcelleNode = dataset.getRealNode().getNodeByNodeableTypeResource(type);
        return parcelleNode == null ? null : (T) parcelleNode.getNodeable();
    }

    /**
     * @param dataset
     * @return
     */
    public static Projet getProjetFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, Projet.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static SiteGLACPE getSiteFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, SiteGLACPE.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static Theme getThemeFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, Theme.class);
    }

    /**
     * @param dataset
     * @return
     */
    public static DataType getDatatypeFromDataset(Dataset dataset) {
        return getNodeableByType(dataset, DataType.class);
    }
}
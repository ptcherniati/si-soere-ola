package org.inra.ecoinfo.glacpe.synthesis.zooplancton;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "ZooplanctonSynthesisDatatype")
public class SynthesisDatatype extends GenericSynthesisDatatype {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param site
     * @param minDate
     * @param maxDate
     * @param idNodes
     */
    public SynthesisDatatype(String site, LocalDateTime minDate, LocalDateTime maxDate, String idNodes) {
        super(minDate, maxDate, site, idNodes);
    }

    /**
     *
     */
    public SynthesisDatatype() {
        super();
    }
}

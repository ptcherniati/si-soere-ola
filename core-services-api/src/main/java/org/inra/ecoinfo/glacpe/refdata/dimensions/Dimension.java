/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.glacpe.refdata.dimensions;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.outilsmesures.OutilsMesure;

/**
 *
 * @author mylene
 */
@Entity
@Table(name = Dimension.TABLE_NAME)
public class Dimension implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    /**
     *
     */
    public static final String ID_JPA = "dim_id";
    
    /**
     *
     */
    public static final String TABLE_NAME = "dimension_dim";
    
    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;
    
    @OneToMany(mappedBy = "dimension", cascade = {MERGE, PERSIST, REFRESH})
    private List<OutilsMesure> outilsMesures = new LinkedList<OutilsMesure>();
    
    @Column(nullable = true)
    private Float xDepot;
    
    @Column(nullable = true)
    private Float yDepot;
    
    @Column(nullable = true)
    private Float zDepot;
    
    @Column(nullable = true)
    private Float lambdaDepot;
    
    @Column(nullable = true)
    private Float xReleve;
    
    @Column(nullable = true)
    private Float yReleve;
    
    @Column(nullable = true)
    private Float zReleve;
    
    @Column(nullable = true)
    private Float lambdaReleve;
    
    @Column(name = "date_depot", nullable = true)
    private LocalDate dateDepot;
    
    @Column(name = "heure_depot", nullable = true)
    private LocalTime heureDepot;
    
    @Column(name = "date_releve", nullable = true)
    private LocalDate dateReleve;
    
    @Column(name = "heure_releve", nullable = true)
    private LocalTime heureReleve;

    /**
     *
     */
    public Dimension() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Float getXDepot() {
        return xDepot;
    }
    
    /**
     *
     * @param xDepot
     */
    public void setXDepot(Float xDepot) {
        this.xDepot = xDepot;
    }

    /**
     *
     * @return
     */
    public Float getYDepot() {
        return yDepot;
    }
    
    /**
     *
     * @param yDepot
     */
    public void setYDepot(Float yDepot) {
        this.yDepot = yDepot;
    }


    /**
     *
     * @return
     */
    public Float getZDepot() {
        return zDepot;
    }
    
    /**
     *
     * @param zDepot
     */
    public void setZDepot(Float zDepot) {
        this.zDepot = zDepot;
    }


    /**
     *
     * @return
     */
    public Float getLambdaDepot() {
        return lambdaDepot;
    }
    
    /**
     *
     * @param lambdaDepot
     */
    public void setLambdaDepot(Float lambdaDepot) {
        this.lambdaDepot = lambdaDepot;
    }

    
    /**
     *
     * @return
     */
    public Float getXReleve() {
        return xReleve;
    }
    
    /**
     *
     * @param xReleve
     */
    public void setXReleve(Float xReleve) {
        this.xReleve = xReleve;
    }


    /**
     *
     * @return
     */
    public Float getYReleve() {
        return yReleve;
    }
    
    /**
     *
     * @param yReleve
     */
    public void setYReleve(Float yReleve) {
        this.yReleve = yReleve;
    }

    /**
     *
     * @return
     */
    public Float getZReleve() {
        return zReleve;
    }
    
    /**
     *
     * @param zReleve
     */
    public void setZReleve(Float zReleve) {
        this.zReleve = zReleve;
    }

    /**
     *
     * @return
     */
    public Float getLambdaReleve() {
        return lambdaReleve;
    }
    
    /**
     *
     * @param lambdaReleve
     */
    public void setLambdaReleve(Float lambdaReleve) {
        this.lambdaReleve = lambdaReleve;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateDepot() {
        return dateDepot;
    }
    
    /**
     *
     * @param dateDepot
     */
    public void setDateDepot(LocalDate dateDepot) {
        this.dateDepot = dateDepot;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeureDepot() {
        return heureDepot;
    }
    
    /**
     *
     * @param heureDepot
     */
    public void setHeureDepot(LocalTime heureDepot) {
        this.heureDepot = heureDepot;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateReleve() {
        return dateReleve;
    }

    /**
     *
     * @param dateReleve
     */
    public void setDateReleve(LocalDate dateReleve) {
        this.dateReleve = dateReleve;
    }
    
    /**
     *
     * @return
     */
    public LocalTime getHeureReleve() {
        return heureReleve;
    }
    
    /**
     *
     * @param heureReleve
     */
    public void setHeureReleve(LocalTime heureReleve) {
        this.heureReleve = heureReleve;
    }

    /**
     *
     * @return
     */
    public List<OutilsMesure> getOutilsMesures() {
        return outilsMesures;
    }
    
    /**
     *
     * @param outilsMesures
     */
    public void setOutilsMesures(List<OutilsMesure> outilsMesures) {
        this.outilsMesures = outilsMesures;
    }

    
}

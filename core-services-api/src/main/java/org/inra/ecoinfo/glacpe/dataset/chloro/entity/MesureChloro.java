/**
 * OREILacs project - see LICENCE.txt for use created: 24 septembre 2010 09:28:20
 */
package org.inra.ecoinfo.glacpe.dataset.chloro.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



/**
 * @author "Cédric Anache"
 * 
 */

@Entity
@Table(name = MesureChloro.TABLE_NAME, 
        uniqueConstraints = 
                @UniqueConstraint(columnNames = {SousSequenceChloro.ID_JPA, MesureChloro.NAME_ATTRIBUTS_PROFONDEUR_MIN, MesureChloro.NAME_ATTRIBUTS_PROFONDEUR_MAX}),
        indexes = {
            @Index(name = MesureChloro.NAME_ATTRIBUTS_PROFONDEUR_MAX, columnList = MesureChloro.NAME_ATTRIBUTS_PROFONDEUR_MAX),
            @Index(name = MesureChloro.NAME_ATTRIBUTS_PROFONDEUR_MIN, columnList = MesureChloro.NAME_ATTRIBUTS_PROFONDEUR_MIN),
            @Index(name = SousSequenceChloro.ID_JPA, columnList = SousSequenceChloro.ID_JPA)
        })
public class MesureChloro implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "mchloro_id";

    /**
     *
     */
    public static final String TABLE_NAME = "mesure_chloro_mchloro";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_PROFONDEUR_MIN = "profondeur_max";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_PROFONDEUR_MAX = "profondeur_min";

    /**
     *
     */
    public static final String NAME_ATTRIBUTS_LIGNE_FICHIER_ECHANGE = "ligne_fichier_echange";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    // Association des mesures à une séquence
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequenceChloro.ID_JPA, referencedColumnName = SousSequenceChloro.ID_JPA, nullable = false)
    private SousSequenceChloro sousSequenceChloro;

    // Une Mesure à plusieurs valeurs - Chloro A , Chloro B, Caroténoïde
    @OneToMany(mappedBy = "mesure", cascade = ALL)
    private List<ValeurMesureChloro> valeurs = new LinkedList<ValeurMesureChloro>();

    @Column(name = NAME_ATTRIBUTS_LIGNE_FICHIER_ECHANGE)
    private Long ligneFichierEchange;

    @Column(name = NAME_ATTRIBUTS_PROFONDEUR_MAX, nullable = false)
    private Float profondeur_max;

    @Column(name = NAME_ATTRIBUTS_PROFONDEUR_MIN, nullable = false)
    private Float profondeur_min;

    /**
     *
     */
    public MesureChloro() {
        super();
    }

    /**
     *
     * @param sousSequenceChloro
     * @param ligneFichierEchange
     * @param profondeur_max
     * @param f1
     * @param profondeur_min
     */
    public MesureChloro(SousSequenceChloro sousSequenceChloro, Long ligneFichierEchange, Float profondeur_max, Float profondeur_min) {
        super();
        this.sousSequenceChloro = sousSequenceChloro;
        this.ligneFichierEchange = ligneFichierEchange;
        this.profondeur_max = profondeur_max;
        this.profondeur_min = profondeur_min;
        if (!sousSequenceChloro.getMesures().contains(this)) {
            sousSequenceChloro.getMesures().add(this);
        }
    }

    // Id

    /**
     *
     * @return
     */
        public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public SousSequenceChloro getSousSequenceChloro() {
        return sousSequenceChloro;
    }

    /**
     *
     * @param sousSequenceChloro
     */
    public void setSousSequenceChloro(SousSequenceChloro sousSequenceChloro) {
        this.sousSequenceChloro = sousSequenceChloro;
    }

    // Liste des Mesures pour une Séquence

    /**
     *
     * @return
     */
        public List<ValeurMesureChloro> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesureChloro> valeurs) {
        this.valeurs = valeurs;
    }

    // Ligne fichier d'échange

    /**
     *
     * @return
     */
        public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    // Porfondeur Max

    /**
     *
     * @return
     */
        public Float getProfondeur_max() {
        return profondeur_max;
    }

    /**
     *
     * @param profondeur_max
     */
    public void setProfondeur_max(Float profondeur_max) {
        this.profondeur_max = profondeur_max;
    }

    // Profondeur Min

    /**
     *
     * @return
     */
        public Float getProfondeur_min() {
        return profondeur_min;
    }

    /**
     *
     * @param profondeur_min
     */
    public void setProfondeur_min(Float profondeur_min) {
        this.profondeur_min = profondeur_min;
    }

}// end of class

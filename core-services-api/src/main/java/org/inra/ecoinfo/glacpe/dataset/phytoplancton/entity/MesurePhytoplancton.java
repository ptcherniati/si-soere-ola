package org.inra.ecoinfo.glacpe.dataset.phytoplancton.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.refdata.site.Site;

/**
 * @author "Guillaume Enrico"
 *
 */
@Entity
@Table(name = MesurePhytoplancton.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {SousSequencePhytoplancton.ID_JPA, Taxon.ID_JPA}),
        indexes = {
            @Index(name = Taxon.ID_JPA, columnList = Taxon.ID_JPA),
            @Index(name = SousSequencePhytoplancton.ID_JPA, columnList = SousSequencePhytoplancton.ID_JPA)
        })
public class MesurePhytoplancton implements Serializable, Comparable<MesurePhytoplancton> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    static public final String TABLE_NAME = "mesure_phytoplancton_mphytoplancton";

    /**
     *
     */
    static public final String ID_JPA = "mphytoplancton_id";

    /**
     *
     */
    @Id
    @Column(name = "mphytoplancton_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = SousSequencePhytoplancton.ID_JPA, referencedColumnName = SousSequencePhytoplancton.ID_JPA, nullable = false)
    private SousSequencePhytoplancton sousSequence;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = Taxon.ID_JPA, referencedColumnName = Taxon.ID_JPA, nullable = false)
    private Taxon taxon;

    @OneToMany(mappedBy = "mesure", cascade = ALL)
    @OrderBy("id")
    private List<ValeurMesurePhytoplancton> valeurs = new LinkedList<ValeurMesurePhytoplancton>();

    /**
     *
     */
    public MesurePhytoplancton() {
        super();
    }

    /**
     * Classe raccourcis
     *
     * @return
     */
    public Site getSite() {
        return this.getSousSequence().getPlateforme().getSite();
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public SousSequencePhytoplancton getSousSequence() {
        return sousSequence;
    }

    /**
     *
     * @param sousSequence
     */
    public void setSousSequence(SousSequencePhytoplancton sousSequence) {
        this.sousSequence = sousSequence;
    }

    /**
     *
     * @return
     */
    public List<ValeurMesurePhytoplancton> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurMesurePhytoplancton> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @return
     */
    public Taxon getTaxon() {
        return taxon;
    }

    /**
     *
     * @param taxon
     */
    public void setTaxon(Taxon taxon) {
        this.taxon = taxon;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.sousSequence);
        hash = 41 * hash + Objects.hashCode(this.taxon);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MesurePhytoplancton other = (MesurePhytoplancton) obj;
        if (!Objects.equals(this.sousSequence, other.sousSequence)) {
            return false;
        }
        if (!Objects.equals(this.taxon, other.taxon)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(MesurePhytoplancton t) {
        if (t == null) {
            return -1;
        }
        int compareToSousSequence = getSousSequence().compareTo(t.getSousSequence());
        if (compareToSousSequence != 0) {
            return compareToSousSequence;
        }
        return getTaxon().compareTo(t.getTaxon());
    }
}

package org.inra.ecoinfo.glacpe.refdata.taxonniveau;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.taxon.Taxon;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TaxonNiveau.TABLE_NAME)
public class TaxonNiveau implements Serializable {

    

    //afiocca

    /**
     *
     */
    public static final String CODE_SANDRE = "code_sandre";

    /**
     *
     */
    public static final String CONTEXTE = "contexte";


    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "taxn_id";

    /**
     *
     */
    public static final String TABLE_NAME = "taxon_niveau_taxn";

 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_JPA)
    protected Long id;

    @Column(unique = true, nullable = false)
    private String nom;

    @Column(nullable = false, unique = true)
    private String code;
    
    

    @Column(nullable = true,name = CODE_SANDRE, unique = false)
    private String codeSandre;
    
    @Column(nullable = true,name = CONTEXTE , unique = false)
    private String contexte;
    @OneToMany(cascade = ALL, mappedBy = "taxonNiveau")
    private List<Taxon> taxons = new LinkedList<Taxon>();
    /**
     *
     */
    public TaxonNiveau() {
        super();
    }
    /**
     *
     * @param string
     * @param nom
     */
    public TaxonNiveau(String nom) {
        super();
        setNom(nom);
    }

    /**
     *
     * @return
     */
    public String getCodeSandre() {
        return codeSandre;
    }

    /**
     *
     * @param codeSandre
     */
    public void setCodeSandre(String codeSandre) {
        this.codeSandre = codeSandre;
    }
    
    /**
     *
     * @return
     */
    public String getContexte() {
        return contexte;
    }

    /**
     *
     * @param contexte
     */
    public void setContexte(String contexte) {
        this.contexte = contexte;
    }


    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     *
     * @return
     */
    public List<Taxon> getTaxons() {
        return taxons;
    }

    /**
     *
     * @param taxons
     */
    public void setTaxons(List<Taxon> taxons) {
        this.taxons = taxons;
    }


    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }
}

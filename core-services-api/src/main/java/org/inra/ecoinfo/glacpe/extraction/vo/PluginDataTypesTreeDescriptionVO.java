package org.inra.ecoinfo.glacpe.extraction.vo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
public class PluginDataTypesTreeDescriptionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pluginId;

    private List<RealNode> datatypes = new LinkedList<RealNode>();

    /**
     *
     * @return
     */
    public String getPluginId() {
        return pluginId;
    }

    /**
     *
     * @param pluginId
     */
    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    /**
     *
     * @return
     */
    public List<RealNode> getDatatypes() {
        return datatypes;
    }

    /**
     *
     * @param datatypes
     */
    public void setDatatypes(List<RealNode> datatypes) {
        this.datatypes = datatypes;
    }
}

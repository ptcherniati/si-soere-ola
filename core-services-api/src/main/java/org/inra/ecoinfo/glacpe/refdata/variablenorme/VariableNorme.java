package org.inra.ecoinfo.glacpe.refdata.variablenorme;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.inra.ecoinfo.glacpe.refdata.variable.VariableGLACPE;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = VariableNorme.TABLE_NAME)
public class VariableNorme implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = "vnor_id";

    /**
     *
     */
    public static final String TABLE_NAME = "variable_norme_vnor";

    /**
     *
     */
    @Id
    @Column(name = ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(nullable = false, unique = true)
    private String nom;

    @OneToMany(mappedBy = "variableNorme", cascade = {MERGE, PERSIST, REFRESH})
    private List<VariableGLACPE> variables = new LinkedList<VariableGLACPE>();

    @Column(nullable = false, unique = true)
    private String code;
    @Column(nullable = true,columnDefinition = "TEXT")
    
    private String definition;
    /**
     *
     * @param nom
     * @param string1
     * @param definition
     */
    public VariableNorme(String nom, String definition) {
        super();
        setNom(nom);
        this.definition = definition;
    }
    /**
     *
     */
    public VariableNorme() {
        super();
        
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
        this.code = Utils.createCodeFromString(nom);
    }


    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public List<VariableGLACPE> getVariables() {
        return variables;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(List<VariableGLACPE> variables) {
        this.variables = variables;
    }

    /**
     *
     * @return
     */
    public String getDefinition() {
        return definition;
    }

    /**
     *
     * @param definition
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }

}
